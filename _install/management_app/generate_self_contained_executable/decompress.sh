#!/bin/bash

echo ""
echo "***************************************************************************************"
echo "***************************************************************************************"
echo "**         Welcome to the installation utility of Reaction Management App!           **"
echo "***************************************************************************************"
echo "***************************************************************************************"
echo ""
echo "Please make sure that the 'tar' command is present on the OS!"
read -p "> Press enter to continue!"
echo ""
echo "...extracting the installation files of Reaction management app and the installation utility will start soon..."
echo ""

export TMPDIR=`mktemp -d /tmp/selfextract.XXXXXX`

ARCHIVE=`awk '/^__ARCHIVE_BELOW__/ {print NR + 1; exit 0; }' $0`

tail -n+$ARCHIVE $0 | tar xz -C $TMPDIR

CDIR=`pwd`
cd $TMPDIR
. ./installer.sh

cd $CDIR
rm -rf $TMPDIR

echo ""
echo "---------------------------------------------------------------------------------------------------------------"
echo "  It might be worth to make a note of the following info..."
echo "  If the installation utility wasn't completed successfully and it has to be called later then please execute"
echo "  (and not executing the reaction_management_app_installation_v1_1.bsx multiple times):"
echo "        $REACTION_MANAGEMENT_APP_INSTALL_DIR/install-reaction-management-app_v1_1.sh"
echo "  If the installation utility asks where the installation files are then please use the following path:"
echo "        $REACTION_MANAGEMENT_APP_INSTALL_DIR"
echo "---------------------------------------------------------------------------------------------------------------"
read -p "> Press enter to continue!"
echo ""

export REACTION_MANAGEMENT_APP_INSTALL_DIR=$REACTION_MANAGEMENT_APP_INSTALL_DIR
$REACTION_MANAGEMENT_APP_INSTALL_DIR/install-reaction-management-app_v1_1.sh

exit 0

__ARCHIVE_BELOW__
