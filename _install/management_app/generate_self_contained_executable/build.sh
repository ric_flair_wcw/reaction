#!/bin/bash
cd payload
tar cf ../payload.tar ./*
cd ..

if [ -e "payload.tar" ]; then
	gzip payload.tar

	if [ -e "payload.tar.gz" ]; then
		cat decompress.sh payload.tar.gz > build/reaction_management_app_installation_v1_1.bsx
		chmod a+x build/reaction_management_app_installation_v1_1.bsx
	else
		echo "payload.tar.gz does not exist"
		exit 1
	fi
else
	echo "payload.tar does not exist"
	exit 1
fi

echo "The file build/reaction_management_app_installation_v1_1.bsx is created!"
rm -f payload.tar.gz
exit 0
