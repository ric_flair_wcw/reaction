#!/bin/bash

install_dir="/tmp/reaction_ma_"$(date +%s%3N)
mkdir $install_dir
export REACTION_MANAGEMENT_APP_INSTALL_DIR=$install_dir
tar -xf ./files.tar -C $install_dir
