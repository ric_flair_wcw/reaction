#!/bin/bash

# ############################ util functions ############################
function read_input_any_not_empty {
    while [[ -z "$string" ]] 
    do
        read -p "$1`echo $'\n> '`" string
    done
    echo "$string"
}

function read_input_yn {
    while true; do
        read -p "$1`echo $'\n> '`" yn
        case $yn in
            [Yn]* ) echo "$yn"; break;;
        esac
    done
}

function read_input {
    while true; do
        read -p "$1`echo $'\n> '`" yn
        case $yn in
            [$2]* ) echo "$yn"; break;;
        esac
    done
}

function question_header {
    echo ""
    echo ""
    echo ""
    echo "------------------------------------------------------------- Reaction management app installation"
    echo "/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\/\\"
    echo "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
}

function execute {
    eval $@
    if [ "$?" -ne "0" ]; then
        echo "The execution of the command '$1' wasn't succesful! Exiting..."
        exit 1
    fi
}

# ############################ the flow of question ############################

# final message
function final_message {
    question_header
    echo "The installation is finished, congratulation!"
    echo ""
    echo "-------------------------------------------------------------------------------------------------------------------------------------"
    echo "The URL of the Reaction management application:          http://"$(hostname -i)"/$reaction_subdomain"
    echo "The URL of the user management"
    echo "  (please log in to this application with the "
    echo "   superuser and create the users):                      http://"$(hostname -i)"/$reaction_subdomain/admin"
    echo "The configuration file of the Reaction management app:   $management_app_location/management_app/settings_reaction.py"
    if [ $os_type == "u" ] || [ $os_type == "s" ]; then
        echo "The configuration file of the web server:                /etc/apache2/sites-available/000-default.conf"
        echo "The log file of the web server:                          /var/log/apache2"
    else
        echo "The configuration file of the web server:                /etc/httpd/conf.d/reaction.conf"
        echo "The log file of the web server:                          /var/log/httpd"
    fi
    echo "The source files of the management app:                  $management_app_location"
    echo "The python virtual environment:                          $virtualenv_path"
    echo "-------------------------------------------------------------------------------------------------------------------------------------"
    echo ""
    echo "Please make sure that the log file of the Reaction management app can be written by the user of the web server"
    echo "    (or by anybody like 'chmod a+w [log file path]')!"
    echo ""
    if [ $os_type == "u" ] || [ $os_type == "s" ]; then
        echo "Restarting apache2..."
        local command="sudo service apache2 restart"
    else
        echo "Restarting the httpd service."
        local command="sudo systemctl restart httpd.service"
    fi
    echo "  ** The following commands will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
}


# management app
function create_management_app_superuser {
    local command="source $virtualenv_activation && cd $management_app_location && python manage.py createsuperuser"
    echo "  ** The following commands will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
    final_message
}

function is_the_management_app_superuser_already_created {
    question_header
    local res=$(read_input_yn "Is the management app superuser already created? [Y/n]")
    case $res in
        Y ) final_message ;;
        n ) create_management_app_superuser ;;
    esac
}

function create_reaction_database {
    question_header
    local command="source $virtualenv_activation && cd $management_app_location && export LD_LIBRARY_PATH=$reaction_ld_library_path:$LD_LIBRARY_PATH && python manage.py makemigrations admin_system admin_execution_flow admin_errordetector monitoring scheduler common worker_status && python manage.py migrate"
    echo "The Reaction database schema will be created! (the schema creation can be run on an existing schema multiple times without loosing data)"
    echo "WARNING! The reaction database and the database user of the reaction database have to be created before this operation and this user has to be able to read/write the reaction database! (please see the DATABASE section in $management_app_location/management_app/settings_reaction.py)"
    echo "  ** The following command will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
    if [ "$database_type" == "m" ]; then
        export R_SQL_F="$install_dir/reaction_mysql.sql"
    else
        export R_SQL_F="$install_dir/reaction_oracle.sql"
    fi
    echo ""
    echo "The SQL file $R_SQL_F will be executed."
    read -p "> Press enter to continue!"
    source $virtualenv_activation && cd $management_app_location && export LD_LIBRARY_PATH=$reaction_ld_library_path:$LD_LIBRARY_PATH && python manage.py shell << EOF
import sys
from django.db import connection
try:
    with connection.cursor() as c:
        with open("$R_SQL_F", "r") as sql_file:
            for sql_statement in sql_file:
               if sql_statement.strip():
                   print(sql_statement)
                   c.execute(sql_statement)
    connection.commit()
finally:
    connection.close()
EOF
    if [ "$?" -ne "0" ]; then
        echo "The execution wasn't succesful! Exiting..."
        exit 1
    fi
    is_the_management_app_superuser_already_created
}

function is_the_reaction_database_already_created {
    question_header
    local res=$(read_input_yn "Are the Reaction database tables created yet? [Y/n]")
    case $res in
        Y ) is_the_management_app_superuser_already_created ;;
        n ) create_reaction_database ;;
    esac
}

function configure_the_management_app_by_altering_the_settings_xxxxx_properties {
    question_header
    echo "Please configure the management app by altering the $management_app_location/management_app/settings_reaction.py config file!"
    echo "    - DATABASES: please see the seetings in https://docs.djangoproject.com/en/1.11/ref/settings/#databases"
    echo "    - LOGGING: 'filename' -> where the log file will reside (write permission is needed so please use absolute path!)"
    echo "    - Mail settings"
    echo "    - REACTION_ENGINE_REST_URL: endpoint URL of the reaction engine REST"
    echo "    - TIME_ZONE"
    echo "    - user name / password for authenticating against Reaction Engine REST: REACTION_REST_AUTH_PUBLIC_KEY, REACTION_REST_AUTH_PRIVATE_KEY"
    read -p "> Press enter to continue!"
    is_the_reaction_database_already_created
}

function perform_the_following_configuration {
    if [ $os_type == "u" ] || [ $os_type == "s" ]; then
        # Ubuntu
        if [[ $use_existing_apache == 'n' || $use_existing_apache == 'Y' && $overwrite_000_default_conf_file == 'Y' ]]; then
            mkdir -p /etc/apache2/sites-available
            echo "Overwriting the file /etc/apache2/sites-available/000-default.conf..."
            export R_SUBDOMAIN="$reaction_subdomain"
            export R_M_APP_LOCATION="$management_app_location"
            export R_VIRT_ENV_DIR="$virtualenv_path"
            sudo -E bash -c 'cat > /etc/apache2/sites-available/000-default.conf << EOF
<VirtualHost *:80>
  LoadModule wsgi_module $R_VIRT_ENV_DIR/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so

  Alias /$R_SUBDOMAIN/static $R_M_APP_LOCATION/static
  <Directory $R_M_APP_LOCATION/static>
    Allow from all
    Require all granted
  </Directory>
  <Directory $R_M_APP_LOCATION/management_app>
    <Files wsgi.py>
      Require all granted
    </Files>
  </Directory>

  WSGIDaemonProcess management-app python-path=$R_M_APP_LOCATION:$R_VIRT_ENV_DIR/lib/python3.6/site-packages
  WSGIProcessGroup management-app
  WSGIScriptAlias /$R_SUBDOMAIN $R_M_APP_LOCATION/management_app/wsgi.py process-group=management-app
  PassEnv LD_LIBRARY_PATH
  PassEnv REACTION_SUBDOMAIN
</VirtualHost>
EOF'
            if [ "$?" -ne "0" ]; then
                echo "The execution of the command wasn't successful! Exiting..."
                exit 1
            fi
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo "!!!!!!!!!!!!!!!!!!!!!! Warning !!!!!!!!!!!!!!!!!!!!!!"
            echo "The following path was used to set the path to mod_wsgi: $virtualenv_path/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so"
            echo "Please check if it really exists and if it doesn't then please change the 'LoadModule' setting of the file '/etc/apache2/sites-available/000-default.conf'!"
            unset R_SUBDOMAIN
            unset R_M_APP_LOCATION
            unset R_VIRT_ENV_DIR
        else
            echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            echo "!!!!!!!!!!!!!!!!!!!!!! Warning !!!!!!!!!!!!!!!!!!!!!!"
            echo "The file '/etc/apache2/sites-available/000-default.conf' cannot be overwritten so Apache is not configured for Reaction management app!"
            echo "The following virtual host configuration should be recommended to put to /etc/apache2/sites-available/000-default.conf:"
            echo "<VirtualHost *:80>"
            echo "  LoadModule wsgi_module $R_VIRT_ENV_DIR/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so"
            echo "  Alias /$R_SUBDOMAIN/static $R_M_APP_LOCATION/static"
            echo "  <Directory $R_M_APP_LOCATION/static>"
            echo "    Allow from all"
            echo "    Require all granted"
            echo "  </Directory>"
            echo "  <Directory $R_M_APP_LOCATION/management_app>"
            echo "    <Files wsgi.py>"
            echo "      Require all granted"
            echo "    </Files>"
            echo "  </Directory>"
            echo "  WSGIDaemonProcess management-app python-path=$R_M_APP_LOCATION:$R_VIRT_ENV_DIR/lib/python3.6/site-packages"
            echo "  WSGIProcessGroup management-app"
            echo "  WSGIScriptAlias /$R_SUBDOMAIN $R_M_APP_LOCATION/management_app/wsgi.py process-group=management-app"
            echo "  PassEnv LD_LIBRARY_PATH"
            echo "  PassEnv REACTION_SUBDOMAIN"
            echo "</VirtualHost>"
            read -p "> Press enter to continue!"
        fi
        echo "Adding the variables to the file /etc/apache2/envvars..."
        if [ "$database_type" == "O" ]; then
            local command="sudo bash -c 'echo \"export LD_LIBRARY_PATH=$reaction_ld_library_path\" >> /etc/apache2/envvars'"
            echo "  ** The following command will be executed: $command"
            execute $command
        fi
        local command="sudo bash -c 'echo \"export REACTION_SUBDOMAIN=$reaction_subdomain\" >> /etc/apache2/envvars'"
        echo "  ** The following command will be executed: $command"
        execute $command
    else
        # CentOS / Red Hat
        export R_SUBDOMAIN="$reaction_subdomain"
        export R_M_APP_LOCATION="$management_app_location"
        export R_VIRT_ENV_DIR="$virtualenv_path"
        sudo -E bash -c 'cat > /etc/httpd/conf.d/reaction.conf << EOF
LoadModule wsgi_module $R_VIRT_ENV_DIR/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so

Alias /$R_SUBDOMAIN/static $R_M_APP_LOCATION/static
<Directory $R_M_APP_LOCATION/static>
  Allow from all
  Require all granted
</Directory>
<Directory $R_M_APP_LOCATION/management_app>
  <Files wsgi.py>
    Require all granted
  </Files>
</Directory>

WSGIDaemonProcess management-app python-path=$R_M_APP_LOCATION:$R_VIRT_ENV_DIR/lib/python3.6/site-packages
WSGIProcessGroup management-app
WSGIScriptAlias /$R_SUBDOMAIN $R_M_APP_LOCATION/management_app/wsgi.py process-group=management-app
PassEnv LD_LIBRARY_PATH
PassEnv REACTION_SUBDOMAIN

EOF'
        if [ "$?" -ne "0" ]; then
            echo "The execution of the command wasn't succesful! Exiting..."
            exit 1
        fi
        echo ""
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!! Warning !!!!!!!!!!!!!!!!!!!!!!"
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "The following path was used to set the path to mod_wsgi: $virtualenv_path/lib/python3.6/site-packages/mod_wsgi/server/mod_wsgi-py36.cpython-36m-x86_64-linux-gnu.so"
        echo "Please check if it really exists and if it doesn't then please change the 'LoadModule' setting of the file '/etc/httpd/conf.d/reaction.conf'!"
        echo ""
        unset R_SUBDOMAIN
        unset R_M_APP_LOCATION
        unset R_VIRT_ENV_DIR
        echo "Adding the variables to the file /etc/sysconfig/httpd..."
        if [ "$database_type" == "O" ]; then
            local command="sudo bash -c 'echo \"LD_LIBRARY_PATH=$reaction_ld_library_path\" >> /etc/sysconfig/httpd'"
            echo "  ** The following command will be executed: $command"
            execute $command
        fi
        local command="sudo bash -c 'echo \"REACTION_SUBDOMAIN=$reaction_subdomain\" >> /etc/sysconfig/httpd'"
        echo "  ** The following command will be executed: $command"
        execute $command
    fi
    echo "Setting the DATABASES/ENGINE in settings_reaction.py file..."
    if [ "$database_type" == "m" ]; then
        local command="sed -i \"s/'ENGINE': 'django.db.backends.oracle'/'ENGINE': 'django.db.backends.mysql'/g\" $management_app_location/management_app/settings_reaction.py"
        echo "  ** The following command will be executed: $command"
        execute $command
        local command="sed -i \"s/'PORT': '1521'/'PORT': '3306', 'OPTIONS': { 'autocommit': False, }/g\" $management_app_location/management_app/settings_reaction.py"
        echo "  ** The following command will be executed: $command"
        execute $command
        echo "Altering $management_app_location/management_app/settings_reaction.py file to initialize the mysql database driver"
        execute "sed -i '1s/^/import pymysql\npymysql.install_as_MySQLdb()\n/' $management_app_location/management_app/settings_reaction.py"
    fi
    configure_the_management_app_by_altering_the_settings_xxxxx_properties
}
    
function may_i_perform_the_following_configuration {
    question_header
    echo "May I perform the following configuration tasks?"
    if [ $os_type == "u" ] || [ $os_type == "s" ]; then
        if [[ $use_existing_apache == 'n' || $use_existing_apache == 'Y' && $overwrite_000_default_conf_file == 'Y' ]]; then
            echo "    - overwriting the file /etc/apache2/sites-available/000-default.conf"
        fi
        echo "    - adding variables to the file /etc/apache2/envvars"
    else
        echo "    - creating the file /etc/httpd/conf.d/reaction.conf"
        echo "    - adding variables to the file /etc/sysconfig/httpd"
    fi
    echo "    - setting the DATABASES/ENGINE in settings_reaction.py file"
    echo "    - altering $management_app_location/management_app/settings_reaction.py file to initialize the mysql database driver"
    local res=$(read_input_yn "[Y/n]")
    case $res in
        Y ) perform_the_following_configuration ;;
        n ) echo "Please be aware that the application won't work without these configuration settings!"; configure_the_management_app_by_altering_the_settings_xxxxx_properties ;;
    esac
}

function specify_subdomain_of_management_app {
    question_header
    reaction_subdomain=$(read_input_any_not_empty "Please specify the subdomain of the Management App (e.g. in the URL 'http://localhost/reaction-management' the subdomain is 'reaction-management'):")
    if [ "$database_type" == "O" ]; then
        reaction_ld_library_path=$(read_input_any_not_empty "Please specify the value of LD_LIBRARY_PATH (e.g. /usr/lib/oracle/12.2/client64/lib):")
    fi
    if [ $os_type == "u" ] || [ $os_type == "s" ]; then
      if [[ $use_existing_apache == "Y" ]]; then
          overwrite_000_default_conf_file=$(read_input_yn "May I overwrite the file '/etc/apache2/sites-available/000-default.conf'? [Y/n]")
      fi
    fi
    may_i_perform_the_following_configuration
}

function specify_where_the_management_app_will_reside {
    management_app_location=$(read_input_any_not_empty "Please specify where the management app will reside (e.g. /local/reaction/management_app):")
    mkdir -p $management_app_location
    local command="unzip $install_dir/management_app-1.1.zip -d $management_app_location"
    echo "  ** The following command will be executed: $command"
    execute $command
    specify_subdomain_of_management_app
}

function may_i_extract_the_management_app_zip {
    question_header
    local res=$(read_input_yn "May I extract the management app ZIP? [Y/n]")
    case $res in
        Y ) specify_where_the_management_app_will_reside ;;
        n ) management_app_location=$(read_input_any_not_empty "Please specify where the management app ZIP was extracted to:"); specify_subdomain_of_management_app ;;
    esac
}

function do_you_need_help_to_set_up_management_app {
    question_header
    local res=$(read_input_yn "Do you need help to set up the management app? [Y/n]")
    case $res in
        Y ) may_i_extract_the_management_app_zip ;;
        n ) final_message ;;
    esac
}

# database client
function install_the_python_oracle_package {
    question_header
    local command="source $virtualenv_activation && pip install cx-Oracle"
    echo "I am going to install the python Oracle package!"
    echo "  ** The following command will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
    do_you_need_help_to_set_up_management_app
}

function is_the_python_oracle_package_already_installed {
    question_header
    local res=$(read_input_yn "Is the python Oracle package already installed? [Y/n]")
    case $res in
        Y ) do_you_need_help_to_set_up_management_app ;;
        n ) install_the_python_oracle_package ;;
    esac
}

function please_manually_install_oracle_client_first {
    question_header
    if [ $os_type == "u" ]; then
        execute "sudo apt-get -y install libaio1"
    elif [ $os_type == "s" ]; then
        execute "zypper install --no-confirm libaio"
    else
        execute "sudo yum -y install libaio"
    fi
    echo "Please manually install Oracle Client first!"
    echo "    The client can be download from: http://www.oracle.com/technetwork/database/features/instant-client/index.html"
    echo "    The Basic Light package is enough if only English language is needed."
    if [ $os_type == "u" ]; then
        echo "    Please download and convert it to to Ubuntu format with alien. (install alien: sudo apt-get install alien; install the RPM: alien -i [RPM package name])"
    fi
    echo "    Installation instructions: https://oracle.github.io/odpi/doc/installation.html#linux"
    echo "    Please be aware, if the local python installation is 32 bit then during the installation of the python Oracle package (see one step later) will install a 32bit cx_oracle so the Oracle Client has to be 32bit too! (Even if the machine/windows is 64 bit.)"
    echo "          To find out if the python is 32 or 64 bit is please execute the following commands in the python shell (make sure the virtual env is activated and just type python in Linux command shell):"
    echo "                 import struct"
    echo '                 print(struct.calcsize("P") * 8)'
    read -p "> Press enter to continue when you finished installing the Oracle Client!"
    is_the_python_oracle_package_already_installed
}

function is_oracle_client_already_installed {
    question_header
    local res=$(read_input_yn "Is Oracle client already installed? [Y/n]")
    case $res in
        Y ) is_the_python_oracle_package_already_installed ;;
        n ) please_manually_install_oracle_client_first ;;
    esac
}

function do_you_use_oracle_or_mysql {
    question_header
    local res=$(read_input "Do you use Oracle or mysql (mariadb)? [O/m]" "Om")
    database_type=$res
    case $res in
        O ) is_oracle_client_already_installed ;;
        m ) echo "The mysql driver (PyMySQL) is already installed."; do_you_need_help_to_set_up_management_app ;;
    esac
}

# Apache
function install_mod_wsgi {
    question_header
    if [ $os_type == "u" ]; then
        local command="sudo apt-get install build-essential && source $virtualenv_activation && pip install -I mod_wsgi==4.6.4"
    elif [ $os_type == "r" ] && [ $redhat_version == "8" ]; then
        local command="sudo yum -y install redhat-rpm-config kernel-headers gcc && source $virtualenv_activation && pip install -I mod_wsgi==4.6.4"
    elif [ $os_type == "s" ]; then
        local command="source $virtualenv_activation && pip install -I mod_wsgi==4.6.4"
    else
        local command="sudo yum -y install gcc && source $virtualenv_activation && pip install -I mod_wsgi==4.6.4"
    fi
    echo "I am going to install mod_wsgi"
    echo "  ** The following commands will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
    do_you_use_oracle_or_mysql
}

function is_mod_wsgi_installed_on_existing_apache {
    question_header
    local res=$(read_input_yn "Is mod_wsgi installed in the virtual environment? [Y/n]")
    case $res in
        Y ) do_you_use_oracle_or_mysql ;;
        n ) install_mod_wsgi ;;
    esac
}

function may_i_install_the_apache {
    echo ""
    if [ $os_type == "u" ]; then
        local command="sudo -E apt-get install -y apache2 apache2-dev"
    elif [ $os_type == "s" ]; then
        local command="zypper install --no-confirm apache2 apache2-devel"
    else
        local command="sudo yum install -y httpd httpd-devel"
    fi
    echo "  ** The following commands will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
    install_mod_wsgi
}

function do_you_want_to_use_an_existing_apache {
    question_header
    local res=$(read_input_yn "Is Apache already installed on the server? [Y/n]")
    if [ $os_type == "u" ] || [ $os_type == "s" ]; then
        use_existing_apache=$res
    fi
    case $res in
        Y ) is_mod_wsgi_installed_on_existing_apache ;;
        n ) may_i_install_the_apache ;;
    esac
}

# python3
function install_django_and_the_dependencies_of_reaction_into_the_virtual_environment {
    if [ $os_type == "u" ]; then
        local command="sudo apt-get install -y libssl-dev libffi-dev && sudo apt-get install -y language-pack-en && source $virtualenv_activation && export LC_ALL=en_US.UTF-8 && export LC_CTYPE=en_US.UTF-8 && pip install --upgrade pip && pip install wheel && pip install -r $install_dir/requirements.txt"
    else
        if [ $os_type == "r" ]; then
            local command="yum install -y gcc && source $virtualenv_activation && export LC_ALL=en_US.UTF-8 && export LC_CTYPE=en_US.UTF-8 && pip install --upgrade pip && pip install -r $install_dir/requirements.txt"
        else
            local command="source $virtualenv_activation && export LC_ALL=en_US.UTF-8 && export LC_CTYPE=en_US.UTF-8 && pip install --upgrade pip && pip install -r $install_dir/requirements.txt"
        fi
    fi
    echo "  ** The following commands will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
    do_you_want_to_use_an_existing_apache
}

function may_i_install_django_and_the_dependencies_of_reaction_into_the_virtual_environment {
    question_header
    local res=$(read_input_yn "May I install Django and the dependencies of Reaction into the virtual environment? [Y/n]")
    case $res in
        Y ) install_django_and_the_dependencies_of_reaction_into_the_virtual_environment ;;
        n ) do_you_want_to_use_an_existing_apache ;;
    esac    
}

function specify_where_the_virtual_environment_should_reside {
    virtualenv_path=$(read_input_any_not_empty "Please specify where the virtual environment should reside (the created virtual environment will be in [specified_location]/reaction-venv):")"/reaction-venv"
    mkdir -p $virtualenv_path
    local command="$python_path -m venv $virtualenv_path"
    echo "  ** The following command will be executed: $command"
    virtualenv_activation=$virtualenv_path/bin/activate
    read -p "> Press enter to continue!"
    execute $command
    may_i_install_django_and_the_dependencies_of_reaction_into_the_virtual_environment
}

function may_i_create_a_virtual_environment {
    question_header
    local res=$(read_input_yn "May I create a virtual environment? [Y/n]")
    case $res in
        Y ) specify_where_the_virtual_environment_should_reside;;
        n ) virtualenv_activation=$(read_input_any_not_empty "Please specify the command to activate the already existing virtual environment (e.g. /local/reaction/reaction-venv/bin/activate):"); virtualenv_path=$(read_input_any_not_empty "Please specify where the virtual environment resides (e.g. /local/reaction/reaction-venv):"); may_i_install_django_and_the_dependencies_of_reaction_into_the_virtual_environment ;;
    esac
}

function install_python_with_development_package_and_pip {
    echo ""
    echo "I am going to install python 3.6 with development package and pip."
    if [ $os_type == "u" ]; then
        if [[ $os_type == "u" && $ubuntu_version == "8" ]]; then
            local command="sudo -E apt-get install -y python3-venv python3-dev python3-pip"
            echo "  ** The following commands will be executed: $command"
            read -p "> Press enter to continue!"
            execute $command
            may_i_create_a_virtual_environment
        else
            local command="sudo -E apt-get install -y software-properties-common && sudo -E add-apt-repository -y ppa:deadsnakes/ppa && sudo -E apt-get update -y && sudo -E apt-get install -y python3.6 python3.6-venv python3.6-dev"
            echo "  ** The following commands will be executed: $command"
            read -p "> Press enter to continue!"
            execute $command
            echo ""
            echo ""
            if [[ $ubuntu_version == "6" ]]; then
                echo "Setting python3.6 to run when python3 is executed."
                local command="sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.5 1 && sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 2 && echo "0" | sudo update-alternatives --config python3"
                echo "  ** The following commands will be executed: $command"
                read -p "> Press enter to continue!"
                execute $command
                echo ""
                echo ""
            fi
            echo "Installing pip."
            local command="sudo apt-get install -y python3-pip"
        fi
    elif [ $os_type == "r" ]; then
        if [[ $redhat_version == "7" ]]; then
            echo "Before executing the command below please make sure to enable the 'rhscl' and 'optional' software repos using subscription-manager!"
            echo "For example: subscription-manager repos --enable rhel-7-server-optional-rpms --enable rhel-server-rhscl-7-rpms"
            echo "More info: https://developers.redhat.com/blog/2018/08/13/install-python3-rhel#"
            local command="sudo yum -y install rh-python36"
            python_path="/opt/rh/rh-python36/root/usr/bin/python"
        else
            local command="yum install -y python36 python36-devel python3-pip"
        fi
    elif [ $os_type == "s" ]; then
        if [[ $(zypper repos) =~ "home_adrianSuSE" ]]; then
            local command="zypper --no-gpg-checks -n refresh && zypper install -y -n python3 python3-pip python3-devel"
        else
            local command="zypper addrepo https://download.opensuse.org/repositories/home:adrianSuSE/openSUSE_Factory/home:adrianSuSE.repo && zypper --no-gpg-checks -n refresh && zypper install -y -n python3 python3-pip python3-devel"
        fi
    else
        local command="sudo yum -y install python36u python36u-devel python36u-pip"
    fi
    echo "  ** The following commands will be executed: $command"
    read -p "> Press enter to continue!"
    execute $command
    may_i_create_a_virtual_environment
}

function is_python_already_installed {
    question_header
    local res=$(read_input_yn "Is python3.6 already installed (with development package and pip)? [Y/n]")
    case $res in
        Y ) python_path=$(read_input_any_not_empty "Please specify the location of the installed python (e.g. '/usr/bin/python3.6', 'python3.6', etc)"); may_i_create_a_virtual_environment ;;
        n ) python_path="python3.6"; install_python_with_development_package_and_pip ;;
    esac
}


# ############################ main ############################
#BASH_XTRACEFD="5"
#PS4='$LINENO: '
#set -x

export DEBIAN_FRONTEND=noninteractive

os_type=$(read_input "Are you using Red Hat Linux, CentOS, Ubuntu or SUSE? [r/c/u/s]" "rcus")
if [ $os_type == "u" ]; then
    ubuntu_version=$(read_input "Which Ubuntu version are you using (16.04, 18.04 or 20.04)? [6/8/2]" "682")
elif [ $os_type == "r" ]; then
    redhat_version=$(read_input "Which Red Hat version are you using (7 or 8)? [7/8]" "78")
fi

echo ""
echo ""

echo "Installing unzip..."
if [ $os_type == "u" ]; then
    execute "sudo apt-get install -y unzip"
elif [ $os_type == "s" ]; then
    execute "zypper install --no-confirm unzip"
else
    execute "sudo yum -y install unzip"
fi
if [[ -z ${REACTION_MANAGEMENT_APP_INSTALL_DIR} ]]; then
    echo ""
    install_dir=$(read_input_any_not_empty "Please specify the location where the installation files are:")
else
    install_dir=${REACTION_MANAGEMENT_APP_INSTALL_DIR}
fi

is_python_already_installed



<< --MULTILINE-COMMENT--

first question: are you using CentOS or (Ubuntu) [c/u]

if REACTION_MANAGEMENT_APP_INSTALL_DIR is not set then
    ask where the installation files are

I. python3 install
Is python3 (python3.5 or higher) already installed (with development package and pip)? [Y/n]
____n. I am going to install python 3.6 with development package and pip. (The following commands will be executed: sudo yum -y install python36u python36u-devel python36u-pip)
       Please press a key to coninue!
____Y. Please specify the location of the installed python (e.g. '/usr/bin/python3.6' or 'python') 
       (afterwards jump to the next question)
May I create a virtual environment? [Y/n]  (The following command will be executed: python3.6 -m venv reaction-venv)
____Y. Please specify where the virtual environment should reside:   $VIRTUALENV_PATH
        (executing the following commands: cd $VIRTUALENV_PATH && python3.6 -m venv venv)
____n. Please specify the command to activate the already existing virtual environment:  $virtualenv_activation
May I install django and the dependencies of Reaction in to the virtual environment?
____Y. (The following commands will be executed: source $VIRTUALENV_PATH/bin/activate && pip install -r $install_dir/requirements.txt)
       (execute the commands and jump to Apache install)
____n. (jump to Apache install)


II. Apache install
Do you want to use an existing Apache? [Y/n]
____n. May I install the Apache vXXX [Y/n]    (The following commands will be executed: ??????????)
_______Y. (execute the commands and jump to the 'May I install mod_wsgi' question)
_______n. Reaction Management App needs a web server to run (the install script supports only Apache)! Exiting... (exit the install)
____Y. Is mod_wsgi installed on the existing Apache? [Y/n]
________Y. (jump to database client install)
________n. I am going to install mod_wsgi (The following commands will be executed: ??????????)
           Please press a key to coninue!
           (execute the commands and jump to database client install)


III. database client install (mysql / Oracle)
Do you use Oracle or mysql (mariadb)? [O/m]    $database_type
____O. Is Oracle client already installed? [Y/n]
________n. display the following message and wait for button press: "Please manually install Oracle Client first!"
            Download from: http://www.oracle.com/technetwork/database/features/instant-client/index.html
            the Basic Light package is enough if only English language is needed
            Installation instructions: https://oracle.github.io/odpi/doc/installation.html#linux
            If the python is 32bit then it will install a 32bit cx_oracle so the Oracle Client has to be 32bit too! Even if the machine/windows is 64 bit.
            To find out if the python is 32 or 64 bit is execute the following commands in the python shell (make sure the virtual env is activated and just type python in Linux command shell)
                import struct
                print(struct.calcsize("P") * 8)
________Y. Is the python Oracle package already installed? [Y/n]
____________n. I am going to install the python Oracle package?       (The following commands will be executed: pip install cx-Oracle)
                   "Please set the LD_LIBRARY_PATH system variable! : export LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib:$LD_LIBRARY_PATH
                   due to an error I had to create a link
                            cd /usr/lib/oracle/12.2/client64/lib/
                            ln libclntsh.so.12.1 libclntsh.so"
____m. Is the python mysql/mariadb package already installed? [Y/n]
________n. I am going to install the mysql Python driver: pip install PyMySQL                   
           I'll alter $management_app_location/management_app/management_app/settings_reaction.py file to initialize the mysql database driver!
                           import pymysql
                           pymysql.install_as_MySQLdb()
________Y. (jump to IV.)


IV. Setting up management app
Do you need help to set up the management app? [Y/n]
____Y. 
      * May I extract the management app ZIP? [Y/n]
________Y. Please specify where the management app will reside (where to extract the management app ZIP to):  $management_app_location     (The following commands will be executed: unzip $install_dir/management_app-1.1.zip -d $management_app_location)
________n. Please specify where the management app ZIP was extracted to:  $management_app_location
      * Please specify the subdomain that the Management App can be accessed (e.g. at the URL 'http://localhost/reaction-management' the subdomain is 'reaction-management'):   $reaction_subdomain
      * Please specify the value of LD_LIBRARY_PATH (e.g. /usr/lib/oracle/12.2/client64/lib):     reaction_ld_library_path    !!! only if the database_type is oracle
      * May I perform the following configuration tasks? [Y/n]
           - overwriting the file /etc/apache2/sites-available/000-default.conf
					LoadModule wsgi_module modules/mod_wsgi.so

					Alias /${REACTION_SUBDOMAIN}/static /local/reaction/management-app/static
					<Directory /local/reaction/management-app/static>
					  Allow from all
					  Require all granted
					</Directory>
					<Directory /local/reaction/management-app/management-app>
					  <Files wsgi.py>
					    Require all granted
					  </Files>
					</Directory>

					WSGIDaemonProcess management-app python-path=/local/reaction/management-app/management-app:/local/reaction/environments/venv/lib/python3.6/site-packages
					WSGIProcessGroup management-app
					WSGIScriptAlias /${REACTION_SUBDOMAIN} /local/reaction/management-app/management-app/wsgi.py process-group=management-app
					PassEnv LD_LIBRARY_PATH
					PassEnv REACTION_SUBDOMAIN
           - adding the variables to the file /etc/sysconfig/httpd:
					LD_LIBRARY_PATH=/usr/lib/oracle/12.2/client64/lib:$LD_LIBRARY_PATH
					REACTION_SUBDOMAIN=reaction-management (the application can be accessed on http://localhost/reaction-management)
           - set the DATABASES/ENGINE in settings_reaction.py file based on $database_type -> use like 
					if mysql then sed -i "s/'ENGINE': 'django.db.backends.oracle'/'ENGINE': 'django.db.backends.mysql'/g" $management_app_location/management_app/settings_reaction.py
					if oracle then don't do anything as oracle is the default database type
      * Please configure the management app by altering the settings_reaction.py config file
        The following settings should be changed:
              - DATABASES: please see the seetings in https://docs.djangoproject.com/en/1.11/ref/settings/#databases
              - LOGGING: 'filename' -> where the log file will reside
              - Mail settings
              - REACTION_ENGINE_REST_URL: endpoint URL of the reaction engine REST
              - TIME_ZONE
              - user name / password for authenticating against Reaction Engine REST: REACTION_REST_AUTH_PUBLIC_KEY, REACTION_REST_AUTH_PRIVATE_KEY
      * Is the Reaction database already created? [Y/n] 
________n. The following commands will be executed (please make sure that the database user is already created!):
              - source $virtualenv_activation && cd $management_app_location/management_app && python manage.py makemigrations admin_system admin_execution_flow admin_errordetector monitoring scheduler common worker_status && python manage.py migrate
              - executing reaction_oracle.sql or reaction_mysql.sql
      * Is the management app superuser database already created? [Y/n] 
________n. The following commands will be executed:  source $virtualenv_activation && cd $management_app_location/management_app &&  && python manage.py createsuperuser

____n. (jump to V.)



V. final message...
like change the DATABASES setting in settings-XXXX.py
--MULTILINE-COMMENT--
