%define release      1
%define version      coming_from_build_gradle_underscrore

Name:                reaction-worker
Version:             %{version}
Release:             %{release}%{?dist}
Summary:             Reaction Worker

Group:               Applications/System
License:             Restricted
URL:                 http://www.reaction-engine.org

Packager:            Viktor Horvath

Prefix:              /local/reaction/worker-%{version}
BuildArchitectures:  noarch


%description
Reaction Worker - standalone Java application that runs in the background and performs the following tasks:
- monitors application log files
- reports incidents to Reaction Engine
- executes OS commands
- sends back the output of the execution


%prep


%build
pwd


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/local/reaction/worker-%{version}
unzip %{_sourcedir}/reaction-worker-%{version}.zip -d $RPM_BUILD_ROOT/local/reaction/worker-%{version}


%clean
rm -rf $RPM_BUILD_ROOT


%post
echo ""
echo "Congratulation! The installation of the Reaction Worker was successful!"
echo ""
echo "The default install dir: /local/reaction/worker-%{version}"
echo ""
echo "******************** Reaction Worker - post installation advice ********************"
echo "In order to make the Reaction Worker work at least the following settings in the configuration file (conf/worker.yml) have to be altered:"
echo " - host_name"
echo " - rest_call.engine_endpoint"
echo "Also please change the password in credential (security/credential) file for the REST authentication (do not forget to do the same in the config file of Reaction Engine)."


%preun
$RPM_BUILD_ROOT/local/reaction/worker-%{version}/reaction_executor.sh stop
$RPM_BUILD_ROOT/local/reaction/worker-%{version}/reaction_reader.sh stop


%postun
echo ""
echo "The Reaction Worker is uninstalled successfully."


%files
%dir /local/reaction/worker-%{version}/conf
%dir /local/reaction/worker-%{version}/logs
%dir /local/reaction/worker-%{version}/lib
%dir /local/reaction/worker-%{version}/security
%dir /local/reaction/worker-%{version}
/local/reaction/worker-%{version}/conf/logback.xml
/local/reaction/worker-%{version}/conf/worker.yml
/local/reaction/worker-%{version}/lib/aopalliance-1.0.jar
/local/reaction/worker-%{version}/lib/commons-codec-1.9.jar
/local/reaction/worker-%{version}/lib/commons-daemon-1.0.15.jar
/local/reaction/worker-%{version}/lib/commons-io-2.5.jar
/local/reaction/worker-%{version}/lib/commons-logging-1.2.jar
/local/reaction/worker-%{version}/lib/httpclient-4.5.2.jar
/local/reaction/worker-%{version}/lib/httpcore-4.4.4.jar
/local/reaction/worker-%{version}/lib/jackson-annotations-2.6.0.jar
/local/reaction/worker-%{version}/lib/jackson-core-2.6.5.jar
/local/reaction/worker-%{version}/lib/jackson-databind-2.6.5.jar
/local/reaction/worker-%{version}/lib/logback-classic-1.1.7.jar
/local/reaction/worker-%{version}/lib/logback-core-1.1.7.jar
/local/reaction/worker-%{version}/lib/lombok-1.16.6.jar
/local/reaction/worker-%{version}/lib/reaction-common-coming_from_build_gradle.jar
/local/reaction/worker-%{version}/lib/reaction-worker-coming_from_build_gradle.jar
/local/reaction/worker-%{version}/lib/slf4j-api-1.7.21.jar
/local/reaction/worker-%{version}/lib/snakeyaml-1.8.jar
/local/reaction/worker-%{version}/lib/spring-aop-4.2.4.RELEASE.jar
/local/reaction/worker-%{version}/lib/spring-beans-4.2.4.RELEASE.jar
/local/reaction/worker-%{version}/lib/spring-context-4.2.4.RELEASE.jar
/local/reaction/worker-%{version}/lib/spring-core-4.2.4.RELEASE.jar
/local/reaction/worker-%{version}/lib/spring-expression-4.2.4.RELEASE.jar
/local/reaction/worker-%{version}/lib/spring-web-4.2.4.RELEASE.jar
/local/reaction/worker-%{version}/lib/javax.annotation-api-1.3.2.jar
/local/reaction/worker-%{version}/reaction_executor.bat
/local/reaction/worker-%{version}/reaction_executor.sh
/local/reaction/worker-%{version}/reaction_reader.bat
/local/reaction/worker-%{version}/reaction_reader.sh
/local/reaction/worker-%{version}/prunsrv.exe
/local/reaction/worker-%{version}/security/credential


%changelog

