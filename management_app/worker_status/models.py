from admin_event_source.models import ParentModel
from django.db import models


# ################################## WorkerStatus ##################################
class WorkerStatus(ParentModel):
    host = models.CharField(max_length=200, blank=False, null=False, unique=True)
    refresh_event_sources = models.DateTimeField(blank=True, null=True)
    report_event = models.DateTimeField(blank=True, null=True)
    check_commands = models.DateTimeField(blank=True, null = True)
    send_command_result = models.DateTimeField(blank=True, null = True)

    class Meta:
        db_table = 'WORKER_STATUS'
        default_permissions = ()
