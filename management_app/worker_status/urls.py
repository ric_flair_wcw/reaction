from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^worker_status$',                              views.index,                       name='worker_status'),
	url(r'^worker_status/(?P<pk>[0-9]+)/delete$',        views.delete_worker_status,        name='worker_status_delete'),
]