import logging
from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required
from .models import WorkerStatus
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.db.utils import IntegrityError

logger = logging.getLogger('root')


@login_required
@permission_required('auth.can_use_worker_status', raise_exception=True)
def index(request):
    logger.info('Getting the status of the workers. {}'.format(request))

    result = []
    worker_status = WorkerStatus.objects.order_by('host')
    for ws in worker_status:
        rec = {'refresh_event_sources': [ws.refresh_event_sources, get_date_diff_desc(ws.refresh_event_sources)]}
        rec['is_reader_old'] = False if not ws.refresh_event_sources else ws.refresh_event_sources + timedelta(hours=9) < datetime.now()
        rec['report_event'] = [ws.report_event, get_date_diff_desc(ws.report_event)]
        rec['check_commands'] = [ws.check_commands, get_date_diff_desc(ws.check_commands)]
        rec['is_executor_old'] = False if not ws.check_commands else ws.check_commands + timedelta(hours=2) < datetime.now()
        rec['send_command_result'] = [ws.send_command_result, get_date_diff_desc(ws.send_command_result)]
        result.append([ws.host, rec, ws.pk])
    context = {'result': result}
    return render(request, 'worker_status/index.html', context)

def get_date_diff_desc(timestamp):
    current = datetime.now()
    diff = relativedelta(current, timestamp)
    diff_text = ''
    if diff.minutes != 0:
        diff_text = str(diff.minutes) + " minutes ago"
    if diff.hours != 0:
        diff_text = str(diff.hours) + " hour(s) " + diff_text
    if diff.days != 0:
        diff_text = str(diff.days) + " day(s) " + diff_text
    if diff.months != 0:
        diff_text = str(diff.months) + " month(s) " + diff_text
    if diff.years != 0:
        diff_text = str(diff.years) + " year(s) " + diff_text
    return diff_text


@login_required
@permission_required('auth.can_use_worker_status', raise_exception=True)
def delete_worker_status(request, pk):
    logger.info('delete_worker_status is called - request={}; pk={}'.format(request, pk))
    try:
        WorkerStatus.objects.filter(id=pk).delete()
        return HttpResponseRedirect(reverse_lazy('worker_status'))
    except IntegrityError:
        message = 'The record from Worker Status statistics cannot be deleted as it is currently being used in somewhere else!'
        logger.exception(message)
        return render(request, 'business_error.html', {'message': message})
