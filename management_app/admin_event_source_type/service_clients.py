from common.rest_utils import call_rest


def call_check_identifiers(message, identifiers, dataType, namespaces):
    url = '/utils/identifiers/check'
    payload = {'message': message,
               'identifiers': identifiers,
               'dataType': dataType,
               'namespaces': namespaces}
    return call_rest(payload, url, 'POST')
