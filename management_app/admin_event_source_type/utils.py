import json


def search_for_invalid_resources(_path_to_identifiers, r_list, r_type):
    invalid_resources = []
    path_to_identifiers = json.loads(_path_to_identifiers)
    for r in r_list:
        if not is_resource_valid(r, path_to_identifiers, r_type):
            invalid_resources.append((r.id, r.name))
    return invalid_resources


def is_resource_valid(r, _path_to_identifiers, r_type):
    path_to_identifiers = [x for x in _path_to_identifiers if x['output'] == r_type]
    if path_to_identifiers:
        if not r.identifiers and not path_to_identifiers:
            return True
        if not r.identifiers and path_to_identifiers or r.identifiers and not path_to_identifiers:
            return False
        identifiers = json.loads(r.identifiers)
        if len(identifiers) != len(path_to_identifiers):
            return False
        for pti in path_to_identifiers:
            if not pti['name']+'-'+pti['source'] in identifiers:
                return False
    else:
        if r.identifiers and len(r.identifiers) > 0:
            return False
    return True
