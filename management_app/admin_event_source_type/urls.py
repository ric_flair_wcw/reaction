from django.conf.urls import url

from . import views
urlpatterns = [
    url(r'^administration/event_source_type$',                                         views.EventSourceTypeList.as_view(),                  name='admin_event_source_type_list'),

    url(r'^administration/event_source_type/(?P<pk>[0-9]+)/update$',                   views.EventSourceTypeUpdate.as_view(),                name='admin_event_source_type_update'),

    url(r'^administration/event_source_type/create$',                                  views.EventSourceTypeCreate.as_view(),                name='admin_event_source_type_create'),

    url(r'^administration/event_source_type/(?P<pk>[0-9]+)/delete$',                   views.delete_event_source_type,                       name='admin_event_source_type_delete'),

    # for REST service
    url(r'^administration/event_source_type/check$',                                   views.send_identifiers_to_be_tested,                  name='send_identifiers_to_be_tested'),

]
