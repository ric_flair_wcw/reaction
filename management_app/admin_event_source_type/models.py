from django.db import models
from admin_event_source.models import ParentModel

# ################################## EventSourceType ##################################

class EventSourceType(ParentModel):
    DATA_TYPES = (
        ('XML', 'XML'),
        ('JSON', 'JSON'),
        ('NONE', 'NONE')
    )
    AUTHENTICATION_TYPE = (
        ('NONE', 'None'),
        ('SECRET', 'Secret'),
        ('BASIC_AUTH', 'Basic Auth')
    )
    IDENTIFIER_SOURCE = (
        ('PAYLOAD', 'Payload'),
        ('QUERY_PARAMETER', 'Query parameter')
    )
    IDENTIFIER_OUTPUT = (
        ('EVENT_SOURCE', 'Event source'),
        ('ERROR_DETECTOR', 'Error detector')
    )

    name = models.CharField(max_length=200, blank=False, unique=True)
    code = models.CharField(max_length=40, blank=False, unique=True)
    data_type = models.CharField(choices=DATA_TYPES, max_length=4, blank=True, null=True)
    namespaces = models.CharField(max_length=2000, blank=True, null=True)
    path_to_identifiers = models.CharField(max_length=3000, blank=True, null=True)
    enforce_ssl = models.BooleanField(default=False)
    authentication_type = models.CharField(choices=AUTHENTICATION_TYPE, max_length=10, blank=False, default='NONE')
    secret = models.CharField(max_length=100, blank=True, null=True)
    user = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'EVENT_SOURCE_TYPE'
        default_permissions = ()

    def __str__(self):
        return "Name: {}\nCode: {}\nData type: {}".format(self.name,
                                                          self.code,
                                                          self.data_type)
