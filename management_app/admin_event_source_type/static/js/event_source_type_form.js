var numRows = 1;
var user;


function openSetSecretDialog() {
    $("#d_set_secret").dialog("open");
}


function openSetCredentialsDialog() {
    $("#i_auth_user").val(user);
    $("#d_set_credentials").dialog("open");
}


function addRowToIdentifiers() {
    var table = document.getElementById("table_identifiers");
    var row = table.insertRow(numRows);
    row.className = 'class="{% cycle \'data_table_event_odd\' \'data_table_even_line\' %}"';
    row.id = "table_identifiers_row_" + numRows;
    createInput(row, 0, 'identifiers_name_'+numRows, true);
    createInput(row, 1, 'identifiers_desc_'+numRows, false);
    createDropDown(row, 2, 'identifiers_source_'+numRows, '{"PAYLOAD": "Payload", "QUERY_PARAMETER": "Query parameter"}');
    createDropDown(row, 3, 'identifiers_output_'+numRows, '{"EVENT_SOURCE": "Event source", "ERROR_DETECTOR": "Error detector"}');
    createInput(row, 4, 'identifiers_path_'+numRows, true);
    let b = document.createElement('button');
    b.type = 'button';
    b.setAttribute('class', 'btn btn-secondary');
    b.setAttribute('onclick', 'deleteRowFromIdentifiers(table_identifiers_row_' + numRows+')');
    b.innerText = 'Delete';
    row.insertCell(5).appendChild(b);
    numRows++;
}

function createInput(row, rowNr, idname, required) {
    let i = document.createElement('input');
    i.type = 'text';
    i.id = idname;
    i.name = idname;
    i.value = '';
    i.setAttribute('class', 'form-control')
    i.required = required;
    row.insertCell(rowNr).appendChild(i);
}

function createDropDown(row, rowNr, id, _values) {
    let values = JSON.parse(_values);
    let s = document.createElement('select');
    s.id = id;
    s.name = id;
    s.setAttribute('class', 'form-control-static');
    for (let v in values) {
        let o = document.createElement("option");
        o.value = v;
        o.text = values[v];
        s.appendChild(o);
    }
    row.insertCell(rowNr).appendChild(s);
}


function openTestIdentifiersDialog() {
    $("#div_test_identifiers").empty();
    let form = document.forms.form_identifiers;
    let formData = new FormData(form);
    let tdHtml = '';
    for(let i=1; i <= numRows; i++) {
        if (formData.has('identifiers_name_'+i)) {
            let identifierName = formData.get('identifiers_name_'+i);
            let identifierPath = formData.get('identifiers_path_'+i);
            tdHtml += '<tr><td>'+identifierName+'</td><td>'+identifierPath+'</td><td><label id="l_test_extr_val_'+i+'" color="green"></label></td></tr>';
        }
    }
    $("#tbody_test_identfiers").html(tdHtml);
    $("#d_test_identifiers").dialog("open");
}


function deleteRowFromIdentifiers(rowId) {
    $(rowId).remove();
    numRows--;
}


function loadIdentifierTable(json) {
    if (json) {
        for (identifier of json) {
            addRowToIdentifiers();
            setIdentifierField("name");
            setIdentifierField("desc");
            setIdentifierField("source");
            setIdentifierField("output");
            setIdentifierField("path");
        }
    }
}


function setIdentifierField(field) {
    if (identifier[field] != null) {
        $("#identifiers_"+field+"_" + (numRows-1)).val(identifier[field]);
    }
}


function buildPathToIdentifiers() {
    let form = document.forms.form_identifiers;
    let formData = new FormData(form);
    let ids = [];
    let names = new Set();
    for(let i=1; i <= numRows; i++) {
        if (formData.has('identifiers_name_'+i)) {
            let identifierName = formData.get('identifiers_name_'+i);
            if (names.has(identifierName)) {
                $("#l_identifiers_errors").text("The name must be unique!");
                return false;
            } else {
                ids.push({
                    name: identifierName,
                    desc: formData.get('identifiers_desc_'+i),
                    path: formData.get('identifiers_path_'+i),
                    source: formData.get('identifiers_source_'+i),
                    output: formData.get('identifiers_output_'+i)
                });
                names.add(identifierName);
            }
        }
    }
    $('<input>').attr('type', 'hidden').attr('name', 'path_to_identifiers').attr('value', JSON.stringify(ids)).appendTo('#form_identifiers');

    $("#id_enforce_ssl").prop( "disabled", false );

    return true;
}


function whenAuthTypeRadioButtonIsChanged() {
    authType = $('input[name=authentication_type]:checked', '#form_identifiers').val();
    // set enforceSsl on if auth type is not NONE
    if (authType == 'NONE') {
        $("#id_enforce_ssl").prop( "disabled", false );
    } else {
        $("#id_enforce_ssl").prop( "disabled", true );
        $("#id_enforce_ssl").prop( "checked", true );
    }
    // change the button to be shown
    if (authType == 'NONE') {
        $("#b_set_basic_auth").remove();
        $("#b_set_secret").remove();
    } else if (authType == 'SECRET') {
        $("#b_set_basic_auth").remove();
        $("#id_authentication_type_1").parent().append('<button id="b_set_secret" type="button" class="btn btn-sm" onclick="openSetSecretDialog()" style="margin-left:40px">Set secret</button>');
    } else if (authType == 'BASIC_AUTH') {
        $("#b_set_secret").remove();
        $("#id_authentication_type_2").parent().append('<button id="b_set_basic_auth" type="button" class="btn btn-sm" onclick="openSetCredentialsDialog()" style="margin-left:40px">Set credentials</button>');
    }
}


function setAuthDialogOkButtonDisablingRule() {
    $('#i_auth_secret').keyup(function() {
        if ($(this).val().length == 0) {
            $('#b_set_secret_ok').attr('disabled', 'disabled');
        } else {
            $('#b_set_secret_ok').removeAttr('disabled');
        }
    });

    $('#i_auth_user').keyup(function() {
        if ($(this).val().length == 0 || $("#i_auth_password").val().length == 0) {
            $('#b_set_credentials_ok').attr('disabled', 'disabled');
        } else {
            $('#b_set_credentials_ok').removeAttr('disabled');
        }
    });
    $('#i_auth_password').keyup(function() {
        if ($(this).val().length == 0 || $("#i_auth_user").val().length == 0) {
            $('#b_set_credentials_ok').attr('disabled', 'disabled');
        } else {
            $('#b_set_credentials_ok').removeAttr('disabled');
        }
    });
}


function setTestIdentifiersDialogOkButtonDisablingRule() {
    $('#ta_test_message').keyup(function() {
        if ($(this).val().length == 0) {
            $('#b_test_ok').attr('disabled', 'disabled');
        } else {
            $('#b_test_ok').removeAttr('disabled');
        }
    });
}