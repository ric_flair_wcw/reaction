from django import forms
from django.db.models import Q
from .models import EventSourceType
import base64, re, hashlib, hmac, json
from admin_event_source.models import EventSource
from admin_errordetector.models import ErrorDetector
from .utils import is_resource_valid, search_for_invalid_resources
from django.core import serializers


class EventSourceTypeForm(forms.ModelForm):
    error_css_class = 'model_form_error'
    name = forms.CharField(max_length=120)
    code = forms.CharField(max_length=120)
    namespaces = forms.CharField(widget=forms.Textarea(attrs={"rows": 3}), required=False)
    authentication_type = forms.ChoiceField(choices=EventSourceType.AUTHENTICATION_TYPE, widget=forms.RadioSelect, initial='NONE')
    save_confirmation_json = forms.CharField(widget=forms.HiddenInput(), required=False)
    confirmed_to_save = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = EventSourceType
        fields = ['name', 'code', 'data_type', 'namespaces', 'path_to_identifiers', 'enforce_ssl', 'authentication_type', 'user', 'secret', 'save_confirmation_json',
                  'confirmed_to_save']

    def __init__(self, *args, **kwargs):
        super(EventSourceTypeForm, self).__init__(*args, **kwargs)
        self.data = self.data.copy()  # IMPORTANT, self.data is immutable
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['code'].widget.attrs['class'] = 'form-control'
        self.fields['data_type'].widget.attrs['class'] = 'form-control'
        self.fields['namespaces'].widget.attrs['class'] = 'form-control'
        self.fields['namespaces'].widget.attrs['style'] = 'resize: vertical'

    def clean(self):
        cleaned_data = super(EventSourceTypeForm, self).clean()
        field = 'authentication_type'
        if cleaned_data.get(field) == 'SECRET' and not cleaned_data['secret'] and not self.initial['secret']:
            self.add_error(field, 'The authentication type is SECRET but no secret value is set!')
        elif cleaned_data.get(field) == 'BASIC_AUTH':
            if not cleaned_data['secret'] and not self.initial['secret']:
                self.add_error(field, 'The authentication type is BASIC_AUTH but the secret value is not set!')
            if not cleaned_data['user'] and not self.initial['user']:
                self.add_error(field, 'The authentication type is BASIC_AUTH but the user value is not set!')
        # the user can contain only [a-zA-Z0-9\_]
        if cleaned_data['user'] and not re.compile('^[a-zA-Z0-9_]+$').match(cleaned_data['user']):
            self.add_error(field, 'The user name can contain only a-z, A-Z, 0-9 and _ characters!')
        # when not saving a new user/secret then these 2 will be None but I want to keep the previously stored values
        if cleaned_data['secret']:
            digester = hmac.new('ricflairwcw'.encode('utf-8'), digestmod=hashlib.sha512)
            digester.update(cleaned_data['secret'].encode('utf-8'))
            cleaned_data['secret'] = str(base64.b64encode(digester.digest()), 'UTF-8')
        if cleaned_data.get(field) == 'BASIC_AUTH' and (not cleaned_data['user'] or not cleaned_data['secret']):
            cleaned_data['user'] = self.initial['user']
            cleaned_data['secret'] = self.initial['secret']
        if cleaned_data.get(field) == 'SECRET':
            cleaned_data['user'] = None
            if not cleaned_data['secret']:
                cleaned_data['secret'] = self.initial['secret']
        if cleaned_data.get(field) == 'NONE':
            cleaned_data['user'] = None
            cleaned_data['secret'] = None
        invalid_event_sources = search_for_invalid_resources(cleaned_data['path_to_identifiers'],
                                                             EventSource.objects.filter(Q(event_source_type=self.instance) & Q(status='VALID')),
                                                             'EVENT_SOURCE')
        invalid_error_detectors = search_for_invalid_resources(cleaned_data['path_to_identifiers'],
                                                               ErrorDetector.objects.filter(Q(event_source__event_source_type=self.instance) & Q(activated=True)),
                                                               'ERROR_DETECTOR')
        invalid_error_detectors_due_to_invalid_event_sources = [(s.pk, s.name) for s in ErrorDetector.objects.filter(Q(event_source__pk__in=[s[0] for s in invalid_event_sources]) &
                                                                                                                     Q(activated=True))]
        if invalid_event_sources or invalid_error_detectors:
            invalid_error_detectors.extend(invalid_error_detectors_due_to_invalid_event_sources)
            if cleaned_data['confirmed_to_save'] == 'false':
                self.add_error('save_confirmation_json', json.dumps({'invalid_error_detectors': self.remove_dupl(invalid_error_detectors),
                                                                     'invalid_event_sources': invalid_event_sources}))
            else:
                ErrorDetector.objects.filter(pk__in=[s[0] for s in invalid_error_detectors]).update(activated=False)
                EventSource.objects.filter(pk__in=[s[0] for s in invalid_event_sources]).update(status='INVALID')
        # the name 'secret' cannot be used as query parameter name
        path_to_identifiers = json.loads(cleaned_data['path_to_identifiers'])
        for pti in path_to_identifiers:
            if pti['source'] == 'QUERY_PARAMETER' and pti['name'] == 'secret':
                self.add_error('path_to_identifiers', 'The name "secret" cannot be used as query parameter name!')

    def remove_dupl(self, l):
        new_l = []
        added_pks = set()
        for e in l:
            if e[0] not in added_pks:
                new_l.append(e)
                added_pks.add(e[0])
        return new_l
