from .models import EventSourceType
from .forms import EventSourceTypeForm
from admin_event_source.models import EventSource
from admin_errordetector.models import ErrorDetector
from common.view_utils import FilteredListView
import logging
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import ListView, UpdateView, CreateView
from django.contrib.auth.decorators import login_required, permission_required
from django.urls import reverse_lazy
from django.forms.models import model_to_dict
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.db.utils import IntegrityError
from django.shortcuts import render
from rest_framework.decorators import api_view
from common.rest_utils import login_required_for_rest
from admin_event_source_type.service_clients import *
from rest_framework.response import Response
from .utils import *


logger = logging.getLogger('root')


class EventSourceTypeList(LoginRequiredMixin, PermissionRequiredMixin, FilteredListView):
    # Get an instance of a logger
    paginate_by = 10
    template_name = 'admin_event_source_type/list.html'
    permission_required = 'auth.can_use_admin_event_source_type'

    def get_queryset(self):
        filtr = self.build_filter()
        order = ['name' if 'order' not in self.request.GET else self.request.GET.get('order'), 'id']
        logger.info('get_queryset - {}|  filter :{},    order: {}'.format(__name__, filtr, order))
        event_source_types = EventSourceType.objects.filter(~Q(code='REACTION-WORKER'), **filtr).order_by(*order)
        return list(map(populate, event_source_types))


def populate(est_model):
    est_dict = model_to_dict(est_model)
    est_dict['invalid_event_sources'] = search_for_invalid_resources(est_dict['path_to_identifiers'],
                                                                     EventSource.objects.filter(Q(event_source_type=est_model)),
                                                                     'EVENT_SOURCE')
    est_dict['invalid_error_detectors'] = search_for_invalid_resources(est_dict['path_to_identifiers'],
                                                                       ErrorDetector.objects.filter(event_source__event_source_type=est_model),
                                                                       'ERROR_DETECTOR')
    return est_dict


class EventSourceTypeUpdateCreateView(LoginRequiredMixin, PermissionRequiredMixin):
    form_class = EventSourceTypeForm
    model = EventSourceType
    template_name = 'admin_event_source_type/form.html'
    permission_required = 'auth.can_use_admin_event_source_type'

    def get_success_url(self):
        return reverse_lazy('admin_event_source_type_list')


class EventSourceTypeUpdate(EventSourceTypeUpdateCreateView, UpdateView):
    pass


class EventSourceTypeCreate(EventSourceTypeUpdateCreateView, CreateView):
    pass


@login_required
@permission_required('auth.can_use_admin_event_source_type', raise_exception=True)
def delete_event_source_type(request, pk):
    logger.info('delete_event_source_type is called - request={}; pk={}'.format(request, pk))
    try:
        EventSourceType.objects.filter(id=pk).delete()
        return HttpResponseRedirect(reverse_lazy('admin_event_source_type_list'))
    except IntegrityError:
        message = 'The event source type cannot be deleted as it is currently being used in an event source or error detector!'
        logger.exception(message)
        return render(request, 'business_error.html', {'message': message})



# check the expression
@api_view(['POST'])
@login_required_for_rest
@permission_required('auth.can_use_admin_event_source_type', raise_exception=True)
def send_identifiers_to_be_tested(request):
    logger.info('send_identifiers_to_be_tested / GET is called - request={}'.format(request.data))
    # calling the REST service
    rest_response = call_check_identifiers(request.data.get('message'),
                                           request.POST.getlist('identifiers[]'),
                                           request.data.get('dataType'),
                                           request.data.get('namespaces'))
    logger.debug('rest_response={}'.format(rest_response))
    return Response(status=rest_response.error_code, data=rest_response.message)
