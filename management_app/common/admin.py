from django import forms
from django.contrib.auth.models import Permission
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.contrib import admin

from .models import Profile


# #########################################################################
#
# #########################################################################
from django.contrib.auth.models import User


# #####################################################################
#   the following is for hiding the default permissions in web admin &
#                 displaying the access groups field
# #####################################################################
MODELS_TO_HIDE_STD_PERMISSIONS = (
    ("permission"),
    ("group"),
    ("user"),
    ("logentry"),
    ("contenttype"),
    ("session"),
    ("common"),
    ("person"),
    ("workerstatus"),
    ("token")
)


def _get_corrected_permissions():
    perms = Permission.objects.all()
    for model_name in MODELS_TO_HIDE_STD_PERMISSIONS:
        perms = perms.exclude(codename='add_%s' % model_name)
        perms = perms.exclude(codename='change_%s' % model_name)
        perms = perms.exclude(codename='delete_%s' % model_name)
    return perms


class MyGroupAdminForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = '__all__'
    permissions = forms.ModelMultipleChoiceField(
        _get_corrected_permissions(),
        widget=admin.widgets.FilteredSelectMultiple(('permissions'), False),
        help_text = 'Hold down "Control", or "Command" on a Mac, to select more than one.'
    )


class MyGroupAdmin(GroupAdmin):
    form = MyGroupAdminForm


class MyUserChangeForm(UserChangeForm):
    user_permissions = forms.ModelMultipleChoiceField(
        _get_corrected_permissions(),
        widget=admin.widgets.FilteredSelectMultiple(('user_permissions'), False),
        help_text = 'Hold down "Control", or "Command" on a Mac, to select more than one.'
    )


# Define an inline admin descriptor for Profile model (it is for managing the access groups in the Profile model)
# which acts a bit like a singleton
class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'profile'
    exclude = ['dashboard_p1_dateperiod', 'dashboard_p2_dateperiod', 'dashboard_p3_dateperiod', 'dashboard_p4_dateperiod', 'dashboard_p2_flow0', 'dashboard_p2_flow1', 'dashboard_p2_flow2',
               'dashboard_p2_flow3', 'dashboard_p2_event_source0', 'dashboard_p2_event_source1', 'dashboard_p2_event_source2', 'dashboard_p2_event_source3', 'dashboard_autorefresh']


class MyUserAdmin(UserAdmin):
    form = MyUserChangeForm
    inlines = (ProfileInline, )


# to suppress the default permissions (session - add/change/delete, permission - ... -> see in MODELS_TO_HIDE_STD_PERMISSIONS) in the admin GUI
# and to display the access groups field
admin.site.unregister(Group)
admin.site.register(Group, MyGroupAdmin)
admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)
# in order to be able to manage permissions (I should disable this in PROD -> write my permission in fixture and run 'python manage.py loaddata <fixturename>' )
admin.site.register(Permission)