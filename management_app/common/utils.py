from django.db.models import Q
from functools import reduce
from operator import or_
import base64
import os
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


# #####################################################################
#                   defining util functions
# #####################################################################
def split_and_strip(s):
    if s is not None:
        return [x.strip() for x in s.split(',')]
    else:
        return s


def build_filter_access_groups(access_groups, for_event, prefix):
    if access_groups is None:
        return []
    argument_list = []
    for access_group in split_and_strip(access_groups):
        argument_list.append(Q(**{prefix+'access_group__startswith': access_group + ','}))
        argument_list.append(Q(**{prefix+'access_group__contains': ',' + access_group + ','}))
        argument_list.append(Q(**{prefix+'access_group__endswith': ',' + access_group}))
        argument_list.append(Q(**{prefix+'access_group__exact': access_group}))
        argument_list.append(Q(**{prefix+'access_group__isnull': True}))
    return reduce(or_, argument_list)


# #####################################################################
#                      encryption
# #####################################################################
def pad(byte_array):
    BLOCK_SIZE = 16
    pad_len = BLOCK_SIZE - len(byte_array) % BLOCK_SIZE
    return byte_array + (bytes([pad_len]) * pad_len)


def encrypt(key, message):
    byte_array = message.encode("UTF-8")
    padded = pad(byte_array)

    key32 = "".join([' ' if i >= len(key) else key[i] for i in range(32)])

    # generate a random iv and prepend that to the encrypted result.
    # The recipient then needs to unpack the iv and use it.
    iv = os.urandom(16)

    cipher = Cipher(algorithms.AES(bytes(key32,"UTF-8")), modes.CBC(iv))
    encryptor = cipher.encryptor()
    encrypted = encryptor.update(padded) + encryptor.finalize()

    # Note we PREPEND the unencrypted iv to the encrypted message
    return base64.b64encode(iv+encrypted).decode("UTF-8")