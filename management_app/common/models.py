from django.db import models
from django.contrib.auth.models import User
from admin_event_source.models import EventSource
from admin_execution_flow.models import ExecutionFlow

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    access_groups = models.CharField(max_length=2000, blank=True, null=True)
    dashboard_autorefresh = models.CharField(max_length=50, blank=True, null=True)
    dashboard_p1_dateperiod = models.CharField(max_length=50, blank=True, null=True)
    dashboard_p2_dateperiod = models.CharField(max_length=50, blank=True, null=True)
    dashboard_p3_dateperiod = models.CharField(max_length=50, blank=True, null=True)
    dashboard_p4_dateperiod = models.CharField(max_length=50, blank=True, null=True)
    dashboard_p2_event_source0 = models.ForeignKey(EventSource, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_event_source0')
    dashboard_p2_event_source1 = models.ForeignKey(EventSource, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_event_source1')
    dashboard_p2_event_source2 = models.ForeignKey(EventSource, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_event_source2')
    dashboard_p2_event_source3 = models.ForeignKey(EventSource, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_event_source3')
    dashboard_p2_flow0 = models.ForeignKey(ExecutionFlow, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_flow0')
    dashboard_p2_flow1 = models.ForeignKey(ExecutionFlow, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_flow1')
    dashboard_p2_flow2 = models.ForeignKey(ExecutionFlow, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_flow2')
    dashboard_p2_flow3 = models.ForeignKey(ExecutionFlow, blank=True, null=True, on_delete=models.DO_NOTHING, related_name='Profile_dashboard_p2_flow3')

    class Meta:
        db_table = 'PROFILE'
        default_permissions = ()