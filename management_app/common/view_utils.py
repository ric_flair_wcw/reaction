import logging
import sys
import traceback
from django.http import HttpResponseServerError
from django.shortcuts import render
from django.template import RequestContext, Context
from django.template import loader
from django.views.generic import ListView

logger = logging.getLogger('root')


class FilteredListView(ListView):
    
    # building the filter for the query in get_queryset
    def build_filter(self):
        filter = {}
        # adding the filters coming from the GUI
        for param in self.request.GET:
            if param.startswith('filter_'):
                value = self.request.GET.get(param)
                if value:
                    key = param[7:]
                    filter[key + '__contains'] = value
        return filter


    def get_context_data(self, **kwargs):
        context = super(FilteredListView, self).get_context_data(**kwargs)
        # adding the filters and order back to the context (in order to be visible for the next request -> they shouldn't disappear if I typed them and click on the Filter! button)
        for param in self.request.GET:
            if (param.startswith('filter_')):
                context[param] = self.request.GET.get(param, '')
        if 'order' in self.request.GET:
            context['order'] = self.request.GET.get('order')
        return context


def custom_500(request, *args, **argv):
    type, exception_message, tb = sys.exc_info()
    logger.exception("Unexpected error occurred!")
    response = render(request, '500.html', {'exception_message': exception_message})
    response.status_code = 500
    return response


def custom_404(request, *args, **argv):
    logger.error("404 - Not Found: {}".format(request.path))
    response = render(request, '404.html', {'path': request.path})
    response.status_code = 404
    return response


def custom_403(request, *args, **argv):
    logger.error("403 - Forbidden: {}".format(request.path))
    response = render(request, '403.html', {'path': request.path,
                                            'user': request.user.first_name+' '+request.user.last_name if request.user.first_name and request.user.last_name else
                                                    request.user.first_name if request.user.first_name else
                                                    request.user.username})
    response.status_code = 403
    return response
