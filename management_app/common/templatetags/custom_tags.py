from django import template
import urllib
from django.utils.safestring import mark_safe
from django.conf import settings

register = template.Library()

# it is for keeping the
@register.simple_tag(takes_context=True)
def url_replace(context, **kwargs):
    # storing a copy of the request parameters
    query = context['request'].GET.copy()
    # the the 'order' is in the kwargs (table header was clicked on) then check the URL parameters (that basically belong to the previous clicking) anf decide if the ASC or DESC
    field = None
    if 'order' in kwargs:
        field = kwargs['order']
        if 'order' in query.keys():
            if query['order'].startswith('-') and query['order'].lstrip('-') == field:
                None
            elif query['order'].lstrip('-') == field:
                field = "-" + field
            else:
                None
    # update all the incoming parameter with the GET parameters (so keep the URL GET parameters!)
    query.update(kwargs)
    # update the order if it has value
    if field != None:
        query['order'] = field
    return urllib.parse.urlencode(query, True)


@register.filter()
def nbsp(value):
    return mark_safe("&nbsp;".join(value.split(' ')))


@register.simple_tag(takes_context=True)
def table_header_sort_icon(context, header):
    if context.request.GET.get('order') == header:
        return '<span class="glyphicon glyphicon-triangle-bottom"></span>'
    elif context.request.GET.get('order') == '-'+header:
        return '<span class="glyphicon glyphicon-triangle-top"></span>'
    else:
        return ''


@register.simple_tag()
def dashboard_dateperiod_combobox(id, on_change_func):
    return '<label style="padding-right: 5px;">Incidents started:</label>' + \
           '<select id="' + id + '" class="form-control-static" onchange="' + on_change_func + '">' + \
           '<option value="STARTED_LAST_10MINS">In the last 10 mins</option>' + \
           '<option value="STARTED_LAST_30MINS">In the last 30 mins</option>' + \
           '<option value="STARTED_LAST_1HOUR">In the last 1 hr</option>' + \
           '<option value="STARTED_LAST_4HOURS">In the last 4 hrs</option>' + \
           '<option value="STARTED_TODAY">Today</option>' + \
           '<option value="STARTED_YESTERDAY">Yesterday</option>' + \
           '<option value="STARTED_THIS_WEEK">This week</option>' + \
           '<option value="STARTED_LAST_WEEK">Last week</option>' + \
           '<option value="STARTED_THIS_MONTH">This month</option>' + \
           '<option value="STARTED_LAST_MONTH">Last month</option>' + \
           '<option value="STARTED_THIS_YEAR">This year</option>' + \
           '</select>'


@register.simple_tag()
def get_loop_value(value_list, separator, ind):
    return value_list.split(separator)[ind]


@register.simple_tag()
def event_source_set_in_parent(map):
    if map:
        return '<p>It is already set in the parent event source <b>' + map['parent'] + '</b>! The value to be set is <b>' + map['value'] + '</b>.</p>'
    else:
        return ''


# See: https://push.cx/2007/django-template-tag-for-dictionary-access
@register.filter
def hash(h, key):
    return h[key]


# displaying the event_source and flow squares on panel 2 & 3 of dashboard
@register.inclusion_tag('tags/dashboard_square.html')
def dashboard_square(dashboard_square_id, background_color, resource_name):
    return {'dashboard_square_id': dashboard_square_id,
            'background_color': background_color,
            'resource_name': resource_name}


@register.simple_tag()
def monitoring_details_tooltip(task):
    if not task:
        return ''
    tooltip = 'Name: ' + task.name + '\n' + 'Type: ' + task.internal_task + '\n' + 'Order: ' + str(task.ordr)
    if task.internal_task == 'LOOP':
        tooltip += g('Value list 1:', task.loop_value_list_1) + g('Separator 1:', task.loop_separator_1) + g('Placeholder variable 1:', task.loop_placeholder_var_1) + g('Requires encryption 1:', task.loop_requires_encryption_1)
        tooltip += g('Value list 2:', task.loop_value_list_2) + g('Separator 2:', task.loop_separator_2) + g('Placeholder variable 2:', task.loop_placeholder_var_2) + g('Requires encryption 2:', task.loop_requires_encryption_2)
        tooltip += g('Value list 3:', task.loop_value_list_3) + g('Separator 3:', task.loop_separator_3) + g('Placeholder variable 3:', task.loop_placeholder_var_3) + g('Requires encryption 3:', task.loop_requires_encryption_3)
        tooltip += g('Value list 4:', task.loop_value_list_4) + g('Separator 4:', task.loop_separator_4) + g('Placeholder variable 4:', task.loop_placeholder_var_4) + g('Requires encryption 4:', task.loop_requires_encryption_4)
    elif task.internal_task == 'IF_ELSE':
        tooltip += g('IF expression:', task.if_expression)
    elif task.internal_task == 'COMMAND_BY_WORKER':
        tooltip += g('Command:', task.command)
        tooltip += g('OS user:', task.os_user)
        tooltip += g('Host:', task.host)
        tooltip += g('Output pattern:', task.output_pattern)
        tooltip += g('Execution timeout:', task.execution_timeout)
    elif task.internal_task == 'COMMAND_USING_SSH':
        tooltip += g('Command:', task.command)
        tooltip += g('OS user:', task.os_user)
        tooltip += g('Host:', task.host)
        tooltip += g('Output pattern:', task.output_pattern)
        tooltip += g('Execution timeout:', task.execution_timeout)
    elif task.internal_task == 'EMAIL_SENDING':
        tooltip += g('Recipients:', task.mail_recipients)
        tooltip += g('Subject:', task.mail_subject)
        tooltip += g('Content:', task.mail_content)
    return tooltip


def g(label, value):
    if value:
        return '\n' + label + ' ' + (str(value) if len(str(value)) < 100 else str(value)[:99])
    else:
        return ''


@register.simple_tag(takes_context=True)
def monitoring_details_filter_selected(context, id, value):
    return 'selected' if context.request.GET.get('loop_task_'+str(id)) == str(value) else ''


# using settings value in template
@register.simple_tag
def read_settings_value(setting_value):
    return getattr(settings, setting_value, "")
