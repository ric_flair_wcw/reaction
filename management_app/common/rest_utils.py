from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework import status
import requests
from django.conf import settings
import logging
import json
import uuid
import hashlib
import hmac
import base64
from django.utils.http import http_date
from requests import Request
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from functools import wraps

logger = logging.getLogger('root')

def custom_exception_handler(exception, context):
	logger.exception(exception)
	# Call REST framework's default exception handler first,
	# to get the standard error response.
	response = exception_handler(exception, context)
	
	if response is not None:
		response.data['errorMessage'] = str(exception)
	else:
		response = Response({'errorMessage': str(exception)}, status=status.HTTP_400_BAD_REQUEST)
	return response


def exists(s):
	try:
		s.REACTION_ENGINE_REST_SSL_VERIFY
		return True
	except AttributeError:
		return False


def get_ssl_settings():
	ssl_settings = {}
	if exists(settings) and not settings.REACTION_ENGINE_REST_SSL_VERIFY:
		ssl_settings['verify'] = False
	elif settings.REACTION_ENGINE_REST_SSL_SERVER_CERT:
		ssl_settings['verify'] = settings.REACTION_ENGINE_REST_SSL_SERVER_CERT
	return ssl_settings


def call_rest(payload, url, verb):
	# TODO put the URL to config file
	url = settings.REACTION_ENGINE_REST_URL + url

	# building the HMAC token
	nonce = str(uuid.uuid4())
	api_key = settings.REACTION_REST_AUTH_PUBLIC_KEY
	api_secret = settings.REACTION_REST_AUTH_PRIVATE_KEY
	date = http_date()
	signature = build_signature(verb, url, api_key, nonce, date, api_secret, payload)
	token = 'HmacSHA512' + ' ' + api_key + ':' + nonce + ':' + signature

	# calling the REST service
	header = {'Authorization': token, 'Date': date}
	if payload:
		logger.debug('Sending a {} request to {},\nthe payload is {}\nthe header is {}'.format(verb, url, payload, header))
	else:
		logger.debug('Sending a {} request to {}\nthe header is {}'.format(verb, url, header))

	ssl_settings = get_ssl_settings()
	try:
		if verb == 'POST':
			resp = requests.post(url, json=payload, timeout=30, headers=header, **ssl_settings)
		elif verb == 'GET':
			resp = requests.get(url, json=payload, timeout=30, headers=header, **ssl_settings)
		else:
			raise Exception('Unknown verb!')
		logger.debug('The response is {}'.format(resp))
		# raising the error if there is any
		resp.raise_for_status()
		return RestResponse(True, resp.text)
	except requests.exceptions.HTTPError as errh:
		# e.response.text ->   '{"timestamp":1500475992303,"status":403,"error":"Forbidden","message":"CallbackAuthenticationFilter.badSignature: Reaction web GUI cannot be called!","path":"/reaction-engine-0.1/executionflow/start/manual"}'
		logger.error('HTTPError : {}'.format(str(errh)))
		try:
			json_e = json.loads(errh.response.text)
			error_message = json_e['message']
		except Exception:
			error_message = errh.response.text
		return RestResponse(False, error_message, error_code=resp.status_code)
	except requests.exceptions.SSLError as errssl:
		logger.error('SSLError : {}'.format(str(errssl)))
		return RestResponse(False, 'SSL error! {}!'.format(errssl), error_code=495)
	except requests.exceptions.ConnectionError as errc:
		logger.error('ConnectionError : {}'.format(str(errc)))
		return RestResponse(False, 'The server on {}:{} is down!'.format(errc.args[0].pool.host, errc.args[0].pool.port), error_code=503)
	except requests.exceptions.Timeout as errt:
		logger.error('Timeout : {}'.format(str(errt)))
		return RestResponse(False, 'Timeoout occurred when tried to call the server on {}:{} is down!'.format(errt.args[0].pool.host, errt.args[0].pool.port), error_code=408)
	except Exception as ex:
		# e.response.text ->   '{"timestamp":1500475992303,"status":403,"error":"Forbidden","message":"CallbackAuthenticationFilter.badSignature: Reaction web GUI cannot be called!","path":"/reaction-engine-0.1/executionflow/start/manual"}'
		logger.error('Exception : {}'.format(str(ex)))
		error_message = ex.message if hasattr(ex, 'message') else str(ex)
		return RestResponse(False, error_message, error_code=500)


def build_signature(http_verb, url, api_key, nonce, date, api_secret, payload):
	d_e_l_i_m_i_t_e_r = b'\n'

	digester = hmac.new(api_secret.encode('utf-8'), digestmod = hashlib.sha512)

	digester.update(http_verb.encode('utf-8'))
	logger.debug('----------------------verb:' + ", ".join("{}".format(ord(c)) for c in http_verb))
	digester.update(d_e_l_i_m_i_t_e_r)

	req = Request(http_verb, url)
	prepped = req.prepare()
	digester.update(prepped.url.encode('utf-8'))
	logger.debug('----------------------url:' + ", ".join("{}".format(ord(c)) for c in prepped.url))
	logger.debug('----------------------url:' + prepped.url)
	digester.update(d_e_l_i_m_i_t_e_r)

	digester.update('application/json'.encode('utf-8'))
	digester.update(d_e_l_i_m_i_t_e_r)

	digester.update(api_key.encode('utf-8'))
	logger.debug('----------------------api_key:' + ", ".join("{}".format(ord(c)) for c in api_key))
	digester.update(d_e_l_i_m_i_t_e_r)

	digester.update(nonce.encode('utf-8'))
	logger.debug('----------------------nonce:' + ", ".join("{}".format(ord(c)) for c in nonce))
	digester.update(d_e_l_i_m_i_t_e_r)

	digester.update(date.encode('utf-8'))
	logger.debug('----------------------date:' + ", ".join("{}".format(ord(c)) for c in date))
	digester.update(d_e_l_i_m_i_t_e_r)

	if payload:
		json_text = json.dumps(payload)
		digester.update(json_text.encode('utf-8'))
		logger.debug('----------------------payload:' + ", ".join("{}".format(ord(c)) for c in json_text))
		digester.update(d_e_l_i_m_i_t_e_r);

	signature1 = digester.digest()
	signature2 = base64.b64encode(signature1)
	return str(signature2, 'UTF-8')


class RestResponse:
	def __init__(self, successful, message, error_code=None):
		self.successful = successful
		self.message = message
		self.error_code = error_code


class LoginRequiredForRestMixin(LoginRequiredMixin):
	"""
    If the user session expired (i.e. login is needed) a redirect (HTTP code is 302) is sent back to browser
      to redirect to the login page.
    However for REST service it is not good as the HTML as text is sent back and the REST client tries to
      process it as JSON so the app will be weird => send back another HTTP code and prepare the REST client
      to be able to process it
    """
	def handle_no_permission(self):
		if self.raise_exception:
			raise PermissionDenied(self.get_permission_denied_message())
		return HttpResponse('The session expired, login is needed! Please refresh the page in order to jump to the login page.',
							status=403)


def login_required_for_rest(function=None, redirect_field_name='next', login_url=None):
	actual_decorator = user_passes_test(lambda u: u.is_authenticated,
										login_url=login_url,
										redirect_field_name=redirect_field_name)
	if function:
		return actual_decorator(function)
	return actual_decorator

def user_passes_test(test_func, login_url=None, redirect_field_name='next'):
	def decorator(view_func):
		@wraps(view_func)
		def _wrapped_view(request, *args, **kwargs):
			if test_func(request.user):
				return view_func(request, *args, **kwargs)
			return HttpResponse('The session expired, login is needed! Please refresh the page in order to jump to the login page.',
								status=403)
		return _wrapped_view
	return decorator
