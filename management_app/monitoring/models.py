from django.db import models
from admin_event_source.models import EventSource
from admin_execution_flow.models import ExecutionFlow, Task
from scheduler.models import ScheduledExecutionFlow
from admin_event_source.models import ParentModel
from admin_event_source_type.models import EventSourceType
import datetime

# ################################## Event ##################################
class Event(ParentModel):
	identifier = models.CharField(max_length = 40, unique=True, db_index=True)
	log_level = models.CharField(choices = ParentModel.LOG_LEVELS, max_length = 40, blank = True, null = True, db_index=True)
	initiated_by = models.CharField(choices = ParentModel.INITIATORS, max_length = 40, default='BY_LOG', db_index=True)
	status = models.CharField(choices = ParentModel.EVENT_STATUSES, max_length = 40, db_index=True)
	message = models.TextField(blank = True, null = True)
	start_date = models.DateTimeField(blank = True, null = True, db_index=True)
	end_date = models.DateTimeField(blank = True, null = True)
	first_event_arrived = models.DateTimeField(blank = True, null=True)
	multiple_events_counter = models.IntegerField(blank = True, null=True)
	reason = models.TextField(blank=True, null=True)
	payload_values = models.CharField(max_length=3000, blank=True, null=True)
	query_parameters = models.CharField(max_length=3000, blank=True, null=True)
	execution_flow = models.ForeignKey(ExecutionFlow, on_delete=models.DO_NOTHING, blank=True, null=True, db_index=True)
	error_detector = models.ForeignKey("admin_errordetector.ErrorDetector", on_delete=models.DO_NOTHING, blank=True, null=True, db_index=True)
	event_source = models.ForeignKey(EventSource, on_delete=models.DO_NOTHING, blank = True, null = True, db_index=True)
	scheduled_execution_flow = models.ForeignKey(ScheduledExecutionFlow, on_delete=models.DO_NOTHING, blank=True, null=True, db_index=True)
	event_source_type = models.ForeignKey(EventSourceType, on_delete=models.DO_NOTHING, blank=True, null=True, db_index=True)

	class Meta:
		db_table = 'EVENT'
		default_permissions = ()
	
	@property
	def is_in_future(self):
		if self.start_date:
			return datetime.datetime.now() < self.start_date
		else:
			return False
	
	
# ################################## EventLife ##################################
class EventLife(ParentModel):
	event_date = models.DateTimeField(blank = True, null = True)
	ordr = models.IntegerField(default=1)
	output = models.TextField(blank = True, null = True)
	extracted_value = models.CharField(max_length = 500, blank = True, null = True)
	ext_command_successful = models.NullBooleanField(blank = True, null = True)
	status = models.CharField(choices = ParentModel.EVENT_STATUSES, max_length = 40)
	history = models.TextField(blank = True, null = True)
	error_message = models.TextField(blank = True, null = True)
	by_whom = models.CharField(max_length=200, blank = True, null = True)
	loop_index = models.IntegerField(blank = True, null = True)
	event = models.ForeignKey(Event, on_delete = models.CASCADE)
	task = models.ForeignKey(Task, on_delete = models.DO_NOTHING, blank = True, null = True)

	class Meta:
		db_table = 'EVENT_LIFE'
		default_permissions = ()
		
		
# ################################## CommandsToBeExecuted ##################################
class CommandsToBeExecuted(ParentModel):
	is_executed = models.BooleanField(default=False)
	event_life = models.ForeignKey(EventLife, on_delete = models.CASCADE)

	class Meta:
		db_table = 'COMMANDS_TO_BE_EXECUTED'
		default_permissions = ()