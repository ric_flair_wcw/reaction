from common.rest_utils import call_rest


def call_restart_eventlife(pk, username):
    url = '/executionflow/start/eventlife'
    payload = {'eventLifeId': pk,
               'byWhom': username}
    return call_rest(payload, url, 'POST')


def call_skip_eventlife(pk, value, username):
    url = '/executionflow/skip/eventlife'
    payload = {'eventLifeId': pk,
               'byWhom': username,
               'value': value}
    return call_rest(payload, url, 'POST')
