import logging
import datetime
from django.views.generic import ListView
from common.view_utils import FilteredListView
from .models import Event, EventLife, Task
from monitoring.service_clients import *
from monitoring.models import CommandsToBeExecuted
from admin_event_source_type.models import EventSourceType
from django.shortcuts import get_object_or_404, redirect
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
import json
from admin_execution_flow.mermaid import FlowBuilder
import csv
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from common.utils import *
from django.db import transaction
from common.rest_utils import login_required_for_rest
import copy
import itertools


logger = logging.getLogger('root')


class MonitoringList(LoginRequiredMixin, PermissionRequiredMixin, FilteredListView):
    paginate_by = 20
    template_name = 'monitoring/list.html'
    permission_required = 'auth.can_use_monitoring'

    # the dispatch is used to handle to click on the Export button (and on the Filter button of course)
    def dispatch(self, request, *args, **kwargs):
         if self.request.GET.get('submit') == 'Filter!':
             return super(MonitoringList, self).dispatch(request, *args, **kwargs)
         elif self.request.GET.get('submit') == 'Export to CVS':
             # Create the HttpResponse object with the appropriate CSV header.
             response = HttpResponse(content_type='text/csv')
             response['Content-Disposition'] = 'attachment; filename="reaction-events-'+datetime.datetime.now().strftime('%Y%m%d-%H%M%S')+'.csv"'
             writer = csv.writer(response)
             events = self.get_queryset()
             self.build_cvs(writer, events, request.GET.get('include_event_life') == 'yes')
             return response
         else:
             return super(MonitoringList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # build the filter
        filter = self.build_filter()
        #      build log level & event type filters
        self.build_multichoice_filter('event_source_type', filter)
        self.build_multichoice_filter('status', filter)
        self.build_multichoice_filter('initiated_by', filter)
        #      build the date filters
        start_date_filter = self.build_start_date_filter(self.request.GET.get('by_date'),
                                                         self.request.GET.get('start_date'),
                                                         self.request.GET.get('end_date'))
        # build the order
        order = ['-start_date' if 'order' not in self.request.GET else self.request.GET.get('order'), 'id']
        # build the access_groups filter
        conditions = build_filter_access_groups(self.request.user.profile.access_groups, True, 'execution_flow__')
        #      remove the hosts related filter and build it correctly later
        filter.pop('hosts__contains', None)
        filter_hosts = self.request.GET.get('filter_hosts')
        if filter_hosts:
            conditions.add(Q(**{'execution_flow__hosts__contains': filter_hosts}), Q.AND)
        # execute the query result (to make the query fast I prefetch the models(event_source, ...) ie. there won't be SELECTs like SELECT * FROM event_source WHERE id=... )
        # only querying the past events (all the events where the start_date < current date)
        start_date_filter['start_date__lt'] = datetime.datetime.now()
        logger.info('get_queryset - {}   |   filter :{}   |   conditions: {}   |   start_date_filter:{}   |   order: {}'.format(__name__, filter, conditions, start_date_filter, order))
        events = self.execute_event_query(conditions,
                                          filter,
                                          order,
                                          start_date_filter,
                                          'past events')
        # add the future (scheduled) events
        self.kwargs['events'] = self.add_closest_future_events(events,
                                                               self.request.GET.get('nr_future_events'),
                                                               conditions,
                                                               order,
                                                               filter)
        logger.debug('return the event list')
        return self.kwargs['events']

    def execute_event_query(self, conditions, filter, order, start_date_filter, logger_text):
        logger.debug('start ' + logger_text + ' SELECT')
        past_events_conditions = copy.deepcopy(conditions)
        past_events_conditions.add(Q(**start_date_filter), Q.AND)
        events = Event.objects.select_related('execution_flow')\
                              .select_related('event_source') \
                              .filter(past_events_conditions, **filter) \
                              .order_by(*order)
        logger.debug('end ' + logger_text + ' SELECT , size: ' + str(events.count()))
        return events

    def get_context_data(self, **kwargs):
        context = super(MonitoringList, self).get_context_data(**kwargs)
        # adding the filters, order and parent_id back to the context (in order to be visible for the next request -> they shouldn't disappear if I typed them and click on the Filter! button)
        for param in self.request.GET:
            if (param.startswith('filter_')):
                context[param] = self.request.GET.get(param, '')
        if 'order' in self.request.GET:
            context['order'] = self.request.GET.get('order')
        else:
            context['order'] = '-start_date'
        if 'by_date' in self.request.GET:
            context['by_date'] = self.request.GET.get('by_date')
        if 'nr_future_events' in self.request.GET:
            context['nr_future_events'] = self.request.GET.get('nr_future_events')
        if 'start_date' in self.request.GET and self.request.GET.get('by_date') == 'STARTED_BETWEEN':
            context['start_date'] = self.request.GET.get('start_date')
        if 'end_date' in self.request.GET and self.request.GET.get('by_date') == 'STARTED_BETWEEN':
            context['end_date'] = self.request.GET.get('end_date')
        # context['hosts'] = self.build_hosts()
        context['filter_log_level'] = self.request.GET.getlist('filter_log_level')
        context['filter_status'] = self.request.GET.getlist('filter_status')
        context['filter_initiated_by'] = self.request.GET.getlist('filter_initiated_by')
        context['filter_event_source_type'] = self.request.GET.getlist('filter_event_source_type')
        event_source_types = EventSourceType.objects.order_by('name')
        context['event_source_types'] = '[' + ','.join("{id:'" + str(event_source_type.pk) + "',text:'" + str(event_source_type.name) + "'}" for event_source_type in event_source_types) + ']';
        return context
    
    def build_cvs(self, writer, events, include):
        # build the header (1st row in CSV)
        event_header = ['Identifier', 'Start date', 'End_date', 'Log level', 'Status', 'Initiated by', 'First event arrived', 'Counter of multiple events', 'Execution flow - name',
                        'Execution flow - conf. needed', 'Execution flow - status', 'Error detector - name', 'Event source - name', 'Event source - log path', 'Event source - type', 'Event source - log level', 'Hosts']
        event_life_header = ['EL date', 'EL task - name', 'EL task - type', 'EL status', 'EL task - host', 'EL task - OS user', 'EL task - command', 'EL task - output pattern', 'EL task - IF operation',
                             'EL task - IF value', 'EL extracted value', 'EL command successful']
        if include :
            event_header.extend(event_life_header)
        writer.writerow(event_header)
        # build the event (and event life) rows
        for event in events:
            # build the event row
            execution_flow = event.error_detector.execution_flow if event.error_detector is not None else event.execution_flow
            error_detector_name = '' if event.error_detector is None else event.error_detector.name
            error_detector_confirmation_needed = '' if event.error_detector is None else event.error_detector.confirmation_needed
            event_source_name = '' if event.event_source is None else event.event_source.name
            event_source_log_path = '' if event.event_source is None else event.event_source.log_path
            event_source_type = '' if event.event_source is None else event.event_source.type
            event_source_log_level = '' if event.event_source is None else event.event_source.log_level
            hosts = '' if execution_flow.hosts is None else execution_flow.hosts
            event_row = [event.identifier, event.start_date, event.end_date, event.log_level, event.status, event.initiated_by, event.first_event_arrived, event.multiple_events_counter,  execution_flow.name, error_detector_confirmation_needed, execution_flow.status, error_detector_name, event_source_name, event_source_log_path, event_source_type, event_source_log_level, hosts]
            if include:
                # query the event_life
                event_life_records = EventLife.objects.filter(event=event).order_by('event_date')
                for event_life in event_life_records:
                    task_name = '' if event_life.task is None else event_life.task.name
                    task_internal_task = '' if event_life.task is None else event_life.task.internal_task
                    task_host = '' if event_life.task is None else event_life.task.host
                    task_os_user = '' if event_life.task is None else event_life.task.os_user
                    task_command = '' if event_life.task is None else event_life.task.command
                    task_output_pattern = '' if event_life.task is None else event_life.task.output_pattern
                    task_if_expression = '' if event_life.task is None else event_life.task.if_expression
                    event_life_row = [event_life.event_date, task_name, task_internal_task, event_life.status, task_host, task_os_user, task_command, task_output_pattern, task_if_expression, event_life.extracted_value, event_life.ext_command_successful]
                    writer.writerow(event_row + event_life_row)
            else:
                writer.writerow(event_row)
        
    def build_start_date_filter(self, by_date, start_date_text, end_date_text):
        filter = {}
        # set the start date
        if by_date is None:
            return filter
        elif by_date.startswith('STARTED_'):
            if by_date == 'STARTED_BETWEEN':
                start_date = self.check_date(start_date_text)
                end_date = self.check_date(end_date_text)
                filter['start_date__range'] = (start_date, end_date)
            else:
                if by_date == 'STARTED_LAST_10MINS':
                    start_date = datetime.datetime.now() - datetime.timedelta(minutes=10)
                elif by_date == 'STARTED_LAST_30MINS':
                    start_date = datetime.datetime.now() - datetime.timedelta(minutes=30)
                elif by_date == 'STARTED_LAST_1HOUR':
                    start_date = datetime.datetime.now() - datetime.timedelta(hours=1)
                elif by_date == 'STARTED_LAST_4HOURS':
                    start_date = datetime.datetime.now() - datetime.timedelta(hours=4)
                elif by_date == 'STARTED_TODAY':
                    start_date = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
                    filter['start_date__lt'] = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) + datetime.timedelta(days=1)
                elif by_date == 'STARTED_YESTERDAY':
                    start_date = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
                    filter['start_date__lt'] = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
                elif by_date == 'STARTED_THIS_WEEK':
                    today_midnight = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
                    start_date = today_midnight - datetime.timedelta(days=today_midnight.weekday())
                    filter['start_date__lt'] = datetime.datetime.now()
                elif by_date == 'STARTED_LAST_WEEK':
                    today_midnight = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
                    # the Monday last week
                    start_date = today_midnight - datetime.timedelta(days=today_midnight.weekday(), weeks=1)
                    # the Sunday last week
                    filter['start_date__lt'] = today_midnight - (datetime.timedelta(days=today_midnight.weekday() + 1))
                elif by_date == 'STARTED_THIS_MONTH':
                    start_date = datetime.datetime.now().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    filter['start_date__lt'] = datetime.datetime.now()
                elif by_date == 'STARTED_LAST_MONTH':
                    last_month = datetime.datetime.now().replace(day=1, hour=0, minute=0, second=0, microsecond=0) - datetime.timedelta(days=1)
                    start_date = last_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                    filter['start_date__lt'] = datetime.datetime.now().replace(day=1, hour=0, minute=0, second=0, microsecond=0)
                elif by_date == 'STARTED_THIS_YEAR':
                    start_date = datetime.datetime.now().replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
                    filter['start_date__lt'] = datetime.datetime.now()
                filter['start_date__gt'] = start_date
        elif by_date == 'NULL':
            return filter
        elif by_date.startswith('IN_STARTED_MORE'):
            if by_date == 'IN_STARTED_MORE_THAN_2HOURS':
                start_date = datetime.datetime.now() - datetime.timedelta(hours=2)
            if by_date == 'IN_STARTED_MORE_THAN_6HOURS':
                start_date = datetime.datetime.now() - datetime.timedelta(hours=6)
            if by_date == 'IN_STARTED_MORE_THAN_24HOURS':
                start_date = datetime.datetime.now() - datetime.timedelta(hours=24)
            if by_date == 'IN_STARTED_MORE_THAN_2DAYS':
                start_date = datetime.datetime.now() - datetime.timedelta(days=2)
            if by_date == 'IN_STARTED_MORE_THAN_4DAYS':
                start_date = datetime.datetime.now() - datetime.timedelta(days=4)
            if by_date == 'IN_STARTED_MORE_THAN_10DAYS':
                start_date = datetime.datetime.now() - datetime.timedelta(days=10)
            filter['start_date__lt'] = start_date
            filter['status'] = 'STARTED'
        return filter

    def add_closest_future_events(self, events, nr_future_events, conditions, order, filter):
        if nr_future_events and nr_future_events == '0':
            # don't include the future events
            return events
        else:
            # if the selected option is 'Include the X closest future events' then the order HAS TO START with start_date -> to pick the X closest events
            if not nr_future_events or not nr_future_events == 'NULL':
                order.insert(0, '-start_date')
            # the future events
            future_events = self.execute_event_query(conditions,
                                                     filter,
                                                     order,
                                                     {'start_date__gte': datetime.datetime.now()},
                                                     'future events')
            # calculate how many have to be included
            if not nr_future_events or nr_future_events == 'NULL':
                # include all the future events
                return future_events | events
            else:
                # include specific number of future events
                future_events_len = len(future_events)
                start_index = 0 if future_events_len - int(nr_future_events) <= 0 else future_events_len - int(nr_future_events)
                return (future_events | events)[start_index:]


    def check_date(self, datetime_text):
        # sample: 2017.03.16 14:59:01
        try:
            return datetime.datetime.strptime(datetime_text, '%Y.%m.%d %H:%M:%S')
        except ValueError:
            raise ValueError("Incorrect data format, should be YYYY.mm.dd HH:MM:SS (e.g. 2017.03.16 17:00:41) !")

    def build_multichoice_filter(self, field, filter):
        # remove the filter and ...
        filter.pop(field+'__contains', None)
        # build it correctly later
        arr = self.request.GET.getlist('filter_'+field)
        if arr:
            filter[field+'__in'] = arr


def convert_json_to_dictionary(json_text):
    try:
        return json.loads(json_text)
    except:
        logging.exception("The JSON text couldn't be converted to dictionary!")
        return {"WARNING!": "The JSON text couldn't be converted to dictionary!"}


class MonitoringDetailsView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'monitoring/details.html'
    permission_required = 'auth.can_use_monitoring'

    def get_queryset(self):
        event_id = self.kwargs.get('pk')
        filter_loop_task = self.get_filter_loop_task(self.request.GET, self.request.GET.get('submit'), Event.objects.get(id=self.kwargs['pk']).execution_flow)
        logger.info('get_queryset - {}|  event_id :{}'.format(__name__, event_id))
        event_life_list = EventLife.objects.select_related('task')\
                                           .filter(event__id=event_id)\
                                           .order_by('event_date', 'ordr')
        return event_life_list[::-1] if self.request.GET.get('submit') == 'Display all!' else self.remove_other_loops(event_life_list, filter_loop_task)

    def get_context_data(self, **kwargs):
        context = super(MonitoringDetailsView, self).get_context_data(**kwargs)
        event = Event.objects.get(id=self.kwargs['pk'])
        context['event'] = event
        # building the text for mermaid flow
        context['mermaid_flow'], context['tooltip_loop'] = FlowBuilder.build(event.execution_flow.id)
        # adding the task ids to highlight the executed tasks in the mermaid flow
        filter_loop_task = self.get_filter_loop_task(self.request.GET, self.request.GET.get('submit'), event.execution_flow)
        # reduced_event_life = self.object_list if self.request.GET.get('submit') == 'Display all!' else set(self.remove_other_loops(self.object_list, filter_loop_task))
        context['executed_task_ids'] = '[' + ','.join((str(v.task.id) if v.task else '') for v in self.object_list) + ']'
        loop_value_dict = {}
        self.get_loop_value_dict('execution_flow', event.execution_flow, loop_value_dict)
        context['loop_value_dict'] = loop_value_dict
        # if error occurred during the REST call then a modal dialog has to be displayed with message
        if self.request.session.has_key('successful'):
            context['successful'] = self.request.session['successful']
            del self.request.session['successful']
            context['errorMessage'] = self.request.session['errorMessage']
            del self.request.session['errorMessage']
        context['payload_values'] = convert_json_to_dictionary(event.payload_values).items()
        context['query_parameters'] = convert_json_to_dictionary(event.query_parameters).items()
        return context

    def get_filter_loop_task(self, get, submit_value, execution_flow):
        filter_loop_task = {}
        for k, v in get.items():
            if k.startswith('loop_task_'):
                filter_loop_task[k.replace('loop_task_', '')] = v
        if not (filter_loop_task and submit_value != 'Display all!'):
            self.get_default_filter_loop_task('execution_flow', execution_flow, filter_loop_task)
        return filter_loop_task

    def get_default_filter_loop_task(self, type, task, default_filter_loop_task):
        filter_text = {
            '{0}__{1}'.format(type, 'exact'): task
        }
        for task in Task.objects.filter(**filter_text):
            if task.internal_task == 'LOOP':
                default_filter_loop_task[task.pk] = 0
                self.get_default_filter_loop_task('primary_task', task, default_filter_loop_task)
            elif task.internal_task == 'IF_ELSE':
                self.get_default_filter_loop_task('primary_task', task, default_filter_loop_task)
                self.get_default_filter_loop_task('secondary_task', task, default_filter_loop_task)

    def get_loop_value_dict(self, type, task, loop_value_dict):
        filter_text = {
            '{0}__{1}'.format(type, 'exact'): task
        }
        for task in Task.objects.filter(**filter_text):
            if task.internal_task == 'LOOP':
                loop_value_dict[(task.pk, task.name)] = self.get_loop_value_dict_values(task)
                self.get_loop_value_dict('primary_task', task, loop_value_dict)
            elif task.internal_task == 'IF_ELSE':
                self.get_loop_value_dict('primary_task', task, loop_value_dict)
                self.get_loop_value_dict('secondary_task', task, loop_value_dict)

    def get_loop_value_dict_values(self, task):
        ind = 0
        result = []
        for rec in task.loop_value_list_1.split(task.loop_separator_1):
            result.append(str(ind) + " - " + task.loop_placeholder_var_1 + ": " + rec + self.get_loop_value(task, 2, ind) + self.get_loop_value(task, 3, ind) + self.get_loop_value(task, 4, ind))
            ind += 1
        return result

    def get_loop_value(self, task, task_ind, ind):
        return ", " + getattr(task, 'loop_placeholder_var_'+str(task_ind)) + ": " + getattr(task, 'loop_value_list_' + str(task_ind)).split(getattr(task, 'loop_separator_'+str(task_ind)))[ind] \
            if getattr(task, 'loop_placeholder_var_'+str(task_ind)) \
            else ""

    def remove_other_loops(self, event_life_list, filter_loop_tasks):
        new_event_life_list = list(event_life_list)
        for loop_task_id, loop_index in filter_loop_tasks.items():
            is_to_be_removed = False
            for event_life in new_event_life_list[:]:
                if event_life.task and event_life.task.id == int(loop_task_id):
                    is_to_be_removed = False
                    if event_life.status != 'LOOP_COMPLETED' and event_life.loop_index != int(loop_index):
                        is_to_be_removed = True
                if is_to_be_removed:
                    new_event_life_list.remove(event_life)
        return new_event_life_list[::-1]


@login_required
@permission_required('auth.can_use_monitoring', raise_exception=True)
def restart_eventlife(request, pk):
    # calling the REST service
    rest_response = call_restart_eventlife(pk, request.user.username)
    # checking if it was successful
    if not rest_response.successful:
        request.session['successful'] = 'N'
        request.session['errorMessage'] = str(rest_response.error_code) + ' - ' + rest_response.message
    event_life = get_object_or_404(EventLife, id=pk)
    return redirect('monitoring_details', pk=event_life.event.id)


@login_required
@permission_required('auth.can_use_monitoring', raise_exception=True)
@transaction.atomic
def cancel_event(request, pk):
    # check if the status of the event is still in CONFIRMED, SCHEDULED, WAITING_FOR_OTHERS, STARTED
    event = get_object_or_404(Event, id=pk)
    if event.status in ('CONFIRMED', 'SCHEDULED', 'WAITING_FOR_OTHERS', 'STARTED'):
        # change the status of the event to CANCELLED
        event.status = 'CANCELLED'
        event.save()
        # create an event life record for the CANCEL operation
        EventLife.objects.create(event_date = datetime.datetime.now(),
                                 event = event,
                                 ordr = 9999,
                                 by_whom = request.user.username,
                                 status = 'CANCELLED')
        # update the 'is_executed' field of the relating CommandsToBeExecuted entities to true (not to process them anymore)
        event_life_list = EventLife.objects.filter(event = event)
        CommandsToBeExecuted.objects.filter(event_life__in = event_life_list).update(is_executed = True)
        # disable the scheduled exec flow if there is any
        scheduled_execution_flow = event.scheduled_execution_flow
        if scheduled_execution_flow:
            scheduled_execution_flow.scheduled_already = False;
            scheduled_execution_flow.save()
    return redirect('monitoring_details', pk = pk)


# get the full text of the output / extracted value / error message (based on the what parameter) of the event life
@api_view(['GET'])
@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
def get_full_text_of_event_life(request, pk, what):
    logger.info('get_full_text_of_event_life / GET is called - pk={}, what={}, request={}'.format(pk, what, request.data))
    event_life = get_object_or_404(EventLife, id=pk)
    if what == 'output':
        return Response(event_life.output, status=status.HTTP_200_OK)
    elif what == 'extracted value':
        return Response(event_life.extracted_value, status=status.HTTP_200_OK)
    elif what == 'error message':
        return Response(event_life.error_message, status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


# get the history of the event life (the history property contains a JSON string that has to be parsed)
@api_view(['GET'])
@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
def get_history_of_event_life(request, pk):
    logger.info('get_history_of_event_life / GET is called - pk={}, request={}'.format(pk, request.data))
    event_life = get_object_or_404(EventLife, id=pk)
    if event_life.history is None:
        return Response(None, status=status.HTTP_200_OK)
    try:
        h = json.loads(event_life.history)
    except ValueError:
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='The history of the event life({}) is not a valid JSON!'.format(pk))
    else:
        return Response(h, status=status.HTTP_200_OK)


# get the type of the next task -> it is needed to be able to skip the task
@api_view(['GET'])
@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
def get_next_task_type(request, pk):
    logger.info('get_next_task_type / GET is called - pk={}'.format(pk))
    event_life = get_object_or_404(EventLife, id=pk)
    # creating the filter
    filter = {}
    if event_life.task.execution_flow is not None:
        filter['execution_flow'] = event_life.task.execution_flow
    elif event_life.task.primary_task is not None:
        filter['primary_task'] = event_life.task.primary_task
    elif event_life.task.secondary_task is not None:
        filter['secondary_task'] = event_life.task.secondary_task
    filter['ordr'] = event_life.task.ordr + 1
    # getting the next task on this level
    next_task = Task.objects.filter(**filter)
    if next_task.exists():
        return Response(next_task.first().internal_task, status=status.HTTP_200_OK)
    else:
        return Response('', status=status.HTTP_200_OK)


# skip the task
@api_view(['POST'])
@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
def skip_task(request, pk):
    logger.info('skip_task / POST is called - pk={}, request={}'.format(pk, request))
    # calling the REST service
    rest_response = call_skip_eventlife(pk, request.data.get('value'), request.user.username)
    # checking if it was successful
    if not rest_response.successful:
        return Response(status=rest_response.error_code, data=rest_response.message)
    else:
        return Response(status=status.HTTP_200_OK, data='')