from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^monitoring$',                                              views.MonitoringList.as_view(),                name = 'monitoring_list'),
	url(r'^monitoring/(?P<pk>[0-9]+)/details$',                       views.MonitoringDetailsView.as_view(),         name = 'monitoring_details'),

    url(r'^monitoring/(?P<pk>[0-9]+)/restart$',                       views.restart_eventlife,                       name = 'monitoring_restart'),
	
	url(r'^monitoring/(?P<pk>[0-9]+)/cancel$',                        views.cancel_event,                            name = 'monitoring_cancel'),
	
	# for REST service
	url(r'^monitoring/event_life/(?P<pk>[0-9]+)/history$',  		  views.get_history_of_event_life,               name = 'monitoring_event_life_history'),
	url(r'^monitoring/event_life/(?P<pk>[0-9]+)/(?P<what>[a-z ]+)$',  views.get_full_text_of_event_life,             name = 'monitoring_event_life_full'),
	url(r'^monitoring/event_life/(?P<pk>[0-9]+)/next_task_type$',	  views.get_next_task_type,						 name = 'monitoring_check_next_task'),
	url(r'^monitoring/event_life/(?P<pk>[0-9]+)/skip_task$',		  views.skip_task,						 		 name = 'monitoring_skip_task')

]