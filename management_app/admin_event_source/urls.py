from django.conf.urls import url

from . import views

urlpatterns=[
    url(r'^administration/event_source(?:/)?$',                                                     views.EventSourceList.as_view(),            name='admin_event_source_list'),
    url(r'^administration/event_source(?:/(?P<parent_id>[0-9]*))?$',                                views.EventSourceList.as_view(),            name='admin_event_source_list'),

    url(r'^administration/event_source/(?P<pk>[0-9]+)/update$',                                     views.EventSourceUpdate.as_view(),          name='admin_event_source_update'),

    url(r'^administration/event_source/(?P<parent_id>[0-9]+)/create$',                              views.EventSourceCreate.as_view(),          name='admin_event_source_create'),
    url(r'^administration/event_source/create$',                                                    views.EventSourceCreate.as_view(),          name='admin_event_source_create'),

    url(r'^administration/event_source/(?P<pk>[0-9]+)/delete$',                                     views.delete_event_source,                  name='admin_event_source_delete'),

    url(r'^administration/event_source/tree$',                                                      views.EventSourceTreeList.as_view(),        name='admin_event_source_tree'),
    
    url(r'^administration/event_source/log_header_pattern/(?P<pk>[0-9]+)/(?P<logentry>.*)$',        views.check_header_pattern,                 name='admin_event_source_check_header_pattern'),

    url(r'^administration/event_source/(?P<pk>[0-9]*)/remove_history',                              views.remove_history,                       name='admin_event_source_remove_history'),

    # for REST service
    url(r'^administration/event_source/list$',                                                      views.get_event_sources,                    name='admin_event_source'),
    url(r'^administration/event_source/event_source_type/(?P<pk>[0-9]+)$',                          views.get_event_source_type,                name='admin_event_source_event_source_type'),
]
