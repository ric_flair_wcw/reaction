import json
import logging

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.db import transaction
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.views.generic import ListView, UpdateView, CreateView
from django.conf import settings
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from admin_errordetector.models import ErrorDetector
from admin_event_source.service_clients import *
from admin_event_source_type.models import EventSourceType
from monitoring.models import Event
from common.rest_utils import login_required_for_rest
from .forms import EventSourceForm
from .models import EventSource
from .serializers import EventSourceSerializer, EventSourceTypeSerializer

# Create your views here.
logger = logging.getLogger('root')


class EventSourceList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    paginate_by = 10
    template_name = 'admin_event_source/list.html'
    permission_required = 'auth.can_use_admin_event_source'

    def get_queryset(self):
        filter = self.build_filter()
        order = ['name' if 'order' not in self.request.GET else self.request.GET.get('order'), 'id']
        logger.info('get_queryset - %s|  filter :%s,    order: %s' % (__name__,filter,order))
        return EventSource.objects.filter(**filter).order_by(*order)

    # building the filter for the query in get_queryset
    def build_filter(self):
        # adding the parent_id part to the filter
        if 'parent_id' in self.kwargs:
            filter = {
                'parent': self.kwargs['parent_id']
            }
        else:
            filter = {
                '{0}__{1}'.format('parent', 'isnull'): True
            }
        # adding the filters coming from the GUI
        for param in self.request.GET:
            if param.startswith('filter_'):
                value = self.request.GET.get(param)
                if value:
                    key = param[7:]
                    filter[key + '__contains'] = value
        return filter

    # building the navigation path that is displayed above the table
    def build_path(self):
        path = []
        if 'parent_id' in self.kwargs:
            parent_id = self.kwargs['parent_id']
            event_source = EventSource.objects.get(id=parent_id)
            path.append({'id': event_source.id, 'name': event_source.name})
            while event_source.parent is not None:
                event_source = EventSource.objects.get(id = event_source.parent.id)
                path.append({'id': event_source.id, 'name': event_source.name})
        path.reverse()
        return path

    def get_context_data(self, **kwargs):
        context = super(EventSourceList, self).get_context_data(**kwargs)
        context['path'] = self.build_path()
        # adding the filters, order and parent_id back to the context (in order to be visible for the next request -> they shouldn't disappear if I typed them and click on the Filter! button)
        for param in self.request.GET:
            if param.startswith('filter_'):
                context[param] = self.request.GET.get(param, '')
        if 'order' in self.request.GET:
            context['order'] = self.request.GET.get('order')
        if 'parent_id' in self.kwargs:
            context['parent_id'] = self.kwargs['parent_id']
        return context


class EventSourceUpdateCreateView(LoginRequiredMixin, PermissionRequiredMixin):
    model = EventSource
    form_class = EventSourceForm
    template_name = 'admin_event_source/form.html'
    permission_required = 'auth.can_use_admin_event_source'

    def get_success_url(self):
        if self.object.parent is None:
            return reverse_lazy('admin_event_source_list')
        else:
            return reverse_lazy('admin_event_source_list', kwargs={'parent_id': self.object.parent.id})

    def get_context_data(self, **kwargs):
        # setting the parent combo box
        context = super(EventSourceUpdateCreateView, self).get_context_data(**kwargs)
        context['form_operation'] = 'update' if self.__class__.__name__ == 'EventSourceUpdate' else 'create'
        if self.object is not None:
            self.set_maintenance_window_params(context, self.object.maintenance_window)
        # setting the event source type id and name to the dropdown
        choices = [(None, '---------')]
        for ist in EventSourceType.objects.order_by('name'):
            choices.append((ist.id, ist.name))
        context['form'].fields['event_source_type'].choices = choices
        # if the event_source_type is set on one of the parents then add the parent and the value to the context
        if context['form_operation'] == 'create':
            if 'parent' in context['form'].initial:
                parent = context['form'].initial['parent']
                context['set_in_parent'] = {'event_source_type': self.get_set_in_parent(parent, 'event_source_type.name')}
        else:
            if self.object.parent:
                context['set_in_parent'] = {'event_source_type': self.get_set_in_parent(self.object.parent, 'event_source_type.name'),
                                            'host': self.get_set_in_parent(self.object.parent, 'host'),
                                            'log_path': self.get_set_in_parent(self.object.parent, 'log_path'),
                                            'log_level': self.get_set_in_parent(self.object.parent, 'log_level')}
        # setting the event_source id and name to the dropdown
        choices = [(None, '---------')]
        complemented_event_sources = build_complemented_event_source_names()
        for ef in complemented_event_sources:
            choices.append((ef['id'], ef['name']))
        context['form'].fields['parent'].choices = choices
        return context

    def set_maintenance_window_params(self, context, maintenance_window_json_text):
        if maintenance_window_json_text is not None:
            maintenance_window = json.loads(maintenance_window_json_text)
            if maintenance_window.get('Mon') is not None:
                context['maint_w_monday'] = ', '.join(maintenance_window.get('Mon'))
            if maintenance_window.get('Tue') is not None:
                context['maint_w_tuesday'] = ', '.join(maintenance_window.get('Tue'))
            if maintenance_window.get('Wed') is not None:
                context['maint_w_wednesday'] = ', '.join(maintenance_window.get('Wed'))
            if maintenance_window.get('Thu') is not None:
                context['maint_w_thursday'] = ', '.join(maintenance_window.get('Thu'))
            if maintenance_window.get('Fri') is not None:
                context['maint_w_friday'] = ', '.join(maintenance_window.get('Fri'))
            if maintenance_window.get('Sat') is not None:
                context['maint_w_saturday'] = ', '.join(maintenance_window.get('Sat'))
            if maintenance_window.get('Sun') is not None:
                context['maint_w_sunday'] = ', '.join(maintenance_window.get('Sun'))

    def get_set_in_parent(self, parent, field):
        if self.get_field(parent, field):
            return {'parent': parent.name,
                    'value': self.get_field(parent, field)}
        else:
            if parent.parent:
                return self.get_set_in_parent(parent.parent, field)
            else:
                return None

    def get_field(self, entity, fields):
        for field in fields.split('.'):
            if entity :
                entity = getattr(entity, field)
            else:
                return None
        return entity


class EventSourceUpdate(EventSourceUpdateCreateView, UpdateView):
    def form_valid(self, form):
        form.instance.status = 'VALID'
        return super(EventSourceUpdate, self).form_valid(form)

class EventSourceCreate(EventSourceUpdateCreateView, CreateView):
    def get_initial(self):
        initial = super(EventSourceCreate, self).get_initial()
        if 'parent_id' in self.kwargs:
            initial['parent'] = EventSource.objects.get(id = self.kwargs['parent_id'])
        return initial

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())


@login_required
@permission_required('auth.can_use_admin_event_source', raise_exception=True)
def delete_event_source(request, pk):
    logger.info('delete_event_source is called - %s' % __name__)
    return delete_event_source_if_possible(request, pk)

def delete_event_source_if_possible(request, pk):
    try:
        is_removable, parent_event_source = is_event_source_removable(pk)
        logger.debug('is_removable: {}, parent_event_source: {}'.format(is_removable, parent_event_source.name if parent_event_source else '---'))
        if is_removable:
            EventSource.objects.filter(id=pk).delete()
            return HttpResponseRedirect(reverse_lazy('admin_event_source_list'))
        else:
            message = 'The event source cannot be deleted as one of its parent ("{}]") is in an error detector and all the ITEM child event source of this parent are in the current event source ' \
                      'that is under deletion (the parent event source that is in error detector would not have any ITEM event source left after deletion => a GROUP event source ' \
                      'without any ITEM child event source cannot be in an error detector).'.format(parent_event_source.name)
            logger.error(message)
            return render(request, 'business_error.html', {'message': message})
    except IntegrityError as e:
        message = 'The event source cannot be deleted as it is currently being used in an error detector!'
        logger.exception(message)
        return render(request, 'business_error.html', {'message': message})

def is_event_source_removable(pk):
    # count the ITEM children of this event source (if the current event source is ITEM then the count will be 1)
    cnt_current_event_source = count_event_source_children(pk)
    logger.debug('cnt_current_event_source: {}'.format(cnt_current_event_source))
    if get_object_or_404(EventSource, pk=pk).type == 'ITEM':
        cnt_current_event_source = 1
    # if the count is bigger then zero then check if one of its parents is in an error detector
    if cnt_current_event_source > 0:
        parent_in_error_detector = get_parent_in_error_detector(pk)
        logger.debug('parent_in_error_detector: {}'.format(parent_in_error_detector.name if parent_in_error_detector else '---'))
        # if it is then count the ITEM children of parent event_source then if the 2 counts are equal then the event_source mustn't be deleted
        if parent_in_error_detector:
            cnt_parent_event_source = count_event_source_children(parent_in_error_detector.pk)
            logger.debug('cnt_parent_event_source: {}'.format(cnt_parent_event_source))
            if cnt_current_event_source == cnt_parent_event_source:
                return False, parent_in_error_detector
    return True, None

def count_event_source_children(id):
    cnt = 0
    for event_source in EventSource.objects.filter(parent=id):
        if event_source.type == 'ITEM':
            cnt += 1
        else:
            cnt += count_event_source_children(event_source.id)
    return cnt

def get_parent_in_error_detector(id):
    event_source = get_object_or_404(EventSource, id=id)
    while event_source.parent:
        if ErrorDetector.objects.filter(event_source=event_source.parent).count() > 0:
            return event_source.parent
        else:
            event_source = event_source.parent
    return None

class EventSourceTreeList(LoginRequiredMixin, PermissionRequiredMixin, APIView):
    permission_required = 'auth.can_use_admin_event_source'
    
    def get(self, request, format=None):
        logger.info('Event source tree list is called.')
        queryset = EventSource.objects.filter(parent=None).order_by('name')
        serializer = EventSourceSerializer(queryset, many=True)
        return Response(serializer.data)
    
    
@login_required_for_rest
@permission_required('auth.can_use_admin_event_source', raise_exception=True)
@api_view(['GET'])
def check_header_pattern(request, pk, logentry):
    logger.info('check_header_pattern / GET is called - pk={}, logentry={}, request={}'.format(pk, logentry, request.data))
    rest_response = call_check_pattern(pk, logentry)
    if rest_response.successful:
        return Response(rest_response.message, status=status.HTTP_200_OK)
    else:
        return Response(rest_response.message, status=rest_response.error_code)


# -------------------------------------------------- for REST --------------------------------------------------

@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['GET'])
@transaction.atomic
def get_event_sources(request):
    logger.info('Getting the event source list. request={}'.format(request))
    event_source_list = build_complemented_event_source_names()
    return Response(event_source_list, status=status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['GET'])
@transaction.atomic
def get_event_source_type(request, pk):
    logger.info('Getting the event source type. pk={}, request={}'.format(pk, request))

    event_source_type = get_object_or_404(EventSourceType, id=pk)
    serializer = EventSourceTypeSerializer(event_source_type, many=False)

    return Response(serializer.data, status=status.HTTP_200_OK)


@login_required
@permission_required('auth.can_use_admin_event_source', raise_exception=True)
def remove_history(request, pk):
    event_source = get_object_or_404(EventSource, pk=pk)
    Event.objects.filter(event_source=event_source).delete()
    return HttpResponseRedirect(reverse_lazy('admin_event_source_update', kwargs={'pk': pk}))


# -------------------------------------------------- utility functions ------------------------------------------------
def build_complemented_event_source_names():
    event_source_list = []
    # querying the database only once and work with the event source list
    event_source_entities = EventSource.objects.filter(status='VALID').order_by('-parent', 'id')
    # looping through the event_source on the highest level
    for event_source_entity in event_source_entities:
        if event_source_entity.parent is None:
            event_source_list.append({'name': event_source_entity.name + ' [' + event_source_entity.type + ']',
                                      'id': event_source_entity.id,
                                      'parent_id': None,
                                      'type': event_source_entity.type})
            complement_event_source(event_source_list, event_source_entities, event_source_entity, event_source_entity.name)
    return event_source_list


# add the parents names to the current event_source recursively
# like 'Melius / SOA servers / SOA1'
#   already_found -> the event_source_entity is ordered by parent so if the id is already found once but the loop stepped to a event_source
#                    whose parent is different then the loop can be terminated (more effective loop)
# Parameters
#       event_source_list : the event source list to be sent back
#       event_source_entities: the database entity list the contains all the event_sources
#       current_event_source_entity: basically the parent event_source entity which was added already
#       complemented_event_source_name: the event_source name that contains the parents too
def complement_event_source(event_source_list, event_source_entities, current_event_source_entity, complemented_event_source_name):
    already_found = False
    for event_source_entity in event_source_entities:
        if event_source_entity.parent == current_event_source_entity:
            already_found = True
            event_source_list.append({'name': complemented_event_source_name + ' / ' + event_source_entity.name + ' [' + event_source_entity.type + ']',
                                     'id': event_source_entity.id,
                                     'parent_id': event_source_entity.parent.id,
                                     'type': event_source_entity.type})
            complement_event_source(event_source_list, event_source_entities, event_source_entity, complemented_event_source_name + ' / ' + event_source_entity.name)
        elif already_found:
            break