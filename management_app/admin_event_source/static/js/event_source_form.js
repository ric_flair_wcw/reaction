var positions = [];
var numRows = 1;


// showing tooltip to help how to specify the date format at log header
$(function() {
    $( '#icon_question_dateformat' ).tooltip({
        position: {
            my: "left-325 bottom",
            at: "right top"
        },
        content: $('#t_dateFormat').html(),
        open: function (event, ui) {
            ui.tooltip.css("max-width", "900px");
        }
    });
});

function init() {
    $('#i_check_pattern').keyup(function() {
        if ($(this).val().length == 0) {
            $('#b_check_pattern').attr('disabled', 'disabled');
        } else {
            $('#b_check_pattern').removeAttr('disabled');
        }
    });

    // ---------------------- enable/disable log header pattern ----------------------
    disable_log_header_pattern($("#id_log_header_pattern_enabled"));
    $('#id_log_header_pattern_enabled').on('change', function() {
        disable_log_header_pattern($(this));
    });

    // ---------------------- accordion for advanced settings ----------------------
    $("#d_advanced_settings").accordion({
        heightStyle: "content",
        collapsible: true,
        active: false
    });
    if (accordion_active) {
        $( "#d_advanced_settings" ).accordion( "option", "active", 0);

    }

    // ---------------------- jQuery for building the log header accordion ----------------------
    var accordion_log_header_pattern = $("#accordion_log_header_pattern").accordion({
        heightStyle: "content",
        active: 0,
        event: false
    });
    $( "#accordion-resizer" ).resizable({
        minHeight: 400,
        minWidth: 830,
        resize: function() {
            $( "#accordion_log_header_pattern" ).accordion( "refresh" );
        }
    });

    // ---------------------- jQuery for configuring the dialog to build the log header pattern ----------------------
    $("#build_log_header_pattern_dialog").dialog({
        autoOpen: false,
        height: 560,
        width: 900,
        modal: true,
        buttons: {
            "Ok": function() {
                if ( $("#i_log_header").text().length > 0) {
                    $("#id_log_header_pattern").val( $("#i_log_header").text() );
                    $("#build_log_header_pattern_dialog").dialog("close");
                } else {
                    alert("The log header pattern input field cannot be empty!");
                }
            },
            Cancel: function() {
                $("#build_log_header_pattern_dialog").dialog("close");
            }
        }
    });

    // ---------------------- jQuery for configuring the dialog to check the log header pattern ----------------------
    $("#check_log_header_pattern_dialog").dialog({
        autoOpen: false,
        height: 300,
        width: 900,
        modal: true
    });

    // ---------------------- handling the change event on the event type source drop-down ----------------------
    $("#id_event_source_type").change(function() {
        if ($( "#id_event_source_type option:selected" ).text() != 'Reaction Worker') {
            $("#div_log_path").addClass("make_it_disappear");
            $("#div_host").addClass("make_it_disappear");
            $("#d_advanced_settings").addClass("make_it_disappear");
        } else {
            $("#div_log_path").removeClass("make_it_disappear");
            $("#div_host").removeClass("make_it_disappear");
            $("#d_advanced_settings").removeClass("make_it_disappear");
        }
    });

    // ---------------------- click event on the 'Check' button ----------------------
    $("#b_check_pattern").on("click", function(e) {
        if (eventSourceId != null) {
            call_check_header_pattern(eventSourceId, $("#i_check_pattern").val());
        }
        else {
            alert("Please save the event source first!");
        }
    });

    // ---------------------- jQuery for opening the dialog to build the log header pattern ----------------------
    $("#b_open_build_pattern").click(function(e) {
        $("#build_log_header_pattern_dialog").dialog("open");
    });

    // ---------------------- jQuery for opening the dialog to check the log header pattern ----------------------
    $("#b_open_check_pattern").click(function(e) {
        $("#check_log_header_pattern_dialog").dialog("open");
    });

    $("#ta_log").click(function(e) {
        var ta = document.getElementById("ta_log");
        var endPos = getPositions(ta).end;
        var startPos = getStartPosition(ta, endPos);
        selectTextInTa(ta, startPos, endPos);
    });

    $("#b_p_date, #b_p_loglevel, #b_p_component, #b_p_errorcode, #b_p_class, #b_p_unknown").click(function(e) {
        // modifying the log header pattern input field on the 2nd tab by replacing the value with the pattern (eg. [~LOGLEVEL])
        var targetId = e.target.id;
        var i = document.getElementById("i_log_header");
        var p = getPositions( i );
        // disable the button after clicking except the UNKNOWN
        if (targetId != 'b_p_unknown') {
            $("#"+targetId).attr("disabled", true);
        }
        if (p.end > p.start) {
            var log_header = $("#i_log_header").text();
            $("#i_log_header").html( getLogHeaderPatternType(log_header, p.start, p.end, targetId) );
        }
    });

    $("#b_to_sec_accord").on("click", function(e) {
        var ta = document.getElementById("ta_log");
        var p = getPositions(ta);
        if (p.end > p.start && ta.value.substr(p.start, p.end - p.start).indexOf("\n") != -1) {
            alert("The selected text mustn't contain line break!");
        } else {
            $("#i_log_header").text( ta.value.substr(p.start, p.end - p.start) );
            accordion_log_header_pattern.accordion('option', 'active', 1);
        }
    });

    $("#b_to_first_accord").on("click", function(e) {
        $("#i_log_header").val("");
        positions = [];
        $("#b_p_date, #b_p_loglevel, #b_p_component, #b_p_errorcode, #b_p_class, #b_p_unknown").attr("disabled", false);
        accordion_log_header_pattern.accordion('option', 'active', 0);
    });
}

// ---------------------- checking the maintenance window fields before submit the form ----------------------
$("#f_event_source_update").submit(function() {
    return check_maintenance_window("monday") && check_maintenance_window("tuesday") && check_maintenance_window("wednesday")
        && check_maintenance_window("thursday") && check_maintenance_window("friday") && check_maintenance_window("saturday")
        && check_maintenance_window("sunday");
});

function check_maintenance_window(day) {
    var regexpr = /^([0-2][0-9]:[0-5][0-9]-[0-2][0-9]:[0-5][0-9])(, [ ]*[0-2][0-9]:[0-5][0-9]-[0-2][0-9]:[0-5][0-9])*$/g;
    if ($("#i_maint_w_"+day).val()) {
        var n = $("#i_maint_w_"+day).val().search(regexpr);
        if (n != 0) {
            $("#l_maint_w_"+day+"_error").html("Warning! The time range is invalid!");
            return false;
        } else {
            $("#l_maint_w_"+day+"_error").html("");
        }
    }
    return true;
}

function disable_log_header_pattern(obj) {
    if (obj.is(':checked')) {
        $("#id_log_header_pattern").removeAttr("disabled");
        $("#b_open_build_pattern").removeAttr("disabled");
        $("#b_open_check_pattern").removeAttr("disabled");
    } else {
        $("#id_log_header_pattern").attr('disabled', true);
        $("#b_open_build_pattern").attr('disabled', true);
        $("#b_open_check_pattern").attr('disabled', true);
    }
}

// get the start and the end positions of the selected text in input field / textarea
function getPositions(target) {
    var p = {start: 0, end:0};
    p.end = 0;
    var doc = target.ownerDocument || target.document;
    var win = doc.defaultView || doc.parentWindow;
    // IE support
    if (document.selection) {
        target.focus();
        var bookmark = document.selection.createRange().getBookmark();
        var sel = target.createTextRange();
        var bfr = sel.duplicate();
        sel.moveToBookmark(bookmark);
        bfr.setEndPoint("EndToStart", sel);
        p.start = bfr.text.length;
        p.end = s.start + sel.text.length;
    }
    // Firefox (and other) support
    else if (typeof target.selectionStart == "number" && typeof target.selectionEnd == "number") {
        p.start = target.selectionStart;
        p.end = target.selectionEnd;
    }
    else if (typeof win.getSelection != "undefined") {
        var sel;
        sel = win.getSelection();
        if (sel.rangeCount > 0) {
            var range = win.getSelection().getRangeAt(0);
            var preCaretRange = range.cloneRange();
            preCaretRange.selectNodeContents(target);
            preCaretRange.setEnd(range.startContainer, range.startOffset);
            p.start = preCaretRange.toString().length;
            preCaretRange.setEnd(range.endContainer, range.endOffset);
            p.end = preCaretRange.toString().length;
        }
    }
    return p;
}

function getStartPosition(ta, endPos) {
    var wholeTillEnd = ta.value.substr(0, endPos);
    var lastIndex = wholeTillEnd.lastIndexOf('\n');
    return lastIndex == -1 ? 0 : lastIndex+1;
}

function selectTextInTa(ta, startPos, endPos) {
    ta.focus();
    if (typeof ta.selectionStart != "undefined") {
        ta.selectionStart = startPos;
        ta.selectionEnd = endPos;
    } else if (document.selection && document.selection.createRange) {
        // IE branch
        ta.select();
        var range = document.selection.createRange();
        range.collapse(true);
        range.moveEnd("character", endPos);
        range.moveStart("character", startPos);
        range.select();
    }
}

function getLogHeaderPatternType(log_header_text, start, end, targetId) {
    if (targetId == 'b_p_date') {
        if ( !$("#i_date_pattern").val() ) {
            alert("The date format textbox cannot be empty!");
            $("#"+targetId).attr("disabled", false);
            return log_header_text;
        }
    } if (targetId == 'b_p_unknown') {
        if ( !$("#i_unknown_acc_chars").val() ) {
            alert("The accepted characters textbox cannot be empty!");
            return log_header_text;
        }
    }
    return buildHtml(start, end, log_header_text, targetId);
}

function buildHtml(start, end, log_header_text, targetId) {
    var date_pattern = $("#i_date_pattern").val();
    var accepted_chars = $("#i_unknown_acc_chars").val();

    if (targetId == 'b_p_date') {
        var new_log_header_text = log_header_text.substr(0, start) + '[~' + targetId.substr(4).toUpperCase() + ':' + date_pattern + ']' + log_header_text.substr(end);
    } else if (targetId == 'b_p_unknown') {
        var new_log_header_text = log_header_text.substr(0, start) + '[~' + targetId.substr(4).toUpperCase() + ':' + accepted_chars + ']' + log_header_text.substr(end);
    } else {
        var new_log_header_text = log_header_text.substr(0, start) + '[~' + targetId.substr(4).toUpperCase() + ']' + log_header_text.substr(end);
    }
    // escape the < and > characters
    new_log_header_text = new_log_header_text.replace(/<|>/ig,function(m){
    return '&'+(m=='>'?'g':'l')+'t;';
    })
    // adding the <kbd> to highlight
    //new_log_header_text = new_log_header_text.replace(/\[~UNKNOWN:[^\]]*\]/g, replaceIfNotPrecededBy('<kbd>', '<kbd>[~UNKNOWN:' + accepted_chars + ']</kbd>'))
    new_log_header_text = new_log_header_text.replace(/\[~LOGLEVEL\]/g, '<kbd>[~LOGLEVEL]</kbd>')
                                             .replace(/\[~DATE:[^\]]*\]/i, '<kbd>' + '[~DATE:' + date_pattern + ']' + '</kbd>');
    var pattern = /\[~UNKNOWN:[^\]]*\]/g;
    while (matcher = pattern.exec(new_log_header_text)) {
        var start = matcher.index;
        var end = pattern.lastIndex;
        new_log_header_text = new_log_header_text.substr(0, start) + '<kbd>' + new_log_header_text.substr(start, end-start) + '</kbd>' + new_log_header_text.substr(end);
    }

    return new_log_header_text;
}

// http://stackoverflow.com/questions/18884262/regular-expression-match-string-not-preceded-by-another-string-javascript
var replaceIfNotPrecededBy = function(notPrecededBy, replacement) {
    return function(match) {
        return match.slice(0, notPrecededBy.length) === notPrecededBy ? match : replacement;
    }
};

function loadIdentifierTable(identifiers, path_to_identifiers) {
    $("#table_identifiers").find("tr:gt(0)").remove();
    if ($("#id_event_source_type option:selected").text() == "Reaction Worker" || $("#id_event_source_type").val() === "") {
        $("#div_identifiers").hide();
    } else {
        let is_no_identifiers_displayed = true;
        if (path_to_identifiers.length) {
            $("#div_identifiers").show();
            numRows = 1;
            for(let i=0; i<path_to_identifiers.length; i++) {
                if (path_to_identifiers[i].output == 'EVENT_SOURCE') {
                    addRowToIdentifiers(path_to_identifiers[i], identifiers, false);
                    is_no_identifiers_displayed = false
                }
            }
        }
        if (is_no_identifiers_displayed) {
            $("#div_identifiers").hide();
        }
    }
}

function rebuildIdentifiers() {
    $("[id^='identifiers_name_']").remove();
    $("[id^='identifiers_value_']").remove();
    if ($("#id_event_source_type option:selected").text() == "Reaction Worker" || $("#id_event_source_type").val() === "") {
        $("#div_identifiers").hide();
    } else {
        $("#div_identifiers").show();
        let idEventSourceType = $("#id_event_source_type").val();
        let loadingDiv = $("#loadingDiv");

        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        $.ajaxSetup({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });
        // calling the server to get event source type
        loadingDiv.show();
        let url = urlForRebuildIdentifiers.replace("43957793052297457", idEventSourceType);
        $.ajax({
            type: 'GET',
            url: url = url,
            timeout: 20000,
            accepts: {
                text: 'application/json'
            },
            success: function(response) {
                // hiding the loading DIV
                loadingDiv.hide();
                loadIdentifierTable({}, JSON.parse(response.path_to_identifiers));
            },
            error: function(request, status, error) {
                // hiding the loading DIV
                loadingDiv.hide();
                handleError(request, status, error);
            }
        });
    }
}
