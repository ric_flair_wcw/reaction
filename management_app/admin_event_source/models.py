from django.db import models
import json

class ParentModel(models.Model):
	LOG_LEVELS = (
		('TRACE', 'TRACE'),
		('DEBUG', 'DEBUG'),
		('INFO', 'INFO'),
		('WARN', 'WARN'),
		('ERROR', 'ERROR'),
		('FATAL', 'FATAL'),
	)
	EVENT_SOURCE_HIERARCHY_TYPES = (
		('GROUP', 'Group'),
		('ITEM', 'Item'),
	)
	INITIATORS = (
		('BY_LOG', 'BY_LOG'),
		('MANUALLY', 'MANUALLY'),
		('BY_SCHEDULER', 'BY_SCHEDULER'),
	)
	INTERNAL_TASKS = (
		('IF_ELSE', 'If-else internal task'),
		('COMMAND_BY_WORKER', 'Command executed by Worker task'),
		('COMMAND_USING_SSH', 'Command executed using SSH task'),
		('EMAIL_SENDING', 'Email sending task'),
		('FAILURE', 'Failure task'),
		('LOOP', 'Loop task'),
	)
	SSH_AUTH_TYPE = (
		('USER_PASSWORD', 'User + password authentication'),
		('SSH_KEY', 'SSH key'),
	)
	OPERATIONS = (
		('GREATER_THAN', '>'),
		('GREATER_THAN_OR_EQUAL_TO' , '>='),
		('EQUAL_TO', '='),
		('LESS_THAN_OR_EQUAL_TO' ,'<='),
		('LESS_THAN' , '<'),
	)
	EVENT_STATUSES = (
		('CONFIRMATION_NEEDED', 'CONFIRMATION_NEEDED'),
		('CONFIRMED', 'CONFIRMED'),
		('CONFIRMED_AND_FORCED_START', 'CONFIRMED_AND_FORCED_START'),
		('REJECTED', 'REJECTED'),
		('SCHEDULED', 'SCHEDULED'),
		('WAITING_FOR_OTHERS', 'WAITING_FOR_OTHERS'),
		('IGNORED', 'IGNORED'),
		('STARTED', 'STARTED'),
		('WAITING_FOR_EXECUTION', 'WAITING_FOR_EXECUTION'),
		('EXECUTION_STARTED', 'EXECUTION_STARTED'),
		('INTERNAL_ERROR', 'INTERNAL_ERROR'),
		('FAILED', 'FAILED'),
		('FINISHED', 'FINISHED'),
		('CANCELLED', 'CANCELLED')
	)
	EXECUTION_FLOW_STATUSES = (
		('INVALID', 'INVALID'),
		('VALID', 'VALID'),
		('FROZEN', 'FROZEN')
	)

	def __str__(self):
		field_values = []
		for field in self._meta.fields:
			field_values.append(str(field.name) + ":'" + str(getattr(self, str(field.name), '')) + "'" )
		return self.__class__.__name__ + '{' + ', '.join(field_values) + '}'

	class Meta:
		abstract = True


# ################################## EventSource ##################################
class EventSource(ParentModel):
	name = models.CharField(max_length=200, blank=False, unique=True)
	host = models.CharField(max_length=200, blank=True, null=True)
	log_path = models.CharField(max_length=2000, blank=True, null=True)
	log_header_pattern = models.CharField(max_length=2000, blank=True, null=True)
	log_header_pattern_enabled = models.BooleanField(default=False)
	description = models.CharField(max_length=2000, blank=True, null=True)
	type = models.CharField(choices=ParentModel.EVENT_SOURCE_HIERARCHY_TYPES, max_length=40)
	log_level = models.CharField(choices=ParentModel.LOG_LEVELS, max_length=40, blank=True, null=True)
	maintenance_window = models.CharField(max_length=2000, blank=True, null=True)
	parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True, related_name="children")
	event_source_type = models.ForeignKey('admin_event_source_type.EventSourceType', on_delete=models.DO_NOTHING, blank=True, null=True)
	identifiers = models.CharField(max_length=3000, blank=True, null=True)
	status = models.CharField(max_length=7, blank=False, default='VALID')

	class Meta:
		db_table = 'EVENT_SOURCE'
		default_permissions = ()

	def __str__(self):
		return "Name: {}\nDescription: {}\nHost: {}\nLog path: {}\nLog header pattern: {}\nType: {}\nLog level: {}\n" \
			   "Maintenance window: {}\nEvent source type: {}\nIdentifiers: {}\n" \
			   "WARNING! Displaying the values of the specific event source only (i.e. not loading them from the parents)!" \
				.format(self.name,
		                ("" if self.description is None else self.description),
		                ("" if self.host is None else self.host),
		                ("" if self.log_path is None else self.log_path),
		                ("" if self.log_header_pattern is None else self.log_header_pattern),
		                self.type,
		                ("" if self.log_level is None else self.log_level),
		                self.format_window(json.loads(self.maintenance_window)) if self.maintenance_window else "",
						("" if self.event_source_type is None else self.event_source_type),
						("" if self.identifiers is None else self.identifiers)
						)
	
	def format_window(selfself, mw):
		f = ''
		if mw:
			days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
			for day in days:
				if mw.get(day):
					f += '\n        ' + day + ': ' + ', '.join(mw.get(day))
		return f