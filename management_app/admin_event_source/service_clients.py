from common.rest_utils import call_rest


def call_check_pattern(pk, log_entry):
    url = '/utils/logheaderpattern/check'
    payload = {'eventSourceId': pk, 'logEntry': log_entry}

    return call_rest(payload, url, 'POST')