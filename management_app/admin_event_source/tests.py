from django.http import HttpResponseRedirect, HttpResponse
from django.http.request import HttpRequest
from django.test import TestCase
import logging

from admin_errordetector.models import ErrorDetector
from admin_execution_flow.models import ExecutionFlow
from admin_event_source.models import EventSource
from admin_event_source.views import delete_event_source_if_possible


class EventSourceTestCase(TestCase):
    logger = logging.getLogger('root')

    @classmethod
    def setUpTestData(cls):
        EventSource.objects.all().delete()
        ErrorDetector.objects.all().delete()
        ExecutionFlow.objects.all().delete()


    def setUp(self):
        pass


    def test_parent_is_in_errordetector__current_is_a_ITEM__cannot_be_deleted(self):
        self.logger.info('************** test_parent_is_in_errordetector__current_is_a_ITEM__cannot_be_deleted **************')
        ef_1 = ExecutionFlow.objects.create(pk=0, name='ef_1')

        s_melius = EventSource.objects.create(pk=0, name='Melius', type='GROUP')
        s_soa_server_1 = EventSource.objects.create(pk=1, name='SOA server 1', type='ITEM', parent=s_melius)

        ed_1 = ErrorDetector.objects.create(pk=0, name='ed_1', event_source=s_melius, execution_flow=ef_1)

        request = HttpRequest()
        response = delete_event_source_if_possible(request, s_soa_server_1.pk)

        self.assertTrue(type(response) == HttpResponse)
        self.assertEquals(200, response.status_code)
        self.assertTrue('The event_source cannot be deleted as one of its parent (&quot;Melius]&quot;) is in an error detector and all the ITEM child event_source of this parent are in the current event_source that is under deletion (the parent event_source that is in error detector would not have any ITEM event_source left after deletion so it should not be in the error detector).' in response.content.decode('utf-8'))
        self.assertTrue('<h4>The operation cannot be executed as it breaks the following rule:</h4>' in response.content.decode('utf-8'))


    def test_parent_is_in_errordetector__current_is_a_GROUP_without_chidren__can_be_deleted(self):
        self.logger.info('************** test_parent_is_in_errordetector__current_is_a_GROUP_without_chidren__can_be_deleted **************')
        ef_1 = ExecutionFlow.objects.create(pk=0, name='ef_1')

        s_melius = EventSource.objects.create(pk=0, name='Melius', type='GROUP')
        s_soa_servers = EventSource.objects.create(pk=1, name='SOA servers', type='GROUP', parent=s_melius)

        ed_1 = ErrorDetector.objects.create(pk=0, name='ed_1', event_source=s_melius, execution_flow=ef_1)

        request = HttpRequest()
        response = delete_event_source_if_possible(request, s_soa_servers.pk)

        self.assertTrue(type(response) == HttpResponseRedirect)
        self.assertEquals(response.url, '/administration/event_source')


    def test_parent_is_in_errordetector__current_is_a_GROUP_with_chidren__can_be_deleted(self):
        self.logger.info('************** test_parent_is_in_errordetector__current_is_a_GROUP_with_chidren__can_be_deleted **************')
        ef_1 = ExecutionFlow.objects.create(pk=0, name='ef_1')

        s_melius = EventSource.objects.create(pk=0, name='Melius', type='GROUP')
        s_soa_servers = EventSource.objects.create(pk=1, name='SOA servers', type='GROUP', parent=s_melius)
        s_soa_servers_east = EventSource.objects.create(pk=2, name='SOA servers EAST', type='GROUP', parent=s_soa_servers)
        s_soa_servers_west = EventSource.objects.create(pk=3, name='SOA servers WEST', type='GROUP', parent=s_soa_servers)
        s_soa_servers_west_0 = EventSource.objects.create(pk=4, name='SOA server WEST 0', type='ITEM', parent=s_soa_servers_west)
        s_adf_servers = EventSource.objects.create(pk=5, name='ADF servers', type='GROUP', parent=s_melius)
        s_adf_server_0 = EventSource.objects.create(pk=6, name='ADF server 0', type='ITEM', parent=s_adf_servers)

        ed_1 = ErrorDetector.objects.create(pk=0, name='ed_1', event_source=s_melius, execution_flow=ef_1)

        request = HttpRequest()
        response = delete_event_source_if_possible(request, s_soa_servers_west.pk)

        self.assertTrue(type(response) == HttpResponseRedirect)
        self.assertEquals(response.url, '/administration/event_source')


    def test_parent_is_in_errordetector__current_is_a_GROUP_with_chidren_but_no_children_elsewhere__cannot_be_deleted(self):
        self.logger.info('************** test_parent_is_in_errordetector__current_is_a_GROUP_with_chidren_but_no_children_elsewhere__cannot_be_deleted **************')
        ef_1 = ExecutionFlow.objects.create(pk=0, name='ef_1')

        s_melius = EventSource.objects.create(pk=0, name='Melius', type='GROUP')
        s_soa_servers = EventSource.objects.create(pk=1, name='SOA servers', type='GROUP', parent=s_melius)
        s_soa_servers_east = EventSource.objects.create(pk=2, name='SOA servers EAST', type='GROUP', parent=s_soa_servers)
        s_soa_servers_west = EventSource.objects.create(pk=3, name='SOA servers WEST', type='GROUP', parent=s_soa_servers)
        s_soa_servers_west_0 = EventSource.objects.create(pk=4, name='SOA server WEST 0', type='ITEM', parent=s_soa_servers_west)
        s_adf_servers = EventSource.objects.create(pk=5, name='ADF servers', type='GROUP', parent=s_melius)

        ed_1 = ErrorDetector.objects.create(pk=0, name='ed_1', event_source=s_melius, execution_flow=ef_1)

        request = HttpRequest()
        response = delete_event_source_if_possible(request, s_soa_servers_west.pk)

        self.assertTrue(type(response) == HttpResponse)
        self.assertEquals(200, response.status_code)
        self.assertTrue('The event_source cannot be deleted as one of its parent (&quot;Melius]&quot;) is in an error detector and all the ITEM child event_source of this parent are in the current event_source that is under deletion (the parent event_source that is in error detector would not have any ITEM event_source left after deletion so it should not be in the error detector).' in response.content.decode('utf-8'))
        self.assertTrue('<h4>The operation cannot be executed as it breaks the following rule:</h4>' in response.content.decode('utf-8'))


    def test_parent_is_not_in_errordetector__can_be_deleted(self):
        self.logger.info('************** test_parent_is_not_in_errordetector__can_be_deleted **************')
        ef_1 = ExecutionFlow.objects.create(pk=0, name='ef_1')

        s_melius = EventSource.objects.create(pk=0, name='Melius', type='GROUP')
        s_soa_servers = EventSource.objects.create(pk=1, name='SOA servers', type='GROUP', parent=s_melius)

        request = HttpRequest()
        response = delete_event_source_if_possible(request, s_soa_servers.pk)

        self.assertTrue(type(response) == HttpResponseRedirect)
        self.assertEquals(response.url, '/administration/event_source')
