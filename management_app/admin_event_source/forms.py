from django import forms
import json
from .models import EventSource
from common.utils import split_and_strip
from django.db.models import Q
from django.shortcuts import get_object_or_404
from admin_event_source_type.models import EventSourceType
import re


class EventSourceForm(forms.ModelForm):
    error_css_class = 'model_form_error'
    maintenance_window = forms.CharField(widget=forms.Textarea, required=False)
    log_path = forms.CharField(widget=forms.TextInput, required=False)
    log_header_pattern = forms.CharField(widget=forms.Textarea(attrs={"rows": 3}), required=False)
    description = forms.CharField(widget=forms.Textarea(attrs={"rows": 3}), required=False)

    class Meta:
        model = EventSource
        fields = ['name', 'host', 'log_path', 'log_header_pattern', 'log_header_pattern_enabled', 'description', 'type', 'log_level', 'maintenance_window',
                  'parent', 'event_source_type', 'identifiers']

    def __init__(self, *args, **kwargs):
        super(EventSourceForm, self).__init__(*args, **kwargs)
        self.data = self.data.copy()  # IMPORTANT, self.data is immutable
        self.fields['parent'].widget.attrs['class'] = 'form-control'
        self.fields['parent'].label_from_instance = self.label_from_instance
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['host'].widget.attrs['class'] = 'form-control'
        self.fields['log_path'].widget.attrs['class'] = 'form-control'
        self.fields['log_header_pattern'].widget.attrs['class'] = 'form-control model_form_error'
        self.fields['description'].widget.attrs['class'] = 'form-control'
        self.fields['description'].widget.attrs['style'] = 'resize: vertical;'
        self.fields['type'].widget.attrs['class'] = 'form-control'
        self.fields['log_level'].widget.attrs['class'] = 'form-control'
        self.fields['maintenance_window'].widget.attrs['class'] = 'form-control'
        self.fields['maintenance_window'].widget.attrs['style'] = 'height: 100px;'
        self.fields['event_source_type'].widget.attrs['class'] = 'form-control'
        self.fields['event_source_type'].widget.attrs['onchange'] = 'rebuildIdentifiers()'


    def clean_maintenance_window(self):
        maintenance_window = {}
        if len(self.data.get('maint_w_monday')) > 0:
            maintenance_window['Mon'] = split_and_strip(self.data.get('maint_w_monday'))
        if len(self.data.get('maint_w_tuesday')) > 0:
            maintenance_window['Tue'] = split_and_strip(self.data.get('maint_w_tuesday'))
        if len(self.data.get('maint_w_wednesday')) > 0:
            maintenance_window['Wed'] = split_and_strip(self.data.get('maint_w_wednesday'))
        if len(self.data.get('maint_w_thursday')) > 0:
            maintenance_window['Thu'] = split_and_strip(self.data.get('maint_w_thursday'))
        if len(self.data.get('maint_w_friday')) > 0:
            maintenance_window['Fri'] = split_and_strip(self.data.get('maint_w_friday'))
        if len(self.data.get('maint_w_saturday')) > 0:
            maintenance_window['Sat'] = split_and_strip(self.data.get('maint_w_saturday'))
        if len(self.data.get('maint_w_sunday')) > 0:
            maintenance_window['Sun'] = split_and_strip(self.data.get('maint_w_sunday'))
        return json.dumps(maintenance_window)

    def clean(self):
        cleaned_data = super(EventSourceForm, self).clean()
        if not self.is_valid_parent(cleaned_data['parent'], self.instance.id):
            self.add_error('parent', 'The selected event source cannot be the parent as it would cause a circular reference!')
        if cleaned_data.get('type') == 'ITEM':
            fields = ['host', 'log_path', 'log_level', 'log_header_pattern', 'log_header_pattern_enabled', 'event_source_type_id']
            if not cleaned_data.get('log_header_pattern_enabled'):
                fields.remove('log_level')
            if cleaned_data.get('parent'):
                cleaned_data['parent_id'] = cleaned_data['parent'].id
            if cleaned_data.get('event_source_type'):
                cleaned_data['event_source_type_id'] = cleaned_data['event_source_type'].id
            values = self.check_hier(cleaned_data, fields, {})
            if not values.get('event_source_type_id'):
                self.add_error('event_source_type', 'Not having value on this record or on any of its parents!')
            else:
                event_source_type = get_object_or_404(EventSourceType, pk=values.get('event_source_type_id'))
                if event_source_type.code == 'REACTION-WORKER':
                    # check if the log header pattern is enabled but no pattern is defined
                    if cleaned_data['log_header_pattern_enabled'] and not values.get('log_header_pattern'):
                        self.add_error('log_header_pattern', 'The log header pattern is enabled but no pattern is defined on this event source or on any its parents')
                    for field in ['host', 'log_path', 'log_level']:
                        if not values.get(field):
                            # if the log_header_pattern_enabled is false then no need to check if the log_level is defined as it won't be used anyway
                            if not field == 'log_level':
                                self.add_error(field, 'Not having value on this record or on any of its parents!')
                            else:
                                if cleaned_data['log_header_pattern_enabled']:
                                    self.add_error(field, 'Not having value on this record or on any of its parents!')

    def check_hier(self, event_source, fields, values):
        # first add the new dictionary (just mined from the current event_source) and add the 'values' parameter (mined previously) => the parent value won't overwrite its child
        values = {**dict((x, event_source.get(x)) for x in fields if event_source.get(x) is not None and str(event_source.get(x))), **values}
        # if the values of all the fields are found then return them
        if len(values) == len(fields):
            return values
        else:
            if event_source.get('parent_id'):
                return self.check_hier(get_object_or_404(EventSource, pk=event_source.get('parent_id')).__dict__, fields, values)
            else:
                return values

    def is_valid_parent(self, parent, current_event_source_id):
        parents = [current_event_source_id] if current_event_source_id else []
        if not parent:
            return True
        while True:
            if parent.id in parents:
                return False
            if not parent.parent:
                return True
            parents.append(parent.id)
            parent = parent.parent

    @staticmethod
    def label_from_instance(obj):
        return "{} [{}]".format(obj.name, obj.type)