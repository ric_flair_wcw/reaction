import datetime
from django.views.decorators.csrf import csrf_protect
from django.shortcuts import render
from admin_execution_flow.models import ExecutionFlow
from common.utils import *
from monitoring.models import Event
from django.db.models import Count, DateTimeField
from django.db.models.functions import Trunc
from dateutil.relativedelta import relativedelta
from admin_event_source.views import *

logger = logging.getLogger('root')


@login_required
@permission_required('auth.can_use_statistics', raise_exception=True)
@csrf_protect
def index_by_flow(request):
    logger.info('Statistics - incidents by flow is called')

    context = index(request, 'execution flow')
    # build the access_groups filter
    access_groups_conditions = build_filter_access_groups(request.user.profile.access_groups, False, '')
    # building the execution flow list
    execution_flows = ExecutionFlow.objects.filter(access_groups_conditions, Q(status='VALID') | Q(status='FROZEN')).order_by('name')
    context['resources'] = execution_flows
    # building the initiator list
    context['initiators'] = ['BY_LOG', 'BY_SCHEDULER', 'MANUALLY', '**NOT SPECIFIED**']

    return render(request, 'statistics/index.html', context)


@login_required
@permission_required('auth.can_use_statistics', raise_exception=True)
@csrf_protect
def index_by_event_source(request):
    logger.info('Statistics - incidents by event source is called')

    context = index(request, 'event_source')
    # building the event_source list
    event_sources = build_complemented_event_source_names()
    context['resources'] = event_sources

    return render(request, 'statistics/index.html', context)


def index(request, resource_name):
    graph_label = []
    graph_data = []
    date_format = ''
    context = {'resource_name': resource_name}
    if request.method == 'POST':
        # getting the parameters
        start_date = check_date(request.POST.get('start_date'))
        end_date = check_date(request.POST.get('end_date'))
        initiator = request.POST.get('initiator')
        resource_id = request.POST.get('resource_id')
        time_scale = request.POST.get('time_scale')
        logger.debug('initiator={}, resource id={}, start_date={}, end_date={}'.format(initiator, resource_id, start_date, end_date))
        if start_date >= end_date:
            raise ValueError("The start date has to be smaller than the end date!")
        # executing the query
        filters = {'start_date__gte': start_date,
                   'end_date__lte': end_date}
        if resource_name == 'execution flow':
            filters['execution_flow_id'] = resource_id
            if initiator != '**NOT SPECIFIED**':
                filters['initiated_by'] = initiator
        else:
            filters['event_source_id'] = resource_id
        events = Event.objects.select_related('error_detector') \
                                 .filter(**filters) \
                                 .annotate(perc = Trunc('start_date', time_scale, output_field=DateTimeField())) \
                                 .values('perc', 'status')\
                                 .annotate(cnt=Count('id'))\
                                 .order_by('perc', 'status')

        # producing a date array that contains all the datetime from start_date to end_date in the specific detail level (time_scale)
        date_array, date_format = get_date_data(end_date, start_date, time_scale)
        graph_label = [d.timestamp() * 1000 for d in date_array]
        borderColor = {'CANCELLED': 'black', 'CONFIRMATION_NEEDED': 'yellow','FAILED': 'red','FINISHED': 'green','IGNORED': 'gray','REJECTED': 'purple','SCHEDULED': 'brown','STARTED': '#999966','WAITING_FOR_OTHERS': '#cc6699'}
        for st in sorted(request.POST.getlist('selected_status')):
            graph_data.append({'label': st,
                               'data': [get_cnt(events, d, st) for d in date_array],
                               'borderColor': borderColor[st]})
        context['start_date'] = request.POST.get('start_date')
        context['end_date'] = request.POST.get('end_date')
        context['selected_resource_id'] = request.POST.get('resource_id')
        context['initiator'] = request.POST.get('initiator')
        context['selected_status'] = request.POST.getlist('selected_status')
        context['selected_time_scale'] = request.POST.get('time_scale')
    context['graph_label'] = graph_label
    context['graph_data'] = graph_data
    context['graph_date_format'] = date_format
    return context


def get_date_data(end_date, start_date, time_scale):
    difference = end_date - start_date
    if time_scale == 'minute':
        date_array = [end_date.replace(second=0, microsecond=0) - datetime.timedelta(minutes=x) for x in list(range(0, int(difference.total_seconds() / 60)+1 ))]
        date_format = 'YYYY.MM.D hh:mm'
    elif time_scale == 'hour':
        date_array = [end_date.replace(second=0, microsecond=0, minute=0) - datetime.timedelta(hours=x) for x in list(range(0, int(difference.total_seconds() / (60*60))+1 ))]
        date_format = 'YYYY.MM.D ha'
    elif time_scale == 'day':
        date_array = [end_date.replace(second=0, microsecond=0, minute=0, hour=0) - datetime.timedelta(days=x) for x in list(range(0, int(difference.total_seconds() / (24*60*60))+1 ))]
        date_format = 'YYYY.MM.D'
    elif time_scale == 'month':
        difference = relativedelta(end_date, start_date)
        date_array = [end_date.replace(second=0, microsecond=0, minute=0, hour=0, day=1) - relativedelta(months=x) for x in list(range(0, difference.months ))]
        date_format = 'YYYY MMM'
    return date_array, date_format


def check_date(datetime_text):
    # sample: 2017.03.16 14:59:01
    try:
        return datetime.datetime.strptime(datetime_text, '%Y.%m.%d %H:%M:%S')
    except ValueError:
        raise ValueError("Incorrect data format, should be YYYY.mm.dd HH:MM:SS (e.g. 2017.03.16 17:00:41) !")


def get_cnt(events, d, status):
    for event in events:
        if event['perc'].timestamp() == d.timestamp() and event['status'] == status:
            return event['cnt']
    return 0
