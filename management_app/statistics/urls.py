from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^statistics/by_flow$',                views.index_by_flow,             name='stat_by_flow'),
	url(r'^statistics/by_event_source$',        views.index_by_event_source,     name='stat_by_event_source'),
]