import calendar
import logging
from datetime import datetime, timedelta
from functools import reduce

from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q, Count, DateTimeField
from django.db.models.functions import Trunc
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from admin_event_source.models import EventSource
from admin_execution_flow.models import ExecutionFlow
from common.rest_utils import login_required_for_rest
from monitoring.models import Event, EventLife

logger = logging.getLogger('root')


@login_required
@permission_required('auth.can_use_monitoring', raise_exception=True)
def index(request):
    logger.info('Displaying the dashboard. {}'.format(request))
    context = {}
    return render(request, 'dashboard/index.html', context)


# -------------------------------------------------- for REST --------------------------------------------------

@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['GET'])
def load_initial_data(request):
    logger.info('Loading the initial data for dashboard.')

    return Response({'autoRefresh': request.user.profile.dashboard_autorefresh,
                     'dateperiod': {
                         'p1': request.user.profile.dashboard_p1_dateperiod,
                         'p2': request.user.profile.dashboard_p2_dateperiod,
                         'p3': request.user.profile.dashboard_p3_dateperiod,
                         'p4': request.user.profile.dashboard_p4_dateperiod,
                     }}, status = status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['POST'])
def save_dateperiod(request):
    logger.info('Saving the time period of dashboard. panel={}, date period={}', request.data['panel'], request.data['dateperiod'])
    if request.data['panel'] and request.data['dateperiod']:
        profile = request.user.profile
        if request.data['panel'] == 'p1':
            profile.dashboard_p1_dateperiod = request.data['dateperiod']
        elif request.data['panel'] == 'p2':
            profile.dashboard_p2_dateperiod = request.data['dateperiod']
        elif request.data['panel'] == 'p3':
            profile.dashboard_p3_dateperiod = request.data['dateperiod']
        elif request.data['panel'] == 'p4':
            profile.dashboard_p4_dateperiod = request.data['dateperiod']
        else:
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='The panel attribute has and invalid value!')
        profile.save()
        return Response(status = status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY , data='The panel and/or dateperiod attributes are missing!')


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['GET'])
def feed_top_markers(request):
    now = datetime.now()
    date_filter = {'start_date__gte': now - timedelta(hours=1),
                   'start_date__lt': now}

    events = Event.objects.filter(**{'status__in': ['FAILED','FINISHED','IGNORED','STARTED', 'CONFIRMATION_NEEDED']}, **date_filter) \
                          .values('status') \
                          .annotate(cnt = Count('status')) \
                          .values('status', 'cnt') \
                          .order_by('status')

    result = {'FAILED': 0,
              'FINISHED': 0,
              'IGNORED': 0,
              'STARTED': 0,
              'CONFIRMATION_NEEDED': 0}
    for event in events:
        result[event['status']] = event['cnt']
    return Response(result, status=status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['GET'])
def feed_p2(request):
    logger.info('Feeding the panel2 (information about selected execution flows) of dashboard.')

    # query the dateperiod of logged in user
    date_filter, detail, date_array, date_format = process_date_period(request.user.profile.dashboard_p2_dateperiod)

    flows = [request.user.profile.dashboard_p2_flow0, request.user.profile.dashboard_p2_flow1,
             request.user.profile.dashboard_p2_flow2, request.user.profile.dashboard_p2_flow3]

    result = build_data_for_feeding(date_array, date_filter, date_format, detail, flows, 'execution_flow')
    return Response(result, status = status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['POST'])
def manage_flow_in_square(request):
    logger.info('Managing the selected flow on dashboard. method={}, data={}', request.method, request.data)

    profile = request.user.profile
    if request.method == 'POST':
        if request.data['squareNumber'] and request.data['executionFlowId']:
            executionFlow = get_object_or_404(ExecutionFlow, id=int(request.data['executionFlowId']))
            return set_profile_dashboard_flow(profile, executionFlow, request.data['squareNumber'])
        else:
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='The squareNumber and/or executionFlowId attributes are missing!')
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['DELETE'])
def remove_flow_from_square(request, squareNr):
    logger.info('Removing the flow from dashboard. squareNumber={}', squareNr)

    profile = request.user.profile
    return set_profile_dashboard_flow(profile, None, squareNr)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['GET'])
def feed_p3(request):
    logger.info('Feeding the panel3 (information about selected event sources) of dashboard.')

    # query the dateperiod of logged in user
    date_filter, detail, date_array, date_format = process_date_period(request.user.profile.dashboard_p3_dateperiod)

    event_sources = [request.user.profile.dashboard_p2_event_source0, request.user.profile.dashboard_p2_event_source1,
                     request.user.profile.dashboard_p2_event_source2, request.user.profile.dashboard_p2_event_source3]

    result = build_data_for_feeding(date_array, date_filter, date_format, detail, event_sources, 'event_source')
    return Response(result, status=status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['POST'])
def manage_event_source_in_square(request):
    logger.info('Managing the selected event source on dashboard. method={}, data={}', request.method, request.data)

    profile = request.user.profile
    if request.method == 'POST':
        if request.data['squareNumber'] and request.data['eventSourceId']:
            event_source = get_object_or_404(EventSource, id=int(request.data['eventSourceId']))
            return set_profile_dashboard_event_source(profile, event_source, request.data['squareNumber'])
        else:
            return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='The squareNumber and/or eventSourceId attributes are missing!')
    else:
        return Response(status=status.HTTP_405_METHOD_NOT_ALLOWED)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['DELETE'])
def remove_event_source_from_square(request, squareNr):
    logger.info('Removing the event source from dashboard. squareNumber={}', squareNr)

    profile = request.user.profile
    return set_profile_dashboard_event_source(profile, None, squareNr)


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['POST'])
def save_auto_refresh(request):
    logger.info('Saving the auto refresh value. method={}, data={}', request.method, request.data)

    if request.data['autoRefresh']:
        profile = request.user.profile
        profile.dashboard_autorefresh = request.data['autoRefresh']
        profile.save()
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='The autoRefresh attribute is missing!')


@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
@api_view(['GET'])
def feed_p1(request):
    logger.info('Feeding the panel1 (general info) of dashboard.')

    # query the dateperiod of logged in user
    date_filter, detail, date_array, date_format = process_date_period(request.user.profile.dashboard_p1_dateperiod)

    # query by host
    flow_date_filter = {'event__start_date__gte': date_filter['start_date__gte'],
                        'event__start_date__lt': date_filter['start_date__lt']}
    by_host = EventLife.objects.filter(**flow_date_filter) \
                               .exclude(task__isnull=True)\
                               .values('task__host') \
                               .annotate(cnt = Count('event', distinct=True)) \
                               .values('task__host', 'cnt') \
                               .order_by('task__host')
    # calculating the sum
    sum = reduce(lambda x, y: x + y['cnt'], by_host, 0)
    # looping through all the events and put the host string and the number of events (and the percentage) next to each other in an array[2]
    host_cnt = [[e['task__host'], str(e['cnt']) + '   (' + "{0:.2f}".format(e['cnt'] / sum * 100) + ' %)'] for e in by_host]

    result = {
        'status': get_panel1_data(date_filter, 'status'),
        'initiated_by': get_panel1_data(date_filter, 'initiated_by'),
        'host': host_cnt,
    }

    return Response(result, status = status.HTTP_200_OK)


# -------------------------------------------------- others --------------------------------------------------

def build_data_for_feeding(date_array, date_filter, date_format, detail, selected_resources, resource_name):
    # loop through the event_sources and query the events
    # https://docs.djangoproject.com/en/2.0/ref/models/database-functions/
    if resource_name == 'execution_flow':
        events = Event.objects.filter(**{resource_name + '__in': selected_resources}, **date_filter) \
                              .annotate(date=Trunc('start_date', detail, output_field=DateTimeField())) \
                              .values(resource_name, 'date') \
                              .annotate(cnt=Count(resource_name)) \
                              .values(resource_name, 'date', 'status', 'cnt') \
                              .order_by(resource_name, 'date', 'status')
    resource_data = []
    for selected_resource in selected_resources:
        if selected_resource:
            if resource_name == 'event_source':
                event_source_ids = []
                get_event_source_ids(get_object_or_404(EventSource, id=selected_resource.id), event_source_ids)
                events = Event.objects.filter(**{resource_name + '__in': event_source_ids}, **date_filter) \
                                      .annotate(date=Trunc('start_date', detail, output_field=DateTimeField())) \
                                      .values(resource_name, 'date') \
                                      .annotate(cnt=Count(resource_name)) \
                                      .values(resource_name, 'date', 'status', 'cnt') \
                                      .order_by(resource_name, 'date', 'status')
            # looping through all the dates in the dateperiod and if there is value in the query then put it there
            graph_FAILED = [[d.timestamp() * 1000, get_cnt(events, d, selected_resource, 'FAILED', resource_name)] for d in date_array]
            graph_FINISHED = [[d.timestamp() * 1000, get_cnt(events, d, selected_resource, 'FINISHED', resource_name)] for d in date_array]
            # calculating the sum
            cnt = reduce(lambda x, y: x + y['cnt'], filter(lambda y: (resource_name == 'event_source' or y[resource_name] == selected_resource.id), events), 0)
            # calculating the sum of IGNORED records
            cnt_IGNORED = reduce(lambda x, y: x + y['cnt'],
                                 filter(lambda y: (resource_name == 'event_source' or y[resource_name] == selected_resource.id) and y['status'] == 'IGNORED', events),
                                 0)
            # calculating the sum of REJECTED records
            cnt_REJECTED = reduce(lambda x, y: x + y['cnt'],
                                  filter(lambda y: (resource_name == 'event_source' or y[resource_name] == selected_resource.id) and y['status'] == 'REJECTED', events),
                                  0)
            # calculating the sum of FINISHED records
            cnt_FINISHED = reduce(lambda x, y: x + y['cnt'],
                                  filter(lambda y: (resource_name == 'event_source' or y[resource_name] == selected_resource.id) and y['status'] == 'FINISHED', events),
                                  0)
            # calculating the sum of FAILED records
            cnt_FAILED = reduce(lambda x, y: x + y['cnt'],
                                filter(lambda y: (resource_name == 'event_source' or y[resource_name] == selected_resource.id) and y['status'] == 'FAILED', events),
                                0)

            resource_data.append({'name': selected_resource.name,
                                  'count': cnt,
                                  'count_IGNORED': cnt_IGNORED,
                                  'count_REJECTED': cnt_REJECTED,
                                  'count_FINISHED': cnt_FINISHED,
                                  'count_FAILED': cnt_FAILED,
                                  'graph_FAILED': graph_FAILED,
                                  'graph_FINISHED': graph_FINISHED})
        else:
            resource_data.append({})
    result = {
        resource_name + 's': resource_data,
        'date_format': date_format,
    }
    return result


def get_panel1_data(date_filter, group_by):
    # query the events
    # https://docs.djangoproject.com/en/2.0/ref/models/database-functions/
    events = Event.objects.filter(**date_filter) \
                          .values(group_by) \
                          .annotate(cnt = Count(group_by)) \
                          .values(group_by, 'cnt') \
                          .order_by(group_by)
    # calculating the sum
    sum = reduce(lambda x, y: x + y['cnt'], events, 0)
    # looping through all the events and put the status string and the number of events (and the percentage) next to each other in an array[2]
    return [[e[group_by], str(e['cnt']) + '   (' + "{0:.2f}".format(e['cnt'] / sum * 100) + ' %)'] for e in events]


def get_cnt(events, d, resource, status, resource_name):
    for event in events:
        if event['date'].timestamp() == d.timestamp() and \
           (resource_name == 'event_source' or event[resource_name] == resource.id) and \
           event['status'] == status:
            return event['cnt']
    return 0

# will return
# - date_filter: contains the filter conditions for the query
# - date_array: contains all the dates in the date period in the specific details (e.g. STARTED_LAST_10MINS -> 10 dates from now increasing 1 minute)
# - detail: the value for the Trunc() function in the query (e.g. STARTED_LAST_10MINS: group by minute, STARTED_TODAY: by hour, etc)
def process_date_period(dateperiod_text):
    date_filter = {}
    now = datetime.now()
    end_date = now
    if not dateperiod_text or dateperiod_text == 'STARTED_LAST_1HOUR':
        start_date = now - timedelta(hours=1)
        detail = 'minute'
        date_array = [now.replace(second=0, microsecond=0) - timedelta(minutes=1 * x) for x in list(range(0, 60))]
        date_format = 'hh:mm'
    elif dateperiod_text == 'STARTED_LAST_10MINS':
        start_date = now - timedelta(minutes=10)
        detail = 'minute'
        date_array = [now.replace(second=0, microsecond=0) - timedelta(minutes=1 * x) for x in list(range(0, 10))]
        date_format = 'hh:mm'
    elif dateperiod_text == 'STARTED_LAST_30MINS':
        start_date = now - timedelta(minutes=30)
        detail = 'minute'
        date_array = [now.replace(second=0, microsecond=0) - timedelta(minutes=1 * x) for x in list(range(0, 30))]
        date_format = 'hh:mm'
    elif dateperiod_text == 'STARTED_LAST_4HOURS':
        start_date = now - timedelta(hours=4)
        detail = 'hour'
        date_array = [now.replace(minute=0, second=0, microsecond=0) - timedelta(hours=1 * x) for x in list(range(0, 4))]
        date_format = 'ha'
    elif dateperiod_text == 'STARTED_TODAY':
        start_date = now.replace(hour=0, minute=0, second=0, microsecond=0)
        detail = 'hour'
        date_array = [now.replace(minute=0, second=0, microsecond=0) - timedelta(hours=1 * x) for x in list(range(0, now.hour+1))]
        date_format = 'ha'
    elif dateperiod_text == 'STARTED_YESTERDAY':
        start_date = now.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        end_date = now.replace(hour=0, minute=0, second=0, microsecond=0)
        detail = 'hour'
        date_array = [now.replace(hour=23, minute=0, second=0, microsecond=0) - timedelta(hours=1 * x) - timedelta(days=1) for x in list(range(0, 24))]
        date_format = 'ha'
    elif dateperiod_text == 'STARTED_THIS_WEEK':
        today_midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
        start_date = today_midnight - timedelta(days=today_midnight.weekday())
        detail = 'day'
        date_array = [now.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1 * x) for x in list(range(0, now.weekday()+1))]
        date_format = 'YYYY.MM.D'
    elif dateperiod_text == 'STARTED_LAST_WEEK':
        today_midnight = now.replace(hour=0, minute=0, second=0, microsecond=0)
        # the Monday last week
        start_date = today_midnight - timedelta(days=today_midnight.weekday(), weeks=1)
        # the Monday 00:00 this week
        end_date = today_midnight - timedelta(days=today_midnight.weekday())
        detail = 'day'
        last_Sunday = now.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days = now.weekday() + 1)
        date_array = [last_Sunday - timedelta(days=1 * x) for x in list(range(0, 7))]
        date_format = 'YYYY.MM.D'
    elif dateperiod_text == 'STARTED_THIS_MONTH':
        start_date = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        detail = 'day'
        date_array = [now.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1 * x) for x in list(range(0, now.day))]
        date_format = 'YYYY.MM.D'
    elif dateperiod_text == 'STARTED_LAST_MONTH':
        last_month = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        start_date = last_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        end_date = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        detail = 'day'
        month_length = calendar.monthrange(start_date.year, start_date.month)[1]
        previous_month = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)
        date_array = [previous_month.replace(day=month_length, hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1 * x) for x in list(range(0, month_length))]
        date_format = 'YYYY.MM.D'
    elif dateperiod_text == 'STARTED_THIS_YEAR':
        start_date = now.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
        detail = 'month'
        date_array = [now.replace(month=x, day=1, hour=0, minute=0, second=0, microsecond=0) for x in list(range(now.month, 0, -1))]
        date_format = 'YYYY MMM'
    else:
        raise Exception('Invalid date period filter! - ' + dateperiod_text)
    date_filter['start_date__gte'] = start_date
    date_filter['start_date__lt'] = end_date
    return date_filter, detail, date_array, date_format


def set_profile_dashboard_flow(profile, value, squareNumber):
    if squareNumber == '0':
        profile.dashboard_p2_flow0 = value
    elif squareNumber == '1':
        profile.dashboard_p2_flow1 = value
    elif squareNumber == '2':
        profile.dashboard_p2_flow2 = value
    elif squareNumber == '3':
        profile.dashboard_p2_flow3 = value
    else:
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='The squareNumber attribute has and invalid value!')
    profile.save()
    return Response(status=status.HTTP_200_OK)


def set_profile_dashboard_event_source(profile, value, squareNumber):
    if squareNumber == '0':
        profile.dashboard_p2_event_source0 = value
    elif squareNumber == '1':
        profile.dashboard_p2_event_source1 = value
    elif squareNumber == '2':
        profile.dashboard_p2_event_source2 = value
    elif squareNumber == '3':
        profile.dashboard_p2_event_source3 = value
    else:
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='The squareNumber attribute has and invalid value!')
    profile.save()
    return Response(status=status.HTTP_200_OK)


def get_event_source_ids(event_source, event_sources):
    event_sources.append(event_source.id)
    for child in EventSource.objects.filter(Q(parent_id=event_source.id) & Q(status='VALID')):
        get_event_source_ids(child, event_sources)
