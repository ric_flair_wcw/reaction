from django.conf.urls import url
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    url(r'^dashboard$',                                 views.index,                      name = 'dashboard'),

    # for REST service
    url(r'^dashboard/initialData$',                     views.load_initial_data,          name = 'dashboard_initial_data'),

    url(r'^dashboard/autoRefresh$',                     views.save_auto_refresh,          name = 'dashboard_save_auto_refresh'),

    url(r'^dashboard/dateperiod$',                      views.save_dateperiod,            name = 'dashboard_dateperiod'),

    url(r'^dashboard/top_markers$',                     views.feed_top_markers,           name = 'dashboard_top_markers'),

    url(r'^dashboard/p1$',                              views.feed_p1,                    name = 'dashboard_p1'),

    url(r'^dashboard/p2$',                              views.feed_p2,                    name = 'dashboard_p2'),
    url(r'^dashboard/p2/flow$',                         views.manage_flow_in_square,      name = 'dashboard_p2_select_flow'),
    url(r'^dashboard/p2/flow/(?P<squareNr>[0-9]+)$',    views.remove_flow_from_square,    name = 'dashboard_p2_remove_flow'),

    url(r'^dashboard/p3$',                              views.feed_p3,                    name = 'dashboard_p3'),
    url(r'^dashboard/p3/event_source$',                 views.manage_event_source_in_square,     name = 'dashboard_p3_select_event_source'),
    url(r'^dashboard/p3/event_source/(?P<squareNr>[0-9]+)$',  views.remove_event_source_from_square,   name = 'dashboard_p3_remove_event_source'),
]