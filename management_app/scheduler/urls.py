from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^scheduler$',                                   views.SchedulerList.as_view(),          name='scheduler_list'),

    url(r'^scheduler/(?P<pk>[0-9]+)/update$',             views.SchedulerUpdate.as_view(),        name='scheduler_update'),

    url(r'^scheduler/create$',                            views.SchedulerCreate.as_view(),        name='scheduler_create'),

    url(r'^scheduler/(?P<pk>[0-9]+)/delete$',             views.delete_scheduler,                 name='scheduler_delete'),
    
    url(r'^scheduler/(?P<pk>[0-9]+)/start$',              views.start_scheduling,                 name='scheduler_start'),
    url(r'^scheduler/(?P<pk>[0-9]+)/stop$',               views.stop_scheduling,                  name='scheduler_stop'),
    
    # for REST service
    url(r'^scheduler/getNextRun$',                        views.get_next_run,                     name='scheduler_get_next_run'),
]
