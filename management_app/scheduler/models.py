from django.db import models
from admin_event_source.models import ParentModel
from admin_execution_flow.models import ExecutionFlow

# ################################## ScheduledExecutionFlow ##################################
class ScheduledExecutionFlow(ParentModel):
    name = models.CharField(max_length=200, unique=True)
    reason = models.TextField(blank = True, null = True)
    cron_expression = models.CharField(max_length=150)
    execution_flow = models.ForeignKey(ExecutionFlow, on_delete=models.CASCADE)
    scheduled_already = models.BooleanField(default=False)
    error_at_last_run = models.TextField(blank = True, null = True)

    class Meta:
        db_table = 'SCHEDULED_EXECUTION_FLOW'
        default_permissions = ()


    def __str__(self):
        return "Name: {}\nReason: {}\nCron expression: {}".format(self.name, self.reason, self.cron_expression)
