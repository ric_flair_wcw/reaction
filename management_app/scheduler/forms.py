from django import forms
from .models import ScheduledExecutionFlow

class SchedulerForm(forms.ModelForm):
    reason = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 3}))

    class Meta:
        model = ScheduledExecutionFlow
        fields = ['name', 'reason', 'cron_expression', 'execution_flow']

    def __init__(self, *args, **kwargs):
        super(SchedulerForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['reason'].widget.attrs['class'] = 'form-control'
        self.fields['reason'].widget.attrs['style'] = 'resize: vertical;'
        self.fields['cron_expression'].widget.attrs['class'] = 'form-control'
        self.fields['execution_flow'].widget.attrs['class'] = 'form-control'