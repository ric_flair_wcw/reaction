import logging
from django.views.generic import UpdateView, CreateView
from common.view_utils import FilteredListView
from .models import ScheduledExecutionFlow
from admin_execution_flow.models import ExecutionFlow
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from .forms import SchedulerForm
from cron_descriptor import ExpressionDescriptor, CasingTypeEnum, Exception
from scheduler.service_clients import *
from django.shortcuts import redirect
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from common.utils import *
from django.shortcuts import get_object_or_404, render
from common.rest_utils import login_required_for_rest
from django.db.utils import IntegrityError


logger = logging.getLogger('root')


class SchedulerList(LoginRequiredMixin, PermissionRequiredMixin, FilteredListView):
    paginate_by = 10
    template_name = 'scheduler/list.html'
    permission_required = 'auth.can_use_scheduler'

    def get_queryset(self):
        # build the filter
        filter = self.build_filter()
        # build the order
        order = ['name' if 'order' not in self.request.GET else self.request.GET.get('order'), 'id']
        # build the access_groups filter
        access_groups_conditions = build_filter_access_groups(self.request.user.profile.access_groups, False, 'execution_flow__')
        logger.info('get_queryset - {}|  filter :{},    order: {}'.format(__name__, filter, order))
        return ScheduledExecutionFlow.objects.filter(access_groups_conditions, **filter).order_by(*order)

    def get_context_data(self, **kwargs):
        context = super(SchedulerList, self).get_context_data(**kwargs)
        # if error occurred during the REST call then a modal dialog has to be displayed with message
        if self.request.session.has_key('successful'):
            context['successful'] = self.request.session['successful']
            del self.request.session['successful']
            context['errorMessage'] = self.request.session['errorMessage']
            del self.request.session['errorMessage']
        return context


class SchedulerUpdateCreateView(LoginRequiredMixin, PermissionRequiredMixin):
    model = ScheduledExecutionFlow
    form_class = SchedulerForm
    template_name = 'scheduler/form.html'
    permission_required = 'auth.can_use_scheduler'

    def get_success_url(self):
        return reverse_lazy('scheduler_list')

    def get_context_data(self, **kwargs):
        # setting the parent combo box
        context = super(SchedulerUpdateCreateView, self).get_context_data(**kwargs)
        # build the access_groups filter
        access_groups_conditions = build_filter_access_groups(self.request.user.profile.access_groups, False, '')
        # setting the execution flow id and name to the dropdown
        choices = []
        for ef in ExecutionFlow.objects.filter(access_groups_conditions, ~Q(status='INVALID')).order_by('name'):
            choices.append((ef.id, ef.name))
            context['form'].fields['execution_flow'].choices = choices
        context['form_operation'] = 'update' if self.__class__.__name__ == 'SchedulerUpdate' else 'create'
        # setting the description of the cron expression
        if context['form_operation'] == 'update':
            descripter = ExpressionDescriptor(context['scheduledexecutionflow'].cron_expression,
                                              throw_exception_on_parse_error = True,
                                              casing_type = CasingTypeEnum.Sentence,
                                              use_24hour_time_format = True)
            try:
                context['cron_expression_description'] = descripter.get_description()
            except Exception.FormatException as e:
                context['cron_expression_description'] = 'The cron expression is not valid! The error is "{}"'.format(e)
        else:
            context['cron_expression_description'] = 'Please save it first!'
        return context


class SchedulerUpdate(SchedulerUpdateCreateView, UpdateView):
    pass


class SchedulerCreate(SchedulerUpdateCreateView, CreateView):
    pass


@login_required
@permission_required('auth.can_use_scheduler', raise_exception=True)
def delete_scheduler(request, pk):
    logger.info('delete_scheduler is called - request={}; pk={}'.format(request,pk))
    try:
        ScheduledExecutionFlow.objects.filter(id = pk).delete()
        return HttpResponseRedirect(reverse_lazy('scheduler_list'))
    except IntegrityError:
        message = 'The scheduled execution flow be deleted as it is currently being used in somewhere else!'
        logger.exception(message)
        return render(request, 'business_error.html', {'message': message})


@login_required
@permission_required('auth.can_use_scheduler', raise_exception=True)
def start_scheduling(request, pk):
    logger.info('start_scheduling is called - request={}; pk={}'.format(request, pk))
    # call the REST of reaction engine
    rest_response = call_start_scheduling(pk)
    if rest_response.successful:
        return redirect('scheduler_list')
    else:
        request.session['successful'] = 'N'
        request.session['errorMessage'] = str(rest_response.error_code) + ' - ' + rest_response.message
        return redirect('scheduler_list')


@login_required
@permission_required('auth.can_use_scheduler', raise_exception=True)
def stop_scheduling(request, pk):
    logger.info('stop_scheduling is called - request={}; pk={}'.format(request, pk))
    scheduled_execution_flow = get_object_or_404(ScheduledExecutionFlow, pk=pk)
    scheduled_execution_flow.scheduled_already = False
    scheduled_execution_flow.save()
    return redirect('scheduler_list')


@login_required_for_rest
@permission_required('auth.can_use_scheduler', raise_exception=True)
@api_view(['POST'])
def get_next_run(request):
    logger.info('Getting the next run date of a cron expression - request.data={}'.format(request.data))
    # call the REST of reaction engine
    try:
        rest_response = call_get_next_run(request.data.get('cron'))
        if rest_response.successful:
            return Response(rest_response.message, status=status.HTTP_200_OK)
        else:
            return Response(rest_response.message, status=rest_response.error_code)
    except Exception as e:
        return Response('The Reaction Engine cannot be called! ' + str(e), status=500)

