from common.rest_utils import call_rest


def call_start_scheduling(pk):
    url = '/executionflow/start/scheduled'
    payload = {'scheduledExecutionFlowId': pk}

    return call_rest(payload, url, 'POST')

    
def call_get_next_run(cron):
    url = '/utils/cron/nextrun'
    payload = {'cron': cron}

    return call_rest(payload, url, 'POST')
