from common.rest_utils import call_rest


def call_approve(pk, username):
    payload = {'eventId': pk,
               'byWhom' : username}
    return call(payload, '/executionflow/approve', 'POST')


def call_approve_and_forced_start(pk, username):
    payload = {'eventId': pk,
               'byWhom' : username}
    return call(payload, '/executionflow/approve_and_forced_start', 'POST')


def call_get_next_in_maintenance_window(event_source, execution_flow_id):
    return call(None, '/utils/maintenancewindow/next/' + str(event_source.id) + '/' + str(execution_flow_id), 'GET')

def call_is_in_maintenance_window(event_source, execution_flow_id):
    return call(None, '/utils/maintenancewindow/isin/' + str(event_source.id) + '/' + str(execution_flow_id), 'GET')

def call(payload, url, verb):
    return call_rest(payload, url, verb)
