from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^approval$',                                            views.ApprovalListView.as_view(),        name = 'approval_list'),
	url(r'^approval/(?P<pk>[0-9]+)/details$',                     views.details,                           name = 'approval_details'),
	url(r'^approval/(?P<pk>[0-9]+)/approve$',                     views.approve,                           name = 'approval_approve'),
	url(r'^approval/(?P<pk>[0-9]+)/approve_and_forced_start$',    views.approve_and_forced_start,          name = 'approval_approve_and_forced_start'),
	url(r'^approval/(?P<pk>[0-9]+)/(?P<reason>.+)/reject$',       views.reject,                            name = 'approval_reject'),
]