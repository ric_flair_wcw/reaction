import logging
import datetime
from common.view_utils import FilteredListView
from monitoring.models import Event, EventLife
from django.shortcuts import render
from admin_execution_flow.mermaid import FlowBuilder
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from approval.service_clients import *
from django.db import transaction
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from common.utils import *
import json
import sys
from monitoring.views import convert_json_to_dictionary


logger = logging.getLogger('root')


class ApprovalListView(LoginRequiredMixin, PermissionRequiredMixin, FilteredListView):
    paginate_by = 17
    template_name = 'approval/list.html'
    permission_required = 'auth.can_use_approval'
    
    def get_queryset(self):
        # build the filter
        filter = self.build_filter()
        #      build log level filters
        self.build_multichoice_filter('log_level', filter)
        #      remove the hosts related filter and build it correctly later
        filter.pop('hosts__contains', None)
        #      build the date filters
        self.build_filter_date(filter, self.request.GET.get('by_date'), self.request.GET.get('start_date'), self.request.GET.get('end_date'))
        # build the order
        order = ['-start_date' if 'order' not in self.request.GET else self.request.GET.get('order'), 'id']
        logger.info('get_queryset - {}|  filter :{},    order: {}'.format(__name__, filter, order))
        # querying only the confirmation_needed events
        filter['status__exact'] = 'CONFIRMATION_NEEDED'
        # build the access_groups filter
        conditions = build_filter_access_groups(self.request.user.profile.access_groups, True, 'execution_flow__')
        #      remove the hosts related filter and build it correctly later
        filter.pop('hosts__contains', None)
        filter_hosts = self.request.GET.get('filter_hosts')
        if filter_hosts is not None:
            conditions.add( Q(**{'error_detector__execution_flow__hosts__contains': filter_hosts}) | Q(**{'execution_flow__hosts__contains': filter_hosts}), Q.AND)
        # execute the query result and add it to kwargs for the build_host
        self.kwargs['events'] = Event.objects.select_related('error_detector__execution_flow')\
                                             .select_related('execution_flow') \
                                             .select_related('event_source') \
                                             .filter(conditions, **filter)\
                                             .order_by(*order)
        return self.kwargs['events']

    def get_context_data(self, **kwargs):
        context = super(ApprovalListView, self).get_context_data(**kwargs)
        # adding the filters, order and parent_id back to the context (in order to be visible for the next request -> they shouldn't disappear if I typed them and click on the Filter! button)
        for param in self.request.GET:
            if (param.startswith('filter_')):
                context[param] = self.request.GET.get(param, '')
        if 'order' in self.request.GET:
            context['order'] = self.request.GET.get('order')
        else:
            context['order'] = '-start_date'
        if 'by_date' in self.request.GET:
            context['by_date'] = self.request.GET.get('by_date')
        if 'start_date' in self.request.GET and self.request.GET.get('by_date') == 'BETWEEN':
            context['start_date'] = self.request.GET.get('start_date')
        if 'end_date' in self.request.GET and self.request.GET.get('by_date') == 'BETWEEN':
            context['end_date'] = self.request.GET.get('end_date')
        context['filter_log_level'] = self.request.GET.getlist('filter_log_level')
        return context
    
    def build_filter_date(self, filter,by_date, start_date_text, end_date_text):
        end_date = datetime.datetime.now()
        # set the start date
        if by_date is None:
            return
        elif by_date == 'BETWEEN':
            start_date = self.check_date(start_date_text)
            end_date = self.check_date(end_date_text)
        elif by_date == 'NULL':
            return
        elif by_date == 'LAST_5MINS':
            start_date = datetime.datetime.now() - datetime.timedelta(minutes=5)
        elif by_date == 'LAST_30MINS':
            start_date = datetime.datetime.now() - datetime.timedelta(minutes=30)
        elif by_date == 'LAST_1HOUR':
            start_date = datetime.datetime.now() - datetime.timedelta(hours=1)
        elif by_date == 'LAST_3HOURS':
            start_date = datetime.datetime.now() - datetime.timedelta(hours=3)
        elif by_date == 'LAST_24HOURS':
            start_date = datetime.datetime.now() - datetime.timedelta(days=1)
        elif by_date == 'LAST_3DAYS':
            start_date = datetime.datetime.now() - datetime.timedelta(days=3)
        elif by_date == 'LAST_7DAYS':
            start_date = datetime.datetime.now() - datetime.timedelta(days=7)
        filter['start_date__range'] = (start_date, end_date)
    
    def check_date(self, datetime_text):
        # sample: 2017.03.16 14:59:01
        try:
            return datetime.datetime.strptime(datetime_text, '%Y.%m.%d %H:%M:%S')
        except ValueError:
            raise ValueError("Incorrect data format, should be YYYY.mm.dd HH:MM:SS (e.g. 2017.03.16 17:00:41) !")

    def build_multichoice_filter(self, field, filter):
        # remove the filter and ...
        filter.pop(field+'__contains', None)
        # build it correctly later
        arr = self.request.GET.getlist('filter_'+field)
        if arr:
            filter[field+'__in'] = arr


@login_required
@permission_required('auth.can_use_approval', raise_exception=True)
def details(request, pk):
    logger.info('Getting the details for approval. request={}'.format(request))
    # adding the event details
    event = Event.objects.get(pk=pk)
    context = {'event': event}
    # getting the date when the event will be executed if there is maintenance window on the event source
    event_source = get_maintenance_window_recursive(event.event_source)
    if event_source:
        rest_response = call_get_next_in_maintenance_window(event_source, event.execution_flow.id)
        analyse_response(context, rest_response, 'next_in_maintenance_window')
        rest_response = call_is_in_maintenance_window(event_source, event.execution_flow.id)
        analyse_response(context, rest_response, 'is_in_maintenance_window')
    else:
        context['is_in_maintenance_window'] = 'true'
    # building the text for mermaid flow
    execution_flow_id = event.error_detector.execution_flow.id if event.error_detector is not None else event.execution_flow.id
    context['mermaid_flow'], context['tooltip_loop'] = FlowBuilder.build(execution_flow_id)
    # if error occurred during the REST call then a modal dialog has to be displayed with message
    if request.session.has_key('successful'):
        context['successful'] = request.session['successful']
        del request.session['successful']
        context['errorMessage'] = request.session['errorMessage']
        del request.session['errorMessage']
    context['payload_values'] = convert_json_to_dictionary(event.payload_values).items()
    context['query_parameters'] = convert_json_to_dictionary(event.query_parameters).items()
    return render(request, 'approval/details.html', context)


def analyse_response(context, rest_response, session_key):
    try:
        if rest_response.successful:
            context[session_key] = rest_response.message
        else:
            context[session_key] = 'Error occurred during the call! HTTP error code: ' + str(rest_response.error_code) + ' - Error message: ' + rest_response.message
    except Exception as e:
        err = sys.exc_info()[0]
        context[session_key] = 'The Reaction Engine cannot be called! ' + str(err)


def get_maintenance_window_recursive(s):
    if s:
        return s if json.loads(s.maintenance_window) else get_maintenance_window_recursive(s.parent)
    else:
        return None


@login_required
@permission_required('auth.can_use_approval', raise_exception=True)
def approve(request, pk):
    logger.info('Approve the event id={},{}'.format(pk,request))
    # call the REST to start the execution flow
    rest_response = call_approve(pk, request.user.username,)
    if rest_response.successful:
        return HttpResponseRedirect(reverse_lazy('approval_list'))
    else:
        request.session['successful'] = 'N'
        request.session['errorMessage'] = str(rest_response.error_code) + ' - ' + rest_response.message
        return HttpResponseRedirect(reverse_lazy('approval_details',
                                                 kwargs={'pk': pk}))


@login_required
@permission_required('auth.can_use_approval', raise_exception=True)
def approve_and_forced_start(request, pk):
    logger.info('Approve and force start the event id={},{}'.format(pk,request))
    # call the REST to start the execution flow
    rest_response = call_approve_and_forced_start(pk, request.user.username)
    if rest_response.successful:
        return HttpResponseRedirect(reverse_lazy('approval_list'))
    else:
        request.session['successful'] = 'N'
        request.session['errorMessage'] = str(rest_response.error_code) + ' - ' + rest_response.message
        return HttpResponseRedirect(reverse_lazy('approval_details',
                                                 kwargs={'pk': pk}))


@login_required
@permission_required('auth.can_use_approval', raise_exception=True)
@transaction.atomic
def reject(request, pk, reason):
    logger.info('Reject the event id={},{}'.format(pk, request))
    # set the status of the event to REJECTED
    event = Event.objects.get(pk=pk)
    event.status = 'REJECTED'
    event.reason = reason
    event.save()
    # save an event life record with REJECTED
    EventLife.objects.create(event_date = datetime.datetime.now(),
                             event = event,
                             ordr = -2,
                             status = 'REJECTED',
                             by_whom = request.user.username)
    return HttpResponseRedirect(reverse_lazy('approval_list'))