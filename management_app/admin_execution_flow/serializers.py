from rest_framework import serializers
from . import models


class TaskSerializer(serializers.ModelSerializer):
    output_pattern = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    command = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    execution_timeout = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    os_password = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    if_expression = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    mail_recipients = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    mail_subject = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    mail_content = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_value_list_1 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_separator_1 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_value_list_2 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_separator_2 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_value_list_3 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_separator_3 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_value_list_4 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)
    loop_separator_4 = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)

    class Meta:
        model = models.Task
        fields = ('id', 'name', 'command', 'os_user', 'os_password', 'is_pwd_set_manually', 'host', 'execution_timeout', 'internal_task',
                  'if_expression', 'execution_flow', 'primary_task', 'secondary_task', 'output_pattern',
                  'mail_recipients', 'mail_subject', 'mail_content', 'loop_value_list_1', 'loop_separator_1',
                  'loop_value_list_2', 'loop_separator_2',  'loop_value_list_3', 'loop_separator_3',
                  'loop_value_list_4', 'loop_separator_4', 'loop_placeholder_var_1', 'loop_placeholder_var_2',
                  'loop_placeholder_var_3', 'loop_placeholder_var_4', 'loop_requires_encryption_1',
                  'loop_requires_encryption_2', 'loop_requires_encryption_3', 'loop_requires_encryption_4',
                  'ssh_auth_type', 'ssh_key_path')

    def __init__(self, *args, **kwargs):
        # initialize fields
        super(TaskSerializer, self).__init__(*args, **kwargs)
        # if the task is an EMAIL_SENDING then the recipients, subject and content fields are mandatory
        if type(self.instance) is models.Task:
            if self.instance.internal_task == 'EMAIL_SENDING':
                self.fields['mail_recipients'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)
                self.fields['mail_subject'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)
                self.fields['mail_content'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)
            elif self.instance.internal_task == 'COMMAND_BY_WORKER':
                self.fields['command'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)
                self.fields['host'] = serializers.CharField(allow_blank=False, allow_null=False)
            elif self.instance.internal_task == 'COMMAND_USING_SSH':
                self.fields['command'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)
                self.fields['host'] = serializers.CharField(allow_blank=False, allow_null=False)
                self.fields['os_user'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)
            elif self.instance.internal_task == 'IF_ELSE':
                self.fields['if_expression'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)
            elif self.instance.internal_task == 'LOOP':
                self.fields['loop_value_list_1'] = serializers.CharField(trim_whitespace=False, allow_blank=False, allow_null=False)

    def validate_execution_timeout(self, val):
        if val:
            try:
                int_val = int(val)
                if int_val == 0 or int_val < -1:
                    raise serializers.ValidationError("This field must be higher than zero or equal to -1.")
            except ValueError:
                raise serializers.ValidationError("This field has to contain an integer value.")
            return val
        else:
            return None

    def validate(self, data):
        from admin_execution_flow.views import check_incoming_data
        ok, errors = check_incoming_data(data, self.initial_data.get('id'), None)
        if ok:
            return data
        else:
            raise serializers.ValidationError(errors)


class ExecutionFlowSerializer(serializers.ModelSerializer):
    name = serializers.CharField(trim_whitespace=False, allow_blank=True, allow_null=True)

    class Meta:
        model = models.ExecutionFlow
        fields = ('id', 'name',)