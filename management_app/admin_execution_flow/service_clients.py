from common.rest_utils import call_rest


def call_check_expression(expression, commandOutput):
    url = '/utils/expression/check'
    payload = {'expression': expression,
               'commandOutput': commandOutput}
    return call_rest(payload, url, 'POST')
