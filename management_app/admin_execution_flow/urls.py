from django.conf.urls import url

from . import views

urlpatterns = [
	# for managing the execution_flow entities
    url(r'^administration/execution_flow$',                                          views.ExecutionFlowList.as_view(),             name = 'admin_execution_flow_list'),
    url(r'^administration/execution_flow/(?P<pk>[0-9]+)/update$',                    views.ExecutionFlowUpdate.as_view(),           name = 'admin_execution_flow_update'),
    url(r'^administration/execution_flow/create$',                                   views.ExecutionFlowCreate.as_view(),           name = 'admin_execution_flow_create'),
    url(r'^administration/execution_flow/(?P<pk>[0-9]+)/delete$',                    views.delete_execution_flow,                   name = 'admin_execution_flow_delete'),
    url(r'^administration/execution_flow/copy$',                                     views.copy_execution_flow,                     name = 'admin_execution_flow_copy'),
    url(r'^administration/execution_flow/(?P<pk>[0-9]*)/unfreeze',                   views.unfreeze_execution_flow,                 name = 'admin_execution_flow_unfreeze'),

	# for REST service
    url(r'^administration/execution_flow/task/(?P<pk>[0-9]+)$',                      views.ExecutionFlowTask.as_view(),             name = 'admin_execution_flow_task'),
	url(r'^administration/execution_flow/(?P<pk>[0-9]+)/tasks$',                     views.get_tasks_of_execution_flow,             name = 'admin_execution_flow_tasks'),
	url(r'^administration/execution_flow/task/(?P<pk>[0-9]+)/move$',                 views.move_task,                               name = 'admin_execution_flow_move_task'),
    url(r'^administration/execution_flow/task/(?P<pk>[0-9]+)/copy$',                 views.copy_task,                               name = 'admin_execution_flow_copy_task'),
    url(r'^administration/execution_flow/task/insert$',                              views.insert_task,                             name = 'admin_execution_flow_insert_task'),
    url(r'^administration/execution_flow/list$',                                     views.get_flows,                               name = 'admin_execution_flow'),
    url(r'^administration/execution_flow/check_expr$',                               views.check_expression,                        name = 'admin_execution_flow_check_expr'),
    url(r'^administration/execution_flow/ssh_auth_types$',                           views.get_ssh_auth_types,                      name = 'admin_execution_flow_ssh_auth_types'),
]
