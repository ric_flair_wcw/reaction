from django.db import models
from admin_event_source.models import ParentModel

# ################################## ExecutionFlow ##################################
class ExecutionFlow(ParentModel):
	name = models.CharField(max_length=200, unique=True)
	time_between_exec = models.IntegerField(blank = True, null = True)
	execution_time = models.IntegerField(blank = True, null = True)
	status = models.CharField(choices=ParentModel.EXECUTION_FLOW_STATUSES, max_length=40, blank = True, null = True)
	access_group = models.CharField(max_length=200, blank = True, null = True)
	error_mail_sending_recipients = models.CharField(max_length=2000, blank=True, null=True)
	start_mail_sending_recipients = models.CharField(max_length=2000, blank=True, null=True)
	hosts = models.CharField(max_length=2000, blank=True, null=True)

	class Meta:
		db_table = 'EXECUTION_FLOW'
		default_permissions = ()
		
	def __str__(self):
		return "Name: {}\nExecution Time: {}\nStatus: {}".format(self.name,
																 "" if self.execution_time is None else self.execution_time,
																 self.status)


# ################################## Task ##################################
class Task(ParentModel):
	name = models.CharField(max_length=200)
	ordr = models.IntegerField(default=1)
	command = models.CharField(max_length=2000, blank=True, null=True)
	os_user = models.CharField(max_length=200, blank=True, null=True)
	os_password = models.CharField(max_length=200, blank=True, null=True)
	is_pwd_set_manually = models.NullBooleanField(blank=True, null=True)
	host = models.CharField(max_length=100, blank = True, null=True)
	ssh_auth_type = models.CharField(choices=ParentModel.SSH_AUTH_TYPE, max_length=20, null=True, blank=True)
	ssh_key_path = models.CharField(max_length=400, null=True, blank=True)
	internal_task = models.CharField(choices=ParentModel.INTERNAL_TASKS, max_length=40)
	if_expression = models.CharField(max_length=2000, blank = True, null = True)
	output_pattern = models.CharField(max_length=300, blank = True, null = True)
	mail_recipients = models.CharField(max_length=2000, blank=True, null=True)
	mail_subject = models.CharField(max_length=300, blank = True, null = True)
	mail_content = models.CharField(max_length=2000, blank=True, null=True)
	execution_timeout = models.IntegerField(blank = True, null = True)
	loop_value_list_1 = models.TextField(blank=True, null=True)
	loop_separator_1 = models.CharField(max_length=20, blank=True, null=True)
	loop_value_list_2 = models.TextField(blank=True, null=True)
	loop_separator_2 = models.CharField(max_length=20, blank=True, null=True)
	loop_value_list_3 = models.TextField(blank=True, null=True)
	loop_separator_3 = models.CharField(max_length=20, blank=True, null=True)
	loop_value_list_4 = models.TextField(blank=True, null=True)
	loop_separator_4 = models.CharField(max_length=20, blank=True, null=True)
	loop_placeholder_var_1 = models.CharField(max_length=100, blank=True, null=True)
	loop_placeholder_var_2 = models.CharField(max_length=100, blank=True, null=True)
	loop_placeholder_var_3 = models.CharField(max_length=100, blank=True, null=True)
	loop_placeholder_var_4 = models.CharField(max_length=100, blank=True, null=True)
	loop_requires_encryption_1 = models.BooleanField(default=False)
	loop_requires_encryption_2 = models.BooleanField(default=False)
	loop_requires_encryption_3 = models.BooleanField(default=False)
	loop_requires_encryption_4 = models.BooleanField(default=False)
	execution_flow = models.ForeignKey(ExecutionFlow, on_delete=models.CASCADE, blank = True, null = True)
	primary_task = models.ForeignKey('self', on_delete=models.CASCADE, related_name='Task_primary_task', blank = True, null = True)
	secondary_task = models.ForeignKey('self', on_delete=models.CASCADE, related_name='Task_secondary_task', blank = True, null = True)

	class Meta:
		db_table = 'TASK'
		default_permissions = ()
		unique_together = ('name', 'execution_flow', 'primary_task', 'secondary_task')

	def __str__(self):
		return "Name: {}\nType: {}\nCommand: {}\nHost: {}\nOS user: {}\n'If' expression: {}\nMail recipients: {}\nMail subject {}\nMail content: {}"\
			.format(self.name,
		            self.internal_task,
		            ("" if self.command is None else self.command),
		            ("" if self.host is None else self.host),
		            ("" if self.os_user is None else self.os_user),
		            ("" if self.if_expression is None else self.if_expression),
		            ("" if self.mail_recipients is None else self.mail_recipients),
		            ("" if self.mail_subject is None else self.mail_subject),
		            ("" if self.mail_content is None else self.mail_content),
					("" if self.loop_value_list_1 is None else self.loop_value_list_1),
					("" if self.loop_separator_1 is None else self.loop_separator_1),
					("" if self.loop_value_list_2 is None else self.loop_value_list_2),
					("" if self.loop_separator_2 is None else self.loop_separator_2),
					("" if self.loop_value_list_3 is None else self.loop_value_list_3),
					("" if self.loop_separator_3 is None else self.loop_separator_3),
					("" if self.loop_value_list_4 is None else self.loop_value_list_4),
					("" if self.loop_separator_4 is None else self.loop_separator_4),
					("" if self.loop_placeholder_var_1 is None else self.loop_placeholder_var_1),
					("" if self.loop_placeholder_var_2 is None else self.loop_placeholder_var_2),
					("" if self.loop_placeholder_var_3 is None else self.loop_placeholder_var_3),
					("" if self.loop_placeholder_var_4 is None else self.loop_placeholder_var_4))
