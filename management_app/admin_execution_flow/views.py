import copy, logging, uuid
from django.conf import settings
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy
from django.db import transaction
from django.db.models import F
from django.db.utils import IntegrityError
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import RequestContext
from django.views.decorators.http import require_http_methods
from django.views.generic import UpdateView, CreateView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.exceptions import NotAcceptable
from rest_framework.response import Response
from rest_framework.views import APIView

from admin_errordetector.models import ErrorDetector
from admin_execution_flow.service_clients import *
from common.rest_utils import LoginRequiredForRestMixin
from common.rest_utils import login_required_for_rest
from common.utils import *
from common.view_utils import FilteredListView
from monitoring.models import Event
from .forms import ExecutionFlowForm
from .mermaid import FlowNodes, FlowBuilder, ParentType
from .models import ExecutionFlow, Task, ParentModel
from .serializers import TaskSerializer, ExecutionFlowSerializer

logger = logging.getLogger('root')


class ExecutionFlowList(LoginRequiredMixin, PermissionRequiredMixin, FilteredListView):
    paginate_by = 10
    template_name = 'admin_execution_flow/list.html'
    permission_required = 'auth.can_use_admin_execution_flow'

    def get_queryset(self):
        # building the filter
        filter = self.build_filter()
        #     remove the hosts & access groups related filter and build it correctly later
        filter.pop('access_groups__contains', None)
        # build the access_groups filter
        access_groups_conditions = build_filter_access_groups(self.request.user.profile.access_groups, False, '')
        # building the order
        order = ['name' if 'order' not in self.request.GET else self.request.GET.get('order'), 'id']
        logger.info('get_queryset - {}, access groups conditions: {}, filter: {}, order: {}'.format(__name__, access_groups_conditions, filter, order))
        # execute the query result and add it to kwargs for the build_host
        execution_flows = ExecutionFlow.objects.filter(access_groups_conditions, **filter).order_by(*order)
        return execution_flows

    # the user can filter by access_group -> loop through the execution flows, check if it has a task with the specific access_group and if it doesn't have then remove it
    def filter_by_access_groups(self, execution_flows, filter_access_groups):
        if filter_access_groups is None or not filter_access_groups:
            return execution_flows
        filtered_execution_flows = []
        for execution_flow in execution_flows:
            if Task.objects.filter(execution_flow=execution_flow, access_group__contains=filter_access_groups).exists():
                filtered_execution_flows.append(execution_flow)
        return filtered_execution_flows


class ExecutionFlowUpdateCreateView(LoginRequiredMixin, PermissionRequiredMixin):
    model = ExecutionFlow
    form_class = ExecutionFlowForm
    template_name = 'admin_execution_flow/form.html'
    permission_required = 'auth.can_use_admin_execution_flow'

    def get_success_url(self):
        return reverse_lazy('admin_execution_flow_list')

    def get_context_data(self, **kwargs):
        # setting the parent combo box
        context = super(ExecutionFlowUpdateCreateView, self).get_context_data(**kwargs)
        context['mermaid_flow'], context['tooltip_loop'] = self.build_mermaid_flow()
        context['form_operation'] = 'update' if self.__class__.__name__ == 'ExecutionFlowUpdate' else 'create'
        context['access_groups'] = '[' + ','.join("{id:'"+str(v)+"',text:'"+str(v)+"'}" for v in settings.ACCESS_GROUPS) + ']';
        return context

    def build_mermaid_flow(self):
        return FlowNodes.definition() + "\n" + FlowNodes.node_start(), ''


class ExecutionFlowUpdate(ExecutionFlowUpdateCreateView, UpdateView):
    def build_mermaid_flow(self):
        # build the text of the mermaid diagram
        mermaid_flow, tooltip_loop_text = FlowBuilder.build(self.kwargs['pk'])
        logger.info('mermaid_flow: {}'.format(mermaid_flow))
        logger.info('tooltip_loop_text: {}'.format(tooltip_loop_text))
        return mermaid_flow, tooltip_loop_text

    def get_context_data(self, **kwargs):
        # setting the parent combo box
        context = super(ExecutionFlowUpdate, self).get_context_data(**kwargs)
        # validating the execution flow afte the deletion
        context['validation_error'] = validate_execution_flow(self.request, self.object.id)
        # the 'object' exists in the context already but it has to be refreshed
        context['object'] = ExecutionFlow.objects.get(pk=self.object.id)
        # deactivate error detectors if necessary
        if context['validation_error'] is not None:
            logger.warn('There was a validation error in the flow so the activated error detectors will be deactivated. validation_error={}'.format(context['validation_error']))
            context['deactivated_error_detectors'] = deactivate_error_detectors(context['object'])
        return context


class ExecutionFlowCreate(ExecutionFlowUpdateCreateView, CreateView):
    pass


@login_required
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
def delete_execution_flow(request, pk):
    logger.info('delete_execution_flow is called - pk={}'.format(pk))
    # delete the tasks of the flow first
    for task in Task.objects.filter(execution_flow=get_object_or_404(ExecutionFlow, pk=pk)):
        recursive_delete(task)
    # delete the flow
    try:
        ExecutionFlow.objects.filter(id=pk).delete()
        return HttpResponseRedirect(reverse_lazy('admin_execution_flow_list'))
    except IntegrityError as e:
        message = 'The execution flow cannot be deleted as it is currently being used in an error detector!'
        logger.exception(message)
        return render(request, 'business_error.html', {'message': message})


def recursive_delete(task):
    for t in Task.objects.filter(primary_task=task):
        recursive_delete(t)
    for t in Task.objects.filter(secondary_task=task):
        recursive_delete(t)
    task.delete()


def deactivate_error_detectors(execution_flow):
    # getting the error detectors that uses the execution flow and activated
    error_detectors = [ed for ed in ErrorDetector.objects.filter(execution_flow=execution_flow, activated=True)]
    # deactivate the error detectors
    ErrorDetector.objects.filter(execution_flow=execution_flow, activated=True).update(activated=False)
    return error_detectors


@login_required
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
def unfreeze_execution_flow(request, pk):
    execution_flow = get_object_or_404(ExecutionFlow, pk=pk)
    # remove the Event and the relating entities
    #   - when started the flow manually or by the scheduled
    Event.objects.filter(execution_flow=execution_flow).delete()
    #   - when started the flow by the log
    for error_detector in ErrorDetector.objects.filter(execution_flow=execution_flow):
        Event.objects.filter(error_detector=error_detector).delete()
    # change the status of the execution flow to VALID
    execution_flow.status = 'VALID'
    execution_flow.save()
    return HttpResponseRedirect(reverse_lazy('admin_execution_flow_update', kwargs={'pk': pk}))


@login_required
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@transaction.atomic
@require_http_methods(['POST'])
def copy_execution_flow(request):
    orig_id = request.POST['orig_id']
    new_name = request.POST['new_name']
    logger.info('copy_execution_flow (POST) is called - original execution flow id = {}, new name={}'.format(orig_id, new_name))
    # getting the original execution flow
    new_execution_flow = get_object_or_404(ExecutionFlow, pk=orig_id)
    # create the new execution flow
    new_execution_flow.pk = None
    new_execution_flow.name = new_name
    new_execution_flow.status = 'VALID'
    new_execution_flow.save()
    # copy the tasks of the original execution flow
    copy_tasks(orig_id, new_execution_flow, ParentType.execution_flow)
    return HttpResponseRedirect(reverse_lazy('admin_execution_flow_list'))
    

# -------------------------------------------------- for REST --------------------------------------------------
class ExecutionFlowTask(LoginRequiredForRestMixin, PermissionRequiredMixin, APIView):
    permission_required = 'auth.can_use_admin_execution_flow'
    
    def get(self, request, pk, format=None, ):
        logger.info('Getting a task of the execution flow - task pk={}, request={}'.format(pk, request.data))
        task = get_object_or_404(Task, pk=pk)
        serializer = TaskSerializer(task, many=False)
        return Response((serializer.data, ParentModel.SSH_AUTH_TYPE))

    # for updating
    def put(self, request, pk, format=None):
        logger.info('Updating (PUT) a task of the execution flow - task pk={}, request={}'.format(pk, request.data))
        task = get_object_or_404(Task, pk=pk)
        if request.data.get('is_pwd_set_manually'):
            encrypt_os_password(request, task)
        encrypt_value_list_if_needed(request, task)
        serializer = TaskSerializer(task, data=request.data, many=False)
        if serializer.is_valid():
            serializer.save()
            # updating the hosts of the execution flow
            update_hosts(get_execution_flow(task))
            return Response((serializer.data, ParentModel.SSH_AUTH_TYPE), status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @transaction.atomic
    def delete(self, request, pk, format=None):
        logger.info('Deleting (DELETE) a task of the execution flow - task pk={}, request={}'.format(pk, request.data))
        # get the record and delete it
        task = get_object_or_404(Task, pk=pk)
        task.delete()
        # updating the hosts of the execution flow
        update_hosts(get_execution_flow(task))
        # decrease the order of the subsequent tasks
        field_name = 'execution_flow' if task.execution_flow is not None else 'primary_task' if task.primary_task is not None else 'secondary_task'
        field_value = task.execution_flow if task.execution_flow is not None else task.primary_task if task.primary_task is not None else task.secondary_task
        filter_text = {
            '{0}__{1}'.format(field_name, 'exact'): field_value,
            '{0}__{1}'.format('ordr', 'gt'): task.ordr,
        }
        Task.objects.filter(**filter_text).update(ordr=F('ordr') - 1)
        return Response(status=status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['GET'])
def get_tasks_of_execution_flow(request, pk):
    logger.info('Getting all the tasks of an execution_flow / GET is called - execution flow id={}, request={}'.format(pk, request.data))
    tasks_entities = Task.objects.filter(execution_flow=pk).order_by('ordr')
    serializer = TaskSerializer(tasks_entities, many=True)
    tasks = []
    for task in serializer.data:
        tasks.extend(_get_tasks_of_execution_flow(task))
    return Response(tasks, status=status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['GET'])
def get_ssh_auth_types(request):
    logger.info('Getting the SSH auth types / GET is called - request={}'.format(request.data))
    return Response(ParentModel.SSH_AUTH_TYPE, status=status.HTTP_200_OK)

@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['POST'])
@transaction.atomic
def insert_task(request):
    logger.info('Inserting (POST) a new task to the execution flow request.data={}'.format(request.data))
    resp, task = insert_or_move_task(request, None)
    # updating the hosts of the execution flow
    if task is not None:
        update_hosts(get_execution_flow(task))
    return resp


@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['PUT'])
@transaction.atomic
def move_task(request, pk):
    logger.info('Moving (PUT) a task in the execution flow pk={}, request.data={}'.format(pk, request.data))
    resp, task = insert_or_move_task(request, pk)
    return resp


@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['PUT'])
@transaction.atomic
def copy_task(request, pk):
    logger.info('Copying (PUT) a task in the execution flow pk={}, request.data={}'.format(pk, request.data))
    resp, task = insert_or_move_task(request, pk)
    return resp


@login_required_for_rest
@permission_required('auth.can_use_admin_execution_flow', raise_exception=True)
@api_view(['GET'])
@transaction.atomic
def get_flows(request):
    logger.info('Getting the flow list. request={}'.format(request))

    access_groups_conditions = build_filter_access_groups(request.user.profile.access_groups, False, '')
    flow_entites = ExecutionFlow.objects.filter(access_groups_conditions).order_by('name','id')

    serializer = ExecutionFlowSerializer(flow_entites, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


# check the expression
@api_view(['POST'])
@login_required_for_rest
@permission_required('auth.can_use_monitoring', raise_exception=True)
def check_expression(request):
    logger.info('check_expression / POST is called - request={}'.format(request))
    # calling the REST service
    rest_response = call_check_expression(request.data.get('expression'), request.data.get('commandOutput'))
    # checking if it was successful
    return Response(status=rest_response.error_code, data=rest_response.message)

# -------------------------------------------------- utility functions --------------------------------------------------
def insert_or_move_task(request, pk):
    # task_source -> the new task to be inserted, the task to be moved or the task to be copied
    # task_destination -> the location to be inserted, moved or copied to
    # ###################### 1. get the source / destination  task
    if request.data['operation'] == 'MOVE':
        task_source = get_object_or_404(Task, pk=pk)
    elif request.data['operation'] == 'INSERT':
        # check if at INSERT the mandatory fields exist in the new task
        ok, errors = check_incoming_data(request.data.get('newTask'), pk, request.data['task_destination'])
        if not ok:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=errors), None
        task_source = Task(name=request.data.get('newTask').get('name') if request.data.get('newTask').get('name') else uuid.uuid4(),
                           internal_task=request.data.get('newTask').get('internal_task'),
                           if_expression=request.data.get('newTask').get('if_expression'),
                           host=request.data.get('newTask').get('host'),
                           execution_timeout=int(request.data.get('newTask').get('execution_timeout')) if request.data.get('newTask').get('execution_timeout') else None,
                           command=request.data.get('newTask').get('command'),
                           os_user=request.data.get('newTask').get('os_user'),
                           os_password=encrypt_password(request.data.get('newTask').get('os_password')),
                           ssh_auth_type=request.data.get('newTask').get('ssh_auth_type'),
                           ssh_key_path=request.data.get('newTask').get('ssh_key_path'),
                           is_pwd_set_manually=request.data.get('newTask').get('is_pwd_set_manually'),
                           output_pattern=request.data.get('newTask').get('output_pattern'),
                           mail_recipients=request.data.get('newTask').get('mail_recipients'),
                           mail_subject=request.data.get('newTask').get('mail_subject'),
                           mail_content=request.data.get('newTask').get('mail_content'),
                           loop_value_list_1=request.data.get('newTask').get('loop_value_list_1'),
                           loop_separator_1=request.data.get('newTask').get('loop_separator_1'),
                           loop_value_list_2=request.data.get('newTask').get('loop_value_list_2'),
                           loop_separator_2=request.data.get('newTask').get('loop_separator_2'),
                           loop_value_list_3=request.data.get('newTask').get('loop_value_list_3'),
                           loop_separator_3=request.data.get('newTask').get('loop_separator_3'),
                           loop_value_list_4=request.data.get('newTask').get('loop_value_list_4'),
                           loop_separator_4=request.data.get('newTask').get('loop_separator_4'),
                           loop_placeholder_var_1=request.data.get('newTask').get('loop_placeholder_var_1'),
                           loop_placeholder_var_2=request.data.get('newTask').get('loop_placeholder_var_2'),
                           loop_placeholder_var_3=request.data.get('newTask').get('loop_placeholder_var_3'),
                           loop_placeholder_var_4=request.data.get('newTask').get('loop_placeholder_var_4'))
    elif request.data['operation'] == 'COPY':
        _task = get_object_or_404(Task, pk=pk)
        # the IF_ELSE cannot be copied
        if _task.internal_task == 'IF_ELSE' or _task.internal_task == 'LOOP':
            raise NotAcceptable('IF-ELSE or LOOP task cannot be copied!')
        task_source = copy.deepcopy(_task)
        task_source.id = None
        task_source.name = 'Copy of ' + task_source.name
    else:
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='Unrecognizable operation: {}!'.format(request.data['OPERATION'])), None
    if not request.data['task_destination'].startswith('START'):
        task_destination = get_object_or_404(Task, pk=request.data['task_destination'])
    else:
        task_destination = Task(execution_flow=get_object_or_404(ExecutionFlow, pk=request.data['task_destination'][6:]),
                                ordr=0,
                                internal_task='COMMAND_USING_SSH')
    #
    if request.data['where'] in ['TO_TRUE', 'TO_FALSE'] and task_destination.internal_task != 'IF_ELSE':
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='In order to move the task to the TRUE or FALSE branch the destination task has to be an IF-ELSE!'), None
    if request.data['where'] == 'INTO_LOOP' and task_destination.internal_task != 'LOOP':
        return Response(status=status.HTTP_422_UNPROCESSABLE_ENTITY, data='In order to move the task to the beginning of the LOOP the destination task has to be an LOOP!'), None
    # ###################### 2. taking out the task from where it is at the moment (ie. the orders of all the tasks that are after it have to be decreased)
    #    only if : - the operation is MOVE
    if request.data['operation'] == 'MOVE':
        field_name = 'execution_flow' if task_source.execution_flow is not None else 'primary_task' if task_source.primary_task is not None else 'secondary_task'
        field_value = task_source.execution_flow if task_source.execution_flow is not None else task_source.primary_task if task_source.primary_task is not None else task_source.secondary_task
        filter_text = {
            '{0}__{1}'.format(field_name, 'exact'): field_value,
            '{0}__{1}'.format('ordr', 'gt'): task_source.ordr,
        }
        Task.objects.filter(**filter_text).update(ordr=F('ordr') - 1)
        # refreshing the destination task
        if not request.data['task_destination'].startswith('START'):
            task_destination.refresh_from_db()
    # ###################### 3. put the task in to the new location (ie. the orders of all the tasks that will be after it have to be increased)
    #    only if : - the internal_task is not 'if_else' and the operation is not INSERT or COPY and if_else_type is not in ('ADD_TO_TRUE','ADD_TO_FALSE')
    if not (request.data.get('newTask') is not None and request.data.get('newTask').get('internal_task') == 'IF_ELSE'
            and request.data['operation'] in ['INSERT','COPY'] and request.data['if_else_type'] in ['ADD_TO_TRUE','ADD_TO_FALSE']):
        field_name = 'primary_task' if request.data['where'] == 'TO_TRUE' else \
                            'secondary_task' if request.data['where'] == 'TO_FALSE' else \
                            'primary_task' if request.data['where'] == 'INTO_LOOP' else \
                            'execution_flow' if task_destination.execution_flow is not None else \
                            'primary_task' if task_destination.primary_task is not None else \
                            'secondary_task'
        field_value = task_destination.id if request.data['where'] in ['TO_TRUE', 'TO_FALSE', 'INTO_LOOP'] else \
                            task_destination.execution_flow if task_destination.execution_flow is not None else \
                            task_destination.primary_task if task_destination.primary_task is not None else task_destination.secondary_task
        filter_text = {
            '{0}__{1}'.format(field_name, 'exact'): field_value,
            '{0}__{1}'.format('ordr', 'gt'): 0 if request.data['where'] in ['TO_TRUE', 'TO_FALSE', 'INTO_LOOP'] else task_destination.ordr,
        }
        Task.objects.filter(**filter_text).update(ordr=F('ordr') + 1)
    # 4. modifying the properties of the task that has to be moved
    task_source.ordr = 1 if request.data['where'] in ['TO_TRUE', 'TO_FALSE', 'INTO_LOOP'] else task_destination.ordr + 1
    if request.data['where'] == 'AFTER':
        task_source.primary_task = task_destination.primary_task
        task_source.secondary_task = task_destination.secondary_task
    elif request.data['where'] == 'TO_TRUE':
        task_source.primary_task = task_destination
        task_source.secondary_task = None
    elif request.data['where'] == 'TO_FALSE':
        task_source.primary_task = None
        task_source.secondary_task = task_destination
    elif request.data['where'] == 'INTO_LOOP':
        task_source.primary_task = task_destination
        task_source.secondary_task = None
    task_source.execution_flow = None if request.data['where'] in ['TO_TRUE', 'TO_FALSE', 'INTO_LOOP'] else task_destination.execution_flow
    task_source.save()
    # 5. if the operation is INSERT or COPY and the internal_task is IF_ELSE ...
    if request.data.get('newTask') is not None and request.data.get('newTask').get('internal_task') == 'IF_ELSE' and \
                    request.data['operation'] in ['INSERT', 'COPY']:
        # if the subsequent tasks have to be added to the TRUE or FALSE branch of the new IF-ELSE task then
        #    set the primary / secondary task values of these tasks to the new IF_ELSE task
        if request.data['if_else_type'] in ['ADD_TO_TRUE','ADD_TO_FALSE']:
            ordr = task_destination.ordr
            if request.data['where'] == 'AFTER':
                field_name = 'execution_flow' if task_destination.execution_flow is not None else 'primary_task' if task_destination.primary_task is not None else \
                                   'secondary_task'
                field_value = task_destination.execution_flow if task_destination.execution_flow is not None else \
                                   task_destination.primary_task if task_destination.primary_task is not None else task_destination.secondary_task
                ordr += 1
            else:
                field_name = 'primary_task' if request.data['where'] == 'INTO_LOOP' else 'primary_task' if request.data['where'] == 'ADD_TO_TRUE' else 'secondary_task'
                field_value = task_destination
            kwargs = {
                '{0}__{1}'.format(field_name, 'exact'): field_value,
                '{0}__{1}'.format('ordr', 'gte'): ordr,
            }
            args = [ ~Q(id__exact=task_source.id) ]
            subsequent_task_set = Task.objects.filter(*args, **kwargs).order_by('ordr')
            new_ordr = 1
            primary_task = task_source if request.data['if_else_type'] == 'ADD_TO_TRUE' else None
            secondary_task = task_source if request.data['if_else_type'] == 'ADD_TO_FALSE' else None
            for subsequent_task in subsequent_task_set:
                subsequent_task.execution_flow = None
                subsequent_task.primary_task = primary_task
                subsequent_task.secondary_task = secondary_task
                subsequent_task.ordr = new_ordr
                subsequent_task.save()
                new_ordr += 1
    return Response(status=status.HTTP_200_OK), task_source


def check_incoming_data(new_task, id, preceding_task_id):
    ok = True
    errors = {}
    if not new_task.get('name'):
        ok = False
        errors['name'] = 'This field may not be blank.'
    if new_task.get('internal_task') == 'EMAIL_SENDING':
        if not new_task.get('mail_recipients'):
            ok = False
            errors['mail_recipients'] = 'This field may not be blank.'
        if not new_task.get('mail_subject'):
            ok = False
            errors['mail_subject'] = 'This field may not be blank.'
        if not new_task.get('mail_content'):
            ok = False
            errors['mail_content'] = 'This field may not be blank.'
    elif new_task.get('internal_task') == 'COMMAND_BY_WORKER':
        if not new_task.get('command'):
            ok = False
            errors['command'] = 'This field may not be blank.'
        if not new_task.get('host'):
            ok = False
            errors['host'] = 'This field may not be blank.'
    elif new_task.get('internal_task') == 'COMMAND_USING_SSH':
        if not new_task.get('command'):
            ok = False
            errors['command'] = 'This field may not be blank.'
        if not new_task.get('host'):
            ok = False
            errors['host'] = 'This field may not be blank.'
        if not new_task.get('os_user'):
            ok = False
            errors['os_user'] = 'This field may not be blank.'
        if new_task.get('execution_timeout'):
            if not is_num(new_task.get('execution_timeout')) or int(new_task.get('execution_timeout')) == 0 or int(new_task.get('execution_timeout')) < -1:
                ok = False
                errors['execution_timeout'] = 'This field must be integer and higher than zero or equal to -1.'
        if new_task.get('ssh_auth_type') == 'USER_PASSWORD':
            if not new_task.get('is_pwd_set_manually'):
                if id:
                    current_task = Task.objects.get(pk=id)
                    found = False
                    while not found:
                        parent_task = current_task.primary_task if current_task.primary_task else current_task.secondary_task
                        if not parent_task:
                            break
                        elif parent_task.internal_task == 'LOOP':
                            found = new_task.get('os_password') == parent_task.loop_placeholder_var_1 or new_task.get('os_password') == parent_task.loop_placeholder_var_2 or \
                                    new_task.get('os_password') == parent_task.loop_placeholder_var_3 or new_task.get('os_password') == parent_task.loop_placeholder_var_4
                        current_task = parent_task
                    if not found:
                        ok = False
                        errors['os_password'] = 'The password should come from a placeholder variable but the specified variable does not exist in any loop above the SSH command.'
            else:
                if not new_task.get('os_password'):
                    ok = False
                    errors['os_password'] = 'This field may not be blank.'
        elif new_task.get('ssh_auth_type') == 'SSH_KEY':
            if not new_task.get('ssh_key_path'):
                ok = False
                errors['ssh_key_path'] = 'This field may not be blank.'
    elif new_task.get('internal_task') == 'LOOP':
        if not new_task.get('loop_value_list_1'):
            ok = False
            errors['loop_value_list_1'] = 'This field may not be blank.'
        else:
            if not new_task.get('loop_placeholder_var_1'):
                ok = False
                errors['loop_placeholder_var_1'] = 'This field may not be blank if value list 1 has value.'
        ok = check_value_list(errors, ok, new_task.get('loop_value_list_1'), new_task.get('loop_separator_1'), new_task.get('loop_value_list_2'), new_task.get('loop_separator_2'), new_task.get('loop_placeholder_var_2'), 2)
        ok = check_value_list(errors, ok, new_task.get('loop_value_list_1'), new_task.get('loop_separator_1'), new_task.get('loop_value_list_3'), new_task.get('loop_separator_3'), new_task.get('loop_placeholder_var_3'), 3)
        ok = check_value_list(errors, ok, new_task.get('loop_value_list_1'), new_task.get('loop_separator_1'), new_task.get('loop_value_list_4'), new_task.get('loop_separator_4'), new_task.get('loop_placeholder_var_4'), 4)
        ok = check_placeholder_vars(errors, ok, new_task, id, preceding_task_id)
        ok = check_req_encryption_value(errors, ok, new_task, 2)
        ok = check_req_encryption_value(errors, ok, new_task, 3)
        ok = check_req_encryption_value(errors, ok, new_task, 4)
    elif new_task.get('internal_task') == 'IF_ELSE':
        if not new_task.get('if_expression'):
            ok = False
            errors['if_expression'] = 'This field may not be blank.'
    return ok, errors


def check_req_encryption_value(errors, ok, new_task, ind):
    if not new_task.get('loop_value_list_'+str(ind)) and new_task.get('loop_requires_encryption_'+str(ind)):
        ok = False
        errors['loop_requires_encryption_'+str(ind)] = 'This field must not be checked if the value list '+str(ind)+' is empty.'
    return ok


def check_placeholder_vars(errors, ok, new_task, id, preceding_task_id):
    ok = check_placeholder_var(errors, ok, new_task, 1, 2)
    ok = check_placeholder_var(errors, ok, new_task, 1, 3)
    ok = check_placeholder_var(errors, ok, new_task, 1, 4)
    ok = check_placeholder_var(errors, ok, new_task, 2, 3)
    ok = check_placeholder_var(errors, ok, new_task, 2, 4)
    ok = check_placeholder_var(errors, ok, new_task, 3, 4)
    # check if a parent loop has the same placeholder var name
    if not id:
        if preceding_task_id.startswith('START'):
            return ok
        else:
            current_task = Task.objects.get(pk=preceding_task_id)
    else:
        current_task = Task.objects.get(pk=id)
    while 1==1:
        parent_task = current_task.primary_task if current_task.primary_task else current_task.secondary_task
        if not parent_task:
            break
        elif parent_task.internal_task == 'LOOP':
            ok = compare_placeholder_var_with(parent_task, new_task, ok, errors)
        current_task = parent_task
    # check if the children has the same placeholder variable name
    if id:
        ok = check_placeholder_vars_in_children(id, new_task, ok, errors)
    return ok


def check_placeholder_vars_in_children(id, task_to_compare, ok, errors):
    if id:
        child_tasks = Task.objects.filter(Q(primary_task=id) | Q(secondary_task=id))
        for child_task in child_tasks:
            if child_task.internal_task == 'IF_ELSE':
                ok = check_placeholder_vars_in_children(child_task.id, task_to_compare, ok, errors)
            elif child_task.internal_task == 'LOOP':
                ok = compare_placeholder_var_with(child_task, task_to_compare, ok, errors)
                ok = check_placeholder_vars_in_children(child_task.id, task_to_compare, ok, errors)
    return ok


def compare_placeholder_var_with(parent_task, new_task, ok, errors):
    for ind in range(1, 5):
        if new_task.get('loop_placeholder_var_' + str(ind)) and \
                new_task.get('loop_placeholder_var_' + str(ind)) in {parent_task.loop_placeholder_var_1, parent_task.loop_placeholder_var_2,
                                                                     parent_task.loop_placeholder_var_3, parent_task.loop_placeholder_var_4}:
            ok = False
            errors['loop_placeholder_var_' + str(ind)] = 'The variable name is already used in the LOOP "'+parent_task.name+'".'
    return ok


def check_placeholder_var(errors, ok, new_task, i, j):
    if new_task.get('loop_value_list_' + str(i)) and new_task.get('loop_value_list_' + str(j)) and \
            new_task.get('loop_placeholder_var_' + str(i)) == new_task.get('loop_placeholder_var_' + str(j)):
        ok = False
        errors['loop_placeholder_var_' + str(j)] = 'It must not have the same variable name as placeholder variable ' + str(i)+'.'
    return ok


def check_value_list(errors, ok, value_list_1, separator_1, value_list_x, separator_x, placeholder_var_x, ind):
    if not value_list_x and separator_x:
        ok = False
        errors['loop_separator_' + str(ind)] = 'It must not have value if the corresponding value list is empty.'
    if value_list_x:
        if bool(separator_1) ^ bool(separator_x):  # it is a XOR => should both separator_1 and separator_2 have values or being empty
            ok = False
            errors['loop_separator_' + str(ind)] = 'It must / mustn''t have the value if the Separator 1 has / doesn''t have.'
        if separator_1:
            if separator_1 and separator_x and len(value_list_1.split(separator_1)) != len(value_list_x.split(separator_x)):
                ok = False
                errors['loop_value_list_' + str(ind)] = 'The number of the element in the value list (' + \
                                                        str(len(value_list_x.split(separator_x))) + \
                                                        ') must be equal to the Value List 1 (' + \
                                                        str(len(value_list_1.split(separator_1))) + ').'
        if not placeholder_var_x:
            ok = False
            errors['loop_placeholder_var_' + str(ind)] = 'This field may not be blank if value list ' + str(ind)+ ' has value.'
    return ok


def validate_execution_flow(request, pk):
    logger.debug('validate_execution_flow is called - pk={}'.format(pk))
    # checking the rules of validation
    error = validate_one_floor(pk, ParentType.execution_flow)
    # alter the valid field of the execution flow
    obj = ExecutionFlow.objects.get(pk=pk)
    # if error was found then set the flow to INVALID
    if obj.status != 'FROZEN':
        status = 'VALID'
        if error is not None:
            status = 'INVALID'
        # alter the valid field of the execution flow
        obj = ExecutionFlow.objects.get(pk=pk)
        obj.status = status
        obj.save()
    return error


def validate_one_floor(parent_id, parent_type):
    ifelseloop_list = []
    # get the tasks for this branch only
    tasks_for_one_floor = FlowBuilder.get_tasks_on_one_floor(parent_id, parent_type)
    for task_for_one_floor in tasks_for_one_floor:
        # check the 1st rule (if more rules have to be added then consider using Rule class or similar !!!!)
        if task_for_one_floor.internal_task == 'IF_ELSE':
            ifelseloop_list.append(task_for_one_floor.id)
            # minus 1 is needed as the ordr starts from 1 but the index from 0
            ordr = task_for_one_floor.ordr-1
            # if order (of the given task) - 1 is not in the list (the flow doesn't start with an IF-ELSE) or the preceding task is not an COMMAND_BY_WORKER or COMMAND_USING_SSH and $COMMANDOUTPUT is in the condition then ERROR!
            if '$COMMANDOUTPUT' in task_for_one_floor.if_expression and \
                    (ordr == 0 or
                     tasks_for_one_floor[ordr-1].internal_task not in ['COMMAND_BY_WORKER', 'COMMAND_USING_SSH']):
                return 'The task ({}) is an IF-ELSE (and its condition contains $COMMANDOUTPUT) but it is not preceded by an COMMAND_BY_WORKER or COMMAND_USING_SSH command!'.format(task_for_one_floor.name)
        elif task_for_one_floor.internal_task == 'LOOP':
            if len(Task.objects.filter(primary_task=task_for_one_floor.pk)) == 0:
                return 'The task ({}) is an LOOP but it is empty!'.format(task_for_one_floor.name)
            else:
                ifelseloop_list.append(task_for_one_floor.id)
        elif task_for_one_floor.internal_task == 'FAILURE':
            if len(tasks_for_one_floor) > task_for_one_floor.ordr:
                return 'No task can be after a FAILURE [{}] command!'.format(task_for_one_floor.name)
    # if the specific task is an IF-ELSE then put it in a list and jump into them here, at the end -> to make it recursive
    for id in ifelseloop_list:
        error = validate_one_floor(id, ParentType.primary_task)
        if error is not None:
            return error
        error = validate_one_floor(id, ParentType.secondary_task)
        if error is not None:
            return error
    return None


def _get_tasks_of_execution_flow(task):
    tasks = [task]
    if task['internal_task'] == 'IF_ELSE' or task['internal_task'] == 'LOOP':
        # looping through the IF and ELSE branches
        tasks_entities = Task.objects.filter(Q(primary_task=task['id']) | Q(secondary_task=task['id'])).order_by('ordr') \
                if task['internal_task'] == 'IF_ELSE' else Task.objects.filter(primary_task=task['id']).order_by('ordr')
        serializer = TaskSerializer(tasks_entities, many=True)
        for task in serializer.data:
            tasks.extend(_get_tasks_of_execution_flow(task))
    return tasks


def copy_tasks(parent_id, new_parent_obj, parent_type):
    ifelseloop_list = []
    # get the tasks for this branch only
    tasks_for_one_floor = FlowBuilder.get_tasks_on_one_floor(parent_id, parent_type)
    for task_for_one_floor in tasks_for_one_floor:
        orig_id = task_for_one_floor.pk
        # save the new task
        task_for_one_floor.pk = None
        task_for_one_floor.execution_flow = new_parent_obj if parent_type == ParentType.execution_flow else None
        task_for_one_floor.primary_task = new_parent_obj if parent_type == ParentType.primary_task else None
        task_for_one_floor.secondary_task = new_parent_obj if parent_type == ParentType.secondary_task else None
        logger.debug('                  task_for_one_floor={}'.format(task_for_one_floor.name + '  ,  ' + str(task_for_one_floor.execution_flow) + '  ,  ' + str(task_for_one_floor.primary_task) + '  ,  ' + str(task_for_one_floor.secondary_task)))
        task_for_one_floor.save()
        # at IF-ELSE / LOOP recursion is needed
        if task_for_one_floor.internal_task in {'IF_ELSE', 'LOOP'}:
            ifelseloop_list.append({"parent_id": orig_id,
                                    "new_parent_obj": task_for_one_floor})
    # recursive call
    for rec in ifelseloop_list:
        copy_tasks(rec['parent_id'], rec['new_parent_obj'], ParentType.primary_task)
        copy_tasks(rec['parent_id'], rec['new_parent_obj'], ParentType.secondary_task)


# the following 3 functions are for updating the hosts of the execution flow
def update_hosts(execution_flow):
    all_hosts = [x.host for x in get_all_tasks(execution_flow.id, 'EXECUTION_FLOW', [])
                 if (x.internal_task == 'COMMAND_BY_WORKER' or x.internal_task == 'COMMAND_USING_SSH') and x.host is not None]
    hosts = sorted( list( set(all_hosts) ) )
    execution_flow.hosts = ', '.join(hosts)
    execution_flow.save()


def get_execution_flow(task):
    if task.execution_flow is not None:
        return task.execution_flow
    else:
        if task.primary_task is not None:
            return get_execution_flow( get_object_or_404(Task, pk=task.primary_task.id) )
        else:
            return get_execution_flow(get_object_or_404(Task, pk=task.secondary_task.id))


def get_all_tasks(parent_id, parent_type, tasks):
    if parent_type == 'EXECUTION_FLOW':
        # querying the tasks that belong to the execution_flow
        _tasks = Task.objects.filter(execution_flow=parent_id)
    elif parent_type == 'PRIMARY_TASK':
        # querying the tasks that belong to the if branch of an IF-ELSE
        _tasks = Task.objects.filter(primary_task=parent_id)
    elif parent_type == 'SECONDARY_TASK':
        # querying the tasks that belong to the if branch of an IF-ELSE
        _tasks = Task.objects.filter(secondary_task=parent_id)
    tasks.extend(_tasks)
    for _task in _tasks:
        get_all_tasks(_task, 'PRIMARY_TASK', tasks)
        get_all_tasks(_task, 'SECONDARY_TASK', tasks)
    return tasks


def encrypt_os_password(request, task):
    if 'is_reset_password' in request.data:
        if request.data['is_reset_password']:
            request.data['os_password'] = encrypt_password(request.data['os_password'])
        else:
            request.data['os_password'] = task.os_password


def encrypt_password(clean_text_pwd):
    if clean_text_pwd:
        f = open(settings.PASSWORD_ENCRYPTION_KEY_FILE, 'r')
        key = f.read()
        return encrypt(key, clean_text_pwd)
    else:
        return clean_text_pwd


def encrypt_value_list_if_needed(request, task):
    for i in range(1, 5):
        encrypted_value_list = []
        if request.data['loop_requires_encryption_'+str(i)]:
            if request.data['is_reset_encr_value_list_'+str(i)]:
                if request.data['loop_separator_' + str(i)]:
                    for value in request.data['loop_value_list_'+str(i)].split(request.data['loop_separator_'+str(i)]):
                        encrypted_value_list.append(encrypt_password(value))
                    request.data['loop_value_list_'+str(i)] = ",".join(encrypted_value_list)
                    request.data['loop_separator_' + str(i)] = ","
                else:
                    request.data['loop_value_list_' + str(i)] = encrypt_password(request.data['loop_value_list_'+str(i)])
            else:
                request.data['loop_value_list_' + str(i)] = getattr(task, 'loop_value_list_'+str(i))
                request.data['loop_separator_' + str(i)] = getattr(task, 'loop_separator_'+str(i))


def is_num(data):
    try:
        int(data)
        return True
    except ValueError:
        return False
