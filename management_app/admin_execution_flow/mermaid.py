from .models import ExecutionFlow, Task
from django.db.models.functions import Coalesce
from enum import Enum
import html
import textwrap
import re

class FlowNodes:

    @staticmethod
    def definition():
        return 'flowchart LR'

    @staticmethod
    def node_start():
        return '  execution_flow_start((fa:fa-play  Start))'

    @staticmethod
    def node_if_else(id, name):
        # execution_flow_node_4{Is it OK?}
        return '  execution_flow_node_{0}{{fa:fa-code-branch {1}}}'.format(id, FlowNodes.wrap(name))

    @staticmethod
    def node_command_by_worker(id, name):
        return '  execution_flow_node_{0}[/fa:fa-hammer {1}/]'.format(id, FlowNodes.wrap(name))

    @staticmethod
    def node_command_using_ssh(id, name):
        return '  execution_flow_node_{0}([fa:fa-terminal {1}])'.format(id, FlowNodes.wrap(name))

    @staticmethod
    def node_loop(id, name):
        return '  \nsubgraph execution_flow_node_{0}[fa:fa-sync {1}]\n  direction LR\n'.format(id, FlowNodes.wrap(name))

    @staticmethod
    def node_sending_email(id, name):
        return '  execution_flow_node_{0}((fa:fa-envelope-square {1}))'.format(id, FlowNodes.wrap(name))

    @staticmethod
    def node_failure(id, name):
        return '  execution_flow_node_{0}>fa:fa-bomb {1}]'.format(id, FlowNodes.wrap(name))

    @staticmethod
    def clickandtooltip_if_else(id, expression, order):
        return '  click execution_flow_node_{} dummyFunc "<div><h4>If-else task</h4></div> <table> <tr><td><b>Expression:</b></td><td>{}</td></tr> </table>"' \
            .format(id,
                    html.escape(html.escape(expression)).replace('&#x27;','&apos;'))

    @staticmethod
    def clickandtooltip_command_by_worker(id, command, host, order, os_user, output_pattern):
        return '  click execution_flow_node_{} dummyFunc "<div><h4>Command executed by the Reaction Worker</h4></div> <table> <tr><td><b>Host:</b></td><td>{}</td></tr> ' \
               '<tr><td><b>Command:</b></td><td>{}</td></tr> <tr><td><b>OS user:</b></td><td>{}</td></tr> <tr><td><b>Output pattern:</b></td><td>{}</td></tr> </table>"'\
            .format(id,
                    html.escape(html.escape(host).replace('&#x27;','&apos;')),
                    html.escape(html.escape(command).replace('&#x27;','&apos;')), # the apostrophe was displayed in tooltip like &&x27; -> replace the hex code to HTML entity
                    '' if os_user is None else os_user,
                    '' if output_pattern is None else html.escape(html.escape(output_pattern)).replace('&#x27;','&apos;'))

    @staticmethod
    def clickandtooltip_command_using_ssh(id, command, host, order, os_user, output_pattern, execution_timeout):
        return '  click execution_flow_node_{} dummyFunc "<div><h4>Command executed using SSH</h4></div> <table> <tr><td><b>Host:</b></td><td>{}</td></tr> ' \
               '<tr><td><b>Command:</b></td><td>{}</td></tr> <tr><td><b>OS user:</b></td><td>{}</td></tr> <tr><td><b>Output pattern:</b></td><td>{}</td></tr>' \
               '<tr><td><b>Execution timeout:</b></td><td>{}</td></tr> </table>"' \
            .format(id,
                    html.escape(html.escape(host).replace('&#x27;','&apos;')),
                    html.escape(html.escape(command).replace('&#x27;','&apos;')), # the apostrophe was displayed in tooltip like &&x27; -> replace the hex code to HTML entity
                    '' if os_user is None else os_user,
                    '' if output_pattern is None else html.escape(html.escape(output_pattern)).replace('&#x27;','&apos;'),
                    '' if execution_timeout is None else execution_timeout)

    @staticmethod
    def click_loop(id):
        return '  click execution_flow_node_{} dummyFunc'.format(id)

    @staticmethod
    def tooltip_loop(value_list_1, separator_1, loop_placeholder_var_1, value_list_2, separator_2, loop_placeholder_var_2, value_list_3, separator_3, loop_placeholder_var_3,
                     value_list_4, separator_4, loop_placeholder_var_4):
        tooltip = '<div><h4>Loop</h4></div> <table> ' +\
                  FlowNodes.fill_loop_tooltip(value_list_1, separator_1, loop_placeholder_var_1, 1) +\
                  FlowNodes.fill_loop_tooltip(value_list_2, separator_2, loop_placeholder_var_2, 2) +\
                  FlowNodes.fill_loop_tooltip(value_list_3, separator_3, loop_placeholder_var_3, 3) +\
                  FlowNodes.fill_loop_tooltip(value_list_4, separator_4, loop_placeholder_var_4, 4)
        return tooltip + '</table>'

    @staticmethod
    def fill_loop_tooltip(value_list, separator, loop_placeholder_var, ind):
        if value_list:
            return '<tr><td><b>Value list {}:</b></td><td>{}</td></tr> <tr><td><b>Separator {}:</b></td><td>{}</td></tr> <tr><td><b>Placeholder variable {}:</b></td><td>{}</td></tr>' \
                .format(ind,
                        html.escape(html.escape(value_list[:100]).replace('&#x27;','&apos;')) if value_list else '',
                        ind,
                        html.escape(html.escape(separator).replace('&#x27;','&apos;')) if separator else '',
                        ind,
                        html.escape(html.escape(loop_placeholder_var).replace('&#x27;', '&apos;')) if loop_placeholder_var else '')
        else:
            return ''

    @staticmethod
    def clickandtooltip_sending_mail(id, recipients, subject, content):
        return '  click execution_flow_node_{} dummyFunc "<div><h4>Sending e-mail</h4></div> <table> <tr><td><b>Recipients:</b></td><td>{}</td></tr> ' \
               '<tr><td><b>Subject:</b></td><td>{}</td></tr> <tr><td><b>Content:</b></td><td>{}</td></tr> </table>"' \
            .format(id,
                    recipients,
                    subject,
                    content.replace('<','&lt;').replace('>','&gt;'))

    @staticmethod
    def clickandtooltip_failure(id):
        return '  click execution_flow_node_{} dummyFunc "<div><h4>Fail the flow task</h4></div>"'.format(id)

    @staticmethod
    def style_command_using_ssh(id):
        return '  style execution_flow_node_{} fill:#9bd39b'.format(id)

    @staticmethod
    def style_command_by_worker(id):
        return '  style execution_flow_node_{} fill:#CDE498'.format(id,id)

    @staticmethod
    def style_loop(id):
        return '  style execution_flow_node_{} fill:#cdffb2,stroke:black,stroke-width:1px,stroke-dasharray: 5 2'.format(id)

    @staticmethod
    def style_if_else(id):
        return '  style execution_flow_node_{} fill:#f7f6b4'.format(id)

    @staticmethod
    def style_sending_email(id):
        return '  style execution_flow_node_{} fill:#cececa'.format(id)

    @staticmethod
    def style_failure(id):
        return '  style execution_flow_node_{} fill:#f4bfbe'.format(id)

    @staticmethod
    def wrap(name):
        aa = '<br>'.join( textwrap.wrap(name or '-', width=26) )
        return aa


class FlowBuilder:

    @staticmethod
    def build(id):
        # building the diagram text
        diagram = FlowBuilder.build_one_floor(id, ParentType.execution_flow)
        return FlowNodes.definition() + '\n' + \
               (diagram['diagramText'] if diagram['diagramText'] != '' else FlowNodes.node_start()) + \
                diagram['clickAndTooltipText'] + \
               (diagram['styleText'] if diagram['styleText'] != '' else ''),\
               diagram['tooltip_loop']

    @staticmethod
    # parent_type can be 'execution_flow', 'primary_task', 'secondary_task'
    def build_one_floor(parent_id, parent_type):
        # get the tasks for this branch only
        tasks_for_one_floor = FlowBuilder.get_tasks_on_one_floor(parent_id, parent_type)
        # initializing the previous node
        prev_node = FlowNodes.node_start() if parent_type == ParentType.execution_flow else 'execution_flow_node_'+str(parent_id)
        diagram_text = ''
        click_and_tooltip_text = ''
        tooltip_loop = {}
        style_text = ''
        prev_task_type = ''
        last_nodes = []
        parent_internal_task = None if parent_type == ParentType.execution_flow else Task.objects.get(pk=parent_id).internal_task
        arrow_type = FlowBuilder.get_arrow(parent_type, parent_internal_task)
        # looping through the tasks and building the worklfow
        last_nodes_to_be_removed = []
        for task_for_one_floor in tasks_for_one_floor:
            # specifying the current node
            if task_for_one_floor.internal_task == 'COMMAND_BY_WORKER':
                current_node = FlowNodes.node_command_by_worker(task_for_one_floor.id, task_for_one_floor.name)
                click_and_tooltip_text += '\n' + FlowNodes.clickandtooltip_command_by_worker(task_for_one_floor.id, task_for_one_floor.command, task_for_one_floor.host, task_for_one_floor.ordr, task_for_one_floor.os_user, task_for_one_floor.output_pattern)
                style_text += '\n' + FlowNodes.style_command_by_worker(task_for_one_floor.id)
            elif task_for_one_floor.internal_task == 'COMMAND_USING_SSH':
                current_node = FlowNodes.node_command_using_ssh(task_for_one_floor.id, task_for_one_floor.name)
                click_and_tooltip_text += '\n' + FlowNodes.clickandtooltip_command_using_ssh(task_for_one_floor.id, task_for_one_floor.command, task_for_one_floor.host, task_for_one_floor.ordr, task_for_one_floor.os_user, task_for_one_floor.output_pattern, task_for_one_floor.execution_timeout)
                style_text += '\n' + FlowNodes.style_command_using_ssh(task_for_one_floor.id)
            elif task_for_one_floor.internal_task == 'LOOP':
                current_node = FlowNodes.node_loop(task_for_one_floor.id, task_for_one_floor.name)
                click_and_tooltip_text += '\n' + FlowNodes.click_loop(task_for_one_floor.id)
                tooltip_loop[task_for_one_floor.id] = FlowNodes.tooltip_loop(task_for_one_floor.loop_value_list_1, task_for_one_floor.loop_separator_1, task_for_one_floor.loop_placeholder_var_1,
                                                                             task_for_one_floor.loop_value_list_2, task_for_one_floor.loop_separator_2, task_for_one_floor.loop_placeholder_var_1,
                                                                             task_for_one_floor.loop_value_list_3, task_for_one_floor.loop_separator_3, task_for_one_floor.loop_placeholder_var_2,
                                                                             task_for_one_floor.loop_value_list_4, task_for_one_floor.loop_separator_4, task_for_one_floor.loop_placeholder_var_3)
                style_text += '\n' + FlowNodes.style_loop(task_for_one_floor.id)
            elif task_for_one_floor.internal_task == 'IF_ELSE':
                current_node = FlowNodes.node_if_else(task_for_one_floor.id, task_for_one_floor.name)
                click_and_tooltip_text += '\n' + FlowNodes.clickandtooltip_if_else(task_for_one_floor.id, task_for_one_floor.if_expression, task_for_one_floor.ordr)
                style_text += '\n' + FlowNodes.style_if_else(task_for_one_floor.id)
            elif task_for_one_floor.internal_task == 'EMAIL_SENDING':
                current_node = FlowNodes.node_sending_email(task_for_one_floor.id, task_for_one_floor.name)
                click_and_tooltip_text += '\n' + FlowNodes.clickandtooltip_sending_mail(task_for_one_floor.id, task_for_one_floor.mail_recipients, task_for_one_floor.mail_subject, task_for_one_floor.mail_content)
                style_text += '\n' + FlowNodes.style_sending_email(task_for_one_floor.id)
            elif task_for_one_floor.internal_task == 'FAILURE':
                current_node = FlowNodes.node_failure(task_for_one_floor.id, task_for_one_floor.name)
                click_and_tooltip_text += '\n' + FlowNodes.clickandtooltip_failure(task_for_one_floor.id)
                style_text += '\n' + FlowNodes.style_failure(task_for_one_floor.id)
            # adding the prev node + arrow + current node
            if prev_task_type == 'IF_ELSE' or prev_task_type == 'LOOP':
                # if the previous task was an IF-ELSE then the last nodes coming from the branches of IF-ELSE have to be used to connect to the current node
                directions = set() # it is for avoiding double arrows to the same element
                for last_node in last_nodes:
                    node_id_connecting_from = re.findall(r'execution_flow_node_([0-9]+).*', last_node)[0]
                    arrow = last_node[re.search(r'([0-9]+)', last_node).end():]
                    real_last_node = str(FlowBuilder.get_real_last_node(node_id_connecting_from, task_for_one_floor))
                    if real_last_node+'-'+str(task_for_one_floor.id) not in directions:
                        diagram_text += '\n' + 'execution_flow_node_' + real_last_node + \
                                        arrow +\
                                        ('execution_flow_node_'+str(task_for_one_floor.id) if task_for_one_floor.internal_task=='LOOP' else current_node) + '\n'
                        directions.add(real_last_node+'-'+str(task_for_one_floor.id))
                    last_nodes_to_be_removed.append(last_node)
                diagram_text += ('\n' + current_node) if task_for_one_floor.internal_task=='LOOP' else ''
                # the nodes have to be removed from the last_nodes list that were already used (see above)
                try:
                    last_nodes = [x for x in last_nodes if x not in last_nodes_to_be_removed]
                except ValueError:
                    pass
                if prev_node not in diagram_text:
                    diagram_text = prev_node + '\n' + diagram_text
            else:
                if task_for_one_floor.primary_task and task_for_one_floor.primary_task.internal_task == 'LOOP' and len(tasks_for_one_floor) == 1:
                    diagram_text += current_node + '\n'
                elif task_for_one_floor.primary_task and task_for_one_floor.primary_task.internal_task == 'LOOP' and len(tasks_for_one_floor) > 1 and task_for_one_floor.ordr == 1:
                    None
                else:
                    diagram_text += prev_node + arrow_type + ('execution_flow_node_'+str(task_for_one_floor.id) if task_for_one_floor.internal_task=='LOOP' else '') + current_node + '\n'
            # at IF-ELSE we have to go to the branches
            if task_for_one_floor.internal_task == 'IF_ELSE':
                # dive into the if branch
                click_and_tooltip_text, tooltip_loop, diagram_text, style_text = FlowBuilder.dive_into(click_and_tooltip_text, tooltip_loop, current_node, diagram_text, last_nodes, style_text, task_for_one_floor, ParentType.primary_task)
                # dive into the else branch
                click_and_tooltip_text, tooltip_loop, diagram_text, style_text = FlowBuilder.dive_into(click_and_tooltip_text, tooltip_loop, current_node, diagram_text, last_nodes, style_text, task_for_one_floor, ParentType.secondary_task)
            elif task_for_one_floor.internal_task == 'LOOP':
                click_and_tooltip_text, tooltip_loop, diagram_text, style_text = FlowBuilder.dive_into(click_and_tooltip_text, tooltip_loop, 'execution_flow_node_{0}'.format(task_for_one_floor.id), diagram_text, last_nodes, style_text, task_for_one_floor, ParentType.primary_task)
                diagram_text += 'end\n'
            prev_node = current_node
            prev_task_type = task_for_one_floor.internal_task
            arrow_type = FlowBuilder.get_arrow(ParentType.execution_flow, task_for_one_floor.internal_task)
        # the last node has to be added if the last task on this floor is not IF_ELSE (if it is IF-ELSE then we have to go deeper so nodes on the lower level have to be added)
        if prev_task_type != 'IF_ELSE' and prev_task_type != 'LOOP':
            if parent_internal_task == 'LOOP':
                last_nodes.append('execution_flow_node_'+str(parent_id) + " " + FlowBuilder.get_arrow(ParentType.execution_flow, None))
            else:
                last_nodes.append(prev_node + " " + FlowBuilder.get_arrow(ParentType.execution_flow, None))
        return {'diagramText': diagram_text,
                'clickAndTooltipText': click_and_tooltip_text,
                'tooltip_loop': tooltip_loop,
                'styleText': style_text,
                'lastNodes': last_nodes}

    @staticmethod
    def are_on_same_level(node1, node2):
        return node1.primary_task and node2.primary_task and node1.primary_task.id == node2.primary_task.id or\
               node1.secondary_task and node2.secondary_task and node1.secondary_task.id == node2.secondary_task.id or\
               node1.execution_flow and node2.execution_flow and node1.execution_flow.id == node2.execution_flow.id

    @staticmethod
    def get_real_last_node(node_id_connecting_from, node_to_connect_to):
        node_connecting_from = Task.objects.get(pk=node_id_connecting_from)
        # if 'node_connecting_from' is on the same level as 'node_to_connect_to' then return
        if FlowBuilder.are_on_same_level(node_connecting_from, node_to_connect_to):
            return node_id_connecting_from
        # else if the parent of 'node_connecting_from' is an IF-ELSE and this parent is on the same level as 'node_to_connect_to' then return
        parent = node_connecting_from.primary_task if node_connecting_from.primary_task else node_connecting_from.secondary_task
        if parent and parent.internal_task == 'IF_ELSE' and FlowBuilder.are_on_same_level(parent, node_to_connect_to):
            return node_connecting_from.id
        # else go one level up
        return FlowBuilder.get_real_last_node(
            node_connecting_from.primary_task.id if node_connecting_from.primary_task else node_connecting_from.secondary_task.id,
            node_to_connect_to)

    @staticmethod
    def dive_into(click_and_tooltip_text, tooltip_loop, current_node, diagram_text, last_nodes, style_text, task_for_one_floor, parent_type):
        diagram_part = FlowBuilder.build_one_floor(task_for_one_floor.id, parent_type)
        diagram_text += diagram_part['diagramText']
        click_and_tooltip_text += diagram_part['clickAndTooltipText']
        tooltip_loop.update(diagram_part['tooltip_loop'])
        style_text += diagram_part['styleText']
        if not diagram_part['diagramText']:
            last_nodes.append(current_node + FlowBuilder.get_arrow(parent_type, task_for_one_floor.internal_task));
        else:
            last_nodes.extend(diagram_part['lastNodes'])
        return click_and_tooltip_text, tooltip_loop, diagram_text, style_text

    @staticmethod
    def get_tasks_on_one_floor(parent_id, parent_type):
        if parent_type == ParentType.execution_flow:
            # querying the tasks that belong to the execution_flow
            return Task.objects.filter(execution_flow=parent_id).order_by('ordr')
        elif parent_type == ParentType.primary_task:
            # querying the tasks that belong to the if branch of an IF-ELSE
            return Task.objects.filter(primary_task=parent_id).order_by('ordr')
        elif parent_type == ParentType.secondary_task:
            # querying the tasks that belong to the if branch of an IF-ELSE
            return Task.objects.filter(secondary_task=parent_id).order_by('ordr')

    @staticmethod
    def get_arrow(parent_type, internal_task):
        if parent_type == ParentType.execution_flow or internal_task == 'LOOP':
            return "-->"
        elif parent_type == ParentType.primary_task:
            return "-- True -->"
        elif parent_type == ParentType.secondary_task:
            return "-- False -->"


class ParentType(Enum):
    execution_flow = 0
    primary_task = 1
    secondary_task = 2
