from django import forms

from .models import ExecutionFlow


class ExecutionFlowForm(forms.ModelForm):
    error_mail_sending_recipients = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 2}) )
    start_mail_sending_recipients = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 2}))

    class Meta:
        model = ExecutionFlow
        fields = ['name', 'time_between_exec', 'execution_time', 'access_group', 'error_mail_sending_recipients', 'start_mail_sending_recipients']

    def __init__(self, *args, **kwargs):
        super(ExecutionFlowForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['time_between_exec'].widget.attrs['class'] = 'form-control'
        self.fields['time_between_exec'].widget.attrs['style'] = 'width: 150px;'
        self.fields['execution_time'].widget.attrs['class'] = 'form-control'
        self.fields['execution_time'].widget.attrs['style'] = 'width: 150px;'
        self.fields['access_group'].widget.attrs['class'] = 'js-example-basic-multiple'
        self.fields['access_group'].widget.attrs['style'] = 'width: 100%;'
        self.fields['access_group'].widget.attrs['multiple'] = 'multiple'
        self.fields['error_mail_sending_recipients'].widget.attrs['class'] = 'form-control'
        self.fields['error_mail_sending_recipients'].widget.attrs['style'] = 'resize: vertical; height: 55px; vertical-align: text-top;'
        self.fields['start_mail_sending_recipients'].widget.attrs['class'] = 'form-control'
        self.fields['start_mail_sending_recipients'].widget.attrs['style'] = 'resize: vertical; height: 55px; vertical-align: text-top;'

    def clean(self):
        cleaned_data = super(ExecutionFlowForm, self).clean()
        # sorting the access groups
        if cleaned_data.get('access_group'):
           access_group = sorted( list( set( [x.strip() for x in cleaned_data.get('access_group').split(',') if x] ) ) )
           cleaned_data['access_group'] = ','.join( access_group)
        else:
            cleaned_data['access_group'] = None
        return cleaned_data

