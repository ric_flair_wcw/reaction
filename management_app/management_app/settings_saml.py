# ####################### Please do not change the following! #######################
# ###################################################################################
import saml2.saml
from os import path, environ
from django.conf import settings
# see why it is needed in settings.py / LOGIN_URL
if environ.get('REACTION_SUBDOMAIN', ''):
    LOGIN_URL = '/' + environ.get('REACTION_SUBDOMAIN', '') + '/saml2/login/'
else:
    LOGIN_URL = '/saml2/login/'

# ####################### Configuration settings to be changed #######################
# ###################################################################################
# djangosaml2 configuration
# See the full configuration in https://djangosaml2.readthedocs.io/contents/setup.html#configuration
SAML_CONFIG = {
    # full path to the xmlsec1 binary program
    'xmlsec_binary': '/usr/bin/xmlsec1',
    # your entity id, usually your subdomain plus the url to the metadata view
    'entityid': 'https://localhost:8000/saml2/acs/',
    # directory with attribute mapping
    'attribute_map_dir': path.join(settings.BASE_DIR, 'management_app/attribute-maps'),
    # Permits to have attributes not configured in attribute-mappings, otherwise...without OID will be rejected
    'allow_unknown_attributes': True,
    # this block states what services we provide
    'service': {
        # we are just a lonely SP
        'sp': {
            'name': 'SP',
            'name_id_format': saml2.saml.NAMEID_FORMAT_TRANSIENT,
            'endpoints': {
                # url and binding to the assertion consumer service view
                'assertion_consumer_service': [
                    ('https://localhost:8000/saml2/acs/', saml2.BINDING_HTTP_POST),
                    ('https://localhost:8000/saml2/acs/', saml2.BINDING_HTTP_REDIRECT),
                ],
                # url and binding to the single logout service view
                'single_logout_service': [
                    # Disable next two lines for HTTP_REDIRECT for IDP's that only support HTTP_POST. Ex. Okta:
                    ('https://localhost:8000/saml2/ls/', saml2.BINDING_HTTP_REDIRECT),
                    ('https://localhost:8000/saml2/ls/post', saml2.BINDING_HTTP_POST),
                ],
            },
            'signing_algorithm':  saml2.xmldsig.SIG_RSA_SHA256,
            'digest_algorithm':  saml2.xmldsig.DIGEST_SHA256,
            # Mandates that the identity provider MUST authenticate the presenter directly rather than rely on a previous security context.
            'force_authn': True,
            # Enable AllowCreate in NameIDPolicy.
            'name_id_format_allow_create': False,
            # attributes that this project need to identify a user
            'required_attributes': ['email'],
            # attributes that may be useful to have but not required
            'optional_attributes': ['surname'],
            'want_response_signed': False,
            'authn_requests_signed': False,
            'logout_requests_signed': True,
            # Indicates that Authentication Responses to this SP must be signed. If set to True, the SP will not consume any SAML Responses that are not signed.
            'want_assertions_signed': True,
            'only_use_keys_in_metadata': True,
            # When set to true, the SP will consume unsolicited SAML Responses, i.e. SAML Responses for which it has not sent a respective SAML Authentication Request.
            'allow_unsolicited': True,
            # in this section the list of IdPs we talk to are defined
            # This is not mandatory! All the IdP available in the metadata will be considered instead.
            'idp': {
                # please see in https://djangosaml2.readthedocs.io/contents/setup.html#idp-scoping:~:text=%27single_sign_on_service%27%3A%20%7B
            },
        },
    },
    # where the remote metadata is stored, local, remote or mdq server. One metadatastore or many ...
    'metadata': {
        # 'local': [path.join(BASE_DIR, 'management_app/azure-ad-metadata.xml')],
        'remote': [{"url": "https://login.microsoftonline.com/8d469bba-ae86-4fe1-a36d-fa9d26ec8ab6/federationmetadata/2007-06/federationmetadata.xml?appid=c05d2876-bac3-4b32-898d-5bd846510974"},],
        # 'mdq': [{"url": "https://ds.testunical.it",
        #          "cert": "certficates/others/ds.testunical.it.cert",
        #         }]
    },
    # set to 1 to output debugging information
    'debug': 1,
    # Signing
    'key_file': path.join(settings.BASE_DIR, 'management_app/azure_ad_sso_saml_signing_private.key'),  # private part
    'cert_file': path.join(settings.BASE_DIR, 'management_app/azure_ad_sso_saml_signing_public.cert'),  # public part
    # Encryption
    'encryption_keypairs': [{
        'key_file': path.join(settings.BASE_DIR, 'management_app/azure_ad_sso_saml_signing_private.key'),  # private part
        'cert_file': path.join(settings.BASE_DIR, 'management_app/azure_ad_sso_saml_signing_public.cert'),  # public part
    }],
    # own metadata settings
    'contact_person': [
        {'given_name': 'Ric',
         'sur_name': 'Flair',
         'company': 'WCW',
         'email_address': 'rf@acme.hu',
         'contact_type': 'technical'},
    ],
    # you can set multilanguage information here
    'organization': {
        'name': [('WCW', 'hu'), ('WCW', 'en')],
        'display_name': [('WCW', 'hu'), ('WCW', 'en')],
        'url': [('http://www.acme.hu', 'es'), ('http://www.acme.com', 'en')],
    },
}
# By default djangosaml2 will do a query on the User model with the USERNAME_FIELD attribute but you can change it to
SAML_DJANGO_USER_MAIN_ATTRIBUTE = 'username'
# to configure is the mapping of SAML2 user attributes to Django user attributes
SAML_ATTRIBUTE_MAPPING = {
    'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name': ('username', 'email'),
    'http://schemas.microsoft.com/identity/claims/displayname': ('first_name', ),
    'http://schemas.microsoft.com/ws/2008/06/identity/claims/role': ('user_permissions')
}
# first checking the SAML Identity Provider and second the embedded Django authentication when auth...ing a user
AUTHENTICATION_BACKENDS = (
    'djangosaml2.backends.Saml2Backend',
    'django.contrib.auth.backends.ModelBackend',
)
# configure the SAML cookie name
SAML_SESSION_COOKIE_NAME = 'saml_session'
SESSION_COOKIE_SECURE = True
# to choose your preferred binding for SP initiated sso requests
SAML_DEFAULT_BINDING = saml2.BINDING_HTTP_POST
# to choose your preferred binding for SP initiated logout requests
SAML_LOGOUT_REQUEST_PREFERRED_BINDING = saml2.BINDING_HTTP_REDIRECT
# to ignore these errors when logging out and SAML IDP will return an error on invalid conditions
SAML_IGNORE_LOGOUT_ERRORS = True
# configure djangosaml2 to create such user if it is not already in the Django database
SAML_CREATE_UNKNOWN_USER = True
SAML_USE_NAME_ID_AS_USERNAME = True
