"""management_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import include
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from common.views import index


urlpatterns = [
    url(r'^',      include('admin_event_source.urls')),
    url(r'^',      include('admin_event_source_type.urls')),
    url(r'^',      include('admin_errordetector.urls')),
    url(r'^',      include('admin_execution_flow.urls')),
    url(r'^',      include('monitoring.urls')),
    url(r'^',      include('scheduler.urls')),
    url(r'^',      include('executor.urls')),
    url(r'^',      include('approval.urls')),
    url(r'^',      include('statistics.urls')),
    url(r'^',      include('worker_status.urls')),
    url(r'^',      include('dashboard.urls')),

    url(r'^help$',  TemplateView.as_view(template_name='help.html'), name='helpp'),

    url(r'^admin/',         admin.site.urls),

    url(r'^api-auth/',      include('rest_framework.urls', namespace='rest_framework')),
    
    url(r'^$', index, name='main'),

    url(r'^loggedout$', TemplateView.as_view(template_name='loggedout.html')),

]
if settings.REACTION_AUTH_TYPE == 'SAML':
    urlpatterns.append(url(r'^$', index, name='login'))
    urlpatterns.insert(0, url(r'^saml2/', include('djangosaml2.urls')))
else:
    urlpatterns.append(url(r'^accounts/', include('django.contrib.auth.urls')))

# for custom error handling
handler500 = 'common.view_utils.custom_500'
handler404 = 'common.view_utils.custom_404'
handler403 = 'common.view_utils.custom_403'

# if settings.DEBUG:
#    import debug_toolbar
#    urlpatterns = [
#        url(r'^__debug__/', include(debug_toolbar.urls)),
#    ] + urlpatterns