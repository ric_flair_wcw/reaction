# ####################### Please do not change the following! #######################
# ###################################################################################
import pymysql
pymysql.install_as_MySQLdb()

import sys, os
modname = "%s.settings" % ".".join(__name__.split('.')[:-1])
globals().update(vars(sys.modules[modname]))

# ####################### Configuration settings to be changed #######################
# ###################################################################################
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# DEBUG_TOOLBAR_CONFIG = {'INSERT_BEFORE': '</head>'}
# enable requests debugging; turn it off in production
#import http.client as http_client
#http_client.HTTPConnection.debuglevel = 1

# If you set DEBUG to False, you also need to properly set the ALLOWED_HOSTS setting.
ALLOWED_HOSTS = ['*']
INTERNAL_IPS = '127.0.0.1'

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
DATABASES = {
    'default': {
        'NAME': 'reactionstore',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'reaction',
        'PASSWORD': 'reactionengine',
        'HOST': 'localhost',
        'PORT': '3306',
        'OPTIONS': {
            'autocommit': False,
        },
    }
}

# configuring logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(pathname)s/%(name)s.%(funcName)s:%(lineno)s --- %(message)s'
        }
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'reaction_debug.log',
            'maxBytes': 1024*1024*10, # 10 MB
            'backupCount': 10,
            'formatter': 'standard'
        },
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        }
    },
    'root': {
        'handlers': ['file', 'console'],
        'level': 'INFO',
        'propagate': True
    }
}

# Mail settings
# It is used by the default Django user administration to send the 'forgotten-password' mail
EMAIL_HOST = ''
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_PORT = -1
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'Reaction-admin@NOREPLY'
SERVER_EMAIL = 'Reaction-admin@NOREPLY'

# Endpoint URL of the reaction engine REST
REACTION_ENGINE_REST_URL = 'https://localhost:8443/reaction-engine'
# only if HTTPS is used in the URL;  if it is set to False, requests will accept any TLS certificate presented by the server, and will ignore hostname mismatches and/or expired certificates, which will make your application vulnerable to man-in-the-middle (MitM) attacks
#REACTION_ENGINE_REST_SSL_VERIFY = False
# only if HTTPS is used in the URL; set the server certificate file; if it isn't a self-signed certificate (i.e. signed by CA) then it doesn't need to be set
REACTION_ENGINE_REST_SSL_SERVER_CERT = '/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.2/security/embedded_tomcat_ssl.cert'

# The list of the access groups
# The name of the group MUSTN'T contain comma (,) and don't use space in front of or at the end of the group name!!!
ACCESS_GROUPS = [
    'Middleware',
    'DBA',
    'UNIX',
    'Microsoft Technologies',
]

# set the timezone (e.g. Europe/London)
TIME_ZONE = 'Europe/London'

# The age of session cookies, in seconds.
SESSION_COOKIE_AGE = 2 * 24 * 60 * 60

# public/private key for authenticating the request that is sent to the Reaction Engine REST
REACTION_REST_AUTH_PUBLIC_KEY = 'reaction-management-web-app'
REACTION_REST_AUTH_PRIVATE_KEY = 'e5574bf1-13c5-476a-b1d3-500bc640564d'

# To execute an SSH command on a remote host (in the execution flow) the Linux user and password have to be provided. The password will be encrypted with the key existing in the file specified by this setting.
PASSWORD_ENCRYPTION_KEY_FILE = '/home/vikhor/work/reaction/src/reaction/management_app/management_app/password_encryption.key'

# authentication type: 'SAML' (using an external Identity Provider, like Azure AD => settings_saml.py must exist!) or 'DJANGO_BACKEND' (using the embedded database-based Django authentication)
REACTION_AUTH_TYPE = 'DJANGO_BACKEND'