import os, sys
import django
from django.core.handlers.wsgi import WSGIHandler

# Calculate the path based on the location of the WSGI script.
apache_configuration= os.path.dirname(__file__)
project = os.path.dirname(apache_configuration)
workspace = os.path.dirname(project)
sys.path.append(project)

class WSGIEnvironment(WSGIHandler):
    def __call__(self, environ, start_response):
        return super(WSGIEnvironment, self).__call__(environ, start_response)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'management_app.settings')

django.setup()
application = WSGIEnvironment()
