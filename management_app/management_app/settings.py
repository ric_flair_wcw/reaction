import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'km7xub+#9fonyx+w_jw*058mpmcy1gy!wsbup()m$+=*+t96*g'

# Application definition
INSTALLED_APPS = [
    'django.contrib.staticfiles',
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'rest_framework',
    'admin_event_source_type',
    'admin_event_source',
    'admin_execution_flow',
    'admin_errordetector',
    'common',
    'scheduler',
    'monitoring',
    'executor',
    'approval',
    'statistics',
    'worker_status',
    'dashboard',
    # 'debug_toolbar',
    'django_extensions',  # !!!!!!!!!! dont commit !!!!!!!!!!!!
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'
]

ROOT_URLCONF = 'management_app.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'management_app.wsgi.application'

SECURE_BROWSER_XSS_FILTER = True

X_FRAME_OPTIONS = 'DENY'

# Password validation (https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators)
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization (https://docs.djangoproject.com/en/1.11/topics/i18n/)
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True
USE_TZ = False

# for REST framework
REST_FRAMEWORK = {
    'EXCEPTION_HANDLER': 'common.rest_utils.custom_exception_handler'
}

# for JQuery POST I have to get the csrf token and I enable it to strore it in the session (see https://docs.djangoproject.com/en/1.11/ref/csrf/)
CSRF_USE_SESSIONS = True

# Redirect to home URL after login (Default redirects to /accounts/profile/)
LOGIN_REDIRECT_URL = '/'

# if using subdomain (e.g. /reaction-management) then the default Django login mechanism would use the http://localhost/accounts/login URL which is non-existing
# the http://localhost/reaction-management/accounts/login has to be used instead
# the same is true for the URL of the static resources (https://docs.djangoproject.com/en/1.11/howto/static-files/)
if os.environ.get('REACTION_SUBDOMAIN', ''):
    LOGIN_URL = '/'+os.environ.get('REACTION_SUBDOMAIN', '')+'/accounts/login/'
    STATIC_ROOT = os.path.join(BASE_DIR, '/'+os.environ.get('REACTION_SUBDOMAIN', '')+'/static/')
    STATIC_URL = '/'+os.environ.get('REACTION_SUBDOMAIN', '')+'/static/'
    LOGOUT_REDIRECT_URL = '/' + os.environ.get('REACTION_SUBDOMAIN', '') + '/loggedout'
else:
    LOGIN_URL = 'accounts/login/'
    STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
    STATIC_URL = '/static/'
    LOGOUT_REDIRECT_URL = '/loggedout'

# Whether to save the session data on every request
SESSION_SAVE_EVERY_REQUEST = True

# which means session cookies will be stored in users' browsers for as long as SESSION_COOKIE_AGE. Use this if you don't want people to have to log in every time they open a browser.
SESSION_EXPIRE_AT_BROWSER_CLOSE = False

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# mail sending configuration
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'


from .settings_reaction import *
if REACTION_AUTH_TYPE == 'SAML':
    INSTALLED_APPS.append('djangosaml2')
    MIDDLEWARE.append('djangosaml2.middleware.SamlSessionMiddleware')
    from .settings_saml import *
# try:
#     from .settings_test import *
# except ModuleNotFoundError:
#     pass