from django.views.generic import UpdateView, CreateView
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from .models import ErrorDetector
from admin_event_source.models import EventSource
from admin_execution_flow.models import ExecutionFlow
from .forms import ErrorDetectorForm
from monitoring.models import Event
from common.view_utils import FilteredListView
import logging
from django.db.models import Q
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from admin_event_source.views import *
from common.utils import *
from django.db.utils import IntegrityError
import urllib


# Create your views here.
logger = logging.getLogger('root')


class ErrorDetectorList(LoginRequiredMixin, PermissionRequiredMixin, FilteredListView):
    # Get an instance of a logger
    paginate_by = 10
    template_name = 'admin_errordetector/list.html'
    permission_required = 'auth.can_use_admin_errordetector'

    def get_queryset(self):
        filter = self.build_filter()
        order = ['name' if 'order' not in self.request.GET else self.request.GET.get('order') , 'id']
        logger.info('get_queryset - {}|  filter :{},    order: {}'.format(__name__, filter, order))
        return ErrorDetector.objects.filter(**filter).order_by(*order)


class ErrorDetectorUpdateCreateView(LoginRequiredMixin, PermissionRequiredMixin):
    model = ErrorDetector
    form_class = ErrorDetectorForm
    template_name = 'admin_errordetector/form.html'
    permission_required = 'auth.can_use_admin_errordetector'

    def get_success_url(self):
        return reverse_lazy('admin_errordetector_list')

    def get_context_data(self, **kwargs):
        # setting the parent combo box
        context = super(ErrorDetectorUpdateCreateView, self).get_context_data(**kwargs)
        # setting the execution flow id and name to the dropdown
        choices = []
        conditions_access_groups = build_filter_access_groups(self.request.user.profile.access_groups, True, '')
        for ef in ExecutionFlow.objects.filter(conditions_access_groups, ~Q(status='INVALID')).order_by('name'):
            choices.append((ef.id, ef.name))
            context['form'].fields['execution_flow'].choices = choices
        # setting the event source id and name to the dropdown
        choices = [(None, '---')]
        event_sources = self.filter_by_type(build_complemented_event_source_names(), [])
        for s in sorted(event_sources, key=lambda d: d['name']):
            choices.append((s['id'], s['name']))
        context['form'].fields['event_source'].choices = choices
        context['form_operation'] = 'update' if self.__class__.__name__ == 'ErrorDetectorUpdate' else 'create'
        distinct_event_source_type_ids = dict(map(lambda es: (es.id,
                                                              list(filter(None, set(self.get_est_id_from_children(es))))),
                                                  EventSource.objects.filter(status='VALID')))
        self.fill_empty_est_ids_from_parent(distinct_event_source_type_ids)
        context['event_source_to_event_source_type'] = json.dumps({k: self.get_est_list_by_es(v) for k, v in distinct_event_source_type_ids.items()})
        context['if_contains_reaction_worker'] = json.dumps(dict(map(lambda es: (es.id,
                                                                                 True if list(filter(None, self.is_worker_est_in_this_es_or_its_children(es))) else False),
                                                                     EventSource.objects.filter(status='VALID'))))
        return context

    def get_est_id_from_children(self, es):
        est = []
        for event_source in EventSource.objects.filter(Q(parent=es.id) & Q(status='VALID')):
            est.extend(self.get_est_id_from_children(event_source))
        est.append(self.get_est_id(es.event_source_type))
        return est

    def is_worker_est_in_this_es_or_its_children(self, es):
        est = []
        for event_source in EventSource.objects.filter(Q(parent=es.id) & Q(status='VALID')):
            est.extend(self.is_worker_est_in_this_es_or_its_children(event_source))
        if es.event_source_type and es.event_source_type.code == 'REACTION-WORKER':
            est.append(1)
        return est

    def get_est_id(self, est):
        return est.id if est else None

    def get_est_list_by_es(self, est_ids):
        est_list = list(EventSourceType.objects.filter(pk__in = est_ids))
        pti_ed_list = []
        for est in est_list:
            if est.path_to_identifiers:
                pti_list = json.loads(est.path_to_identifiers)
                for pti in pti_list:
                    if pti['output'] == 'ERROR_DETECTOR':
                        pti['event_source_type_name'] = est.name
                        pti_ed_list.append(pti)
        return pti_ed_list

    def fill_empty_est_ids_from_parent(self, est_ids):
        for k, v in est_ids.items():
            if not v:
                est_from_parent = self.get_est_from_parent(k)
                if est_from_parent:
                    est_ids[k] = [est_from_parent]

    def get_est_from_parent(self, es_id):
        es = EventSource.objects.get(id=es_id)
        if es.parent:
            if es.parent.event_source_type:
                return es.parent.event_source_type.pk
            else:
                return self.get_est_from_parent(es.parent.pk)
        else:
            return None

    # retrieves all the ITEM event sources and only those GROUP ones which (or one of its groups) have ITEM event source(s)
    def filter_by_type(self, event_sources, filtered_event_sources):
        for _es in event_sources[:]:
            for es in event_sources[:]:
                if es['type'] == 'ITEM':
                    filtered_event_sources.append(es)
                    event_sources.remove(es)
                else:
                    if len([filtered_es for filtered_es in filtered_event_sources if filtered_es['parent_id'] == es['id']]) > 0:
                        filtered_event_sources.append(es)
                        event_sources.remove(es)
        return filtered_event_sources


class ErrorDetectorUpdate(ErrorDetectorUpdateCreateView, UpdateView):
    pass


class ErrorDetectorCreate(ErrorDetectorUpdateCreateView, CreateView):
    pass

# -------------------------------------------------- for REST --------------------------------------------------

@login_required
@permission_required('auth.can_use_admin_errordetector', raise_exception=True)
def delete_errordetector(request, pk):
    logger.info('delete_errordetector is called - request={}; pk={}'.format(request,pk))
    try:
        ErrorDetector.objects.filter(id = pk).delete()
        return HttpResponseRedirect(reverse_lazy('admin_errordetector_list'))
    except IntegrityError:
        message = 'The error detector cannot be deleted as it is currently being used in somewhere else!'
        logger.exception(message)
        return render(request, 'business_error.html', {'message': message})


@login_required_for_rest
@permission_required('auth.can_use_admin_errordetector', raise_exception=True)
@api_view(['GET'])
@transaction.atomic
def get_event_source(request, pk):
    logger.info('Getting the event source. pk={}, request={}'.format(pk, request))

    event_source = get_object_or_404(EventSource, id=pk)
    serializer = EventSourceTypeSerializer(event_source, many=False)

    return Response(serializer.data, status=status.HTTP_200_OK)


@login_required_for_rest
@permission_required('auth.can_use_admin_errordetector', raise_exception=True)
@api_view(['GET'])
@transaction.atomic
def get_errordetector_endpoint(request, pk):
    logger.info('Getting the endpoint to be called from external monitoring app. pk={}, request={}'.format(pk, request))

    error_detector = get_object_or_404(ErrorDetector, id=pk)

    item_event_sources = []
    get_item_event_sources(error_detector.event_source, item_event_sources)

    fill_est_and_identifiers_in_item_event_sources(item_event_sources)

    result_list = []
    for item_event_source in item_event_sources:
        result = {'name': item_event_source.name,
                  'id': item_event_source.pk}
        if item_event_source.event_source_type.code == 'REACTION-WORKER':
            result['is_reaction_worker'] = True
        else:
            try:
                result.update(get_endpoint_to_be_called(error_detector, item_event_source))
            except KeyError:
                message = "The endpoint cannot be determined! Please check if all the identifier values are specified in the event source [{}] / error detector [{}].".format(item_event_source.name, error_detector.name)
                logger.exception(message)
                return Response(message, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        result_list.append(result)
    return Response(result_list, status=status.HTTP_200_OK)


@login_required
@permission_required('auth.can_use_admin_errordetector', raise_exception=True)
def remove_history(request, pk):
    error_detector = get_object_or_404(ErrorDetector, pk=pk)
    Event.objects.filter(error_detector=error_detector).delete()
    return HttpResponseRedirect(reverse_lazy('admin_errordetector_update', kwargs={'pk': pk}))


# -------------------------------------------------- utility functions ------------------------------------------------
# filling the event source type and identifiers from parent if necessary
def fill_est_and_identifiers_in_item_event_sources(item_event_sources):
    for item_event_source in item_event_sources:
        fill_from_parent(item_event_source, 'event_source_type')
        fill_from_parent(item_event_source, 'identifiers')


def fill_from_parent(item_event_source, attribute):
    if not getattr(item_event_source, attribute):
        parent_event_source = item_event_source
        while not getattr(parent_event_source, attribute) and parent_event_source.parent:
            parent_event_source = parent_event_source.parent
        if not getattr(parent_event_source, attribute) and not parent_event_source.parent:
            message = "The {} couldn't be found for the [{}] event source!".format(attribute, item_event_source.name)
            logger.error(message)
            return Response(message, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            setattr(item_event_source, attribute, getattr(parent_event_source, attribute))


def get_item_event_sources(event_source, item_event_sources):
    if event_source.type == 'ITEM':
        item_event_sources.append(event_source)
    else:
        event_source_children = EventSource.objects.filter(Q(parent=event_source.id) & Q(status='VALID'))
        for event_source_child in event_source_children:
            get_item_event_sources(event_source_child, item_event_sources)


def get_endpoint_to_be_called(error_detector, event_source):
    url = settings.REACTION_ENGINE_REST_URL + '/notification/event/' + event_source.event_source_type.code
    pti_list = json.loads(event_source.event_source_type.path_to_identifiers) if event_source.event_source_type.path_to_identifiers else []
    identifiers_es = json.loads(event_source.identifiers) if event_source.identifiers else {}
    identifiers_ed = json.loads(error_detector.identifiers) if error_detector.identifiers else {}
    query_parameters = {}
    query_parameters_pattern_list = []
    payload_pattern_list = []
    for pti in pti_list:
        pattern = {'path': pti['path'],
                   'desc': pti['desc'],
                   'output': pti['output']}
        if pti['output'] == 'EVENT_SOURCE':
            pattern['value'] = identifiers_es[pti['name'] + '-' + pti['source']]
        else:
            pattern['value'] = identifiers_ed[pti['name'] + '-' + pti['source']]
        if pti['source'] == 'QUERY_PARAMETER':
            query_parameters[pti['path']] = 'XXXXX'
            query_parameters_pattern_list.append(pattern)
        else:
            pattern['name'] = pti['name']
            payload_pattern_list.append(pattern)
    return {'url': url + ("?" + urllib.parse.urlencode(query_parameters) if query_parameters else ""),
            'query_parameters_pattern_list': query_parameters_pattern_list,
            'payload_pattern_list': payload_pattern_list}

