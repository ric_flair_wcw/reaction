from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^administration/errordetector$',                                         views.ErrorDetectorList.as_view(),                  name='admin_errordetector_list'),
    url(r'^administration/errordetector/(?P<pk>[0-9]+)/update$',                   views.ErrorDetectorUpdate.as_view(),                name='admin_errordetector_update'),
    url(r'^administration/errordetector/create$',                                  views.ErrorDetectorCreate.as_view(),                name='admin_errordetector_create'),
    url(r'^administration/errordetector/(?P<pk>[0-9]+)/delete$',                   views.delete_errordetector,                         name='admin_errordetector_delete'),
    url(r'^administration/errordetector/(?P<pk>[0-9]*)/remove_history',            views.remove_history,                               name='admin_errordetector_remove_history'),

    # for REST service
    url(r'^administration/errordetector/event_source/(?P<pk>[0-9]+)$',             views.get_event_source,                             name='admin_errordetector_event_source'),
    url(r'^administration/errordetector/endpoint/(?P<pk>[0-9]+)$',                 views.get_errordetector_endpoint,                   name='admin_errordetector_get_endpoint')

]
