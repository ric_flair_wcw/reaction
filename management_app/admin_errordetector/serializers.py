from rest_framework import serializers
from . import models
from admin_event_source_type.models import EventSourceType

class RecursiveField(serializers.Serializer):
    def to_representation(self, value):
        serializer = self.parent.parent.__class__(value, context=self.context)
        return serializer.data


class EventSourceSerializer(serializers.ModelSerializer):
    children = RecursiveField(many = True, read_only = True)
    log_header_pattern = serializers.CharField(trim_whitespace=False)

    class Meta:
        model = models.EventSource
        fields = ('id', 'name', 'type', 'host', 'log_path', 'log_header_pattern', 'description', 'log_level',
                  'maintenance_window', 'children', 'event_source_type')

