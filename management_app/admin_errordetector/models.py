from django.db import models
from admin_event_source.models import ParentModel, EventSource

# ################################## ErrorDetector ##################################
class ErrorDetector(ParentModel):
    name = models.CharField(max_length=200, blank=False, unique=True)
    message_pattern = models.CharField(max_length=400, blank=True, null=True)
    activated = models.BooleanField(default=False)
    confirmation_needed = models.BooleanField(default=False)
    multiple_events_cnt = models.IntegerField(blank=True, null=True)
    multiple_events_timefr = models.IntegerField(blank=True, null=True)
    execution_flow = models.ForeignKey("admin_execution_flow.ExecutionFlow", on_delete=models.DO_NOTHING)
    event_source = models.ForeignKey(EventSource, on_delete=models.DO_NOTHING)
    identifiers = models.CharField(max_length=3000, blank=True, null=True)

    class Meta:
        db_table = 'ERROR_DETECTOR'
        default_permissions = ()

    def __str__(self):
        return "Name: {}\nMessage pattern: {}\nConfirmation needed: {}\nActivated: {}".format(self.name,
                                                                                              self.message_pattern,
                                                                                              self.confirmation_needed,
                                                                                              self.activated)
