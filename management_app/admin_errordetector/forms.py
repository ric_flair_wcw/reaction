from django import forms

from .models import ErrorDetector
from admin_event_source.models import EventSource

class ErrorDetectorForm(forms.ModelForm):

	class Meta:
		model = ErrorDetector
		fields = ['name', 'activated', 'confirmation_needed', 'multiple_events_cnt', 'multiple_events_timefr', 'execution_flow',
				  'event_source', 'identifiers', 'message_pattern']

	def __init__(self, *args, **kwargs):
		super(ErrorDetectorForm, self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs['class'] = 'form-control'
		self.fields['activated'].widget.attrs['class'] = 'form-control'
		self.fields['activated'].widget.attrs['style'] = 'width: 50px;'
		self.fields['confirmation_needed'].widget.attrs['class'] = 'form-control'
		self.fields['confirmation_needed'].widget.attrs['style'] = 'width: 50px;'
		self.fields['multiple_events_cnt'].widget.attrs['class'] = 'form-control'
		self.fields['multiple_events_cnt'].widget.attrs['style'] = 'width: 150px;'
		self.fields['multiple_events_timefr'].widget.attrs['class'] = 'form-control'
		self.fields['multiple_events_timefr'].widget.attrs['style'] = 'width: 150px;'
		self.fields['execution_flow'].widget.attrs['class'] = 'form-control'
		self.fields['event_source'].widget.attrs['class'] = 'form-control'
		self.fields['event_source'].widget.attrs['onchange'] = 'rebuildIdentifiers()'
		self.fields['message_pattern'].widget.attrs['class'] = 'form-control'


	def clean(self):
		cleaned_data = super(ErrorDetectorForm, self).clean()
		cnt = cleaned_data.get('multiple_events_cnt')
		if cnt and int(cnt) < 2:
			self.add_error('multiple_events_cnt', 'If the multiple event counter is set then it has to be bigger than 1!')
		if not cnt:
			cleaned_data['multiple_events_timefr'] = None