import logging
from admin_execution_flow.models import ExecutionFlow
from django.shortcuts import render
from admin_execution_flow.mermaid import FlowBuilder
from executor.service_clients import *
from django.contrib.auth.decorators import login_required, permission_required
from common.utils import *
from django.views.decorators.csrf import csrf_protect
from django.urls import reverse

logger = logging.getLogger('root')


@login_required
@permission_required('auth.can_use_executor', raise_exception=True)
@csrf_protect
def index(request):
    logger.info('Starting a flow / {} is called'.format(request.method))
    # build the access_groups filter
    access_groups_conditions = build_filter_access_groups(request.user.profile.access_groups, False, '')
    # building the execution flow list
    execution_flows = ExecutionFlow.objects.filter(access_groups_conditions, Q(status='VALID') | Q(status='FROZEN')).order_by('name')
    context = { 'execution_flows': execution_flows }
    if request.method == 'POST':
        # the execution flow drop down was changed or clicking on the 'Do it!'
        operation = request.POST.get('operation')
        execution_flow_id = request.POST.get('execution_flow_id')
        logger.debug('operation={}, execution flow id={}'.format(operation, execution_flow_id))
        context['selected_execution_flow_id'] = execution_flow_id
        # building the text for mermaid flow
        mermaid_flow = ''
        if execution_flow_id != '':
            context['mermaid_flow'], context['tooltip_loop'] = FlowBuilder.build(execution_flow_id)
        # putting back the date in order to display it after the POST call
        date = None if request.POST.get('when') == 'NOW' else request.POST.get('date')
        context['date'] = request.POST.get('date')
        context['when'] = request.POST.get('when')
        if (operation == 'Do it!'):
            # the selected execution flow has to be started; display the index.html with a message
            rest_response = call_start_execution_flow(execution_flow_id, request.POST.get('reason'), date)
            context['rest_successful'] = rest_response.successful
            context['rest_message'] = rest_response.message
            context['url'] = reverse('monitoring_list') + '?filter_identifier='+rest_response.message
            context['rest_error_code'] = rest_response.error_code
    return render(request, 'executor/index.html', context)