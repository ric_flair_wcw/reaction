from common.rest_utils import call_rest


def call_start_execution_flow(pk, reason, date):
    url = '/executionflow/start/manual'
    payload = {'executionFlowId': pk,
               'date': date,
               'reason': reason}

    return call_rest(payload, url, 'POST')
