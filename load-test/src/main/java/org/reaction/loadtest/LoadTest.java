package org.reaction.loadtest;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoadTest {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(LoadTest.class);
	private static final Map<Integer, String> irrelevantMessages = new HashMap<>();
	
	static {
		irrelevantMessages.put(0, "org.acme.super.ExportExpection: There was a problem in Melius export!");
		irrelevantMessages.put(1, "java.lang.NullPointerException ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		irrelevantMessages.put(2, "Everything is alright!aaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		irrelevantMessages.put(3, "Everything is doomed...\n-------------------------------no hope...\nmisery....");
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		// the length of the load test (in sec)
		int testLength = 8 * 60;
//		// the number of the flows that will be initiated (it doesn't mean that they will be started...)
//		int startedAllFlows = 100;
//		// the number of the flows that will be started
//		//   startedAllFlows >= startedRealFlows  - it is because the timeperiod between executions can be set for the execution
//		//      flow so the same flow won't be started if it is in this period
//		int startedRealFlows = 60;
		//String logMessage = "WFLYSRV0027: Starting deployment of \"bea_wls_cluster_internal.war\"";
		String logMessage = 
				"Servlet.service() for servlet rest-jersey-servlet threw exception\r\n" + 
				"com.acme.service.util.SAPBusinessException: com.acme.policy.PolicyViolation: Policy failed:\r\n" + 
				"com.acme.policy.element.PolicyElementViolation<element=com.acme.policy.element.RegexpCheck@5a5b9c53<displayName=idNoSpecialChars, limitProperty=null, comparisonFactor=1, regexpString=[^-a-zA-Z0-9@./_:(){}\\[\\]=!~+]>, actualValue=1>\r\n" + 
				"    at com.acme.service.plainspring.UnitsServiceHelper.createUnit(UnitsServiceHelper.java:211)\r\n" + 
				"    at com.acme.service.plainspring.UnitsServiceHelper.createUnit(UnitsServiceHelper.java:98)\r\n" + 
				"Caused by: com.acme.policy.PolicyViolation: Policy failed:\r\n" + 
				"com.acme.policy.element.PolicyElementViolation<element=com.acme.policy.element.RegexpCheck@5a5b9c53<displayName=idNoSpecialChars, limitProperty=null, comparisonFactor=1, regexpString=[^-a-zA-Z0-9@./_:(){}\\[\\]=!~+]>, actualValue=1>\r\n" + 
				"    at com.acme.policy.PolicyPluginImpl.checkPolicy(PolicyPluginImpl.java:41)\r\n" + 
				"    at com.acme.policy.PolicyModuleImpl.checkPolicy(PolicyModuleImpl.java:47)\r\n" + 
				"";
		LOGGER.error(logMessage);
		System.exit(-1);
		int periodBetweenExecutions = 10;
		
		int nrOfStartedFlows = 1;
		
		long startingTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("START!   for " + testLength + " sec   - current date:"+new Date(startingTime));
		// the loop below will log the logmessage and "wait" (it means that it will write irrelevant log messages to the file) 
		//    for a random number millisec
		
		boolean forward = true;
		long modifier = 1;
		
		while (Calendar.getInstance().getTimeInMillis() < startingTime + testLength*1000) {
			LOGGER.error(logMessage);
			long waitingPeriod = ThreadLocalRandom.current().nextLong(100+20*modifier, 200+20*modifier);
			//
			long loopStartingTime = Calendar.getInstance().getTimeInMillis();
			while(Calendar.getInstance().getTimeInMillis() < loopStartingTime + waitingPeriod) {
				if (waitingPeriod % 2 == 0) {
					LOGGER.error(getIrrelevantMessage());
				} else {
					LOGGER.info(getIrrelevantMessage());
				}
				Thread.sleep(ThreadLocalRandom.current().nextLong(60+30*modifier, 150+20*modifier));
			}
			// count how many flow will be started 
			if (Calendar.getInstance().getTimeInMillis() - loopStartingTime > periodBetweenExecutions) {
				nrOfStartedFlows++;
			}
			
			if (forward) {
				modifier += 0.0001;
			} else {
				modifier -= 0.0001;
			}
			if (modifier > 2) {
				forward = false;
			}
			if (modifier <= 0) {
				forward = true;
			}
			
		}
			
		System.out.println("The number of the possible started flows: " + nrOfStartedFlows);
		System.out.println("END...   "+new Date(Calendar.getInstance().getTimeInMillis()));
		
		
		
		
		
		
		
//		for (int i = 0; i < startedAllFlows; i++) {
//			LOGGER.error("org.acme.super.ExportExpection: There was a problem in Ares export!");
//			Thread.sleep( testLength*1000 / startedAllFlows );
//		}
	}


	private static String getIrrelevantMessage() {
		return irrelevantMessages.get(ThreadLocalRandom.current().nextInt(0, 4));
	}
	
}
