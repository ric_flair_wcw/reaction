1. Opening screen
	reaction standard design
	
	                        Reaction
				 Automatic incident detector and resolver
				 
				           Case study

2. describe what I want to solve with reaction
   for example: 2-3 times a week the Hermes CRM application goes down. There is a memory leak that causes OutOfMemoryException. In this case the administrators are notified that nobody can work and the admin restart the app servers -> these all take time
   Hermes -> 2 servers -> if the exception occurs that both servers have to be restarted.
						   
3. First install the worker
    unzip it, show config file, start and show log file
	
3. add the ref data
   - create Hermes event source (add the maintenance window and some other data to it to show that these are inherited), Hermes0 and Hermes1
   - create the exec flow to restart the servers
   - create the error detector
   
4. try to execute the flow manually

5. make the OutOfMemoryException to the log file and confirm it, see if it will be scheduled and check how it is executed.