INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Monitoring',1,'can_use_monitoring');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use System adminstration',1,'can_use_admin_system');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Execution Flow administration',1,'can_use_admin_execution_flow');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Errordetector administration',1,'can_use_admin_errordetector');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Scheduler',1,'can_use_scheduler');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Executor',1,'can_use_executor');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Approval',1,'can_use_approval');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Statistics',1,'can_use_statistics');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Worker status',1,'can_use_worker_status');