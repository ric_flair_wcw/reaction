-- decide if only a create_tables.sql is needed or a reaction.sql has to be created too which can be used if Django's migration commands are used


-- add the other SQL commands
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Event Source Type administration',1,'can_use_admin_event_source_type');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Event Source administration',1,'can_use_admin_event_source');
-- TODO delete the 'can_use_admin_systm'

-- event source types
INSERT INTO event_source_type (id, name, code, data_type, namespaces, path_to_identifiers, path_to_error_message, authentication_type, enforce_ssl, secret, user)
    VALUES (0,'Reaction Worker','REACTION-WORKER','NONE',NULL,NULL,'','NONE',0,NULL,NULL);
INSERT INTO event_source_type (id, name, code, data_type, namespaces, path_to_identifiers, path_to_error_message, authentication_type, enforce_ssl, secret, user)
    VALUES (1,'Splunk 8.1','splunk_81','JSON','',
    '[{\"name\":\"host\",\"desc\":\"Host name\",\"path\":\"$.result.host\"},{\"name\":\"source\",\"desc\":\"Source\",\"path\":\"$.result.source\"}]',
    '$.result._raw','NONE',0,NULL,NULL);
INSERT INTO event_source_type (id, name, code, data_type, namespaces, path_to_identifiers, path_to_error_message, authentication_type, enforce_ssl, secret, user)
    VALUES (2,'Prometheus 2.23.0','prometheus_2_23','JSON','',
    '[{\"name\":\"exporter_instance\",\"desc\":\"The exporter target where the alert comes from\",\"path\":\"$.alerts[0].labels.instance\"}]',
    '$.alerts[0].annotations.summary','NONE',0,NULL,NULL);