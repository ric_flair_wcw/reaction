The following files have to be released and they can be found here:

Engine
 - reaction-engine-config-1.1.zip  ->  reaction-engine/build/dist/reaction-engine-config-1.1.zip
 - reaction-engine-tomcat-1.1.war  ->  reaction-engine/build/dist/reaction-engine-tomcat-1.1.war
 - reaction-engine-weblogic-1.1.war  ->  reaction-engine/build/dist/reaction-engine-weblogic-1.1.war
 - reaction-engine-wildfly-1.1.war   ->  reaction-engine/build/dist/reaction-engine-wildfly-1.1.war
 - reaction-engine-standalone-1.1.zip   ->  reaction-engine/build/dist/reaction-engine-standalone-1.1.war


Managmenet App
 - reaction_management_app_installation_v1_1.bsx   ->  _install/management_app/generate_self_contained_executable/build/reaction_management_app_installation_v1_1.bsx
 - reaction_management_app_v1_1.zip   ->  management_app/_build/reaction_management_app_v1_1.zip


Worker
 - reaction-worker-1.1.zip  ->  worker/build/dist/reaction-worker-1.1.zip
 - reaction-worker-1.1-1.el7.noarch.rpm   ->  _install/worker/RPMS/noarch/reaction-worker-1.1-1.el7.noarch.rpm


Docker images
 - oracle-instantclient12.2-basic-12.2.0.1.0-1.x86_64.rpm   ->   it is already in https://bitbucket.org/ric_flair_wcw/reaction-storage-v1_1/downloads/
 - docker_demo_init_tables.sql   ->   it is already in https://bitbucket.org/ric_flair_wcw/reaction-storage-v1_1/downloads/

 
Others:
 - _release\1.1\create_tables_mysql.sql
 - _release\1.1\create_tables_oracle.sql


Build the projects in Linux!