--------------------------------------------------------
--  DDL for Table AUTH_GROUP
--------------------------------------------------------

  CREATE TABLE "AUTH_GROUP" 
   (	"ID" NUMBER(11,0), 
	"NAME" NVARCHAR2(80)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_GROUP_PERMISSIONS
--------------------------------------------------------

  CREATE TABLE "AUTH_GROUP_PERMISSIONS" 
   (	"ID" NUMBER(11,0), 
	"GROUP_ID" NUMBER(11,0), 
	"PERMISSION_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_PERMISSION
--------------------------------------------------------

  CREATE TABLE "AUTH_PERMISSION" 
   (	"ID" NUMBER(11,0), 
	"NAME" NVARCHAR2(255), 
	"CONTENT_TYPE_ID" NUMBER(11,0), 
	"CODENAME" NVARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_USER
--------------------------------------------------------

  CREATE TABLE "AUTH_USER" 
   (	"ID" NUMBER(11,0), 
	"PASSWORD" NVARCHAR2(128), 
	"LAST_LOGIN" TIMESTAMP (6), 
	"IS_SUPERUSER" NUMBER(1,0), 
	"USERNAME" NVARCHAR2(150), 
	"FIRST_NAME" NVARCHAR2(30), 
	"LAST_NAME" NVARCHAR2(30), 
	"EMAIL" NVARCHAR2(254), 
	"IS_STAFF" NUMBER(1,0), 
	"IS_ACTIVE" NUMBER(1,0), 
	"DATE_JOINED" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_USER_GROUPS
--------------------------------------------------------

  CREATE TABLE "AUTH_USER_GROUPS" 
   (	"ID" NUMBER(11,0), 
	"USER_ID" NUMBER(11,0), 
	"GROUP_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table AUTH_USER_USER_PERMISSIONS
--------------------------------------------------------

  CREATE TABLE "AUTH_USER_USER_PERMISSIONS" 
   (	"ID" NUMBER(11,0), 
	"USER_ID" NUMBER(11,0), 
	"PERMISSION_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table COMMANDS_TO_BE_EXECUTED
--------------------------------------------------------

  CREATE TABLE "COMMANDS_TO_BE_EXECUTED" 
   (	"ID" NUMBER(11,0), 
	"IS_EXECUTED" NUMBER(1,0), 
	"EVENT_LIFE_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table DJANGO_ADMIN_LOG
--------------------------------------------------------

  CREATE TABLE "DJANGO_ADMIN_LOG" 
   (	"ID" NUMBER(11,0), 
	"ACTION_TIME" TIMESTAMP (6), 
	"OBJECT_ID" NCLOB, 
	"OBJECT_REPR" NVARCHAR2(200), 
	"ACTION_FLAG" NUMBER(11,0), 
	"CHANGE_MESSAGE" NCLOB, 
	"CONTENT_TYPE_ID" NUMBER(11,0), 
	"USER_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table DJANGO_CONTENT_TYPE
--------------------------------------------------------

  CREATE TABLE "DJANGO_CONTENT_TYPE" 
   (	"ID" NUMBER(11,0), 
	"APP_LABEL" NVARCHAR2(100), 
	"MODEL" NVARCHAR2(100)
   ) ;
--------------------------------------------------------
--  DDL for Table DJANGO_MIGRATIONS
--------------------------------------------------------

  CREATE TABLE "DJANGO_MIGRATIONS" 
   (	"ID" NUMBER(11,0), 
	"APP" NVARCHAR2(255), 
	"NAME" NVARCHAR2(255), 
	"APPLIED" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table DJANGO_SESSION
--------------------------------------------------------

  CREATE TABLE "DJANGO_SESSION" 
   (	"SESSION_KEY" NVARCHAR2(40), 
	"SESSION_DATA" NCLOB, 
	"EXPIRE_DATE" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table ERROR_DETECTOR
--------------------------------------------------------

  CREATE TABLE "ERROR_DETECTOR" 
   (	"ID" NUMBER(11,0), 
	"NAME" NVARCHAR2(200), 
	"MESSAGE_PATTERN" NVARCHAR2(200), 
	"ACTIVATED" NUMBER(1,0), 
	"CONFIRMATION_NEEDED" NUMBER(1,0), 
	"MULTIPLE_EVENTS_CNT" NUMBER(11,0), 
	"MULTIPLE_EVENTS_TIMEFR" NUMBER(11,0), 
	"EXECUTION_FLOW_ID" NUMBER(11,0), 
	"SYSTM_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table EVENT
--------------------------------------------------------

  CREATE TABLE "EVENT" 
   (	"ID" NUMBER(11,0), 
	"IDENTIFIER" NVARCHAR2(40), 
	"LOG_LEVEL" NVARCHAR2(40), 
	"INITIATED_BY" NVARCHAR2(40), 
	"STATUS" NVARCHAR2(40), 
	"MESSAGE" NCLOB, 
	"START_DATE" TIMESTAMP (6), 
	"END_DATE" TIMESTAMP (6), 
	"FIRST_EVENT_ARRIVED" TIMESTAMP (6), 
	"MULTIPLE_EVENTS_COUNTER" NUMBER(11,0), 
	"REASON" NCLOB, 
	"ERROR_DETECTOR_ID" NUMBER(11,0), 
	"EXECUTION_FLOW_ID" NUMBER(11,0), 
	"SCHEDULED_EXECUTION_FLOW_ID" NUMBER(11,0), 
	"SYSTM_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table EVENT_LIFE
--------------------------------------------------------

  CREATE TABLE "EVENT_LIFE" 
   (	"ID" NUMBER(11,0), 
	"EVENT_DATE" TIMESTAMP (6), 
	"ORDR" NUMBER(11,0), 
	"OUTPUT" NCLOB, 
	"EXTRACTED_VALUE" NVARCHAR2(500), 
	"EXT_COMMAND_SUCCESSFUL" NUMBER(1,0), 
	"STATUS" NVARCHAR2(40), 
	"HISTORY" NCLOB, 
	"ERROR_MESSAGE" NCLOB, 
	"BY_WHOM" NVARCHAR2(200), 
	"EVENT_ID" NUMBER(11,0), 
	"TASK_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table EXECUTION_FLOW
--------------------------------------------------------

  CREATE TABLE "EXECUTION_FLOW" 
   (	"ID" NUMBER(11,0), 
	"NAME" NVARCHAR2(200), 
	"TIME_BETWEEN_EXEC" NUMBER(11,0), 
	"EXECUTION_TIME" NUMBER(11,0), 
	"STATUS" NVARCHAR2(40), 
	"ACCESS_GROUP" NVARCHAR2(200), 
	"ERROR_MAIL_SENDING_RECIPIENTS" NVARCHAR2(2000), 
	"START_MAIL_SENDING_RECIPIENTS" NVARCHAR2(2000), 
	"HOSTS" NVARCHAR2(2000)
   ) ;
--------------------------------------------------------
--  DDL for Table PROFILE
--------------------------------------------------------

  CREATE TABLE "PROFILE" 
   (	"ID" NUMBER(11,0), 
	"ACCESS_GROUPS" NVARCHAR2(2000), 
	"DASHBOARD_AUTOREFRESH" NVARCHAR2(50), 
	"DASHBOARD_P1_DATEPERIOD" NVARCHAR2(50), 
	"DASHBOARD_P2_DATEPERIOD" NVARCHAR2(50), 
	"DASHBOARD_P3_DATEPERIOD" NVARCHAR2(50), 
	"DASHBOARD_P4_DATEPERIOD" NVARCHAR2(50), 
	"DASHBOARD_P2_FLOW0_ID" NUMBER(11,0), 
	"DASHBOARD_P2_FLOW1_ID" NUMBER(11,0), 
	"DASHBOARD_P2_FLOW2_ID" NUMBER(11,0), 
	"DASHBOARD_P2_FLOW3_ID" NUMBER(11,0), 
	"DASHBOARD_P2_SYSTEM0_ID" NUMBER(11,0), 
	"DASHBOARD_P2_SYSTEM1_ID" NUMBER(11,0), 
	"DASHBOARD_P2_SYSTEM2_ID" NUMBER(11,0), 
	"DASHBOARD_P2_SYSTEM3_ID" NUMBER(11,0), 
	"USER_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SCHEDULED_EXECUTION_FLOW
--------------------------------------------------------

  CREATE TABLE "SCHEDULED_EXECUTION_FLOW" 
   (	"ID" NUMBER(11,0), 
	"NAME" NVARCHAR2(200), 
	"REASON" NCLOB, 
	"CRON_EXPRESSION" NVARCHAR2(150), 
	"SCHEDULED_ALREADY" NUMBER(1,0), 
	"ERROR_AT_LAST_RUN" NCLOB, 
	"EXECUTION_FLOW_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SYSTM
--------------------------------------------------------

  CREATE TABLE "SYSTM"
   (	"ID" NUMBER(11,0),
	"NAME" NVARCHAR2(200), 
	"HOST" NVARCHAR2(200), 
	"LOG_PATH" NVARCHAR2(2000), 
	"LOG_HEADER_PATTERN" NVARCHAR2(2000), 
	"LOG_HEADER_PATTERN_ENABLED" NUMBER(1,0), 
	"DESCRIPTION" NVARCHAR2(2000), 
	"TYPE" NVARCHAR2(40), 
	"LOG_LEVEL" NVARCHAR2(40), 
	"MAINTENANCE_WINDOW" NVARCHAR2(2000), 
	"PARENT_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table TASK
--------------------------------------------------------

  CREATE TABLE "TASK" 
   (	"ID" NUMBER(11,0), 
	"NAME" NVARCHAR2(200), 
	"ORDR" NUMBER(11,0), 
	"COMMAND" NVARCHAR2(2000), 
	"OS_USER" NVARCHAR2(200), 
	"HOST" NVARCHAR2(100), 
	"INTERNAL_TASK" NVARCHAR2(40), 
	"IF_EXPRESSION" NVARCHAR2(2000), 
	"OUTPUT_PATTERN" NVARCHAR2(300), 
	"MAIL_RECIPIENTS" NVARCHAR2(2000), 
	"MAIL_SUBJECT" NVARCHAR2(300), 
	"MAIL_CONTENT" NVARCHAR2(2000), 
	"EXECUTION_FLOW_ID" NUMBER(11,0), 
	"PRIMARY_TASK_ID" NUMBER(11,0), 
	"SECONDARY_TASK_ID" NUMBER(11,0)
   ) ;
--------------------------------------------------------
--  DDL for Table WORKER_STATUS
--------------------------------------------------------

  CREATE TABLE "WORKER_STATUS" 
   (	"ID" NUMBER(11,0), 
	"HOST" NVARCHAR2(200), 
	"REFRESH_SYSTEMS" TIMESTAMP (6), 
	"REPORT_EVENT" TIMESTAMP (6), 
	"CHECK_COMMANDS" TIMESTAMP (6), 
	"SEND_COMMAND_RESULT" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Sequence AUTH_GROUP_PERMISSIONS_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTH_GROUP_PERMISSIONS_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence AUTH_GROUP_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTH_GROUP_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence AUTH_PERMISSION_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTH_PERMISSION_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence AUTH_USER_GROUPS_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTH_USER_GROUPS_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence AUTH_USER_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "AUTH_USER_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence AUTH_USER_USER_PERMISSI7B1E
--------------------------------------------------------

   CREATE SEQUENCE  "AUTH_USER_USER_PERMISSI7B1E"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence COMMANDS_TO_BE_EXECUTED_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "COMMANDS_TO_BE_EXECUTED_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence DJANGO_ADMIN_LOG_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "DJANGO_ADMIN_LOG_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence DJANGO_CONTENT_TYPE_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "DJANGO_CONTENT_TYPE_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence DJANGO_MIGRATIONS_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "DJANGO_MIGRATIONS_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence ERROR_DETECTOR_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "ERROR_DETECTOR_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence EVENT_LIFE_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "EVENT_LIFE_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence EVENT_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "EVENT_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence EXECUTION_FLOW_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "EXECUTION_FLOW_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence PROFILE_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "PROFILE_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence SCHEDULED_EXECUTION_FLOW_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "SCHEDULED_EXECUTION_FLOW_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence SYSTM_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "SYSTM_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence TASK_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "TASK_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
--------------------------------------------------------
--  DDL for Sequence WORKER_STATUS_SQ
--------------------------------------------------------

   CREATE SEQUENCE  "WORKER_STATUS_SQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOKEEP  NOSCALE  GLOBAL ;
   
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (1,'Can add permission',1,'add_permission');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (2,'Can change permission',1,'change_permission');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (3,'Can delete permission',1,'delete_permission');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (4,'Can add group',2,'add_group');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (5,'Can change group',2,'change_group');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (6,'Can delete group',2,'delete_group');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (7,'Can add user',3,'add_user');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (8,'Can change user',3,'change_user');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (9,'Can delete user',3,'delete_user');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (10,'Can add log entry',5,'add_logentry');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (11,'Can change log entry',5,'change_logentry');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (12,'Can delete log entry',5,'delete_logentry');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (13,'Can add content type',6,'add_contenttype');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (14,'Can change content type',6,'change_contenttype');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (15,'Can delete content type',6,'delete_contenttype');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (16,'Can add session',7,'add_session');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (17,'Can change session',7,'change_session');
Insert into AUTH_PERMISSION (ID,NAME,CONTENT_TYPE_ID,CODENAME) values (18,'Can delete session',7,'delete_session');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Monitoring',1,'can_use_monitoring');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use System adminstration',1,'can_use_admin_system');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Execution Flow administration',1,'can_use_admin_execution_flow');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Errordetector administration',1,'can_use_admin_errordetector');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Scheduler',1,'can_use_scheduler');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Executor',1,'can_use_executor');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Approval',1,'can_use_approval');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Statistics',1,'can_use_statistics');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Worker status',1,'can_use_worker_status');

Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (1,'auth','permission');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (2,'auth','group');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (3,'auth','user');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (4,'common','profile');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (5,'admin','logentry');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (6,'contenttypes','contenttype');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (7,'sessions','session');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (8,'admin_execution_flow','executionflow');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (9,'admin_execution_flow','task');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (10,'admin_errordetector','errordetector');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (11,'admin_system','systm');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (12,'scheduler','scheduledexecutionflow');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (13,'monitoring','commandstobeexecuted');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (14,'monitoring','event');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (15,'monitoring','eventlife');
Insert into DJANGO_CONTENT_TYPE (ID,APP_LABEL,MODEL) values (16,'worker_status','workerstatus');

Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (1,'contenttypes','0001_initial',to_timestamp('11-MAR-19 16.52.05.164866000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (2,'auth','0001_initial',to_timestamp('11-MAR-19 16.52.05.522161000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (3,'admin','0001_initial',to_timestamp('11-MAR-19 16.52.05.635853000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (4,'admin','0002_logentry_remove_auto_add',to_timestamp('11-MAR-19 16.52.05.662469000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (5,'admin_system','0001_initial',to_timestamp('11-MAR-19 16.52.05.737138000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (6,'admin_execution_flow','0001_initial',to_timestamp('11-MAR-19 16.52.05.879316000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (7,'admin_errordetector','0001_initial',to_timestamp('11-MAR-19 16.52.05.965717000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (8,'contenttypes','0002_remove_content_type_name',to_timestamp('11-MAR-19 16.52.06.250493000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (9,'auth','0002_alter_permission_name_max_length',to_timestamp('11-MAR-19 16.52.06.301611000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (10,'auth','0003_alter_user_email_max_length',to_timestamp('11-MAR-19 16.52.06.341758000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (11,'auth','0004_alter_user_username_opts',to_timestamp('11-MAR-19 16.52.06.373899000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (12,'auth','0005_alter_user_last_login_null',to_timestamp('11-MAR-19 16.52.06.414091000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (13,'auth','0006_require_contenttypes_0002',to_timestamp('11-MAR-19 16.52.06.437057000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (14,'auth','0007_alter_validators_add_error_messages',to_timestamp('11-MAR-19 16.52.06.463674000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (15,'auth','0008_alter_user_username_max_length',to_timestamp('11-MAR-19 16.52.06.500517000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (16,'common','0001_initial',to_timestamp('11-MAR-19 16.52.06.677719000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (17,'scheduler','0001_initial',to_timestamp('11-MAR-19 16.52.06.765047000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (18,'monitoring','0001_initial',to_timestamp('11-MAR-19 16.52.07.061838000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (19,'sessions','0001_initial',to_timestamp('11-MAR-19 16.52.07.102027000','DD-MON-RR HH24.MI.SSXFF'));
Insert into DJANGO_MIGRATIONS (ID,APP,NAME,APPLIED) values (20,'worker_status','0001_initial',to_timestamp('11-MAR-19 16.52.07.161759000','DD-MON-RR HH24.MI.SSXFF'));

-- admin / reactionengine
Insert into AUTH_USER (ID,PASSWORD,LAST_LOGIN,IS_SUPERUSER,USERNAME,FIRST_NAME,LAST_NAME,EMAIL,IS_STAFF,IS_ACTIVE,DATE_JOINED) values (1,'pbkdf2_sha256$36000$XqDJpwSJkst9$4S2vSe8uNFQy6xgOvcajbe4TecxmxeWdLqjVDDwPVAQ=',null,1,'admin',null,null,null,1,1,to_timestamp('11-MAR-19 17.17.13.648896000','DD-MON-RR HH24.MI.SSXFF'));
--------------------------------------------------------
--  DDL for Index AUTH_GROU_GROUP_ID__0CD325B0_U
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTH_GROU_GROUP_ID__0CD325B0_U" ON "AUTH_GROUP_PERMISSIONS" ("GROUP_ID", "PERMISSION_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_PERM_CONTENT_T_01AB375A_U
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTH_PERM_CONTENT_T_01AB375A_U" ON "AUTH_PERMISSION" ("CONTENT_TYPE_ID", "CODENAME") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_USER_USER_ID_G_94350C0C_U
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTH_USER_USER_ID_G_94350C0C_U" ON "AUTH_USER_GROUPS" ("USER_ID", "GROUP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_USER_USER_ID_P_14A6B632_U
--------------------------------------------------------

  CREATE UNIQUE INDEX "AUTH_USER_USER_ID_P_14A6B632_U" ON "AUTH_USER_USER_PERMISSIONS" ("USER_ID", "PERMISSION_ID") 
  ;
--------------------------------------------------------
--  DDL for Index DJANGO_CO_APP_LABEL_76BD3D3B_U
--------------------------------------------------------

  CREATE UNIQUE INDEX "DJANGO_CO_APP_LABEL_76BD3D3B_U" ON "DJANGO_CONTENT_TYPE" ("APP_LABEL", "MODEL") 
  ;
--------------------------------------------------------
--  DDL for Index TASK_NAME_EXEC_667D275C_U
--------------------------------------------------------

  CREATE UNIQUE INDEX "TASK_NAME_EXEC_667D275C_U" ON "TASK" ("NAME", "EXECUTION_FLOW_ID", "PRIMARY_TASK_ID", "SECONDARY_TASK_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_USER__PERMISSION_1FBB5F2C
--------------------------------------------------------

  CREATE INDEX "AUTH_USER__PERMISSION_1FBB5F2C" ON "AUTH_USER_USER_PERMISSIONS" ("PERMISSION_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_SCHEDULED__1B94D8EC
--------------------------------------------------------

  CREATE INDEX "EVENT_SCHEDULED__1B94D8EC" ON "EVENT" ("SCHEDULED_EXECUTION_FLOW_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__33886109
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__33886109" ON "PROFILE" ("DASHBOARD_P2_FLOW1_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__1DD43DF0
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__1DD43DF0" ON "PROFILE" ("DASHBOARD_P2_SYSTEM0_ID") 
  ;
--------------------------------------------------------
--  DDL for Index COMMANDS_T_EVENT_LIFE_FEC2C836
--------------------------------------------------------

  CREATE INDEX "COMMANDS_T_EVENT_LIFE_FEC2C836" ON "COMMANDS_TO_BE_EXECUTED" ("EVENT_LIFE_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__CB9F1D5C
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__CB9F1D5C" ON "PROFILE" ("DASHBOARD_P2_SYSTEM1_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_PERMI_CONTENT_TY_2F476E4B
--------------------------------------------------------

  CREATE INDEX "AUTH_PERMI_CONTENT_TY_2F476E4B" ON "AUTH_PERMISSION" ("CONTENT_TYPE_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_USER__USER_ID_6A12ED8B
--------------------------------------------------------

  CREATE INDEX "AUTH_USER__USER_ID_6A12ED8B" ON "AUTH_USER_GROUPS" ("USER_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_ERROR_DETE_4A184E1E
--------------------------------------------------------

  CREATE INDEX "EVENT_ERROR_DETE_4A184E1E" ON "EVENT" ("ERROR_DETECTOR_ID") 
  ;
--------------------------------------------------------
--  DDL for Index DJANGO_ADM_USER_ID_C564EBA6
--------------------------------------------------------

  CREATE INDEX "DJANGO_ADM_USER_ID_C564EBA6" ON "DJANGO_ADMIN_LOG" ("USER_ID") 
  ;
--------------------------------------------------------
--  DDL for Index TASK_SECONDARY__B005E148
--------------------------------------------------------

  CREATE INDEX "TASK_SECONDARY__B005E148" ON "TASK" ("SECONDARY_TASK_ID") 
  ;
--------------------------------------------------------
--  DDL for Index ERROR_DETE_EXECUTION__74B13218
--------------------------------------------------------

  CREATE INDEX "ERROR_DETE_EXECUTION__74B13218" ON "ERROR_DETECTOR" ("EXECUTION_FLOW_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_GROUP_PERMISSION_84C5C92E
--------------------------------------------------------

  CREATE INDEX "AUTH_GROUP_PERMISSION_84C5C92E" ON "AUTH_GROUP_PERMISSIONS" ("PERMISSION_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__109F6401
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__109F6401" ON "PROFILE" ("DASHBOARD_P2_FLOW2_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__087D2484
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__087D2484" ON "PROFILE" ("DASHBOARD_P2_FLOW3_ID") 
  ;
--------------------------------------------------------
--  DDL for Index TASK_EXECUTION__AC20876E
--------------------------------------------------------

  CREATE INDEX "TASK_EXECUTION__AC20876E" ON "TASK" ("EXECUTION_FLOW_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__6D633335
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__6D633335" ON "PROFILE" ("DASHBOARD_P2_FLOW0_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_USER__USER_ID_A95EAD1B
--------------------------------------------------------

  CREATE INDEX "AUTH_USER__USER_ID_A95EAD1B" ON "AUTH_USER_USER_PERMISSIONS" ("USER_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_EXECUTION__6B7013AD
--------------------------------------------------------

  CREATE INDEX "EVENT_EXECUTION__6B7013AD" ON "EVENT" ("EXECUTION_FLOW_ID") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__8FF013F7
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__8FF013F7" ON "PROFILE" ("DASHBOARD_P2_SYSTEM2_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_LIFE_TASK_ID_F70384EF
--------------------------------------------------------

  CREATE INDEX "EVENT_LIFE_TASK_ID_F70384EF" ON "EVENT_LIFE" ("TASK_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_LOG_LEVEL_A263F771
--------------------------------------------------------

  CREATE INDEX "EVENT_LOG_LEVEL_A263F771" ON "EVENT" ("LOG_LEVEL") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_START_DATE_EEC04655
--------------------------------------------------------

  CREATE INDEX "EVENT_START_DATE_EEC04655" ON "EVENT" ("START_DATE") 
  ;
--------------------------------------------------------
--  DDL for Index PROFILE_DASHBOARD__DCD3894A
--------------------------------------------------------

  CREATE INDEX "PROFILE_DASHBOARD__DCD3894A" ON "PROFILE" ("DASHBOARD_P2_SYSTEM3_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_STATUS_23C63D06
--------------------------------------------------------

  CREATE INDEX "EVENT_STATUS_23C63D06" ON "EVENT" ("STATUS") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_INITIATED_BY_F6AEAA14
--------------------------------------------------------

  CREATE INDEX "EVENT_INITIATED_BY_F6AEAA14" ON "EVENT" ("INITIATED_BY") 
  ;
--------------------------------------------------------
--  DDL for Index SCHEDULED__EXECUTION__F75B8BCF
--------------------------------------------------------

  CREATE INDEX "SCHEDULED__EXECUTION__F75B8BCF" ON "SCHEDULED_EXECUTION_FLOW" ("EXECUTION_FLOW_ID") 
  ;
--------------------------------------------------------
--  DDL for Index DJANGO_SES_EXPIRE_DAT_A5C62663
--------------------------------------------------------

  CREATE INDEX "DJANGO_SES_EXPIRE_DAT_A5C62663" ON "DJANGO_SESSION" ("EXPIRE_DATE") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_GROUP_GROUP_ID_B120CBF9
--------------------------------------------------------

  CREATE INDEX "AUTH_GROUP_GROUP_ID_B120CBF9" ON "AUTH_GROUP_PERMISSIONS" ("GROUP_ID") 
  ;
--------------------------------------------------------
--  DDL for Index DJANGO_ADM_CONTENT_TY_C4BCE8EB
--------------------------------------------------------

  CREATE INDEX "DJANGO_ADM_CONTENT_TY_C4BCE8EB" ON "DJANGO_ADMIN_LOG" ("CONTENT_TYPE_ID") 
  ;
--------------------------------------------------------
--  DDL for Index ERROR_DETE_SYSTM_ID_F44FC791
--------------------------------------------------------

  CREATE INDEX "ERROR_DETE_SYSTM_ID_F44FC791" ON "ERROR_DETECTOR" ("SYSTM_ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYSTM_PARENT_ID_C47B4F35
--------------------------------------------------------

  CREATE INDEX "SYSTM_PARENT_ID_C47B4F35" ON "SYSTM" ("PARENT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index TASK_PRIMARY_TASK_ID_00379DC6
--------------------------------------------------------

  CREATE INDEX "TASK_PRIMARY_TASK_ID_00379DC6" ON "TASK" ("PRIMARY_TASK_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_SYSTM_ID_67F45543
--------------------------------------------------------

  CREATE INDEX "EVENT_SYSTM_ID_67F45543" ON "EVENT" ("SYSTM_ID") 
  ;
--------------------------------------------------------
--  DDL for Index EVENT_LIFE_EVENT_ID_2E14A881
--------------------------------------------------------

  CREATE INDEX "EVENT_LIFE_EVENT_ID_2E14A881" ON "EVENT_LIFE" ("EVENT_ID") 
  ;
--------------------------------------------------------
--  DDL for Index AUTH_USER__GROUP_ID_97559544
--------------------------------------------------------

  CREATE INDEX "AUTH_USER__GROUP_ID_97559544" ON "AUTH_USER_GROUPS" ("GROUP_ID") 
  ;
--------------------------------------------------------
--  DDL for Trigger AUTH_GROUP_PERMISSIONS_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "AUTH_GROUP_PERMISSIONS_TR" 
BEFORE INSERT ON "AUTH_GROUP_PERMISSIONS"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "AUTH_GROUP_PERMISSIONS_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "AUTH_GROUP_PERMISSIONS_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_GROUP_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "AUTH_GROUP_TR" 
BEFORE INSERT ON "AUTH_GROUP"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "AUTH_GROUP_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "AUTH_GROUP_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_PERMISSION_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "AUTH_PERMISSION_TR" 
BEFORE INSERT ON "AUTH_PERMISSION"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "AUTH_PERMISSION_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "AUTH_PERMISSION_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_USER_GROUPS_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "AUTH_USER_GROUPS_TR" 
BEFORE INSERT ON "AUTH_USER_GROUPS"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "AUTH_USER_GROUPS_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "AUTH_USER_GROUPS_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_USER_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "AUTH_USER_TR" 
BEFORE INSERT ON "AUTH_USER"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "AUTH_USER_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "AUTH_USER_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger AUTH_USER_USER_PERMISSI17F3
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "AUTH_USER_USER_PERMISSI17F3" 
BEFORE INSERT ON "AUTH_USER_USER_PERMISSIONS"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "AUTH_USER_USER_PERMISSI7B1E".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "AUTH_USER_USER_PERMISSI17F3" ENABLE;
--------------------------------------------------------
--  DDL for Trigger COMMANDS_TO_BE_EXECUTED_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "COMMANDS_TO_BE_EXECUTED_TR" 
BEFORE INSERT ON "COMMANDS_TO_BE_EXECUTED"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "COMMANDS_TO_BE_EXECUTED_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "COMMANDS_TO_BE_EXECUTED_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger DJANGO_ADMIN_LOG_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DJANGO_ADMIN_LOG_TR" 
BEFORE INSERT ON "DJANGO_ADMIN_LOG"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "DJANGO_ADMIN_LOG_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "DJANGO_ADMIN_LOG_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger DJANGO_CONTENT_TYPE_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DJANGO_CONTENT_TYPE_TR" 
BEFORE INSERT ON "DJANGO_CONTENT_TYPE"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "DJANGO_CONTENT_TYPE_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "DJANGO_CONTENT_TYPE_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger DJANGO_MIGRATIONS_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "DJANGO_MIGRATIONS_TR" 
BEFORE INSERT ON "DJANGO_MIGRATIONS"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "DJANGO_MIGRATIONS_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "DJANGO_MIGRATIONS_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger ERROR_DETECTOR_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "ERROR_DETECTOR_TR" 
BEFORE INSERT ON "ERROR_DETECTOR"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "ERROR_DETECTOR_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "ERROR_DETECTOR_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger EVENT_LIFE_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "EVENT_LIFE_TR" 
BEFORE INSERT ON "EVENT_LIFE"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "EVENT_LIFE_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "EVENT_LIFE_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger EVENT_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "EVENT_TR" 
BEFORE INSERT ON "EVENT"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "EVENT_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "EVENT_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger EXECUTION_FLOW_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "EXECUTION_FLOW_TR" 
BEFORE INSERT ON "EXECUTION_FLOW"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "EXECUTION_FLOW_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "EXECUTION_FLOW_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger PROFILE_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "PROFILE_TR" 
BEFORE INSERT ON "PROFILE"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "PROFILE_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "PROFILE_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SCHEDULED_EXECUTION_FLOW_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SCHEDULED_EXECUTION_FLOW_TR" 
BEFORE INSERT ON "SCHEDULED_EXECUTION_FLOW"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "SCHEDULED_EXECUTION_FLOW_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "SCHEDULED_EXECUTION_FLOW_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger SYSTM_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "SYSTM_TR" 
BEFORE INSERT ON "SYSTM"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "SYSTM_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "SYSTM_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger TASK_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "TASK_TR" 
BEFORE INSERT ON "TASK"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "TASK_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "TASK_TR" ENABLE;
--------------------------------------------------------
--  DDL for Trigger WORKER_STATUS_TR
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE TRIGGER "WORKER_STATUS_TR" 
BEFORE INSERT ON "WORKER_STATUS"
FOR EACH ROW
 WHEN (new."ID" IS NULL) BEGIN
        SELECT "WORKER_STATUS_SQ".nextval
        INTO :new."ID" FROM dual;
    END;

/
ALTER TRIGGER "WORKER_STATUS_TR" ENABLE;
--------------------------------------------------------
--  Constraints for Table AUTH_GROUP
--------------------------------------------------------

  ALTER TABLE "AUTH_GROUP" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_GROUP" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "AUTH_GROUP" ADD UNIQUE ("NAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table TASK
--------------------------------------------------------

  ALTER TABLE "TASK" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "TASK" MODIFY ("ORDR" NOT NULL ENABLE);
  ALTER TABLE "TASK" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "TASK" ADD CONSTRAINT "TASK_NAME_EXEC_667D275C_U" UNIQUE ("NAME", "EXECUTION_FLOW_ID", "PRIMARY_TASK_ID", "SECONDARY_TASK_ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table AUTH_USER
--------------------------------------------------------

  ALTER TABLE "AUTH_USER" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER" MODIFY ("IS_SUPERUSER" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER" MODIFY ("IS_STAFF" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER" MODIFY ("IS_ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER" MODIFY ("DATE_JOINED" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER" ADD CHECK ("IS_SUPERUSER" IN (0,1)) ENABLE;
  ALTER TABLE "AUTH_USER" ADD CHECK ("IS_STAFF" IN (0,1)) ENABLE;
  ALTER TABLE "AUTH_USER" ADD CHECK ("IS_ACTIVE" IN (0,1)) ENABLE;
  ALTER TABLE "AUTH_USER" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "AUTH_USER" ADD UNIQUE ("USERNAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table DJANGO_ADMIN_LOG
--------------------------------------------------------

  ALTER TABLE "DJANGO_ADMIN_LOG" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_ADMIN_LOG" MODIFY ("ACTION_TIME" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_ADMIN_LOG" MODIFY ("ACTION_FLAG" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_ADMIN_LOG" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_ADMIN_LOG" ADD CHECK ("ACTION_FLAG" >= 0) ENABLE;
  ALTER TABLE "DJANGO_ADMIN_LOG" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table DJANGO_SESSION
--------------------------------------------------------

  ALTER TABLE "DJANGO_SESSION" MODIFY ("SESSION_KEY" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_SESSION" MODIFY ("EXPIRE_DATE" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_SESSION" ADD PRIMARY KEY ("SESSION_KEY")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table WORKER_STATUS
--------------------------------------------------------

  ALTER TABLE "WORKER_STATUS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "WORKER_STATUS" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "WORKER_STATUS" ADD UNIQUE ("HOST")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table AUTH_PERMISSION
--------------------------------------------------------

  ALTER TABLE "AUTH_PERMISSION" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_PERMISSION" MODIFY ("CONTENT_TYPE_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_PERMISSION" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "AUTH_PERMISSION" ADD CONSTRAINT "AUTH_PERM_CONTENT_T_01AB375A_U" UNIQUE ("CONTENT_TYPE_ID", "CODENAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table EVENT
--------------------------------------------------------

  ALTER TABLE "EVENT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "EVENT" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "EVENT" ADD UNIQUE ("IDENTIFIER")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table AUTH_GROUP_PERMISSIONS
--------------------------------------------------------

  ALTER TABLE "AUTH_GROUP_PERMISSIONS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_GROUP_PERMISSIONS" MODIFY ("GROUP_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_GROUP_PERMISSIONS" MODIFY ("PERMISSION_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_GROUP_PERMISSIONS" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "AUTH_GROUP_PERMISSIONS" ADD CONSTRAINT "AUTH_GROU_GROUP_ID__0CD325B0_U" UNIQUE ("GROUP_ID", "PERMISSION_ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table EVENT_LIFE
--------------------------------------------------------

  ALTER TABLE "EVENT_LIFE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "EVENT_LIFE" MODIFY ("ORDR" NOT NULL ENABLE);
  ALTER TABLE "EVENT_LIFE" MODIFY ("EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "EVENT_LIFE" ADD CHECK (("EXT_COMMAND_SUCCESSFUL" IN (0,1)) OR ("EXT_COMMAND_SUCCESSFUL" IS NULL)) ENABLE;
  ALTER TABLE "EVENT_LIFE" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table DJANGO_MIGRATIONS
--------------------------------------------------------

  ALTER TABLE "DJANGO_MIGRATIONS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_MIGRATIONS" MODIFY ("APPLIED" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_MIGRATIONS" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table ERROR_DETECTOR
--------------------------------------------------------

  ALTER TABLE "ERROR_DETECTOR" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "ERROR_DETECTOR" MODIFY ("ACTIVATED" NOT NULL ENABLE);
  ALTER TABLE "ERROR_DETECTOR" MODIFY ("CONFIRMATION_NEEDED" NOT NULL ENABLE);
  ALTER TABLE "ERROR_DETECTOR" MODIFY ("EXECUTION_FLOW_ID" NOT NULL ENABLE);
  ALTER TABLE "ERROR_DETECTOR" MODIFY ("SYSTM_ID" NOT NULL ENABLE);
  ALTER TABLE "ERROR_DETECTOR" ADD CHECK ("ACTIVATED" IN (0,1)) ENABLE;
  ALTER TABLE "ERROR_DETECTOR" ADD CHECK ("CONFIRMATION_NEEDED" IN (0,1)) ENABLE;
  ALTER TABLE "ERROR_DETECTOR" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "ERROR_DETECTOR" ADD UNIQUE ("NAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table AUTH_USER_GROUPS
--------------------------------------------------------

  ALTER TABLE "AUTH_USER_GROUPS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER_GROUPS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER_GROUPS" MODIFY ("GROUP_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER_GROUPS" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "AUTH_USER_GROUPS" ADD CONSTRAINT "AUTH_USER_USER_ID_G_94350C0C_U" UNIQUE ("USER_ID", "GROUP_ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table DJANGO_CONTENT_TYPE
--------------------------------------------------------

  ALTER TABLE "DJANGO_CONTENT_TYPE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "DJANGO_CONTENT_TYPE" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "DJANGO_CONTENT_TYPE" ADD CONSTRAINT "DJANGO_CO_APP_LABEL_76BD3D3B_U" UNIQUE ("APP_LABEL", "MODEL")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table EXECUTION_FLOW
--------------------------------------------------------

  ALTER TABLE "EXECUTION_FLOW" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "EXECUTION_FLOW" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "EXECUTION_FLOW" ADD UNIQUE ("NAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table COMMANDS_TO_BE_EXECUTED
--------------------------------------------------------

  ALTER TABLE "COMMANDS_TO_BE_EXECUTED" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "COMMANDS_TO_BE_EXECUTED" MODIFY ("IS_EXECUTED" NOT NULL ENABLE);
  ALTER TABLE "COMMANDS_TO_BE_EXECUTED" ADD CHECK ("IS_EXECUTED" IN (0,1)) ENABLE;
  ALTER TABLE "COMMANDS_TO_BE_EXECUTED" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "COMMANDS_TO_BE_EXECUTED" MODIFY ("EVENT_LIFE_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PROFILE
--------------------------------------------------------

  ALTER TABLE "PROFILE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "PROFILE" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "PROFILE" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "PROFILE" ADD UNIQUE ("USER_ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table SYSTM
--------------------------------------------------------

  ALTER TABLE "SYSTM" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SYSTM" MODIFY ("LOG_HEADER_PATTERN_ENABLED" NOT NULL ENABLE);
  ALTER TABLE "SYSTM" ADD CHECK ("LOG_HEADER_PATTERN_ENABLED" IN (0,1)) ENABLE;
  ALTER TABLE "SYSTM" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "SYSTM" ADD UNIQUE ("NAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table AUTH_USER_USER_PERMISSIONS
--------------------------------------------------------

  ALTER TABLE "AUTH_USER_USER_PERMISSIONS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER_USER_PERMISSIONS" MODIFY ("USER_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER_USER_PERMISSIONS" MODIFY ("PERMISSION_ID" NOT NULL ENABLE);
  ALTER TABLE "AUTH_USER_USER_PERMISSIONS" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "AUTH_USER_USER_PERMISSIONS" ADD CONSTRAINT "AUTH_USER_USER_ID_P_14A6B632_U" UNIQUE ("USER_ID", "PERMISSION_ID")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Constraints for Table SCHEDULED_EXECUTION_FLOW
--------------------------------------------------------

  ALTER TABLE "SCHEDULED_EXECUTION_FLOW" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SCHEDULED_EXECUTION_FLOW" MODIFY ("SCHEDULED_ALREADY" NOT NULL ENABLE);
  ALTER TABLE "SCHEDULED_EXECUTION_FLOW" MODIFY ("EXECUTION_FLOW_ID" NOT NULL ENABLE);
  ALTER TABLE "SCHEDULED_EXECUTION_FLOW" ADD CHECK ("SCHEDULED_ALREADY" IN (0,1)) ENABLE;
  ALTER TABLE "SCHEDULED_EXECUTION_FLOW" ADD PRIMARY KEY ("ID")
  USING INDEX  ENABLE;
  ALTER TABLE "SCHEDULED_EXECUTION_FLOW" ADD UNIQUE ("NAME")
  USING INDEX  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table AUTH_GROUP_PERMISSIONS
--------------------------------------------------------

  ALTER TABLE "AUTH_GROUP_PERMISSIONS" ADD CONSTRAINT "AUTH_GROU_GROUP_ID_B120CBF9_F" FOREIGN KEY ("GROUP_ID")
	  REFERENCES "AUTH_GROUP" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "AUTH_GROUP_PERMISSIONS" ADD CONSTRAINT "AUTH_GROU_PERMISSIO_84C5C92E_F" FOREIGN KEY ("PERMISSION_ID")
	  REFERENCES "AUTH_PERMISSION" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table AUTH_PERMISSION
--------------------------------------------------------

  ALTER TABLE "AUTH_PERMISSION" ADD CONSTRAINT "AUTH_PERM_CONTENT_T_2F476E4B_F" FOREIGN KEY ("CONTENT_TYPE_ID")
	  REFERENCES "DJANGO_CONTENT_TYPE" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table AUTH_USER_GROUPS
--------------------------------------------------------

  ALTER TABLE "AUTH_USER_GROUPS" ADD CONSTRAINT "AUTH_USER_USER_ID_6A12ED8B_F" FOREIGN KEY ("USER_ID")
	  REFERENCES "AUTH_USER" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "AUTH_USER_GROUPS" ADD CONSTRAINT "AUTH_USER_GROUP_ID_97559544_F" FOREIGN KEY ("GROUP_ID")
	  REFERENCES "AUTH_GROUP" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table AUTH_USER_USER_PERMISSIONS
--------------------------------------------------------

  ALTER TABLE "AUTH_USER_USER_PERMISSIONS" ADD CONSTRAINT "AUTH_USER_USER_ID_A95EAD1B_F" FOREIGN KEY ("USER_ID")
	  REFERENCES "AUTH_USER" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "AUTH_USER_USER_PERMISSIONS" ADD CONSTRAINT "AUTH_USER_PERMISSIO_1FBB5F2C_F" FOREIGN KEY ("PERMISSION_ID")
	  REFERENCES "AUTH_PERMISSION" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table COMMANDS_TO_BE_EXECUTED
--------------------------------------------------------

  ALTER TABLE "COMMANDS_TO_BE_EXECUTED" ADD CONSTRAINT "COMMANDS__EVENT_LIF_FEC2C836_F" FOREIGN KEY ("EVENT_LIFE_ID")
	  REFERENCES "EVENT_LIFE" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table DJANGO_ADMIN_LOG
--------------------------------------------------------

  ALTER TABLE "DJANGO_ADMIN_LOG" ADD CONSTRAINT "DJANGO_AD_CONTENT_T_C4BCE8EB_F" FOREIGN KEY ("CONTENT_TYPE_ID")
	  REFERENCES "DJANGO_CONTENT_TYPE" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "DJANGO_ADMIN_LOG" ADD CONSTRAINT "DJANGO_AD_USER_ID_C564EBA6_F" FOREIGN KEY ("USER_ID")
	  REFERENCES "AUTH_USER" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ERROR_DETECTOR
--------------------------------------------------------

  ALTER TABLE "ERROR_DETECTOR" ADD CONSTRAINT "ERROR_DET_EXECUTION_74B13218_F" FOREIGN KEY ("EXECUTION_FLOW_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "ERROR_DETECTOR" ADD CONSTRAINT "ERROR_DET_SYSTM_ID_F44FC791_F" FOREIGN KEY ("SYSTM_ID")
	  REFERENCES "SYSTM" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EVENT
--------------------------------------------------------

  ALTER TABLE "EVENT" ADD CONSTRAINT "EVENT_ERROR_DET_4A184E1E_F" FOREIGN KEY ("ERROR_DETECTOR_ID")
	  REFERENCES "ERROR_DETECTOR" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "EVENT" ADD CONSTRAINT "EVENT_EXECUTION_6B7013AD_F" FOREIGN KEY ("EXECUTION_FLOW_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "EVENT" ADD CONSTRAINT "EVENT_SCHEDULED_1B94D8EC_F" FOREIGN KEY ("SCHEDULED_EXECUTION_FLOW_ID")
	  REFERENCES "SCHEDULED_EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "EVENT" ADD CONSTRAINT "EVENT_SYSTM_ID_67F45543_F" FOREIGN KEY ("SYSTM_ID")
	  REFERENCES "SYSTM" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EVENT_LIFE
--------------------------------------------------------

  ALTER TABLE "EVENT_LIFE" ADD CONSTRAINT "EVENT_LIF_EVENT_ID_2E14A881_F" FOREIGN KEY ("EVENT_ID")
	  REFERENCES "EVENT" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "EVENT_LIFE" ADD CONSTRAINT "EVENT_LIF_TASK_ID_F70384EF_F" FOREIGN KEY ("TASK_ID")
	  REFERENCES "TASK" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PROFILE
--------------------------------------------------------

  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_6D633335_F" FOREIGN KEY ("DASHBOARD_P2_FLOW0_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_33886109_F" FOREIGN KEY ("DASHBOARD_P2_FLOW1_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_109F6401_F" FOREIGN KEY ("DASHBOARD_P2_FLOW2_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_087D2484_F" FOREIGN KEY ("DASHBOARD_P2_FLOW3_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_1DD43DF0_F" FOREIGN KEY ("DASHBOARD_P2_SYSTEM0_ID")
	  REFERENCES "SYSTM" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_CB9F1D5C_F" FOREIGN KEY ("DASHBOARD_P2_SYSTEM1_ID")
	  REFERENCES "SYSTM" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_8FF013F7_F" FOREIGN KEY ("DASHBOARD_P2_SYSTEM2_ID")
	  REFERENCES "SYSTM" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_DASHBOARD_DCD3894A_F" FOREIGN KEY ("DASHBOARD_P2_SYSTEM3_ID")
	  REFERENCES "SYSTM" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "PROFILE" ADD CONSTRAINT "PROFILE_USER_ID_ED0036AC_F" FOREIGN KEY ("USER_ID")
	  REFERENCES "AUTH_USER" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SCHEDULED_EXECUTION_FLOW
--------------------------------------------------------

  ALTER TABLE "SCHEDULED_EXECUTION_FLOW" ADD CONSTRAINT "SCHEDULED_EXECUTION_F75B8BCF_F" FOREIGN KEY ("EXECUTION_FLOW_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SYSTM
--------------------------------------------------------

  ALTER TABLE "SYSTM" ADD CONSTRAINT "SYSTM_PARENT_ID_C47B4F35_F" FOREIGN KEY ("PARENT_ID")
	  REFERENCES "SYSTM" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table TASK
--------------------------------------------------------

  ALTER TABLE "TASK" ADD CONSTRAINT "TASK_EXECUTION_AC20876E_F" FOREIGN KEY ("EXECUTION_FLOW_ID")
	  REFERENCES "EXECUTION_FLOW" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "TASK" ADD CONSTRAINT "TASK_PRIMARY_T_00379DC6_F" FOREIGN KEY ("PRIMARY_TASK_ID")
	  REFERENCES "TASK" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
  ALTER TABLE "TASK" ADD CONSTRAINT "TASK_SECONDARY_B005E148_F" FOREIGN KEY ("SECONDARY_TASK_ID")
	  REFERENCES "TASK" ("ID") DEFERRABLE INITIALLY DEFERRED ENABLE;
