CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `worker_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(200) NOT NULL,
  `refresh_systems` datetime(6) DEFAULT NULL,
  `report_event` datetime(6) DEFAULT NULL,
  `check_commands` datetime(6) DEFAULT NULL,
  `send_command_result` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `host` (`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `systm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `host` varchar(200) DEFAULT NULL,
  `log_path` varchar(2000) DEFAULT NULL,
  `log_header_pattern` varchar(2000) DEFAULT NULL,
  `log_header_pattern_enabled` tinyint(1) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `type` varchar(40) NOT NULL,
  `log_level` varchar(40) DEFAULT NULL,
  `maintenance_window` varchar(2000) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `SYSTM_parent_id_c47b4f35_fk_SYSTM_id` (`parent_id`),
  CONSTRAINT `SYSTM_parent_id_c47b4f35_fk_SYSTM_id` FOREIGN KEY (`parent_id`) REFERENCES `systm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `execution_flow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `time_between_exec` int(11) DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `access_group` varchar(200) DEFAULT NULL,
  `error_mail_sending_recipients` varchar(2000) DEFAULT NULL,
  `start_mail_sending_recipients` varchar(2000) DEFAULT NULL,
  `hosts` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `ordr` int(11) NOT NULL,
  `command` varchar(2000) DEFAULT NULL,
  `os_user` varchar(200) DEFAULT NULL,
  `host` varchar(100) DEFAULT NULL,
  `internal_task` varchar(40) NOT NULL,
  `if_expression` varchar(2000) DEFAULT NULL,
  `output_pattern` varchar(300) DEFAULT NULL,
  `mail_recipients` varchar(2000) DEFAULT NULL,
  `mail_subject` varchar(300) DEFAULT NULL,
  `mail_content` varchar(2000) DEFAULT NULL,
  `execution_flow_id` int(11) DEFAULT NULL,
  `primary_task_id` int(11) DEFAULT NULL,
  `secondary_task_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `TASK_name_execution_flow_id_p_667d275c_uniq` (`name`,`execution_flow_id`,`primary_task_id`,`secondary_task_id`),
  KEY `TASK_execution_flow_id_ac20876e_fk_EXECUTION_FLOW_id` (`execution_flow_id`),
  KEY `TASK_primary_task_id_00379dc6_fk_TASK_id` (`primary_task_id`),
  KEY `TASK_secondary_task_id_b005e148_fk_TASK_id` (`secondary_task_id`),
  CONSTRAINT `TASK_execution_flow_id_ac20876e_fk_EXECUTION_FLOW_id` FOREIGN KEY (`execution_flow_id`) REFERENCES `execution_flow` (`id`),
  CONSTRAINT `TASK_primary_task_id_00379dc6_fk_TASK_id` FOREIGN KEY (`primary_task_id`) REFERENCES `task` (`id`),
  CONSTRAINT `TASK_secondary_task_id_b005e148_fk_TASK_id` FOREIGN KEY (`secondary_task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `scheduled_execution_flow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `reason` longtext DEFAULT NULL,
  `cron_expression` varchar(150) NOT NULL,
  `scheduled_already` tinyint(1) NOT NULL,
  `error_at_last_run` longtext DEFAULT NULL,
  `execution_flow_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `SCHEDULED_EXECUTION__execution_flow_id_f75b8bcf_fk_EXECUTION` (`execution_flow_id`),
  CONSTRAINT `SCHEDULED_EXECUTION__execution_flow_id_f75b8bcf_fk_EXECUTION` FOREIGN KEY (`execution_flow_id`) REFERENCES `execution_flow` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `error_detector` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `message_pattern` varchar(200) NOT NULL,
  `activated` tinyint(1) NOT NULL,
  `confirmation_needed` tinyint(1) NOT NULL,
  `multiple_events_cnt` int(11) DEFAULT NULL,
  `multiple_events_timefr` int(11) DEFAULT NULL,
  `execution_flow_id` int(11) NOT NULL,
  `systm_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `ERROR_DETECTOR_execution_flow_id_74b13218_fk_EXECUTION_FLOW_id` (`execution_flow_id`),
  KEY `ERROR_DETECTOR_systm_id_f44fc791_fk_SYSTM_id` (`systm_id`),
  CONSTRAINT `ERROR_DETECTOR_execution_flow_id_74b13218_fk_EXECUTION_FLOW_id` FOREIGN KEY (`execution_flow_id`) REFERENCES `execution_flow` (`id`),
  CONSTRAINT `ERROR_DETECTOR_systm_id_f44fc791_fk_SYSTM_id` FOREIGN KEY (`systm_id`) REFERENCES `systm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(40) NOT NULL,
  `log_level` varchar(40) DEFAULT NULL,
  `initiated_by` varchar(40) NOT NULL,
  `status` varchar(40) NOT NULL,
  `message` longtext DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `first_event_arrived` datetime(6) DEFAULT NULL,
  `multiple_events_counter` int(11) DEFAULT NULL,
  `reason` longtext DEFAULT NULL,
  `error_detector_id` int(11) DEFAULT NULL,
  `execution_flow_id` int(11) DEFAULT NULL,
  `scheduled_execution_flow_id` int(11) DEFAULT NULL,
  `systm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identifier` (`identifier`),
  KEY `EVENT_log_level_a263f771` (`log_level`),
  KEY `EVENT_initiated_by_f6aeaa14` (`initiated_by`),
  KEY `EVENT_status_23c63d06` (`status`),
  KEY `EVENT_start_date_eec04655` (`start_date`),
  KEY `EVENT_error_detector_id_4a184e1e` (`error_detector_id`),
  KEY `EVENT_execution_flow_id_6b7013ad` (`execution_flow_id`),
  KEY `EVENT_scheduled_execution_flow_id_1b94d8ec` (`scheduled_execution_flow_id`),
  KEY `EVENT_systm_id_67f45543` (`systm_id`),
  CONSTRAINT `EVENT_error_detector_id_4a184e1e_fk_ERROR_DETECTOR_id` FOREIGN KEY (`error_detector_id`) REFERENCES `error_detector` (`id`),
  CONSTRAINT `EVENT_execution_flow_id_6b7013ad_fk_EXECUTION_FLOW_id` FOREIGN KEY (`execution_flow_id`) REFERENCES `execution_flow` (`id`),
  CONSTRAINT `EVENT_scheduled_execution__1b94d8ec_fk_SCHEDULED` FOREIGN KEY (`scheduled_execution_flow_id`) REFERENCES `scheduled_execution_flow` (`id`),
  CONSTRAINT `EVENT_systm_id_67f45543_fk_SYSTM_id` FOREIGN KEY (`systm_id`) REFERENCES `systm` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `event_life` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_date` datetime(6) DEFAULT NULL,
  `ordr` int(11) NOT NULL,
  `output` longtext DEFAULT NULL,
  `extracted_value` varchar(500) DEFAULT NULL,
  `ext_command_successful` tinyint(1) DEFAULT NULL,
  `status` varchar(40) NOT NULL,
  `history` longtext DEFAULT NULL,
  `error_message` longtext DEFAULT NULL,
  `by_whom` varchar(200) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `task_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `EVENT_LIFE_event_id_2e14a881_fk_EVENT_id` (`event_id`),
  KEY `EVENT_LIFE_task_id_f70384ef_fk_TASK_id` (`task_id`),
  CONSTRAINT `EVENT_LIFE_event_id_2e14a881_fk_EVENT_id` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  CONSTRAINT `EVENT_LIFE_task_id_f70384ef_fk_TASK_id` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `commands_to_be_executed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_executed` tinyint(1) NOT NULL,
  `event_life_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `COMMANDS_TO_BE_EXECUTED_event_life_id_fec2c836_fk_EVENT_LIFE_id` (`event_life_id`),
  CONSTRAINT `COMMANDS_TO_BE_EXECUTED_event_life_id_fec2c836_fk_EVENT_LIFE_id` FOREIGN KEY (`event_life_id`) REFERENCES `event_life` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_groups` varchar(2000) DEFAULT NULL,
  `dashboard_autorefresh` varchar(50) DEFAULT NULL,
  `dashboard_p1_dateperiod` varchar(50) DEFAULT NULL,
  `dashboard_p2_dateperiod` varchar(50) DEFAULT NULL,
  `dashboard_p3_dateperiod` varchar(50) DEFAULT NULL,
  `dashboard_p4_dateperiod` varchar(50) DEFAULT NULL,
  `dashboard_p2_flow0_id` int(11) DEFAULT NULL,
  `dashboard_p2_flow1_id` int(11) DEFAULT NULL,
  `dashboard_p2_flow2_id` int(11) DEFAULT NULL,
  `dashboard_p2_flow3_id` int(11) DEFAULT NULL,
  `dashboard_p2_system0_id` int(11) DEFAULT NULL,
  `dashboard_p2_system1_id` int(11) DEFAULT NULL,
  `dashboard_p2_system2_id` int(11) DEFAULT NULL,
  `dashboard_p2_system3_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `PROFILE_dashboard_p2_flow0_id_6d633335_fk_EXECUTION_FLOW_id` (`dashboard_p2_flow0_id`),
  KEY `PROFILE_dashboard_p2_flow1_id_33886109_fk_EXECUTION_FLOW_id` (`dashboard_p2_flow1_id`),
  KEY `PROFILE_dashboard_p2_flow2_id_109f6401_fk_EXECUTION_FLOW_id` (`dashboard_p2_flow2_id`),
  KEY `PROFILE_dashboard_p2_flow3_id_087d2484_fk_EXECUTION_FLOW_id` (`dashboard_p2_flow3_id`),
  KEY `PROFILE_dashboard_p2_system0_id_1dd43df0_fk_SYSTM_id` (`dashboard_p2_system0_id`),
  KEY `PROFILE_dashboard_p2_system1_id_cb9f1d5c_fk_SYSTM_id` (`dashboard_p2_system1_id`),
  KEY `PROFILE_dashboard_p2_system2_id_8ff013f7_fk_SYSTM_id` (`dashboard_p2_system2_id`),
  KEY `PROFILE_dashboard_p2_system3_id_dcd3894a_fk_SYSTM_id` (`dashboard_p2_system3_id`),
  CONSTRAINT `PROFILE_dashboard_p2_flow0_id_6d633335_fk_EXECUTION_FLOW_id` FOREIGN KEY (`dashboard_p2_flow0_id`) REFERENCES `execution_flow` (`id`),
  CONSTRAINT `PROFILE_dashboard_p2_flow1_id_33886109_fk_EXECUTION_FLOW_id` FOREIGN KEY (`dashboard_p2_flow1_id`) REFERENCES `execution_flow` (`id`),
  CONSTRAINT `PROFILE_dashboard_p2_flow2_id_109f6401_fk_EXECUTION_FLOW_id` FOREIGN KEY (`dashboard_p2_flow2_id`) REFERENCES `execution_flow` (`id`),
  CONSTRAINT `PROFILE_dashboard_p2_flow3_id_087d2484_fk_EXECUTION_FLOW_id` FOREIGN KEY (`dashboard_p2_flow3_id`) REFERENCES `execution_flow` (`id`),
  CONSTRAINT `PROFILE_dashboard_p2_system0_id_1dd43df0_fk_SYSTM_id` FOREIGN KEY (`dashboard_p2_system0_id`) REFERENCES `systm` (`id`),
  CONSTRAINT `PROFILE_dashboard_p2_system1_id_cb9f1d5c_fk_SYSTM_id` FOREIGN KEY (`dashboard_p2_system1_id`) REFERENCES `systm` (`id`),
  CONSTRAINT `PROFILE_dashboard_p2_system2_id_8ff013f7_fk_SYSTM_id` FOREIGN KEY (`dashboard_p2_system2_id`) REFERENCES `systm` (`id`),
  CONSTRAINT `PROFILE_dashboard_p2_system3_id_dcd3894a_fk_SYSTM_id` FOREIGN KEY (`dashboard_p2_system3_id`) REFERENCES `systm` (`id`),
  CONSTRAINT `PROFILE_user_id_ed0036ac_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `django_content_type` VALUES (5,'admin','logentry'),(10,'admin_errordetector','errordetector'),(8,'admin_execution_flow','executionflow'),(9,'admin_execution_flow','task'),(11,'admin_system','systm'),(2,'auth','group'),(1,'auth','permission'),(3,'auth','user'),(4,'common','profile'),(6,'contenttypes','contenttype'),(13,'monitoring','commandstobeexecuted'),(14,'monitoring','event'),(15,'monitoring','eventlife'),(12,'scheduler','scheduledexecutionflow'),(7,'sessions','session'),(16,'worker_status','workerstatus');

INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Monitoring',1,'can_use_monitoring');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use System adminstration',1,'can_use_admin_system');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Execution Flow administration',1,'can_use_admin_execution_flow');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Errordetector administration',1,'can_use_admin_errordetector');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Scheduler',1,'can_use_scheduler');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Executor',1,'can_use_executor');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Approval',1,'can_use_approval');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Statistics',1,'can_use_statistics');
INSERT INTO auth_permission (name, content_type_id, codename) VALUES ('Can use Worker status',1,'can_use_worker_status');

-- admin / reactionengine
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$36000$SYlMPam5KCfJ$lxedpn5tQu1ftSO9Vwckvy9RzhwvVGv1GpUHguoS7Ao=',NULL,1,'admin','','','',1,1,'2019-03-11 15:58:28.708618');

INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-03-11 11:52:38.217148'),(2,'auth','0001_initial','2019-03-11 11:52:39.313353'),(3,'admin','0001_initial','2019-03-11 11:52:39.582196'),(4,'admin','0002_logentry_remove_auto_add','2019-03-11 11:52:39.600265'),(5,'admin_system','0001_initial','2019-03-11 11:52:39.754849'),(6,'admin_execution_flow','0001_initial','2019-03-11 11:52:40.224299'),(7,'admin_errordetector','0001_initial','2019-03-11 11:52:40.496818'),(8,'contenttypes','0002_remove_content_type_name','2019-03-11 11:52:40.674575'),(9,'auth','0002_alter_permission_name_max_length','2019-03-11 11:52:40.786598'),(10,'auth','0003_alter_user_email_max_length','2019-03-11 11:52:40.818683'),(11,'auth','0004_alter_user_username_opts','2019-03-11 11:52:40.838742'),(12,'auth','0005_alter_user_last_login_null','2019-03-11 11:52:40.922953'),(13,'auth','0006_require_contenttypes_0002','2019-03-11 11:52:40.934485'),(14,'auth','0007_alter_validators_add_error_messages','2019-03-11 11:52:40.953033'),(15,'auth','0008_alter_user_username_max_length','2019-03-11 11:52:41.067331'),(16,'common','0001_initial','2019-03-11 11:52:42.060534'),(17,'scheduler','0001_initial','2019-03-11 11:52:42.228413'),(18,'monitoring','0001_initial','2019-03-11 11:52:43.416870'),(19,'sessions','0001_initial','2019-03-11 11:52:43.483932'),(20,'worker_status','0001_initial','2019-03-11 11:52:43.535961');

COMMIT;