Set the following system variables in the JVM of the app server
-Dspring.profiles.active=commonjWorkmanager
-Dspring.config.location=/local/vikhor/reaction/reaction-engine/reaction-engine-application.yml
-Dreaction.logback.config=/local/vikhor/reaction/reaction-engine/logback-include.xml