@ECHO OFF
    
REM ########################################################################################################
REM ##################################### VALUES TO BE CHANGED #############################################
REM ########################################################################################################

REM The path to the folder containing the java runtime
SET J_HOME=%JAVA_HOME%

REM ########################################################################################################
REM ############################ VALUES THAT DON'T NEED TO BE CHANGED ######################################
REM ########################################################################################################

SET VERSION=1.2
SET NAME=reaction-engine-daemon
SET DESC=Reaction Engine Daemon service v%VERSION%

ECHO.
ECHO -----------------------------------
ECHO ^| Reaction Engine v%VERSION%            ^|
ECHO -----------------------------------
ECHO.

REM The folder where the worker resides
SET mypath=%~dp0
SET BASE_FOLDER=%mypath:~0,-1%

SET spring.profiles.active=threadPool
SET spring.config.location=%BASE_FOLDER%\conf\reaction-engine-application.yml
SET reaction.logback.config=%BASE_FOLDER%\conf\logback-include.xml

REM The path to nsmm
REM https://nssm.cc/usage
SET EXEC=%BASE_FOLDER%\win64\nssm.exe

IF "%1" == "install" (
    ECHO Installing the %DESC% ...
    %EXEC% install %NAME% "%J_HOME%\bin\java.exe" "-Dspring.profiles.active=threadPool" "-Dspring.config.location=%BASE_FOLDER%\conf\reaction-engine-application.yml" "-Dreaction.logback.config=%BASE_FOLDER%\conf\logback-include.xml" "-jar" "%BASE_FOLDER%\reaction-engine-standalone-%VERSION%.war"
    %EXEC% set %NAME% AppDirectory "%BASE_FOLDER%"
    %EXEC% set %NAME% AppExit Default Exit
    %EXEC% set %NAME% AppExit 2 Restart
    %EXEC% set %NAME% DisplayName "%NAME%"
    %EXEC% set %NAME% Description "%DESC%"
) ELSE IF "%1" == "deinstall" (
    ECHO Deinstalling the %DESC% ...
    %EXEC% remove %NAME% confirm
) ELSE IF "%1" == "start" (
    ECHO Starting the %DESC% ...
    %EXEC% start %NAME%
) ELSE IF "%1" == "restart" (
    ECHO Restarting the %DESC% ...
    %EXEC% restart %NAME%
) ELSE IF "%1" == "stop" (
    ECHO Stopping the %DESC% ...
    %EXEC% stop %NAME%
) ELSE IF "%1" == "status" (
    ECHO Getting the status of the %DESC% ...
    %EXEC% status %NAME%
) ELSE (
    ECHO Usage: reaction_engine.bat {install^|deinstall^|start^|stop^|restart^|status}
)
