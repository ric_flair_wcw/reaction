#! /bin/sh
#  /etc/init.d/reaction-engine-daemon

# ########################################################################################################
# ##################################### VALUES TO BE CHANGED #############################################
# ########################################################################################################

# The path to the folder containing the java runtime
J_HOME=$JAVA_HOME

# The user to run the daemon
USER=root

# JAVA options (e.g. memory) for the embedded Tomcat
JAVA_OPTS="-Xms128m -Xmx512m"

# ########################################################################################################
# ############################ VALUES THAT DON'T NEED TO BE CHANGED ######################################
# ########################################################################################################

VERSION=1.2
NAME=reaction-engine-daemon
DESC="Reaction Engine Daemon service v$VERSION"

echo ""
echo "-----------------------------------"
echo "| Reaction Engine v$VERSION            |"
echo "-----------------------------------"
echo ""

# The folder where the worker resides
SCRIPT=$(readlink -f "$0")
export BASE_FOLDER=$(dirname "$SCRIPT")

# Setting the evironment variables for Reaction Engine
REACTION_OPTS="-Dspring.profiles.active=threadPool -Dspring.config.location=$BASE_FOLDER/conf/reaction-engine-application.yml -Dreaction.logback.config=$BASE_FOLDER/conf/logback-include.xml"

# The file that will contain the PID of the parent process.
PID_FILE=/var/run/$NAME.pid
PARENTPID=""
if [ -f "$PID_FILE" ]; then
    PARENTPID="$(sudo cat $PID_FILE)"
    PID=$(ps --ppid $PARsudo -u $USER nohup $J_HOME/bin/java $JAVA_OPTS $REACTION_OPTS -jar $BASE_FOLDER/reaction-engine-standalone-$VERSION.war 2>> /dev/null >> /dev/null &ENTPID -o pid= | awk '{print $1}')
    if [ -z "$PID" ]; then
        sudo -u $USER rm -f $PID_FILE
        PARENTPID=""
    fi
fi

# check if JAVA_HOME is set correctly
if [ -z "$J_HOME" ]; then
    echo "Warning! The JDK home is not set properly either in the script (see J_HOME variable) or in the JAVA_HOME system variable in the context of the Linux user that runs the script (i.e. if the script is executed with sudo then the JAVA_HOME has to be set for the root user)!"
    echo "Exiting..."
    exit -1
fi
if [ ! -f $J_HOME/bin/java ]; then
    echo "Warning! The $J_HOME/bin/java cannot be found! The JDK home is not set properly either in the script (see J_HOME variable) or in the JAVA_HOME system variable in the context of the Linux user that runs the script (i.e. if the script is executed with sudo then the JAVA_HOME has to be set for the root user)!"
    echo "Exiting...!"
    exit -1
fi
if [ ! -f $BASE_FOLDER/reaction-engine-standalone-$VERSION.war ]; then
    echo "Warning! The $BASE_FOLDER/reaction-engine-standalone-$VERSION.war cannot be found!"
    echo "Exiting...!"
    exit -1
fi

# check the first argument (start/stop/status)
case "$1" in
    start)
        if [ -f "$PID_FILE" ]; then
            echo "The $DESC is running already, no action taken."
        else
            echo "Starting the $DESC with the user '$USER'..."
            # Start the service
            sudo -u $USER nohup $J_HOME/bin/java $JAVA_OPTS $REACTION_OPTS -jar $BASE_FOLDER/reaction-engine-standalone-$VERSION.war 2>> /dev/null >> /dev/null &
            PID=$!
            sudo -u $USER PID_FILE=$PID_FILE PID=$PID sh -c 'echo $PID > $PID_FILE'
            echo "The $DESC is starting (PID=$PID)..."
        fi
    ;;
    stop)
        if [ -f "$PID_FILE" ]; then
            echo "Stopping the $DESC..."
            # Stop the service
            sudo -u $USER kill -15 $(ps --ppid $PARENTPID -o pid= | awk '{print $1}')
            sudo -u $USER rm -f $PID_FILE
            echo "The $DESC is stopping..."
        else
            echo "The $DESC is not running, no action taken."
            exit 1
        fi
    ;;
    status)
        if [ -f "$PID_FILE" ]; then
            STATUS=RUNNING
        else
            STATUS=stopped
        fi
        echo "Status                    : $STATUS"
        echo "PID                       : $PID"
        echo "PID file                  : $PID_FILE"
        echo "Location of log files     : $BASE_FOLDER/logs"
        echo "Application config file   : $BASE_FOLDER/conf/reaction-engine-application.yml"
        echo "Logging config file       : $BASE_FOLDER/conf/logback-include.xml"
    ;;
    *)
    echo "Usage: /etc/init.d/$NAME {start|stop|status}" >&2
    exit 3
    ;;
esac
