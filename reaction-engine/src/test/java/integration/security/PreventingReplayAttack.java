package integration.security;

import integration.TestConfig;
import integration.reactionWorker.ParentIntegrationTest;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.reaction.engine.WebInitializer;
import org.reaction.engine.controller.WorkerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@ContextConfiguration(classes = {
		TestConfig.class
})
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@SqlGroup({
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/before-test-admin.sql", "classpath:sql/before-test-data.sql"}),
    @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/after-test.sql")
})
public class PreventingReplayAttack extends ParentIntegrationTest {


    private static final String REST_ENDPOINT = "/worker/event";
	@Autowired
	private WorkerController workerController;
	
	
    @Test
    public void firingTheSameRequestTwice() throws Exception {
    	// ---------------------------------------------------------------------------
    	// --------------------- 1. report the event for the 1st time ----------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", "1jfghg874hf65bhfg74hf6");
    	request.put("systmId", "3");

    	HttpHeaders httpHeaders = buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + REST_ENDPOINT);
    	
    	mockMvc.perform( post(REST_ENDPOINT)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(httpHeaders))
    	       .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------- 2. replaying the same HTTP request ------------------
    	// ---------------------------------------------------------------------------
    	mockMvc.perform( post(REST_ENDPOINT)
          				 .content(this.fromJson(request))
          				 .contentType(contentType)
          				 .headers(httpHeaders))
	           .andExpect(status().isForbidden());
    }


    @Test
    public void firingTwoDifferentRequest() throws Exception {
    	// ---------------------------------------------------------------------------
    	// --------------------- 1. report the event ---------------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", "1jfghg874hf65bhfg74hf6");
    	request.put("systmId", "3");

    	mockMvc.perform( post(REST_ENDPOINT)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + REST_ENDPOINT)))
    	       .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------- 2. firing another HTTP request ----------------------
    	// ---------------------------------------------------------------------------
    	mockMvc.perform( post(REST_ENDPOINT)
          				 .content(this.fromJson(request))
          				 .contentType(contentType)
          				 .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + REST_ENDPOINT)))
	           .andExpect(status().isOk());
    }

    
    @Test
    public void firingAnOldRequest() throws Exception {
    	// ---------------------------------------------------------------------------
    	// --------------------- 1. firing an old request ----------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", "1jfghg874hf65bhfg74hf6");
    	request.put("systmId", "3");

    	HttpHeaders httpHeaders = buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + REST_ENDPOINT);
    	httpHeaders.setDate(4);
    	
    	mockMvc.perform( post(REST_ENDPOINT)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(httpHeaders))
    	       .andExpect(status().isForbidden());
    }

    
    @After
	public void clearMocks() {
		Mockito.reset(mockedJavaMailSender);
		workerController.identifierCache.clear();
	}

}