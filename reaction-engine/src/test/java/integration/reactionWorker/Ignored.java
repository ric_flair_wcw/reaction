package integration.reactionWorker;

import integration.TestConfig;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.reaction.engine.WebInitializer;
import org.reaction.engine.controller.WorkerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
		TestConfig.class
})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@SqlGroup({
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/before-test-admin.sql", "classpath:sql/before-test-data.sql"}),
    @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/after-test.sql")
})
public class Ignored extends ParentIntegrationTest {


	@Autowired
	private WorkerController workerController;
	
	
    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
    //    - 2 events with the same identifier
    @Test
    public void eventWithTheSameIdentifier() throws Exception {
    	String identifier = "1jfghg874hf65bhfg74hf6";
    	workerController.identifierCache.add(identifier);
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event ---------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier);
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd();
    }


    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
    //    - no error detector found
    @Test
    public void noErrorDetectorFound() throws Exception {
    	String identifier = "1jfghg874hf65bhfg74hf7";
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event ---------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) everything is allright";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier);
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd();
    }

    
    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
    //    - the same flow has already started
    @Test
    public void theSameFlowHasAlreadyStarted() throws Exception {
    	String identifier = "1jfghg874hf65bhfg74hf7";
    	Calendar now = Calendar.getInstance();
    	
		// create the expectations for the mocks
    	//   for mockedJavaMailSender
    	Session session = null;
    	when(mockedJavaMailSender.createMimeMessage()).then(inv -> new MimeMessage(session));

    	// maintenance window is not needed in this test
    	this.jdbcTemplate.update("UPDATE event_source SET maintenance_window = '{}' WHERE id = 1", new HashMap<>());
		// confirmation is not needed in this test and time_between_exec = 60sec
    	this.jdbcTemplate.update("UPDATE execution_flow SET time_between_exec = 600 WHERE id = 1", new HashMap<>());
    	this.jdbcTemplate.update("UPDATE error_detector SET confirmation_needed = 0 WHERE id = 2", new HashMap<>());
    	
    	// insert the Event that will prevent the new event to be started
    	Map<String,Object> params = new HashMap<>();
    	params.put("end_date", now);
    	this.jdbcTemplate.update("INSERT INTO event (id, identifier, status, start_date, ERROR_DETECTOR_ID, execution_flow_id) VALUES (999, 'aaa', 'STARTED', :end_date, 1,1 )", params);
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event ---------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier);
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	
		verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd2(identifier, message);
    }

    
    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
    //    - the same flow has just finished 5 mins ago and the period between executions is 20mins
    @Test
    public void theSameFlowHasJustFinishedAndShouldnotStarted() throws Exception {
    	String identifier = "1jfghg874hf65bhfg74hf7";
    	Calendar now = Calendar.getInstance();
    	
		// create the expectations for the mocks
    	//   for mockedJavaMailSender
    	Session session = null;
    	when(mockedJavaMailSender.createMimeMessage()).then(inv -> new MimeMessage(session));

    	// maintenance window is not needed in this test
    	this.jdbcTemplate.update("UPDATE event_source SET maintenance_window = '{}' WHERE id = 1", new HashMap<>());
		// confirmation is not needed in this test and time_between_exec = 60sec
    	this.jdbcTemplate.update("UPDATE execution_flow SET time_between_exec = 1200 WHERE id = 1", new HashMap<>());
    	this.jdbcTemplate.update("UPDATE error_detector SET confirmation_needed = 0 WHERE id = 2", new HashMap<>());
    	
    	// insert the Event that will prevent the new event to be started
    	Map<String,Object> params = new HashMap<>();
    	Calendar fiveMinutesAgo = Calendar.getInstance();
		fiveMinutesAgo.setTimeInMillis(now.getTimeInMillis() - (1000 * 60 * 5));
    	params.put("end_date", fiveMinutesAgo);
    	this.jdbcTemplate.update("INSERT INTO event (id, identifier, status, start_date, ERROR_DETECTOR_ID, execution_flow_id) VALUES (999, 'aaa', 'STARTED', :end_date, 1,1 )", params);
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event ---------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier);
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	
		verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd2(identifier, message);
    }

    
    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
    //    - the same flow is scheduled today
    @Test
    public void sameFlowIsScheduledToday() throws Exception {
    	String identifier = "1jfghg874hf65bhfg74hf7";
    	Calendar now = Calendar.getInstance();
    	
		// create the expectations for the mocks
    	//   for mockedJavaMailSender
    	Session session = null;
    	when(mockedJavaMailSender.createMimeMessage()).then(inv -> new MimeMessage(session));

    	// maintenance window is not needed in this test
    	this.jdbcTemplate.update("UPDATE event_source SET maintenance_window = '{\"Mon\": [\"02:50-02:51\"]}' WHERE id = 1", new HashMap<>());
		// confirmation is not needed in this test and time_between_exec = 60sec
    	this.jdbcTemplate.update("UPDATE execution_flow SET time_between_exec = 1200 WHERE id = 1", new HashMap<>());
    	this.jdbcTemplate.update("UPDATE error_detector SET confirmation_needed = 0 WHERE id = 2", new HashMap<>());
    	
    	// insert the Event that will prevent the new event to be started
    	Map<String,Object> params = new HashMap<>();
    	params.put("end_date", now);
    	this.jdbcTemplate.update("INSERT INTO event (id, identifier, status, start_date, ERROR_DETECTOR_ID, execution_flow_id) VALUES (999, 'aaa', 'SCHEDULED', :end_date, 1,1 )", params);
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event ---------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier);
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	
		verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd3(identifier, message);
    }

    
    private void checkDbAtEnd() throws IOException {
		// check the EVENT table
		List<Map<String,Object>> result = jdbcTemplate.queryForList("SELECT * FROM event", new MapSqlParameterSource());
    	assertEquals(0, result.size());

    	// check the EVENT_LIFE table
    	result = jdbcTemplate.queryForList("SELECT * FROM event_life ORDER BY id", new MapSqlParameterSource());
    	assertEquals(0, result.size());
	}

    
    private void checkDbAtEnd2(String identifier, String message) throws IOException {
		// check the EVENT table
		List<Map<String,Object>> result = jdbcTemplate.queryForList("SELECT * FROM event", new MapSqlParameterSource());
    	assertEquals(2, result.size());
    	
    	assertEquals(999l, result.get(0).get("ID"));
    	
    	Map<String,Object> rec = result.get(1);
    	assertNotNull(rec.get("END_DATE"));
    	assertNull(rec.get("FIRST_EVENT_ARRIVED"));
    	assertEquals(identifier, rec.get("IDENTIFIER"));
    	assertEquals("BY_LOG", rec.get("INITIATED_BY"));
    	assertEquals("ERROR", rec.get("LOG_LEVEL"));
    	assertEquals(message, rec.get("MESSAGE"));
    	assertNull(rec.get("MULTIPLE_EVENTS_COUNTER"));
    	assertEquals("The event is ignored as a same execution flow is (was recently) under operation already! The identifier of this other event is aaa (its status is STARTED).", rec.get("REASON"));
    	assertNotNull(rec.get("START_DATE"));
    	assertEquals("IGNORED", rec.get("STATUS"));
    	assertEquals(2l, rec.get("ERROR_DETECTOR_ID"));
    	assertEquals(1l, rec.get("EXECUTION_FLOW_ID"));
    	assertNull(rec.get("SCHEDULED_EXECUTION_FLOW_ID"));
    	assertEquals(3l, rec.get("EVENT_SOURCE_ID"));

    	// check the EVENT_LIFE table
    	result = jdbcTemplate.queryForList("SELECT * FROM event_life ORDER BY id", new MapSqlParameterSource());
    	assertEquals(0, result.size());
	}


    private void checkDbAtEnd3(String identifier, String message) throws IOException {
		// check the EVENT table
		List<Map<String,Object>> result = jdbcTemplate.queryForList("SELECT * FROM event", new MapSqlParameterSource());
    	assertEquals(2, result.size());
    	
    	assertEquals(999l, result.get(0).get("ID"));
    	
    	Map<String,Object> rec = result.get(1);
    	assertNotNull(rec.get("END_DATE"));
    	assertNull(rec.get("FIRST_EVENT_ARRIVED"));
    	assertEquals(identifier, rec.get("IDENTIFIER"));
    	assertEquals("BY_LOG", rec.get("INITIATED_BY"));
    	assertEquals("ERROR", rec.get("LOG_LEVEL"));
    	assertEquals(message, rec.get("MESSAGE"));
    	assertNull(rec.get("MULTIPLE_EVENTS_COUNTER"));
    	assertEquals("The event is ignored as a same execution flow is already scheduled for today! The identifier of this other event is aaa).",
    			rec.get("REASON"));
    	assertNotNull(rec.get("START_DATE"));
    	assertEquals("IGNORED", rec.get("STATUS"));
    	assertEquals(2l, rec.get("ERROR_DETECTOR_ID"));
    	assertEquals(1l, rec.get("EXECUTION_FLOW_ID"));
    	assertNull(rec.get("SCHEDULED_EXECUTION_FLOW_ID"));
    	assertEquals(3l, rec.get("EVENT_SOURCE_ID"));

    	// check the EVENT_LIFE table
    	result = jdbcTemplate.queryForList("SELECT * FROM event_life ORDER BY id", new MapSqlParameterSource());
    	assertEquals(0, result.size());
	}

    
    @After
	public void clearMocks() {
		Mockito.reset(mockedJavaMailSender);
		workerController.identifierCache.clear();
	}

}