package integration.reactionWorker;

import integration.TestConfig;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.reaction.common.contants.Constants;
import org.reaction.engine.WebInitializer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
		TestConfig.class
})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@SqlGroup({
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/before-test-admin.sql", "classpath:sql/before-test-data.sql"}),
    @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/after-test.sql")
})
public class MultipleEventNeeded_successfulStart extends ParentIntegrationTest {

    
    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
	//    - multiple event is needed (the event will arrive in the timeframe)
    //    - no maintenance window (I emptied it) => no scheduling
    //    - no error
    @Test
    public void successfulStart() throws Exception {
    	Integer identifier = 16;
    	
		// create the expectations for the mocks
    	//   for mockedJavaMailSender
    	Session session = null;
    	when(mockedJavaMailSender.createMimeMessage()).then(inv -> new MimeMessage(session));
    	
		// maintenance window is not needed in this test
    	this.jdbcTemplate.update("UPDATE event_source SET maintenance_window = '{}' WHERE id = 1", new HashMap<>());
    	this.jdbcTemplate.update("UPDATE error_detector SET multiple_events_cnt=3,multiple_events_timefr=60 WHERE id = 2", new HashMap<>());
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event (3-times) -----------------
    	// ---------------------------------------------------------------------------
    	//            FIRST
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier.toString());
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());
    	// get the event id by the identifier
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("identifier", identifier.toString());    	
    	List<Map<String, Object>> resultSet = jdbcTemplate.queryForList("SELECT id FROM event WHERE identifier = :identifier", parameters);
    	String eventId = resultSet.get(0).get("ID").toString();
    	
    	// the event cannot be confirmed and no commands to be executed can be retrieved for the event
    	//      no confirmation
    	request = new HashMap<>();
    	request.put("eventId", eventId);
    	request.put("byWhom", "vikhor");    	
    	mockMvc.perform( post("/executionflow/approve")
    			         .content(this.fromJson(request)).contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/executionflow/approve")))
    	       .andExpect(status().is4xxClientError());
    	//      no commands
    	String host = "urmlprod60";
    	mockMvc.perform( post("/worker/command/"+host)
		                .contentType(contentType)
		                .headers(buildHttpHeaders(HttpMethod.POST, null, URL_PREFIX + "/worker/command/"+host)))
  		       .andExpect( status().isOk() )
		       .andExpect(jsonPath("$", Matchers.hasSize(0)));

    	//            SECOND
    	request = new RequestBuilder().put("message", message)
    			                      .put("logLevel", "ERROR")
    			                      .put("uuid", (++identifier).toString())
    			                      .put("systmId", "3").build();
    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
				         .content(this.fromJson(request))
				         .contentType(contentType)
				         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
               .andExpect(status().isOk());
    	//            THIRD
    	request = new RequestBuilder().put("message", message)
    			                      .put("logLevel", "ERROR")
    			                      .put("uuid", (++identifier).toString())
    			                      .put("systmId", "3").build();
    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
				         .content(this.fromJson(request))
				         .contentType(contentType)
				         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
               .andExpect(status().isOk());
    	// ---------------------------------------------------------------------------
    	// --------------------------- 2. confirm the flow ---------------------------
    	// ---------------------------------------------------------------------------
    	request = new HashMap<>();
    	request.put("eventId", eventId);
    	request.put("byWhom", "vikhor");
    	
    	mockMvc.perform( post("/executionflow/approve") 
    					 .content(this.fromJson(request))
    					 .contentType(contentType)
    					 .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/executionflow/approve")))
               .andExpect(status().isOk());
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 3.a. get the commands to be executed (stopping the DB)
    	// ---------------------------------------------------------------------------
    	// get the event life id
    	resultSet = jdbcTemplate.queryForList("SELECT id FROM event_life WHERE event_id = :event_id AND status = 'WAITING_FOR_EXECUTION'", new RequestBuilder().put("event_id", eventId).build());
    	String eventLifeId = resultSet.get(0).get("ID").toString();

    	mockMvc.perform( post("/worker/command/"+host)
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, null, URL_PREFIX + "/worker/command/"+host)))
    	       .andExpect( status().isOk() )
    	       .andExpect(jsonPath("$", Matchers.hasSize(1)))
    	       .andExpect(jsonPath("$[0].eventLifeId", is(eventLifeId)))
    	       .andExpect(jsonPath("$[0].command", is("/etc/init.d/mysqld stop")))
    	       .andExpect(jsonPath("$[0].osUser", is("mysql")))
    	       .andExpect(jsonPath("$[0].outputPattern", is("Stopping mysqld:[ +]\\\\[  (?<COMMANDOUTPUT>[A-Z]+)  \\\\]")));
    	// ---------------------------------------------------------------------------
    	// --------------------------- 3.b. sending back the result of the command ---
    	// ---------------------------------------------------------------------------
    	String output1 = "Stopping mysqld:                                           [  OK  ]";
    	String extractedValue = "OK";
    	request = new HashMap<>();
    	request.put("eventLifeId", eventLifeId);
    	request.put("output", output1);
    	request.put("extractedValue", extractedValue);
    	request.put("successful", "TRUE");
    	mockMvc.perform( post("/worker/command/result")
				        .content(this.fromJson(request))
				        .contentType(contentType)
				        .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/worker/command/result")))
                .andExpect(status().isOk());
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 4.a. get the commands to be executed (stop Hermes SOA server 0)
    	// ---------------------------------------------------------------------------
    	// get the event life id
    	resultSet = jdbcTemplate.queryForList("SELECT id FROM event_life WHERE event_id = :event_id AND status = 'WAITING_FOR_EXECUTION'", new RequestBuilder().put("event_id", eventId).build());
    	eventLifeId = resultSet.get(0).get("ID").toString();
    	host = "urmlprod20";
    	mockMvc.perform( post("/worker/command/"+host)
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, null, URL_PREFIX + "/worker/command/"+host)))
    	       .andExpect( status().isOk() )
    	       .andExpect(jsonPath("$", Matchers.hasSize(1)))
    	       .andExpect(jsonPath("$[0].eventLifeId", is(eventLifeId)))
    	       .andExpect(jsonPath("$[0].command", is("cd /home/vikhor/projects/acme && ant container-stop")))
    	       .andExpect(jsonPath("$[0].osUser", is("")))
    	       .andExpect(jsonPath("$[0].outputPattern", is("     [echo] Status     : (?<COMMANDOUTPUT>[A-Z]+)")));
    	// ---------------------------------------------------------------------------
    	// --------------------------- 4.b. sending back the result of the command ---
    	// ---------------------------------------------------------------------------
    	String output2 = "container-status:\n"+
                 "[echo] ----------------------------------------------------------------------------------------------------------\n"+
    			 "[echo] Status     : DOWN\n"+
                 "[echo] Component  : wildfly10/10.1.0.Final";
    	extractedValue = "DOWN";
    	request = new HashMap<>();
    	request.put("eventLifeId", eventLifeId);
    	request.put("output", output2);
    	request.put("extractedValue", extractedValue);
    	request.put("successful", "TRUE");
    	mockMvc.perform( post("/worker/command/result")
				        .content(this.fromJson(request))
				        .contentType(contentType)
				        .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/worker/command/result")))
                .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------------- 5.a. get the commands to be executed (stop Hermes SOA server 1)
    	// ---------------------------------------------------------------------------
    	// get the event life id
    	resultSet = jdbcTemplate.queryForList("SELECT id FROM event_life WHERE event_id = :event_id AND status = 'WAITING_FOR_EXECUTION'", new RequestBuilder().put("event_id", eventId).build());
    	eventLifeId = resultSet.get(0).get("ID").toString();
    	host = "urmlprod21";
    	mockMvc.perform( post("/worker/command/"+host)
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, null, URL_PREFIX + "/worker/command/"+host)))
    	       .andExpect( status().isOk() )
    	       .andExpect(jsonPath("$", Matchers.hasSize(1)))
    	       .andExpect(jsonPath("$[0].eventLifeId", is(eventLifeId)))
    	       .andExpect(jsonPath("$[0].command", is("cd /home/vikhor/projects/acme && ant container-stop")))
    	       .andExpect(jsonPath("$[0].osUser", is("")))
    	       .andExpect(jsonPath("$[0].outputPattern", is("")));
    	// ---------------------------------------------------------------------------
    	// --------------------------- 5.b. sending back the result of the command ---
    	// ---------------------------------------------------------------------------
    	String output3 = "container-status:\n"+
                 "[echo] ----------------------------------------------------------------------------------------------------------\n"+
   			     "[echo] Status     : DOWN\n"+
                 "[echo] Component  : wildfly10/10.1.0.Final";
	   	extractedValue = "DOWN";
	   	request = new HashMap<>();
	   	request.put("eventLifeId", eventLifeId);
	   	request.put("output", output3);
	   	request.put("extractedValue", extractedValue);
	   	request.put("successful", "TRUE");
	   	mockMvc.perform( post("/worker/command/result")
					        .content(this.fromJson(request))
					        .contentType(contentType)
					        .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/worker/command/result")))
	               .andExpect(status().isOk());

    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	//                                 the mock
    	// sending mail that confirmation is needed
    	//                                 the mock
		verify(mockedJavaMailSender, times(4)).createMimeMessage();
		ArgumentCaptor<MimeMessage> captorMimeMessages = ArgumentCaptor.forClass(MimeMessage.class);
		verify(mockedJavaMailSender, times(4)).send(captorMimeMessages.capture());
		
    	// sending a mail to administrators that confirmation is needed
		assertEquals("Reaction Engine - Confirmation needed for the flow 'stop Hermes DB and app servers'", captorMimeMessages.getAllValues().get(0).getSubject());
		assertEquals(Constants.MAIL_FROM, (captorMimeMessages.getAllValues().get(0).getFrom()[0]).toString());
		assertEquals("ric.flair.wcw@gmail.com", captorMimeMessages.getAllValues().get(0).getRecipients(Message.RecipientType.TO)[0].toString());
		// sending mail at the beginning of the flow 
		assertEquals("Hermes PROD will be stopped", captorMimeMessages.getAllValues().get(1).getSubject());
		assertEquals(Constants.MAIL_FROM, (captorMimeMessages.getAllValues().get(1).getFrom()[0]).toString());
		assertEquals("reaction.engine.sup@gmail.com", captorMimeMessages.getAllValues().get(1).getRecipients(Message.RecipientType.TO)[0].toString());
		// send a mail to administrators that execution flow is started
		assertEquals("Reaction Engine - The execution flow 'stop Hermes DB and app servers' has just started!", captorMimeMessages.getAllValues().get(2).getSubject());
		assertEquals(Constants.MAIL_FROM, (captorMimeMessages.getAllValues().get(2).getFrom()[0]).toString());
		assertEquals("reaction.engine.sup@gmail.com", captorMimeMessages.getAllValues().get(2).getRecipients(Message.RecipientType.TO)[0].toString());
    	// sending mail at the end of the flow 
		assertEquals("Maintenance can be started, Hermes is DOWN", captorMimeMessages.getAllValues().get(3).getSubject());
		assertEquals(Constants.MAIL_FROM, (captorMimeMessages.getAllValues().get(3).getFrom()[0]).toString());
		assertEquals("reaction.engine.sup@gmail.com", captorMimeMessages.getAllValues().get(3).getRecipients(Message.RecipientType.TO)[0].toString());
		
    	verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd(identifier.toString(), message, output1, output2, output3);
    }


	private void checkDbAtEnd(String identifier, String message, String output1, String output2, String output3) throws IOException {
		// check the EVENT table
		List<Map<String,Object>> result = jdbcTemplate.queryForList("SELECT * FROM event", new MapSqlParameterSource());
    	assertEquals(1, result.size());
    	
    	Map<String,Object> rec = result.get(0);
    	assertNotNull(rec.get("END_DATE"));
    	assertNotNull(rec.get("FIRST_EVENT_ARRIVED"));
    	assertEquals("16", rec.get("IDENTIFIER"));
    	assertEquals("BY_LOG", rec.get("INITIATED_BY"));
    	assertEquals("ERROR", rec.get("LOG_LEVEL"));
    	assertEquals(message, rec.get("MESSAGE"));
    	assertEquals(3, rec.get("MULTIPLE_EVENTS_COUNTER"));
    	assertNull(rec.get("REASON"));
    	assertNotNull(rec.get("START_DATE"));
    	assertEquals("FINISHED", rec.get("STATUS"));
    	assertEquals(2l, rec.get("ERROR_DETECTOR_ID"));
    	assertEquals(1l, rec.get("EXECUTION_FLOW_ID"));
    	assertNull(rec.get("SCHEDULED_EXECUTION_FLOW_ID"));
    	assertEquals(3l, rec.get("EVENT_SOURCE_ID"));
    	
		// check the EVENT_LIFE table
    	result = jdbcTemplate.queryForList("SELECT * FROM event_life ORDER BY id", new MapSqlParameterSource());
    	assertEquals(9, result.size());
    	// CONFIRMATION_NEEDED
    	assertEquals("CONFIRMATION_NEEDED", result.get(0).get("STATUS"));
    	assertEquals(-2, result.get(0).get("ORDR"));
    	assertNull(result.get(0).get("TASK_ID"));
    	assertNull(result.get(0).get("BY_WHOM"));
    	List<Map<String,?>> history = toJson(result.get(0).get("HISTORY").toString(), List.class);
    	assertEquals(1, history.size());
    	assertEquals("CONFIRMATION_NEEDED", history.get(0).get("eventStatus"));
    	// CONFIRMATION is done
    	assertEquals("CONFIRMED", result.get(1).get("STATUS"));
    	assertEquals(-2, result.get(1).get("ORDR"));
    	assertNull(result.get(1).get("TASK_ID"));
    	assertEquals("vikhor", result.get(1).get("BY_WHOM"));
    	history = toJson(result.get(1).get("HISTORY").toString(), List.class);
    	assertEquals(1, history.size());
    	assertEquals("CONFIRMED", history.get(0).get("eventStatus"));
    	// send mail at the beginning
    	assertEquals("FINISHED", result.get(2).get("STATUS"));
    	assertEquals(0, result.get(2).get("ORDR"));
    	assertEquals(2l, result.get(2).get("TASK_ID"));
    	assertNull(result.get(2).get("BY_WHOM"));
    	history = toJson(result.get(2).get("HISTORY").toString(), List.class);
    	assertEquals(2, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("FINISHED", history.get(1).get("eventStatus"));
    	// stopping DB
    	assertEquals("FINISHED", result.get(3).get("STATUS"));
    	assertEquals(1, result.get(3).get("ORDR"));
    	assertEquals(1l, result.get(3).get("TASK_ID"));
    	assertNull(result.get(3).get("BY_WHOM"));
    	assertTrue((Boolean)result.get(3).get("EXT_COMMAND_SUCCESSFUL"));
    	assertEquals("OK", result.get(3).get("EXTRACTED_VALUE"));
    	assertEquals(output1, result.get(3).get("OUTPUT"));
    	history = toJson(result.get(3).get("HISTORY").toString(), List.class);
    	assertEquals(4, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("WAITING_FOR_EXECUTION", history.get(1).get("eventStatus"));
    	assertEquals("EXECUTION_STARTED", history.get(2).get("eventStatus"));
    	assertEquals("FINISHED", history.get(3).get("eventStatus"));
    	// if DB stopped correctly
    	assertEquals("FINISHED", result.get(4).get("STATUS"));
    	assertEquals(2, result.get(4).get("ORDR"));
    	assertEquals(4l, result.get(4).get("TASK_ID"));
    	assertNull(result.get(4).get("BY_WHOM"));
    	history = toJson(result.get(4).get("HISTORY").toString(), List.class);
    	assertEquals(2, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("FINISHED", history.get(1).get("eventStatus"));
    	// stop Hermes SOA server 0
    	assertEquals("FINISHED", result.get(5).get("STATUS"));
    	assertEquals(3, result.get(5).get("ORDR"));
    	assertEquals(5l, result.get(5).get("TASK_ID"));
    	assertNull(result.get(5).get("BY_WHOM"));
    	assertTrue((Boolean)result.get(5).get("EXT_COMMAND_SUCCESSFUL"));
    	assertEquals("DOWN", result.get(5).get("EXTRACTED_VALUE"));
    	assertEquals(output2, result.get(5).get("OUTPUT"));
    	history = toJson(result.get(5).get("HISTORY").toString(), List.class);
    	assertEquals(4, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("WAITING_FOR_EXECUTION", history.get(1).get("eventStatus"));
    	assertEquals("EXECUTION_STARTED", history.get(2).get("eventStatus"));
    	assertEquals("FINISHED", history.get(3).get("eventStatus"));
    	// if Hermes SOA 0 stopped correctly
    	assertEquals("FINISHED", result.get(6).get("STATUS"));
    	assertEquals(4, result.get(6).get("ORDR"));
    	assertEquals(10l, result.get(6).get("TASK_ID"));
    	assertNull(result.get(6).get("BY_WHOM"));
    	history = toJson(result.get(6).get("HISTORY").toString(), List.class);
    	assertEquals(2, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("FINISHED", history.get(1).get("eventStatus"));
    	// stop Hermes SOA server 1
    	assertEquals("FINISHED", result.get(7).get("STATUS"));
    	assertEquals(5, result.get(7).get("ORDR"));
    	assertEquals(8l, result.get(7).get("TASK_ID"));
    	assertNull(result.get(7).get("BY_WHOM"));
    	assertTrue((Boolean)result.get(7).get("EXT_COMMAND_SUCCESSFUL"));
    	assertEquals("DOWN", result.get(7).get("EXTRACTED_VALUE"));
    	assertEquals(output3, result.get(7).get("OUTPUT"));
    	history = toJson(result.get(7).get("HISTORY").toString(), List.class);
    	assertEquals(4, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("WAITING_FOR_EXECUTION", history.get(1).get("eventStatus"));
    	assertEquals("EXECUTION_STARTED", history.get(2).get("eventStatus"));
    	assertEquals("FINISHED", history.get(3).get("eventStatus"));
    	// Sending a mail to Middleware administrator , Hermes is DOWN
    	assertEquals("FINISHED", result.get(8).get("STATUS"));
    	assertEquals(6, result.get(8).get("ORDR"));
    	assertEquals(9l, result.get(8).get("TASK_ID"));
    	assertNull(result.get(8).get("BY_WHOM"));
    	history = toJson(result.get(8).get("HISTORY").toString(), List.class);
    	assertEquals(2, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("FINISHED", history.get(1).get("eventStatus"));
	}


	@After
	public void clearMocks() {
		Mockito.reset(mockedJavaMailSender);
	}

}