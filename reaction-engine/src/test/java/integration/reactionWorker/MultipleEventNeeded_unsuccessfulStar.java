package integration.reactionWorker;

import integration.TestConfig;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.reaction.engine.WebInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@ContextConfiguration(classes = {
		TestConfig.class
})
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@SqlGroup({
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/before-test-admin.sql", "classpath:sql/before-test-data.sql"}),
    @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/after-test.sql")
})
public class MultipleEventNeeded_unsuccessfulStar extends ParentIntegrationTest {

	    // for H2 management console UI -> debug - expressions : org.h2.tools.Server.startWebServer(((javax.sql.DataSource)context.getBean("dataSource")).getConnection())
	@Autowired
	private ApplicationContext context;

    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
	//    - multiple event is needed (the event WON'T arrive in the timeframe)
    //    - no maintenance window (I emptied it) => no scheduling
    //    - no error
    @Test
    public void unsuccessfulStart() throws Exception {
    	Integer identifier = 116;
    	
		// create the expectations for the mocks
    	//   for mockedJavaMailSender
    	Session session = null;
    	when(mockedJavaMailSender.createMimeMessage()).then(inv -> new MimeMessage(session));
    	
		// maintenance window is not needed in this test
    	this.jdbcTemplate.update("UPDATE event_source SET maintenance_window = '{}' WHERE id = 1", new HashMap<>());
    	// but multiple event is needed
    	this.jdbcTemplate.update("UPDATE error_detector SET multiple_events_cnt=3, multiple_events_timefr=1 WHERE id = 2", new HashMap<>());
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event (3-times) -----------------
    	// ---------------------------------------------------------------------------
    	//            FIRST
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier.toString());
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());
    	// get the event id by the identifier
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("identifier", identifier.toString());    	
    	List<Map<String, Object>> resultSet = jdbcTemplate.queryForList("SELECT id FROM event WHERE identifier = :identifier", parameters);
    	String eventId = resultSet.get(0).get("ID").toString();
    	
    	// the event cannot be confirmed and no commands to be executed can be retrieved for the event
    	//      no confirmation
    	request = new HashMap<>();
    	request.put("eventId", eventId);
    	request.put("byWhom", "vikhor");    	
    	mockMvc.perform( post("/executionflow/approve")
    			         .content(this.fromJson(request)).contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/executionflow/approve")))
    	       .andExpect(status().is4xxClientError());
    	//      no commands
    	String host = "urmlprod60";
    	mockMvc.perform( post("/worker/command/"+host)
		                .contentType(contentType)
		                .headers(buildHttpHeaders(HttpMethod.POST, null, URL_PREFIX + "/worker/command/"+host)))
  		       .andExpect( status().isOk() )
		       .andExpect(jsonPath("$", Matchers.hasSize(0)));

    	//            SECOND
    	request = new RequestBuilder().put("message", message)
    			                      .put("logLevel", "ERROR")
    			                      .put("uuid", (++identifier).toString())
    			                      .put("systmId", "3").build();
    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
				         .content(this.fromJson(request))
				         .contentType(contentType)
				         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
               .andExpect(status().isOk());
    	
    	// waiting for 2 sec so the flow won't be started (the timeframe is one sec)
    	Thread.sleep(2000);
    	
    	//            THIRD
    	request = new RequestBuilder().put("message", message)
    			                      .put("logLevel", "ERROR")
    			                      .put("uuid", (++identifier).toString())
    			                      .put("systmId", "3").build();
    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
				         .content(this.fromJson(request))
				         .contentType(contentType)
				         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
               .andExpect(status().isOk());
    	// ---------------------------------------------------------------------------
    	// --------------------------- 2. confirm the flow ---------------------------
    	// ---------------------------------------------------------------------------
    	request = new HashMap<>();
    	request.put("eventId", eventId);
    	request.put("byWhom", "vikhor");
    	
    	mockMvc.perform( post("/executionflow/approve") 
    					 .content(this.fromJson(request))
    					 .contentType(contentType)
    					 .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/executionflow/approve")))
               .andExpect(status().is4xxClientError());
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 3.a. get the commands to be executed (stopping the DB)
    	// ---------------------------------------------------------------------------
    	mockMvc.perform( post("/worker/command/"+host)
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, null, URL_PREFIX + "/worker/command/"+host)))
    	       .andExpect( status().isOk() )
    	       .andExpect(jsonPath("$", Matchers.hasSize(0)));

    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	//                                 the mock
    	verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd(identifier.toString(), message);
    }

	private void checkDbAtEnd(String identifier, String message) throws IOException {
		// check the EVENT table
		List<Map<String,Object>> result = jdbcTemplate.queryForList("SELECT * FROM event", new MapSqlParameterSource());
    	assertEquals(2, result.size());
    	
    	Map<String,Object> rec = result.get(0);
    	assertNull(rec.get("END_DATE"));
    	assertNotNull(rec.get("FIRST_EVENT_ARRIVED"));
    	assertEquals("116", rec.get("IDENTIFIER"));
    	assertEquals("BY_LOG", rec.get("INITIATED_BY"));
    	assertEquals("ERROR", rec.get("LOG_LEVEL"));
    	assertEquals(message, rec.get("MESSAGE"));
    	assertEquals(2, rec.get("MULTIPLE_EVENTS_COUNTER"));
    	assertNull(rec.get("REASON"));
    	assertNotNull(rec.get("START_DATE"));
    	assertEquals("WAITING_FOR_OTHERS", rec.get("STATUS"));
    	assertEquals(2l, rec.get("ERROR_DETECTOR_ID"));
    	assertEquals(1l, rec.get("EXECUTION_FLOW_ID"));
    	assertNull(rec.get("SCHEDULED_EXECUTION_FLOW_ID"));
    	assertEquals(3l, rec.get("EVENT_SOURCE_ID"));

    	rec = result.get(1);
    	assertNull(rec.get("END_DATE"));
    	assertNotNull(rec.get("FIRST_EVENT_ARRIVED"));
    	assertEquals("118", rec.get("IDENTIFIER"));
    	assertEquals("BY_LOG", rec.get("INITIATED_BY"));
    	assertEquals("ERROR", rec.get("LOG_LEVEL"));
    	assertEquals(message, rec.get("MESSAGE"));
    	assertEquals(1, rec.get("MULTIPLE_EVENTS_COUNTER"));
    	assertNull(rec.get("REASON"));
    	assertNotNull(rec.get("START_DATE"));
    	assertEquals("WAITING_FOR_OTHERS", rec.get("STATUS"));
    	assertEquals(2l, rec.get("ERROR_DETECTOR_ID"));
    	assertEquals(1l, rec.get("EXECUTION_FLOW_ID"));
    	assertNull(rec.get("SCHEDULED_EXECUTION_FLOW_ID"));
    	assertEquals(3l, rec.get("EVENT_SOURCE_ID"));

		// check the EVENT_LIFE table
    	result = jdbcTemplate.queryForList("SELECT * FROM event_life ORDER BY id", new MapSqlParameterSource());
    	assertEquals(0, result.size());
	}


	@After
	public void clearMocks() {
		Mockito.reset(mockedJavaMailSender);
	}

}