package integration.reactionWorker;

import com.jcraft.jsch.JSch;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.reaction.common.contants.Constants;
import org.reaction.common.domain.MaintenanceWindow;
import org.reaction.engine.WebInitializer;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.TaskService;
import org.reaction.engine.persistence.service.impl.RepositoryEventLifeService;
import org.reaction.engine.persistence.service.impl.RepositoryTaskService;
import org.reaction.engine.scheduling.ScheduledRunnable;
import org.reaction.engine.scheduling.Scheduler;
import org.reaction.engine.scheduling.impl.ThreadPoolScheduler;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.reaction.engine.service.impl.DefaultEventMachineServiceImpl;
import org.reaction.engine.service.impl.DefaultPlaceholderVariableServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
		Scheduled_dueToMaintenanceWindow.TestConfig.class
})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@SqlGroup({
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/before-test-admin.sql", "classpath:sql/before-test-data.sql"}),
    @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/after-test.sql")
})
public class Scheduled_dueToMaintenanceWindow extends ParentIntegrationTest {

	
	@Autowired
	@Qualifier("mockedThreadPoolTaskScheduler")
	private ThreadPoolTaskScheduler mockedThreadPoolTaskScheduler;
	
	
    // executing the 'stop Hermes DB and app servers' execution flow (see data-sql-images/stop Hermes DB and app servers.png)
    //    - without maintenance window (I emptied it, see from line 84) => no scheduling
    //    - no error
	//    - no full test but only till the first command
	//    ok, the issue is as follows: see in Scheduled_butForcedStarted
    @Test
    public void start() throws Exception {
    	String identifier = "1jfghg874hf65bhfg74hf60002";
    	
		// create the expectations for the mocks
    	//   for mockedJavaMailSender
    	Session session = null;
    	when(mockedJavaMailSender.createMimeMessage()).then(inv -> new MimeMessage(session));
    	//   for mockedThreadPoolTaskScheduler
    	Mockito.doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				ScheduledRunnable scheduledRunnable = invocation.getArgumentAt(0, ScheduledRunnable.class);
				
				MaintenanceWindow.isAlwaysInMaintenanceWindow = true;
				scheduledRunnable.run();
				MaintenanceWindow.isAlwaysInMaintenanceWindow = false;
				return null;
			}
		}).when(mockedThreadPoolTaskScheduler).schedule(any(Runnable.class), any(Date.class));
    	
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- 1. report the event ---------------------------
    	// ---------------------------------------------------------------------------
    	Map<String, String> request = new HashMap<>();
    	String message = "2017-06-28 11:39:44,515 INFO  [stdout] (ServerService Thread Pool -- 36) Caused by: java.lang.OutOfMemoryError: Java heap space";
    	request.put("message", message);
    	request.put("logLevel", "ERROR");
    	request.put("uuid", identifier);
    	request.put("systmId", "3");

    	mockMvc.perform( post(ENDPOINT_REACTION_WORKER)
    					 .content(this.fromJson(request))
    			         .contentType(contentType)
    			         .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + ENDPOINT_REACTION_WORKER)))
    	       .andExpect(status().isOk());
    	// get the event id by the identifier
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("identifier", identifier);    	
    	List<Map<String, Object>> resultSet = jdbcTemplate.queryForList("SELECT id FROM event WHERE identifier = :identifier", parameters);
    	String eventId = resultSet.get(0).get("ID").toString();

    	// ---------------------------------------------------------------------------
    	// --------------------------- 2. confirm the flow ---------------------------
    	// ---------------------------------------------------------------------------
    	request = new HashMap<>();
    	request.put("eventId", eventId);
    	request.put("byWhom", "vikhor");
    	
    	mockMvc.perform( post("/executionflow/approve") 
    					 .content(this.fromJson(request))
    					 .contentType(contentType)
    					 .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/executionflow/approve")))
    		    .andExpect(status().isOk());
    	
    	// ---------------------------------------------------------------------------
    	// --------------------------- Verifying -------------------------------------
    	// ---------------------------------------------------------------------------
    	//                                 the mock
		verify(mockedJavaMailSender, times(3)).createMimeMessage();
		ArgumentCaptor<MimeMessage> captorMimeMessages = ArgumentCaptor.forClass(MimeMessage.class);
		verify(mockedJavaMailSender, times(3)).send(captorMimeMessages.capture());

		// sending a mail to administrators that confirmation is needed
		assertEquals("Reaction Engine - Confirmation needed for the flow 'stop Hermes DB and app servers'", captorMimeMessages.getAllValues().get(0).getSubject());
		assertEquals(Constants.MAIL_FROM, (captorMimeMessages.getAllValues().get(0).getFrom()[0]).toString());
		assertEquals("ric.flair.wcw@gmail.com", captorMimeMessages.getAllValues().get(0).getRecipients(Message.RecipientType.TO)[0].toString());
		// sending mail at the beginning of the flow
		assertEquals("Hermes PROD will be stopped", captorMimeMessages.getAllValues().get(1).getSubject());
		assertEquals(Constants.MAIL_FROM, (captorMimeMessages.getAllValues().get(1).getFrom()[0]).toString());
		assertEquals("reaction.engine.sup@gmail.com", captorMimeMessages.getAllValues().get(1).getRecipients(Message.RecipientType.TO)[0].toString());
		// send a mail to administrators that execution flow is started
		assertEquals("Reaction Engine - The execution flow 'stop Hermes DB and app servers' has just started!", captorMimeMessages.getAllValues().get(2).getSubject());
		assertEquals(Constants.MAIL_FROM, (captorMimeMessages.getAllValues().get(2).getFrom()[0]).toString());
		assertEquals("reaction.engine.sup@gmail.com", captorMimeMessages.getAllValues().get(2).getRecipients(Message.RecipientType.TO)[0].toString());

		verify(mockedThreadPoolTaskScheduler, times(1)).schedule(any(ScheduledRunnable.class), any(Date.class));
		verify(mockedThreadPoolTaskScheduler, times(1)).setBeanName(any());
		//verify(mockedThreadPoolTaskScheduler, times(1)).setBeanName(any());

    	verifyNoMoreInteractions(mockedJavaMailSender);
    	
    	//                                 the database
    	checkDbAtEnd(identifier, message);
    }


	private void checkDbAtEnd(String identifier, String message) throws IOException {
		// check the EVENT table
		List<Map<String,Object>> result = jdbcTemplate.queryForList("SELECT * FROM event", new MapSqlParameterSource());
    	assertEquals(1, result.size());
    	
    	Map<String,Object> rec = result.get(0);
    	assertNull(rec.get("END_DATE"));
    	assertNull(rec.get("FIRST_EVENT_ARRIVED"));
    	assertEquals(identifier, rec.get("IDENTIFIER"));
    	assertEquals("BY_LOG", rec.get("INITIATED_BY"));
    	assertEquals("ERROR", rec.get("LOG_LEVEL"));
    	assertEquals(message, rec.get("MESSAGE"));
    	assertNull(rec.get("MULTIPLE_EVENTS_COUNTER"));
    	assertNull(rec.get("REASON"));
    	assertNotNull(rec.get("START_DATE"));
    	assertEquals("STARTED", rec.get("STATUS"));
    	assertEquals(2l, rec.get("ERROR_DETECTOR_ID"));
    	assertEquals(1l, rec.get("EXECUTION_FLOW_ID"));
    	assertNull(rec.get("SCHEDULED_EXECUTION_FLOW_ID"));
    	assertEquals(3l, rec.get("EVENT_SOURCE_ID"));
    	
		// check the EVENT_LIFE table
    	result = jdbcTemplate.queryForList("SELECT * FROM event_life ORDER BY id", new MapSqlParameterSource());
    	assertEquals(5, result.size());
    	// CONFIRMATION_NEEDED
    	assertEquals("CONFIRMATION_NEEDED", result.get(0).get("STATUS"));
    	assertEquals(-2, result.get(0).get("ORDR"));
    	assertNull(result.get(0).get("TASK_ID"));
    	assertNull(result.get(0).get("BY_WHOM"));
    	List<Map<String,?>> history = toJson(result.get(0).get("HISTORY").toString(), List.class);
    	assertEquals(1, history.size());
    	assertEquals("CONFIRMATION_NEEDED", history.get(0).get("eventStatus"));
    	// CONFIRMATION is done
    	assertEquals("CONFIRMED", result.get(1).get("STATUS"));
    	assertEquals(-2, result.get(1).get("ORDR"));
    	assertNull(result.get(1).get("TASK_ID"));
    	assertEquals("vikhor", result.get(1).get("BY_WHOM"));
    	history = toJson(result.get(1).get("HISTORY").toString(), List.class);
    	assertEquals(1, history.size());
    	assertEquals("CONFIRMED", history.get(0).get("eventStatus"));
    	// SCHEDULED
    	assertEquals("SCHEDULED", result.get(2).get("STATUS"));
    	assertEquals(-1, result.get(2).get("ORDR"));
    	assertNull(result.get(2).get("TASK_ID"));
    	history = toJson(result.get(2).get("HISTORY").toString(), List.class);
    	assertEquals(1, history.size());
    	assertEquals("SCHEDULED", history.get(0).get("eventStatus"));
    	// send mail at the beginning
    	assertEquals("FINISHED", result.get(3).get("STATUS"));
    	assertEquals(0, result.get(3).get("ORDR"));
    	assertEquals(2l, result.get(3).get("TASK_ID"));
    	assertNull(result.get(3).get("BY_WHOM"));
    	history = toJson(result.get(3).get("HISTORY").toString(), List.class);
    	assertEquals(2, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("FINISHED", history.get(1).get("eventStatus"));
    	// stopping DB
    	assertEquals("WAITING_FOR_EXECUTION", result.get(4).get("STATUS"));
    	assertEquals(1, result.get(4).get("ORDR"));
    	assertEquals(1l, result.get(4).get("TASK_ID"));
    	assertNull(result.get(4).get("BY_WHOM"));
    	assertNull(result.get(4).get("EXT_COMMAND_SUCCESSFUL"));
    	assertNull(result.get(4).get("EXTRACTED_VALUE"));
    	assertNull(result.get(4).get("OUTPUT"));
    	history = toJson(result.get(4).get("HISTORY").toString(), List.class);
    	assertEquals(2, history.size());
    	assertEquals("STARTED", history.get(0).get("eventStatus"));
    	assertEquals("WAITING_FOR_EXECUTION", history.get(1).get("eventStatus"));
	}


	@After
	public void clearMocks() {
		Mockito.reset(mockedJavaMailSender);
	}


	@Configuration
	@ComponentScan(basePackages = {
			"org.reaction.engine.persistence.repository",
			"org.reaction.engine.persistence.service.impl",
			"org.reaction.engine.controller",
			"org.reaction.engine.persistence.service",
			"org.reaction.engine.persistence.converter",
			"org.reaction.engine.service",
			"org.reaction.engine.errors",
			"org.reaction.engine.notification",
			"org.reaction.engine.notification.message.converter",
			"org.reaction.engine.scheduling.utils"
	})
	public static class TestConfig {

		@Primary
		@Bean
		public EventMachineService eventMachineService() {
			return new DefaultEventMachineServiceImpl();
		}

		@Primary
		@Bean
		public JSch jSch() {
			return mock(JSch.class);
		}

		@Bean
		public TaskService taskService() {
			return new RepositoryTaskService();
		}

		@Bean
		public Scheduler scheduler() {
			return new ThreadPoolScheduler();
		}
		@Bean
		public ThreadPoolTaskScheduler mockedThreadPoolTaskScheduler() {
			return mock(ThreadPoolTaskScheduler.class);
		}

		@Bean
		public PlaceholderVariableService placeholderVariableService() {
			return new DefaultPlaceholderVariableServiceImpl();
		}

		@Bean
		public JavaMailSender javaMailSender() {
			return mock(JavaMailSender.class);
		}

		@Bean
		public EventLifeService eventLifeService() {
			return new RepositoryEventLifeService();
		}
	}

}