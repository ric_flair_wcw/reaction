package integration.reactionWorker;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.reaction.common.security.HmacSignatureBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.LocalManagementPort;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertNotNull;

public class ParentIntegrationTest {

	
	protected String URL_PREFIX = "http://localhost";
	protected static final String ENDPOINT_REACTION_WORKER = "/worker/event";

	
    @LocalServerPort
    protected int randomServerPort;
    @LocalManagementPort
    protected int randomManagementPort;
    
    protected MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
                                                  MediaType.APPLICATION_JSON.getSubtype(),
                                                  Charset.forName("utf8"));
	protected HttpMessageConverter mappingJackson2HttpMessageConverter;
	
	@Autowired
	protected MockMvc mockMvc;
    @Autowired
    protected NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    protected JavaMailSender mockedJavaMailSender;
    protected ObjectMapper objectMapper = new ObjectMapper();
    

	protected String fromJson(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


    protected <T> T toJson(String s, Class<T> clazz) throws IOException {
    	return this.objectMapper.readValue(s, clazz);
    }
    
    
    protected String readFile(String path) throws IOException {
		return StreamUtils.copyToString(new ClassPathResource(path).getInputStream(), Charset.defaultCharset());
	}

    
    protected HttpHeaders buildHttpHeaders(HttpMethod httpMethod, Object objectToBeSent, String url) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        String apiKey = "localhost";
        String apiSecret = "f0dedb78-3eb6-4a56-8428-e8e40584a01c";

        String nonce = UUID.randomUUID().toString();

        httpHeaders.setDate(Clock.systemUTC().millis());
        String dateString = httpHeaders.getFirst(HttpHeaders.DATE);

        String valueAsString = null;
        if (objectToBeSent != null) {
	        try {
	            valueAsString = objectMapper.writeValueAsString(objectToBeSent);
	        } catch (JsonProcessingException e) {
	            throw new RuntimeException(e);
	        }
        }

        final HmacSignatureBuilder signatureBuilder = new HmacSignatureBuilder()
                .method(httpMethod.name())
                .endpoint(url)
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .date(dateString)
                .nonce(nonce)
                .apiKey(apiKey)
                .apiSecret(apiSecret)
                .payload(valueAsString != null ? valueAsString.getBytes(StandardCharsets.UTF_8) : new byte[0]);
        final String signature = signatureBuilder.buildAsBase64String();

        final String authHeader = signatureBuilder.getAlgorithm() + " " + apiKey + ":" + nonce + ":" + signature;
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authHeader);

        httpHeaders.add(HttpHeaders.USER_AGENT, "Reaction Rest client");
        
        return httpHeaders;
	}

    
    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
										            .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
										            .findAny()
										            .orElse(null);
        assertNotNull("the JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }
    
    
    public static class RequestBuilder {
    	
    	Map<String, String> request = new HashMap<>();
    	
    	public RequestBuilder put(String key, String value) {
    		request.put(key, value);
    		return this;
    	}
    	
    	public Map<String, String> build() {
    		return request;
    	}
    }


}