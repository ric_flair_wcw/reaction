package integration;

import com.jcraft.jsch.JSch;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.TaskService;
import org.reaction.engine.persistence.service.impl.RepositoryEventLifeService;
import org.reaction.engine.persistence.service.impl.RepositoryTaskService;
import org.reaction.engine.scheduling.Scheduler;
import org.reaction.engine.scheduling.impl.ThreadPoolScheduler;
import org.reaction.engine.service.PlaceholderVariableService;
import org.reaction.engine.service.impl.DefaultPlaceholderVariableServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import static org.mockito.Mockito.mock;

@Configuration
@ComponentScan(basePackages = {
        "org.reaction.engine.persistence.repository",
        "org.reaction.engine.persistence.service.impl",
        "org.reaction.engine.controller",
        "org.reaction.engine.persistence.service",
        "org.reaction.engine.persistence.converter",
        "org.reaction.engine.service",
        "org.reaction.engine.errors",
        "org.reaction.engine.notification",
        "org.reaction.engine.notification.message.converter",
        "org.reaction.engine.scheduling.utils"
})
public class TestConfig {

    @Primary
    @Bean
    public JSch jSch() {
        return mock(JSch.class);
    }

    @Bean
    public TaskService taskService() {
        return new RepositoryTaskService();
    }

    @Bean
    public Scheduler scheduler() {
        return new ThreadPoolScheduler();
    }

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    @Bean
    public PlaceholderVariableService placeholderVariableService() {
        return new DefaultPlaceholderVariableServiceImpl();
    }

    @Bean
    public JavaMailSender javaMailSender() {
        return mock(JavaMailSender.class);
    }

    @Bean
    public EventLifeService eventLifeService() {
        return new RepositoryEventLifeService();
    }

}
