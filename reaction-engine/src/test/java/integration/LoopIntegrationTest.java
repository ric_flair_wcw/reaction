package integration;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import integration.reactionWorker.ParentIntegrationTest;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;
import org.reaction.engine.WebInitializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@ContextConfiguration(classes = {
        TestConfig.class
})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@AutoConfigureMockMvc
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/before-test-admin.sql", "classpath:sql/before-test-data-loop.sql"}),
        @Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/after-test.sql")
})
@ActiveProfiles(profiles = "singleThreadPool")
public class LoopIntegrationTest extends ParentIntegrationTest {


    private static final String EXECUTIONFLOW_START_MANUAL_URL = "/executionflow/start/manual";

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    private JSch mockedJSch;
    // for H2 management console UI -> debug - expressions : org.h2.tools.Server.startWebServer(((javax.sql.DataSource)context.getBean("dataSource")).getConnection())
//	@Autowired
//	private ApplicationContext context;


    @Before
    public void setUp() {
        Mockito.reset(mockedJSch, mockedJavaMailSender);
    }


    @Test
    public void startFlow() throws Exception {
        Session mockedSessionStartBlackout = mock(Session.class);
        ChannelExec mockedChannelExecStartBlackout = mock(ChannelExec.class);
        whenCallingSSHCommand(mockedSessionStartBlackout, mockedChannelExecStartBlackout, "The blackout has started!\n0", "");

        Session mockedSessionStopServerCycle0 = mock(Session.class);
        ChannelExec mockedChannelStopServerCycle0 = mock(ChannelExec.class);
        whenCallingSSHCommand(mockedSessionStopServerCycle0, mockedChannelStopServerCycle0, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", "");
        Session mockedSessionStopServerCycle1 = mock(Session.class);
        ChannelExec mockedChannelStopServerCycle1 = mock(ChannelExec.class);
        whenCallingSSHCommand(mockedSessionStopServerCycle1, mockedChannelStopServerCycle1, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", "");
        Session mockedSessionStopServerCycle2 = mock(Session.class);
        ChannelExec mockedChannelStopServerCycle2 = mock(ChannelExec.class);
        whenCallingSSHCommand(mockedSessionStopServerCycle2, mockedChannelStopServerCycle2, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", "");

        Session mockedSessionStopBlackout = mock(Session.class);
        ChannelExec mockedChannelExecStopBlackout = mock(ChannelExec.class);
        whenCallingSSHCommand(mockedSessionStopBlackout, mockedChannelExecStopBlackout, "The blackout has stopped.\n0", "");

        Session mockedSessionHousekeeping = mock(Session.class);
        ChannelExec mockedChannelExecHousekeeping = mock(ChannelExec.class);
        whenCallingSSHCommand(mockedSessionHousekeeping, mockedChannelExecHousekeeping, "The housekeeping is done!\n0", "");

        when(mockedJSch.getSession("tp64856", "ACME1", 22)).thenReturn(mockedSessionStartBlackout, mockedSessionStopServerCycle0, mockedSessionStopServerCycle1, mockedSessionStopServerCycle2, mockedSessionStopBlackout);
        when(mockedJSch.getSession("tp65789", "ACME2", 22)).thenReturn(mockedSessionStartBlackout, mockedSessionStopServerCycle0, mockedSessionStopServerCycle1, mockedSessionStopServerCycle2, mockedSessionStopBlackout);
        when(mockedJSch.getSession("notimportant", "DO_NOT_RUN_ON_IT", 22)).thenReturn(mockedSessionStartBlackout, mockedSessionStopServerCycle0, mockedSessionStopServerCycle1, mockedSessionStopServerCycle2,
                mockedSessionStopBlackout);
        when(mockedJSch.getSession("vikhor", "ACME9", 22)).thenReturn(mockedSessionHousekeeping);

        whenCallingEmailSending();

        // --------------------------- starting the flow ---------------------------
        Map<String,Object> request = new HashMap<>();
        request.put("executionFlowId", 51);
        startTheFlow(request);

        // --------------------------- getting the commands to be executed and sending back the result ---------------------------
        // LOOP through hosts - index: 0
        String eventLifeId = getCommandsToBeExecutedByWorker("ACME1", "ACME1-SERVER", "tp64856");
        sendBackResult(eventLifeId);
        eventLifeId = getCommandsToBeExecutedByWorker("ACME1", "ACME2-SERVER", "tp64856");
        sendBackResult(eventLifeId);
        eventLifeId = getCommandsToBeExecutedByWorker("ACME1", "DO_NOT_RUN_ON_IT", "tp64856");
        sendBackResult(eventLifeId);
        // LOOP through hosts - index: 1
        eventLifeId = getCommandsToBeExecutedByWorker("ACME2", "ACME1-SERVER", "tp65789");
        sendBackResult(eventLifeId);
        eventLifeId = getCommandsToBeExecutedByWorker("ACME2", "ACME2-SERVER", "tp65789");
        sendBackResult(eventLifeId);
        eventLifeId = getCommandsToBeExecutedByWorker("ACME2", "DO_NOT_RUN_ON_IT", "tp65789");
        sendBackResult(eventLifeId);

        // verifying
        verify(mockedJSch, times(5)).getSession("tp64856", "ACME1", 22);
        verify(mockedJSch, times(5)).getSession("tp65789", "ACME2", 22);
        verify(mockedJSch, times(5)).getSession("notimportant", "DO_NOT_RUN_ON_IT", 22);
        verify(mockedJSch, times(1)).getSession("vikhor", "ACME9", 22);

        verifyIfSSHCommandIsCalled(mockedSessionStartBlackout, mockedChannelExecStartBlackout, "secret1", "/etc/rc0.d/start_blackout.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStartBlackout, mockedChannelExecStartBlackout, "secret2", "/etc/rc0.d/start_blackout.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStartBlackout, mockedChannelExecStartBlackout, "secret3", "/etc/rc0.d/start_blackout.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle0, mockedChannelStopServerCycle0, "secret1", "/wls-12/wls12213/user_projects/domains/" + "ACME1-SERVER" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle0, mockedChannelStopServerCycle0, "secret2", "/wls-12/wls12213/user_projects/domains/" + "ACME1-SERVER" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle0, mockedChannelStopServerCycle0, "secret3", "/wls-12/wls12213/user_projects/domains/" + "ACME1-SERVER" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle1, mockedChannelStopServerCycle1, "secret1", "/wls-12/wls12213/user_projects/domains/" + "ACME2-SERVER" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle1, mockedChannelStopServerCycle1, "secret2", "/wls-12/wls12213/user_projects/domains/" + "ACME2-SERVER" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle1, mockedChannelStopServerCycle1, "secret3", "/wls-12/wls12213/user_projects/domains/" + "ACME2-SERVER" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle2, mockedChannelStopServerCycle2, "secret1", "/wls-12/wls12213/user_projects/domains/" + "DO_NOT_RUN_ON_IT" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle2, mockedChannelStopServerCycle2, "secret2", "/wls-12/wls12213/user_projects/domains/" + "DO_NOT_RUN_ON_IT" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopServerCycle2, mockedChannelStopServerCycle2, "secret3", "/wls-12/wls12213/user_projects/domains/" + "DO_NOT_RUN_ON_IT" + "/bin/stopServer.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopBlackout, mockedChannelExecStopBlackout, "secret1", "/etc/rc0.d/stop_blackout.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopBlackout, mockedChannelExecStopBlackout, "secret2", "/etc/rc0.d/stop_blackout.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionStopBlackout, mockedChannelExecStopBlackout, "secret3", "/etc/rc0.d/stop_blackout.sh; echo $?", 1, 3);
        verifyIfSSHCommandIsCalled(mockedSessionHousekeeping, mockedChannelExecHousekeeping, "secret1", "/etc/rc0.d/housekeeping.sh; echo $?", 1, 1);

        verifyEmailSending();

        verifyNoMoreInteractions(mockedJSch, mockedJavaMailSender, mockedSessionStartBlackout, mockedSessionStopServerCycle0, mockedSessionStopServerCycle1, mockedSessionStopServerCycle2,
                mockedSessionStopBlackout, mockedSessionHousekeeping);

        checkDB();
    }

    private void sendBackResult(String eventLifeId) throws Exception {
        Map<String, Object> request;
        // ---------------------------------------------------------------------------
        // --------------------------- sending back the result of the command (start the server): 1st cycle
        request = new HashMap<>();
        request.put("eventLifeId", eventLifeId);
        request.put("output", "Starting and ...\nThe server started successfully.\nDi-gi-sziii!");
        request.put("extractedValue", "successfully");
        request.put("successful", "TRUE");
        mockMvc.perform(post("/worker/command/result")
                .content(this.fromJson(request))
                .contentType(contentType)
                .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + "/worker/command/result")))
                .andExpect(status().isOk());
    }


    private String getCommandsToBeExecutedByWorker(String host, String server, String osUser) throws Exception {
        // ---------------------------------------------------------------------------
        // --------------------------- get the commands to be executed (start the server): 1st cycle
        // get the event life id
        List<Map<String, Object>> resultSet = jdbcTemplate.queryForList("SELECT id FROM event_life WHERE task_id = :task_id AND status = 'WAITING_FOR_EXECUTION'", new RequestBuilder().put("task_id", "239").build());
        String eventLifeId = resultSet.get(0).get("ID").toString();
        mockMvc.perform(post("/worker/command/" + host).contentType(contentType)
                .headers(buildHttpHeaders(HttpMethod.POST, null, URL_PREFIX + "/worker/command/" + host)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].eventLifeId", is(eventLifeId)))
                .andExpect(jsonPath("$[0].command", is("/wls-12/wls12213/user_projects/domains/" + server + "/bin/startServer.sh")))
                .andExpect(jsonPath("$[0].osUser", is(osUser)))
                .andExpect(jsonPath("$[0].outputPattern", is("The server started (?<COMMANDOUTPUT>[a-z]+).")));
        return eventLifeId;
    }


    private void startTheFlow(Map<String, Object> request) throws Exception {
        mockMvc.perform(post(EXECUTIONFLOW_START_MANUAL_URL)
                        .content(this.fromJson(request))
                        .contentType(contentType)
                        .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + EXECUTIONFLOW_START_MANUAL_URL)))
               .andExpect(status().isOk());
    }


    private void verifyEmailSending() throws MessagingException {
        verify(mockedJavaMailSender, times(10)).createMimeMessage();
        ArgumentCaptor<MimeMessage> captorMimeMessage = ArgumentCaptor.forClass(MimeMessage.class);
        verify(mockedJavaMailSender, times(10)).send(captorMimeMessage.capture());
        List<MimeMessage> mimeMessages = captorMimeMessage.getAllValues();
        assertEquals("The servers will be restarted", mimeMessages.get(0).getSubject());
        // loop through hosts - cycle 0
        assertEquals("The server [ACME1-SERVER] restarted successfully on the host [ACME1]", mimeMessages.get(1).getSubject());
        assertEquals("The server [ACME2-SERVER] restarted successfully on the host [ACME1]", mimeMessages.get(2).getSubject());
        assertEquals("The server did not start successfully", mimeMessages.get(3).getSubject());
        // loop through hosts - cycle 1
        assertEquals("The server [ACME1-SERVER] restarted successfully on the host [ACME2]", mimeMessages.get(4).getSubject());
        assertEquals("The server [ACME2-SERVER] restarted successfully on the host [ACME2]", mimeMessages.get(5).getSubject());
        assertEquals("The server did not start successfully", mimeMessages.get(6).getSubject());
        // loop through hosts - cycle 2
        assertEquals("the server did not stop successfully", mimeMessages.get(7).getSubject());
        assertEquals("the server did not stop successfully", mimeMessages.get(8).getSubject());
        assertEquals("the server did not stop successfully", mimeMessages.get(9).getSubject());
    }

    private void whenCallingEmailSending() {
        when(mockedJavaMailSender.createMimeMessage()).thenReturn(new MimeMessage((javax.mail.Session) null), new MimeMessage((javax.mail.Session) null), new MimeMessage((javax.mail.Session) null),
                new MimeMessage((javax.mail.Session) null), new MimeMessage((javax.mail.Session) null), new MimeMessage((javax.mail.Session) null), new MimeMessage((javax.mail.Session) null),
                new MimeMessage((javax.mail.Session) null), new MimeMessage((javax.mail.Session) null), new MimeMessage((javax.mail.Session) null));
    }


    private void verifyIfSSHCommandIsCalled(Session mockedSession, ChannelExec mockedChannelExec, String password, String command, int times1, int times2) throws JSchException, IOException {
        verify(mockedSession, times(times1)).setPassword(password.getBytes(StandardCharsets.UTF_8));
        verify(mockedSession, times(times2)).setConfig("StrictHostKeyChecking", "no");
        verify(mockedSession, times(times2)).connect();
        verify(mockedSession, times(times2)).openChannel("exec");
        verify(mockedChannelExec, times(times2)).setCommand(command.getBytes(StandardCharsets.UTF_8));
        verify(mockedChannelExec, times(times2)).setInputStream(null);
        verify(mockedChannelExec, times(times2)).connect();
        verify(mockedChannelExec, times(times2)).getInputStream();
        verify(mockedChannelExec, times(times2)).getErrStream();
        verify(mockedChannelExec, times(times2)).disconnect();
        verify(mockedSession, times(times2)).disconnect();
    }


    private void whenCallingSSHCommand(Session mockedSession, ChannelExec mockedChannelExec, String commandOutput, String commandErrorOutput) throws JSchException, IOException {
        when(mockedSession.openChannel("exec")).thenReturn(mockedChannelExec);
        when(mockedChannelExec.getInputStream()).thenAnswer((Answer<InputStream>) invocation -> new ByteArrayInputStream(commandOutput.getBytes()));
        when(mockedChannelExec.getErrStream()).thenAnswer((Answer<InputStream>) invocation -> new ByteArrayInputStream(commandErrorOutput.getBytes()));
        when(mockedChannelExec.isClosed()).thenReturn(true);
    }


    private void checkDB() throws IOException {
        // check the EVENT table
        List<Map<String,Object>> result = jdbcTemplate.queryForList("SELECT * FROM event", new MapSqlParameterSource());
        assertEquals(1, result.size());

        Map<String,Object> rec = result.get(0);
        assertEquals("FINISHED", rec.get("STATUS"));
        assertEquals(51l, rec.get("EXECUTION_FLOW_ID"));
        assertNotNull(rec.get("END_DATE"));

        // check the EVENT_LIFE table
        result = jdbcTemplate.queryForList("SELECT * FROM event_life ORDER BY id", new MapSqlParameterSource());
        assertEquals(63, result.size());
        // checkEventLife(List<Map<String, Object>> result, int i, String status, String extractedValue, int loopIndex, String output, int taskId)
        checkEventLife(result, 0, "FINISHED", null, null, null, 263);
        // loop through hosts - cycle 0
        checkEventLife(result, 1, "CYCLE_STARTED", null, 0, null, 230);
        checkEventLife(result, 2, "FINISHED", "0", null, "The blackout has started!\n0", 244);
        //     loop through servers - cycle 0
        checkEventLife(result, 3, "CYCLE_STARTED", null, 0, null, 231);
        checkEventLife(result, 4, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 5, "FINISHED", null, null, "true", 235);
        checkEventLife(result, 6, "FINISHED", "successfully", null, "Starting and ...\nThe server started successfully.\nDi-gi-sziii!", 239);
        checkEventLife(result, 7, "FINISHED", null, null, "true", 240);
        checkEventLife(result, 8, "FINISHED", null, null, null, 245);
        //     loop through servers - cycle 1
        checkEventLife(result, 9, "CYCLE_STARTED", null, 1, null, 231);
        checkEventLife(result, 10, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 11, "FINISHED", null, null, "true", 235);
        checkEventLife(result, 12, "FINISHED", "successfully", null, "Starting and ...\nThe server started successfully.\nDi-gi-sziii!", 239);
        checkEventLife(result, 13, "FINISHED", null, null, "true", 240);
        checkEventLife(result, 14, "FINISHED", null, null, null, 245);
        //     loop through servers - cycle 2
        checkEventLife(result, 15, "CYCLE_STARTED", null, 2, null, 231);
        checkEventLife(result, 16, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 17, "FINISHED", null, null, "true", 235);
        checkEventLife(result, 18, "FINISHED", "successfully", null, "Starting and ...\nThe server started successfully.\nDi-gi-sziii!", 239);
        checkEventLife(result, 19, "FINISHED", null, null, "false", 240);
        checkEventLife(result, 20, "FINISHED", null, null, null, 249);
        checkEventLife(result, 21, "LOOP_COMPLETED", null, null, null, 231);
        checkEventLife(result, 22, "FINISHED", "0", null, "The blackout has stopped.\n0", 262);

        // loop through hosts - cycle 1
        checkEventLife(result, 23, "CYCLE_STARTED", null, 1, null, 230);
        checkEventLife(result, 24, "FINISHED", "0", null, "The blackout has started!\n0", 244);
        //     loop through servers - cycle 0
        checkEventLife(result, 25, "CYCLE_STARTED", null, 0, null, 231);
        checkEventLife(result, 26, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 27, "FINISHED", null, null, "true", 235);
        checkEventLife(result, 28, "FINISHED", "successfully", null, "Starting and ...\nThe server started successfully.\nDi-gi-sziii!", 239);
        checkEventLife(result, 29, "FINISHED", null, null, "true", 240);
        checkEventLife(result, 30, "FINISHED", null, null, null, 245);
        //     loop through servers - cycle 1
        checkEventLife(result, 31, "CYCLE_STARTED", null, 1, null, 231);
        checkEventLife(result, 32, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 33, "FINISHED", null, null, "true", 235);
        checkEventLife(result, 34, "FINISHED", "successfully", null, "Starting and ...\nThe server started successfully.\nDi-gi-sziii!", 239);
        checkEventLife(result, 35, "FINISHED", null, null, "true", 240);
        checkEventLife(result, 36, "FINISHED", null, null, null, 245);
        //     loop through servers - cycle 2
        checkEventLife(result, 37, "CYCLE_STARTED", null, 2, null, 231);
        checkEventLife(result, 38, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 39, "FINISHED", null, null, "true", 235);
        checkEventLife(result, 40, "FINISHED", "successfully", null, "Starting and ...\nThe server started successfully.\nDi-gi-sziii!", 239);
        checkEventLife(result, 41, "FINISHED", null, null, "false", 240);
        checkEventLife(result, 42, "FINISHED", null, null, null, 249);
        checkEventLife(result, 43, "LOOP_COMPLETED", null, null, null, 231);
        checkEventLife(result, 44, "FINISHED", "0", null, "The blackout has stopped.\n0", 262);

        // loop through hosts - cycle 2
        checkEventLife(result, 45, "CYCLE_STARTED", null, 2, null, 230);
        checkEventLife(result, 46, "FINISHED", "0", null, "The blackout has started!\n0", 244);
        //     loop through servers - cycle 0
        checkEventLife(result, 47, "CYCLE_STARTED", null, 0, null, 231);
        checkEventLife(result, 48, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 49, "FINISHED", null, null, "false", 235);
        checkEventLife(result, 50, "FINISHED", null, null, null, 248);
        //     loop through servers - cycle 1
        checkEventLife(result, 51, "CYCLE_STARTED", null, 1, null, 231);
        checkEventLife(result, 52, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 53, "FINISHED", null, null, "false", 235);
        checkEventLife(result, 54, "FINISHED", null, null, null, 248);
        //     loop through servers - cycle 2
        checkEventLife(result, 55, "CYCLE_STARTED", null, 2, null, 231);
        checkEventLife(result, 56, "FINISHED", "successfully", null, "The server will be stopped in a jiffy...\nThe server stopped successfully.\nCongratulations!\n0", 247);
        checkEventLife(result, 57, "FINISHED", null, null, "false", 235);
        checkEventLife(result, 58, "FINISHED", null, null, null, 248);
        checkEventLife(result, 59, "LOOP_COMPLETED", null, null, null, 231);
        checkEventLife(result, 60, "FINISHED", "0", null, "The blackout has stopped.\n0", 262);

        checkEventLife(result, 61, "LOOP_COMPLETED", null, null, null, 230);
        checkEventLife(result, 62, "FINISHED", "0", null, "The housekeeping is done!\n0", 264);
    }

    private void checkEventLife(List<Map<String, Object>> result, int i, String status, String extractedValue, Integer loopIndex, String output, long taskId) {
        Map<String, Object> rec = result.get(i);
        assertEquals(status, rec.get("STATUS"));
        assertEquals(extractedValue, rec.get("EXTRACTED_VALUE"));
        assertEquals(loopIndex, rec.get("LOOP_INDEX"));
        assertEquals(output, rec.get("OUTPUT"));
        assertEquals(taskId, rec.get("TASK_ID"));
        assertEquals(i, rec.get("ORDR"));
    }

}
