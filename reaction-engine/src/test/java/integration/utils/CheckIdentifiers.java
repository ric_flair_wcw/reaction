package integration.utils;

import integration.TestConfig;
import integration.reactionWorker.ParentIntegrationTest;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.reaction.engine.WebInitializer;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@ContextConfiguration(classes = {
		TestConfig.class
})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@AutoConfigureMockMvc
public class CheckIdentifiers extends ParentIntegrationTest {

	
	private String url = "/utils/identifiers/check";
	
	
	@Test
	public void successful_XML() throws Exception {
		// preparing the request to be sent
		// see the message in integrationTest/otherInput/xmlinput-req.xml
		Map<String, Object> request = new HashMap<>();
		request.put("message", "PHNvYXBlbnY6RW52ZWxvcGUKCXhtbG5zOnNvYXBlbnY9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIgoJeG1sbnM6bnM9Imh0dHA6Ly9hY21lLmNvbS9ucyI+Cgk8c29hcGVudjpCb2R5PgoJCTxuczpyZXF1ZXN0PgoJCQk8bnM6ZXZlbnQ+CgkJCQk8bnM6aG9zdD5sb2NhbGhvc3Q8L25zOmhvc3Q+CgkJCQk8bnM6c291cmNlPi9ob21lL3Zpa2hvci93b3JrL3JlYWN0aW9uL3RtcC9yZWFjdGlvbi1lbmdpbmUtc3RhbmRhbG9uZS0xLjEvbG9ncy9yZWFjdGlvbi1lbmdpbmUubG9nPC9uczpzb3VyY2U+CgkJCQk8bnM6bWVzc2FnZT4yMDE5LTA3LTI1IDE1OjI1OjQwLjM5MyBFUlJPUiBbb3JnLnJlYWN0aW9uLmVuZ2luZS5SZWFjdGlvbkFwcGxpY2F0aW9uOjUzXSAgLSBFcnJvciBvY2N1cmVkIHdoaWxlIHN0b3BwaW5nIHRoZSBhcHBsaWNhdGlvbiBjb250ZXh0ISBqYXZhLmxhbmcuTnVsbFBvaW50ZXJFeGNlcHRpb246IDwvbnM6bWVzc2FnZT4KCQkJPC9uczpldmVudD4KCQk8L25zOnJlcXVlc3Q+Cgk8L3NvYXBlbnY6Qm9keT4KPC9zb2FwZW52OkVudmVsb3BlPg==");
		request.put("identifiers", new String[] {"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host", "/soap:Envelope/soap:Body/ns:request/ns:event/ns:source"});
		request.put("dataType", "XML");
		request.put("namespaces", "soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"");

    	// firing the JSON request
    	mockMvc.perform( post(url)
							 .content(fromJson(request))
					         .contentType(contentType)
 				             .headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + url)))
			             .andExpect(status().isOk())
				         .andExpect(jsonPath("$", Matchers.hasSize(2)))
				         .andExpect(jsonPath("$[0]", is("localhost")))
				         .andExpect(jsonPath("$[1]", is("/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log")));
	}


	@Test
	public void unsuccessful_XML_invalidBase64String() throws Exception {
		// preparing the request to be sent
		// see the message in integrationTest/otherInput/xmlinput-req.xml
		Map<String, Object> request = new HashMap<>();
		request.put("message", "PHNvYXBlbnsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssY6RW52ZWxvcGUKCXhtbG5zOnNvYXBlbnY9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIgoJeG1sbnM6bnM9Imh0dHA6Ly9hY21lLmNvbS9ucyI+Cgk8c29hcGVudjpCb2R5PgoJCTxuczpyZXF1ZXN0PgoJCQk8bnM6ZXZlbnQ+CgkJCQk8bnM6aG9zdD5sb2NhbGhvc3Q8L25zOmhvc3Q+CgkJCQk8bnM6c291cmNlPi9ob21lL3Zpa2hvci93b3JrL3JlYWN0aW9uL3RtcC9yZWFjdGlvbi1lbmdpbmUtc3RhbmRhbG9uZS0xLjEvbG9ncy9yZWFjdGlvbi1lbmdpbmUubG9nPC9uczpzb3VyY2U+CgkJCQk8bnM6bWVzc2FnZT4yMDE5LTA3LTI1IDE1OjI1OjQwLjM5MyBFUlJPUiBbb3JnLnJlYWN0aW9uLmVuZ2luZS5SZWFjdGlvbkFwcGxpY2F0aW9uOjUzXSAgLSBFcnJvciBvY2N1cmVkIHdoaWxlIHN0b3BwaW5nIHRoZSBhcHBsaWNhdGlvbiBjb250ZXh0ISBqYXZhLmxhbmcuTnVsbFBvaW50ZXJFeGNlcHRpb246IDwvbnM6bWVzc2FnZT4KCQkJPC9uczpldmVudD4KCQk8L25zOnJlcXVlc3Q+Cgk8L3NvYXBlbnY6Qm9keT4KPC9zb2FwZW52OkVudmVsb3BlPg==");
		request.put("identifiers", new String[] {"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host", "/soap:Envelope/soap:Body/ns:request/ns:event/ns:source"});
		request.put("dataType", "XML");
		request.put("namespaces", "soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"");

		// firing the JSON request
		mockMvc.perform( post(url)
							.content(fromJson(request))
							.contentType(contentType)
							.headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + url)))
						.andExpect(status().isBadRequest())
						.andExpect(jsonPath("$", is("Exception occurred when Base64 decoding the message!")));
	}


	@Test
	public void unsuccessful_XML_missingNamespace() throws Exception {
		// preparing the request to be sent
		// see the message in integrationTest/otherInput/xmlinput-req.xml
		Map<String, Object> request = new HashMap<>();
		request.put("message", "PHNvYXBlbnY6RW52ZWxvcGUKCXhtbG5zOnNvYXBlbnY9Imh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3NvYXAvZW52ZWxvcGUvIgoJeG1sbnM6bnM9Imh0dHA6Ly9hY21lLmNvbS9ucyI+Cgk8c29hcGVudjpCb2R5PgoJCTxuczpyZXF1ZXN0PgoJCQk8bnM6ZXZlbnQ+CgkJCQk8bnM6aG9zdD5sb2NhbGhvc3Q8L25zOmhvc3Q+CgkJCQk8bnM6c291cmNlPi9ob21lL3Zpa2hvci93b3JrL3JlYWN0aW9uL3RtcC9yZWFjdGlvbi1lbmdpbmUtc3RhbmRhbG9uZS0xLjEvbG9ncy9yZWFjdGlvbi1lbmdpbmUubG9nPC9uczpzb3VyY2U+CgkJCQk8bnM6bWVzc2FnZT4yMDE5LTA3LTI1IDE1OjI1OjQwLjM5MyBFUlJPUiBbb3JnLnJlYWN0aW9uLmVuZ2luZS5SZWFjdGlvbkFwcGxpY2F0aW9uOjUzXSAgLSBFcnJvciBvY2N1cmVkIHdoaWxlIHN0b3BwaW5nIHRoZSBhcHBsaWNhdGlvbiBjb250ZXh0ISBqYXZhLmxhbmcuTnVsbFBvaW50ZXJFeGNlcHRpb246IDwvbnM6bWVzc2FnZT4KCQkJPC9uczpldmVudD4KCQk8L25zOnJlcXVlc3Q+Cgk8L3NvYXBlbnY6Qm9keT4KPC9zb2FwZW52OkVudmVsb3BlPg==");
		request.put("identifiers", new String[] {"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host", "/soap:Envelope/soap:Body/ns:request/ns:event/ns:source"});
		request.put("dataType", "XML");
		request.put("namespaces", "soap=\"http://schemas.xmlsoap.org/soap/eeeeeeeeenvelope/\"");

		// firing the JSON request
		mockMvc.perform( post(url)
							.content(fromJson(request))
							.contentType(contentType)
							.headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + url)))
						.andExpect(jsonPath("$", Matchers.hasSize(2)))
						.andExpect(jsonPath("$[0]", is("com.sun.org.apache.xpath.internal.domapi.XPathStylesheetDOM3Exception: Prefix must resolve to a namespace: ns")))
						.andExpect(jsonPath("$[1]", is("com.sun.org.apache.xpath.internal.domapi.XPathStylesheetDOM3Exception: Prefix must resolve to a namespace: ns")));
	}


	@Test
	public void successful_JSON() throws Exception {
		// preparing the request to be sent
		// see the message in integrationTest/otherInput/xmlinput-req.xml
		Map<String, Object> request = new HashMap<>();
		request.put("message", "eyAgCiAgICJyZXN1bHQiOnsgIAogICAgICAidGltZXN0YXJ0cG9zIjoiMCIsCiAgICAgICJ0aW1lZW5kcG9zIjoiMjQiLAogICAgICAiX2t2IjoiMSIsCiAgICAgICJkYXRlX3pvbmUiOiJsb2NhbCIsCiAgICAgICJsaW5lY291bnQiOiIiLAogICAgICAiX3NlcmlhbCI6IjEiLAogICAgICAiZXZlbnR0eXBlIjoiIiwKICAgICAgImRhdGVfeWVhciI6IjIwMTkiLAogICAgICAiX2V2ZW50dHlwZV9jb2xvciI6IiIsCiAgICAgICJfcmF3IjoiMjAxOS0wNy0yNSAxNToyNTo0MC4zOTMgRVJST1IgW29yZy5yZWFjdGlvbi5lbmdpbmUuUmVhY3Rpb25BcHBsaWNhdGlvbjo1M10gIC0gRXJyb3Igb2NjdXJlZCB3aGlsZSBzdG9wcGluZyB0aGUgYXBwbGljYXRpb24gY29udGV4dCEgamF2YS5sYW5nLk51bGxQb2ludGVyRXhjZXB0aW9uOiAiLAogICAgICAiX3NpIjpbICAKICAgICAgICAgImxvY2FsaG9zdC5sb2NhbGRvbWFpbiIsCiAgICAgICAgICJtYWluIgogICAgICBdLAogICAgICAiX3RpbWUiOiIxNTY0MDY0NzQwLjM5MyIsCiAgICAgICJfY29uZnN0ciI6InNvdXJjZTo6L2hvbWUvdmlraG9yL3dvcmsvcmVhY3Rpb24vdG1wL3JlYWN0aW9uLWVuZ2luZS1zdGFuZGFsb25lLTEuMS9sb2dzL3JlYWN0aW9uLWVuZ2luZS5sb2d8aG9zdDo6bG9jYWxob3N0LmxvY2FsZG9tYWlufHJlYWN0aW9uLWVuZ2luZSIsCiAgICAgICJfc291cmNldHlwZSI6InJlYWN0aW9uLWVuZ2luZSIsCiAgICAgICJkYXRlX21vbnRoIjoianVseSIsCiAgICAgICJob3N0IjoibG9jYWxob3N0IiwKICAgICAgIl9pbmRleHRpbWUiOiIxNTY0MDY0NzQzIiwKICAgICAgImRhdGVfbWRheSI6IjI1IiwKICAgICAgInNvdXJjZXR5cGUiOiJyZWFjdGlvbi1lbmdpbmUiLAogICAgICAic3BsdW5rX3NlcnZlciI6ImxvY2FsaG9zdC5sb2NhbGRvbWFpbiIsCiAgICAgICJpbmRleCI6Im1haW4iLAogICAgICAicHVuY3QiOiItLV86Oi5fX1suLi46XV9fLV9fX19fX18hXy4uOl90Xy4uLi4oLjopdF8uLi4uJCQoLiIsCiAgICAgICJfc3Vic2Vjb25kIjoiLjM5MyIsCiAgICAgICJkYXRlX3NlY29uZCI6IjQwIiwKICAgICAgImRhdGVfd2RheSI6InRodXJzZGF5IiwKICAgICAgImRhdGVfbWludXRlIjoiMjUiLAogICAgICAiZGF0ZV9ob3VyIjoiMTUiLAogICAgICAic291cmNlIjoiL2hvbWUvdmlraG9yL3dvcmsvcmVhY3Rpb24vdG1wL3JlYWN0aW9uLWVuZ2luZS1zdGFuZGFsb25lLTEuMS9sb2dzL3JlYWN0aW9uLWVuZ2luZS5sb2ciCiAgIH0sCiAgICJyZXN1bHRzX2xpbmsiOiJodHRwOi8vbG9jYWxob3N0LmxvY2FsZG9tYWluOjgwMDAvYXBwL3JlYWN0aW9uLWVuZ2luZS1zdGFuZGFsb25lL3NlYXJjaD9xPSU3Q2xvYWRqb2IlMjBydF9zY2hlZHVsZXJfX2RhdGFhZG1pbl9jbVZoWTNScGIyNHRaVzVuYVc1bExYTjBZVzVrWVd4dmJtVV9fUk1ENTdkZmQyOGY3YTdkNzc0NGVfYXRfMTU2NDA2NDY5OV8zLjAlMjAlN0MlMjBoZWFkJTIwMiUyMCU3QyUyMHRhaWwlMjAxJmVhcmxpZXN0PTAmbGF0ZXN0PW5vdyIsCiAgICJhcHAiOiJyZWFjdGlvbi1lbmdpbmUtc3RhbmRhbG9uZSIsCiAgICJzZWFyY2hfbmFtZSI6IkFsZXJ0IC0gTnVsbFBvaW50ZXJFeGNlcHRpb24iLAogICAic2lkIjoicnRfc2NoZWR1bGVyX19kYXRhYWRtaW5fY21WaFkzUnBiMjR0Wlc1bmFXNWxMWE4wWVc1a1lXeHZibVVfX1JNRDU3ZGZkMjhmN2E3ZDc3NDRlX2F0XzE1NjQwNjQ2OTlfMy4wIiwKICAgIm93bmVyIjoiZGF0YWFkbWluIgp9");
		request.put("identifiers", new String[] {"$.result.host", "$.result.source"});
		request.put("dataType", "JSON");
		request.put("namespaces", "soapssssssss");

		// firing the JSON request
		mockMvc.perform( post(url)
				.content(fromJson(request))
				.contentType(contentType)
				.headers(buildHttpHeaders(HttpMethod.POST, request, URL_PREFIX + url)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", Matchers.hasSize(2)))
				.andExpect(jsonPath("$[0]", is("localhost")))
				.andExpect(jsonPath("$[1]", is("/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log")));
	}
}