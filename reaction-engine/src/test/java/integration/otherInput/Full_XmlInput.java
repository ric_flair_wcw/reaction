package integration.otherInput;

import com.fasterxml.jackson.core.type.TypeReference;
import com.jcraft.jsch.JSch;
import integration.reactionWorker.ParentIntegrationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.domain.Event;
import org.reaction.engine.WebInitializer;
import org.reaction.engine.persistence.service.TaskService;
import org.reaction.engine.persistence.service.impl.RepositoryTaskService;
import org.reaction.engine.scheduling.Scheduler;
import org.reaction.engine.scheduling.impl.ThreadPoolScheduler;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.reaction.engine.service.impl.DefaultPlaceholderVariableServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
@ContextConfiguration(classes = {
		Full_XmlInput.TestConfig.class
})
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = WebInitializer.class)
@AutoConfigureMockMvc
@SqlGroup({
    @Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = {"classpath:sql/before-test-admin.sql", "classpath:sql/before-test-xmlinput.sql"}),
    @Sql(executionPhase = ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:sql/after-test.sql")
})
public class Full_XmlInput extends ParentIntegrationTest {

	
	@Autowired
	private EventMachineService mockedEventMachineService;
	
	
	@Test
	// not checking the EventMachineService as it is already checked in integration.reactionWorker
	public void successful() throws Exception {
		// preparing the request to be sent
		String url = "/notification/event";
    	String errorMessage = "2019-07-25 15:25:40.393 ERROR [org.reaction.engine.ReactionApplication:53]  - Error occured while stopping the application context! java.lang.NullPointerException: ";
		
		String request = readFile("integrationTest/otherInput/xmlinput-req.xml");
		
    	// firing the JSON request
    	mockMvc.perform( post(url + "/acmemonitor")
							 .content(request)
							 .servletPath(url + "/acmemonitor")
					         .contentType(contentType))
			             .andExpect(status().isCreated());
    	
    	// checking the HTTP code and if the EventMachineService is called
    	ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
    	verify(mockedEventMachineService, times(1)).processEvent(captorEvent.capture());
    	assertEquals(3, captorEvent.getValue().getEventSourceId().intValue());
    	assertEquals(EventInitiatorEnum.BY_EXTERNAL_SOURCE, captorEvent.getValue().getInitiatedBy());
    	assertNotNull(captorEvent.getValue().getIdentifier());
		assertNull(captorEvent.getValue().getMessage());
		Map<String,String> identifiersPayloadValues = objectMapper.readValue(captorEvent.getValue().getPayloadValues(), new TypeReference<Map<String,String>>() {});
        assertEquals(3, identifiersPayloadValues.size());
        assertEquals(errorMessage, identifiersPayloadValues.get("message"));
        assertEquals("localhost", identifiersPayloadValues.get("host"));
        assertEquals("/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log", identifiersPayloadValues.get("source"));
    	assertEquals(1, captorEvent.getValue().getErrorDetector().getId().intValue());
    	assertEquals(1, captorEvent.getValue().getExecutionFlow().getId().intValue());
	}


	@Configuration
	@ComponentScan(basePackages = {
			"org.reaction.engine.persistence.repository",
			"org.reaction.engine.persistence.service.impl",
			"org.reaction.engine.controller",
			"org.reaction.engine.persistence.service",
			"org.reaction.engine.persistence.converter",
			"org.reaction.engine.service",
			"org.reaction.engine.errors",
			"org.reaction.engine.notification",
			"org.reaction.engine.notification.message.converter",
			"org.reaction.engine.scheduling.utils"
	})
	public static class TestConfig {

		@Primary
		@Bean
		public JSch jSch() {
			return mock(JSch.class);
		}

		@Bean
		public TaskService taskService() {
			return new RepositoryTaskService();
		}

		@Bean
		public Scheduler scheduler() {
			return new ThreadPoolScheduler();
		}
		@Bean
		public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
			return new ThreadPoolTaskScheduler();
		}

		@Bean
		public PlaceholderVariableService placeholderVariableService() {
			return new DefaultPlaceholderVariableServiceImpl();
		}

		@Bean
		public JavaMailSender javaMailSender() {
			return mock(JavaMailSender.class);
		}

		@Primary
		@Bean
		public EventMachineService eventMachineService() {
			return Mockito.mock(EventMachineService.class);
		}

	}
}
