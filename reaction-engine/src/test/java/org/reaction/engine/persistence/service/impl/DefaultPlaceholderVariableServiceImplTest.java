package org.reaction.engine.persistence.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.reaction.common.contants.TaskEnum;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.TaskEntity;
import org.reaction.engine.persistence.repository.EventLifeEntityRepository;
import org.reaction.engine.persistence.repository.TaskEntityRepository;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.service.impl.DefaultPlaceholderVariableServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.reaction.engine.service.impl.DefaultPlaceholderVariableServiceImpl.PLACEHOLDER_NOT_FOUND;

@RunWith(SpringRunner.class)
public class DefaultPlaceholderVariableServiceImplTest {

    @Autowired
    private DefaultPlaceholderVariableServiceImpl uut;
    @Autowired
    private EventLifeEntityRepository mockedEventLifeEntityRepository;
    @Autowired
    private TaskEntityRepository taskEntityRepository;
    @Autowired
    private EventLifeService eventLifeService;

    @Test
    public void testReplace_successful() {
        long eventLifeId = 78373;

        TaskEntity t4 = TaskEntity.builder().internalTask(TaskEnum.LOOP)
                                            .loopValueList1("ACME2,ACME32,ACME43")
                                            .loopSeparator1(",")
                                            .loopPlaceholderVar1("server")
                                            .loopValueList2("ACMEserver1,ACMEserver2,ACMEserver3")
                                            .loopSeparator2(",")
                                            .loopPlaceholderVar2("host")
                                            .executionFlowEntity(ExecutionFlowEntity.builder().build())
                                            .id(0l).build();
        TaskEntity t3 = TaskEntity.builder().internalTask(TaskEnum.LOOP)
                                            .loopValueList1("almafa,kortefa,szilvafa")
                                            .loopSeparator1(",")
                                            .loopPlaceholderVar1("fak")
                                            .primaryTaskEntity(t4)
                                            .id(1l).build();
        TaskEntity t2 = TaskEntity.builder().internalTask(TaskEnum.COMMAND_USING_SSH)
                                            .host("##{server}##")
                                            .primaryTaskEntity(t3)
                                            .id(2l).build();
        EventEntity eventEntity = EventEntity.builder().id(0l).build();
        EventLifeEntity ele1 = EventLifeEntity.builder().loopIndex(1)
                                                        .taskEntity(t2)
                                                        .eventEntity(eventEntity).build();
        when(mockedEventLifeEntityRepository.getOne(eventLifeId)).thenReturn(ele1);
        when(taskEntityRepository.getOne(0l)).thenReturn(t4);
        when(taskEntityRepository.getOne(1l)).thenReturn(t3);
        when(taskEntityRepository.getOne(2l)).thenReturn(t2);
        when(eventLifeService.getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(0l, 0l)).thenReturn(1);
        when(eventLifeService.getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(0l, 1l)).thenReturn(2);

        String replaced = uut.replace("##{server}##.##{host}##-##{fak}##", eventLifeId);

        assertEquals("ACME32.ACMEserver2-szilvafa", replaced);
    }

    @Test
    public void testReplace_unsuccessful_noPlaceholderVar() {
        long eventLifeId = 78373;

        TaskEntity t4 = TaskEntity.builder().internalTask(TaskEnum.LOOP)
                .loopValueList1("ACME2,ACME32,ACME43")
                .loopSeparator1(",")
                .loopPlaceholderVar1("server")
                .id(0l)
                .executionFlowEntity(ExecutionFlowEntity.builder().build()).build();
        TaskEntity t3 = TaskEntity.builder().internalTask(TaskEnum.LOOP)
                .loopValueList1("almafa,kortefa,szilvafa")
                .loopSeparator1(",")
                .loopPlaceholderVar1("fak")
                .id(1l)
                .primaryTaskEntity(t4).build();
        TaskEntity t2 = TaskEntity.builder().internalTask(TaskEnum.COMMAND_USING_SSH)
                .host("##{server}##")
                .id(2l)
                .primaryTaskEntity(t3).build();
        EventEntity eventEntity = EventEntity.builder().id(0l).build();
        EventLifeEntity ele1 = EventLifeEntity.builder().loopIndex(1)
                                                        .eventEntity(eventEntity)
                                                        .taskEntity(t2).build();
        when(mockedEventLifeEntityRepository.getOne(eventLifeId)).thenReturn(ele1);
        when(taskEntityRepository.getOne(0l)).thenReturn(t4);
        when(taskEntityRepository.getOne(1l)).thenReturn(t3);
        when(taskEntityRepository.getOne(2l)).thenReturn(t2);
        when(eventLifeService.getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(0l, 0l)).thenReturn(1);
        when(eventLifeService.getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(0l, 1l)).thenReturn(2);

        String replaced = uut.replace("##{host}##", eventLifeId);

        assertEquals(PLACEHOLDER_NOT_FOUND, replaced);
    }

    @Configuration
    @ComponentScan(basePackages = {
            "org.reaction.engine.persistence.converter"
    })
    static class TestConfig {

        @Bean
        public DefaultPlaceholderVariableServiceImpl placeholderVariableServiceImpl() {
            return new DefaultPlaceholderVariableServiceImpl();
        }

        @Bean
        public EventLifeEntityRepository eventLifeEntityRepository() {
            return mock(EventLifeEntityRepository.class);
        }

        @Bean
        public TaskEntityRepository taskEntityRepository() {
            return mock(TaskEntityRepository.class);
        }

        @Bean
        public EventLifeService eventLifeService() {
            return mock(EventLifeService.class);
        }

    }

}