package org.reaction.engine.persistence.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.reaction.common.contants.DataTypeEnum;
import org.reaction.common.domain.*;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.converter.*;
import org.reaction.engine.persistence.model.ErrorDetectorEntity;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.reaction.engine.persistence.model.EventSourceTypeEntity;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.repository.ErrorDetectorEntityRepository;
import org.reaction.engine.persistence.repository.EventSourceEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {RepositoryErrorDetectorService.class, RepositoryErrorDetectorServiceTest.TestConfig.class})
public class RepositoryErrorDetectorServiceTest {


    @Autowired
    private RepositoryErrorDetectorService uut;
    @MockBean
    private EventSourceEntityRepository eventSourceEntityRepository;
    @MockBean
    private ErrorDetectorEntityRepository errorDetectorEntityRepository;
    @Autowired
    private EventSourceTypeToEntityConverter<EventSourceType, EventSourceTypeEntity> eventSourceTypeToEntityConverter;

    private String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"$.result.host\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                        "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"$.result.source\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                        "{\"name\": \"app_name\",\"desc\": \"App name\",\"path\": \"app_name\", \"source\":\"QUERY_PARAMETER\",\"output\":\"EVENT_SOURCE\"}," +
                                        "{\"name\": \"message\",\"desc\":\"Message\",\"path\":\"$.result._raw\",\"source\":\"PAYLOAD\",\"output\":\"ERROR_DETECTOR\"}," +
                                        "{\"name\": \"error_type\",\"desc\":\"Error type\",\"path\":\"error_type\",\"source\":\"QUERY_PARAMETER\",\"output\":\"ERROR_DETECTOR\"}]";
    private EventSourceType splunkEST = EventSourceType.builder().code("splunk8")
                                                                 .name("Splunk 8")
                                                                 .pathToIdentifiers(pathToIdentifiers)
                                                                 .dataType(DataTypeEnum.JSON).build();
    private EventSourceTypeEntity nagiosESTE = EventSourceTypeEntity.builder().code("nagios")
                                                                              .name("Nagios")
                                                                              .pathToIdentifiers("[{\"name\": \"location\",\"desc\": \"Location\",\"path\": \"$.meta.location\", \"source\":\"PAYLOAD\",\"output\":\"ERROR_DETECTOR\"}]")
                                                                              .dataType(DataTypeEnum.JSON).build();

    @Test
    public void whenMoreErrorDetectorsMatchButOnDifferentEventSourcesThenSelectTheLowestLevel() throws ReactionConfigurationException {
        Event event = Event.builder().eventSourceId(0l).build();
        boolean isFromWorker = false;
        Map<String, String> payloadValues = new HashMap<>();
        payloadValues.put("message", "2019-07-25 15:25:40.393 ERROR [org.reaction.engine.ReactionApplication:53]  - Error occurred while stopping the application context! java.lang.NullPointerException:");
        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("error_type", "FATAL error");
        EventSourceTypeEntity splunkESTE = eventSourceTypeToEntityConverter.convert(splunkEST);

        EventSourceEntity meliusESE = EventSourceEntity.builder().id(2l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServersESE = EventSourceEntity.builder().parent(meliusESE).id(1l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServer0ESE = EventSourceEntity.builder().parent(soaServersESE).id(0l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServer1ESE = EventSourceEntity.builder().parent(soaServersESE).id(4l).eventSourceTypeEntity(nagiosESTE).build();
        when(eventSourceEntityRepository.findOne(eq(0l))).thenReturn(soaServer0ESE);
        when(eventSourceEntityRepository.findOne(eq(1l))).thenReturn(soaServersESE);
        when(eventSourceEntityRepository.findOne(eq(2l))).thenReturn(meliusESE);

        List<ErrorDetectorEntity> errorDetectorEntities = Arrays.asList(
                ErrorDetectorEntity.builder().eventSourceEntity(meliusESE)
                                             .id(0l)
                                             .identifiers("{\"message-PAYLOAD\": \".*Exception.*\", " +
                                                           "\"location-PAYLOAD\": \".*ACME.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*FATAL.*\"}").build(),
                ErrorDetectorEntity.builder().eventSourceEntity(soaServersESE)
                                             .id(1l)
                                             .identifiers("{\"message-PAYLOAD\": \".*java.lang.NullPointerException.*\", " +
                                                          "\"location-PAYLOAD\": \".*ACME.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*INFO.*\"}").build(),
                ErrorDetectorEntity.builder().eventSourceEntity(soaServer0ESE)
                                             .id(2l)
                                             .identifiers("{\"message-PAYLOAD\": \".*java.lang.NullPointerException.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*FATAL.*\"}").build()
        );
        when(errorDetectorEntityRepository.findByEventSourceIds(anyList())).thenReturn(errorDetectorEntities);

        ErrorDetector errorDetector = uut.search(event, isFromWorker, payloadValues, queryParameters, splunkEST);

        assertNotNull(errorDetector);
        assertEquals(2l, errorDetector.getId().longValue());

        ArgumentCaptor<List> captorFindByEventSourceIds = ArgumentCaptor.forClass(List.class);
        verify(errorDetectorEntityRepository).findByEventSourceIds(captorFindByEventSourceIds.capture());
        assertEquals(Arrays.asList(0l, 1l, 2l), captorFindByEventSourceIds.getValue());
    }


    @Test
    public void whenMoreErrorDetectorsMatchAndOnTheSameEventSourcesThenExceptionIsThrown() throws ReactionConfigurationException {
        Event event = Event.builder().eventSourceId(0l).build();
        boolean isFromWorker = false;
        Map<String, String> payloadValues = new HashMap<>();
        payloadValues.put("message", "2019-07-25 15:25:40.393 ERROR [org.reaction.engine.ReactionApplication:53]  - Error occurred while stopping the application context! java.lang.NullPointerException:");
        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("error_type", "FATAL error");
        EventSourceTypeEntity splunkESTE = eventSourceTypeToEntityConverter.convert(splunkEST);

        EventSourceEntity meliusESE = EventSourceEntity.builder().id(2l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServersESE = EventSourceEntity.builder().parent(meliusESE).id(1l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServer0ESE = EventSourceEntity.builder().parent(soaServersESE).id(0l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServer1ESE = EventSourceEntity.builder().parent(soaServersESE).id(4l).eventSourceTypeEntity(nagiosESTE).build();
        when(eventSourceEntityRepository.findOne(eq(0l))).thenReturn(soaServer0ESE);
        when(eventSourceEntityRepository.findOne(eq(1l))).thenReturn(soaServersESE);
        when(eventSourceEntityRepository.findOne(eq(2l))).thenReturn(meliusESE);

        List<ErrorDetectorEntity> errorDetectorEntities = Arrays.asList(
                ErrorDetectorEntity.builder().eventSourceEntity(meliusESE)
                                             .id(0l)
                                             .identifiers("{\"message-PAYLOAD\": \".*Exception.*\", " +
                                                           "\"location-PAYLOAD\": \".*ACME.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*FATAL.*\"}").build(),
                ErrorDetectorEntity.builder().eventSourceEntity(soaServer0ESE)
                                             .id(1l)
                                             .identifiers("{\"message-PAYLOAD\": \".*Error.*NullPointerException.*\", " +
                                                          "\"location-PAYLOAD\": \".*ACME.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*error.*\"}").build(),
                ErrorDetectorEntity.builder().eventSourceEntity(soaServer0ESE)
                                             .id(2l)
                                             .identifiers("{\"message-PAYLOAD\": \".*java.lang.NullPointerException.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*FATAL.*\"}").build()
        );
        when(errorDetectorEntityRepository.findByEventSourceIds(anyList())).thenReturn(errorDetectorEntities);

        try {
            uut.search(event, isFromWorker, payloadValues, queryParameters, splunkEST);
            fail("ReactionRuntimeException should have been thrown!");
        } catch(ReactionRuntimeException e) {
            assertEquals("More than one error detector records can handle the incoming event so not sure which one should be used! Please check the log for details.", e.getMessage());
        }

    }


        @Test
    public void whenNoErrorDetectorsMatchesThenReturnNull() throws ReactionConfigurationException {
        Event event = Event.builder().eventSourceId(0l).build();
        boolean isFromWorker = false;
        Map<String, String> payloadValues = new HashMap<>();
        payloadValues.put("message", "2019-07-25 15:25:40.393 ERROR [org.reaction.engine.ReactionApplication:53]  - Error occurred while stopping the application context! java.lang.NullPointerException:");
        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("error_type", "FATAL error");
        EventSourceTypeEntity splunkESTE = eventSourceTypeToEntityConverter.convert(splunkEST);

        EventSourceEntity meliusESE = EventSourceEntity.builder().id(2l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServersESE = EventSourceEntity.builder().parent(meliusESE).id(1l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServer0ESE = EventSourceEntity.builder().parent(soaServersESE).id(0l).eventSourceTypeEntity(splunkESTE).build();
        EventSourceEntity soaServer1ESE = EventSourceEntity.builder().parent(soaServersESE).id(4l).eventSourceTypeEntity(nagiosESTE).build();
        when(eventSourceEntityRepository.findOne(eq(0l))).thenReturn(soaServer0ESE);
        when(eventSourceEntityRepository.findOne(eq(1l))).thenReturn(soaServersESE);
        when(eventSourceEntityRepository.findOne(eq(2l))).thenReturn(meliusESE);

        List<ErrorDetectorEntity> errorDetectorEntities = Arrays.asList(
                ErrorDetectorEntity.builder().eventSourceEntity(meliusESE)
                                             .id(0l)
                                             .identifiers("{\"message-PAYLOAD\": \".*Exceptionnnnnnnnnnnnnnn.*\", " +
                                                           "\"location-PAYLOAD\": \".*ACME.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*FATAL.*\"}").build(),
                ErrorDetectorEntity.builder().eventSourceEntity(soaServersESE)
                                             .id(1l)
                                             .identifiers("{\"message-PAYLOAD\": \".*java.lang.NullPointerExceptionnnnnnnnnnnn.*\", " +
                                                          "\"location-PAYLOAD\": \".*ACME.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*INFO.*\"}").build(),
                ErrorDetectorEntity.builder().eventSourceEntity(soaServer0ESE)
                                             .id(2l)
                                             .identifiers("{\"message-PAYLOAD\": \".*java.lang.NullPointerExceptionnnnnnnnnnnn.*\", " +
                                                          "\"error_type-QUERY_PARAMETER\": \".*FATAL.*\"}").build()
        );
        when(errorDetectorEntityRepository.findByEventSourceIds(anyList())).thenReturn(errorDetectorEntities);

        ErrorDetector errorDetector = uut.search(event, isFromWorker, payloadValues, queryParameters, splunkEST);

        assertNull(errorDetector);

        ArgumentCaptor<List> captorFindByEventSourceIds = ArgumentCaptor.forClass(List.class);
        verify(errorDetectorEntityRepository).findByEventSourceIds(captorFindByEventSourceIds.capture());
        assertEquals(Arrays.asList(0l, 1l, 2l), captorFindByEventSourceIds.getValue());
    }


    @After
    public void cleanUp() {
        Mockito.reset(eventSourceEntityRepository, errorDetectorEntityRepository);
    }

    @Configuration
    static class TestConfig {

        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }

        @Bean
        public ErrorDetectorToDomainConverter<ErrorDetectorEntity, ErrorDetector> errorDetectorToDomainConverter() {
            return new ErrorDetectorToDomainConverter<>();
        }

        @Bean
        public ExecutionFlowToDomainConverter<ExecutionFlowEntity, ExecutionFlow> executionFlowToDomainConverter() {
            return new ExecutionFlowToDomainConverter<>();
        }

        @Bean
        public EventSourceToDomainConverter<EventSourceEntity, EventSource> eventSourceToDomainConverter() {
            return new EventSourceToDomainConverter<>();
        }

        @Bean
        public EventSourceTypeToDomainConverter<EventSourceTypeEntity, EventSourceType> eventSourceTypeToDomainConverter() {
            return new EventSourceTypeToDomainConverter<>();
        }

        @Bean
        public EventSourceTypeToEntityConverter<EventSourceType, EventSourceTypeEntity> eventSourceTypeToEntityConverter() {
            return new EventSourceTypeToEntityConverter<>();
        }
    }

}
