package org.reaction.engine.persistence.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.reaction.common.contants.TaskEnum;
import org.reaction.engine.persistence.converter.*;
import org.reaction.engine.persistence.model.CommandsToBeExecutedEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.reaction.engine.persistence.model.TaskEntity;
import org.reaction.engine.persistence.repository.CommandsToBeExecutedEntityRepository;
import org.reaction.engine.persistence.repository.EventLifeEntityRepository;
import org.reaction.engine.persistence.repository.TaskEntityRepository;
import org.reaction.engine.persistence.service.TaskService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class RepositoryCommandsToBeExecutedServiceTest {

    @Autowired
    private RepositoryCommandsToBeExecutedService uut;
    @Autowired
    private CommandsToBeExecutedEntityRepository commandsToBeExecutedEntityRepository;
    @Autowired
    private PlaceholderVariableService placeholderVariableService;
    @Autowired
    private CommandsToBeExecutedToDomainConverter commandsToBeExecutedToDomainConverter;

    @Test
    public void testProcessCommandsToBeExecuted() {
        String host = "ACME2-kortefa";

        TaskEntity t4 = TaskEntity.builder().internalTask(TaskEnum.LOOP)
                                            .loopValueList1("ACME2,ACME32,ACME43")
                                            .loopSeparator1(",")
                                            .loopPlaceholderVar1("server").build();
        TaskEntity t3 = TaskEntity.builder().internalTask(TaskEnum.LOOP)
                                            .loopValueList1("kortefa,almafa,szilvafa")
                                            .loopSeparator1(",")
                                            .loopPlaceholderVar1("fak")
                                            .primaryTaskEntity(t4).build();
        TaskEntity t2 = TaskEntity.builder().internalTask(TaskEnum.COMMAND_USING_SSH)
                                            .host("##{server}##-##{fak}##")
                                            .primaryTaskEntity(t3).build();
        EventLifeEntity ele1 = EventLifeEntity.builder().loopIndex(0)
                                                        .id(343l)
                                                        .taskEntity(t2).build();
        CommandsToBeExecutedEntity c1 = CommandsToBeExecutedEntity.builder().eventLifeEntity(ele1).build();

        TaskEntity t1 = TaskEntity.builder().internalTask(TaskEnum.LOOP)
                                            .loopValueList1("almafa,kortefa,szilvafa")
                                            .loopSeparator1(",")
                                            .loopPlaceholderVar1("fak")
                                            .loopValueList2("ACME1,ACME2,ACME3")
                                            .loopSeparator2(",")
                                            .loopPlaceholderVar2("host").build();
        TaskEntity t0 = TaskEntity.builder().internalTask(TaskEnum.COMMAND_USING_SSH)
                                            .host("##{host}##-##{fak}##")
                                            .primaryTaskEntity(t1).build();
        EventLifeEntity ele0 = EventLifeEntity.builder().loopIndex(1)
                                                        .id(11l)
                                                        .taskEntity(t0).build();
        CommandsToBeExecutedEntity c0 = CommandsToBeExecutedEntity.builder().eventLifeEntity(ele0).build();
        List<CommandsToBeExecutedEntity> commandsToBeExecutedEntities = Stream.of(c0, c1).collect(Collectors.toList());

        when(commandsToBeExecutedEntityRepository.getCommandsToBeExecutedEntityByHost(host)).thenReturn(commandsToBeExecutedEntities);

        when(placeholderVariableService.replace("##{host}##-##{fak}##", 11l)).thenReturn(host);
        when(placeholderVariableService.replace("##{server}##-##{fak}##", 343l)).thenReturn(host);

        uut.processCommandsToBeExecuted(host);

        ArgumentCaptor<List> captorList = ArgumentCaptor.forClass(List.class);
        verify(commandsToBeExecutedToDomainConverter).convertToList(captorList.capture());
        assertTrue(((CommandsToBeExecutedEntity)captorList.getValue().get(0)).isExecuted());
        assertTrue(((CommandsToBeExecutedEntity)captorList.getValue().get(1)).isExecuted());
        assertEquals(c0, captorList.getValue().get(0));
        assertEquals(c1, captorList.getValue().get(1));

        verify(commandsToBeExecutedEntityRepository).save(anyList());
    }

    @Configuration
    static class TestConfig {

        @Bean
        public CommandsToBeExecutedEntityRepository commandsToBeExecutedEntityRepository() {
            return mock(CommandsToBeExecutedEntityRepository.class);
        }

        @Bean
        public CommandsToBeExecutedToEntityConverter commandsToBeExecutedToEntityConverter() {
            return mock(CommandsToBeExecutedToEntityConverter.class);
        }

        @Bean
        public CommandsToBeExecutedToDomainConverter commandsToBeExecutedToDomainConverter() {
            return mock(CommandsToBeExecutedToDomainConverter.class);
        }

        @Bean
        public EventLifeToEntityConverter eventLifeToEntityConverter() {
            return mock(EventLifeToEntityConverter.class);
        }

        @Bean
        public EventLifeToDomainConverter eventLifeToDomainConverter() {
            return mock(EventLifeToDomainConverter.class);
        }

        @Bean
        public EventToDomainConverter eventToDomainConverter() {
            return mock(EventToDomainConverter.class);
        }

        @Bean
        public ErrorDetectorToDomainConverter errorDetectorToDomainConverter() {
            return mock(ErrorDetectorToDomainConverter.class);
        }

        @Bean
        public PlaceholderVariableService placeholderVariableService() {
            return mock(PlaceholderVariableService.class);
        }

        @Bean
        public EventLifeEntityRepository eventLifeEntityRepository() {
            return mock(EventLifeEntityRepository.class);
        }

        @Bean
        public ExecutionFlowToDomainConverter executionFlowToDomainConverter() {
            return mock(ExecutionFlowToDomainConverter.class);
        }

        @Bean
        public EventSourceToDomainConverter eventSourceToDomainConverter() {
            return mock(EventSourceToDomainConverter.class);
        }

        @Bean
        public EventSourceTypeToDomainConverter eventSourceTypeToDomainConverter() {
            return mock(EventSourceTypeToDomainConverter.class);
        }

        @Bean
        public ScheduledExecutionFlowToDomainConverter scheduledExecutionFlowToDomainConverter() {
            return mock(ScheduledExecutionFlowToDomainConverter.class);
        }

        @Bean
        public TaskToDomainConverter taskToDomainConverter() {
            return mock(TaskToDomainConverter.class);
        }

        @Bean
        public TaskService taskService() {
            return mock(TaskService.class);
        }

        @Bean
        public RepositoryCommandsToBeExecutedService repositoryCommandsToBeExecutedService() {
            return new RepositoryCommandsToBeExecutedService();
        }

        @Bean
        public TaskEntityRepository taskEntityRepository() {
            return mock(TaskEntityRepository.class);
        }
    }
}
