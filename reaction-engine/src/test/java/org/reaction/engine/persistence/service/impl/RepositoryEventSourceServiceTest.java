package org.reaction.engine.persistence.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.reaction.common.contants.EventSourceHierarchyTypeEnum;
import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.domain.EventSource;
import org.reaction.common.domain.EventSourceType;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.reaction.engine.persistence.model.EventSourceTypeEntity;
import org.reaction.engine.persistence.repository.EventSourceEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(locations="classpath:applicationContext-unit-test.xml")
public class RepositoryEventSourceServiceTest {

	private RepositoryEventSourceService instance;
	
	@Autowired
	private ApplicationContext applicationContext;
	@MockBean
	private EventSourceEntityRepository mockedEventSourceEntityRepository;
	
	private EventSourceEntity meliusEventSource;
	private EventSourceEntity soaServersEventSource;
	private EventSourceEntity soaServer1EventSource;
	private EventSourceEntity soaServer2EventSource;
	private EventSourceTypeEntity reactionEventSourceTypeEntity = EventSourceTypeEntity.builder().id(0l)
                                                                                         .code("REACTION-WORKER").build();
	private EventSourceTypeEntity splunkEventSourceTypeEntity = EventSourceTypeEntity.builder().id(1l)
			                                                                                   .code("splunk8").build();
	private EventSourceType splunkEventSourceType = EventSourceType.builder().id(1l)
																			 .code("splunk8").build();


	@Before
	public void setUp() {
		instance = applicationContext.getBean(RepositoryEventSourceService.class);
	}
	
	
	@Test
	public void testGetEventSourceByHost_successful_group1() {
		String host = "ADNLT653";
		builEventSourceHier1();
		
		// creating the expectations in mocks
		when(mockedEventSourceEntityRepository.findByHost(host)).thenReturn( Collections.singletonList(soaServersEventSource) );
		
		List<EventSourceEntity> children = new ArrayList<>();
		children.add(soaServer1EventSource);
		children.add(soaServer2EventSource);
		when(mockedEventSourceEntityRepository.findByParent( any() )).thenReturn(children);
		
		// call the method to be tested
		List<EventSource> eventSources = instance.getEventSourceByHost(host );
		
		// check & verify
		assertEquals(2, eventSources.size());
		assertEquals(host, eventSources.get(0).getHost());
		assertEquals(host, eventSources.get(1).getHost());
		assertEquals(EventSourceHierarchyTypeEnum.ITEM, eventSources.get(0).getType());
		assertEquals(EventSourceHierarchyTypeEnum.ITEM, eventSources.get(1).getType());
		assertEquals(LogLevelEnum.ERROR, eventSources.get(0).getLogLevel());
		assertEquals(LogLevelEnum.ERROR, eventSources.get(1).getLogLevel());
		assertEquals("/usr/weblogic/soa_server1/logs/soa1.log", eventSources.get(0).getLogPath());
		assertEquals("/usr/weblogic/soa_server1/logs/soa2.log", eventSources.get(1).getLogPath());
		assertEquals("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~COMPONENT]] ([~UNKNOWN])", eventSources.get(0).getLogHeaderPattern());
		assertEquals("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~COMPONENT]] ([~UNKNOWN])", eventSources.get(1).getLogHeaderPattern());
		assertTrue(eventSources.get(0).getLogHeaderPatternEnabled());
		assertTrue(eventSources.get(1).getLogHeaderPatternEnabled());
		assertEquals("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~COMPONENT]] ([~UNKNOWN])", eventSources.get(1).getLogHeaderPattern());
		
		assertEquals("{ \"Sun\": [\"00:00-24:00\"] }", eventSources.get(0).getMaintenanceWindow().getMaintenanceWindowText());
		assertEquals("{ \"Sun\": [\"00:00-24:00\"] }", eventSources.get(1).getMaintenanceWindow().getMaintenanceWindowText());
		
		verify(mockedEventSourceEntityRepository, times(1)).findByHost(host);
		verify(mockedEventSourceEntityRepository, times(1)).findByParent(any());
		verifyNoMoreInteractions(mockedEventSourceEntityRepository);
	}
	
	
	@Test
	public void testGetEventSourceByHost_successful_group2() {
		String host = "ADNLT653";
		builEventSourceHier2();
		
		// creating the expectations in mocks
		when(mockedEventSourceEntityRepository.findByHost(host)).thenReturn( Collections.singletonList(soaServersEventSource) );
		
		List<EventSourceEntity> children = new ArrayList<>();
		children.add(soaServer1EventSource);
		children.add(soaServer2EventSource);
		when(mockedEventSourceEntityRepository.findByParent( any() )).thenReturn(children);
		
		// call the method to be tested
		List<EventSource> eventSources = instance.getEventSourceByHost(host );
		
		// check & verify
		assertEquals(2, eventSources.size());
		assertEquals("ADNLT653", eventSources.get(0).getHost());
		assertEquals("ADNLT654", eventSources.get(1).getHost());
		assertEquals(EventSourceHierarchyTypeEnum.ITEM, eventSources.get(0).getType());
		assertEquals(EventSourceHierarchyTypeEnum.ITEM, eventSources.get(1).getType());
		assertEquals(LogLevelEnum.ERROR, eventSources.get(0).getLogLevel());
		assertEquals(LogLevelEnum.ERROR, eventSources.get(1).getLogLevel());
		assertEquals("/usr/weblogic/soa_server/logs/soa.log", eventSources.get(0).getLogPath());
		assertEquals("/usr/weblogic/soa_server/logs/soa.log", eventSources.get(1).getLogPath());
		assertEquals("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~COMPONENT]] ([~UNKNOWN])", eventSources.get(0).getLogHeaderPattern());
		assertTrue(eventSources.get(0).getLogHeaderPatternEnabled());
		assertFalse(eventSources.get(1).getLogHeaderPatternEnabled());
		assertEquals("{ \"Sun\": [\"00:00-24:00\"] }", eventSources.get(0).getMaintenanceWindow().getMaintenanceWindowText());
		assertEquals("{ \"Sun\": [\"00:00-24:00\"] }", eventSources.get(1).getMaintenanceWindow().getMaintenanceWindowText());
		
		verify(mockedEventSourceEntityRepository, times(1)).findByHost(host);
		verify(mockedEventSourceEntityRepository, times(1)).findByParent(any());
		verifyNoMoreInteractions(mockedEventSourceEntityRepository);
	}

	
	@Test
	public void testGetEventSourceByHost_successful_log() {
		String host = "ADNLT653";
		builEventSourceHier1();
		
		// creating the expectations in mocks
		when(mockedEventSourceEntityRepository.findByHost(host)).thenReturn( Collections.singletonList(soaServer1EventSource) );
		
		// call the method to be tested
		List<EventSource> eventSources = instance.getEventSourceByHost(host );
		
		// check & verify
		assertEquals(1, eventSources.size());
		assertEquals(host, eventSources.get(0).getHost());
		assertEquals(EventSourceHierarchyTypeEnum.ITEM, eventSources.get(0).getType());
		assertEquals(LogLevelEnum.ERROR, eventSources.get(0).getLogLevel());
		assertEquals("/usr/weblogic/soa_server1/logs/soa1.log", eventSources.get(0).getLogPath());
		assertEquals("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~COMPONENT]] ([~UNKNOWN])", eventSources.get(0).getLogHeaderPattern());
		assertEquals("{ \"Sun\": [\"00:00-24:00\"] }", eventSources.get(0).getMaintenanceWindow().getMaintenanceWindowText());

		verify(mockedEventSourceEntityRepository, times(1)).findByHost(host);
		verifyNoMoreInteractions(mockedEventSourceEntityRepository);
	}


	@Test
	public void testGetEventSourceByEventSourceType_successful() {
		buildEventSourceHier3();

		// creating the expectations in mocks
		when(mockedEventSourceEntityRepository.findByEventSourceTypeEntity(splunkEventSourceTypeEntity)).thenReturn( Collections.singletonList(soaServersEventSource) );
		when(mockedEventSourceEntityRepository.findByParent(argThat(new IsRequestWithId(soaServersEventSource.getId())))).thenReturn(Arrays.asList(new EventSourceEntity[]{soaServer1EventSource, soaServer2EventSource}));

		// call the method to be tested
		List<EventSource> eventSources = instance.getEventSourceByEventSourceType(splunkEventSourceType);

		// check & verify
		assertEquals(1, eventSources.size());
		assertEquals(soaServer2EventSource.getName(), eventSources.get(0).getName());
		assertEquals(soaServersEventSource.getIdentifiers(), eventSources.get(0).getIdentifiers());
	}


	private EventSourceEntity builEventSourceHier1() {
		meliusEventSource = EventSourceEntity.builder().id(0l)
											.name("Melius")
											.logLevel(LogLevelEnum.ERROR)
											.eventSourceTypeEntity(reactionEventSourceTypeEntity)
											.type(EventSourceHierarchyTypeEnum.GROUP).build();
		soaServersEventSource = EventSourceEntity.builder().id(1l)
												.name("SOA servers")
												.type(EventSourceHierarchyTypeEnum.GROUP)
												.host("ADNLT653")
												.logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~COMPONENT]] ([~UNKNOWN])")
												.logHeaderPatternEnabled(true)
												.maintenanceWindow("{ \"Sun\": [\"00:00-24:00\"] }")
												.parent(meliusEventSource).build();
		soaServer1EventSource = EventSourceEntity.builder().id(2l)
												.name("SOA server1")
												.type(EventSourceHierarchyTypeEnum.ITEM)
												.logPath("/usr/weblogic/soa_server1/logs/soa1.log")
												.parent(soaServersEventSource).build();
		soaServer2EventSource = EventSourceEntity.builder().id(3l)
												.name("SOA server2")
												.type(EventSourceHierarchyTypeEnum.ITEM)
												.logPath("/usr/weblogic/soa_server1/logs/soa2.log")
												.parent(soaServersEventSource).build();
		return meliusEventSource;
	}


	private EventSourceEntity builEventSourceHier2() {
		meliusEventSource = EventSourceEntity.builder().id(0l)
											.name("Melius")
											.logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~COMPONENT]] ([~UNKNOWN])")
											.logHeaderPatternEnabled(true)
											.maintenanceWindow("{ \"Sun\": [\"00:00-24:00\"] }")
											.logPath("/usr/weblogic/soa_server/logs/soa.log")
											.logLevel(LogLevelEnum.ERROR)
											.type(EventSourceHierarchyTypeEnum.GROUP).build();
		soaServersEventSource = EventSourceEntity.builder().id(1l)
												.name("SOA servers")
												.eventSourceTypeEntity(reactionEventSourceTypeEntity)
												.type(EventSourceHierarchyTypeEnum.GROUP)
												.parent(meliusEventSource).build();
		soaServer1EventSource = EventSourceEntity.builder().id(2l)
												.name("SOA server1")
												.host("ADNLT653")
												.type(EventSourceHierarchyTypeEnum.ITEM)
												.parent(soaServersEventSource).build();
		soaServer2EventSource = EventSourceEntity.builder().id(3l)
												.name("SOA server2")
												.logHeaderPatternEnabled(false)
												.host("ADNLT654")
												.type(EventSourceHierarchyTypeEnum.ITEM)
												.parent(soaServersEventSource).build();
		return meliusEventSource;
	}

	private EventSourceEntity buildEventSourceHier3() {
		meliusEventSource = EventSourceEntity.builder().id(0l)
				.name("Melius")
				.maintenanceWindow("{ \"Sun\": [\"00:00-24:00\"] }")
				.type(EventSourceHierarchyTypeEnum.GROUP).build();
		soaServersEventSource = EventSourceEntity.builder().id(1l)
				.name("SOA servers")
				.eventSourceTypeEntity(splunkEventSourceTypeEntity)
				.identifiers("[{\"name\":\"host\",\"value\":\"localhost\"},{\"name\":\"source\",\"value\":\"/home/vikhor/work/reaction/src/reaction/management_app/reaction_debug.log\"}]")
				.type(EventSourceHierarchyTypeEnum.GROUP)
				.parent(meliusEventSource).build();
		soaServer1EventSource = EventSourceEntity.builder().id(2l)
				.name("SOA server1")
				.eventSourceTypeEntity(reactionEventSourceTypeEntity)
				.type(EventSourceHierarchyTypeEnum.ITEM)
				.host("a")
				.logPath("b")
				.logHeaderPatternEnabled(false)
				.parent(soaServersEventSource).build();
		soaServer2EventSource = EventSourceEntity.builder().id(3l)
				.name("SOA server2")
				.type(EventSourceHierarchyTypeEnum.ITEM)
				.identifiers("[]")
				.parent(soaServersEventSource).build();
		return meliusEventSource;
	}

	class IsRequestWithId extends ArgumentMatcher<EventSourceEntity> {

		private final Long id;

		public IsRequestWithId(long id) {
			this.id = id;
		}

		@Override
		public boolean matches(Object arg) {
			EventSourceEntity eventSourceEntity = (EventSourceEntity)arg;
			return id == eventSourceEntity.getId();
		}
	}
}