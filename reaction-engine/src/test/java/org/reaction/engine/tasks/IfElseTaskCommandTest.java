package org.reaction.engine.tasks;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.CommandByWorkerTask;
import org.reaction.common.domain.task.InternalIfElseTask;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.expression.ExpressionInterpreter;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.*;
import org.reaction.engine.service.PlaceholderVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class IfElseTaskCommandTest {

	
	@Autowired
	private IfElseTaskCommand instance;
	
	@MockBean
	private CommandsToBeExecutedService mockedCommandsToBeExecutedService;
	@MockBean
	private EventLifeService mockedEventLifeService;
	@MockBean
	private TaskService mockedTaskService;
	@MockBean
	private EventService mockedEventService;
	@MockBean
	private CommandByWorkerTaskCommand mockedNextCommand;
	@MockBean
	private ReactionMailSender mockedMailSender;
	@MockBean
	private ExecutionFlowService mockedExecutionFlowService;
	@MockBean
	private ScheduledExecutionFlowService mockedScheduledExecutionFlowService;
	@MockBean
	private AuthQuery mockedAuthQuery;
	@MockBean
	private ErrorHandler errorHandler;
	@MockBean
	private PlaceholderVariableService mockedPlaceholderVariableService;

	
	// successful test
	//   - it will go to the true branch of the if
	//   - it compares numbers (operation: greater than)     123 > 100
	@Test
	public void test_execute_successful_numbers_toTrueInIf() throws ReactionExecutionFlowException {
		Event event = Event.builder().status(EventStatusEnum.STARTED)
									 .id(123l).build();
		EventLife eventLife = EventLife.builder().order(2)
				                                 .event(event)
				                                 .eventDate(Calendar.getInstance())
				                                 .eventStatus(EventStatusEnum.STARTED)
												 .task(instance)
				                                 .id(0l).build();
		CommandByWorkerTask firstCommand = CommandByWorkerTask.builder().build();
		firstCommand.setId(1123l);
		CommandByWorkerTaskCommand firstCommandInIf = new CommandByWorkerTaskCommand(firstCommand);
		String ifExpression = "Integer.parseInt("+ExpressionInterpreter.VARIABLE+") > 100";
		instance.getInternalIfElseTask().setIfExpression(ifExpression);
		instance.getInternalIfElseTask().setFirstCommandInIf(firstCommandInIf);
		instance.getInternalIfElseTask().setFirstCommandInElse(null);
		instance.getInternalIfElseTask().setName("if-else");
		instance.setEventLife(eventLife);
		
		// creating the expectations in mocks
		//    for mockedEventLifeService
		EventLife prevEventLife = EventLife.builder().id(0l)
                                                     .extractedValue("123")
                                                     .task(new CommandByWorkerTaskCommand(null)).build();
		when(mockedEventLifeService.getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent())).thenReturn(prevEventLife);
		EventLife savedEventLife = EventLife.builder().build();
		//       updating the current eventLife
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		//       starting the next command so a new eventLife has to be saved
		EventLife savedNewEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedNewEventLife);
		//	    for mockedNextCommand
		CommandByWorkerTask nextCommandByWorkerTask = CommandByWorkerTask.builder().build();
		nextCommandByWorkerTask.setOrder(23);
		when(mockedNextCommand.getSuperTask()).thenReturn(nextCommandByWorkerTask);
		doNothing().when(mockedNextCommand).execute(savedNewEventLife);
		//    for mockedTaskService
		when(mockedTaskService.getCommandById(1123l)).thenReturn(mockedNextCommand);
		//    for mockedEventService
		when(mockedEventService.getEvent(event.getId())).thenReturn(event);
		//    for mockedPlaceholderVariableService
		when(mockedPlaceholderVariableService.replace(ifExpression, 0l)).thenReturn(ifExpression);
		
		// call the method to be tested
		instance._execute(eventLife);
		
		// check & verify
		verify(mockedEventLifeService, times(1)).getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent());
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(2)).save(captorEventLife.capture());
		List<EventLife> capturedEventLifes = captorEventLife.getAllValues();
		//               first save
		assertEquals(EventStatusEnum.FINISHED, capturedEventLifes.get(0).getEventStatus());
		//               second save
		assertEquals(eventLife.getEvent(), capturedEventLifes.get(1).getEvent());
		assertEquals(mockedNextCommand, capturedEventLifes.get(1).getTask());
		assertEquals(EventStatusEnum.STARTED, capturedEventLifes.get(1).getEventStatus());
		assertEquals(eventLife.getOrder() + 1, capturedEventLifes.get(1).getOrder().intValue());
		assertNotNull(capturedEventLifes.get(1).getEventDate());

		verify(mockedTaskService, times(1)).getCommandById(1123l);

		verify(mockedNextCommand, times(1)).execute(savedNewEventLife);
		
		verify(mockedEventService, times(1)).getEvent(event.getId());
		
		verifyNoMoreInteractions(mockedCommandsToBeExecutedService, mockedEventLifeService, mockedTaskService, mockedEventService);
	}
	
	
	// successful test
	//   - it will go to the false branch of the if
	//   - it compares numbers (operation: greater than)
	@Test
	public void test_execute_successful_numbers_toFalseInIf() throws ReactionExecutionFlowException {
		Event event = Event.builder().status(EventStatusEnum.STARTED)
									 .id(123l).build();
		EventLife eventLife = EventLife.builder().order(2)
		                            .event(event)
		                            .eventDate(Calendar.getInstance())
		                            .eventStatus(EventStatusEnum.STARTED)
									.task(instance)
		                            .id(0l).build();
		CommandByWorkerTask firstCommand = CommandByWorkerTask.builder().build();
		firstCommand.setId(1123l);
		CommandByWorkerTaskCommand firstCommandInElse = new CommandByWorkerTaskCommand(firstCommand);
		String ifExpression = "Float.parseFloat("+ExpressionInterpreter.VARIABLE+") > 99.654";
		instance.getInternalIfElseTask().setIfExpression(ifExpression);
		instance.getInternalIfElseTask().setFirstCommandInIf(null);
		instance.getInternalIfElseTask().setFirstCommandInElse(firstCommandInElse);
		instance.getInternalIfElseTask().setName("if-else");
		instance.setEventLife(eventLife);
		
		// creating the expectations in mocks
		//    for mockedEventLifeService
		EventLife prevEventLife = EventLife.builder().id(0l)
		                                			 .extractedValue("23.3")
		                                			 .task(new CommandByWorkerTaskCommand(null)).build();
		when(mockedEventLifeService.getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent())).thenReturn(prevEventLife);
		EventLife savedEventLife = EventLife.builder().build();
		//       updating the current eventLife
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		//       starting the next command so a new eventLife has to be saved
		EventLife savedNewEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedNewEventLife);
		//	    for mockedNextCommand
		CommandByWorkerTask nextCommandByWorkerTask = CommandByWorkerTask.builder().build();
		nextCommandByWorkerTask.setOrder(23);
		when(mockedNextCommand.getSuperTask()).thenReturn(nextCommandByWorkerTask);
		doNothing().when(mockedNextCommand).execute(savedNewEventLife);
		//    for mockedTaskService
		when(mockedTaskService.getCommandById(1123l)).thenReturn(mockedNextCommand);
		//    for mockedEventService
		when(mockedEventService.getEvent(event.getId())).thenReturn(event);
		//    for mockedPlaceholderVariableService
		when(mockedPlaceholderVariableService.replace(ifExpression, 0l)).thenReturn(ifExpression);

		// call the method to be tested
		instance._execute(eventLife);
		
		// check & verify
		verify(mockedEventLifeService, times(1)).getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent());
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(2)).save(captorEventLife.capture());
		List<EventLife> capturedEventLifes = captorEventLife.getAllValues();
		//               first save
		assertEquals(EventStatusEnum.FINISHED, capturedEventLifes.get(0).getEventStatus());
		//               second save
		assertEquals(eventLife.getEvent(), capturedEventLifes.get(1).getEvent());
		assertEquals(mockedNextCommand, capturedEventLifes.get(1).getTask());
		assertEquals(EventStatusEnum.STARTED, capturedEventLifes.get(1).getEventStatus());
		assertEquals(eventLife.getOrder() + 1, capturedEventLifes.get(1).getOrder().intValue());
		assertNotNull(capturedEventLifes.get(1).getEventDate());
		
		verify(mockedTaskService, times(1)).getCommandById(1123l);
		
		verify(mockedNextCommand, times(1)).execute(savedNewEventLife);
		
		verify(mockedEventService, times(1)).getEvent(event.getId());
		
		verifyNoMoreInteractions(mockedCommandsToBeExecutedService, mockedEventLifeService, mockedTaskService, mockedEventService);
	}

	
	// successful test
	//   - it will go to the false branch of the if
	//   - it compares strings (operation: equal)
	@Test
	public void test_execute_successful_strings_toFalseInIf() throws ReactionExecutionFlowException {
		Event event = Event.builder().status(EventStatusEnum.STARTED)
									 .id(123l).build();
		EventLife eventLife = EventLife.builder().order(2)
		                            .event(event)
		                            .eventDate(Calendar.getInstance())
		                            .eventStatus(EventStatusEnum.STARTED)
									.task(instance)
									.id(0l).build();
		CommandByWorkerTask firstCommand = CommandByWorkerTask.builder().build();
		firstCommand.setId(1123l);
		CommandByWorkerTaskCommand firstCommandInIf = new CommandByWorkerTaskCommand(firstCommand);
		String ifExpression = ExpressionInterpreter.VARIABLE+" == \"kanya2\" || "+ExpressionInterpreter.VARIABLE+" == \"RicFlair\"";
		instance.getInternalIfElseTask().setIfExpression(ifExpression);
		instance.getInternalIfElseTask().setFirstCommandInIf(firstCommandInIf);
		instance.getInternalIfElseTask().setFirstCommandInElse(null);
		instance.getInternalIfElseTask().setName("if-else");
		instance.setEventLife(eventLife);
		
		// creating the expectations in mocks
		//    for mockedEventLifeService
		EventLife prevEventLife = EventLife.builder().id(0l)
		                                			 .extractedValue("RicFlair")
		                                			 .task(new CommandByWorkerTaskCommand(null)).build();
		when(mockedEventLifeService.getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent())).thenReturn(prevEventLife);
		EventLife savedEventLife = EventLife.builder().build();
		//       updating the current eventLife
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		//       starting the next command so a new eventLife has to be saved
		EventLife savedNewEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedNewEventLife);
		//	    for mockedNextCommand
		CommandByWorkerTask nextCommandByWorkerTask = CommandByWorkerTask.builder().build();
		nextCommandByWorkerTask.setOrder(23);
		when(mockedNextCommand.getSuperTask()).thenReturn(nextCommandByWorkerTask);
		doNothing().when(mockedNextCommand).execute(savedNewEventLife);
		//    for mockedTaskService
		when(mockedTaskService.getCommandById(1123l)).thenReturn(mockedNextCommand);
		//    for mockedEventService
		when(mockedEventService.getEvent(event.getId())).thenReturn(event);
		//    for mockedPlaceholderVariableService
		when(mockedPlaceholderVariableService.replace(ifExpression, 0l)).thenReturn(ifExpression);

		// call the method to be tested
		instance._execute(eventLife);
		
		// check & verify
		verify(mockedEventLifeService, times(1)).getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent());
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(2)).save(captorEventLife.capture());
		List<EventLife> capturedEventLifes = captorEventLife.getAllValues();
		//               first save
		assertEquals(EventStatusEnum.FINISHED, capturedEventLifes.get(0).getEventStatus());
		//               second save
		assertEquals(eventLife.getEvent(), capturedEventLifes.get(1).getEvent());
		assertEquals(mockedNextCommand, capturedEventLifes.get(1).getTask());
		assertEquals(EventStatusEnum.STARTED, capturedEventLifes.get(1).getEventStatus());
		assertEquals(eventLife.getOrder() + 1, capturedEventLifes.get(1).getOrder().intValue());
		assertNotNull(capturedEventLifes.get(1).getEventDate());
		
		verify(mockedTaskService, times(1)).getCommandById(1123l);
		
		verify(mockedNextCommand, times(1)).execute(savedNewEventLife);
		
		verify(mockedEventService, times(1)).getEvent(event.getId());
		
		verifyNoMoreInteractions(mockedCommandsToBeExecutedService, mockedEventLifeService, mockedTaskService, mockedEventService);
	}

	
	// unsuccessful test
	//   - the value should be a number but it is not
	@Test
	public void test_execute_unsuccessful_notLongValue() throws ReactionExecutionFlowException {
		Event event = Event.builder().build();
		EventLife eventLife = EventLife.builder().order(2)
				                                 .event(event)
				                                 .eventDate(Calendar.getInstance())
				                                 .eventStatus(EventStatusEnum.STARTED)
												.task(instance)
												.id(0l).build();
		CommandByWorkerTask firstCommand = CommandByWorkerTask.builder().build();
		firstCommand.setId(1123l);
		CommandByWorkerTaskCommand firstCommandInIf = new CommandByWorkerTaskCommand(firstCommand);
		String ifExpression = "Integer.parseInt("+ExpressionInterpreter.VARIABLE+") > 99.654";
		instance.getInternalIfElseTask().setIfExpression(ifExpression);
		instance.getInternalIfElseTask().setFirstCommandInIf(firstCommandInIf);
		instance.getInternalIfElseTask().setFirstCommandInElse(null);
		instance.getInternalIfElseTask().setName("if-else");
		instance.setEventLife(eventLife);
		
		// creating the expectations in mocks
		//    for mockedEventLifeService
		EventLife prevEventLife = EventLife.builder().id(0l)
                                                     .extractedValue("RicFlair")
                                                     .task(new CommandByWorkerTaskCommand(null)).build();
		when(mockedEventLifeService.getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent())).thenReturn(prevEventLife);
		//    for mockedPlaceholderVariableService
		when(mockedPlaceholderVariableService.replace(ifExpression, 0l)).thenReturn(ifExpression);

		// call the method to be tested
		try {
			instance._execute(eventLife);
			fail("A ReactionRuntimeException should have been thrown!");
		} catch(ReactionExecutionFlowException e) {
			assertEquals("Error occured during the evaluation of the IF-ELSE expression! >> NumberFormatException: For input string: \"RicFlair\"", e.getMessage());
		}
		
		// check & verify
		verify(mockedEventLifeService, times(1)).getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent());
		verifyNoMoreInteractions(mockedCommandsToBeExecutedService, mockedEventLifeService, mockedTaskService, mockedEventService);
	}

	
	// unsuccessful test
	//   - the condition contains the $COMMANDOUTPUT but no previous COMMAND_BY_WORKER command exists
	@Test
	public void test_execute_unsuccessful_noPrevExtCommand() throws ReactionExecutionFlowException {
		Event event = Event.builder().build();
		EventLife eventLife = EventLife.builder().order(2)
				                                 .event(event)
				                                 .eventDate(Calendar.getInstance())
				                                 .eventStatus(EventStatusEnum.STARTED)
								 				 .task(instance)
												 .id(0l).build();
		CommandByWorkerTask firstCommand = CommandByWorkerTask.builder().build();
		firstCommand.setId(1123l);
		CommandByWorkerTaskCommand firstCommandInIf = new CommandByWorkerTaskCommand(firstCommand);
		String ifExpression = "Integer.parseInt("+ExpressionInterpreter.VARIABLE+") > 99.654";
		instance.getInternalIfElseTask().setIfExpression(ifExpression);
		instance.getInternalIfElseTask().setFirstCommandInIf(firstCommandInIf);
		instance.getInternalIfElseTask().setFirstCommandInElse(null);
		instance.getInternalIfElseTask().setName("if-else");
		instance.setEventLife(eventLife);
		
		// creating the expectations in mocks
		when(mockedEventLifeService.getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent())).thenReturn(null);
		when(mockedPlaceholderVariableService.replace(ifExpression, 0l)).thenReturn(ifExpression);

		// call the method to be tested
		try {
			instance._execute(eventLife);
			fail("A ReactionExecutionFlowException should have been thrown!");
		} catch(ReactionExecutionFlowException e) {
			assertEquals("The eventLife ("+eventLife.getId()+") contains an if-else task but an COMMAND_BY_WORKER or COMMAND_USING_SSH command doesn't precede it!", e.getMessage());
		}
		
		// check & verify
		verify(mockedEventLifeService, times(1)).getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent());
		verifyNoMoreInteractions(mockedCommandsToBeExecutedService, mockedEventLifeService, mockedTaskService, mockedEventService);
	}

	
	@Configuration
    @Import(IfElseTaskCommand.class)
    static class Config {
    	@Bean
    	public InternalIfElseTask internalIfElseTask() {
    		return new InternalIfElseTask();
    	}
    	@Bean
    	public ExpressionInterpreter expressionInterpreter() {
    		return new ExpressionInterpreter();
    	}
    }

}
