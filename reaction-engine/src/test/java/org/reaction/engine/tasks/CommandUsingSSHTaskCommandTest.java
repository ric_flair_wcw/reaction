package org.reaction.engine.tasks;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.CommandUsingSSHTask;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.CommandsToBeExecutedService;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.persistence.service.TaskService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {
        CommandUsingSSHTaskCommandTest.TestConfig.class
})
@TestPropertySource(properties = {
        "reaction.security.password_encryption.key_file=classpath:password_encryption.key",
        "reaction.commands.using-ssh.default-execution-timeout=100"
})
public class CommandUsingSSHTaskCommandTest {

    @Autowired
    private ApplicationContext context;
    private CommandUsingSSHTaskCommand instance;
    @MockBean
    private EventLifeService mockedEventLifeService;
    @MockBean
    private JSch mockedJsch;
    @MockBean
    private Session mockedSession;
    @MockBean
    private ChannelExec mockedChannelExec;
    @MockBean
    private TaskService mockedTaskService;
    @MockBean
    private EventService mockedEventService;
    @MockBean
    private ErrorHandler mockedErrorHandler;
    @MockBean
    private CommandsToBeExecutedService mockedCommandsToBeExecutedService;
    @MockBean
    private ExecutorService mockedThreadPoolTaskExecutor;
    @MockBean
    private PlaceholderVariableService placeholderVariableService;


    @Before
    public void setUp() {
        Mockito.reset(mockedChannelExec, mockedSession, mockedJsch);
    }


    @Test
    public void successful() throws JSchException, IOException {
        long eventLifeId = 123;
        String command = "ls -l";
        String commandOutput =
                "drwxrwxr-x. 6 vikhor vikhor      120 Oct 15 15:17 utils\n" +
                "drwxr-xr-x. 2 vikhor vikhor        6 Jul 16  2019 Videos\n" +
                "drwxrwxr-x. 5 vikhor vikhor       51 Jun 22  2020 work\n" +
                "0";

        CommandUsingSSHTask commandUsingSSHTask = CommandUsingSSHTask.builder().command(command)
                                                                               .host("localhost:23")
                                                                               .osUser("centos")
                                                                               .osPassword("L1hkOSalusX/VR5Zr36/M789Ch4pGUWbWH7SgHunAF8=")
                                                                               .isPwdSetManually(true)
                                                                               .outputPattern(".*  (?<COMMANDOUTPUT>[0-9]+) [a-zA-Z]+")
                                                                               .build();
        instance = (CommandUsingSSHTaskCommand) context.getBean("commandUsingSSHTaskCommand", commandUsingSSHTask);
        EventLife eventLife = EventLife.builder().id(eventLifeId)
                                                 .task(instance).build();

        when(mockedJsch.getSession("centos", "localhost", 23)).thenReturn(mockedSession);
        when(mockedSession.openChannel("exec")).thenReturn(mockedChannelExec);
        when(mockedChannelExec.getInputStream()).thenReturn(new ByteArrayInputStream(commandOutput.getBytes()));
        when(mockedChannelExec.getErrStream()).thenReturn(new ByteArrayInputStream("".getBytes()));
        when(mockedChannelExec.isClosed()).thenReturn(true);

        when(placeholderVariableService.replace(commandUsingSSHTask.getHost(), eventLifeId)).thenReturn(commandUsingSSHTask.getHost());
        when(placeholderVariableService.replace(commandUsingSSHTask.getOsUser(), eventLifeId)).thenReturn(commandUsingSSHTask.getOsUser());
        when(placeholderVariableService.replace(commandUsingSSHTask.getCommand(), eventLifeId)).thenReturn(commandUsingSSHTask.getCommand());
        when(placeholderVariableService.replace(commandUsingSSHTask.getOutputPattern(), eventLifeId)).thenReturn(commandUsingSSHTask.getOutputPattern());
        when(placeholderVariableService.replace("localhost", eventLifeId)).thenReturn("localhost");

        doAnswer(new Answer() {
            private int count = 0;
            public Object answer(InvocationOnMock invocation) {
                // I cannot use ArgumentCaptor as I use the same object (eventLife) at eventLifeService.save(...) so in the getAllValues I could see only the last status of eventLife (FINISHED but not the EXECUTION_STARTED etc.)
                EventLife capturedEventLife = (EventLife) invocation.getArguments()[0];
                if (count == 0) {
                    assertEquals(EventStatusEnum.EXECUTION_STARTED, capturedEventLife.getEventStatus());
                    assertNotNull(capturedEventLife.getEventDate());
                } else if (count == 1) {
                    assertNotNull(capturedEventLife.getEventDate());
                    assertEquals(EventStatusEnum.FINISHED, capturedEventLife.getEventStatus());
                    assertEquals("2020", capturedEventLife.getExtractedValue());
                    assertTrue(capturedEventLife.getExtCommandSuccesful());
                    assertEquals(commandOutput, capturedEventLife.getOutput());
                }
                count++;
                return null;
            }
        }).when(mockedEventLifeService).save(eventLife);

        instance.setEventLife(eventLife);
        instance.internalExecute(eventLife);

        verify(mockedSession).setPassword("centos".getBytes(StandardCharsets.UTF_8));
        verify(mockedChannelExec).setCommand((command + "; echo $?").getBytes(StandardCharsets.UTF_8));
        verify(mockedEventLifeService, times(2)).save(any());
    }


    @Test
    public void ssh_auth_failed() throws JSchException, IOException {
        String command = "ls -l";
        long eventLifeId = 123;

        CommandUsingSSHTask commandUsingSSHTask = CommandUsingSSHTask.builder().command(command)
                                                                               .host("localhost:23")
                                                                               .osUser("centos")
                                                                               .osPassword("L1hkOSalusX/VR5Zr36/M789Ch4pGUWbWH7SgHunAF8=")
                                                                               .outputPattern(".*  (?<COMMANDOUTPUT>[0-9]+) [a-zA-Z]+")
                                                                               .isPwdSetManually(true)
                                                                               .build();
        instance = (CommandUsingSSHTaskCommand) context.getBean("commandUsingSSHTaskCommand", commandUsingSSHTask);
        EventLife eventLife = EventLife.builder().id(eventLifeId)
                                                 .task(instance).build();

        when(mockedJsch.getSession("centos", "localhost", 23)).thenReturn(mockedSession);
        doThrow(new JSchException("Auth fail")).when(mockedSession).connect();

        when(placeholderVariableService.replace(commandUsingSSHTask.getHost(), eventLifeId)).thenReturn(commandUsingSSHTask.getHost());
        when(placeholderVariableService.replace(commandUsingSSHTask.getOsUser(), eventLifeId)).thenReturn(commandUsingSSHTask.getOsUser());
        when(placeholderVariableService.replace(commandUsingSSHTask.getCommand(), eventLifeId)).thenReturn(commandUsingSSHTask.getCommand());
        when(placeholderVariableService.replace(commandUsingSSHTask.getOutputPattern(), eventLifeId)).thenReturn(commandUsingSSHTask.getOutputPattern());
        when(placeholderVariableService.replace("localhost", eventLifeId)).thenReturn("localhost");

        doAnswer(new Answer() {
            private int count = 0;
            public Object answer(InvocationOnMock invocation) {
                // I cannot use ArgumentCaptor as I use the same object (eventLife) at eventLifeService.save(...) so in the getAllValues I could see only the last status of eventLife (FINISHED but not the EXECUTION_STARTED etc.)
                EventLife capturedEventLife = (EventLife) invocation.getArguments()[0];
                if (count == 0) {
                    assertEquals(EventStatusEnum.EXECUTION_STARTED, capturedEventLife.getEventStatus());
                    assertNotNull(capturedEventLife.getEventDate());
                }
                count++;
                return null;
            }
        }).when(mockedEventLifeService).save(eventLife);

        instance.setEventLife(eventLife);
        instance.internalExecute(eventLife);

        verify(mockedSession).setPassword("centos".getBytes(StandardCharsets.UTF_8));
        verify(mockedEventLifeService, times(1)).save(any());
        verify(mockedErrorHandler).handleErrorInCommand(eq(eventLife), any(ReactionExecutionFlowException.class));
    }


    @Configuration
    static class TestConfig {

        @Bean
        @Scope(value = "prototype")
        public CommandUsingSSHTaskCommand commandUsingSSHTaskCommand(CommandUsingSSHTask commandUsingSSHTask) {
            return new CommandUsingSSHTaskCommand(commandUsingSSHTask);
        }

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertiesResolver() {
            return new PropertySourcesPlaceholderConfigurer();
        }
    }
}