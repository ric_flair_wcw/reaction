package org.reaction.engine.tasks;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.CommandsToBeExecuted;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.CommandByWorkerTask;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.*;
import org.reaction.engine.service.PlaceholderVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class CommandByWorkerTaskCommandTest {

	
	@Autowired
	private CommandByWorkerTaskCommand instance;
	@MockBean
	private CommandsToBeExecutedService mockedCommandsToBeExecutedService;
	@MockBean
	private EventLifeService mockedEventLifeService;
	@MockBean
	private TaskService mockedTaskService;
	@MockBean
	private EventService mockedEventService;
	@MockBean
	private ReactionMailSender mockedReactionMailSender;
	@MockBean
	private ExecutionFlowService mockedExecutionFlowService;
	@MockBean
	private ScheduledExecutionFlowService mockedScheduledExecutionFlowService;
	@MockBean
	private AuthQuery mockedAuthQuery;
	@MockBean
	private ErrorHandler errorHandler;
	@MockBean
	private PlaceholderVariableService mockedPlaceholderVariableService;

	
	@Test
	public void test_execute_successful() {
		EventLife eventLife = EventLife.builder().eventDate(Calendar.getInstance())
				                                 .task(instance)
				                                 .eventStatus(EventStatusEnum.STARTED).build();

		// creating the expectations in mocks
		//    for mockedCommandsToBeExecutedService
		CommandsToBeExecuted commandsToBeExecuted = CommandsToBeExecuted.builder().executed(false)
                                                                                  .eventLife(eventLife).build();
		doNothing().when(mockedCommandsToBeExecutedService).save(commandsToBeExecuted);
		//    for mockedEventLifeService
		EventLife saveEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(eventLife)).thenReturn(saveEventLife);
		
		// call the method to be tested
		instance._execute(eventLife);
		
		// check & verify
		verify(mockedCommandsToBeExecutedService, times(1)).save(commandsToBeExecuted);
		verify(mockedEventLifeService, times(1)).save(eventLife);
		assertEquals(EventStatusEnum.WAITING_FOR_EXECUTION, eventLife.getEventStatus());
		
		verifyNoMoreInteractions(mockedCommandsToBeExecutedService, mockedEventLifeService, mockedTaskService, mockedEventService);
	}
	
	
    @Configuration
    @Import(CommandByWorkerTaskCommand.class)
    static class Config {
    	@Bean
    	public CommandByWorkerTask commandByWorkerTask() {
    		CommandByWorkerTask commandByWorkerTask = CommandByWorkerTask.builder().command("ls -l")
                                                                                   .host("localhost").build();
    		return commandByWorkerTask;
    	}
    }


}