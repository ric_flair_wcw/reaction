package org.reaction.engine.scheduling;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.persistence.service.ScheduledExecutionFlowService;
import org.reaction.engine.service.ExecutionFlowMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ScheduledRunnableTest {

	
	@Autowired
	private ApplicationContext applicationContext;
	private ScheduledRunnable instance;
	@MockBean
	private ScheduledExecutionFlowService mockedScheduledExecutionFlowService;
	@MockBean
	private EventService mockedEventService;
	@MockBean
	private EventLifeService mockedEventLifeService;
	@MockBean
	private ExecutionFlowMachineService mockedExecutionFlowMachineService;
	@MockBean
	private ErrorHandler mockedErrorHandler;
	@MockBean
	private Scheduler mockedScheduler;

	
	@Before
	public void setUp() {
		instance = applicationContext.getBean(ScheduledRunnable.class);
	}
	
	
	@Test
	public void testRun_successful_byScheduler() throws ReactionExecutionFlowException {
		// cron expression : every Monday at 22:00
		ScheduledExecutionFlow scheduledExecutionFlow = ScheduledExecutionFlow.builder().id(0l)
																						.cronExpression("0 0 22 ? * MON")
																						.scheduledAlready(true).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		Event event = Event.builder().scheduledExecutionFlow(scheduledExecutionFlow)
                                     .executionFlow(executionFlow)
                                     .id(45l)
                                     .startDate(new GregorianCalendar(2097, Calendar.JANUARY, 30, 22, 10, 0))
                                     .initiatedBy(EventInitiatorEnum.BY_SCHEDULER).build();
		instance.setEvent(event);
		instance.setDate( new GregorianCalendar(2097, Calendar.JANUARY, 30, 22, 10, 0).getTime() );
		
		Event savedNextEvent = Event.builder().build();
		
		// creating the expectations in mocks
		//    for mockedEventService
		when(mockedEventService.getEvent(event.getId())).thenReturn(event);
		when(mockedEventService.isScheduledEventExecuted(instance.event)).thenReturn(false);
		when(mockedEventService.save(any(Event.class))).thenReturn(savedNextEvent);
		//    for mockedScheduledExecutionFlowService
		when(mockedScheduledExecutionFlowService.getScheduledExecutionFlow(event.getScheduledExecutionFlow().getId())).thenReturn(scheduledExecutionFlow);
		//    for mockedExecutionFlowMachineService
		when(mockedExecutionFlowMachineService.startIt(instance.event)).thenReturn(null);
		
		// call the method to be tested
		instance.run();
		
		// check & verify
		verify(mockedEventService,times(1)).getEvent(instance.event.getId());

		verify(mockedEventService,times(1)).isScheduledEventExecuted(instance.event);
		
		ArgumentCaptor<Event> captorScheduledEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(1)).save(captorScheduledEvent.capture());
		//    second save
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		assertEquals("2097-02-04T22:00:00.000", dateFormat.format(captorScheduledEvent.getValue().getStartDate().getTime()));
		assertEquals(EventInitiatorEnum.BY_SCHEDULER, captorScheduledEvent.getValue().getInitiatedBy());
		assertEquals(EventStatusEnum.SCHEDULED, captorScheduledEvent.getValue().getStatus());
		assertEquals(instance.event.getExecutionFlow(), captorScheduledEvent.getValue().getExecutionFlow());
		assertEquals(instance.event.getScheduledExecutionFlow(), captorScheduledEvent.getValue().getScheduledExecutionFlow());
		
		verify(mockedExecutionFlowMachineService, times(1)).startIt(instance.event);
		
		verify(mockedScheduler, times(1)).schedule(any());
		
		verify(mockedScheduledExecutionFlowService, times(1)).getScheduledExecutionFlow(event.getScheduledExecutionFlow().getId());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler, mockedScheduler, mockedScheduledExecutionFlowService);
	}

	
	@Test
	public void testRun_successful_notByScheduler() throws ReactionExecutionFlowException {
		// cron expression : every Monday at 22:00
		ScheduledExecutionFlow scheduledExecutionFlow = ScheduledExecutionFlow.builder().id(0l)
																						.cronExpression("0 0 22 ? * MON")
																						.scheduledAlready(true).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		Event event = Event.builder().scheduledExecutionFlow(scheduledExecutionFlow)
                                     .executionFlow(executionFlow)
                                     .id(45l)
                                     .startDate(new GregorianCalendar(2097, Calendar.JANUARY, 30, 22, 10, 0))
                                     .initiatedBy(EventInitiatorEnum.BY_LOG).build();
		instance.setEvent(event);
		instance.setDate( new GregorianCalendar(2097, Calendar.JANUARY, 30, 22, 10, 0).getTime() );
		
		// creating the expectations in mocks
		//    for mockedEventService
		when(mockedEventService.getEvent(event.getId())).thenReturn(event);
		when(mockedEventService.isScheduledEventExecuted(instance.event)).thenReturn(false);
		//    for mockedScheduledExecutionFlowService
		when(mockedScheduledExecutionFlowService.getScheduledExecutionFlow(event.getScheduledExecutionFlow().getId())).thenReturn(scheduledExecutionFlow);
		//    for mockedExecutionFlowMachineService
		when(mockedExecutionFlowMachineService.startIt(instance.event)).thenReturn(null);
		
		// call the method to be tested
		instance.run();
		
		// check & verify
		verify(mockedEventService,times(1)).getEvent(instance.event.getId());

		verify(mockedEventService,times(1)).isScheduledEventExecuted(instance.event);
		
		verify(mockedExecutionFlowMachineService, times(1)).startIt(instance.event);
		
		verify(mockedScheduledExecutionFlowService, times(1)).getScheduledExecutionFlow(event.getScheduledExecutionFlow().getId());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler, mockedScheduler, mockedScheduledExecutionFlowService);
	}
	
	
	@Test
	public void testRun_successful_descheduledMeanwhile() throws ReactionExecutionFlowException {
		// cron expression : every Monday at 22:00
		ScheduledExecutionFlow scheduledExecutionFlow = ScheduledExecutionFlow.builder().id(0l)
																						.cronExpression("0 0 22 ? * MON")
																						.scheduledAlready(false).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		Event event = Event.builder().scheduledExecutionFlow(scheduledExecutionFlow)
                                     .executionFlow(executionFlow)
                                     .id(45l)
                                     .initiatedBy(EventInitiatorEnum.BY_SCHEDULER).build();
		instance.setEvent(event);
		instance.setDate( new GregorianCalendar(2017, Calendar.JANUARY, 30, 22, 10, 0).getTime() );
		
		// creating the expectations in mocks
		//    for mockedEventService
		when(mockedEventService.getEvent(event.getId())).thenReturn(event);
		//    for mockedScheduledExecutionFlowService
		when(mockedScheduledExecutionFlowService.getScheduledExecutionFlow(scheduledExecutionFlow.getId())).thenReturn(scheduledExecutionFlow);
		
		// call the method to be tested
		instance.run();
		
		// check & verify
		verify(mockedEventService,times(1)).getEvent(instance.event.getId());
		
		verify(mockedScheduledExecutionFlowService, times(1)).getScheduledExecutionFlow(scheduledExecutionFlow.getId());
		
		verify(mockedEventService,times(1)).save(event);

		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler, mockedScheduler, mockedScheduledExecutionFlowService);
	}
	
	
	@Test
	public void testRun_successful_theScheduledEventAlreadyExecuted() throws ReactionExecutionFlowException {
		// cron expression : every Monday at 22:00
		ScheduledExecutionFlow scheduledExecutionFlow = ScheduledExecutionFlow.builder().id(0l)
																						.cronExpression("0 0 22 ? * MON")
																						.scheduledAlready(true).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		Event event = Event.builder().scheduledExecutionFlow(scheduledExecutionFlow)
                                     .executionFlow(executionFlow)
                                     .id(45l)
                                     .initiatedBy(EventInitiatorEnum.BY_SCHEDULER).build();
		instance.setEvent(event);
		instance.setDate( new GregorianCalendar(2017, Calendar.JANUARY, 30, 22, 10, 0).getTime() );
		
		// creating the expectations in mocks
		//    for mockedEventService
		when(mockedEventService.getEvent(event.getId())).thenReturn(event);
		when(mockedEventService.isScheduledEventExecuted(instance.event)).thenReturn(true);
		//    for mockedScheduledExecutionFlowService
		when(mockedScheduledExecutionFlowService.getScheduledExecutionFlow(scheduledExecutionFlow.getId())).thenReturn(scheduledExecutionFlow);
		
		// call the method to be tested
		instance.run();
		
		// check & verify
		verify(mockedEventService,times(1)).getEvent(instance.event.getId());
		
		verify(mockedEventService,times(1)).isScheduledEventExecuted(instance.event);
		
		verify(mockedScheduledExecutionFlowService, times(1)).getScheduledExecutionFlow(scheduledExecutionFlow.getId());

		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler, mockedScheduler, mockedScheduledExecutionFlowService);
	}
	
	
	@After
	public void clearMocks() {
		Mockito.reset(mockedScheduledExecutionFlowService);
		Mockito.reset(mockedEventService);
		Mockito.reset(mockedEventLifeService);
		Mockito.reset(mockedExecutionFlowMachineService);
		Mockito.reset(mockedErrorHandler);
		Mockito.reset(mockedScheduler);
	}
	
	
    @Configuration
    @Import(ScheduledRunnable.class)
    static class Config {
    }
	

}
