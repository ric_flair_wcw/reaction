package org.reaction.engine.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.ErrorDetector;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.service.ExecutionFlowMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
public class DefaultEventMachineServiceImplTest {

	
	@Autowired
	private DefaultEventMachineServiceImpl instance;
	
	@MockBean
	private EventService mockedEventService;
	@MockBean
	private ExecutionFlowMachineService mockedExecutionFlowMachineService;
	@MockBean
	private ErrorHandler mockedErrorHandler;

	
	// successful process
	//    - one event is needed
	//    - the same executionFlow is not started (the status of latest event is FAILED)
	//    - the end date of the latestEvent is not null
	@Test
	public void testProcessEvent_successful_oneEventNeeded_sameExecutionFlowNotStarted() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().executionTime(60).build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(null).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow ).build();
		
		// creating the expectations in mocks
		//    for mockedEventService
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.HOUR_OF_DAY, -2);
		Event latestFailedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FAILED).build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFailedEvent);
		Event latestWaitingEvent = Event.builder().build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestWaitingEvent);
		//    for mockedExecutionFlowMachineService
		when(mockedExecutionFlowMachineService.startIt(event)).thenReturn(null);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedExecutionFlowMachineService, times(1)).startIt(event);
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}
	
	
	// successful process
	//    - one event is needed
	//    - the same executionFlow is not started
	//    - the end date of the latestEvent is null
	@Test
	public void testProcessEvent_successful_oneEventNeeded_sameExecutionFlowNotStarted2() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().executionTime(60).build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(null).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow ).build(); 
		// creating the expectations in mocks
		//    for mockedEventService
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		Event latestConfEvent = Event.builder().endDate(null)
				                                   .status(EventStatusEnum.CONFIRMATION_NEEDED).build();
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestConfEvent);
		Event latestWaitingEvent = Event.builder().build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestWaitingEvent);
		when(mockedEventService.save(event)).thenReturn(null);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		assertNotNull(captorEvent.getValue().getReason());
		assertEquals(EventStatusEnum.IGNORED, captorEvent.getValue().getStatus());
		assertEquals(captorEvent.getValue().getStartDate().getTimeInMillis(), captorEvent.getValue().getEndDate().getTimeInMillis());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}

	
	// successful process
	//    - one event is needed
	//    - the same executionFlow is started recently
	@Test
	public void testProcessEvent_successful_oneEventNeeded_sameExecutionFlowIsStarted() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().timeBetweenExecutions(2 * 60 * 60).build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(null).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow).build(); 

		// creating the expectations in mocks
		//    for mockedEventService
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.HOUR_OF_DAY, -1);
		Event latestFinishedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FINISHED).build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFinishedEvent);
		Event latestEvent = Event.builder().build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestEvent);
		when(mockedEventService.save(event)).thenReturn(null);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());

		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		assertNotNull(captorEvent.getValue().getReason());
		assertEquals(EventStatusEnum.IGNORED, captorEvent.getValue().getStatus());
		assertEquals(captorEvent.getValue().getStartDate().getTimeInMillis(), captorEvent.getValue().getEndDate().getTimeInMillis());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}

	
	// successful process
	//    - more events are needed
	//    - there isn't latest event yet	
	@Test
	public void testProcessEvent_successful_moreEventsNeeded_noLatesetEventYet() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(2).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow).build(); 

		// creating the expectations in mocks
		//    for mockedEventService
		Event latestFinishedEvent = Event.builder().build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFinishedEvent);
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(null);
		Event savedEvent = Event.builder().id(11l).build();
		when(mockedEventService.save(event)).thenReturn(savedEvent);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		assertNotNull(captorEvent.getValue().getFirstEventArrived());
		assertEquals(EventStatusEnum.WAITING_FOR_OTHERS, captorEvent.getValue().getStatus());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}
	
	
	// successful process
	//    - more events are needed
	//    - latest event not null
	//    - the event that arrived is in the timeframe (executionFlow.multipleEventsTimeframe) -> for example 3 NullPointerExcpetions have to arrive in one hour to start the executionFlow; the event is in this one hour timeframe
	//                first event arrived 30 min ago and the multipleEventsTimeframe is 60min
	//                3 events need to start the executionFlow (multipleEventsCnt) and so far we have 2 events
	//    - equal to count => executionFlow has to start
	@Test
	public void testProcessEvent_successful_moreEventsNeeded_latesetEventNotNull_eventInTimeFrame_equalToMultipleEventsCnt() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(3)
				                                             .multipleEventsTimeframe(1*60*60).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow).build(); 

		// creating the expectations in mocks
		//    for mockedEventService
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MINUTE, -40);
		Event latestFinishedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FINISHED).build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFinishedEvent);
		Calendar firstEventArrived = Calendar.getInstance();
		firstEventArrived.add(Calendar.MINUTE, -30);
		Event latestWaitingEvent = Event.builder().firstEventArrived(firstEventArrived)
				                                  .errorDetector(errorDetector)
				                                  .multipleEventsCounter(2)
				                                  .id(14l).build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestWaitingEvent);
		//    for mockedExecutionFlowMachineService
		when(mockedExecutionFlowMachineService.startIt(latestWaitingEvent)).thenReturn(null);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());

		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		List<Event> capturedEvents = captorEvent.getAllValues();
		//    first save
		assertEquals(EventStatusEnum.WAITING_FOR_OTHERS, capturedEvents.get(0).getStatus());
		assertEquals(3, capturedEvents.get(0).getMultipleEventsCounter().intValue());
		
		verify(mockedExecutionFlowMachineService, times(1)).startIt(latestWaitingEvent);

		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}


	// successful process
	//    - more events are needed
	//    - latest event not null
	//    - the event that arrived is in the timeframe (executionFlow.multipleEventsTimeframe) -> for example 3 NullPointerExcpetions have to arrive in one hour to start the executionFlow; the event is in this one hour timeframe
	//                first event arrived 30 min ago and the multipleEventsTimeframe is 60min
	//                3 events need to start the executionFlow (multipleEventsCnt) but the current one is the 4th one
	//    - cnt > multipleEventsCnt
	@Test
	public void testProcessEvent_successful_moreEventsNeeded_latesetEventNotNull_eventInTimeFrame_biggerThanMultipleEventsCnt() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().timeBetweenExecutions(70 * 60).build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(3)
				                                             .multipleEventsTimeframe(1*60*60).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow).build(); 
		
		// creating the expectations in mocks
		//    for mockedEventService
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MINUTE, -40);
		Event latestFinishedEvent = Event.builder().endDate(endDate).build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFinishedEvent);
		
		Calendar firstEventArrived = Calendar.getInstance();
		firstEventArrived.add(Calendar.MINUTE, -30);
		Event latestEvent = Event.builder().firstEventArrived(firstEventArrived)
		             .errorDetector(errorDetector)
		             .multipleEventsCounter(3)
		             .id(14l).build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestEvent);
		
		// call the method to be tested
		instance.processEvent(event);
		
		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		List<Event> capturedEvents = captorEvent.getAllValues();
		//    first save
		assertEquals(EventStatusEnum.WAITING_FOR_OTHERS, capturedEvents.get(0).getStatus());
		assertEquals(1, capturedEvents.get(0).getMultipleEventsCounter().intValue());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}

	
	// successful process
	//    - more events are needed
	//    - latest event not null
	//    - the event that arrived is in the timeframe (executionFlow.multipleEventsTimeframe) -> for example 3 NullPointerExcpetions have to arrive in one hour to start the executionFlow; the event is in this one hour timeframe
	//                first event arrived 30 min ago and the multipleEventsTimeframe is 60min
	//                4 events need to start the executionFlow (multipleEventsCnt) and so far we have 2 events
	@Test
	public void testProcessEvent_successful_moreEventsNeeded_latesetEventNotNull_eventInTimeFrame_notEnoughEventYet() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(4)
				                                             .multipleEventsTimeframe(1*60*60).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow).build(); 

		// creating the expectations in mocks
		//    for mockedEventService
		Event latestFinishedEvent = Event.builder().id(12l).build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFinishedEvent);
		Calendar firstEventArrived = Calendar.getInstance();
		firstEventArrived.add(Calendar.MINUTE, -30);
		Event latestEvent = Event.builder().firstEventArrived(firstEventArrived)
				                           .errorDetector(errorDetector)
				                           .id(14l).build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestEvent);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.WAITING_FOR_OTHERS, captorEvent.getValue().getStatus());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService,  mockedErrorHandler);
	}

	
	// successful process
	//    - more events are needed
	//    - latest event not null
	//    - the event that arrived is outside the timeframe (executionFlow.multipleEventsTimeframe) -> for example 3 NullPointerExcpetions have to arrive in one hour to start the executionFlow; the event is outside this one hour timeframe
	//                first event arrived 90 min ago and the multipleEventsTimeframe is 60min
	//    - cnt > multipleEventsCnt
	@Test
	public void testProcessEvent_successful_moreEventsNeeded_latestEventNotNull_eventOutsideTimeFrame() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(3)
				                                             .multipleEventsTimeframe(1*60*60).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow).build(); 

		// creating the expectations in mocks
		//    for mockedEventService
		Event latestFinishedEvent = Event.builder().id(12l).build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFinishedEvent);
		Calendar firstEventArrived = Calendar.getInstance();
		firstEventArrived.add(Calendar.MINUTE, -90);
		Event latestEvent = Event.builder().firstEventArrived(firstEventArrived)
				                           .errorDetector(errorDetector)
				                           .id(14l).build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestEvent);
		Event savedEvent = Event.builder().id(11l).build();
		when(mockedEventService.save(event)).thenReturn(savedEvent);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		assertNull(captorEvent.getValue().getId());
		assertEquals(EventStatusEnum.WAITING_FOR_OTHERS, captorEvent.getValue().getStatus());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}
	
	
	// successful process
	//    - more events are needed
	//    - latest event not null
	//    - the event that arrived is in the timeframe (executionFlow.multipleEventsTimeframe) -> for example 3 NullPointerExcpetions have to arrive in one hour to start the executionFlow; the event is in this one hour timeframe
	//                first event arrived 30 min ago and the multipleEventsTimeframe is 60min
	//                3 events need to start the executionFlow (multipleEventsCnt) and so far we have 2 events
	//    - equal to count => executionFlow should be started but
	//    - another same executionFlow was started recently (it was started 40 min ago) and the timeBetweenExecutions is 70 min
	@Test
	public void testProcessEvent_successful_moreEventsNeeded_latesetEventNotNull_eventInTimeFrame_equalToMultipleEventsCnt_sameExecutionFlowIsStarted() throws ReactionExecutionFlowException {
		ExecutionFlow executionFlow = ExecutionFlow.builder().timeBetweenExecutions(70 * 60).build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow )
				                                             .multipleEventsCnt(3)
				                                             .multipleEventsTimeframe(1*60*60).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .executionFlow(executionFlow).build(); 

		// creating the expectations in mocks
		//    for mockedEventService
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MINUTE, -40);
		Event latestFinishedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FINISHED).build();
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true)).thenReturn(latestFinishedEvent);
		
		Calendar firstEventArrived = Calendar.getInstance();
		firstEventArrived.add(Calendar.MINUTE, -30);
		Event latestEvent = Event.builder().firstEventArrived(firstEventArrived)
				                           .errorDetector(errorDetector)
				                           .multipleEventsCounter(2)
				                           .id(14l).build();
		when(mockedEventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow())).thenReturn(latestEvent);

		// call the method to be tested
		instance.processEvent(event);

		// check & verify
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
		
		verify(mockedEventService, times(1)).getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(2)).save(captorEvent.capture());
		List<Event> capturedEvents = captorEvent.getAllValues();
		System.out.println(capturedEvents.get(0) == capturedEvents.get(1));
		//    first save
//		assertEquals(EventStatusEnum.WAITING_FOR_OTHERS, capturedEvents.get(0).getStatus());
//		assertEquals(3, capturedEvents.get(0).getMultipleEventsCounter().intValue());
		//    second save
		assertNotNull(capturedEvents.get(1).getReason());
		assertEquals(EventStatusEnum.IGNORED, capturedEvents.get(1).getStatus());
		assertEquals(capturedEvents.get(1).getStartDate().getTimeInMillis(), capturedEvents.get(1).getEndDate().getTimeInMillis());
		
		verifyNoMoreInteractions(mockedEventService, mockedExecutionFlowMachineService, mockedErrorHandler);
	}
	
	
	@Configuration
    @Import(DefaultEventMachineServiceImpl.class)
    static class Config {
    }
	
}
