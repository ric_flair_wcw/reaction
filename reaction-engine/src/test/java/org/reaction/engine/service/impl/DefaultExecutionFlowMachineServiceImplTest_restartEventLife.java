package org.reaction.engine.service.impl;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.*;
import org.reaction.engine.scheduling.Scheduler;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.tasks.CommandByWorkerTaskCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Clock;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
public class DefaultExecutionFlowMachineServiceImplTest_restartEventLife {

	
	@Autowired
	private DefaultExecutionFlowMachineServiceImpl instance;
	
	@MockBean
	private EventService mockedEventService;
	@MockBean
	private EventLifeService mockedEventLifeService;
	@MockBean
	private TaskService mockedTaskService;
	@MockBean
	private Scheduler mockedScheduler;
	@MockBean
	private CommandByWorkerTaskCommand mockedCommand;
	@MockBean
	private Clock mockedClock;
	@MockBean
	private ExecutionFlowService mockedExecutionFlowService;
	@MockBean
	private EventSourceService mockedEventSourceService;
	@MockBean
	private AuthQuery mockedAuthQuery;
	@MockBean
	private ReactionMailSender mockedMailSender;
	@MockBean
	private ErrorDetectorService mockedErrorDetectorService;
	@MockBean
	private EventMachineService mockedEventMachineService;

	
	@Test
	public void testRestartEventLife() throws ReactionExecutionFlowException {
		Event event = Event.builder().endDate(Calendar.getInstance())
									 .status(EventStatusEnum.FAILED).build();
		EventLife eventLife = EventLife.builder().event(event )
												 .order(4)
												 .task(mockedCommand).build();
		String user = "ric.flair";
		
		// call the method to be tested
		instance.restartEventLife(eventLife, user );
		
		// check & verify
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.STARTED, captorEvent.getValue().getStatus());
		assertNull(captorEvent.getValue().getEndDate());
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(2)).save(captorEventLife.capture());
		List<EventLife> eventLifeRecs = captorEventLife.getAllValues();
		//    first save
		assertEquals(EventStatusEnum.FAILED_AND_RESTARTED, eventLifeRecs.get(0).getEventStatus());
		assertEquals(user, eventLifeRecs.get(0).getByWhom());
		//    second save
		assertEquals(EventStatusEnum.STARTED, eventLifeRecs.get(1).getEventStatus());
		assertEquals(eventLife.getOrder() + 1, eventLifeRecs.get(1).getOrder().intValue());
		assertEquals(mockedCommand, eventLifeRecs.get(1).getTask());
		
		verify(mockedCommand, times(1)).execute( any());
		
		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedCommand);
	}
	
	
	@After
	public void clearMocks() {
		Mockito.reset(mockedEventService);
		Mockito.reset(mockedEventLifeService);
		Mockito.reset(mockedCommand);
	}

	
	@Configuration
    @Import(DefaultExecutionFlowMachineServiceImpl.class)
    static class Config {
    }

}
