package org.reaction.engine.service.impl;

import org.junit.Test;
import org.mockito.Mockito;
import org.reaction.common.contants.AuthenticationTypeEnum;
import org.reaction.common.domain.EventSourceType;
import org.reaction.engine.errors.ResourceException;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.reaction.engine.service.impl.DefaultSecurityServiceImpl.HEADER_BASIC_AUTH;

public class DefaultSecurityServiceImplTest {

    private DefaultSecurityServiceImpl instance = new DefaultSecurityServiceImpl();


    @Test
    public void successful_BASIC_AUTH() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("Basic cmljX2ZsYWlyOnJpY19mbGFpcg==");

        instance.check(eventSourceType, mockedRequest);
    }

    @Test
    public void unsuccessful_BASIC_AUTH_incorrectPassword() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                .secret("fecskakanya")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("Basic cmljX2ZsYWlyOnJpY19mbGFpcg==");

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The user+password in the request header is not equal to the ones in the database!");
        }
    }

    @Test
    public void unsuccessful_BASIC_AUTH_headerMissing() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("NEMTOMMI");

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The request header doesn't contain the BASIC AUTH header!");
        }
    }

    @Test
    public void unsuccessful_BASIC_AUTH_headerIncomplete() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("Basic");

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The request header doesn't contain the BASIC AUTH header!");
        }
    }

    @Test
    public void unsuccessful_BASIC_AUTH_noColonInBase64edHeader() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("Basic cmljX2ZsYWly");

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The BASIC AUTH info in the header is invalid!");
        }
    }

    @Test
    public void unsuccessful_BASIC_AUTH_noUserInBase64edHeader() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("Basic OnJpY19mbGFpcg==");

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The BASIC AUTH info in the header doesn't contain the user name!");
        }
    }

    @Test
    public void unsuccessful_BASIC_AUTH_headerNotBase64() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("Basic WellYouWontBase64This");

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The BASIC AUTH info couldn't be decoded from the request header!");
        }
    }

    @Test
    public void unsuccessful_BASIC_AUTH_noPasswordInBase64edHeader() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .user("ric_flair")
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.BASIC_AUTH).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        when(mockedRequest.getHeader(HEADER_BASIC_AUTH)).thenReturn("Basic cmljX2ZsYWlyOg==");

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The BASIC AUTH info in the header doesn't contain the password!");
        }
    }


    @Test
    public void successful_SECRET() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.SECRET).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        Map<String, String[]> params = new HashMap<>();
        params.put("secret", new String[] {"ric_flair"});
        when(mockedRequest.getParameterMap()).thenReturn(params);

        instance.check(eventSourceType, mockedRequest);
    }

    @Test
    public void unsuccessful_SECRET_noSecretUrlParam() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                // the password is ric_flair
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.SECRET).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        Map<String, String[]> params = new HashMap<>();
        params.put("secet", new String[] {"ric_flair"});
        when(mockedRequest.getParameterMap()).thenReturn(params);

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The secret is not in the request but the authentication type is SECRET!");
        }
    }

    @Test
    public void unsuccessful_SECRET_IncorrectPassword() {
        EventSourceType eventSourceType = EventSourceType.builder()
                .enforceSsl(true)
                .secret("TO37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==")
                .authenticationType(AuthenticationTypeEnum.SECRET).build();

        HttpServletRequest mockedRequest = Mockito.mock(HttpServletRequest.class);
        when(mockedRequest.isSecure()).thenReturn(true);
        Map<String, String[]> params = new HashMap<>();
        params.put("secret", new String[] {"kanya2"});
        when(mockedRequest.getParameterMap()).thenReturn(params);

        try {
            instance.check(eventSourceType, mockedRequest);
        } catch(ResourceException e) {
            assertEquals(e.getHttpStatus(), HttpStatus.UNAUTHORIZED);
            assertEquals(e.getMessage(), "The secret in the request is not equal to the one in the database!");
        }
    }

}
