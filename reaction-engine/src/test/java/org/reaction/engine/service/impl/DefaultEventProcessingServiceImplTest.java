package org.reaction.engine.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.reaction.common.contants.DataTypeEnum;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventSourceHierarchyTypeEnum;
import org.reaction.common.domain.*;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.service.ErrorDetectorService;
import org.reaction.engine.persistence.service.EventSourceService;
import org.reaction.engine.persistence.service.EventSourceTypeService;
import org.reaction.engine.service.EventMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StreamUtils;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class DefaultEventProcessingServiceImplTest {

	@Autowired
	private DefaultEventProcessingServiceImpl instance;
	@Autowired
    private ObjectMapper objectMapper;
	
	@MockBean
	private EventSourceTypeService mockedEventSourceTypeService;
    @MockBean
    private EventSourceService mockedEventSourceService;
    @MockBean
    private ErrorDetectorService mockedErrorDetectorService;
    @MockBean
    private EventMachineService mockedEventMachineService;
    @Captor
    private ArgumentCaptor<Event> captorEvent;

    
	@Test
	public void testProcess_successful_json() throws IOException, XPathExpressionException, ReactionConfigurationException, ReactionExecutionFlowException {
		String uuid = "uuid";
		String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"$.result.host\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"$.result.source\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"app_name\",\"desc\": \"App name\",\"path\": \"app_name\", \"source\":\"QUERY_PARAMETER\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"message\",\"desc\":\"Message\",\"path\":\"$.result._raw\",\"source\":\"PAYLOAD\",\"output\":\"ERROR_DETECTOR\"}," +
                                    "{\"name\": \"error_type\",\"desc\":\"Error type\",\"path\":\"error_type\",\"source\":\"QUERY_PARAMETER\",\"output\":\"ERROR_DETECTOR\"}]";
		EventSourceType eventSourceType = EventSourceType.builder().code("splunk8")
                                                                   .name("Splunk 8")
                                                                   .pathToIdentifiers(pathToIdentifiers)
                                                                   .dataType(DataTypeEnum.JSON).build();
        List<EventSource> eventSources = Collections.singletonList(
                EventSource.builder().id(0l)
                                     .identifiers("{\"logfile-PAYLOAD\": \"/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log\", " +
                                                   "\"app_name-QUERY_PARAMETER\": \"kanyaapp\", " +
                                                   "\"host-PAYLOAD\": \"localhost\"}")
                                     .type(EventSourceHierarchyTypeEnum.ITEM).build());
        ExecutionFlow executionFlow = ExecutionFlow.builder().build();
        ErrorDetector errorDetector = ErrorDetector.builder().id(0l)
                                                             .executionFlow(executionFlow)
                                                             .identifiers("{\"message-PAYLOAD\": \".*java\\.lang\\.NullPointerException.*\"," +
                                                                           "\"error_type-QUERY_PARAMETER\": \".*veryserious.*\"}").build();
		String requestBody = readFile("integrationTest/otherInput/Splunk-req.json");
        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("app_name", "kanyaapp");
        queryParameters.put("error_type", "not sure but possibley veryserious");

		// setting the expectations for mocks
        when(mockedEventSourceService.getEventSourceByEventSourceType(eventSourceType)).thenReturn(eventSources);
        when(mockedErrorDetectorService.search(any(Event.class), eq(false), any(Map.class), any(Map.class), eq(eventSourceType))).thenReturn(errorDetector);

		// call the method to be tested
        HttpStatus httpStatus = instance.process(eventSourceType, requestBody, queryParameters, uuid);
		
		// check and verify
        assertEquals(HttpStatus.CREATED, httpStatus);
		verify(mockedEventMachineService, times(1)).processEvent(captorEvent.capture());
		assertEquals(eventSources.get(0).getId(), captorEvent.getValue().getEventSourceId());
        assertEquals(EventInitiatorEnum.BY_EXTERNAL_SOURCE, captorEvent.getValue().getInitiatedBy());
        assertEquals(uuid, captorEvent.getValue().getIdentifier());
        Map<String,String> identifiersPayload = objectMapper.readValue(captorEvent.getValue().getPayloadValues(), new TypeReference<Map<String,String>>() {});
        assertEquals(3, identifiersPayload.size());
        assertEquals("/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log", identifiersPayload.get("logfile"));
        assertEquals("2019-07-25 15:25:40.393 ERROR [org.reaction.engine.ReactionApplication:53]  - Error occured while stopping the application context! java.lang.NullPointerException: ", identifiersPayload.get("message"));
        assertEquals("localhost", identifiersPayload.get("host"));
        Map<String,String> identifiersQueryParameters = objectMapper.readValue(captorEvent.getValue().getQueryParameters(), new TypeReference<Map<String,String>>() {});
        assertEquals(2, identifiersQueryParameters.size());
        assertEquals("not sure but possibley veryserious", identifiersQueryParameters.get("error_type"));
        assertEquals("kanyaapp", identifiersQueryParameters.get("app_name"));
        assertEquals(errorDetector, captorEvent.getValue().getErrorDetector());
        assertEquals(executionFlow, captorEvent.getValue().getExecutionFlow());
        verifyNoMoreInteractions(mockedEventMachineService);
	}
	
	
    @Test
    public void testProcess_successful_xml() throws IOException, XPathExpressionException, ReactionConfigurationException, ReactionExecutionFlowException {
        String uuid = "uuid";
        String eventSourceTypeCode = "xmlinput";
        String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:source\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"message\",\"desc\":\"Message\",\"path\":\"/soap:Envelope/soap:Body/ns:request/ns:event/ns:message\",\"source\":\"PAYLOAD\",\"output\":\"ERROR_DETECTOR\"}]";
        EventSourceType eventSourceType = EventSourceType.builder().code(eventSourceTypeCode)
                                                                   .name("XML input")
                                                                   .pathToIdentifiers(pathToIdentifiers)
                                                                   .namespaces("soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"")
                                                                   .dataType(DataTypeEnum.XML).build();
        List<EventSource> eventSources = Collections.singletonList(
                EventSource.builder().id(0l)
                                     .identifiers("{\"logfile-PAYLOAD\": \"/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log\", \"host\": \"localhost\"}")
                                     .type(EventSourceHierarchyTypeEnum.ITEM).build());
        ExecutionFlow executionFlow = ExecutionFlow.builder().build();
        ErrorDetector errorDetector = ErrorDetector.builder().id(0l)
                                                             .executionFlow(executionFlow)
                                                             .identifiers("{\"message-PAYLOAD\": \".*java\\.lang\\.NullPointerException.*\"}").build();
        String requestBody = readFile("integrationTest/otherInput/xmlinput-req.xml");
        Map<String, String> queryParameters = new HashMap<>();
        
        // setting the expectations for mocks
        when(mockedEventSourceTypeService.getEventSourceTypeByCode(eventSourceTypeCode)).thenReturn(eventSourceType);
        when(mockedEventSourceService.getEventSourceByEventSourceType(eventSourceType)).thenReturn(eventSources);
        when(mockedErrorDetectorService.search(any(Event.class), eq(false), any(Map.class), any(Map.class), eq(eventSourceType))).thenReturn(errorDetector);

        // call the method to be tested
        HttpStatus httpStatus = instance.process(eventSourceType, requestBody, queryParameters, uuid);
        
        // check and verify
        assertEquals(HttpStatus.CREATED, httpStatus);
        verify(mockedEventMachineService, times(1)).processEvent(captorEvent.capture());
        assertEquals(eventSources.get(0).getId(), captorEvent.getValue().getEventSourceId());
        assertEquals(EventInitiatorEnum.BY_EXTERNAL_SOURCE, captorEvent.getValue().getInitiatedBy());
        assertEquals(uuid, captorEvent.getValue().getIdentifier());
        Map<String,String> identifiers = objectMapper.readValue(captorEvent.getValue().getPayloadValues(), new TypeReference<Map<String,String>>() {});
        assertEquals("/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log", identifiers.get("logfile"));
        assertEquals("2019-07-25 15:25:40.393 ERROR [org.reaction.engine.ReactionApplication:53]  - Error occured while stopping the application context! java.lang.NullPointerException: ", identifiers.get("message"));
        assertEquals("localhost", identifiers.get("host"));
        assertEquals(errorDetector, captorEvent.getValue().getErrorDetector());
        assertEquals(executionFlow, captorEvent.getValue().getExecutionFlow());
        verifyNoMoreInteractions(mockedEventMachineService);
    }
    

    @Test
    public void testProcess_unsuccessful_xml_invalidNamespace() throws IOException, XPathExpressionException, ReactionExecutionFlowException {
        String uuid = "uuid";
        String eventSourceTypeCode = "xmlinput";
        String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                   " {\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:source\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}]";
        EventSourceType eventSourceType = EventSourceType.builder().code(eventSourceTypeCode)
                                                                   .name("XML input")
                                                                   .pathToIdentifiers(pathToIdentifiers)
                                                                   .namespaces("soap\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"")
                                                                   .dataType(DataTypeEnum.XML).build();
        String requestBody = readFile("integrationTest/otherInput/xmlinput-req.xml");
        Map<String, String> queryParameters = new HashMap<>();
        
        // setting the expectations for mocks
        when(mockedEventSourceTypeService.getEventSourceTypeByCode(eventSourceTypeCode)).thenReturn(eventSourceType);

        // call the method to be tested
        try {
            instance.process(eventSourceType, requestBody, queryParameters, uuid);
            fail("ReactionConfigurationException should have been thrown!");
        } catch(ReactionConfigurationException e) {
            assertEquals("The namespace text doesn't contain an equal sign!", e.getMessage());
        }
        verifyZeroInteractions(mockedEventMachineService);
    }

    
    @Test
    public void testProcess_unsuccessful_xml_invalidXML() throws IOException, XPathExpressionException, ReactionConfigurationException, ReactionExecutionFlowException {
        String uuid = "uuid";
        String eventSourceTypeCode = "xmlinput";
        String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:source\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}]";
        EventSourceType eventSourceType = EventSourceType.builder().code(eventSourceTypeCode)
                                                                   .name("XML input")
                                                                   .pathToIdentifiers(pathToIdentifiers)
                                                                   .namespaces("soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"")
                                                                   .dataType(DataTypeEnum.XML).build();
        String requestBody = "not really...";
        Map<String, String> queryParameters = new HashMap<>();
        
        // setting the expectations for mocks
        when(mockedEventSourceTypeService.getEventSourceTypeByCode(eventSourceTypeCode)).thenReturn(eventSourceType);

        // call the method to be tested
        try {
            instance.process(eventSourceType, requestBody, queryParameters, uuid);
            fail("ReactionRuntimeException should have been thrown!");
        } catch(ReactionRuntimeException e) {
            assertEquals("Error occurred when unmarhalling the request to XML!", e.getMessage());
        }
        verifyZeroInteractions(mockedEventMachineService);
    }

    
    @Test
    public void testProcess_unsuccessful_xml_moreMatchingEventSources() throws IOException, XPathExpressionException, ReactionConfigurationException, ReactionExecutionFlowException {
        String uuid = "uuid";
        String eventSourceTypeCode = "xmlinput";
        String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:source\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}]";
        EventSourceType eventSourceType = EventSourceType.builder().code(eventSourceTypeCode)
                                                                   .name("XML input")
                                                                   .pathToIdentifiers(pathToIdentifiers)
                                                                   .namespaces("soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"")
                                                                   .dataType(DataTypeEnum.XML).build();
        List<EventSource> eventSources = new ArrayList<>();
        eventSources.add(EventSource.builder().id(0l)
                                              .identifiers("{\"logfile-PAYLOAD\": \"/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log\", \"host\": \"localhost\"}")
                                              .type(EventSourceHierarchyTypeEnum.ITEM).build());
        eventSources.add(EventSource.builder().id(1l)
                                              .identifiers("{\"logfile-PAYLOAD\": \"/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log\", \"host\": \"localhost\"}")
                                              .type(EventSourceHierarchyTypeEnum.ITEM).build());
        String requestBody = readFile("integrationTest/otherInput/xmlinput-req.xml");
        Map<String, String> queryParameters = new HashMap<>();
        
        // setting the expectations for mocks
        when(mockedEventSourceTypeService.getEventSourceTypeByCode(eventSourceTypeCode)).thenReturn(eventSourceType);
        when(mockedEventSourceService.getEventSourceByEventSourceType(eventSourceType)).thenReturn(eventSources);

        // call the method to be tested
        try {
            instance.process(eventSourceType, requestBody, queryParameters, uuid);
            fail("ReactionConfigurationException should have been thrown!");
        } catch(ReactionConfigurationException e) {
            assertEquals(String.format("More than one lowest level event sources are found but only one is expected!"), e.getMessage());
        }
        verifyZeroInteractions(mockedEventMachineService);
    }

    
    @Test
    public void testProcess_unsuccessful_xml_noMatchingEventSource() throws IOException, XPathExpressionException, ReactionConfigurationException, ReactionExecutionFlowException {
        String uuid = "uuid";
        String eventSourceTypeCode = "xmlinput";
        String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:source\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}]";
        EventSourceType eventSourceType = EventSourceType.builder().code(eventSourceTypeCode)
                                                                   .name("XML input")
                                                                   .pathToIdentifiers(pathToIdentifiers)
                                                                   .namespaces("soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"")
                                                                   .dataType(DataTypeEnum.XML).build();
        List<EventSource> eventSources = new ArrayList<>();
        String requestBody = readFile("integrationTest/otherInput/xmlinput-req.xml");
        Map<String, String> queryParameters = new HashMap<>();
        
        // setting the expectations for mocks
        when(mockedEventSourceTypeService.getEventSourceTypeByCode(eventSourceTypeCode)).thenReturn(eventSourceType);
        when(mockedEventSourceService.getEventSourceByEventSourceType(eventSourceType)).thenReturn(eventSources);

        // call the method to be tested
        try {
            instance.process(eventSourceType, requestBody, queryParameters, uuid);
            fail("ReactionRuntimeException should have been thrown!");
        } catch(ReactionRuntimeException e) {
            assertEquals(String.format("No event source could be found for the specific event source type and the identifiers!"), e.getMessage());
        }
        verifyZeroInteractions(mockedEventMachineService);
    }

    
    @Test
    public void testProcess_unsuccessful_xml_noErrorDetector() throws IOException, XPathExpressionException, ReactionConfigurationException, ReactionExecutionFlowException {
        String uuid = "uuid";
        String eventSourceTypeCode = "xmlinput";
        String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:host\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                                    "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"/soap:Envelope/soap:Body/ns:request/ns:event/ns:source\",\"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}]";
        EventSourceType eventSourceType = EventSourceType.builder().code(eventSourceTypeCode)
                                                                   .name("XML input")
                                                                   .pathToIdentifiers(pathToIdentifiers)
                                                                   .namespaces("soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"")
                                                                   .dataType(DataTypeEnum.XML).build();
        List<EventSource> eventSources = Collections.singletonList(
                EventSource.builder().id(0l)
                                     .identifiers("{\"logfile-PAYLOAD\": \"/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log\", \"host\": \"localhost\"}")
                                     .type(EventSourceHierarchyTypeEnum.ITEM).build());
        String requestBody = readFile("integrationTest/otherInput/xmlinput-req.xml");
        Map<String, String> queryParameters = new HashMap<>();
        
        // setting the expectations for mocks
        when(mockedEventSourceTypeService.getEventSourceTypeByCode(eventSourceTypeCode)).thenReturn(eventSourceType);
        when(mockedEventSourceService.getEventSourceByEventSourceType(eventSourceType)).thenReturn(eventSources);
        when(mockedErrorDetectorService.search(any(Event.class), eq(false), any(Map.class), any(Map.class), eq(eventSourceType))).thenReturn(null);

        // call the method to be tested
        HttpStatus httpStatus = instance.process(eventSourceType, requestBody, queryParameters, uuid);
        
        // check and verify
        assertEquals(HttpStatus.OK, httpStatus);
        verifyZeroInteractions(mockedEventMachineService);
    }


    @Test
    public void whenThereAreMoreEventSourcesAndOneMatchesThenFindIt() throws IOException, ReactionConfigurationException, XPathExpressionException, ReactionExecutionFlowException {
        String uuid = "uuid";
        String pathToIdentifiers = "[{\"name\": \"host\",\"desc\": \"Host\",\"path\": \"$.result.host\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                "{\"name\": \"logfile\",\"desc\": \"Log file\",\"path\": \"$.result.source\", \"source\":\"PAYLOAD\",\"output\":\"EVENT_SOURCE\"}," +
                "{\"name\": \"app_name\",\"desc\": \"App name\",\"path\": \"app_name\", \"source\":\"QUERY_PARAMETER\",\"output\":\"EVENT_SOURCE\"}," +
                "{\"name\": \"message\",\"desc\":\"Message\",\"path\":\"$.result._raw\",\"source\":\"PAYLOAD\",\"output\":\"ERROR_DETECTOR\"}," +
                "{\"name\": \"error_type\",\"desc\":\"Error type\",\"path\":\"error_type\",\"source\":\"QUERY_PARAMETER\",\"output\":\"ERROR_DETECTOR\"}]";
        EventSourceType eventSourceType = EventSourceType.builder().code("splunk8")
                .name("Splunk 8")
                .pathToIdentifiers(pathToIdentifiers)
                .dataType(DataTypeEnum.JSON).build();
        EventSource meliusEventSource = EventSource.builder().type(EventSourceHierarchyTypeEnum.GROUP)
                                                             .identifiers("{\"logfile-PAYLOAD\": \"/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log\", " +
                                                                          "\"app_name-QUERY_PARAMETER\": \"kanyaapp\", " +
                                                                          "\"host-PAYLOAD\": \"localhost\"}").build();
        List<EventSource> eventSources = Arrays.asList(
                EventSource.builder().identifiers("{\"logfile-PAYLOAD\": \".*reaction-engine.log\", " +
                                                  "\"app_name-QUERY_PARAMETER\": \"focskeapp\", " +
                                                  "\"host-PAYLOAD\": \"localhost\"}")
                                     .parent(meliusEventSource)
                                     .type(EventSourceHierarchyTypeEnum.ITEM).build(),
                EventSource.builder().identifiers("{\"logfile-PAYLOAD\": \"/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log\", " +
                                                    "\"app_name-QUERY_PARAMETER\": \"kanyaapp\", " +
                                                    "\"host-PAYLOAD\": \"localhost\"}")
                                     .parent(meliusEventSource)
                                     .type(EventSourceHierarchyTypeEnum.ITEM).build()


                );
        ExecutionFlow executionFlow = ExecutionFlow.builder().build();
        ErrorDetector errorDetector = ErrorDetector.builder().id(0l)
                .executionFlow(executionFlow)
                .identifiers("{\"message-PAYLOAD\": \".*java\\.lang\\.NullPointerException.*\"," +
                              "\"error_type-QUERY_PARAMETER\": \".*veryserious.*\"}").build();
        String requestBody = readFile("integrationTest/otherInput/Splunk-req.json");
        Map<String, String> queryParameters = new HashMap<>();
        queryParameters.put("app_name", "kanyaapp");
        queryParameters.put("error_type", "not sure but possibley veryserious");

        // setting the expectations for mocks
        when(mockedEventSourceService.getEventSourceByEventSourceType(eventSourceType)).thenReturn(eventSources);
        when(mockedErrorDetectorService.search(any(Event.class), eq(false), any(Map.class), any(Map.class), eq(eventSourceType))).thenReturn(errorDetector);

        // call the method to be tested
        HttpStatus httpStatus = instance.process(eventSourceType, requestBody, queryParameters, uuid);

        // check and verify
        assertEquals(HttpStatus.CREATED, httpStatus);
        verify(mockedEventMachineService, times(1)).processEvent(captorEvent.capture());
        assertEquals(eventSources.get(0).getId(), captorEvent.getValue().getEventSourceId());
        assertEquals(EventInitiatorEnum.BY_EXTERNAL_SOURCE, captorEvent.getValue().getInitiatedBy());
        assertEquals(uuid, captorEvent.getValue().getIdentifier());
        Map<String,String> identifiersPayload = objectMapper.readValue(captorEvent.getValue().getPayloadValues(), new TypeReference<Map<String,String>>() {});
        assertEquals(3, identifiersPayload.size());
        assertEquals("/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log", identifiersPayload.get("logfile"));
        assertEquals("2019-07-25 15:25:40.393 ERROR [org.reaction.engine.ReactionApplication:53]  - Error occured while stopping the application context! java.lang.NullPointerException: ", identifiersPayload.get("message"));
        assertEquals("localhost", identifiersPayload.get("host"));
        Map<String,String> identifiersQueryParameters = objectMapper.readValue(captorEvent.getValue().getQueryParameters(), new TypeReference<Map<String,String>>() {});
        assertEquals(2, identifiersQueryParameters.size());
        assertEquals("not sure but possibley veryserious", identifiersQueryParameters.get("error_type"));
        assertEquals("kanyaapp", identifiersQueryParameters.get("app_name"));
        assertEquals(errorDetector, captorEvent.getValue().getErrorDetector());
        assertEquals(executionFlow, captorEvent.getValue().getExecutionFlow());
        verifyNoMoreInteractions(mockedEventMachineService);
    }


    protected String readFile(String path) throws IOException {
		return StreamUtils.copyToString(new ClassPathResource(path).getInputStream(), Charset.defaultCharset());
	}


    @Configuration
    @Import(DefaultEventProcessingServiceImpl.class)
    static class Config {
    
        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }
    }

}
