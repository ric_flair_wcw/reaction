package org.reaction.engine.service.impl;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.contants.ExecutionFlowStatusEnum;
import org.reaction.common.contants.PermissionEnum;
import org.reaction.common.domain.*;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.*;
import org.reaction.engine.scheduling.Scheduler;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.tasks.CommandByWorkerTaskCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
public class DefaultExecutionFlowMachineServiceImplTest_startIt {

	
	@Autowired
	private DefaultExecutionFlowMachineServiceImpl instance;
	
	@MockBean
	private EventService mockedEventService;
	@MockBean
	private EventLifeService mockedEventLifeService;
	@MockBean
	private TaskService mockedTaskService;
	@MockBean
	private ErrorDetectorService mockedErrorDetectorService;
	@MockBean
	private Scheduler mockedScheduler;
	@MockBean
	private CommandByWorkerTaskCommand mockedCommand;
	@MockBean
	private Clock mockedClock;
	@MockBean
	private ExecutionFlowService mockedExecutionFlowService;
	@MockBean
	private EventSourceService mockedEventSourceService;
	@MockBean
	private AuthQuery mockedAuthQuery;
	@MockBean
	private ReactionMailSender mockedMailSender;
	@MockBean
	private EventMachineService mockedEventMachineService;
	
	// exec flow start
	//  - in the maintenance window
	//  - there are tasks to be executed
	//  - no confirmation was needed
	//  - initiated BY_LOG
	//  - the execution flow is not frozen yet
	//  - there is at least one task in the flow
	//  - the exec time of the execution flow is set
	//  - there is one latest FINISHED event and it is outside the period between executions
	@Test
	public void testStartIt_successful_inMaintenanceWindow_thereAreTasksToBeExecuted() throws ReactionExecutionFlowException {
		MaintenanceWindow maintenanceWindow = new MaintenanceWindow(
						"{ \"Sun\": [\"14:00-04:00\"]," +
						"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
						"  \"Tue\": [\"23:00-04:00\"]," +
						"  \"Wed\": [\"23:00-04:00\"]," +
						"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
						"  \"Fri\": [\"23:00-04:00\"]," +
						"  \"Sat\": [\"20:00-04:00\"] }",		
						mockedClock);
		EventSource eventSource = EventSource.builder().id(32434l)
				                             .maintenanceWindow(maintenanceWindow).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(13l)
															 .status(ExecutionFlowStatusEnum.VALID)
															 .executionTime(20)
															 .timeBetweenExecutions(3600).build();
		ErrorDetector errorDetector = ErrorDetector.builder().eventSource(eventSource)
				                                             .executionFlow(executionFlow )	
				                                             .confirmationNeeded(false).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .initiatedBy(EventInitiatorEnum.BY_LOG)
				                     .eventSourceId(eventSource.getId())
				                     .executionFlow(executionFlow)
				                     .build();
		// creating the expectations in mocks
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-27T23:15:30.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("Europe/London"));
		when(mockedTaskService.getTopByExecutionFlowEntityOrderByOrderAsc(any(ExecutionFlow.class))).thenReturn(mockedCommand);
		EventLife savedEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		doNothing().when(mockedCommand).execute(any(EventLife.class));
		when(mockedEventSourceService.getEventSourceAndFillProperties(event.getEventSourceId())).thenReturn(eventSource);
		when(mockedExecutionFlowService.getExecutionFlow(executionFlow.getId())).thenReturn(executionFlow);
		when(mockedEventService.save(event)).thenReturn(event);
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.FINISHED);
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MINUTE, -120);
		Event latestFinishedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FINISHED)
				                                   .executionFlow(executionFlow).build();
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false)).thenReturn(latestFinishedEvent);
		//    for mockedErrorDetectorService
		when(mockedErrorDetectorService.getErrorDetectorById(event.getErrorDetector().getId())).thenReturn(errorDetector);
		
		// call the method to be tested
		instance.sendMailWhenStartingFlow = true;
		instance.sendMailWhenConfirmationIsNeeded = true;
		instance.startIt(event);
		
		// check & verify
		verify(mockedEventSourceService, times(1)).getEventSourceAndFillProperties(event.getEventSourceId());
		
		verify(mockedExecutionFlowService, times(1)).changeStatus(executionFlow, ExecutionFlowStatusEnum.FROZEN);
		verify(mockedExecutionFlowService, times(1)).getExecutionFlow(executionFlow.getId());
				
		ArgumentCaptor<ExecutionFlow> captorExecutionFlow = ArgumentCaptor.forClass(ExecutionFlow.class);
		verify(mockedTaskService,times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(captorExecutionFlow.capture());
		assertEquals(executionFlow.getId(), captorExecutionFlow.getValue().getId());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.STARTED, captorEvent.getValue().getStatus());
		assertNotNull(captorEvent.getValue().getStartDate());

		verify(mockedTaskService, times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(executionFlow);
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(1)).save(captorEventLife.capture());
		assertEquals(event, captorEventLife.getValue().getEvent());
		assertEquals(mockedCommand, captorEventLife.getValue().getTask());
		assertEquals(EventStatusEnum.STARTED, captorEventLife.getValue().getEventStatus());
		assertEquals(0, captorEventLife.getValue().getOrder().intValue());
		
		verify(mockedCommand, times(1)).execute(captorEventLife.getValue());
		
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false);
		
		verify(mockedMailSender, times(1)).sendMailAboutStart(executionFlow, event);
		
		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedTaskService, mockedScheduler, mockedCommand, mockedExecutionFlowService, mockedEventSourceService, mockedAuthQuery, mockedMailSender);
	}
	
	
	// flow will end immediately
	//  - there is NOT any task in the flow
	//  - in the maintenance window
	//  - there are tasks to be executed
	//  - no confirmation was needed
	//  - initiated BY_LOG
	//  - the execution flow is not frozen yet
	//  - there is no latest event	
	@Test
	public void testStartIt_successful_inMaintenanceWindow_thereAreNotTasksToBeExecuted() throws ReactionExecutionFlowException {
		MaintenanceWindow maintenanceWindow = new MaintenanceWindow(
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }",		
						mockedClock);
		EventSource eventSource = EventSource.builder().id(32434l)
				                      .maintenanceWindow(maintenanceWindow).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(13l)
															 .status(ExecutionFlowStatusEnum.VALID)
															 .executionTime(20).build();
		ErrorDetector errorDetector = ErrorDetector.builder().eventSource(eventSource)
				                                             .executionFlow(executionFlow )
				                                             .confirmationNeeded(false).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .initiatedBy(EventInitiatorEnum.BY_LOG)
				                     .eventSourceId(eventSource.getId())
				                     .executionFlow(executionFlow).build();
		// creating the expectations in mocks
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-27T23:15:30.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("Europe/London"));
		when(mockedTaskService.getTopByExecutionFlowEntityOrderByOrderAsc(any(ExecutionFlow.class))).thenReturn(null);
		when(mockedEventSourceService.getEventSourceAndFillProperties(event.getEventSourceId())).thenReturn(eventSource);
		Event savedEvent = Event.builder().build();
		when(mockedEventService.save(event)).thenReturn(savedEvent);
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.FINISHED);
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false)).thenReturn(null);		
		
		// call the method to be tested
		instance.startIt(event);
		
		// check & verify
		verify(mockedEventSourceService, times(1)).getEventSourceAndFillProperties(event.getEventSourceId());
		
		verify(mockedExecutionFlowService, times(1)).changeStatus(executionFlow, ExecutionFlowStatusEnum.FROZEN);
		
		ArgumentCaptor<ExecutionFlow> captorExecutionFlow = ArgumentCaptor.forClass(ExecutionFlow.class);
		verify(mockedTaskService,times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(captorExecutionFlow.capture());
		assertEquals(executionFlow.getId(), captorExecutionFlow.getValue().getId());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(2)).save(captorEvent.capture());
		List<Event> capturedEvents = captorEvent.getAllValues();
		// first save
		assertEquals(EventStatusEnum.STARTED, capturedEvents.get(0).getStatus());
		assertNotNull(capturedEvents.get(0).getStartDate());
		// second save
		assertEquals(EventStatusEnum.FINISHED, capturedEvents.get(1).getStatus());
		assertNotNull(capturedEvents.get(1).getEndDate());
		
		verify(mockedTaskService, times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(executionFlow);
		
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false);		

		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedTaskService, mockedScheduler, mockedCommand, mockedExecutionFlowService, mockedEventSourceService, mockedAuthQuery, mockedMailSender);
	}

	
	// Will be scheduled
	//  - NOT in the maintenance window
	//  - there are tasks to be executed
	//  - no confirmation was needed
	//  - initiated BY_LOG
	//  - the execution flow is not frozen yet
	//  - there is at least one task in the flow
	//  - there is one latest FINISHED event and it is outside the period between executions; it finished at 9:15, the window will open at 11:00, the timebetweenexec is 120min so it has to be schedulked by 11:15
	//  - there is one latest SCHEDULED event and it was scheduled 25 hours ago
	@Test
	public void testStartIt_successful_notInMaintenanceWindow_thereAreTasksToBeExecuted() throws ReactionExecutionFlowException, ParseException {
		MaintenanceWindow maintenanceWindow = new MaintenanceWindow(
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"11:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }",		
						mockedClock);
		EventSource eventSource = EventSource.builder().id(32434l)
				                      .maintenanceWindow(maintenanceWindow).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(13l)
															 .status(ExecutionFlowStatusEnum.VALID)
															 .timeBetweenExecutions(7200)
															 .executionTime(20).build();
		ErrorDetector errorDetector = ErrorDetector.builder().eventSource(eventSource)
				                                             .executionFlow(executionFlow )
				                                             .confirmationNeeded(false).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .initiatedBy(EventInitiatorEnum.BY_LOG)
				                     .executionFlow(executionFlow)
				                     .eventSourceId(eventSource.getId())
				                     //.startDate()
				                     .build();
		// creating the expectations in mocks
		when(mockedClock.instant()).thenReturn(buildCalendar("2017-01-27T09:15:30.000").toInstant());
		when(mockedClock.getZone()).thenReturn(ZoneId.systemDefault());
		EventLife savedEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		when(mockedEventSourceService.getEventSourceAndFillProperties(event.getEventSourceId())).thenReturn(eventSource);
		when(mockedEventService.save(event)).thenReturn(event);
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.FINISHED);
		Calendar endDate = GregorianCalendar.from(  ZonedDateTime.ofInstant(  buildCalendar("2017-01-27T09:15:30.000").toInstant(), ZoneId.systemDefault()  )  );
		Event latestFinishedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FINISHED)
				                                   .executionFlow(executionFlow).build();
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false)).thenReturn(latestFinishedEvent);		
		endDate = GregorianCalendar.from(  ZonedDateTime.ofInstant(  buildCalendar("2017-01-27T09:15:30.000").toInstant().minus(25, ChronoUnit.HOURS), ZoneId.systemDefault()  )  );
		Event latestScheduledEvent = Event.builder().startDate(endDate)
				                                    .status(EventStatusEnum.SCHEDULED)
				                                    .executionFlow(executionFlow).build();
		when(mockedEventService.getClosestScheduledEventsByExecutionFlow(event.getExecutionFlow())).thenReturn(latestScheduledEvent);
		
		// call the method to be tested
		instance.startIt(event);
		
		// check & verify
		verify(mockedEventSourceService, times(1)).getEventSourceAndFillProperties(event.getEventSourceId());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.SCHEDULED, captorEvent.getValue().getStatus());
		assertEquals("2017-01-27T11:15:30.000", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(captorEvent.getValue().getStartDate().getTime()) );
		
		verify(mockedEventService, times(1)).getClosestScheduledEventsByExecutionFlow(event.getExecutionFlow());
		
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false);
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(1)).save(captorEventLife.capture());
		assertEquals(event, captorEventLife.getValue().getEvent());
		assertEquals(EventStatusEnum.SCHEDULED, captorEventLife.getValue().getEventStatus());
		assertEquals(-1, captorEventLife.getValue().getOrder().intValue());
		
		verify(mockedScheduler, times(1)).schedule(event);
		
		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedTaskService, mockedScheduler, mockedCommand, mockedExecutionFlowService, mockedEventSourceService, mockedAuthQuery, mockedMailSender);
	}

	
	// won't be started but waiting for confirmation
	//  - confirmation is needed
	//  - in the maintenance window
	//  - there are tasks to be executed
	//  - initiated BY_LOG
	//  - the execution flow is not frozen yet
	//  - there is at least one task in the flow
	@Test
	public void testStartIt_successful_confirmationNeeded() throws ReactionExecutionFlowException {
		MaintenanceWindow maintenanceWindow = new MaintenanceWindow(
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }",		
						mockedClock);
		EventSource eventSource = EventSource.builder().id(32434l)
									  .maintenanceWindow(maintenanceWindow).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(13l)
												 .status(ExecutionFlowStatusEnum.VALID)
												 .accessGroups(Arrays.asList("DBA", "Middleware"))
												 .name("aaa")
												 .executionTime(20).build();
		ErrorDetector errorDetector = ErrorDetector.builder().eventSource(eventSource)
		                                                     .executionFlow(executionFlow )
		                                                     .confirmationNeeded(true).build();
		Event event = Event.builder().errorDetector(errorDetector)
		               .initiatedBy(EventInitiatorEnum.BY_LOG)
		               .eventSourceId(eventSource.getId())
		               .executionFlow(executionFlow).build();
		// creating the expectations in mocks
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-27T09:15:30.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("Europe/London"));
		EventLife savedEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		when(mockedEventSourceService.getEventSourceAndFillProperties(event.getEventSourceId())).thenReturn(eventSource);
		when(mockedEventService.save(event)).thenReturn(event);
		List<String> emailAddresses = Collections.singletonList("a@b.c");
		when(mockedAuthQuery.getEmailsByPermissionsAndAccesGroups(any(List.class), any(List.class))).thenReturn(emailAddresses);
		when(mockedExecutionFlowService.getExecutionFlow(executionFlow.getId())).thenReturn(executionFlow);
		
		// call the method to be tested
		instance.startIt(event);
		
		// check & verify
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.CONFIRMATION_NEEDED, captorEvent.getValue().getStatus());
		assertNotNull(captorEvent.getValue().getStartDate().getTime() );
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(1)).save(captorEventLife.capture());
		assertEquals(event, captorEventLife.getValue().getEvent());
		assertEquals(EventStatusEnum.CONFIRMATION_NEEDED, captorEventLife.getValue().getEventStatus());
		assertEquals(-2, captorEventLife.getValue().getOrder().intValue());
		
		List<String> permissions = Collections.singletonList(PermissionEnum.CAN_USE_APPROVAL.getCodename());
		List<String> accessGroups = Arrays.asList("DBA", "Middleware");
		verify(mockedAuthQuery, times(1)).getEmailsByPermissionsAndAccesGroups(permissions , accessGroups );
		
		verify(mockedMailSender, times(1)).sendMail(eq(Arrays.asList("a@b.c")), 
				                                    eq("Reaction Engine - Confirmation needed for the flow '" + executionFlow.getName() + "'"), 
				                                    any(Event.class), 
				                                    any(), 
				                                    eq("confirmationNeededMailTemplate"));
		
		verify(mockedExecutionFlowService, times(1)).getExecutionFlow(executionFlow.getId());

		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedTaskService, mockedScheduler, mockedCommand, mockedExecutionFlowService, mockedEventSourceService, mockedAuthQuery, mockedMailSender);
	}
	
	
	// flow will be started
	//  - status is SCHEDULED and initiated BY_SCHEDULER
	//  - NOT in the maintenance window
	//  - there are tasks to be executed
	//  - the execution flow is not frozen yet
	//  - there is at least one task in the flow
	//  - the exec time of the execution flow is set
	//  - there is one latest FINISHED event that ended 1 min ago (timebetweenexecution is 30)
	@Test
	public void testStartIt_successful_inMaintenanceWindow_statusIsScheduled_ByScheduler() throws ReactionExecutionFlowException {
		MaintenanceWindow maintenanceWindow = new MaintenanceWindow(
						"{ \"Sun\": [\"14:00-04:00\"]," +
						"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
						"  \"Tue\": [\"23:00-04:00\"]," +
						"  \"Wed\": [\"23:00-04:00\"]," +
						"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
						"  \"Fri\": [\"23:00-04:00\"]," +
						"  \"Sat\": [\"20:00-04:00\"] }",		
						mockedClock);
		EventSource eventSource = EventSource.builder().id(32434l)
				                      .maintenanceWindow(maintenanceWindow).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(13l)
															 .status(ExecutionFlowStatusEnum.VALID)
															 .timeBetweenExecutions(30)
															 .executionTime(20).build();
		ErrorDetector errorDetector = ErrorDetector.builder().eventSource(eventSource)
				                                             .executionFlow(executionFlow )
				                                             .confirmationNeeded(true).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .initiatedBy(EventInitiatorEnum.BY_SCHEDULER)
				                     .status(EventStatusEnum.SCHEDULED)
				                     .eventSourceId(eventSource.getId())
				                     .executionFlow(executionFlow).build();
		// creating the expectations in mocks
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-27T09:15:30.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("Europe/London"));
		when(mockedTaskService.getTopByExecutionFlowEntityOrderByOrderAsc(any(ExecutionFlow.class))).thenReturn(mockedCommand);
		EventLife savedEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		doNothing().when(mockedCommand).execute(any(EventLife.class));
		when(mockedEventSourceService.getEventSourceAndFillProperties(event.getEventSourceId())).thenReturn(eventSource);
		when(mockedExecutionFlowService.getExecutionFlow(executionFlow.getId())).thenReturn(executionFlow);
		when(mockedEventService.save(event)).thenReturn(event);
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.FINISHED);
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MINUTE, -1);
		Event latestFinishedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FINISHED)
				                                   .executionFlow(executionFlow).build();
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false)).thenReturn(latestFinishedEvent);
		when(mockedErrorDetectorService.getErrorDetectorById(event.getErrorDetector().getId())).thenReturn(errorDetector);
		when(mockedEventMachineService.isSameExecutionFlowNotOperated(event, latestFinishedEvent)).thenReturn(true);
		
		// call the method to be tested
		instance.startIt(event);
		
		// check & verify
		verify(mockedEventSourceService, times(1)).getEventSourceAndFillProperties(event.getEventSourceId());
		
		verify(mockedExecutionFlowService, times(1)).changeStatus(executionFlow, ExecutionFlowStatusEnum.FROZEN);
		verify(mockedExecutionFlowService, times(1)).getExecutionFlow(executionFlow.getId());
				
		ArgumentCaptor<ExecutionFlow> captorExecutionFlow = ArgumentCaptor.forClass(ExecutionFlow.class);
		verify(mockedTaskService,times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(captorExecutionFlow.capture());
		assertEquals(executionFlow.getId(), captorExecutionFlow.getValue().getId());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.STARTED, captorEvent.getValue().getStatus());
		assertNotNull(captorEvent.getValue().getStartDate());
		
		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false);

		verify(mockedTaskService, times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(executionFlow);
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(1)).save(captorEventLife.capture());
		assertEquals(event, captorEventLife.getValue().getEvent());
		assertEquals(mockedCommand, captorEventLife.getValue().getTask());
		assertEquals(EventStatusEnum.STARTED, captorEventLife.getValue().getEventStatus());
		assertEquals(0, captorEventLife.getValue().getOrder().intValue());
		
		verify(mockedCommand, times(1)).execute(captorEventLife.getValue());

		verify(mockedErrorDetectorService, times(1)).getErrorDetectorById(event.getErrorDetector().getId());
		
		verify(mockedEventMachineService, times(1)).isSameExecutionFlowNotOperated(event, latestFinishedEvent);

		verify(mockedMailSender, times(1)).sendMailAboutStart(executionFlow, event);
		
		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedTaskService, mockedScheduler, mockedCommand, mockedExecutionFlowService, mockedEventSourceService, mockedAuthQuery, mockedMailSender);
	}

	
	// flow will be started
	//  - status is CONFIRMED_AND_FORCED_START and initiated BY_LOG
	//  - NOT in the maintenance window
	//  - there are tasks to be executed
	//  - confirmation is needed
	//  - the execution flow is frozen already
	//  - there is at least one task in the flow
	//  - the exec time of the execution flow is set
	//  - there is one FAILED latest event
	@Test
	public void testStartIt_successful_inMaintenanceWindow_statusIsCONFIRMED_AND_FORCED_START() throws ReactionExecutionFlowException {
		MaintenanceWindow maintenanceWindow = new MaintenanceWindow(
						"{ \"Sun\": [\"14:00-04:00\"]," +
						"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
						"  \"Tue\": [\"23:00-04:00\"]," +
						"  \"Wed\": [\"23:00-04:00\"]," +
						"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
						"  \"Fri\": [\"23:00-04:00\"]," +
						"  \"Sat\": [\"20:00-04:00\"] }",		
						mockedClock);
		EventSource eventSource = EventSource.builder().id(32434l)
				                      .maintenanceWindow(maintenanceWindow).build();
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(13l)
															 .status(ExecutionFlowStatusEnum.FROZEN)
															 .executionTime(20).build();
		ErrorDetector errorDetector = ErrorDetector.builder().eventSource(eventSource)
				                                             .executionFlow(executionFlow )
				                                             .confirmationNeeded(true).build();
		Event event = Event.builder().errorDetector(errorDetector)
				                     .initiatedBy(EventInitiatorEnum.BY_SCHEDULER)
				                     .status(EventStatusEnum.CONFIRMED_AND_FORCED_START)
				                     .eventSourceId(eventSource.getId())
				                     .executionFlow(executionFlow).build();
		// creating the expectations in mocks
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-27T09:15:30.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("Europe/London"));
		when(mockedTaskService.getTopByExecutionFlowEntityOrderByOrderAsc(any(ExecutionFlow.class))).thenReturn(mockedCommand);
		EventLife savedEventLife = EventLife.builder().build();
		when(mockedEventLifeService.save(any(EventLife.class))).thenReturn(savedEventLife);
		doNothing().when(mockedCommand).execute(any(EventLife.class));
		when(mockedEventSourceService.getEventSourceAndFillProperties(event.getEventSourceId())).thenReturn(eventSource);
		when(mockedExecutionFlowService.getExecutionFlow(executionFlow.getId())).thenReturn(executionFlow);
		when(mockedEventService.save(event)).thenReturn(event);
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.FINISHED);
		Calendar endDate = Calendar.getInstance();
		endDate.add(Calendar.MINUTE, -120);
		Event latestFinishedEvent = Event.builder().endDate(endDate)
				                                   .status(EventStatusEnum.FAILED)
				                                   .executionFlow(executionFlow).build();
		when(mockedEventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false)).thenReturn(latestFinishedEvent);
		when(mockedErrorDetectorService.getErrorDetectorById(event.getErrorDetector().getId())).thenReturn(errorDetector);
		
		// call the method to be tested
		instance.startIt(event);
		
		// check & verify
		verify(mockedEventSourceService, times(1)).getEventSourceAndFillProperties(event.getEventSourceId());
		
		verify(mockedExecutionFlowService, times(1)).getExecutionFlow(executionFlow.getId());
				
		ArgumentCaptor<ExecutionFlow> captorExecutionFlow = ArgumentCaptor.forClass(ExecutionFlow.class);
		verify(mockedTaskService,times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(captorExecutionFlow.capture());
		assertEquals(executionFlow.getId(), captorExecutionFlow.getValue().getId());
		
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService,times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.STARTED, captorEvent.getValue().getStatus());
		assertNotNull(captorEvent.getValue().getStartDate());

		verify(mockedEventService, times(1)).getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false);
		
		verify(mockedTaskService, times(1)).getTopByExecutionFlowEntityOrderByOrderAsc(executionFlow);
		
		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(1)).save(captorEventLife.capture());
		assertEquals(event, captorEventLife.getValue().getEvent());
		assertEquals(mockedCommand, captorEventLife.getValue().getTask());
		assertEquals(EventStatusEnum.STARTED, captorEventLife.getValue().getEventStatus());
		assertEquals(0, captorEventLife.getValue().getOrder().intValue());
		
		verify(mockedCommand, times(1)).execute(captorEventLife.getValue());
		
		verify(mockedMailSender, times(1)).sendMailAboutStart(executionFlow, event);
		
		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedTaskService, mockedScheduler, mockedCommand, mockedExecutionFlowService, mockedEventSourceService, mockedAuthQuery, mockedMailSender);
	}

	
	private Calendar buildCalendar(String dateText) throws ParseException {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(dateText));
		return calendar;
	}
	
	
	@After
	public void clearMocks() {
		Mockito.reset(mockedEventService);
		Mockito.reset(mockedEventLifeService);
		Mockito.reset(mockedTaskService);
		Mockito.reset(mockedScheduler);
		Mockito.reset(mockedCommand);
		Mockito.reset(mockedExecutionFlowService);
		Mockito.reset(mockedEventSourceService);
		Mockito.reset(mockedAuthQuery);
		Mockito.reset(mockedMailSender);
	}

	
	@Configuration
    @Import(DefaultExecutionFlowMachineServiceImpl.class)
    static class Config {
    }

}
