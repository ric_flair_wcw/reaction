package org.reaction.engine.expression;

import org.junit.Before;
import org.junit.Test;
import org.reaction.common.exception.ReactionExecutionFlowException;

import static org.junit.Assert.assertTrue;
import static org.reaction.engine.expression.ExpressionInterpreter.VARIABLE;

public class ExpressionInterpreterTest {

	private ExpressionInterpreter expressionInterpreter;
	
	
	@Before
	public void setUp() {
		expressionInterpreter = new ExpressionInterpreter();
	}
	
	
	@Test
	public void testEvaluate_stringEquals_or_true() throws ReactionExecutionFlowException {
		String value = "kanya2";
		String expression = VARIABLE + ".equals(\"fecske\") || " + VARIABLE + ".equals(\"" + value + "\")";
		
		// call the method to be tested
		boolean result = expressionInterpreter.evaluate(value, expression);
		
		// check & verify
		assertTrue(result);
	}
	
	@Test
	public void testEvaluate_stringEquals_single_true() throws ReactionExecutionFlowException {
		String value = "OK";
		String expression = "$COMMANDOUTPUT.equals(\"OK\")";
		
		// call the method to be tested
		boolean result = expressionInterpreter.evaluate(value, expression);
		
		// check & verify
		assertTrue(result);
	}
		
}