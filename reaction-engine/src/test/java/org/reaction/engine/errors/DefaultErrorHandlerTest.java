package org.reaction.engine.errors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.ErrorDetector;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.persistence.service.ExecutionFlowService;
import org.reaction.engine.persistence.service.ScheduledExecutionFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
public class DefaultErrorHandlerTest {

	@Autowired
	private DefaultErrorHandler instance;
	
	@MockBean
	private EventService mockedEventService;
	@MockBean
	private EventLifeService mockedEventLifeService;
	@MockBean
	private ReactionMailSender mockedReactionMailSender;
	@MockBean
	private ExecutionFlowService mockedExecutionFlowService;
	@MockBean
	private AuthQuery mockedAuthQuery;
	@MockBean
	private ScheduledExecutionFlowService scheduledExecutionFlowService;
	
	
	@Test
	public void testHandleError() {
		// creating the expectations in mocks
		//    for mockedEventService	
		List<String> recipients	= Collections.singletonList("a@b.c");
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(231243l)
															 .name("sejsefkjl")
															 .errorMailSendingRecipients(recipients).build();
		ErrorDetector errorDetector = ErrorDetector.builder().executionFlow(executionFlow ).build();
		Event event = Event.builder().identifier("1")
									 .errorDetector(errorDetector )
									 .executionFlow(executionFlow).build();
		Event savedEvent = Event.builder().identifier("1")
				                          .id(0l)
				                          .executionFlow(executionFlow)
				                          .errorDetector(errorDetector ).build();
		when(mockedEventService.save(event)).thenReturn(savedEvent);
		when(mockedEventService.getEventByIdentifier(event.getIdentifier())).thenReturn(event);
		//    for mockedEventService		
		EventLife eventLife = EventLife.builder().build();
		EventLife savedEventLife = EventLife.builder().id(0l).build();
		when(mockedEventLifeService.save(eventLife)).thenReturn(savedEventLife);
		//    for mockedExecutionFlowService
		when(mockedExecutionFlowService.getExecutionFlow(executionFlow.getId())).thenReturn(executionFlow);
		
		// call the method to be tested
		Throwable t = new RuntimeException("Something must have happened!");
		instance.sendMail = true;
		instance.handleError(event, t);
		
		// check & verify
		ArgumentCaptor<Event> captorEvent = ArgumentCaptor.forClass(Event.class);
		verify(mockedEventService, times(1)).save(captorEvent.capture());
		assertEquals(EventStatusEnum.FAILED, captorEvent.getValue().getStatus());
		assertNotNull(captorEvent.getValue().getEndDate());

		ArgumentCaptor<EventLife> captorEventLife = ArgumentCaptor.forClass(EventLife.class);
		verify(mockedEventLifeService, times(1)).save(captorEventLife.capture());
		assertEquals(EventStatusEnum.INTERNAL_ERROR, captorEventLife.getValue().getEventStatus());
		assertEquals(savedEvent, captorEventLife.getValue().getEvent());
		assertEquals(9999, captorEventLife.getValue().getOrder().intValue());
		assertEquals(t.getLocalizedMessage(), captorEventLife.getValue().getErrorMessage());
		
		verify(mockedEventService, times(1)).getEventByIdentifier(event.getIdentifier());
		
		verify(mockedReactionMailSender, times(1)).sendMail(eq(recipients), 
				                                            eq("Reaction Engine - Unknown error occured when running the execution flow!"), 
				                                            any(), 
				                                            any(), 
				                                            eq("unknownErrorMailTemplate"));
		
		verify(mockedExecutionFlowService, times(1)).getExecutionFlow(executionFlow.getId());
		
		verifyNoMoreInteractions(mockedEventService, mockedEventLifeService, mockedReactionMailSender, mockedExecutionFlowService);
	}
	

	@Test
	public void testHandleErrorInCommand() {
		// TODO ...
	}

    @Configuration
    @Import(DefaultErrorHandler.class)
    static class Config {
    }
}
