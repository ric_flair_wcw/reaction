-- initializing the AUTH tables

CREATE TABLE django_admin_log (
  id int(11) NOT NULL,
  action_time datetime NOT NULL,
  object_id longtext,
  object_repr varchar(200) NOT NULL,
  action_flag smallint(5) unsigned NOT NULL,
  change_message longtext NOT NULL,
  content_type_id int(11) DEFAULT NULL,
  user_id int(11) NOT NULL);


CREATE TABLE auth_permission (
  id int(11) NOT NULL,
  name varchar(255) NOT NULL,
  content_type_id int(11) NOT NULL,
  codename varchar(100) NOT NULL);
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (1,'Can add permission',1,'add_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (2,'Can change permission',1,'change_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (3,'Can delete permission',1,'delete_permission');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (4,'Can add group',2,'add_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (5,'Can change group',2,'change_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (6,'Can delete group',2,'delete_group');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (7,'Can add user',3,'add_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (8,'Can change user',3,'change_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (9,'Can delete user',3,'delete_user');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (10,'Can add log entry',5,'add_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (11,'Can change log entry',5,'change_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (12,'Can delete log entry',5,'delete_logentry');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (13,'Can add content type',6,'add_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (14,'Can change content type',6,'change_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (15,'Can delete content type',6,'delete_contenttype');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (16,'Can add session',7,'add_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (17,'Can change session',7,'change_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (18,'Can delete session',7,'delete_session');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (19,'Can use Monitoring',1,'can_use_monitoring');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (20,'Can use Event Source adminstration',1,'can_use_admin_event_source');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (21,'Can use Execution Flow administration',1,'can_use_admin_execution_flow');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (22,'Can use Errordetector administration',1,'can_use_admin_errordetector');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (23,'Can use Scheduler',1,'can_use_scheduler');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (24,'Can use Executor',1,'can_use_executor');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (25,'Can use Approval',1,'can_use_approval');
INSERT INTO auth_permission (id, name, content_type_id, codename) VALUES (26,'Can use Statistics',1,'can_use_statistics');


CREATE TABLE auth_group (
  id int(11) NOT NULL,
  name varchar(80) NOT NULL);


CREATE TABLE auth_group_permissions (
  id int(11) NOT NULL,
  group_id int(11) NOT NULL,
  permission_id int(11) NOT NULL);


CREATE TABLE django_session (
  session_key varchar(40) NOT NULL,
  session_data longtext NOT NULL,
  expire_date datetime NOT NULL);
--INSERT INTO django_session (session_key, session_data, expire_date) VALUES ('iu5cdixygvx3b366h6xgda2kuk1rdhxn','YTY5MGQwYWNkNDk4MWQ5MWE1NTRjZGFmODViODhlNzhlNmRmN2VhZDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3NjZmNjNlNjRiYjY2ZWNlY2IzNjZmZjA3YjJlMzBlMGE4MGEyMDNmIiwiX2NzcmZ0b2tlbiI6Im1MdnB3Z0dhc0hGdVpXaTdudUdiWlh5YjVaSjVtTW9IZEJtNWRZN1l2TVBQVnpyOVBocXFQS0F5eUFqQ0lyNWgifQ==','2017-07-12 13:12:31');


CREATE TABLE auth_user_groups (
  id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  group_id int(11) NOT NULL);


CREATE TABLE auth_user_user_permissions (
  id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  permission_id int(11) NOT NULL);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (2,3,19);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (3,3,20);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (4,3,21);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (5,3,22);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (6,3,23);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (7,3,24);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (8,3,25);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (9,3,26);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (10,4,19);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (11,5,20);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (12,5,21);
INSERT INTO auth_user_user_permissions (id, user_id, permission_id) VALUES (13,5,22);


CREATE TABLE django_migrations (
  id int(11) NOT NULL,
  app varchar(255) NOT NULL,
  name varchar(255) NOT NULL,
  applied datetime NOT NULL);


CREATE TABLE auth_user (
  id int(11) NOT NULL,
  password varchar(128) NOT NULL,
  last_login datetime DEFAULT NULL,
  is_superuser tinyint(1) NOT NULL,
  username varchar(150) NOT NULL,
  first_name varchar(30) NOT NULL,
  last_name varchar(30) NOT NULL,
  email varchar(254) NOT NULL,
  is_staff tinyint(1) NOT NULL,
  is_active tinyint(1) NOT NULL,
  date_joined datetime NOT NULL);
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (1,'pbkdf2_sha256$36000$FnrPRsX4ZSFe$iSDpdmNChY45dDuwqFcGLOhZDud9ZWlILRyFy3cwMyU=','2017-06-28 13:06:23',1,'admin','','','ric.flair.wcw@gmail.com',1,1,'2017-06-20 13:02:14');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (3,'pbkdf2_sha256$36000$rOpm97qpHsy4$NFKCCfMmve1Z6c1U/grizJ6TyQck3bE/Fe+Gy3Gi+c8=','2017-06-22 09:43:32',0,'vikhor','','','ric.flair.wcw@gmail.com',0,1,'2017-06-20 13:51:20');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (4,'pbkdf2_sha256$36000$AguMfMCxNW56$vO9vHNYWr28X6xyu8q5qaosucg3J5baqYNPI5thypLo=',NULL,0,'reaction_monitor','','','ric.flair.wcw@gmail.com',0,1,'2017-06-28 13:07:44');
INSERT INTO auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) VALUES (5,'pbkdf2_sha256$36000$x0pQrQqQbBfW$A4ltp3z3yB9P75cX7C/kdd9ONzCGqq8k1irJj5xGyIg=',NULL,0,'reaction_admin','','','ric.flair.wcw@gmail.com',0,1,'2017-06-28 13:11:59');


CREATE TABLE django_content_type (
  id int(11) NOT NULL,
  app_label varchar(100) NOT NULL,
  model varchar(100) NOT NULL);
INSERT INTO django_content_type (id, app_label, model) VALUES (1,'auth','permission');
INSERT INTO django_content_type (id, app_label, model) VALUES (2,'auth','group');
INSERT INTO django_content_type (id, app_label, model) VALUES (3,'auth','user');
INSERT INTO django_content_type (id, app_label, model) VALUES (4,'common','profile');
INSERT INTO django_content_type (id, app_label, model) VALUES (5,'admin','logentry');
INSERT INTO django_content_type (id, app_label, model) VALUES (6,'contenttypes','contenttype');
INSERT INTO django_content_type (id, app_label, model) VALUES (7,'sessions','session');
INSERT INTO django_content_type (id, app_label, model) VALUES (8,'admin_execution_flow','executionflow');
INSERT INTO django_content_type (id, app_label, model) VALUES (9,'admin_execution_flow','task');
INSERT INTO django_content_type (id, app_label, model) VALUES (10,'admin_errordetector','errordetector');
INSERT INTO django_content_type (id, app_label, model) VALUES (11,'admin_event_source','event_source');
INSERT INTO django_content_type (id, app_label, model) VALUES (12,'scheduler','scheduledexecutionflow');
INSERT INTO django_content_type (id, app_label, model) VALUES (13,'monitoring','event');
INSERT INTO django_content_type (id, app_label, model) VALUES (14,'monitoring','eventlife');
INSERT INTO django_content_type (id, app_label, model) VALUES (15,'monitoring','commandstobeexecuted');


CREATE TABLE profile (
  id int(11) NOT NULL,
  access_groups varchar(500) NOT NULL,
  user_id int(11) NOT NULL);
INSERT INTO profile (id, access_groups, user_id) VALUES (2,'DBA, Middleware',3);
INSERT INTO profile (id, access_groups, user_id) VALUES (3,'Middleware, DBA, UNIX, Microsoft Technologies',4);
INSERT INTO profile (id, access_groups, user_id) VALUES (4,'DBA, Middleware, Microsoft Technologies, UNIX',5);