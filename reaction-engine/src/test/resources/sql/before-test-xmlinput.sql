INSERT INTO EVENT_SOURCE_TYPE (id, name, code, data_type, namespaces, path_to_identifiers, ENFORCE_SSL, AUTHENTICATION_TYPE, SECRET, USER) values
	(0, 'Acme Monitor', 'acmemonitor', 'XML', STRINGDECODE('soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\nns=\"http://acme.com/ns\"'), 
	'[{"name": "host","desc": "Host","path": "/soap:Envelope/soap:Body/ns:request/ns:event/ns:host","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "source","desc": "Source","path": "/soap:Envelope/soap:Body/ns:request/ns:event/ns:source","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "message","desc": "Message","path": "/soap:Envelope/soap:Body/ns:request/ns:event/ns:message","source":"PAYLOAD","output":"ERROR_DETECTOR"}]',
	false, 'NONE', null, null);
INSERT INTO EVENT_SOURCE_TYPE (code, name, data_type) VALUES ('REACTION-WORKER', 'Reaction Worker', 'NONE');

INSERT INTO event_source (id, name, host, log_path, log_header_pattern, log_header_pattern_enabled, description, type, log_level, maintenance_window, parent_id) VALUES
	(1,'Hermes',NULL,NULL,NULL,false,'Billing application','GROUP',null,'{"Mon": ["02:50-02:51"], "Tue": ["02:50-02:51"], "Wed": ["02:50-02:51"], "Thu": ["02:50-02:51"], "Fri": ["02:50-02:51"], "Sat": ["02:50-02:51"], "Sun": ["02:50-02:51"]}',NULL);
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, log_header_pattern_enabled, description, type, log_level, maintenance_window, parent_id) VALUES
	(4,'Hermes SOA servers',NULL,'/local/vikhor/container/acme/wildfly-10.1.0.Final/standalone/log/server.log','[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~UNKNOWN:a-z]] ([~UNKNOWN:.])',true,'','GROUP',NULL,'{}',1);
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, description, type, log_level, maintenance_window, parent_id) VALUES
	(2,'Hermes SOA server 0','urmlprod20',NULL,NULL,'','ITEM',NULL,'{}',4);
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, description, type, log_level, maintenance_window, parent_id,EVENT_SOURCE_TYPE_ID,IDENTIFIERS) VALUES
	(3,'Hermes SOA server 1','urmlprod21',NULL,NULL,'','ITEM',NULL,'{}',4,0,'{"message": "/home/vikhor/work/reaction/tmp/reaction-engine-standalone-1.1/logs/reaction-engine.log", "host": "localhost"}');
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, description, type, log_level, maintenance_window, parent_id) VALUES
	(5,'Hermes web app','urmlprod30',NULL,NULL,'','ITEM',NULL,'{}',1);

INSERT INTO execution_flow (id, name, time_between_exec, execution_time, status, access_group, start_mail_sending_recipients, error_mail_sending_recipients, hosts) VALUES 
	(1,'stop Hermes DB and app servers',NULL,NULL,'VALID','DBA,Middleware','reaction.engine.sup@gmail.com','reaction.engine.sup@gmail.com','urmlprod20, urmlprod21, urmlprod60');

INSERT INTO error_detector (id, name, message_pattern, activated, execution_flow_id, event_source_id, multiple_events_cnt, multiple_events_timefr, confirmation_needed, identifiers) VALUES
	(1,'restart Hermes if nullpointerexception occurs','.*java\.lang\.NullPointerException.*',1,1,1,NULL,NULL,1,'{"message-PAYLOAD": ".*NullPointerException.*"}');
	