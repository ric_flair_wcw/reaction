INSERT INTO event_source_type (id, name, code, data_type, namespaces, path_to_identifiers, ENFORCE_SSL, AUTHENTICATION_TYPE, SECRET, USER) values
	(0, 'Splunk 8', 'splunk8', 'JSON', null,
	'[{"name": "host","desc": "Host","path": "$.result.host","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "source","desc": "Source","path": "$.result.source","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "message","desc": "Message","path": "$.result._raw","source":"PAYLOAD","output":"ERROR_DETECTOR"}]', false, 'NONE', null, null);
INSERT INTO event_source_type (id, code, name, data_type) VALUES (1, 'REACTION-WORKER', 'Reaction Worker', 'NONE');
INSERT INTO event_source_type (id, name, code, data_type, namespaces, path_to_identifiers, ENFORCE_SSL, AUTHENTICATION_TYPE, SECRET, USER) values
	(2, 'Splunk 8 BASIC AUTH', 'splunk8_BASIC_AUTH', 'JSON', null, '[{"name": "host","desc": "Host","path": "$.result.host","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "source","desc": "Source","path": "$.result.source","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "message","desc": "Message","path": "$.result._raw","source":"PAYLOAD","output":"ERROR_DETECTOR"}]',
	false, 'BASIC_AUTH', 'ric_flair', 'O37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==');
INSERT INTO event_source_type (id, name, code, data_type, namespaces, path_to_identifiers, ENFORCE_SSL, AUTHENTICATION_TYPE, SECRET, USER) values
	(3, 'Splunk 8 SECRET', 'splunk8_SECRET', 'JSON', null, '[{"name": "host","desc": "Host","path": "$.result.host","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "source","desc": "Source","path": "$.result.source","source":"PAYLOAD","output":"EVENT_SOURCE"},{"name": "message","desc": "Message","path": "$.result._raw","source":"PAYLOAD","output":"ERROR_DETECTOR"}]',
	false, 'SECRET', 'O37g9wDaKgc01aDNf8CEuWTYVfx/YRzoZ9rYejp2Q8ckcc5BjYsYLJFhWwdC1sqE5mVK8h0BaBcn7mqnUU52Q==', null);

INSERT INTO event_source (id, name, host, log_path, log_header_pattern, log_header_pattern_enabled, description, type, log_level,
    maintenance_window, parent_id) VALUES
	(1,'Hermes',NULL,NULL,NULL,false,'Billing application','GROUP',null,
	'{"Mon": ["02:50-02:51"], "Tue": ["02:50-02:51"], "Wed": ["02:50-02:51"], "Thu": ["02:50-02:51"], "Fri": ["02:50-02:51"], "Sat": ["02:50-02:51"], "Sun": ["02:50-02:51"]}',NULL);
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, log_header_pattern_enabled, description, type, log_level, maintenance_window, parent_id,EVENT_SOURCE_TYPE_ID,IDENTIFIERS) VALUES
	(4,'Hermes SOA servers',NULL,'/local/vikhor/container/acme/wildfly-10.1.0.Final/standalone/log/server.log','[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~UNKNOWN:a-z]] ([~UNKNOWN:.])',false,'','GROUP',NULL,
	'{}',1,0,'{"source": ".*reaction-engine.*", "host": ".*local.*"}');
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, description, type, log_level, maintenance_window, parent_id, EVENT_SOURCE_TYPE_ID) VALUES
	(2,'Hermes SOA server 0','urmlprod20',NULL,NULL,'','ITEM',NULL,
	'{}',4, 1);
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, description, type, log_level, maintenance_window, parent_id) VALUES
	(3,'Hermes SOA server 1','urmlprod21',NULL,NULL,'','ITEM',NULL,
	'{}',4);
INSERT INTO event_source (id, name, host, log_path, log_header_pattern, description, type, log_level, maintenance_window, parent_id) VALUES
	(5,'Hermes web app','urmlprod30',NULL,NULL,'','ITEM',NULL,
	'{}',1);

INSERT INTO execution_flow (id, name, time_between_exec, execution_time, status, access_group, start_mail_sending_recipients, error_mail_sending_recipients, hosts) VALUES 
	(1,'stop Hermes DB and app servers',NULL,NULL,'VALID','DBA,Middleware','reaction.engine.sup@gmail.com','reaction.engine.sup@gmail.com','urmlprod20, urmlprod21, urmlprod60');

INSERT INTO error_detector (id, name, message_pattern, activated, execution_flow_id, event_source_id, multiple_events_cnt, multiple_events_timefr, confirmation_needed, identifiers) VALUES
	(1,'restart Hermes if nullpointerexception occurs',NULL,1,1,1,NULL,NULL,1, '{"message-PAYLOAD": ".*NullPointerException.*"}');
	