package org.reaction.engine.mail;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeMessage;

import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.persistence.service.EventSourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.util.StringUtils;
import org.thymeleaf.context.Context;


public class ReactionMailSender {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReactionMailSender.class);
	
	@Autowired
	private ApplicationContext applicationContext;
    @Autowired
    public JavaMailSender emailSender;
	@Autowired
	private AuthQuery authQuery;
	@Autowired
	private EventSourceService eventSourceService;

	@Value("${reaction.management_web_app.endpoint}")
	private String managementWebAppEndpoint;
	@Value("${reaction.mail.template_folder}")
	private String templateFolder;
    
    
	@PostConstruct
	public void init() throws ReactionConfigurationException {
		// check if the 'templateFolder' setting is not empty -> i.e. external mail templates have to be used
		if (!StringUtils.isEmpty(templateFolder)) {
			// check if all the templates are in the folder: confirmationNeededMailTemplate.html, externalErrorMailTemplate.html, startedFlowMailTemplate.html, unknownErrorMailTemplate.html
			String[] fileName = {"confirmationNeededMailTemplate.html", "externalErrorMailTemplate.html", "startedFlowMailTemplate.html", "unknownErrorMailTemplate.html"};
			List<File> nonExistingTemplateFiles = Arrays.stream(fileName).map(n -> new File(templateFolder + "/" + n))
			                                                             .filter(f -> !f.exists())
			                                                             .collect(Collectors.toList());
			if (!nonExistingTemplateFiles.isEmpty()) {
				throw new ReactionConfigurationException("The setting 'reaction.mail.templateFolder' is set but the folder doesn't contain the following templates: " + 
						nonExistingTemplateFiles.stream().map(f -> f.getName()).collect(Collectors.joining(", ")));
			}
		}
	}
	
	public void sendMail(List<String> r, String subject, Event event, Context context, String emailTemplate) {
		if (r == null || r.size() == 0) {
			LOGGER.warn("An email should be sent but no recipients are specified!\n\tSubject:{}\n\tEvent:{}", subject, event);
			return;
		}
		String[] recipients = r.toArray(new String[0]);
		try {
	        // building the message
			ErrorMailBuilder errorMailBuilder = applicationContext.getBean("errorMailBuilder", ErrorMailBuilder.class);
			MimeMessage mimeMessage = errorMailBuilder.subject(subject)
			                                          .mimeMessage( this.emailSender.createMimeMessage() )
			                                          .from(Constants.MAIL_FROM)
			                                          .context(context)
			                                          .emailTemplate(emailTemplate)
			                                          .templateFolder(templateFolder)
			                                          .to(recipients).build();
			// send it
	        this.emailSender.send(mimeMessage);
	        LOGGER.debug("Email sent to {}", Arrays.toString(recipients));
		} catch(Exception e) {
			LOGGER.error("While sending the following mail an exception occurred!\n\tSubject:{}\n\tTo:{}\n\tEvent:{}", subject, recipients, event);
			LOGGER.error("",e);
		}
	}

	
	public void sendMailAboutStart(ExecutionFlow executionFlow, Event event) {
		// only send the notification about the start if it was started by the log
		if (event.getInitiatedBy().equals(EventInitiatorEnum.BY_LOG)) {
			LOGGER.debug("A mail will be sent to let the administrators know that a flow is started. event identifier: {}", event.getIdentifier());
			try {
				// setting the values for the template
		        final Context context = new Context(Locale.UK);
		        context.setVariable("event_identifier", event.getIdentifier());
		        context.setVariable("executionFlow_name", executionFlow.getName());
		        context.setVariable("event_id", event.getId());
		        context.setVariable("web_gui_url", managementWebAppEndpoint);
		        String host = event.getEventSourceId() == null ? null : eventSourceService.getEventSourceAndFillProperties(event.getEventSourceId()).getHost();
		        context.setVariable("host", host);
		        context.setVariable("message", event.getMessage());
		        // getting the recipients to send the mail to
		        List<String> recipients = null;
		        if (executionFlow.getStartMailSendingRecipients() != null && executionFlow.getStartMailSendingRecipients().size() > 0) {
		        	// if the start mail sending is checked in the get recipients from there
		        	recipients = executionFlow.getStartMailSendingRecipients();
		        } else {
		        	// otherwise the recipients are the members of the access groups assigned to the flow
		        	recipients = authQuery.getEmailsByAccesGroups(executionFlow.getAccessGroups());
		        }
		        LOGGER.debug("The recipients: '{}'", recipients.stream().collect(Collectors.joining(", ")));
				// send the mail
				sendMail(recipients, 
						 "Reaction Engine - The execution flow '" + executionFlow.getName() + "' has just started!",
						 event,
						 context,
						 "startedFlowMailTemplate");
			} catch(Exception e) {
				LOGGER.error("Error occurred when trying to send notification about the start of the flow!", e);
			}
		}
	}
	
}
