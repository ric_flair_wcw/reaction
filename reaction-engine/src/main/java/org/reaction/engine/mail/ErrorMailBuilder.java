package org.reaction.engine.mail;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.StringUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

public class ErrorMailBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorMailBuilder.class);
	
	private String subject;
	private MimeMessage mimeMessage;
	private String[] to;
	private String from;
	private String templateFolder;
	private String emailTemplate;
	private Context context;

	@Autowired
	private TemplateEngine templateEngine;
	
	
    public MimeMessage build() throws MessagingException {
    	LOGGER.debug("Building a mail to send\n\tto: {}\n\tfrom: {}\n\twith the subject: {}", to, from, subject);
        final MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, false, "UTF-8");
        messageHelper.setSubject(subject);
        messageHelper.setTo(to);
        messageHelper.setFrom(from);
        
		if (!StringUtils.isEmpty(templateFolder)) {
			TemplateResolver resolver = new FileTemplateResolver();
			resolver.setPrefix(templateFolder);
			resolver.setSuffix(".html");
			resolver.setCacheable(true);
			resolver.setCharacterEncoding("UTF-8");
			resolver.setTemplateMode("HTML5");
	        this.templateEngine.setTemplateResolver(resolver);
		}
		final String htmlContent = this.templateEngine.process(emailTemplate, context);
        messageHelper.setText(htmlContent, true);

        return mimeMessage;
    }
    
    public ErrorMailBuilder subject(String subject) {
    	this.subject = subject;
    	return this;
    }

    public ErrorMailBuilder mimeMessage(MimeMessage mimeMessage) {
    	this.mimeMessage = mimeMessage;
    	return this;
    }

    public ErrorMailBuilder to(String[] to) {
    	this.to = to;
    	return this;
    }

    public ErrorMailBuilder from(String from) {
    	this.from = from;
    	return this;
    }
    
    public ErrorMailBuilder context(Context context) {
    	this.context = context;
    	return this;
    }
    
    public ErrorMailBuilder emailTemplate(String emailTemplate) {
    	this.emailTemplate = emailTemplate;
    	return this;
    }

    public ErrorMailBuilder templateFolder(String templateFolder) {
    	this.templateFolder = templateFolder;
    	return this;
    }

}