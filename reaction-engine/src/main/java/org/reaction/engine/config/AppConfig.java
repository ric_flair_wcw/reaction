package org.reaction.engine.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.task.TaskDecorator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Map;
import java.util.concurrent.Executor;

@Configuration
@Profile("!singleThreadPool")
public class  AppConfig {


	@Value("${reaction.commands.using-ssh.threadpool.max-pool-size:500}")
	private transient int threadMaxPoolSize;
	@Value("${reaction.commands.using-ssh.threadpool.core-pool-size:10}")
	private transient int threadCorePoolSize;
	@Value("${reaction.commands.using-ssh.threadpool.thread-keep-alive-seconds:60}")
	private transient int threadKeepAliveSeconds;
	@Value("${reaction.commands.using-ssh.threadpool.thread-wait-on-shutdown:true}")
	private transient boolean threadWaitForTasksToCompleteOnShutdown;
	@Value("${reaction.commands.using-ssh.threadpool.thread-await-termination-seconds:30}")
	private transient int threadAwaitTerminationSeconds;
	@Value("${reaction.commands.using-ssh.threadpool.thread-queue-capacity:0}")
	private transient int threadQueueCapacity;


	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}


	@Bean(name = "executorCommandUsingSSH")
	public Executor executorCommandUsingSSH() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setThreadPriority(Thread.MAX_PRIORITY);
		threadPoolTaskExecutor.setCorePoolSize(threadCorePoolSize);
		threadPoolTaskExecutor.setMaxPoolSize(threadMaxPoolSize);
		threadPoolTaskExecutor.setQueueCapacity(threadQueueCapacity);
		threadPoolTaskExecutor.setKeepAliveSeconds(threadKeepAliveSeconds);
		threadPoolTaskExecutor.setThreadGroupName("CQS - ML call Thread Group");
		threadPoolTaskExecutor.setThreadNamePrefix("CQS - ML call Thread ");
		threadPoolTaskExecutor.setWaitForTasksToCompleteOnShutdown(threadWaitForTasksToCompleteOnShutdown);
		threadPoolTaskExecutor.setAwaitTerminationSeconds(threadAwaitTerminationSeconds);
		threadPoolTaskExecutor.setTaskDecorator(new MDCTaskDecorator());
		return threadPoolTaskExecutor;
	}


	private final class MDCTaskDecorator implements TaskDecorator {

		@Override
		public Runnable decorate(Runnable task) {
			Map<String, String> webThreadContext = MDC.getCopyOfContextMap();
			return () -> {
				try {
					MDC.setContextMap(webThreadContext);
					task.run();
				} finally {
					MDC.clear();
				}
			};
		}

	}
}
