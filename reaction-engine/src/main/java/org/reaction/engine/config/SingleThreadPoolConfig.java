package org.reaction.engine.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

@Configuration
@Profile("singleThreadPool")
public class SingleThreadPoolConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(SingleThreadPoolConfig.class);

    @Bean(name = "executorCommandUsingSSH")
    public ExecutorService executorCommandUsingSSH() {
        return new ExecutorService() {
            private boolean shutdown = false;
            @Override
            public void execute(Runnable runnable) {
                runnable.run();
            }
            @Override
            public void shutdown() {
                shutdown = true;
            }
            @Override
            public List<Runnable> shutdownNow() {
                shutdown = true;
                return null;
            }
            @Override
            public boolean isShutdown() {
                return false;
            }
            @Override
            public boolean isTerminated() {
                return false;
            }
            @Override
            public boolean awaitTermination(long l, TimeUnit timeUnit) throws InterruptedException {
                return false;
            }
            @Override
            public <T> Future<T> submit(Callable<T> callable) {
                return null;
            }
            @Override
            public <T> Future<T> submit(Runnable runnable, T t) {
                return null;
            }
            @Override
            public Future<?> submit(Runnable runnable) {
                return null;
            }
            @Override
            public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) throws InterruptedException {
                return null;
            }
            @Override
            public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long l, TimeUnit timeUnit) throws InterruptedException {
                return null;
            }
            @Override
            public <T> T invokeAny(Collection<? extends Callable<T>> collection) throws InterruptedException, ExecutionException {
                return null;
            }
            @Override
            public <T> T invokeAny(Collection<? extends Callable<T>> collection, long l, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
                return null;
            }
        };
    }
}
