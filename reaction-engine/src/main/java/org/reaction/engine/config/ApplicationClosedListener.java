package org.reaction.engine.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

@Component
public class ApplicationClosedListener implements ApplicationListener<ContextClosedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationClosedListener.class);

    @Autowired
    @Qualifier("executorCommandUsingSSH")
    private Executor executorCommandUsingSSH;

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        LOGGER.info("ContextClosedEvent is fired.");
        if (executorCommandUsingSSH != null) {
            try {
                if (executorCommandUsingSSH instanceof ThreadPoolTaskExecutor) {
                    LOGGER.info("Shutting down the ThreadPoolTaskExecutor 'executorCommandUsingSSH' ...");
                    ((ThreadPoolTaskExecutor) executorCommandUsingSSH).shutdown();
                } else {
                    LOGGER.info("Shutting down the ExecutorService 'executorCommandUsingSSH'...");
                    ((ExecutorService) executorCommandUsingSSH).shutdownNow();
                }
                LOGGER.info("The executor is stopped successfully.");
            } catch (Exception e) {
                LOGGER.error("Exception occurred when trying to close the thread pool!", e);
            }
        }
    }

}