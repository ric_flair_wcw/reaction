package org.reaction.engine.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	public BadRequestException(String text) {
		super(text);
	}

	private static final long serialVersionUID = 7295096157991094739L;


}
