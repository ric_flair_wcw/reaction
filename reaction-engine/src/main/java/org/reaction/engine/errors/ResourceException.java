package org.reaction.engine.errors;

import org.springframework.http.HttpStatus;

public class ResourceException extends RuntimeException {

    private HttpStatus httpStatus;

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ResourceException(HttpStatus httpStatus, Exception e) {
        super(e);
        this.httpStatus = httpStatus;
    }

    public ResourceException(HttpStatus httpStatus, String message, Exception e) {
        super(message, e);
        this.httpStatus = httpStatus;
    }

    public ResourceException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }

}