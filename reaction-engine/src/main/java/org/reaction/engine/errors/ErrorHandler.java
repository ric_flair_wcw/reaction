package org.reaction.engine.errors;

import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;

public interface ErrorHandler {

	void handleError(Event event, Throwable t);

	void handleErrorInCommand(EventLife eventLife, Exception e);

}