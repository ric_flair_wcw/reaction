package org.reaction.engine.errors;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.common.domain.task.CommandByWorkerTask;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.persistence.service.ExecutionFlowService;
import org.reaction.engine.persistence.service.ScheduledExecutionFlowService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

@Component
public class DefaultErrorHandler implements ErrorHandler {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultErrorHandler.class);
	
	@Autowired
	private EventService eventService;
	@Autowired
	private EventLifeService eventLifeService;
	@Autowired
	private ReactionMailSender mailSender;
    @Autowired
    private ExecutionFlowService executionFlowService;
	@Autowired
	private AuthQuery authQuery;
	@Autowired
	private ReactionMailSender reactionMailSender;
	@Autowired
	private ScheduledExecutionFlowService scheduledExecutionFlowService;

	@Value("${reaction.management_web_app.endpoint}")
	private String managementWebAppEndpoint;
	@Value("#{new Boolean('${reaction.mail.enabled.error_unknown}'.trim())}")
	Boolean sendMail;
	
	
	@Override
	@Transactional(Transactional.TxType.REQUIRES_NEW)
	public void handleError(Event _event, Throwable t) {
		// setting the values for the mail template
        final Context context = new Context(Locale.UK);
        context.setVariable("exceptionText", t.getMessage());
        context.setVariable("executionFlow_name", executionFlowService.getExecutionFlow(_event.getExecutionFlow().getId()).getName());
        context.setVariable("event_identifier", _event.getIdentifier());
        context.setVariable("event_id", _event.getId());
        context.setVariable("web_gui_url", managementWebAppEndpoint);
        // sending the mail
        if (sendMail) {
	        List<String> recipients = null;
	        if (_event.getExecutionFlow().getErrorMailSendingRecipients() != null && _event.getExecutionFlow().getErrorMailSendingRecipients().size() > 0) {
	        	// if the error mail sending is checked in the get recipients from there
	        	recipients = _event.getExecutionFlow().getErrorMailSendingRecipients();
	        } else {
	        	// otherwise the recipients are the members of the access groups assigned to the flow
	        	recipients = authQuery.getEmailsByAccesGroups(_event.getExecutionFlow().getAccessGroups());
	        }
			mailSender.sendMail(recipients, "Reaction Engine - Unknown error occured when running the execution flow!", _event, context, "unknownErrorMailTemplate");
        }

		// if the event hasn't been saved yet then save it
		Event event = eventService.getEventByIdentifier(_event.getIdentifier());
		if (event == null) {
			event = _event;
		}		
		event.setStatus(EventStatusEnum.FAILED);
		event.setEndDate(Calendar.getInstance());
		Event savedEvent = eventService.save(event);
		LOGGER.debug("The following Even is saved: \n{}", savedEvent);
		// adding an INTERNAL_ERROR EventLife at the end
		Calendar now = Calendar.getInstance();
		EventLife eventLife = EventLife.builder()
									   .eventDate(now)
									   .event(savedEvent)
									   .errorMessage(t.getLocalizedMessage())
									   .order(9999)
									   .eventStatus(EventStatusEnum.INTERNAL_ERROR).build();
		eventLife.addToHistory();
		EventLife savedEventLife = eventLifeService.save( eventLife );
		LOGGER.debug("The following EvenLife is saved: \n{}", savedEventLife);
	}


	@Override
	@Transactional
	public void handleErrorInCommand(EventLife eventLife, Exception e) {
		try {
			LOGGER.error("Error happened during the execution of the command!", e);
			LOGGER.info("{}", eventLife);
			// setting the values for the template
			final Context context = new Context(Locale.UK);
			context.setVariable("event_identifier", eventLife.getEvent().getIdentifier());
			context.setVariable("task_name", eventLife.getTask().getSuperTask().getName());
			context.setVariable("task_type", eventLife.getTask().getSuperTask().getClass().getSimpleName());
			context.setVariable("task_host", eventLife.getTask().getSuperTask() instanceof CommandByWorkerTask ? ((CommandByWorkerTask)eventLife.getTask().getSuperTask()).getHost() : "");
			context.setVariable("task_command", eventLife.getTask().getSuperTask() instanceof CommandByWorkerTask ? ((CommandByWorkerTask)eventLife.getTask().getSuperTask()).getCommand() : "");
			context.setVariable("eventLife_output", e.getMessage());
			ExecutionFlow executionFlow = eventLife.getEvent().getErrorDetector() != null && eventLife.getEvent().getErrorDetector().getExecutionFlow() != null ?
					eventLife.getEvent().getErrorDetector().getExecutionFlow() :
					eventLife.getEvent().getExecutionFlow();
			context.setVariable("executionFlow_name", executionFlowService.getExecutionFlow(executionFlow.getId()).getName());
			context.setVariable("event_id", eventLife.getEvent().getId());
			context.setVariable("web_gui_url", managementWebAppEndpoint);
			// sending the mail
			if (sendMail) {
				try {
					List<String> recipients = null;
					if (executionFlow.getErrorMailSendingRecipients() != null && executionFlow.getErrorMailSendingRecipients().size() > 0) {
						// if the error mail sending is checked in the get recipients from there
						recipients = executionFlow.getErrorMailSendingRecipients();
					} else {
						// otherwise the recipients are the members of the access groups assigned to the flow
						recipients = authQuery.getEmailsByAccesGroups(executionFlow.getAccessGroups());
					}
					reactionMailSender.sendMail(recipients, "Reaction Engine - error occurred when running an execution flow!", eventLife.getEvent(), context, "externalErrorMailTemplate");
				} catch(Exception e2) {
					LOGGER.error("The error mail cannot be sent out!", e2);
				}
			}
			// altering the event
			Event event = eventLife.getEvent();
			event.setStatus(EventStatusEnum.FAILED);
			event.setEndDate(Calendar.getInstance());
			Event savedEvent = eventService.save(event);
			LOGGER.debug("The following Even is saved: \n{}", savedEvent);
			// altering the event life
			Calendar now = Calendar.getInstance();
			eventLife.setEventDate(now);
			eventLife.setErrorMessage(e.getLocalizedMessage());
			eventLife.setEventStatus(EventStatusEnum.FAILED);
			eventLife.addToHistory();
			EventLife savedEventLife = eventLifeService.save( eventLife );
			LOGGER.debug("The following EvenLife is saved: \n{}", savedEventLife);
			// stop the related scheduler if there is any
			if (event.getScheduledExecutionFlow() != null) {
				ScheduledExecutionFlow scheduledExecutionFlow = event.getScheduledExecutionFlow();
				LOGGER.warn("Due to an error the scheduled execution flow (scheduled execution flow id={}) will be stopped!", scheduledExecutionFlow.getId());
				scheduledExecutionFlow.setScheduledAlready(false);
				scheduledExecutionFlow.setErrorAtLastRun(e.getMessage());
				scheduledExecutionFlowService.save(scheduledExecutionFlow);
			}
		} catch(Throwable t) {
			LOGGER.error("Great, an error occurred while I tried to handle another one...!", t);
		}

	}

}
