package org.reaction.engine.controller;

import com.jayway.jsonpath.JsonPathException;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.errors.ResourceException;
import org.reaction.engine.persistence.service.EventSourceTypeService;
import org.reaction.engine.service.EventProcessingService;
import org.reaction.engine.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.XPathExpressionException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
public class EventController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventController.class);

    @Autowired
    private EventProcessingService eventProcessingService;
    @Autowired
    private EventSourceTypeService eventSourceTypeService;
    @Autowired
    private SecurityService securityService;


    @PostMapping(value = "/notification/event/{eventSourceTypeCode}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<String> receiveEvent(HttpServletRequest req,
                                               @PathVariable String eventSourceTypeCode,
                                               @RequestParam Map<String,String> queryParameters) {
        String uuid = UUID.randomUUID().toString();
        MDC.put("uuid", uuid);
        LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
                "An event has just arrived from [{}] event source type"+
                "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", eventSourceTypeCode);
        LOGGER.debug("Request headers: {}", Collections.list(req.getHeaderNames()).stream().map(h -> h+" : "+ req.getHeader(h)).collect(Collectors.joining(", ")));
        LOGGER.debug("Query parameters: {}", queryParameters);
        LOGGER.debug("The request URL what was called by the client: {}", req.getRequestURL().toString());
        try {
            String requestBody = StreamUtils.copyToString(req.getInputStream(), StandardCharsets.UTF_8);
            LOGGER.debug("Request body: {}", requestBody);
            EventSourceType eventSourceType = getEventSourceType(eventSourceTypeCode);
            securityService.check(eventSourceType, req);
            HttpStatus httpStatus = eventProcessingService.process(eventSourceType, requestBody, queryParameters, uuid);
            return new ResponseEntity<>(httpStatus);
        } catch(ResourceException re) {
            LOGGER.error("Exception occurred!", re);
            throw re;
        } catch(ReactionRuntimeException ree) {
            LOGGER.error("Runtime exception occurred!", ree);
            throw new ResourceException(HttpStatus.BAD_REQUEST, ree);
        } catch(XPathExpressionException xee) {
            LOGGER.error("Error occurred when trying to extract from XML!", xee);
            throw new ResourceException(HttpStatus.BAD_REQUEST, "Error occurred when trying to extract from XML!");
        } catch(JsonPathException spe) {
            LOGGER.error("Error occurred when trying to extract from JSON!", spe);
            throw new ResourceException(HttpStatus.BAD_REQUEST, "Error occurred when trying to extract from JSON!");
        } catch(ReactionConfigurationException rce) {
            LOGGER.error("There is a problem with the Reaction Engine's configuration!", rce);
            throw new ResourceException(HttpStatus.INTERNAL_SERVER_ERROR, "There is a problem with the Reaction Engine's configuration!");
        } catch(Exception e) {
            LOGGER.error("Error occurred during the event candidate processing!", e);
            throw new ResourceException(HttpStatus.INTERNAL_SERVER_ERROR, "Error occurred during the event candidate processing!");
        } finally {
            MDC.remove("uuid");
        }
    }


    private EventSourceType getEventSourceType(String eventSourceTypeCode) {
        EventSourceType eventSourceType = eventSourceTypeService.getEventSourceTypeByCode(eventSourceTypeCode);
        if (eventSourceType == null) {
            throw new ReactionRuntimeException(String.format("The event source type [%s] doesn't exist!", eventSourceTypeCode));
        }
        return eventSourceType;
    }

}
