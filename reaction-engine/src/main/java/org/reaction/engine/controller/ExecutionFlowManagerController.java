package org.reaction.engine.controller;

import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.errors.BadRequestException;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.persistence.service.ExecutionFlowService;
import org.reaction.engine.persistence.service.ScheduledExecutionFlowService;
import org.reaction.engine.scheduling.Scheduler;
import org.reaction.engine.service.ExecutionFlowMachineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/executionflow", produces = MediaType.APPLICATION_JSON_VALUE)
public class ExecutionFlowManagerController extends ParentController {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExecutionFlowManagerController.class);

	@Autowired
	private ExecutionFlowMachineService executionFlowMachineService;
	@Autowired
	private Scheduler scheduler;
	@Autowired
	private EventService eventService;
	@Autowired
	private EventLifeService eventLifeService;
	@Autowired
	private ScheduledExecutionFlowService scheduledExecutionFlowService;
	@Autowired
	private ExecutionFlowService executionFlowService;
	@Autowired
	private ErrorHandler errorHandler;
	
	
	// the request can arrive from the Executor: start it right now or schedule it once
	@RequestMapping(value = "/start/manual", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public @ResponseBody String startExecutionFlowManually(@RequestBody Map<String,String> request) throws ReactionExecutionFlowException {
		String uuid = UUID.randomUUID().toString();
		MDC.put("uuid", uuid);
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "A request arrived to start the following executionFlow manually. request={}"+
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", request);		
		// getting the execution flow id from the request
		Long executionFlowId = get("executionFlowId", request, Long.class);
		ExecutionFlow executionFlow = executionFlowService.getExecutionFlow(executionFlowId);
		// getting the reason (why to execute the flow ) from the request
		String reason = request.get("reason");
		
		// starting the execution flow now
		if (request.get("date") == null) {
			// creating an Event
			Event event = Event.builder()
							   .identifier(uuid)
							   .initiatedBy(EventInitiatorEnum.MANUALLY)
							   .executionFlow(executionFlow)
							   .startDate(Calendar.getInstance())
							   .reason(reason).build();
			LOGGER.debug("I built the following Event: \n{}", event);
			
			try {
				// start the executionFlow
				event = executionFlowMachineService.startIt(event);
			} catch(Throwable t) {
				LOGGER.error("Error happened when started the execution flow!", t);
				errorHandler.handleError(event, t);
			}
		}
		// scheduling the execution flow
		else {
			// getting the start date (when to schedule the exec flow to) from the request (if it is empty then it has to be executed now)
			Calendar startDate = Calendar.getInstance();
			try {
				startDate.setTime(new SimpleDateFormat("yyyy.MM.dd HH:mm:ss").parse(request.get("date")));
			} catch(Exception e) {
				throw new ReactionRuntimeException("The request doesn't contain the start date or it is invalid!",e);
			}
			// creating an Event and store it
			Event event = Event.builder()
							   .identifier(uuid)
							   .initiatedBy(EventInitiatorEnum.MANUALLY)
							   .status(EventStatusEnum.SCHEDULED)
							   .startDate(startDate)
							   .executionFlow(executionFlow)
							   .reason(reason).build();
			Event savedEvent = eventService.save(event);
			LOGGER.debug("The following Event is saved: \n{}", savedEvent);
			EventLife eventLife = EventLife.builder()
                                           .eventDate(Calendar.getInstance())
                                           .event(savedEvent)
                                           .eventStatus(EventStatusEnum.SCHEDULED)
                                           .order(-2).build();
			eventLife.addToHistory();
			eventLifeService.save(eventLife);

			// scheduling it
			scheduler.schedule(savedEvent);
		}
		return uuid;
	}
	

	// the request can arrive from the Scheduler: regular scheduling
	@RequestMapping(value = "/start/scheduled", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public void scheduleExecutionFlow(@RequestBody Map<String,String> request) {
		String uuid = UUID.randomUUID().toString();
		MDC.put("uuid", uuid);
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "A request arrived to schedule an executionFlow. request={}"+
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", request);		
		
		// getting the scheduled execution flow id from the request
		Long scheduledExecutionFlowId = get("scheduledExecutionFlowId", request, Long.class);
		// getting the DO
		ScheduledExecutionFlow scheduledExecutionFlow = scheduledExecutionFlowService.getScheduledExecutionFlow(scheduledExecutionFlowId);
		
		// calculating the future date when the execution flow will run
		CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(scheduledExecutionFlow.getCronExpression());
		Calendar next = Calendar.getInstance();
		next.setTime(cronSequenceGenerator.next(Calendar.getInstance().getTime()));

		// set the setScheduledAlready property to true
		scheduledExecutionFlow.setScheduledAlready(true);
		scheduledExecutionFlow.setErrorAtLastRun(null);
		scheduledExecutionFlowService.save(scheduledExecutionFlow);
		
		// creating an Event and store it
		Event event = Event.builder()
						   .identifier(uuid)
						   .initiatedBy(EventInitiatorEnum.BY_SCHEDULER)
						   .status(EventStatusEnum.SCHEDULED)
						   .startDate(next)
						   .executionFlow(scheduledExecutionFlow.getExecutionFlow())
						   .scheduledExecutionFlow(scheduledExecutionFlow).build();
		Event savedEvent = eventService.save(event);
		LOGGER.debug("The following Event is saved: \n{}", savedEvent);
		EventLife eventLife = EventLife.builder()
				                       .eventDate(Calendar.getInstance())
				                       .event(savedEvent)
				                       .eventStatus(EventStatusEnum.SCHEDULED)
				                       .order(-2).build();
		eventLife.addToHistory();
		eventLifeService.save(eventLife);

		// scheduling it
		scheduler.schedule(savedEvent);
	}
	
	
	@RequestMapping(value = "/start/eventlife", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void restartEventLife(@RequestBody Map<String,String> request) throws ReactionExecutionFlowException {
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "A request arrived to restart a failed event life. request={}"+
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", request);		
		
		// getting the event life id and the user from the request
		Long eventLifeId = get("eventLifeId", request, Long.class);
		String user = get("byWhom", request, String.class);
		// getting the event life DO and ...
		EventLife eventLife = eventLifeService.getEventLife(eventLifeId);
		MDC.put("uuid", eventLife.getEvent().getIdentifier());
		// restart the it
		executionFlowMachineService.restartEventLife(eventLife, user);
	}	


	@RequestMapping(value = "/skip/eventlife", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void skipEventLife(@RequestBody Map<String,String> request) throws ReactionExecutionFlowException {
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "A request arrived to skip a failed event life. request={}"+
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", request);		
		
		// getting the event life id and the user from the request
		Long eventLifeId = get("eventLifeId", request, Long.class);
		String value = get("value", request, String.class);
		String user = get("byWhom", request, String.class);
		// getting the event life DO and ...
		EventLife eventLife = eventLifeService.getEventLife(eventLifeId);
		MDC.put("uuid", eventLife.getEvent().getIdentifier());
		// skip the it
		executionFlowMachineService.skipEventLife(eventLife, user, value);
	}	

	
	@RequestMapping(value = "/approve", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public void approve(@RequestBody Map<String,String> request) throws ReactionExecutionFlowException {
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "The following event is approved: {}"+
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", request);		
		_approve(request, EventStatusEnum.CONFIRMED);
	}


	@RequestMapping(value = "/approve_and_forced_start", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
	public void approveAndForcedStart(@RequestBody Map<String,String> request) throws ReactionExecutionFlowException {
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "The following event is approved and forced start: {}"+
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", request);		
		_approve(request, EventStatusEnum.CONFIRMED_AND_FORCED_START);
	}

	
	private void _approve(Map<String, String> request, EventStatusEnum status) throws ReactionExecutionFlowException {
		// getting the event 
		Long eventId = get("eventId", request, Long.class);
		String user = get("byWhom", request, String.class);
		Event event = eventService.getEvent(eventId);
		MDC.put("uuid", event.getIdentifier());
		if (EventStatusEnum.CONFIRMATION_NEEDED.equals(event.getStatus())) {
			// set to CONFIRMED
			event.setStatus(status);
			eventService.save(event);
			// save an event life record with CONFIRMED
			EventLife eventLife = EventLife.builder()
					   .eventDate(Calendar.getInstance())
					   .event(event)
					   .eventStatus(status)
					   .byWhom(user)
					   .order(-2).build();
			eventLife.addToHistory();
			eventLifeService.save(eventLife);
			// call the exec flow machine
			executionFlowMachineService.startIt(event);
		} else {
			LOGGER.error("Warning! The request is rejected! The status of the event ({}) is not CONFIRMATION_NEEDED but {} so approve/reject operation is not permitted!", event.getIdentifier(), event.getStatus());
			throw new BadRequestException("Error! The status of the event is not CONFIRMATION_NEEDED so approve/reject operation is not permitted!");
		}
	}	

}