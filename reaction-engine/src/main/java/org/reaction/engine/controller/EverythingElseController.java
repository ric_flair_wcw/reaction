package org.reaction.engine.controller;

import org.reaction.common.contants.Constants;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.EventSource;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.log.LogAnalyzer;
import org.reaction.engine.errors.ResourceException;
import org.reaction.engine.expression.ExpressionInterpreter;
import org.reaction.engine.persistence.service.ExecutionFlowService;
import org.reaction.engine.persistence.service.EventSourceService;
import org.reaction.engine.service.EventProcessingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/utils", produces = MediaType.APPLICATION_JSON_VALUE)
public class EverythingElseController extends ParentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EverythingElseController.class);
	
	@Autowired
	private ApplicationContext applicationContext;
	@Autowired
	private EventSourceService eventSourceService;
	@Autowired
	private ExecutionFlowService executionFlowService; 
	@Autowired
	private ExpressionInterpreter expressionInterpreter;
	@Autowired
	private EventProcessingService eventProcessingService;
	
	
	// I have to send the cron in the request body as I cannot encode the / character (getting back 400 Bad Request) -> http://stackoverflow.com/questions/4069002/receive-an-http-400-error-if-2f-is-part-of-the-get-url-in-jboss
	@RequestMapping(value = "/cron/nextrun", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String getNextRun(@RequestBody Map<String,String> request) {
		LOGGER.info("A request arrived to get the next run date of the following cron expression. request={}", request);
		String cron = request.get("cron");	
		CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(cron);
		Calendar next = Calendar.getInstance();
		next.setTime(cronSequenceGenerator.next(Calendar.getInstance().getTime()));
		return Constants.getDateFormat().format(next.getTime());
	}

	
	// on the eventSource admin GUI the user can check if the built log header pattern is correct -> specifying a log entry and check if the pattern matches
	// I have to send the log entry in the request body as it can be long
	@RequestMapping(value = "/logheaderpattern/check", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> checkLogHeaderPattern(@RequestBody Map<String,String> request) {
		Map<String, Object> result = new HashMap<>();
		LOGGER.info("A request arrived to check the log header pattern. request={}", request);
		String logEntry = request.get("logEntry");
		String eventSourceId = request.get("eventSourceId");
		// loading the eventSource
		EventSource eventSource = eventSourceService.getEventSource(Long.valueOf(eventSourceId));
		// call the log analyzer
		LogAnalyzer logAnalyzer = applicationContext.getBean("logAnalyzer", LogAnalyzer.class);
		logAnalyzer.setEventSource(eventSource);
		boolean found = logAnalyzer.process(logEntry);
		// build the response
		result.put("found", found);
		if (found) {
			if (logAnalyzer.getLogLevel() != null) {
				result.put("logLevel", logAnalyzer.getLogLevel());
			}
			if (logAnalyzer.getDateTime() != null) {
				result.put("date", logAnalyzer.getDateTime().toString());
			}
			if (logAnalyzer.getUnknown() != null) {
				result.put("unknown", logAnalyzer.getUnknown());
			}
		}
		LOGGER.debug("The result is {}", result);
		return result;
	}
	
	
	@RequestMapping(value = "/maintenancewindow/next/{_eventSourceId}/{_executionFlowId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String getNextInMaintenaceWindow(@PathVariable String _eventSourceId, @PathVariable String _executionFlowId) {
		LOGGER.info("A request arrived to get the next date in maintenance window. eventSourceId={}, _executionFlowId={}", _eventSourceId, _executionFlowId);
		// getting the eventSource and execution flow
		Long eventSourceId = convert("eventSourceId", _eventSourceId, Long.class);
		Long executionFlowId = convert("executionFlowId", _executionFlowId, Long.class);
		EventSource eventSource = eventSourceService.getEventSource(eventSourceId);
		ExecutionFlow executionFlow = executionFlowService.getExecutionFlow(executionFlowId);
		// generating the next date in maintenance window
		String next = eventSource.getMaintenanceWindow().isInMaintenanceWindow(executionFlow.getExecutionTime()) ?
				      "It is in the maintenance window, can be started immediately." :
			          Constants.getDateFormat().format(eventSource.getMaintenanceWindow().nextInMaintenanceWindow(executionFlow.getExecutionTime()).getTime());
		LOGGER.debug("The result is {}", next);
		return next;
	}


	@RequestMapping(value = "/maintenancewindow/isin/{_eventSourceId}/{_executionFlowId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody String isInMaintenaceWindow(@PathVariable String _eventSourceId, @PathVariable String _executionFlowId) {
		LOGGER.info("A request arrived to check if we are in in the maintenance window. eventSourceId={}, _executionFlowId={}", _eventSourceId, _executionFlowId);
		// getting the eventSource and execution flow
		Long eventSourceId = convert("eventSourceId", _eventSourceId, Long.class);
		Long executionFlowId = convert("executionFlowId", _executionFlowId, Long.class);
		EventSource eventSource = eventSourceService.getEventSource(eventSourceId);
		ExecutionFlow executionFlow = executionFlowService.getExecutionFlow(executionFlowId);
		// generating the next date in maintenance window
		boolean isIn = eventSource.getMaintenanceWindow().isInMaintenanceWindow(executionFlow.getExecutionTime());
		LOGGER.debug("The result is {}", isIn);
		return Boolean.toString(isIn);
	}

	
	@RequestMapping(value = "/expression/check", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public boolean checkExpression(@RequestBody Map<String,String> request) throws ReactionExecutionFlowException {
		LOGGER.info("A request arrived to check the expression in IF-ELSE condition. request={}", request);
		String expression = get("expression", request, String.class);
		String commandOutput = request.get("commandOutput");
		
		return expressionInterpreter.evaluate(commandOutput, expression);
	}


	@RequestMapping(value = "/identifiers/check", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<String>> checkIdentifiers(@RequestBody Map<String,Object> request) {
		LOGGER.info("A request arrived to check the admin event source type's identifiers. request={}", request);
		String message = (String)request.get("message");
		List<String> identifiers = (List<String>) request.get("identifiers");
		String dataType = (String)request.get("dataType");
		String namespaces = (String)request.get("namespaces");
		LOGGER.debug("identifiers: {}", identifiers);

		try {
			List<String> values = eventProcessingService.checkIdentifiers(message, identifiers, dataType, namespaces);
			LOGGER.debug("The result is {}", values);
			return new ResponseEntity<List<String>>(values, HttpStatus.OK);
		} catch (ReactionConfigurationException rce) {
			LOGGER.error("ReactionConfigurationException occurred!", rce);
			throw new ResourceException(HttpStatus.INTERNAL_SERVER_ERROR, rce.getMessage());
		} catch(Exception e) {
			LOGGER.error("Exception occurred!", e);
			throw new ResourceException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
	}

}