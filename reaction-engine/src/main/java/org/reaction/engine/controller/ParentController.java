package org.reaction.engine.controller;

import java.util.Map;

import org.reaction.common.exception.ReactionRuntimeException;

public class ParentController {

	protected static final String ERROR = "ERROR";
	
	
	protected <T> T get(String param, Map<String,String> map, Class<T> clazz) {
		try {
			if (clazz.equals(Long.class)) {
				try {
					return clazz.cast( Long.valueOf(map.get(param)) );
				} catch(NumberFormatException nfe) {
					throw new ReactionRuntimeException("The '" + map.get(param) + "' is not a long number! ", nfe);
				}
			} if (clazz.equals(String.class)) {
				return clazz.cast( map.get(param).toString() );
			} else {
				throw new ReactionRuntimeException("Unknown class! " + clazz);
			}
		} catch(Exception e) {
			throw new ReactionRuntimeException("The '" + param + "' cannot be found in the request!", e);
		}		
	}
	

	protected <T> T convert(String param, String value, Class<T> clazz) {
		if (clazz.equals(Long.class)) {
			try {
				return clazz.cast( Long.valueOf(value) );
			} catch(NumberFormatException nfe) {
				throw new ReactionRuntimeException("The value of the parameter '" +param + "' is not a long number! ", nfe);
			}
		} else {
			throw new ReactionRuntimeException("Unknown class! " + clazz);
		}
	}

}
