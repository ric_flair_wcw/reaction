package org.reaction.engine.controller;


import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.domain.*;
import org.reaction.common.domain.task.CommandByWorkerTask;
import org.reaction.common.dto.EventSourceWithErrorDetectorPattern;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.queue.LimitedSet;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.*;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.reaction.engine.tasks.CommandByWorkerTaskCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.context.Context;

import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping(value = "/worker", produces = MediaType.APPLICATION_JSON_VALUE)
public class WorkerController extends ParentController {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(WorkerController.class);
	
	// max 1000 identifiers can be in the cache
	public List<String> identifierCache = Collections.synchronizedList(new LimitedSet<String>(1000));
	
	@Autowired
	private ErrorDetectorService errorDetectorService;
	@Autowired
	private EventLifeService eventLifeService;
	@Autowired
	private EventService eventService;
	@Autowired
	private CommandsToBeExecutedService commandsToBeExecutedService;
	@Autowired
	private EventSourceService eventSourceService;
	@Autowired
	private EventMachineService eventMachineService;
	@Autowired
	private ErrorHandler errorHandler;
	@Autowired
	private ReactionMailSender mailSender;
    @Autowired
    private ExecutionFlowService executionFlowService;
	@Autowired
	private AuthQuery authQuery;
	@Autowired
	private WorkerStatusService workerStatusService;
	@Autowired
	private PlaceholderVariableService placeholderVariableService;

	@Value("${reaction.management_web_app.endpoint}")
	private String managementWebAppEndpoint;
	@Value("#{new Boolean('${reaction.mail.enabled.error_when_executing_task_on_host}'.trim())}")
	private Boolean sendMail;
	
	
	@RequestMapping(value = "/event", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void reportEvent(@RequestBody Map<String, String> eventMap) throws ReactionExecutionFlowException, ReactionConfigurationException {
		String uuid = get("uuid", eventMap, String.class);	
		MDC.put("uuid", uuid);
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "An event has just arrived: \n{}"+
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", eventMap);
		// searching for an ErrorDetector record -> checking if the error is registered in the database and action (executionFlow) should be taken
		String message = get("message", eventMap, String.class);
		Long eventSourceId = get("systmId", eventMap, Long.class);
		LogLevelEnum logLevel = eventMap.get("logLevel") == null ? null : LogLevelEnum.convert( get("logLevel", eventMap, String.class) );

		// refreshing the status of the specific worker
		workerStatusService.refreshReaderReportEvent(eventSourceId);

		// check if the event was submitted already (sometimes the same event came here twice... not sure why but with this it can be ruled out)
		if (isInCache(uuid)) {
			return;
		}

		Event event = Event.builder()
				           .logLevel(logLevel )
				           .eventSourceId(eventSourceId)
				           .initiatedBy(EventInitiatorEnum.BY_LOG)
				           .identifier(uuid)
				           .message(message).build();
		LOGGER.debug("I built the following Event: \n{}", event);
		ErrorDetector errorDetector = errorDetectorService.search(event, true, null, null, null);
		// process the event -> start a executionFlow or ignore the event
		if (errorDetector != null) {
			LOGGER.debug("I have found the following ErrorDetector for the event: \n{}", errorDetector);
			
			event.setErrorDetector(errorDetector);
			event.setExecutionFlow(errorDetector.getExecutionFlow());
			eventMachineService.processEvent(event);
		} else {
			LOGGER.warn("No ErrorDetector record belongs to arrived event => it cannot be processed, it is ignored.");
		}
	}	


	// the worker polls this endpoint to check if there is a command to be executed
	@RequestMapping(value = "/command/{host}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Map<String,String>> checkCommand(@PathVariable String host) {
		LOGGER.debug("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		             "A request has just arrived from the host '{}' to check if there is any command to be executed."+
		             "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", host);
		workerStatusService.refreshExecutorCheckCommands(host);
		
		// querying and updating (executed=true) the CommandsToBeExecuted for the specific host
		List<CommandsToBeExecuted> listCommandsToBeExecuted = commandsToBeExecutedService.processCommandsToBeExecuted(host);
		LOGGER.debug("Commands to be executed are \n{}", listCommandsToBeExecuted.stream().map(CommandsToBeExecuted::toString).collect(Collectors.joining(", ")));
		// updating the event life and building the list of maps to be sent back
		List<Map<String,String>> result = listCommandsToBeExecuted.stream().map(c -> {
			EventLife eventLife = c.getEventLife();
			// updating the status of EventLife to STARTED
			// TODO if there is an exception then the worker won't get records that were set to EXECUTION_STARTED but they are already in the EXECUTION_STARTED state => all the records have to be altered in one transaction!
			eventLife.setEventDate(Calendar.getInstance());
			eventLife.setEventStatus(EventStatusEnum.EXECUTION_STARTED);
			eventLife.addToHistory();
			eventLifeService.save(eventLife);
			LOGGER.trace("The following eventLife is stored : {}", eventLife);
			// creating a map from CommandsToBeExecuted 
			Map<String,String> m = new HashMap<>();
			m.put("eventLifeId", Long.toString(c.getEventLife().getId()));
			CommandByWorkerTaskCommand commandByWorkerTaskCommand = (CommandByWorkerTaskCommand) c.getEventLife().getTask();
			m.put("command", placeholderVariableService.replace( ((CommandByWorkerTask)commandByWorkerTaskCommand.getSuperTask()).getCommand(), eventLife.getId() ) );
			m.put("osUser", placeholderVariableService.replace( ((CommandByWorkerTask)commandByWorkerTaskCommand.getSuperTask()).getOsUser(), eventLife.getId() ) );
			m.put("outputPattern", placeholderVariableService.replace( ((CommandByWorkerTask)commandByWorkerTaskCommand.getSuperTask()).getOutputPattern(), eventLife.getId() ) );
			return m;
		}).collect(Collectors.toList());
		LOGGER.debug("The commands to send back are: \n{}", result.stream().map(Map::toString).collect(Collectors.joining(", ")));
		return result;
	}
	
	
	// the commandByWorker tasks are executed asynchronously -> the worker can send back the result of the COMMAND_BY_WORKER task via this endpoint
	// TODO using PUT instead of POST!!!! this call has to be made idempotent!!!!
	@RequestMapping(value = "/command/result", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void setCommandResult(@RequestBody Map<String,String> commandResult) throws ReactionExecutionFlowException {
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		            "A command result has arrived! \n{}"+		
		            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", commandResult);
		EventLife eventLife = null;
		try {
			// get the date from the request and retrieve the EventLife
			Long eventLifeId = get("eventLifeId", commandResult, Long.class);
			String output = get("output", commandResult, String.class);
			String extractedValue = commandResult.get("extractedValue");
			boolean successful = get("successful", commandResult, String.class).toUpperCase().equals("TRUE");
			eventLife = eventLifeService.getEventLife(eventLifeId);
			MDC.put("uuid", eventLife.getEvent().getIdentifier());
			
			workerStatusService.refreshExecutorSendCommandResult(eventLife);
			
			// save the result of the command and update the status of EventLife record
			eventLife.setOutput(output);
			eventLife.setExtractedValue(extractedValue);
			eventLife.setEventDate(Calendar.getInstance());
			eventLife.setExtCommandSuccesful(successful);
			eventLife.setEventStatus(successful ? EventStatusEnum.FINISHED : EventStatusEnum.FAILED);
			eventLife.addToHistory();
			eventLifeService.save(eventLife);
			LOGGER.trace("The following eventLife is stored : {}", eventLife);
			// call the next() method of the current Command if successful
			if (eventLife.getExtCommandSuccesful()) {
				LOGGER.debug("The command was executed successfully so calling the next command.");
				eventLife.getTask().setEventLife(eventLife);
				eventLife.getTask().next(null);
			} else {
				LOGGER.warn("The command WASN'T executed successfully so the execution flow will be terminated!");
				// check if the event is not cancelled already
				Event event = eventService.getEvent(eventLife.getEvent().getId());
				if (!event.getStatus().equals(EventStatusEnum.CANCELLED)) {
					event.setStatus(EventStatusEnum.FAILED);
					event.setEndDate(Calendar.getInstance());
					Event savedEvent = eventService.save(event);
					LOGGER.trace("The following Even is saved: \n{}", savedEvent);
					// setting the values for the template
			        final Context context = new Context(Locale.UK);
			        context.setVariable("event_identifier", eventLife.getEvent().getIdentifier());
			        context.setVariable("task_name", eventLife.getTask().getSuperTask().getName());
			        context.setVariable("task_type", eventLife.getTask().getSuperTask().getClass().getSimpleName());
			        context.setVariable("task_host", eventLife.getTask().getSuperTask() instanceof CommandByWorkerTask ? ((CommandByWorkerTask)eventLife.getTask().getSuperTask()).getHost() : null);
			        context.setVariable("task_command", eventLife.getTask().getSuperTask() instanceof CommandByWorkerTask ? ((CommandByWorkerTask)eventLife.getTask().getSuperTask()).getCommand() : null);
			        context.setVariable("eventLife_output", eventLife.getOutput());
			        ExecutionFlow executionFlow = eventLife.getEvent().getErrorDetector() != null ? eventLife.getEvent().getErrorDetector().getExecutionFlow() : eventLife.getEvent().getExecutionFlow();
			        context.setVariable("executionFlow_name", executionFlowService.getExecutionFlow(executionFlow.getId()).getName());
			        context.setVariable("event_id", eventLife.getEvent().getId());
			        context.setVariable("web_gui_url", managementWebAppEndpoint);
			        // sending the mail
			        if (sendMail) {
				        List<String> recipients = null;
				        if (executionFlow.getErrorMailSendingRecipients() != null && executionFlow.getErrorMailSendingRecipients().size() > 0) {
				        	// if the error mail sending is checked in the get recipients from there
				        	recipients = executionFlow.getErrorMailSendingRecipients();
				        } else {
				        	// otherwise the recipients are the members of the access groups assigned to the flow
				        	recipients = authQuery.getEmailsByAccesGroups(executionFlow.getAccessGroups());
				        }
						mailSender.sendMail(recipients, "Reaction Engine - Error occured when executing a task of the execution flow on a host!", eventLife.getEvent(), context, "externalErrorMailTemplate");
			        }
				} else {
					LOGGER.info("The event was cancelled so no mail will be sent. event identifier = {}", event.getIdentifier());					
				}
			}
		} catch(Throwable t) {
			LOGGER.error("Error happened when processing the event!", t);
			if (eventLife != null) {
				errorHandler.handleError(eventLife.getEvent(), t);
			} else {
				LOGGER.warn("The event cannot be retrieved from the database so the error won't be attached to the any of the events!", t);
			}
		}
	}
	
	
	// it is called by the worker every X sec to refresh its cache 
	@RequestMapping(value = "/system/{host}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<EventSourceWithErrorDetectorPattern> refreshEventSource(@PathVariable String host) {
		LOGGER.debug("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
		             "The host [{}] wants to refresh its event source cache."+
		             "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", host);		
		workerStatusService.refreshReaderRefreshEventSources(host);
		
		List<EventSource> eventSources = eventSourceService.getEventSourceByHost(host);
		// get the pattern of the error detectors of the event sources or their parents or ...
		List<EventSourceWithErrorDetectorPattern> eventSourceWithPatternList = new ArrayList<>();
		for(EventSource eventSource : eventSources) {
			// collect all the eventSource ids
			List<Long> ids = new ArrayList<>();
			getAllIdsinEventSourceHierarchy(eventSource, ids);
			LOGGER.trace("getAllIdsinEventSourceHierarchy: {}", ids);
			// get the patterns from the error detectors
			List<ErrorDetector> errorDetectors = errorDetectorService.getErrorDetectorsByEventSourcedIds(ids);
			LOGGER.trace("errorDetectors: {}", errorDetectors);
			// send back only those eventSources where there is at least on active error detector
			if (errorDetectors.size() > 0) {
				List<Map<Long,String>> patterns = new ArrayList<>();
				for(ErrorDetector errorDetector : errorDetectors) {
					Map<Long, String> rec = new HashMap<>();
					rec.put(errorDetector.getId(), errorDetector.getMessagePattern());
					patterns.add(rec);
				}
				LOGGER.trace("patterns: {}", patterns);
				// create the EventSourceWithPattern
				eventSourceWithPatternList.add(new EventSourceWithErrorDetectorPattern(eventSource.getId(), eventSource.getName(), eventSource.getHost(), eventSource.getLogPath(), eventSource.getLogHeaderPattern(), eventSource.getLogHeaderPatternEnabled(),
						eventSource.getDescription(), eventSource.getType(), eventSource.getLogLevel(), null, null, patterns, eventSource.getEventSourceType(), eventSource.getIdentifiers()));
			}
		}
		LOGGER.debug("EventSource records to be sent back: \n{}", eventSourceWithPatternList.stream().map(EventSourceWithErrorDetectorPattern::toString).collect(Collectors.joining("\n")));
		return eventSourceWithPatternList;
	}

	
	private void getAllIdsinEventSourceHierarchy(EventSource eventSource, List<Long> ids) {
		ids.add(eventSource.getId());
		if (eventSource.getParent() != null) {
			getAllIdsinEventSourceHierarchy(eventSource.getParent(), ids);
		}
	}	
	

	// check if the event was already sent in by the worker (ie. there was a timeout exception while the worker sent in an event to be reported 
	//     -> due to the exception the client will send the event again but the engine already started to process it => 2 events in the engine instead of one!!)
	// 		added an index to the identifier column of the EVENT table	
	private boolean isInCache(String uuid) {
		synchronized (identifierCache) {
			boolean in = false;
			if (identifierCache.contains(uuid)) {
				LOGGER.warn("An event with the same identifier {} was already arrived to the controller! This one is ignored.", uuid);
				in = true;
			} else {
				// TODO investigate: perhaps I don't need the eventService.getEventByIdentifier(...) call as checking the identifierCache is enough
				Event existingEvent = eventService.getEventByIdentifier(uuid);
				if (existingEvent != null) {
					LOGGER.warn("An event with the same identifier {} was already processed! This one is ignored. The event to be found is {}", uuid, existingEvent);
					in = true;
				}
			}
			// adding to the cache
			identifierCache.add(uuid);
			return in;
		}
	}

}