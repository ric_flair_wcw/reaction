package org.reaction.engine.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class DjangoAuthQuery implements AuthQuery {

	private static final Logger LOGGER = LoggerFactory.getLogger(DjangoAuthQuery.class);
			
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	
	
	@Override
	// if the accessGroups is empty then all the users (mails addresses) have to retrieved that have the specific permission (the empty accessGroups parameter have to be handled differently in the 'getEmailsByAccesGroups' method)
	public List<String> getEmailsByPermissionsAndAccesGroups(List<String> permissions, List<String> accessGroups) {
		String sql = "SELECT DISTINCT u.email, pr.access_groups " +
	                 "FROM auth_permission p, auth_user u LEFT OUTER JOIN profile pr ON pr.user_id = u.id, auth_user_user_permissions up " +
	                 "WHERE p.id = up.permission_id AND up.user_id = u.id AND u.email IS NOT NULL AND LENGTH(TRIM(u.email)) > 0 AND p.codename IN (:permissions) ";
		MapSqlParameterSource parameters = new MapSqlParameterSource();
		parameters.addValue("permissions", permissions);		
		List<Map<String,Object>> result = jdbcTemplate.queryForList(sql, parameters);
		
		if (accessGroups == null) {
			return result.stream()
					     .map(r -> r.get("email").toString())
					     .collect(Collectors.toList());
		} else {
			return result.stream()
					     .filter(r -> {
					    	 if (r.get("access_groups") == null) {
					    		 return false;
					    	 } else {
					    		 List<String> common = Arrays.stream(  r.get("access_groups").toString().split(",")  ).map(ag -> ag.trim())
					    				                                                                              .filter(accessGroups::contains)
					    				                                                                              .collect(Collectors.toList());
					    		 return common.size() > 0;
					    	 }
					     })
					     .map(r -> r.get("email").toString())
					     .collect(Collectors.toList());
		}
	}

	
	@Override
	// if the accessGroups is empty then no email addresses will be retrieved
	public List<String> getEmailsByAccesGroups(List<String> accessGroups) {
		if (accessGroups == null || accessGroups.size() == 0) {
			return new ArrayList<>();
		}

		String sql = "SELECT DISTINCT u.email, pr.access_groups " +
                     "FROM auth_permission p, auth_user u LEFT OUTER JOIN profile pr ON pr.user_id = u.id, auth_user_user_permissions up " +
				     "WHERE p.id = up.permission_id AND up.user_id = u.id AND u.email IS NOT NULL AND LENGTH(TRIM(u.email)) > 0 ";
		List<Map<String,Object>> result = jdbcTemplate.queryForList(sql, new MapSqlParameterSource());
		LOGGER.debug("Emails + access_groups: {}", result.stream().map(m -> m.toString()).collect(Collectors.joining(", ")));
	
		return result.stream()
				     .filter(r -> {
				    	 if (r.get("access_groups") == null) {
				    		 return false;
				    	 } else {
				    		 List<String> common = Arrays.stream(  r.get("access_groups").toString().split(",")  ).map(ag -> ag.trim())
				    				                                                                              .filter(accessGroups::contains)
				    				                                                                              .collect(Collectors.toList());
				    		 return common.size() > 0;
				    	 }
				     })
				     .map(r -> r.get("email").toString())
				     .collect(Collectors.toList());
	}
	
}