package org.reaction.engine.auth;

import java.util.List;

public interface AuthQuery {

	List<String> getEmailsByPermissionsAndAccesGroups(List<String> permissions, List<String> accessGroups);
	
	List<String> getEmailsByAccesGroups(List<String> accessGroups);
	
}
