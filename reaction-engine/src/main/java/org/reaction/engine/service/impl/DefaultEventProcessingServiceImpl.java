package org.reaction.engine.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.reaction.common.contants.DataTypeEnum;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventSourceHierarchyTypeEnum;
import org.reaction.common.domain.ErrorDetector;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventSource;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.service.ErrorDetectorService;
import org.reaction.engine.persistence.service.EventSourceService;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.service.EventProcessingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.transaction.Transactional;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class DefaultEventProcessingServiceImpl implements EventProcessingService {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEventProcessingServiceImpl.class);
	
	@Autowired
	private ObjectMapper objectMapper;
	@Autowired
	private EventMachineService eventMachineService;
	@Autowired
	private EventSourceService eventSourceService;
	@Autowired
	private ErrorDetectorService errorDetectorService;
	// for H2 management console UI -> debug - expressions : org.h2.tools.Server.startWebServer(((javax.sql.DataSource)context.getBean("dataSource")).getConnection())
//	@Autowired
//	private ApplicationContext context;
	
	
	@Override
	@Transactional
	public HttpStatus process(EventSourceType eventSourceType,
							  String requestBody,
							  Map<String, String> queryParameters,
							  String uuid) throws XPathExpressionException, IOException, ReactionConfigurationException, ReactionExecutionFlowException {
		LOGGER.info("An event candidate has arrived. Event source type: {}\nQuery parameters: {}\nRequest body: {}", eventSourceType.getName(), queryParameters, requestBody);
		// get namespace map
		Map<String,String> namespaces = getNamespaces(eventSourceType.getNamespaces(), eventSourceType.getDataType());
		// unmarshalling the payload text to JSON or to XML
		Object payload = unmarshall(eventSourceType.getDataType(), requestBody);
		// extracting the information from payload
		Map<String, String> payloadValues = extractPayloadIdentifiers(payload, eventSourceType, namespaces);
		// getting the eventSource by the extracted info and the event source type
		EventSource eventSource = getEventSource(payloadValues, queryParameters, eventSourceType);
		// creating the event
		Event event = craftEvent(eventSource, uuid, payloadValues, queryParameters);
		// looking for a matching error detector
		ErrorDetector errorDetector = errorDetectorService.search(event, false, payloadValues, queryParameters, eventSourceType);
		// process the event if error detector is found
		if (errorDetector != null) {
			LOGGER.debug("I have found the following ErrorDetector for the event: \n{}", errorDetector);
			
			event.setErrorDetector(errorDetector);
			event.setExecutionFlow(errorDetector.getExecutionFlow());
			eventMachineService.processEvent(event);
			return HttpStatus.CREATED;
		} else {
			LOGGER.warn("No ErrorDetector record belongs to arrived event => it cannot be processed, it is ignored.");
			return HttpStatus.OK;
		}
	}


	@Override
	public List<String> checkIdentifiers(String message, List<String> identifiers, String dataTypeString, String namespacesString) throws ReactionConfigurationException {
		List<String> values = new ArrayList<>();
		DataTypeEnum dataType = DataTypeEnum.valueOf(dataTypeString);
		String payloadText = decodeBase64(message);
		LOGGER.debug("payloadText: {}", payloadText);
		Object payload = unmarshall(dataType, payloadText);
		if (payload != null) {
			Map<String, String> namespaces = getNamespaces(namespacesString, dataType);
			for (String identifier : identifiers) {
				try {
					values.add(extract(payload, identifier, dataType, namespaces));
				} catch (Exception e) {
					values.add(e.getMessage());
				}
				LOGGER.trace("value: {}", values.get(values.size() - 1));
			}
		}
		return values;
	}

	private String decodeBase64(String message) {
		try {
			return new String(Base64Utils.decodeFromString(message));
		} catch(Exception e) {
			throw new ReactionRuntimeException("Exception occurred when Base64 decoding the message!");
		}
	}


	private Event craftEvent(EventSource eventSource, String uuid, Map<String, String> payloadValues, Map<String, String> queryParameters) throws JsonProcessingException {
		return Event.builder().eventSourceId(eventSource.getId())
		                      .initiatedBy(EventInitiatorEnum.BY_EXTERNAL_SOURCE)
		                      .identifier(uuid)
		                      .payloadValues(objectMapper.writeValueAsString(payloadValues))
				 			  .queryParameters(objectMapper.writeValueAsString(queryParameters)).build();
	}


	private EventSource getEventSource(Map<String, String> payloadValues,
									   Map<String, String> queryParameters,
									   EventSourceType eventSourceType) throws ReactionConfigurationException {
		List<EventSource> foundEventSources = new ArrayList<>();
		final List<Map<String,String>> pathToIdentifiers;
		try {
			pathToIdentifiers = objectMapper.readValue(eventSourceType.getPathToIdentifiers(), new TypeReference<List<Map<String,String>>>() {});
		} catch (IOException e) {
			LOGGER.error("Error occurred when trying the parse the 'pathToIdentifiers' in the following EventSourceType: {}", eventSourceType);
			throw new ReactionConfigurationException("Error occurred when reading the JSON text from EventSourceType.pathToIdentifiers!", e);
		}
		// first get the eventSources that has the event source type
		List<EventSource> eventSources = eventSourceService.getEventSourceByEventSourceType(eventSourceType);
		// looping through these eventSources and check which one has the same values as payloadIdentifierValues and queryParameters
		for(EventSource eventSource : eventSources) {
			try {
				Map<String,String> identifiers = objectMapper.readValue(eventSource.getIdentifiers() == null ? "{}" : eventSource.getIdentifiers() ,
						                                                new TypeReference<Map<String,String>>() {});
				Map<String,String> identifiersPayload = identifiers.keySet().stream().filter(k -> k.endsWith("-PAYLOAD"))
						                                                             .collect(Collectors.toMap(k -> k.substring(0, k.length() - 8),
																							                   k -> identifiers.get(k)));
				Map<String,String> identifiersQueryParam = identifiers.keySet().stream().filter(k -> k.endsWith("-QUERY_PARAMETER"))
																						.collect(Collectors.toMap(k -> getPathFromPathToIdentifiersByName(pathToIdentifiers, k.substring(0, k.length() - 16)),
																												  k -> identifiers.get(k)));
				LOGGER.debug("payloadValues: {}   ?==   identifiersPayload: {}", payloadValues, identifiersPayload);
				LOGGER.debug("queryParameters: {}   ?==   identifiersQueryParam: {}", queryParameters, identifiersQueryParam);
				boolean foundInvalidPayloadIdentifier = identifiersPayload.keySet().stream().filter(k -> StringUtils.isEmpty(payloadValues.get(k)) ||
						                                                                                 !payloadValues.get(k).matches(identifiersPayload.get(k)))
																							.findFirst().isPresent();
				boolean foundInvalidQueryParam = identifiersQueryParam.keySet().stream().filter(k -> StringUtils.isEmpty(queryParameters.get(k)) ||
																									 !queryParameters.get(k).matches(identifiersQueryParam.get(k)))
																						.findFirst().isPresent();
				LOGGER.debug("foundInvalidPayloadIdentifier: {}, foundInvalidQueryParam: {}", foundInvalidPayloadIdentifier, foundInvalidQueryParam);
				if (!foundInvalidQueryParam && !foundInvalidPayloadIdentifier) {
					foundEventSources.add(eventSource);
				}
			} catch(IOException|NullPointerException e) {
				LOGGER.error("Error occurred when trying the parse the 'identifiers' in the following EventSource: {}", eventSource);
				throw new ReactionConfigurationException("Error occurred when reading the JSON text from EventSource.identifiers!", e);
			}
		}
		if (foundEventSources.size() == 0) {
            LOGGER.error("No event source could be found for the event source type [{}].\npayloadValues: {}\nqueryParameters: {}", eventSourceType.getName(), payloadValues, queryParameters);
            throw new ReactionRuntimeException("No event source could be found for the specific event source type and the identifiers!");
		} else if (foundEventSources.size() == 1) {
			LOGGER.debug("The found eventSource is {}.", foundEventSources.get(0));
			if (!EventSourceHierarchyTypeEnum.ITEM.equals(foundEventSources.get(0).getType())) {
				LOGGER.error("The incoming event belongs to a non-ITEM event source! The found event source is {}", foundEventSources.get(0));
				throw new ReactionRuntimeException("The incoming event belongs to a non-ITEM event source!");
			} else {
				return foundEventSources.get(0);
			}
		} else {
			LOGGER.error("More than lowest level event source are found but only one is expected! The found event sources are {}",
					foundEventSources.stream().map(s -> String.format("(id=%d, name=%s)", s.getId(), s.getName())).collect(Collectors.joining(", ")));
			throw new ReactionConfigurationException("More than one lowest level event sources are found but only one is expected!");
		}
	}


	private String getPathFromPathToIdentifiersByName(List<Map<String,String>> pathToIdentifiers, String name) {
		return pathToIdentifiers.stream().filter(m -> m.get("name").equals(name))
 										 .map(m -> m.get("path"))
										 .findFirst()
										 .orElse(name);
	}


	private Map<String,String> getNamespaces(String namespacesString, DataTypeEnum dataType) throws ReactionConfigurationException {
		if (!DataTypeEnum.XML.equals(dataType)) {
			return null;
		}
		Map<String,String> namespaces = new HashMap<>();
		if (namespacesString != null) {
			for(String _line : namespacesString.split("\\r?\\n")) {
				String line = _line.replace("\"", "");
				if (!line.contains("=")) {
				    LOGGER.error(String.format("The following namespace doesn't contain an equal sign! [{}]", _line));
					throw new ReactionConfigurationException("The namespace text doesn't contain an equal sign!"); 
				}
				if (line.indexOf("=") == 0 || line.indexOf("=") == line.length()-1) {
                    LOGGER.error(String.format("The following namespace doesn't contain URI or key! [{}]", _line));
					throw new ReactionConfigurationException("The namespace text doesn't contains the URI or the key!"); 
				}
				namespaces.put(line.substring(0,line.indexOf("=")), line.substring(line.indexOf("=")+1));
			}
		}
		LOGGER.debug("The namespaces that were extracted are [{}]", namespaces);
		return namespaces;
	}


	private Map<String, String> extractPayloadIdentifiers(Object payload,
														  EventSourceType eventSourceType,
														  Map<String, String> namespaces) throws XPathExpressionException, IOException, ReactionConfigurationException {
		Map<String, String> payloadIdentifierValues = new HashMap<>();
		List<Map<String,String>> identifiers = getIdentifiers(eventSourceType.getPathToIdentifiers());
		
		for(Map<String,String> identifier : identifiers) {
			if ("PAYLOAD".equals(identifier.get("source"))) {
				if (StringUtils.isEmpty(identifier.get("name")) || StringUtils.isEmpty(identifier.get("path"))) {
					LOGGER.error("The 'name' or 'path' attributes are not found in the 'pathToIdentifiers' element in the EventSourceType [{}].", eventSourceType.getName());
					throw new ReactionConfigurationException(String.format("The 'name' or 'path' attributes are not found in the 'pathToIdentifiers' element in the EventSourceType [%s].", eventSourceType.getName()));
				}
				payloadIdentifierValues.put(identifier.get("name"),
                                            extract(payload, identifier.get("path"), eventSourceType.getDataType(), namespaces));
			}
		}
		LOGGER.debug("The extracted values: {}", payloadIdentifierValues);
		return payloadIdentifierValues;
	}

	
	private List<Map<String, String>> getIdentifiers(String pathToIdentifiers) throws IOException, ReactionConfigurationException {
		if (StringUtils.isEmpty(pathToIdentifiers)) {
			throw new ReactionConfigurationException("The PATH_TO_IDENTIFIERS is empty!");
		}
		return objectMapper.readValue(pathToIdentifiers, new TypeReference<List<Map<String,String>>>(){});
	}


	private String extract(Object payload, String path, DataTypeEnum dataType, Map<String,String> namespaces) throws XPathExpressionException {
		LOGGER.trace("Extracting the path [{}] from the [{}] object with the namespaces [{}].", path, dataType, namespaces);
		String value = null;
		if (dataType != null) {
			switch (dataType) {
				case JSON:
					value = ((DocumentContext) payload).read(path);
					break;
				case XML:
					XPath xPath = XPathFactory.newInstance().newXPath();
					xPath.setNamespaceContext(
							new NamespaceContext() {
								@Override
								public Iterator<String> getPrefixes(String arg0) {
									return null;
								}

								@Override
								public String getPrefix(String arg0) {
									return null;
								}

								@Override
								public String getNamespaceURI(String key) {
									return namespaces.get(key);
								}
							});
					value = (String) xPath.compile(path).evaluate((Document) payload, XPathConstants.STRING);
					break;
			}
		}
		LOGGER.trace("The extracted value is {}.", value);
		return value;
	}


	private Object unmarshall(DataTypeEnum dataType, String requestBody) {
		if (dataType == null) {
			return null;
		}
		switch(dataType) {
			case JSON:
				return StringUtils.isEmpty(requestBody) ? null : JsonPath.parse(requestBody);
			case XML:
				try {
					if (StringUtils.isEmpty(requestBody)) {
						return null;
					} else {
						InputSource source = new InputSource(new StringReader(requestBody));

						DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
						documentBuilderFactory.setNamespaceAware(true);
						DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
						return documentBuilder.parse(source);
					}
				} catch(IOException|ParserConfigurationException|SAXException e) {
					throw new ReactionRuntimeException("Error occurred when unmarhalling the request to XML!", e);
				}
			case NONE:
				return null;
			default:
				throw new ReactionRuntimeException(String.format("Unknown data type [%s]!", dataType));
		}
	}

}
