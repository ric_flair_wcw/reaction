package org.reaction.engine.service;

import org.reaction.common.domain.Event;
import org.reaction.common.exception.ReactionExecutionFlowException;

public interface EventMachineService {

	
	void processEvent(Event event) throws ReactionExecutionFlowException;

	boolean isSameExecutionFlowNotOperated(Event event, Event latestStartedEvent);
	
}
