package org.reaction.engine.service.impl;

import org.reaction.common.contants.TaskEnum;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.reaction.engine.persistence.model.TaskEntity;
import org.reaction.engine.persistence.repository.EventLifeEntityRepository;
import org.reaction.engine.persistence.repository.TaskEntityRepository;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class DefaultPlaceholderVariableServiceImpl implements PlaceholderVariableService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPlaceholderVariableServiceImpl.class);
    public static final String PLACEHOLDER_NOT_FOUND = "[!!!NOT-FOUND!!!]";

    private Pattern pattern = Pattern.compile("##\\{.+\\}##");
    @Autowired
    private EventLifeEntityRepository eventLifeEntityRepository;
    @Autowired
    private TaskEntityRepository taskEntityRepository;
    @Autowired
    private EventLifeService eventLifeService;


    @Transactional
    public String replace(final String fieldValue, Long eventLifeId) {
        LOGGER.debug("The placeholder variables will be replaced in the field [{}], event life id is {}", fieldValue, eventLifeId);
        EventLifeEntity eventLifeEntity = eventLifeEntityRepository.getOne(eventLifeId);
        String replacedFieldValue = _replace(fieldValue, eventLifeEntity.getTaskEntity(), eventLifeEntity);
        LOGGER.debug("The replaced value is [{}].", replacedFieldValue);
        return replacedFieldValue;
    }


    private String _replace(final String _fieldValue, TaskEntity _taskEntity, EventLifeEntity eventLifeEntity) {
        String fieldValue = _fieldValue;
        TaskEntity taskEntity = taskEntityRepository.getOne(_taskEntity.getId());
        Matcher matcher = pattern.matcher(fieldValue);
        if (matcher.find()) {
            if (TaskEnum.LOOP.equals(taskEntity.getInternalTask())) {
                Integer loopIndex = eventLifeService.getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(eventLifeEntity.getEventEntity().getId(), taskEntity.getId());
                loopIndex = loopIndex == null ? 0 : loopIndex;

                if (loopIndex != null) {
                    fieldValue = replaceAll(fieldValue, taskEntity.getLoopPlaceholderVar1(), taskEntity.getLoopValueList1(), taskEntity.getLoopSeparator1(), loopIndex);
                    fieldValue = replaceAll(fieldValue, taskEntity.getLoopPlaceholderVar2(), taskEntity.getLoopValueList2(), taskEntity.getLoopSeparator2(), loopIndex);
                    fieldValue = replaceAll(fieldValue, taskEntity.getLoopPlaceholderVar3(), taskEntity.getLoopValueList3(), taskEntity.getLoopSeparator3(), loopIndex);
                    fieldValue = replaceAll(fieldValue, taskEntity.getLoopPlaceholderVar4(), taskEntity.getLoopValueList4(), taskEntity.getLoopSeparator4(), loopIndex);
                }
            }
            if (taskEntity.getExecutionFlowEntity() != null) {
                return fieldValue.replaceAll("##\\{.+\\}##", PLACEHOLDER_NOT_FOUND);
            } else {
                return _replace(fieldValue, taskEntity.getPrimaryTaskEntity() != null ? taskEntity.getPrimaryTaskEntity() : taskEntity.getSecondaryTaskEntity(), eventLifeEntity);
            }
        } else {
            return fieldValue;
        }
    }

    private String replaceAll(String fieldValue, String placeholderVar, String valueList, String separator, int loopIndex) {
        if (!StringUtils.isEmpty(valueList)) {
            return fieldValue.replace("##{" + placeholderVar + "}##",
                                      valueList.split(separator)[loopIndex]);
        } else {
            return fieldValue;
        }
    }
}
