package org.reaction.engine.service.impl;

import org.reaction.common.contants.*;
import org.reaction.common.domain.*;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.auth.AuthQuery;
import org.reaction.engine.mail.ReactionMailSender;
import org.reaction.engine.persistence.service.*;
import org.reaction.engine.scheduling.Scheduler;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.service.ExecutionFlowMachineService;
import org.reaction.engine.tasks.IfElseTaskCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class DefaultExecutionFlowMachineServiceImpl implements ExecutionFlowMachineService {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultExecutionFlowMachineServiceImpl.class);
	
	@Autowired
	private EventService eventService;
	@Autowired
	private EventLifeService eventLifeService;
	@Autowired
	private TaskService taskService;
	@Autowired
	private Scheduler scheduler;
	@Autowired
	private ExecutionFlowService executionFlowService;
	@Autowired
	private ErrorDetectorService errorDetectorService;
	@Autowired
	private EventSourceService eventSourceService;
	@Autowired
	private AuthQuery authQuery;
	@Autowired
	private ReactionMailSender reactionMailSender;
	@Autowired
	private EventMachineService eventMachineService;

	@Value("${reaction.management_web_app.endpoint}")
	private String managementWebAppEndpoint;
	@Value("#{new Boolean('${reaction.mail.enabled.when_starting_a_flow}'.trim())}")
	Boolean sendMailWhenStartingFlow;
	@Value("#{new Boolean('${reaction.mail.enabled.when_confirmation_is_needed}'.trim())}")
	Boolean sendMailWhenConfirmationIsNeeded;
	
	
	@Override
	public Event startIt(Event event) throws ReactionExecutionFlowException {
		Event savedEvent = null;
		Calendar now = Calendar.getInstance();
		ExecutionFlow executionFlow = event.getExecutionFlow();
		// if confirmation is needed to start the execution flow then it mustn't be started
		//     ONLY if the initiator is the LOG (scheduled and manual execution will be started immediately even if the confirmation needed flag is true)
		//          or if the event was just confirmed then it must go through
		//          or if the status is SCHEDULED (avoiding the following scenario: CONFIRMATION_NEEDED -> SCHEDULED -> CONFIRMATION_NEEDED -> ...)
		if ( (event.getStatus() != null && (event.getStatus().equals(EventStatusEnum.CONFIRMED) ||
				                            event.getStatus().equals(EventStatusEnum.CONFIRMED_AND_FORCED_START) ||
				                            event.getStatus().equals(EventStatusEnum.SCHEDULED))) ||
				( !(isConfirmationNeeded(event.getErrorDetector()) && (event.getInitiatedBy().equals(EventInitiatorEnum.BY_LOG) || event.getInitiatedBy().equals(EventInitiatorEnum.BY_EXTERNAL_SOURCE))) ) ) {
			// ok, when the worker reports an event then it will send the id of a ITEM and this event source (ITEM) has to be used
			// the event.getErrorDetector().getEventSource() can be an top level GROUP => not all the properties of the APPLICATION event source can be determined (as there are some that are specified on the deeper level)
			EventSource eventSource = event.getEventSourceId() == null ? null : eventSourceService.getEventSourceAndFillProperties(event.getEventSourceId());
			LOGGER.trace("eventSource: {}", eventSource);
			// if status is CONFIRMED_AND_FORCED_START then start it
			// if it is started manually or by the scheduler or no maintenance window is set or maintenance window is set for the eventSource and we are in this window then start it
			if ( EventStatusEnum.CONFIRMED_AND_FORCED_START.equals(event.getStatus()) || event.getInitiatedBy().equals(EventInitiatorEnum.MANUALLY) || event.getInitiatedBy().equals(EventInitiatorEnum.BY_SCHEDULER) ||
					(eventSource.getMaintenanceWindow() == null || eventSource.getMaintenanceWindow().isInMaintenanceWindow(executionFlow.getExecutionTime())) ) {
				// SCHEDULED -> IGNORED, CONFIRMED -> IGNORED, CONFIRMED_AND_FORCED_START -> IGNORED if there is already a flow having STARTED or FINSIHED(with timeBetweenExecutions specified) status
				Event latestStartedOrFinishedEvent = isSameFlowRunningOrFinished(event);
				if ( (EventStatusEnum.CONFIRMED.equals(event.getStatus()) || EventStatusEnum.CONFIRMED_AND_FORCED_START.equals(event.getStatus()) || EventStatusEnum.SCHEDULED.equals(event.getStatus())) &&
						latestStartedOrFinishedEvent != null && !eventMachineService.isSameExecutionFlowNotOperated(event, latestStartedOrFinishedEvent)) {
					ignoreEvent(event, "The event is ignored as a same execution flow is (was recently) under operation already! The identifier of this other event is " + latestStartedOrFinishedEvent.getIdentifier() + 
							" (its status is " + latestStartedOrFinishedEvent.getStatus().toString() + ").");
				} else {
					LOGGER.info("A executionFlow will be started for the following event\n{}.", event);
					// if the execution flow is not frozen yet (i.e. making sure that it cannot be modified) then freeze it
					if (!executionFlow.getStatus().equals(ExecutionFlowStatusEnum.FROZEN)) {
						executionFlow.setStatus(ExecutionFlowStatusEnum.FROZEN);
						executionFlowService.changeStatus(executionFlow, ExecutionFlowStatusEnum.FROZEN);
					}
					// storing an EventEntity
					event.setStartDate(now);
					event.setStatus(EventStatusEnum.STARTED);
					savedEvent = eventService.save(event);
					LOGGER.debug("The Even is updated: \n{}", savedEvent);
					// querying the first task in the executionFlow
					SuperCommand command = taskService.getTopByExecutionFlowEntityOrderByOrderAsc( executionFlow );
					LOGGER.debug("The following Command has been found with getTopByExecutionFlowEntityOrderByOrderAsc: \n{}", command);
					if (command != null) {
						doFinalCheck(savedEvent);
						// and storing an eventLifeEntity
						EventLife eventLife = EventLife.builder()
													   .eventDate(now)
													   .event(savedEvent)
													   .task(command)
													   .eventStatus(EventStatusEnum.STARTED)
													   .order(0).build();
						eventLife.addToHistory();
						EventLife savedEventLife = eventLifeService.save(eventLife);
						LOGGER.debug("The following EvenLife is saved: \n{}", savedEventLife);
						// starting the first task of the executionFlow
						eventLife.setId(savedEventLife.getId());
						command.execute( eventLife );
						// send a mail about the start
						if (sendMailWhenStartingFlow) {
							reactionMailSender.sendMailAboutStart(executionFlow, savedEvent);
						}
					} else {
						// no task in the execution flow but it means that it was successful... (the start and end date will be the same)
						savedEvent.setStatus(EventStatusEnum.FINISHED);
						savedEvent.setEndDate(now);
						eventService.save(savedEvent);
						LOGGER.warn("The executionFlow doesn't contain any task to be executed! execution flow id = {}", executionFlow.getId());
					}
				}
			}
			// the executionFlow cannot be started now but schedule it to run in the next maintenance window
			else {
				LOGGER.info("The event has to be scheduled as it is not in the maintenance window\n{}.", event);
				Event latestStartedOrFinishedEvent = isSameFlowRunningOrFinished(event);
				LOGGER.trace("latestStartedOrFinishedEvent = {}", latestStartedOrFinishedEvent);
				long deltaInMilliSec = 0;
				// don't do anything as the currently running flow is to remedy this issue -> it doesn't have to be scheduled but ignored the event with meaningful reason text
				if (latestStartedOrFinishedEvent != null && EventStatusEnum.STARTED.equals(latestStartedOrFinishedEvent.getStatus())) {
					String message = "The event is ignored as a same execution flow is already running (which will remedy this problem too, fingers cross... :) )! The identifier of this other event is " + 
							latestStartedOrFinishedEvent.getIdentifier() + ").";
					LOGGER.warn(message);
					ignoreEvent(event, message);
				}
				else {
					// check if the same flow is already scheduled and initiated BY_LOG and the this event and the current (incoming) event happened on the same day 
					Event closestScheduledAndInitiatedByLogEvent = isSameFlowScheduledAndInitiatedByLog(event);
					LOGGER.trace("latestScheduledAndInitiatedByLogEvent = {}", closestScheduledAndInitiatedByLogEvent);

					LocalDate startDateEvent = event.getStartDate() == null ? LocalDate.now() : event.getStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
					LocalDate startDateClosestScheduledAndInitiatedByLogEvent = closestScheduledAndInitiatedByLogEvent != null ? 
							closestScheduledAndInitiatedByLogEvent.getStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate() : null;
					// if the 2 start dates are on the same day
					if (startDateClosestScheduledAndInitiatedByLogEvent != null && startDateEvent.isEqual(startDateClosestScheduledAndInitiatedByLogEvent)) {
						String message = "The event is ignored as a same execution flow is already scheduled for today! The identifier of this other event is " + closestScheduledAndInitiatedByLogEvent.getIdentifier() + ").";
						LOGGER.warn(message);
						ignoreEvent(event, message);
					}
					else {
						Calendar next = eventSource.getMaintenanceWindow().nextInMaintenanceWindow(event.getExecutionFlow().getExecutionTime());
	
						if (latestStartedOrFinishedEvent != null && EventStatusEnum.FINISHED.equals(latestStartedOrFinishedEvent.getStatus())) {
							// the maintenance window starts from 19:00  and  a same flow has just finished at 18:50 and the timeBetweenExecutions is 60*60 sec
							// now it is 18:55 -> the flow will be scheduled to run to 19:00 but it should start at 19:50 (however when calculate it the end timeperiod of maintenance window should be taken into consideration ! not to calculate a time which is after (the enddate - execution time) )
							Integer timeBetweenExecutionsInSec = latestStartedOrFinishedEvent.getExecutionFlow().getTimeBetweenExecutions();
							if (timeBetweenExecutionsInSec != 0) {
								// if 18:50 + 60 min > 19:00 then add 50 min to 19:00
								if (latestStartedOrFinishedEvent.getEndDate().getTimeInMillis() + timeBetweenExecutionsInSec * 1000 > next.getTimeInMillis()) {
									deltaInMilliSec = latestStartedOrFinishedEvent.getEndDate().getTimeInMillis() - next.getTimeInMillis() + timeBetweenExecutionsInSec * 1000;
									LOGGER.debug("The next running date will be altered with the following delta (in millisec): {}.", deltaInMilliSec);
									next.setTimeInMillis(next.getTimeInMillis() + deltaInMilliSec);
								}
							}
						}
	
						// storing the event and eventLife
						event.setStatus(EventStatusEnum.SCHEDULED);
						event.setStartDate(next);
						savedEvent = eventService.save(event);
						LOGGER.debug("The event below is going to run on {}\n{}", Constants.getDateFormat().format(next.getTime()), savedEvent);
						EventLife eventLife = EventLife.builder()
								                       .eventDate(now)
								                       .event(savedEvent)
								                       .eventStatus(EventStatusEnum.SCHEDULED)
								                       .order(-1).build();
						eventLife.addToHistory();
						eventLifeService.save(eventLife);
						
						// scheduling it to run in the maintenance window
						scheduler.schedule(savedEvent);
					}
				}
			}
		}
		// manual confirmation needed on the web application only ...
		else {
			LOGGER.info("The following event needs to be confirmed on Administration App.\n{}", event);
			// save a eventLife
			event.setStatus(EventStatusEnum.CONFIRMATION_NEEDED);
			event.setStartDate(Calendar.getInstance());
			savedEvent = eventService.save(event);
			EventLife eventLife = EventLife.builder()
										   .eventDate(now)
										   .event(savedEvent)
										   .eventStatus(EventStatusEnum.CONFIRMATION_NEEDED)
										   .order(-2).build();
			eventLife.addToHistory();
			eventLifeService.save(eventLife);
			// send mail to the users who can approve or reject
			if (sendMailWhenConfirmationIsNeeded) {
				sendMailAboutConfirmation(executionFlow, eventLife);
			}
		}
		return savedEvent;
	}


	@Override
	@Transactional
	public void restartEventLife(EventLife eventLife, String user) throws ReactionExecutionFlowException {
		// set & save the event 
		Event event = eventLife.getEvent();
		event.setEndDate(null);
		event.setStatus(EventStatusEnum.STARTED);
		Event savedEvent = eventService.save(event);
		
		// set & save the failed event life
		eventLife.setEventDate(Calendar.getInstance());
		eventLife.setEventStatus(EventStatusEnum.FAILED_AND_RESTARTED);
		eventLife.setByWhom(user);
		eventLife.addToHistory();
		eventLifeService.save(eventLife);
		
		// storing an EventLife record for the next task
		EventLife newEventLife = EventLife.builder()
				   					   .eventDate(Calendar.getInstance())
				   					   .event(savedEvent)
				   					   .task(eventLife.getTask())
				   					   .eventStatus(EventStatusEnum.STARTED)
				   					   .order(eventLife.getOrder() + 1).build();
		eventLife.addToHistory();
		EventLife savedNewEventLife = eventLifeService.save( newEventLife );
		// executing the execute() method of the next task
		eventLife.getTask().execute( savedNewEventLife );
	}


	@Override
	@Transactional
	public void skipEventLife(EventLife eventLife, String user, String value) throws ReactionExecutionFlowException {
		// search for the next task after the one belongs to the current eventLife
		SuperCommand nextCommand = taskService.getNextTaskRecursively( eventLife.getTask().getSuperTask() );

		// if there is then ...
		if (nextCommand != null) {
			// set & save the event 
			Event event = eventLife.getEvent();
			event.setEndDate(null);
			event.setStatus(EventStatusEnum.STARTED);
			Event savedEvent = eventService.save(event);
			
			// set & save the failed event life
			eventLife.setEventDate(Calendar.getInstance());
			eventLife.setEventStatus(EventStatusEnum.FAILED_AND_SKIPPED);
			eventLife.setByWhom(user);
			//     if the next command is an IF_ELSE then put the value to the current eventLife
			if (nextCommand instanceof IfElseTaskCommand) {
				if (value != null && !value.isEmpty()) {
					eventLife.setExtractedValue(value);
				} else {
					throw new ReactionRuntimeException("The event life has to be skipped but the next task is an IF-ELSE and no value was provided to evaluate the IF-ELSE condition!");
				}
			}
			eventLife.addToHistory();
			eventLifeService.save(eventLife);
			
			// storing an EventLife record for the next task
			EventLife newEventLife = EventLife.builder()
					   					      .eventDate(Calendar.getInstance())
					   					      .event(savedEvent)
					   					      .task(nextCommand)
					   					      .eventStatus(EventStatusEnum.STARTED)
					   					      .order(eventLife.getOrder() + 1).build();
			eventLife.addToHistory();
			EventLife savedNewEventLife = eventLifeService.save( newEventLife );
			// executing the execute() method of the next task
			nextCommand.execute( savedNewEventLife );
		}
	}
	
	
	private boolean isConfirmationNeeded(ErrorDetector errorDetector) {
		return errorDetector == null ? false : (errorDetector.getConfirmationNeeded() == null ? false : errorDetector.getConfirmationNeeded());
	}


	private Event isSameFlowRunningOrFinished(Event event) {
		List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.FINISHED);
		Event latestStartedFinishedEvent = eventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, false);
		if (latestStartedFinishedEvent == null) {
			return null;
		} else if (EventStatusEnum.STARTED.equals(latestStartedFinishedEvent.getStatus())) {
			return latestStartedFinishedEvent;
		} else if (EventStatusEnum.FINISHED.equals(latestStartedFinishedEvent.getStatus())) {
			return latestStartedFinishedEvent.getExecutionFlow().getExecutionTime() == null ? null : latestStartedFinishedEvent;
		} else {
			return null;
		}
	}

	
	private Event isSameFlowScheduledAndInitiatedByLog(Event event) {
		return eventService.getClosestScheduledEventsByExecutionFlow(event.getExecutionFlow());
	}

	
	private void ignoreEvent(Event event, String reasonText) {
		LOGGER.warn(reasonText);
		event.setStatus(EventStatusEnum.IGNORED);
		event.setReason(reasonText);
		Calendar now = Calendar.getInstance();
		if (event.getStartDate() == null) {
			event.setStartDate(now);
		}
		event.setEndDate(now);
		Event savedNewEvent = eventService.save(event);
		LOGGER.debug("The following Event is saved: \n{}", savedNewEvent);
	}
	
	// do final check before the start
	//  1. check if the execution flow is still valid (it was scheduled but meanwhile somebody broke the flow)
	private void doFinalCheck(Event event) throws ReactionExecutionFlowException {
		// reload the execution flow
		ExecutionFlow executionFlow = event.getExecutionFlow();
		if (event.getErrorDetector() != null) {
			ErrorDetector errorDetector = errorDetectorService.getErrorDetectorById(event.getErrorDetector().getId());
			executionFlow = errorDetector.getExecutionFlow();
		}
		LOGGER.trace("doFinalCheck - executionFlow : {}", executionFlow);
		executionFlow = executionFlowService.getExecutionFlow(executionFlow.getId());
		if (ExecutionFlowStatusEnum.INVALID.equals(executionFlow.getStatus())) {
			throw new ReactionRuntimeException("The execution flow (" + executionFlow.getName() + ") is not valid, it cannot be started!");
		}
	}


	private void sendMailAboutConfirmation(ExecutionFlow executionFlow, EventLife eventLife) {
		// send mail - to the users who has can_use_approval permission and has access (i.e. are in the access group that the flow has) to the 
		//   execution flow
		//           - to all the users who has can_use_approval permission if the access group property of the flow is empty
		try {
			List<String> permissions = Collections.singletonList(PermissionEnum.CAN_USE_APPROVAL.getCodename());
			List<String> emailAddresses = authQuery.getEmailsByPermissionsAndAccesGroups(permissions, executionFlow.getAccessGroups());
			// setting the values for the template
	        final Context context = new Context(Locale.UK);
	        context.setVariable("event_identifier", eventLife.getEvent().getIdentifier());
	        context.setVariable("executionFlow_name", executionFlowService.getExecutionFlow(executionFlow.getId()).getName());
	        context.setVariable("event_id", eventLife.getEvent().getId());
	        context.setVariable("web_gui_url", managementWebAppEndpoint);
	        // sending the mail
	        if (emailAddresses != null && emailAddresses.size() > 0) {
				reactionMailSender.sendMail(emailAddresses, 
						                    "Reaction Engine - Confirmation needed for the flow '" + executionFlow.getName() + "'",
						                    eventLife.getEvent(),
						                    context,
						                    "confirmationNeededMailTemplate");
	        }
		} catch(Exception e) {
			LOGGER.warn("Warning! No mail can be sent to the users to confirm the event!", e);
		}
	}


}
