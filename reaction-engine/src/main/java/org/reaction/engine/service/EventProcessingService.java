package org.reaction.engine.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.springframework.http.HttpStatus;

import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface EventProcessingService {

	public HttpStatus process(EventSourceType eventSourceType, String requestBody, Map<String, String> queryParameters, String uuid) throws XPathExpressionException, JsonParseException, JsonMappingException, IOException, ReactionConfigurationException, ReactionExecutionFlowException;

	List<String> checkIdentifiers(String message, List<String> identifiers, String dataType, String namespaces) throws ReactionConfigurationException;
}
