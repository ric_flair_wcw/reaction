package org.reaction.engine.service.impl;

import org.reaction.common.contants.AuthenticationTypeEnum;
import org.reaction.common.domain.EventSourceType;
import org.reaction.engine.errors.ResourceException;
import org.reaction.engine.service.SecurityService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class DefaultSecurityServiceImpl implements SecurityService {


    private static final String URL_PARAM_SECRET = "secret";
    static final String HEADER_BASIC_AUTH = "Authorization";

    @Override
    public void check(EventSourceType eventSourceType, HttpServletRequest req) {
        // enforce SSL if it is set
         if (eventSourceType.getEnforceSsl() && !req.isSecure()) {
            throw new ResourceException(HttpStatus.FORBIDDEN, "The event source has to establish an SSL connection but normal HTTP request arrived!");
        }
        // check authentication if it is set
        if (eventSourceType.getAuthenticationType().equals(AuthenticationTypeEnum.SECRET)) {
            if (req.getParameterMap().containsKey(URL_PARAM_SECRET) && req.getParameterMap().get(URL_PARAM_SECRET).length == 1) {
                String hashedSecret = hash(req.getParameterMap().get(URL_PARAM_SECRET)[0]);
                if (!eventSourceType.getSecret().equals(hashedSecret)) {
                    throw new ResourceException(HttpStatus.UNAUTHORIZED, "The secret in the request is not equal to the one in the database!");
                }
            } else {
                throw new ResourceException(HttpStatus.UNAUTHORIZED, "The secret is not in the request but the authentication type is SECRET!");
            }
        } else if (eventSourceType.getAuthenticationType().equals(AuthenticationTypeEnum.BASIC_AUTH)) {
            String authString = getBasicAuthFromHeader(req);
            String[] credentials = getCredentials(authString);
            String hashedPassword = hash(credentials[1]);
            if (!(eventSourceType.getUser().equals(credentials[0]) &&
                  eventSourceType.getSecret().equals(hashedPassword))) {
                throw new ResourceException(HttpStatus.UNAUTHORIZED, "The user+password in the request header is not equal to the ones in the database!");
            }
        }
    }

    private String hash(String password) {
        try {
            String algorithm = "HmacSHA512";
            final Mac digest = Mac.getInstance(algorithm);
            byte[] apiSecret = "ricflairwcw".getBytes(StandardCharsets.UTF_8);
            SecretKeySpec secretKey = new SecretKeySpec(apiSecret, algorithm);
            digest.init(secretKey);
            digest.update(password.getBytes(StandardCharsets.UTF_8));
            final byte[] signatureBytes = digest.doFinal();
            digest.reset();
            return DatatypeConverter.printBase64Binary(signatureBytes);
        } catch(NoSuchAlgorithmException | InvalidKeyException e) {
            throw new ResourceException(HttpStatus.INTERNAL_SERVER_ERROR, "The password hashing cannot be performed!", e);
        }
    }

    private String[] getCredentials(String authString) {
        if (StringUtils.isEmpty(authString) || !authString.contains(":")) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "The BASIC AUTH info in the header is invalid!");
        }
        if (authString.indexOf(":") == 0) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "The BASIC AUTH info in the header doesn't contain the user name!");
        }
        if (!(authString.indexOf(":") < authString.length() - 1)) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "The BASIC AUTH info in the header doesn't contain the password!");
        }
        String user = authString.substring(0, authString.indexOf(":"));
        String password = authString.substring(authString.indexOf(":") + 1);
        return new String[] {user, password};
    }


    private String getBasicAuthFromHeader(HttpServletRequest req) {
        String authString = req.getHeader(HEADER_BASIC_AUTH);
        if (authString == null) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "The request header doesn't contain the BASIC AUTH header!");
        }
        String[] authParts = authString.split("\\s+");
        if (authParts.length != 2) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "The request header doesn't contain the BASIC AUTH header!");
        }
        String authInfo = authParts[1];
        try {
            return new String(Base64Utils.decodeFromString(authInfo));
        } catch(RuntimeException e) {
            throw new ResourceException(HttpStatus.UNAUTHORIZED, "The BASIC AUTH info couldn't be decoded from the request header!", e);
        }
    }

}