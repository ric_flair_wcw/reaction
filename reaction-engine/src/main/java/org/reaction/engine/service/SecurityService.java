package org.reaction.engine.service;

import org.reaction.common.domain.EventSourceType;

import javax.servlet.http.HttpServletRequest;

public interface SecurityService {

    void check(EventSourceType eventSourceType, HttpServletRequest req);

}
