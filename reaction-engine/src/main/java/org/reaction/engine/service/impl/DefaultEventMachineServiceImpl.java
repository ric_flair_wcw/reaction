package org.reaction.engine.service.impl;

import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.service.EventMachineService;
import org.reaction.engine.service.ExecutionFlowMachineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


@Service
public class DefaultEventMachineServiceImpl implements EventMachineService {


	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEventMachineServiceImpl.class);
	
	@Autowired
	private EventService eventService;
	@Autowired
	private ExecutionFlowMachineService executionFlowMachineService;
	@Autowired
	private ErrorHandler errorHandler;
	
	
	/*
	 * The Reader worker can report an event occurred in the log
	 * case 1 - the executionFlow needs only one event to be started AND 
	 *          the same executionFlow hasn't been started recently (the same executionFlow started/finished recently + ExecutionFlowEntity.timeBetweenExecutions > current time)
	 *        => start it
	 *        -> if the 2nd condition doesn't fulfill then ignore the error
	 * case 2 - the executionFlow needs more events to be started AND 
	 *          this exception is the n-times one in the given period AND
	 *          the same executionFlow hasn't been started recently
	 *        => start it
	 *        -> if the exception is the first one then store an Event record with WAITING_FOR_OTHERS status and counter=1 
	 *        -> if the exception is not the first one but not the last then 
	 *        		if we are in the timeframe (first EventLife timestamp - current timestamp > ExecutionFlowEntity.multipleEventsTimeframe) then
	 *        			update the latest event's counter (counter = counter + 1)  (-> the incoming event will be lost which is good)
	 *        		else 
	 *        			the event mustn't be lost so store the incoming event with WAITING_FOR_OTHERS status and counter=1
	 *        	 else if the counter of the latest event is equal to the limit then the execution flow has started already but exception is
	 *        		 the event mustn't be lost so store the incoming event with WAITING_FOR_OTHERS status and counter=1
	 */
	@Override
	@Transactional
	public void processEvent(final Event event) throws ReactionExecutionFlowException {
		LOGGER.debug("Starting the processEvent method. \n{}", event);
		
		try {
			// get the latest event and latest waiting event
			// SELECT ... FOR UPDATE when I check if the same flow is not started, under confirmation or finished already
			List<EventStatusEnum> statuses = Arrays.asList(EventStatusEnum.STARTED, EventStatusEnum.CONFIRMATION_NEEDED, EventStatusEnum.FINISHED);
			Event latestButNotScheduledEvent = eventService.getLatestEventsByExecutionFlow(event.getExecutionFlow(), statuses, true);
			LOGGER.trace("latestButNotScheduledEvent = {}", latestButNotScheduledEvent);
			Event latestWaitingEvent = eventService.getLatestWaitingEventByExecutionFlow(event.getExecutionFlow());
			LOGGER.trace("latestWaitingEvent = {}", latestWaitingEvent);
			
			// getting the status for the executionFlow start
			Status status = getStatus(Status.THE_BEGINNING, event, latestWaitingEvent, latestButNotScheduledEvent);
			
			// start the executionFlow
			switch(status) {
				case START_SINGLE: 		executionFlowMachineService.startIt(event);
								   		break;
				case START_MULTIPLE: 	executionFlowMachineService.startIt(latestWaitingEvent);
				   					 	break;
				case END_OF_PROCESSING: // doing nothing
										break;
				default: throw new ReactionExecutionFlowException("Unexpected status when starting the executionFlow: " + status.name());
			}
		} catch(Throwable t) {
			LOGGER.error("Error happened when processing the event!", t);
			errorHandler.handleError(event, t);
		}
	}
	

	// the same executionFlow hasn't been started,confirmed,scheduled recently (the same executionFlow started/finished recently + ExecutionFlowEntity.timeBetweenExecutions > current time)
	// it can be called for
	//   - FINISHED - endDate is not null
	//   - STARTED, CONFIRMATION_NEEDED, SCHEDULED - endDate is null
	@Override
	public boolean isSameExecutionFlowNotOperated(Event event, Event latestButNotScheduledEvent) {
		if (latestButNotScheduledEvent == null) {
			LOGGER.trace("There isn't a latest event yet.");
			return true;
		} else {
			int timeBetweenExecutions = event.getExecutionFlow().getTimeBetweenExecutions() == null ? 0 : event.getExecutionFlow().getTimeBetweenExecutions();
			// if the type is STARTED, CONFIRMATION_NEEDED then false has to be retrieved
			if (latestButNotScheduledEvent.getStatus().equals(EventStatusEnum.STARTED) || latestButNotScheduledEvent.getStatus().equals(EventStatusEnum.CONFIRMATION_NEEDED)) {
				LOGGER.trace("There is a latest event with the same exec flow whose status is " + latestButNotScheduledEvent.getStatus());
				return false;
			} 
			// the type is FINISHED or FAILED then calculate then 'the same executionFlow finished recently' + ExecutionFlowEntity.timeBetweenExecutions has to be greater than current time
			else if (latestButNotScheduledEvent.getStatus().equals(EventStatusEnum.FINISHED) ) {
				boolean b = latestButNotScheduledEvent.getEndDate().getTimeInMillis() + (timeBetweenExecutions * 1000) < Calendar.getInstance().getTimeInMillis();
				LOGGER.trace("The endDate of the same started executionFlow is {}, the time needed between executions is {} sec and the value of the isSameExecutionFlowNotStarted is {}.", 
						Constants.getDateFormat().format(latestButNotScheduledEvent.getEndDate().getTime()), timeBetweenExecutions, b);
				return b;
			} else {
				return true;
			}	
		}
	}

	
	private Status getStatus(final Status status, final Event event, final Event latestWaitingEvent, final Event latestButNotScheduledEvent) throws ReactionExecutionFlowException {
		LOGGER.debug("++++++++++++++++++ {} - {}", status.name(), status.getDescription());

		switch(status) {
			// the process has just started
			case THE_BEGINNING:
					// **************** case 1 ****************
					// if the executionFlow needs only one event to start
					if (isOneEventNeeded(event)) {
						LOGGER.trace("The executionFlow needs only one event to start");
						// the same executionFlow isn't started recently (the same executionFlow started/finished recently + ExecutionFlowEntity.timeBetweenExecutions < current time)
						if (isSameExecutionFlowNotOperated(event, latestButNotScheduledEvent)) {
							LOGGER.trace("The same executionFlow isn't started recently.");
							//start it
							return Status.START_SINGLE;
						}
						else {
							// ignore the error
							return ignoreEvent(event, latestButNotScheduledEvent);
						}
					}
					// the executionFlow needs more events to be started
					else {
						LOGGER.trace("The executionFlow needs more events to start");
						return getStatus(Status.START_MULTIPLE, event, latestWaitingEvent, latestButNotScheduledEvent);
					}

			// **************** case 2 ****************
			// the executionFlow needs more events to be started
			case START_MULTIPLE:
					// this exception is the n-times one in the given period
					if (isTheEventTheNTimes(event, latestWaitingEvent)) {
						// the same executionFlow isn't started recently (the same executionFlow started/finished recently + ExecutionFlowEntity.timeBetweenExecutions < current time)
						if (isSameExecutionFlowNotOperated(event, latestButNotScheduledEvent)) {
							LOGGER.trace("The same executionFlow isn't started recently.");
							//start it
							return Status.START_MULTIPLE;
						}
						else {
							return ignoreEvent(latestWaitingEvent, latestButNotScheduledEvent);
						}
					} else {
						return Status.END_OF_PROCESSING;
					}

			default: 
					throw new ReactionExecutionFlowException("Unexpected status when processing the event: " + status.name());
		}
		
	}


	private Status ignoreEvent(final Event event, final Event latestButNotScheduledEvent) {
		String reason = "The event is ignored as a same execution flow is (was recently) under operation already! The identifier of this other event is " + latestButNotScheduledEvent.getIdentifier() + 
				" (its status is " + latestButNotScheduledEvent.getStatus().toString() + ").";
		LOGGER.warn(reason);
		event.setStatus(EventStatusEnum.IGNORED);
		event.setReason(reason);
		Calendar now = Calendar.getInstance();
		if (event.getStartDate() == null) {
			event.setStartDate(now);
		}
		event.setEndDate(now);
		Event savedNewEvent = eventService.save(event);
		LOGGER.debug("The following Event is saved: \n{}", savedNewEvent);
		return Status.END_OF_PROCESSING;
	}

	
	private boolean isTheEventTheNTimes(Event event, Event latestWaitingEvent) {
		Calendar now = Calendar.getInstance();
		// there isn't latest event yet
		if (latestWaitingEvent == null) {
			LOGGER.debug("There isn't latest event yet -> creating a new Event + EventLife record");
			// creating a new Event + EventLife record
			event.setFirstEventArrived( now );
			event.setStartDate(now);
			saveEvent(event, 1);
			return false;
		} else {
			// this exception is the n-times one in the given period
			//    check if it is in the given period: now - firstEventArrived < multipleEventsTimeframe*1000
			LOGGER.trace("Is {} smaller than {} ?", Calendar.getInstance().getTimeInMillis() - latestWaitingEvent.getFirstEventArrived().getTimeInMillis(), (latestWaitingEvent.getErrorDetector().getMultipleEventsTimeframe() * 1000));
			if ( Calendar.getInstance().getTimeInMillis() - latestWaitingEvent.getFirstEventArrived().getTimeInMillis() < (latestWaitingEvent.getErrorDetector().getMultipleEventsTimeframe() * 1000) ) {
				LOGGER.debug("The event is in the given period.");
				//    check if it is the n-times one
				int counterCurrent = latestWaitingEvent.getMultipleEventsCounter() == null ? 0 : latestWaitingEvent.getMultipleEventsCounter();				
				int multipleEventsCnt = latestWaitingEvent.getErrorDetector().getMultipleEventsCnt();
				LOGGER.debug("The count of the eventLife records (without the current one) is {} and the count needed to start the executionFlow is {}.", counterCurrent, multipleEventsCnt);
				if (counterCurrent == multipleEventsCnt - 1) {
					LOGGER.debug("The current one is the n-th event so the executionFlow has to start.");
					// updating the counter of the event
					saveEvent(latestWaitingEvent, ++counterCurrent);
					return true;
				} else if (counterCurrent > multipleEventsCnt - 1) {
					LOGGER.debug("The latest stored Event has all the necessary EventLife records to start the executionFlow (likely it has started already) so new Event (with WAITING_FOR_OTHERS status) record has to be stored.");
					// creating a new Event with WAITING_FOR_OTHERS status
					event.setFirstEventArrived( now );
					saveEvent(event, 1);
					return false;
				} else {
					LOGGER.debug("We don't have the necessary number of the EventLife records to start the executionFlow, so increasing the multipleEventsCounter of the current waiting event.");
					// updating the counter of the event
					saveEvent(latestWaitingEvent, ++counterCurrent);
					return false;
				}
			} else {
				LOGGER.debug("The event has arrived too late so it cannot be used for the latest event. Creating a new Event record.");
				// the event has arrived too late so it cannot be used for the latest event
				// however it doesn't mean that we have to leave it to be lost => creating a new Event record with WAITING_FOR_OTHERS status and counter=1 
				event.setFirstEventArrived( now );
				event.setStartDate(now);
				saveEvent(event, 1);
				return false;
			}
		}
	}


	private Event saveEvent(Event event, Integer counter) {
		// saving the event
		event.setStatus(EventStatusEnum.WAITING_FOR_OTHERS);
		event.setMultipleEventsCounter(counter);
		Event savedNewEvent = eventService.save(event);
		LOGGER.debug("The following Event is saved: \n{}", savedNewEvent);
		return savedNewEvent;
	}

	
	private boolean isOneEventNeeded(Event event) {
		return event.getErrorDetector().getMultipleEventsCnt() == null;
	}
	

enum Status {
	
	THE_BEGINNING("Start the processing"),
	START_SINGLE("Start a single executionFlow"),
	START_MULTIPLE("Start a executionFlow that need more events to be started"),
	END_OF_PROCESSING("Finish the processing")
	;
	
	private String description = null;
	
	private Status(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
}

}