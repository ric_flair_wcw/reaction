package org.reaction.engine.service;

public interface PlaceholderVariableService {

    String replace(final String fieldValue, Long eventLifeId);

}
