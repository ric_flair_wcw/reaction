package org.reaction.engine.service;

import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.exception.ReactionExecutionFlowException;

public interface ExecutionFlowMachineService {

	public Event startIt(Event event) throws ReactionExecutionFlowException;
	
	public void restartEventLife(EventLife eventLife, String user) throws ReactionExecutionFlowException;

	public void skipEventLife(EventLife eventLife, String user, String value) throws ReactionExecutionFlowException;
	
}
