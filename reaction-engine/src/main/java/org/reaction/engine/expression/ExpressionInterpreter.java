package org.reaction.engine.expression;

import org.codehaus.commons.compiler.CompileException;
import org.codehaus.janino.ExpressionEvaluator;
import org.reaction.common.exception.ReactionExecutionFlowException;

import java.lang.reflect.InvocationTargetException;

public class ExpressionInterpreter {

	public static final String VARIABLE = "$COMMANDOUTPUT";
	
	public boolean evaluate(final String _value, final String expression) throws ReactionExecutionFlowException {
		String value = _value;
		if (value == null) {
			value = "";
		}
		
		ExpressionEvaluator expressionEvaluator = new ExpressionEvaluator();
		expressionEvaluator.setParameters(new String[] { VARIABLE }, new Class[] { String.class });
		expressionEvaluator.setExpressionType(boolean.class);
		try {
			expressionEvaluator.cook(expression);
			boolean result = (boolean)expressionEvaluator.evaluate(new Object[] { value } );
			return result;
		} catch (CompileException|InvocationTargetException e) {
			throw new ReactionExecutionFlowException("Error occured during the evaluation of the IF-ELSE expression! >> " + getMessage(e));
		}
	}


	private String getMessage(Throwable throwable) {
		if (throwable.getMessage() == null) {
			if (throwable.getCause() == null) {
				return "Cannot be determined...";
			} else {
				return getMessage(throwable.getCause());
			}
		} else {
			return throwable.getClass().getSimpleName() + ": " + throwable.getMessage();
		}
	}
	
}
