package org.reaction.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

// For standalone
@Configuration
@ImportResource({ "classpath:applicationContext.xml", "classpath:securityContext.xml" })
@ComponentScan(basePackages = {
		"org.reaction.engine.config",
		"org.reaction.engine.controller",
		"org.reaction.engine.persistence.service",
		"org.reaction.engine.persistence.converter",
		"org.reaction.engine.service",
		"org.reaction.engine.errors",
		"org.reaction.engine.scheduling.utils" })
@EnableAutoConfiguration
@SpringBootApplication
public class ReactionApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReactionApplication.class);
	private static AnnotationConfigEmbeddedWebApplicationContext APPLICATION_CONTEXT;

	public static void main(String[] args) {
		try {
			LOGGER.info("Adding a shutdown hook in order to stop Tomcat and the application properly.");
			Runtime.getRuntime().addShutdownHook(new Thread(() -> { ReactionApplication.stop(); }));
			start();
		} catch (Exception e) {
			LOGGER.error("Fatal error occured! Exiting...", e);
			System.exit(-1);
		}
	}

	public static void start() {
		LOGGER.info("Reaction engine standalone application is starting...");
		APPLICATION_CONTEXT = (AnnotationConfigEmbeddedWebApplicationContext) SpringApplication.run(ReactionApplication.class);
	}

	public static void stop() {
		try {
			LOGGER.info("Stopping embedded Tomcat server...");
			APPLICATION_CONTEXT.getEmbeddedServletContainer().stop();
		} catch (Exception e) {
			LOGGER.error("Error occured while stopping Tomcat!", e);
		}
		try {
			LOGGER.info("Closing application context...");
			APPLICATION_CONTEXT.close();
		} catch (Exception e) {
			LOGGER.error("Error occured while stopping the application context!", e);
		}
		
		LOGGER.info("The application is exiting ...");
		SpringApplication.exit(APPLICATION_CONTEXT, () -> 0);
		LOGGER.info("The shutdown process is finished.");
	}
}