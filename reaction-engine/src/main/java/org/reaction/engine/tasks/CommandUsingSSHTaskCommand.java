package org.reaction.engine.tasks;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import lombok.ToString;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.contants.SshAuthTypeEnum;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.CommandUsingSSHTask;
import org.reaction.common.domain.task.SuperTask;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.common.queue.LimitedList;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.CommandsToBeExecutedService;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StreamUtils;
import org.springframework.util.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@ToString(exclude = {"commandsToBeExecutedService", "eventLifeService", "eventService", "executorCommandUsingSSH", "errorHandler", "jsch", "encryptionKeyFile"})
public class CommandUsingSSHTaskCommand extends SuperNextCommand {


	private static final Logger LOGGER = LoggerFactory.getLogger(CommandUsingSSHTaskCommand.class);
	private static final String REG_EXP_HOST = ".+\\:[0-9]+$";
	private static final int DEFAULT_SSH_PORT = 22;

	private CommandUsingSSHTask commandUsingSSHTask;

	@Autowired
	private CommandsToBeExecutedService commandsToBeExecutedService;
	@Autowired
	private EventLifeService eventLifeService;
	@Autowired
	private EventService eventService;
	@Autowired
    private Executor executorCommandUsingSSH;
	@Autowired
	private ErrorHandler errorHandler;
	@Autowired
	private JSch jsch;
	@Autowired
	private PlaceholderVariableService placeholderVariableService;
	@Value("${reaction.security.password_encryption.key_file}")
	private String encryptionKeyFile;
    @Value("${reaction.commands.using-ssh.default-execution-timeout}")
    private Integer defaultExecutionTimeout;

	public CommandUsingSSHTaskCommand(CommandUsingSSHTask commandUsingSSHTask) {
		this.commandUsingSSHTask = commandUsingSSHTask;
	}
	
	
	@Override
	protected void _execute(EventLife eventLife) {
		LOGGER.debug("Executing the command using SSH in a separate thread...");
		try {
			executorCommandUsingSSH.execute(() -> this.internalExecute(eventLife));
		} catch(Exception e) {
			errorHandler.handleErrorInCommand(eventLife, e);
		}
	}


	void internalExecute(EventLife eventLife) {
		LOGGER.info ("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+
		             "\n  COMMAND EXECUTION - Executing an COMMAND_USING_SSH command: {}"+
		             "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", eventLife.getTask().getSuperTask().getName());
		LOGGER.debug("{}", eventLife);
		try {
			checkCommandProperties();

			updateStatusExecutionStarted(eventLife);
			// 1st element is the extracted value, the 2nd is the full output, the 3rd is if it was successful
			Object[] result = executeCommandUsingSSH();
			if ((boolean) result[3]) {
				updateStatusFinished(eventLife, (String) result[0], (String) result[1], (boolean) result[3]);
			} else {
				saveOutput(eventLife, (String) result[1]);
				throw new ReactionExecutionFlowException("The SSH command failed! " + result[2]);
			}

			next(null);
		} catch(Exception e) {
			errorHandler.handleErrorInCommand(eventLife, e);
		}
	}


	private Object[] executeCommandUsingSSH() throws Exception {
		String commandUsingSSHTaskHost = placeholderVariableService.replace(commandUsingSSHTask.getHost(), eventLife.getId());
		String host = placeholderVariableService.replace(getHost(commandUsingSSHTaskHost), eventLife.getId());
		int port = getPort(commandUsingSSHTaskHost);
		String username = placeholderVariableService.replace(commandUsingSSHTask.getOsUser(), eventLife.getId());
		String password = decrypt(getPassword());
		String command = placeholderVariableService.replace(commandUsingSSHTask.getCommand(), eventLife.getId());
		String outputPattern = placeholderVariableService.replace(commandUsingSSHTask.getOutputPattern(), eventLife.getId());
		try {
			return viaSSH(host, port, username, password, command, outputPattern);
		} catch (JSchException | IOException e) {
			throw new ReactionExecutionFlowException("Exception occurred when trying to execute a command using SSH! - " + e.getLocalizedMessage(), e);
		}
	}

	private String getPassword() {
		if (commandUsingSSHTask.getIsPwdSetManually()) {
			return commandUsingSSHTask.getOsPassword();
		} else {
			return placeholderVariableService.replace("##{" + commandUsingSSHTask.getOsPassword() + "}##", eventLife.getId());
		}
	}

	// from https://github.com/ijl20/python_java_crypto/blob/master/java/AESCrypt.java
	private String decrypt(String osPassword) throws ReactionExecutionFlowException {
		try {
			byte[] cipherbytes = Base64.getDecoder().decode(osPassword);
			byte[] initVector = Arrays.copyOfRange(cipherbytes, 0, 16);
			byte[] messagebytes = Arrays.copyOfRange(cipherbytes, 16, cipherbytes.length);

			IvParameterSpec iv = new IvParameterSpec(initVector);
			SecretKeySpec skeySpec = new SecretKeySpec(getKeyForPasswordEncryption().getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] byte_array = cipher.doFinal(messagebytes);

			return new String(byte_array, StandardCharsets.UTF_8);
		} catch(Exception e) {
			throw new ReactionExecutionFlowException("Exception occurred when trying to decrypt the password of the OS user to execute a command using SSH!", e);
		}
	}

	private int getPort(String host) {
		if (host.matches(REG_EXP_HOST)) {
			return Integer.parseInt(host.substring(host.lastIndexOf(":")+1));
		} else {
			return DEFAULT_SSH_PORT;
		}

	}

	private String getHost(String host) {
		if (host.matches(REG_EXP_HOST)) {
			return host.substring(0, host.lastIndexOf(":"));
		} else {
			return host;
		}
	}


	private Object[] viaSSH(String host, int port, String username, String password, String command, String outputPattern) throws Exception {
		Session session = null;
		ChannelExec channel = null;

		try {
			SshAuthTypeEnum sshAuthType = commandUsingSSHTask.getSshAuthType() == null ? SshAuthTypeEnum.USER_PASSWORD : commandUsingSSHTask.getSshAuthType();
			if (sshAuthType.equals(SshAuthTypeEnum.SSH_KEY)) {
				jsch.addIdentity(commandUsingSSHTask.getSshKeyPath());
			}
			session = jsch.getSession(username, host, port);

			if (sshAuthType.equals(SshAuthTypeEnum.USER_PASSWORD)) {
				session.setPassword(password.getBytes(StandardCharsets.UTF_8));
			}
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();

			channel = (ChannelExec) session.openChannel("exec");
			// adding ;echo $? at the end of the command to retrieve the exit status
			channel.setCommand((command + "; echo $?").getBytes(StandardCharsets.UTF_8));
			channel.setInputStream(null);
			channel.connect();

			return extractOutputValue(channel, channel.getInputStream(), channel.getErrStream(), outputPattern);
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}
		}
	}

	private Object[] extractOutputValue(ChannelExec channel, InputStream inputStream, InputStream errorStream, String outputPattern) throws Exception {
		Object[] result = new Object[4];
		Pattern pattern = Pattern.compile(outputPattern);
		String extractedOutput = null;
		LimitedList<String> limitedQueueInput = new LimitedList<>(15);
		String exitStatus = null;

        byte[] buffer = new byte[1024];
        Optional<String> streamValueOptional = readStream(channel, inputStream, buffer);
        if (streamValueOptional.isPresent()) {
            for (String line : streamValueOptional.get().split("[\\r\\n]+")) {
                if (!StringUtils.isEmpty(line)) {
                    limitedQueueInput.add(line);
                    if (StringUtils.isEmpty(outputPattern)) {
                        extractedOutput = line;
                    } else {
                        Matcher matcher = pattern.matcher(line);
                        if (matcher.find()) {
                            extractedOutput = matcher.group("COMMANDOUTPUT");
                        }
                    }
                    exitStatus = line;
                }
            }
        } else {
            limitedQueueInput.add(String.format("WARNING! The command execution was interrupted as it took too long (more than %s seconds)!", getExecutionTimeout().toString()));
        }
		LOGGER.debug("limitedQueueInput: {}", limitedQueueInput.stream().collect(Collectors.joining("\n")));

		LimitedList<String> limitedQueueError = new LimitedList<>(15);
		// if the normal stream was interrupted then don't read the error stream
        if (streamValueOptional.isPresent()) {
            streamValueOptional = readStream(channel, errorStream, buffer);
            if (streamValueOptional.isPresent()) {
                for (String line : streamValueOptional.get().split("[\\r\\n]+")) {
                    if (!StringUtils.isEmpty(line)) {
                        limitedQueueError.add(line);
                    }
                }
            } else {
                limitedQueueInput.add(String.format("WARNING! The command execution was interrupted as it took too long (more than %s seconds)!", getExecutionTimeout().toString()));
            }
        }
		LOGGER.debug("limitedQueueError: {}", limitedQueueError.stream().collect(Collectors.joining("\n")));
		LOGGER.debug("exit-status: {}", channel.getExitStatus());

		result[0] = extractedOutput;
		result[1] = limitedQueueInput.stream().collect(Collectors.joining("\n"));
		result[2] = limitedQueueError.stream().collect(Collectors.joining("\n"));
		result[3] = exitStatus != null && exitStatus.equals("0");
		return result;
	}

    private Optional<String> readStream(ChannelExec channel, InputStream inputStream, byte[] buffer) throws Exception {
	    StringBuilder output = new StringBuilder();
	    Instant then = Instant.now();
		Integer executionTimeout = getExecutionTimeout();

		Duration duration = Duration.ofSeconds(executionTimeout);
        while (true) {
            while(inputStream.available() > 0){
                int i= inputStream.read(buffer, 0, 1024);
                if (i < 0) break;
                output.append(new String(buffer, 0, i));
            }
            if (channel.isClosed()){
                if (inputStream.available() > 0) continue;
                break;
            }
            if (executionTimeout != -1 && Duration.between(then, Instant.now()).toMillis() > duration.toMillis()) {
                LOGGER.warn("The command execution is interrupted as it took too long! execution duration ({} ms) > execution timeout ({} ms)", Duration.between(then, Instant.now()).toMillis(), duration.toMillis());
				channel.sendSignal("INT");
				return Optional.empty();
			}
            try{Thread.sleep(500);}catch(Exception ee){}
        }
        return Optional.of(output.toString());
    }

	private Integer getExecutionTimeout() {
		return Optional.ofNullable(commandUsingSSHTask.getExecutionTimeout() != null ? commandUsingSSHTask.getExecutionTimeout() :
				                   defaultExecutionTimeout == null || defaultExecutionTimeout < 0 ? null : defaultExecutionTimeout)
				       .orElseThrow(() -> new ReactionRuntimeException("Neither the value of the config 'reaction.commands.using-ssh.default-execution-timeout' is not specified or less/equal to zero" +
												   "nor the execution timeout in the task!"));
	}


	private void updateStatusFinished(EventLife eventLife, String extractedValue, String fullOutput, boolean successful) {
		eventLife.setEventDate(Calendar.getInstance());
		eventLife.setEventStatus(EventStatusEnum.FINISHED);
		eventLife.setOutput(fullOutput);
		eventLife.setExtractedValue(extractedValue);
		eventLife.setExtCommandSuccesful(successful);
		eventLife.addToHistory();
		EventLife savedEventLife = eventLifeService.save(eventLife);
		LOGGER.debug("The EvenLife is updated: {}", savedEventLife);
	}


	private void saveOutput(EventLife eventLife, String fullOutput) {
		eventLife.setOutput(fullOutput);
		eventLifeService.save(eventLife);
	}


	private void updateStatusExecutionStarted(EventLife eventLife) {
		eventLife.setEventDate(Calendar.getInstance());
		eventLife.setEventStatus(EventStatusEnum.EXECUTION_STARTED);
		eventLife.addToHistory();
		EventLife savedEventLife = eventLifeService.save(eventLife);
		LOGGER.debug("The EvenLife is updated: {}", savedEventLife);
	}


	private void checkCommandProperties() {
		if (StringUtils.isEmpty(commandUsingSSHTask.getCommand())) {
			throw new ReactionRuntimeException("Command of the COMMAND_USING_SSH task is not defined!");
		}
		if (StringUtils.isEmpty(commandUsingSSHTask.getHost())) {
			throw new ReactionRuntimeException("Host of the COMMAND_USING_SSH task is not defined!");
		}
		if (StringUtils.isEmpty(commandUsingSSHTask.getOsUser())) {
			throw new ReactionRuntimeException("OS user of the COMMAND_USING_SSH task is not defined!");
		}
		if (StringUtils.isEmpty(commandUsingSSHTask.getOsPassword())) {
			throw new ReactionRuntimeException("OS password of the COMMAND_USING_SSH task is not defined!");
		}
	}

	public String getKeyForPasswordEncryption() throws IOException {
		return StreamUtils.copyToString(new FileInputStream(ResourceUtils.getFile(encryptionKeyFile)), Charset.defaultCharset());
	}

	@Override
	public SuperTask getSuperTask() {
		return commandUsingSSHTask;
	}

}