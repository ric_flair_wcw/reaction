package org.reaction.engine.tasks;

import lombok.ToString;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.InternalIfElseTask;
import org.reaction.common.domain.task.SuperTask;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.expression.ExpressionInterpreter;
import org.reaction.engine.persistence.service.EventLifeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;


@ToString(exclude = {"eventLifeService"})
public class IfElseTaskCommand extends SuperNextCommand {


	private static final Logger LOGGER = LoggerFactory.getLogger(IfElseTaskCommand.class);

	private InternalIfElseTask internalIfElseTask;
	
	@Autowired
	private EventLifeService eventLifeService;
	@Autowired
	private ExpressionInterpreter expressionInterpreter;

	public IfElseTaskCommand(InternalIfElseTask internalIfElseTask) {
		this.internalIfElseTask = internalIfElseTask;
	}
	
	
	@Override
	protected void _execute(EventLife eventLife) throws ReactionExecutionFlowException {
		LOGGER.info ("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+
		             "\n  COMMAND EXECUTION - executing an if-else command: {}"+
		             "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", eventLife.getTask().getSuperTask().getName());
		LOGGER.debug("{}", eventLife);
		checkCommandProperties();
		String ifExpression = placeholderVariableService.replace(internalIfElseTask.getIfExpression(), eventLife.getId());
		// if the task is an if-else and its condition contains $COMMANDOUTPUT then the previous task has to provide a value that can be used to evaluate the condition in if-else
		//      => at this moment the value has to be ready to evaluate the condition in 'if'
		// the 'value' has to be retrieved from the previous EventLife
		EventLife prevEventLife = eventLifeService.getEventLifeByOrderAndEvent(eventLife.getOrder(), eventLife.getEvent());
		if (ifExpression.contains(ExpressionInterpreter.VARIABLE) &&
				(prevEventLife == null || (!(prevEventLife.getTask() instanceof CommandByWorkerTaskCommand) &&
						                   !(prevEventLife.getTask() instanceof CommandUsingSSHTaskCommand)))) {
			throw new ReactionExecutionFlowException("The eventLife ("+eventLife.getId()+") contains an if-else task but an COMMAND_BY_WORKER or COMMAND_USING_SSH command doesn't precede it!");
		}
		String value = prevEventLife.getExtractedValue();
		// evaluating the IF-ELSE condition expression
		boolean inIf = expressionInterpreter.evaluate(value, ifExpression);
		// updating the EventLife with FINISHED
		eventLife.setEventDate(Calendar.getInstance());
		eventLife.setEventStatus(EventStatusEnum.FINISHED);
		eventLife.setOutput(Boolean.toString(inIf));
		eventLife.addToHistory();
		eventLifeService.save(eventLife);
				
		if (inIf) {
			LOGGER.info("It is TRUE - the {} is [{}], the expression is [{}]", ExpressionInterpreter.VARIABLE, value, ifExpression);
			if (internalIfElseTask.getFirstCommandInIf() != null) {
				next(internalIfElseTask.getFirstCommandInIf().getSuperTask().getId());
			} else {
				next(null);
			}
		} 
		else {
			LOGGER.info("It is FALSE - the {} is [{}], the expression is [{}]", ExpressionInterpreter.VARIABLE, value, ifExpression);
			if (internalIfElseTask.getFirstCommandInElse() != null) {
				next(internalIfElseTask.getFirstCommandInElse().getSuperTask().getId());
			} else {
				next(null);
			}
		}
	}
	

	private void checkCommandProperties() {
        if (internalIfElseTask.getIfExpression() == null) {
			throw new ReactionRuntimeException("The expression of the IF-ELSE condition is not defined!");
        }
	}

	
	public InternalIfElseTask getInternalIfElseTask() {
		return internalIfElseTask;
	}


	@Override
	public SuperTask getSuperTask() {
		return internalIfElseTask;
	}


}
