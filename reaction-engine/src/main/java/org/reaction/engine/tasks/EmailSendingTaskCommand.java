package org.reaction.engine.tasks;

import lombok.ToString;
import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.EmailSendingTask;
import org.reaction.common.domain.task.SuperTask;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.service.EventLifeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Calendar;

@ToString(exclude = {"javaMailSender", "eventLifeService"})
public class EmailSendingTaskCommand extends SuperNextCommand {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailSendingTaskCommand.class);

	private EmailSendingTask emailSendingTask;
    @Autowired
    public JavaMailSender javaMailSender;	
    @Autowired
    public EventLifeService eventLifeService;

    
	public EmailSendingTaskCommand(EmailSendingTask emailSendingTask) {
		this.emailSendingTask = emailSendingTask;
	}

	
	@Override
	protected void _execute(EventLife eventLife) throws ReactionExecutionFlowException {
		LOGGER.info ("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+
		             "\n  COMMAND EXECUTION - executing an email sending command: {}"+
		             "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", eventLife.getTask().getSuperTask().getName());
		LOGGER.debug("{}", eventLife);
		checkCommandProperties();
		// building the message to be sent
		MimeMessage message = javaMailSender.createMimeMessage();
		String content = placeholderVariableService.replace(emailSendingTask.getContent(), eventLife.getId());
		String[] to = emailSendingTask.getRecipients().stream().map( t -> placeholderVariableService.replace(t, eventLife.getId())).toArray(String[]::new);
		String subject = placeholderVariableService.replace(emailSendingTask.getSubject(), eventLife.getId());
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, false, "utf-8");
			message.setContent(content, "text/html");
			helper.setTo(to);
			helper.setSubject(subject);
			helper.setFrom(Constants.MAIL_FROM);
		} catch(MessagingException e) {
			throw new ReactionExecutionFlowException("Exception when tried to create the mail to be sent!", e);
		}
        
        // send the message
        LOGGER.debug("Sending the message to '{}' from '{}' with the subject '{}' and the content is\n{}", to, Constants.MAIL_FROM, subject, content);
        javaMailSender.send(message);
        
		// updating the EventLife with FINISHED
		eventLife.setEventDate(Calendar.getInstance());
		eventLife.setEventStatus(EventStatusEnum.FINISHED);
		eventLife.addToHistory();
		eventLifeService.save(eventLife);
        
        // call the next command in the flow
		next(null);
	}

	
	private void checkCommandProperties() {
        if (emailSendingTask.getRecipients() == null || emailSendingTask.getRecipients().size() == 0) {
			throw new ReactionRuntimeException("Recipients of the mail are not defined!");
        }
        if (emailSendingTask.getSubject() == null || emailSendingTask.getSubject().length() == 0) {
			throw new ReactionRuntimeException("Subject of the mail is not defined!");
        }
        if (emailSendingTask.getContent() == null || emailSendingTask.getContent().length() == 0) {
			throw new ReactionRuntimeException("Content of the mail is not defined!");
        }
	}


	@Override
	public SuperTask getSuperTask() {
		return emailSendingTask;
	}

}
