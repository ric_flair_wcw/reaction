package org.reaction.engine.tasks;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.SuperCommand;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.persistence.service.TaskService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;

public abstract class SuperNextCommand extends SuperCommand {


	private static final Logger LOGGER = LoggerFactory.getLogger(SuperNextCommand.class);
	
	@Autowired
	private TaskService taskService;
	@Autowired
	private EventLifeService eventLifeService;
	@Autowired
	private EventService eventService;
	@Autowired
	private ErrorHandler errorHandler;
	@Autowired
	protected PlaceholderVariableService placeholderVariableService;

	
	// if the taskId is not null it means that the next is called from if-else or loop task and the next task is determined there
	// the taskId is null at any other case
	public void next(final Long taskId) throws ReactionExecutionFlowException {
		SuperCommand command = null;
		// check if the event wasn't cancelled in the meantime
		Event event = eventService.getEvent(eventLife.getEvent().getId());
		if (event.getStatus().equals(EventStatusEnum.CANCELLED)) {
			LOGGER.info("The event was cancelled so the next task won't be executed! event identifier = {}", event.getIdentifier());
			return;
		}
		// searching for the next task to be executed
		if (taskId == null) {
			command = taskService.getNextTaskRecursively( getSuperTask() );
		} else {
			command = taskService.getCommandById(taskId);
		}
		
		if (command != null) {
			// storing an EventLife record for the next task
			EventLife newEventLife = EventLife.builder().eventDate(Calendar.getInstance())
														.event(eventLife.getEvent())
														.task(command)
														.eventStatus(command instanceof LoopTaskCommand ? EventStatusEnum.LOOP_COMPLETED : EventStatusEnum.STARTED)
														.order(eventLife.getOrder() + 1).build();
			newEventLife.addToHistory();
			EventLife savedNewEventLife = eventLifeService.save(newEventLife);
			// executing the execute() method of the next task
			command.execute( savedNewEventLife );
		} else {
			// setting the endDate and successful properties of EventEntity
			event.setEndDate(Calendar.getInstance());
			event.setStatus(EventStatusEnum.FINISHED);
			eventService.save(event);
			// no more task => end of executionFlow
			LOGGER.info("No more tasks in the execution flow, the flow finished successfully! event id = {}", event.getId());
		}
	}
	
	
	@Override
	public void execute(EventLife eventLife) throws ReactionExecutionFlowException {
		try {
			super.execute(eventLife);
		} catch(Exception e) {
			errorHandler.handleErrorInCommand(eventLife, e);
		}
	}
}
