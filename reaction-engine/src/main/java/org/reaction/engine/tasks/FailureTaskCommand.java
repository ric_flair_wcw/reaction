package org.reaction.engine.tasks;

import lombok.ToString;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.FailureTask;
import org.reaction.common.domain.task.SuperTask;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;

@ToString(exclude = {"eventLifeService"})
public class FailureTaskCommand extends SuperNextCommand {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(FailureTaskCommand.class);

	private FailureTask failureTask;
    @Autowired
    public EventLifeService eventLifeService;	
	@Autowired
	private EventService eventService;

    
	public FailureTaskCommand(FailureTask failureTask) {
		this.failureTask = failureTask;
	}

	
	@Override
	protected void _execute(EventLife eventLife) throws ReactionExecutionFlowException {
		LOGGER.info ("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+
		             "\n  COMMAND EXECUTION - failing the flow: {}"+
		             "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", eventLife.getTask().getSuperTask().getName());
		LOGGER.debug("{}", eventLife);
		// updating the EventLife with FINISHED
		eventLife.setEventDate(Calendar.getInstance());
		eventLife.setEventStatus(EventStatusEnum.FINISHED);
		eventLife.addToHistory();
		eventLifeService.save(eventLife);
		
		// set FAILED status to the event
		Event event = eventLife.getEvent();
		event.setStatus(EventStatusEnum.FAILED);
		event.setEndDate(Calendar.getInstance());
		Event savedEvent = eventService.save(event);
	}

	
	@Override
	public SuperTask getSuperTask() {
		return failureTask;
	}

}
