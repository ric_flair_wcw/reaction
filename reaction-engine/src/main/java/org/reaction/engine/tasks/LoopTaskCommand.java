package org.reaction.engine.tasks;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.LoopTask;
import org.reaction.common.domain.task.SuperTask;
import org.reaction.common.exception.ReactionExecutionFlowException;
import org.reaction.engine.persistence.repository.EventLifeEntityRepository;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Calendar;

public class LoopTaskCommand extends SuperNextCommand {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoopTaskCommand.class);

    private LoopTask loopTask;
    @Autowired
    private EventLifeService eventLifeService;
    @Autowired
    private EventLifeEntityRepository eventLifeEntityRepository;
    @Autowired
    private TaskService taskService;

    public LoopTaskCommand(LoopTask loopTask) {
        this.loopTask = loopTask;
    }

    @Override
    protected void _execute(EventLife eventLife) throws ReactionExecutionFlowException {
        Integer loopIndex = eventLifeService.getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(eventLife.getEvent().getId(), eventLife.getTask().getSuperTask().getId());
        loopIndex = loopIndex == null ? 0 : loopIndex+1;
        if (isNewCycleNeeded(loopIndex)) {
            LOGGER.info ("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+
                         "\n  COMMAND EXECUTION - loop: {}, index: {}"+
                         "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",
                    eventLife.getTask().getSuperTask().getName(), loopIndex);
            LOGGER.debug("{}", eventLife);
            updateStatusNewCycleStarted(eventLife, loopIndex);

            next(loopTask.getFirstCommandInLoop().getSuperTask().getId());
        } else {
            LOGGER.info("No more cycle for the loop '{}'.", eventLife.getTask().getSuperTask().getName());
            next(null);
        }
    }


    private boolean isNewCycleNeeded(Integer loopIndex) {
        int nrOfCycles = loopTask.getLoopValueList1().split(loopTask.getLoopSeparator1()).length;
        return loopIndex < nrOfCycles;
    }


    private void updateStatusNewCycleStarted(EventLife eventLife, Integer loopIndex) {
        eventLife.setEventDate(Calendar.getInstance());
        eventLife.setEventStatus(EventStatusEnum.CYCLE_STARTED);
        eventLife.setLoopIndex(loopIndex);
        eventLife.addToHistory();
        EventLife savedEventLife = eventLifeService.save(eventLife);
        LOGGER.debug("The EvenLife is updated: {}", savedEventLife);
    }


    @Override
    public SuperTask getSuperTask() {
        return loopTask;
    }

}
