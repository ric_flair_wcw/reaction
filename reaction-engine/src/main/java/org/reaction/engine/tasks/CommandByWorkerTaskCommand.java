package org.reaction.engine.tasks;

import lombok.ToString;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.CommandsToBeExecuted;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.task.CommandByWorkerTask;
import org.reaction.common.domain.task.SuperTask;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.service.CommandsToBeExecutedService;
import org.reaction.engine.persistence.service.EventLifeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import java.util.Calendar;


@ToString(exclude = {"commandsToBeExecutedService", "eventLifeService"})
public class CommandByWorkerTaskCommand extends SuperNextCommand {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(CommandByWorkerTaskCommand.class);
	
	private CommandByWorkerTask commandByWorkerTask;
	
	@Autowired
	private CommandsToBeExecutedService commandsToBeExecutedService;
	@Autowired
	private EventLifeService eventLifeService;

	
	public CommandByWorkerTaskCommand(CommandByWorkerTask commandByWorkerTask) {
		this.commandByWorkerTask = commandByWorkerTask;
	}
	
	
	@Override
	// the COMMAND_BY_WORKER commands are executed asynchronously => only save the CommandsToBeExecuted to DB here and the worker will poll the WorkerController.checkCommand(...)
	protected void _execute(EventLife eventLife) {
		LOGGER.info ("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+
		             "\n  COMMAND EXECUTION - executing an COMMAND_BY_WORKER command: {}"+
		             "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", eventLife.getTask().getSuperTask().getName());
		LOGGER.debug("{}", eventLife);
		checkCommandProperties();
		// storing the COMMAND_BY_WORKER task in the COMMANDS_TO_BE_EXECUTED table -> the record will be read in WorkerController.checkCommand(...)
		CommandsToBeExecuted d = CommandsToBeExecuted.builder()
													 .executed(false)
													 .eventLife(eventLife).build();
		commandsToBeExecutedService.save(d);
		
		// updating the EventLife to WAITING_FOR_EXECUTION
		eventLife.setEventDate(Calendar.getInstance());
		eventLife.setEventStatus(EventStatusEnum.WAITING_FOR_EXECUTION);
		eventLife.addToHistory();
		EventLife savedEventLife = eventLifeService.save(eventLife);
		LOGGER.debug("The EvenLife is updated: {}", savedEventLife);
	}


	private void checkCommandProperties() {
        if (StringUtils.isEmpty(commandByWorkerTask.getCommand())) {
			throw new ReactionRuntimeException("Command of the COMMAND_BY_WORKER task is not defined!");
        }
        if (StringUtils.isEmpty(commandByWorkerTask.getHost())) {
			throw new ReactionRuntimeException("Host of the COMMAND_BY_WORKER task is not defined!");
        }
	}

	
	@Override
	public SuperTask getSuperTask() {
		return commandByWorkerTask;
	}

		
}