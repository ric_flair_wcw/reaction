package org.reaction.engine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.WebApplicationInitializer;


@Configuration
@ImportResource({"classpath:applicationContext.xml", "classpath:securityContext.xml"})
@ComponentScan(basePackages = {
		"org.reaction.engine.config",
		"org.reaction.engine.controller",
		"org.reaction.engine.persistence.service",
		"org.reaction.engine.persistence.converter",
		"org.reaction.engine.service",
		"org.reaction.engine.scheduling.utils",
		"org.reaction.engine.errors" })
@EnableAutoConfiguration
public class WebInitializer extends SpringBootServletInitializer implements WebApplicationInitializer {

	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebInitializer.class);
	}

	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(WebInitializer.class, args);
	}
	
	
}