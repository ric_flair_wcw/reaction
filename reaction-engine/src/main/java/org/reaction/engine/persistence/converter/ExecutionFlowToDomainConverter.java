package org.reaction.engine.persistence.converter;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.reaction.common.domain.ExecutionFlow;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;


public class ExecutionFlowToDomainConverter<A, B> implements SuperConverter<ExecutionFlowEntity, ExecutionFlow> {


	@Override
	public ExecutionFlow apply(ExecutionFlowEntity e) {
		if (e == null) {
			return null;
		}
		List<String> errorMailSendingRecipients = null;
		if (e.getErrorMailSendingRecipients() != null && !e.getErrorMailSendingRecipients().trim().isEmpty()) {
			errorMailSendingRecipients = Arrays.asList(e.getErrorMailSendingRecipients().split(",")).stream()
																					    .map(r -> r.trim())
																					    .collect(Collectors.toList());
		}
		List<String> startMailSendingRecipients = null;
		if (e.getStartMailSendingRecipients() != null && !e.getStartMailSendingRecipients().trim().isEmpty()) {
			startMailSendingRecipients = Arrays.asList(e.getStartMailSendingRecipients().split(",")).stream()
																					    .map(r -> r.trim())
																					    .collect(Collectors.toList());
		}
		List<String> accessGroups = null;
		if (e.getAccessGroup() != null && !e.getAccessGroup().trim().isEmpty()) {
			accessGroups = Arrays.asList(e.getAccessGroup().split(",")).stream()
                                                                       .map(r -> r.trim())
                                                                       .collect(Collectors.toList());
		}
		// setting the basic properties
		ExecutionFlow executionFlow = ExecutionFlow.builder().id(e.getId())
                                                             .timeBetweenExecutions(e.getTimeBetweenExecutions())
                                                             .executionTime(e.getExecutionTime())
                                                             .status(e.getStatus())
                                                             .accessGroups(accessGroups)
                                                             .errorMailSendingRecipients(errorMailSendingRecipients)
                                                             .startMailSendingRecipients(startMailSendingRecipients)
                                                             .name(e.getName()).build();
		return executionFlow;
	}
	
}
