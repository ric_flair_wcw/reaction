package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.EventLife;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.reaction.engine.persistence.model.TaskEntity;

public class EventLifeToEntityConverter<A, B> implements SuperConverter<EventLife, EventLifeEntity> {


    @Override
    public EventLifeEntity apply(EventLife d) {
        if (d == null) {
            return null;
        }
        TaskEntity taskEntity = null;
        if (d.getTask() != null) {
            taskEntity = TaskEntity.builder().id(d.getTask().getSuperTask().getId()).build();
        }
        EventEntity eventEntity = EventEntity.builder().id(d.getEvent().getId()).build();
        EventLifeEntity e = EventLifeEntity.builder()
                                     .eventDate(d.getEventDate())
                                     .eventEntity(eventEntity)
                                     .eventStatus(d.getEventStatus())
                                     .id(d.getId())
                                     .order(d.getOrder())
                                     .output(d.getOutput())
                                     .extractedValue(d.getExtractedValue())
                                     .errorMessage(d.getErrorMessage())
                                     .extCommandSuccesful(d.getExtCommandSuccesful())
                                     .history(d.getHistory())
                                     .loopIndex(d.getLoopIndex())
                                     .byWhom(d.getByWhom())
                                     .taskEntity(taskEntity).build();
        return e;
    }


}
