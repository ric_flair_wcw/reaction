package org.reaction.engine.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ERROR_DETECTOR")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ErrorDetectorEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_ErrorDetectorEntity")
	@SequenceGenerator(name="id_generator_ErrorDetectorEntity", sequenceName = "ERROR_DETECTOR_SQ")
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "MESSAGE_PATTERN", length = 400)
	private String messagePattern;
	
	@Column(name = "ACTIVATED")
	private boolean activated;
	
	@Column(name = "CONFIRMATION_NEEDED")
	private boolean confirmationNeeded;
	
	// multiple exceptions (events) are needed to start the executionFlow -> how many?
	@Column(name = "MULTIPLE_EVENTS_CNT")
	private Integer multipleEventsCnt;
	
	// multiple exceptions (events) are needed to start the executionFlow -> in what time frame? e.g. 3 exceptions in 20 mins
	// in seconds
	@Column(name = "MULTIPLE_EVENTS_TIMEFR")
	private Integer multipleEventsTimeframe;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EXECUTION_FLOW_ID")
	private ExecutionFlowEntity executionFlowEntity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EVENT_SOURCE_ID")
	private EventSourceEntity eventSourceEntity;

	@Column(name = "IDENTIFIERS", length = 3000)
	private String identifiers;

}