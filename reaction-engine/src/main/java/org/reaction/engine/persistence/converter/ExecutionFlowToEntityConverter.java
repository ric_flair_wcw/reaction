package org.reaction.engine.persistence.converter;


import org.reaction.common.domain.ExecutionFlow;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;


public class ExecutionFlowToEntityConverter<A, B> implements SuperConverter<ExecutionFlow, ExecutionFlowEntity> {


	@Override
	public ExecutionFlowEntity apply(ExecutionFlow d) {
		if (d == null) {
			return null;
		}
		// setting the basic properties
		ExecutionFlowEntity executionFlowEntity = ExecutionFlowEntity.builder()
                                                      .id(d.getId())
                                                      .timeBetweenExecutions(d.getTimeBetweenExecutions())
                                                      .executionTime(d.getExecutionTime())
                                                      .status(d.getStatus())
                                                      .name(d.getName()).build();
		return executionFlowEntity;
	}
	
}
