package org.reaction.engine.persistence.repository;

import org.reaction.engine.persistence.model.WorkerStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkerStatusEntityRepository extends JpaRepository<WorkerStatusEntity, Long> {


	WorkerStatusEntity findByHost(String host);
	
}
