package org.reaction.engine.persistence.repository;


import org.reaction.engine.persistence.model.ScheduledExecutionFlowEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ScheduledExecutionFlowEntityRepository extends JpaRepository<ScheduledExecutionFlowEntity, Long> {
	
}
