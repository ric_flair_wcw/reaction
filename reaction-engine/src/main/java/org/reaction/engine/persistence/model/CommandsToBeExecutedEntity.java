package org.reaction.engine.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "COMMANDS_TO_BE_EXECUTED")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CommandsToBeExecutedEntity {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_CommandsToBeExecutedEntity")
//	@GenericGenerator(name = "id_generator_CommandsToBeExecutedEntity", strategy = "native", parameters = {
//			@Parameter(name="sequence_name", value="COMMANDS_TO_BE_EXECUTED_SQ") 
//	})
	@SequenceGenerator(name="id_generator_CommandsToBeExecutedEntity", sequenceName = "COMMANDS_TO_BE_EXECUTED_SQ")
	private Long id;

	@Column(name = "IS_EXECUTED")
	private boolean executed;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EVENT_LIFE_ID")
	private EventLifeEntity eventLifeEntity;

}
