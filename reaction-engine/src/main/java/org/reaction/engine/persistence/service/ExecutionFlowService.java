package org.reaction.engine.persistence.service;

import org.reaction.common.contants.ExecutionFlowStatusEnum;
import org.reaction.common.domain.ExecutionFlow;

public interface ExecutionFlowService {

	ExecutionFlow getExecutionFlow(Long executionFlowId);

	void changeStatus(ExecutionFlow executionFlow, ExecutionFlowStatusEnum status);

}