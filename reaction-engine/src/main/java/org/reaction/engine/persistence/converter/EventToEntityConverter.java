package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.Event;
import org.reaction.engine.persistence.model.ErrorDetectorEntity;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.ScheduledExecutionFlowEntity;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.reaction.engine.persistence.service.EventSourceService;
import org.springframework.beans.factory.annotation.Autowired;

public class EventToEntityConverter<A, B> implements SuperConverter<Event, EventEntity> {

	
	@Autowired
	private EventSourceService eventSourceService;
	
	
	@Override
	public EventEntity apply(Event d) {
		if (d == null) {
			return null;
		}
		
		ErrorDetectorEntity errorDetectorEntity = null;
		if (d.getErrorDetector() != null) {
			errorDetectorEntity = ErrorDetectorEntity.builder().id(d.getErrorDetector().getId()).build();
		}
		ExecutionFlowEntity executionFlowEntity = null;
		if (d.getExecutionFlow() != null) {													   
			executionFlowEntity = ExecutionFlowEntity.builder().id(d.getExecutionFlow().getId()).build();
		}
		ScheduledExecutionFlowEntity scheduledExecutionFlowEntity = null;
		if (d.getScheduledExecutionFlow() != null) {
			scheduledExecutionFlowEntity = ScheduledExecutionFlowEntity.builder().id(d.getScheduledExecutionFlow().getId()).build();
		}
		EventSourceEntity eventSourceEntity = null;
		if (d.getEventSourceId() != null) {
			eventSourceEntity = eventSourceService.getEventSourceEntity(d.getEventSourceId());
		}
		
		EventEntity e = EventEntity.builder()
								   .errorDetectorEntity(errorDetectorEntity)
								   .eventSourceEntity(eventSourceEntity)
								   .id(d.getId())
								   .identifier(d.getIdentifier())
								   .logLevel(d.getLogLevel())
								   .initiatedBy(d.getInitiatedBy())
								   .status(d.getStatus())
							 	   .startDate(d.getStartDate())
							 	   .endDate(d.getEndDate())
							 	   .firstEventArrived(d.getFirstEventArrived())
							 	   .multipleEventsCounter(d.getMultipleEventsCounter())
							 	   .reason(d.getReason())
								   .payloadValues(d.getPayloadValues())
			                       .queryParameters(d.getQueryParameters())
							 	   .executionFlowEntity(executionFlowEntity)
							 	   .scheduledExecutionFlowEntity(scheduledExecutionFlowEntity)
								   .message(d.getMessage()).build();
		return e;
	}


}
