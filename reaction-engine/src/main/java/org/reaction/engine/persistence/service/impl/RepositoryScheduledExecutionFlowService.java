package org.reaction.engine.persistence.service.impl;

import javax.transaction.Transactional;

import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.engine.persistence.converter.ScheduledExecutionFlowToDomainConverter;
import org.reaction.engine.persistence.converter.ScheduledExecutionFlowToEntityConverter;
import org.reaction.engine.persistence.model.ScheduledExecutionFlowEntity;
import org.reaction.engine.persistence.repository.ScheduledExecutionFlowEntityRepository;
import org.reaction.engine.persistence.service.ScheduledExecutionFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryScheduledExecutionFlowService implements ScheduledExecutionFlowService {

	
	@Autowired
	private ScheduledExecutionFlowEntityRepository scheduledExecutionFlowEntityRepository;
	@Autowired
	private ScheduledExecutionFlowToEntityConverter<ScheduledExecutionFlow, ScheduledExecutionFlowEntity> scheduledExecutionFlowToEntityConverter;
	@Autowired
	private ScheduledExecutionFlowToDomainConverter<ScheduledExecutionFlowEntity, ScheduledExecutionFlow> scheduledExecutionFlowToDomainConverter;
	
	
	@Override
	@Transactional
	public ScheduledExecutionFlow save(ScheduledExecutionFlow scheduledExecutionFlow) {
		ScheduledExecutionFlowEntity entity = scheduledExecutionFlowToEntityConverter.convert(scheduledExecutionFlow);
		ScheduledExecutionFlowEntity savedEntity = scheduledExecutionFlowEntityRepository.save(entity);
		return scheduledExecutionFlowToDomainConverter.convert(savedEntity);
	}

	@Override
	@Transactional
	public ScheduledExecutionFlow getScheduledExecutionFlow(Long scheduledExecutionFlowId) {
		ScheduledExecutionFlowEntity entity = scheduledExecutionFlowEntityRepository.getOne(scheduledExecutionFlowId);
		return scheduledExecutionFlowToDomainConverter.convert(entity);
	}

}