package org.reaction.engine.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.contants.LogLevelEnum;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "EVENT")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EventEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_EventEntity")
	@SequenceGenerator(name="id_generator_EventEntity", sequenceName = "EVENT_SQ")
	private Long id;
	@Column(name = "IDENTIFIER", length = 70)
	private String identifier;
	@Column(name = "LOG_LEVEL")
	@Enumerated(EnumType.STRING)
	private LogLevelEnum logLevel;
	@Column(name = "INITIATED_BY")
	@Enumerated(EnumType.STRING)
	private EventInitiatorEnum initiatedBy;
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private EventStatusEnum status;
	@Column(name = "MESSAGE", length = 20000)
	private String message;
	@Column(name = "START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar startDate;
	@Column(name = "END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar endDate;
	// it makes sense only at events where more exceptions are needed to start the executionFlow (MULTIPLE... are set)
	// it contains the date of the first event arrived -> it is here only for performance reasons
	@Column(name = "FIRST_EVENT_ARRIVED")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar firstEventArrived;
	// it makes sense only at events where more exceptions are needed to start the executionFlow (MULTIPLE... are set)
	// the counter of the how many events arrived
	@Column(name = "MULTIPLE_EVENTS_COUNTER")
	private Integer multipleEventsCounter;
	// set a reason why the executionFlow is started manually
	// no meaning for INITIATED_BY_LOG events
	@Column(name = "REASON")
	private String reason;
	@Column(name = "PAYLOAD_VALUES", length = 3000)
	private String payloadValues;
	@Column(name = "QUERY_PARAMETERS", length = 3000)
	private String queryParameters;
	// it will have value only when starting manually or by the scheduler
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EXECUTION_FLOW_ID")
	private ExecutionFlowEntity executionFlowEntity;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ERROR_DETECTOR_ID")
	private ErrorDetectorEntity errorDetectorEntity;
	// when an event is reported (in WorkerController) then the eventSourceId of the Event domain is filled (not the EventSource DO for performance reason) -> when saving the event
	//     the converter will fill the eventSourceEntity
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EVENT_SOURCE_ID")
	private EventSourceEntity eventSourceEntity;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SCHEDULED_EXECUTION_FLOW_ID")
	private ScheduledExecutionFlowEntity scheduledExecutionFlowEntity;

	public void setMessage(String message) {
		this.message = message.length() > 20000 ? message.substring(0,20000) : message;
	}
}