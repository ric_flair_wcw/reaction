package org.reaction.engine.persistence.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "WORKER_STATUS")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkerStatusEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_WorkerStatusEntity")
	@SequenceGenerator(name="id_generator_WorkerStatusEntity", sequenceName = "WORKER_STATUS_SQ")
	private Long id;

	@Column(name = "HOST")
	private String host;
	
	@Column(name = "REFRESH_EVENT_SOURCES")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar refreshEventSource;

	@Column(name = "REPORT_EVENT")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar reportEvent;

	@Column(name = "CHECK_COMMANDS")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar checkCommands;

	@Column(name = "SEND_COMMAND_RESULT")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar sendCommandResult;

}
