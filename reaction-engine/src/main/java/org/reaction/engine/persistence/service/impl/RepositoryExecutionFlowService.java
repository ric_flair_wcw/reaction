package org.reaction.engine.persistence.service.impl;

import javax.transaction.Transactional;

import org.reaction.common.contants.ExecutionFlowStatusEnum;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.engine.persistence.converter.ExecutionFlowToDomainConverter;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.repository.ExecutionFlowEntityRepository;
import org.reaction.engine.persistence.service.ExecutionFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryExecutionFlowService implements ExecutionFlowService {

	
	@Autowired
	private ExecutionFlowEntityRepository executionFlowEntityRepository;
	@Autowired
	private ExecutionFlowToDomainConverter<ExecutionFlowEntity, ExecutionFlow> executionFlowToDomainConverter;
	
	
	@Override
	@Transactional
	public ExecutionFlow getExecutionFlow(Long executionFlowId) {
		ExecutionFlowEntity entity = executionFlowEntityRepository.getOne(executionFlowId);
		return executionFlowToDomainConverter.convert(entity);
	}


	@Override
	public void changeStatus(ExecutionFlow executionFlow, ExecutionFlowStatusEnum status) {
		ExecutionFlowEntity entity = executionFlowEntityRepository.getOne(executionFlow.getId());
		entity.setStatus(status);
		executionFlowEntityRepository.save(entity);
	}
	
}
