package org.reaction.engine.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "SCHEDULED_EXECUTION_FLOW")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScheduledExecutionFlowEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_ScheduledExecutionFlowEntity")
	@SequenceGenerator(name="id_generator_ScheduledExecutionFlowEntity", sequenceName = "SCHEDULED_EXECUTION_FLOW_SQ")
	private Long id;
	
	@Column(name = "NAME")
	private String name;

	// set a reason why the executionFlow is scheduled
	// no meaning for INITIATED_BY_LOG events
	@Column(name = "REASON")
	private String reason;
	
	@Column(name = "CRON_EXPRESSION")
	private String cronExpression;
	
	@Column(name = "SCHEDULED_ALREADY")
	private Boolean scheduledAlready;

	// if an error occurred when executing the scheduled event (in)
	@Column(name = "ERROR_AT_LAST_RUN")
	private String errorAtLastRun;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EXECUTION_FLOW_ID")
	private ExecutionFlowEntity executionFlowEntity;
	

}
