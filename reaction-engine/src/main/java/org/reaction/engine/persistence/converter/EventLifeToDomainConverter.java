package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.Command;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.reaction.engine.persistence.model.TaskEntity;
import org.springframework.beans.factory.annotation.Autowired;

public class EventLifeToDomainConverter<A, B> implements SuperConverter<EventLifeEntity, EventLife> {


    @Autowired
    private EventToDomainConverter<EventEntity, Event> eventToDomainConverter;
    @Autowired
    private TaskToDomainConverter<TaskEntity, Command> taskToDomainConverter;


    @Override
    public EventLife apply(EventLifeEntity e) {
        if (e == null) {
            return null;
        }
        e.getTaskEntity().getCommand();
        EventLife d = EventLife.builder()
                               .eventDate(e.getEventDate())
                               .event( eventToDomainConverter.convert(e.getEventEntity()) )
                               .eventStatus(e.getEventStatus())
                               .id(e.getId())
                               .order(e.getOrder())
                               .output(e.getOutput())
                               .extractedValue(e.getExtractedValue())
                               .errorMessage(e.getErrorMessage())
                               .extCommandSuccesful(e.getExtCommandSuccesful())
                               .history(e.getHistory())
                               .loopIndex(e.getLoopIndex())
                               .byWhom(e.getByWhom())
                               .task( taskToDomainConverter.convert(e.getTaskEntity()) ).build();
        return d;
    }


}
