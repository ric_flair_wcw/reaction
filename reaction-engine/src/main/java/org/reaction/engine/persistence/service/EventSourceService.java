package org.reaction.engine.persistence.service;

import org.reaction.common.domain.EventSource;
import org.reaction.common.domain.EventSourceType;
import org.reaction.engine.persistence.model.EventSourceEntity;

import java.util.List;

public interface EventSourceService {

	List<EventSource> getEventSourceByHost(String host);
	
	EventSourceEntity getEventSourceEntity(Long eventSourceId);
	
	EventSource getEventSource(Long eventSourceId);

	EventSource getEventSourceAndFillProperties(Long eventSourceId);

	List<EventSource> getEventSourceByEventSourceType(EventSourceType eventSourceType);
	
}