package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.CommandsToBeExecuted;
import org.reaction.common.domain.EventLife;
import org.reaction.engine.persistence.model.CommandsToBeExecutedEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.springframework.beans.factory.annotation.Autowired;

public class CommandsToBeExecutedToDomainConverter<A, B> implements SuperConverter<CommandsToBeExecutedEntity, CommandsToBeExecuted> {

	
	@Autowired
	private EventLifeToDomainConverter<EventLifeEntity, EventLife> eventLifeToDomainConverter;
	
	
	@Override
	public CommandsToBeExecuted apply(CommandsToBeExecutedEntity e) {
		if (e == null) {
			return null;
		}
		CommandsToBeExecuted d = CommandsToBeExecuted.builder()
													 .id(e.getId())
													 .executed(e.isExecuted())
													 .eventLife( eventLifeToDomainConverter.convert(e.getEventLifeEntity()) ).build();
		return d;
	}
	

}
