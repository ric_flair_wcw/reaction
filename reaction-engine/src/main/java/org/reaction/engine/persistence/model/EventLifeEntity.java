package org.reaction.engine.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.reaction.common.contants.EventStatusEnum;

import javax.persistence.*;
import java.util.Calendar;

@Entity
@Table(name = "EVENT_LIFE")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventLifeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_EventLifeEntity")
	@SequenceGenerator(name="id_generator_EventLifeEntity", sequenceName = "EVENT_LIFE_SQ")
	private Long id;
	@Column(name = "EVENT_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar eventDate;
	@Column(name = "ORDR")
	private Integer order;
	// it is the output of the COMMAND_BY_WORKER command after being executed (the last 30 lines only)
	@Column(name = "OUTPUT", length = 8000)
	private String output;
	// The extracted value from the OUTPUT (based on the TaskEntity.outputPattern). If no outputPattern exists then it contains the last line of the output
	@Column(name = "EXTRACTED_VALUE", length = 1000)
	private String extractedValue;
	@Column(name = "EXT_COMMAND_SUCCESSFUL")
	private Boolean extCommandSuccesful;
	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private EventStatusEnum eventStatus;
	@Column(name = "HISTORY", length=8000)
	private String history;
	@Column(name = "ERROR_MESSAGE", length = 20000)
	private String errorMessage;
	@Column(name = "BY_WHOM")
	private String byWhom;
	@Column(name = "LOOP_INDEX")
	private Integer loopIndex;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EVENT_ID")
	private EventEntity eventEntity;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TASK_ID")
	private TaskEntity taskEntity;

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage.length() > 20000 ? errorMessage.substring(0,20000) : errorMessage;
	}

	public void setOutput(String output) {
		this.output = output.length() > 8000 ? output.substring(0,8000) : output;
	}
}