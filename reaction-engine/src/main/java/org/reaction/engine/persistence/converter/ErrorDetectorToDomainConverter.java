package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.ErrorDetector;
import org.reaction.common.domain.EventSource;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.engine.persistence.model.ErrorDetectorEntity;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.springframework.beans.factory.annotation.Autowired;


public class ErrorDetectorToDomainConverter<A, B> implements SuperConverter<ErrorDetectorEntity, ErrorDetector> {

	
	@Autowired
	private ExecutionFlowToDomainConverter<ExecutionFlowEntity, ExecutionFlow> executionFlowToDomainConverter;
	@Autowired
	private EventSourceToDomainConverter<EventSourceEntity, EventSource> eventSourceToDomainConverter;

	
	@Override
	public ErrorDetector apply(ErrorDetectorEntity e) {
		if (e == null) {
			return null;
		}
		ErrorDetector d = ErrorDetector.builder()
				                   .name(e.getName())
				                   .messagePattern(e.getMessagePattern())
				                   .id(e.getId())
				                   .executionFlow(executionFlowToDomainConverter.convert(e.getExecutionFlowEntity()))
				                   .eventSource(eventSourceToDomainConverter.convert(e.getEventSourceEntity()))
				                   .activated(e.isActivated())
				                   .confirmationNeeded(e.isConfirmationNeeded())
				                   .multipleEventsCnt(e.getMultipleEventsCnt())
				                   .multipleEventsTimeframe(e.getMultipleEventsTimeframe())
								   .identifiers(e.getIdentifiers()).build();
		return d;
	}


}
