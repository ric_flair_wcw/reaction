package org.reaction.engine.persistence.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.reaction.common.domain.ErrorDetector;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.converter.ErrorDetectorToDomainConverter;
import org.reaction.engine.persistence.model.ErrorDetectorEntity;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.reaction.engine.persistence.repository.ErrorDetectorEntityRepository;
import org.reaction.engine.persistence.repository.EventSourceEntityRepository;
import org.reaction.engine.persistence.service.ErrorDetectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class RepositoryErrorDetectorService implements ErrorDetectorService {


    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryErrorDetectorService.class);

    @Autowired
    private ErrorDetectorEntityRepository errorDetectorEntityRepository;
    @Autowired
    private ErrorDetectorToDomainConverter<ErrorDetectorEntity, ErrorDetector> errorDetectorToDomainConverter;
    @Autowired
    private EventSourceEntityRepository eventSourceEntityRepository;
    @Autowired
    private ObjectMapper objectMapper;


    @Override
    @Transactional
    public ErrorDetector search(final Event event,
                                final boolean isFromWorker,
                                final Map<String, String> payloadValues,
                                final Map<String, String> queryParameters,
                                final EventSourceType eventSourceType) throws ReactionConfigurationException {
        // loading the error detector records from the database
        //    the error detector might be attached to a higher level eventSource (why? see RepositoryEventSourceService.getEventSourceByHost)
        //    so get all the eventSources starting from ITEM (what exists in event.eventSourceId) to top-level GROUP
        List<Long> eventSourceIds = getEventSourcesFromBottomToTop(event.getEventSourceId());
        LOGGER.trace("The eventSourceIds after calling the method 'getEventSourcesFromLowToTop': {}\nThe event is {}",
                eventSourceIds.stream().map(id -> id.toString()).collect(Collectors.joining(", ")), event);
        List<ErrorDetector> errorDetectors = errorDetectorToDomainConverter.convertToList( errorDetectorEntityRepository.findByEventSourceIds(eventSourceIds) );
        // first that error detector has to be used that belongs to the deepest eventSource (e.g. there is an top level GROUP and ITEM eventSource and 2 error detectors specified for them
        //  		-> first check the error detector that belongs to the ITEM then...)
        Comparator<ErrorDetector> comparator = (ed1,ed2) -> (  (Integer)eventSourceIds.indexOf(ed1.getEventSource().getId())  ).compareTo(  (Integer)eventSourceIds.indexOf( ed2.getEventSource().getId())  );
        errorDetectors.sort(comparator);
        // finding the errorDetector by regular expression
        if (isFromWorker) {
            errorDetectors = errorDetectors.stream().filter(errorDetector -> Pattern.compile(errorDetector.getMessagePattern()).matcher(event.getMessage()).find())
                                                    .collect(Collectors.toList());
        } else {
            errorDetectors = findErrorDetectorsByIdentifiers(errorDetectors, eventSourceType, payloadValues, queryParameters);
        }
        // it is a problem if more matching error detectors exist for the same ITEM eventSource (the logic is not sure which one to start) so check it
        //    however it is not a problem if there are more matching error detectors but for different eventSources (one for LOGFILE and there is another on for top level GROUP)
        //    other scenario : 1 error detector for top level GROUP, 2 for GROUP and 0 for ITEM => throw exception
        long cnt = 0;
        if (errorDetectors.size() > 1) {
            final ErrorDetector first = errorDetectors.get(0);
            cnt = errorDetectors.stream().filter(ed -> ed.getEventSource().getId().longValue() == first.getEventSource().getId()).count();
        }
        if (cnt > 1) {
            LOGGER.error("More than one error detector records can handle the incoming event so not sure which one should be used!\nThe error detectors in question:{}\nThe log entry that triggered the event: {}",
                    errorDetectors.stream().map(ed -> ed.getName()).collect(Collectors.joining(", ")), event.getMessage());
            throw new ReactionRuntimeException("More than one error detector records can handle the incoming event so not sure which one should be used! Please check the log for details.");
        }
        if (errorDetectors.size() == 0) {
            LOGGER.warn("No error detector could be found for the incoming event.\nEvent source: {}\nError detectors: {}\nPayload values: {}\nQuery parameters: {}", eventSourceType,
                    errorDetectors.stream().map(ed -> ed.getName()).collect(Collectors.joining(", ")), payloadValues, queryParameters);
            return null;
        } else {
            return errorDetectors.get(0);
        }
    }


    @Override
    @Transactional
    public List<ErrorDetector> getErrorDetectorsByEventSourcedIds(List<Long> eventSourceIds) {
        return errorDetectorToDomainConverter.convertToList( errorDetectorEntityRepository.findByEventSourceIds(eventSourceIds) );
    }


    @Override
    @Transactional
    public ErrorDetector getErrorDetectorById(Long id) {
        return errorDetectorToDomainConverter.convert( errorDetectorEntityRepository.findOne(id) );
    }


    // the Worker sends back a low level EventSource (ITEM) but we need all the all the EventSources that belong to this specific eventSource
    // so query all the eventSources from ITEM (what exists in event.eventSourceId) to top level GROUP
    private List<Long> getEventSourcesFromBottomToTop(Long eventSourceId) {
        List<Long> eventSourceIds = new ArrayList<>();
        eventSourceIds.add(eventSourceId);
        EventSourceEntity eventSourceEntity;
        do {
            eventSourceEntity = eventSourceEntityRepository.findOne(eventSourceIds.get(eventSourceIds.size()-1));
            if (eventSourceEntity == null) {
                throw new ReactionRuntimeException("The event source id '" + eventSourceIds.get(eventSourceIds.size()-1) + "' doesn't exist!");
            }
            if (eventSourceEntity.getParent() != null) {
                eventSourceIds.add(eventSourceEntity.getParent().getId());
            }
        } while(eventSourceEntity.getParent() != null);
        return eventSourceIds;
    }


    private List<ErrorDetector> findErrorDetectorsByIdentifiers(final List<ErrorDetector> errorDetectors,
                                                                final EventSourceType eventSourceType,
                                                                final Map<String, String> payloadValues,
                                                                final Map<String, String> queryParameters) throws ReactionConfigurationException {
        List<ErrorDetector> foundErrorDetectors = new ArrayList<>();
        for(ErrorDetector errorDetector : errorDetectors) {
            try {
                Map<String,String> identifiers = objectMapper.readValue(errorDetector.getIdentifiers() == null ? "{}" : errorDetector.getIdentifiers() ,
						                                                new TypeReference<Map<String,String>>() {});
                List<Map<String,String>> pathToIdentifiers = objectMapper.readValue(eventSourceType.getPathToIdentifiers(), new TypeReference<List<Map<String,String>>>() {});
                Set<String> namesInPathToIdentifiersPayload = pathToIdentifiers.stream().filter(m -> "ERROR_DETECTOR".equals(m.get("output")))
                                                                                        .filter(m -> "PAYLOAD".equals(m.get("source")))
                                                                                        .map(m -> m.get("name")+"-PAYLOAD")
                                                                                        .collect(Collectors.toSet());
                Map<String,String> identifiersPayload = identifiers.keySet().stream().filter(k -> k.endsWith("-PAYLOAD"))
                                                                                     .filter(k -> namesInPathToIdentifiersPayload.contains(k))
                                                                                     .collect(Collectors.toMap(k -> k.substring(0, k.length() - 8),
                                                                                                               k -> identifiers.get(k)));
                Set<String> namesInPathToIdentifiersQueryParam = pathToIdentifiers.stream().filter(m -> "ERROR_DETECTOR".equals(m.get("output")))
                                                                                           .filter(m -> "QUERY_PARAMETER".equals(m.get("source")))
                                                                                           .map(m -> m.get("name")+"-QUERY_PARAMETER")
                                                                                           .collect(Collectors.toSet());
                Map<String,String> identifiersQueryParam = identifiers.keySet().stream().filter(k -> k.endsWith("-QUERY_PARAMETER"))
                                                                                        .filter(k -> namesInPathToIdentifiersQueryParam.contains(k))
                                                                                        .collect(Collectors.toMap(k -> getPathFromPathToIdentifiersByName(pathToIdentifiers, k.substring(0, k.length() - 16)),
                                                                                                                  k -> identifiers.get(k)));
                LOGGER.debug("payloadValues: {}   ?==   identifiersPayload: {}", payloadValues, identifiersPayload);
                LOGGER.debug("queryParameters: {}   ?==   identifiersQueryParam: {}", queryParameters, identifiersQueryParam);
                boolean foundInvalidPayloadIdentifier = identifiersPayload.keySet().stream().anyMatch(k -> StringUtils.isEmpty(payloadValues.get(k)) ||
                                                                                                           !payloadValues.get(k).matches(identifiersPayload.get(k)));
                boolean foundInvalidQueryParam = identifiersQueryParam.keySet().stream().anyMatch(k -> StringUtils.isEmpty(queryParameters.get(k)) ||
                                                                                                       !queryParameters.get(k).matches(identifiersQueryParam.get(k)));
                LOGGER.debug("foundInvalidPayloadIdentifier: {}, foundInvalidQueryParam: {}", foundInvalidPayloadIdentifier, foundInvalidQueryParam);
                if (!foundInvalidQueryParam && !foundInvalidPayloadIdentifier) {
                    foundErrorDetectors.add(errorDetector);
                }
            } catch(IOException|NullPointerException e) {
                LOGGER.error("Error occurred when trying the parse the 'identifiers' in the following ErrorDetector: {}", errorDetector);
                throw new ReactionConfigurationException("Error occurred when reading the JSON text from ErrorDetector.identifiers!", e);
            }
        }
        return foundErrorDetectors;
    }


    private String getPathFromPathToIdentifiersByName(List<Map<String,String>> pathToIdentifiers, String name) {
		return pathToIdentifiers.stream().filter(m -> m.get("name").equals(name))
 										 .map(m -> m.get("path"))
										 .findFirst()
										 .orElse(name);
	}

}
