package org.reaction.engine.persistence.service;

import org.reaction.common.domain.EventSourceType;

public interface EventSourceTypeService {

	public EventSourceType getEventSourceTypeByCode(String code);
	
}