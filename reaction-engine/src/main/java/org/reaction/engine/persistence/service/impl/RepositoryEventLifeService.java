package org.reaction.engine.persistence.service.impl;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.contants.TaskEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.engine.persistence.converter.EventLifeToDomainConverter;
import org.reaction.engine.persistence.converter.EventLifeToEntityConverter;
import org.reaction.engine.persistence.converter.EventToEntityConverter;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.reaction.engine.persistence.model.TaskEntity;
import org.reaction.engine.persistence.repository.EventLifeEntityRepository;
import org.reaction.engine.persistence.repository.TaskEntityRepository;
import org.reaction.engine.persistence.service.EventLifeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RepositoryEventLifeService implements EventLifeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryEventLifeService.class);

	@Autowired
	private EventLifeEntityRepository eventLifeEntityRepository;
	@Autowired
	private EventLifeToDomainConverter<EventLifeEntity, EventLife> eventLifeToDomainConverter;
	@Autowired
	private EventLifeToEntityConverter<EventLifeEntity, EventLife> eventLifeToEntityConverter;
	@Autowired
	private EventToEntityConverter<Event, EventEntity> eventToEntityConverter;
	@Autowired
	private TaskEntityRepository taskEntityRepository;
	
	
	@Override
	@Transactional
	public EventLife getEventLife(Long eventLifeId) {
		// searching for the executionFlow in the DB
		EventLifeEntity eventLifeEntity = eventLifeEntityRepository.findOne(eventLifeId);
		EventLife eventLife = eventLifeToDomainConverter.convert(eventLifeEntity);
		
		return eventLife;
	}
	

	@Override
	@Transactional
	public EventLife save(EventLife eventLife) {
		EventLifeEntity entity = eventLifeToEntityConverter.convert(eventLife);
		EventLifeEntity savedEntity = eventLifeEntityRepository.save(entity);
		eventLife.setId(savedEntity.getId());
		return eventLife;
	}

	
	@Override
	@Transactional
	public EventLife getEventLifeByOrderAndEvent(Integer order, Event event) {
		EventEntity eventEntity = eventToEntityConverter.convert(event);
		EventLifeEntity prevEventLifeEntity = eventLifeEntityRepository.findByOrderAndEventEntity(--order, eventEntity );
		return eventLifeToDomainConverter.convert(prevEventLifeEntity);
	}


	@Override
	@Transactional
	public int getCountByEventAndEventStatus(Event latestEvent, EventStatusEnum eventStatus) {
		EventEntity latestEventEntity = eventToEntityConverter.convert(latestEvent);
		return eventLifeEntityRepository.countByEventEntityAndEventStatus(latestEventEntity, eventStatus);
	}


	@Override
	@Transactional
	public Integer getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(Long eventId, Long taskId) {
		List<EventLifeEntity> loopEventLifeEntities = eventLifeEntityRepository.getMaximumLoopIndexByEventEntity(eventId);
		// - LOOP 1
		//    - LOOP 2
		// check what is the 1st element in loopEventLifeEntities
		//  - if its task id == taskId then return its loop_index
		//  - if it is not then check
		//        - if the loop in 1st element of loopEventLifeEntities (LOOP 1) is higher than taskId loop (LOOP 2) then return null (the loopIndex has to be initiated to zero) -> the LOOP 2 is checked
		//              (it is passed as parameter) and when the preceding loop (LOOP 1) in the execution flow is higher then the LOOP 2 has to start from 0
		//        - if the loop in 1st element of loopEventLifeEntities (LOOP 2) is lower then taskId loop (LOOP 1) then go back in loopEventLifeEntities list until it finds a similar loop than taskId loop (LOOP 1)
		//              and return its loop_index otherwise null -> the LOOP 1 is checked (it is passed as parameter) and when the preceding loop (LOOP 2) in the execution flow is lower then likely there was another cycle
		//              of LOOP 1 beforehand so look for it
		TaskEntity currentTaskEntity = taskEntityRepository.getOne(taskId);
		if (loopEventLifeEntities.size() == 0) {
			return null;
		} else if (loopEventLifeEntities.get(0).getTaskEntity().getId().longValue() == getParentLoop(currentTaskEntity)) {
			return loopEventLifeEntities.get(0).getLoopIndex();
		} else {
			if (isHigher(loopEventLifeEntities.get(0).getTaskEntity(), currentTaskEntity)) {
				return null;
			} else {
				return loopEventLifeEntities.stream().filter(ele -> ele.getTaskEntity().getId().longValue() == taskId)
						                             .map(ele -> ele.getLoopIndex())
						                             .findFirst().orElse(null);
			}
		}
	}

	private long getParentLoop(TaskEntity taskEntity) {
		if (TaskEnum.LOOP.equals(taskEntity.getInternalTask())) {
			return taskEntity.getId();
		} else {
			return taskEntity.getPrimaryTaskEntity() == null && taskEntity.getSecondaryTaskEntity() == null ? -1 :
				   taskEntity.getPrimaryTaskEntity() != null ? getParentLoop(taskEntity.getPrimaryTaskEntity()) : getParentLoop(taskEntity.getSecondaryTaskEntity());
		}
	}

	private boolean isHigher(TaskEntity firstTaskEntity, TaskEntity currentTaskEntity) {
		if (currentTaskEntity == null) {
			return false;
		} else if (!TaskEnum.LOOP.equals(currentTaskEntity.getInternalTask())) {
			return currentTaskEntity.getPrimaryTaskEntity() == null && currentTaskEntity.getSecondaryTaskEntity() == null ? false :
				   currentTaskEntity.getPrimaryTaskEntity() != null ? isHigher(firstTaskEntity, currentTaskEntity.getPrimaryTaskEntity()) : isHigher(firstTaskEntity, currentTaskEntity.getSecondaryTaskEntity());
		} else {
			return firstTaskEntity.getId().longValue() == currentTaskEntity.getId().longValue() ? true : isHigher(firstTaskEntity, currentTaskEntity.getPrimaryTaskEntity());
		}
	}

}