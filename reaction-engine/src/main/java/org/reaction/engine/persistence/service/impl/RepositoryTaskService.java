package org.reaction.engine.persistence.service.impl;

import org.reaction.common.domain.Command;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.SuperCommand;
import org.reaction.common.domain.task.LoopTask;
import org.reaction.common.domain.task.SuperTask;
import org.reaction.engine.persistence.converter.ExecutionFlowToEntityConverter;
import org.reaction.engine.persistence.converter.TaskToDomainConverter;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.TaskEntity;
import org.reaction.engine.persistence.repository.TaskEntityRepository;
import org.reaction.engine.persistence.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class RepositoryTaskService implements TaskService {

	
	@Autowired
	private TaskEntityRepository taskEntityRepository;
	@Autowired
	private TaskToDomainConverter<TaskEntity, Command> taskToDomainConverter;
	@Autowired
	private ExecutionFlowToEntityConverter<ExecutionFlow, ExecutionFlowEntity> executionFlowToEntityConverter;
	
	
	@Override
	@Transactional
	public List<SuperCommand> getNextTask(Long primaryTaskId, Long secondaryTaskId, Long executionFlowId, Integer order) {
		List<TaskEntity> taskEntities = taskEntityRepository.findNextTask(primaryTaskId, secondaryTaskId, executionFlowId, order);
		List<SuperCommand> commands = taskToDomainConverter.convertToList(taskEntities);
		
		return commands;
	}


	@Override
	@Transactional
	public SuperCommand getNextTaskRecursively(final SuperTask _task) {
		SuperTask task = _task;
		SuperCommand command = null;
		boolean stopCycles = false;
		while (!stopCycles) {
			// based on the parent task id (primaryTaskEntity or secondaryTaskEntity) and the order of the current task (-> getTaskId() ) the next task can be queried
			// if there isn't next task on this level then keep going up one level until finds the next task
			List<SuperCommand> commands = getNextTask(task.getPrimaryTaskId(), 
					                                  task.getSecondaryTaskId(), 
					                                  task.getExecutionFlow() == null ? null : task.getExecutionFlow().getId(), 
					                                  task.getOrder());
			if (commands.size() > 0) {
				stopCycles = true;
				command = commands.get(0);
			} else {
				// if we are at the top level but didn't find the next then we are at the end of the executionFlow
				if (task.getExecutionFlow() != null) {
					stopCycles = true;
				}
				// else going up one level
				else {
					task = getCommandById(task.getPrimaryTaskId() == null ? task.getSecondaryTaskId() : task.getPrimaryTaskId()).getSuperTask();
					// if the parent task is a loop then it has to be retrieved as a new cycle might have to be started
					if (task instanceof LoopTask) {
						return taskToDomainConverter.apply(taskEntityRepository.getOne(task.getId()));
					}
				}
			}
		}
		return command;
	}

	
	@Override
	@Transactional
	public SuperCommand getCommandById(Long taskId) {
		TaskEntity taskEntity = taskEntityRepository.findOne(taskId);
		SuperCommand command = taskToDomainConverter.apply(taskEntity);
		
		return command;
	}


	@Override
	@Transactional
	public List<SuperCommand> getByPrimaryTaskOrderBy(Long taskId) {
		TaskEntity taskEntity = taskEntityRepository.findOne(taskId);
		List<TaskEntity> primaries = taskEntityRepository.findByPrimaryTaskEntityOrderByOrderAsc(taskEntity);
		
		return taskToDomainConverter.convertToList(primaries);
	}
	

	@Override
	@Transactional
	public List<SuperCommand> getBySecondaryTaskOrderBy(Long taskId) {
		TaskEntity taskEntity = taskEntityRepository.findOne(taskId);
		List<TaskEntity> primaries = taskEntityRepository.findBySecondaryTaskEntityOrderByOrderAsc(taskEntity);
		
		return taskToDomainConverter.convertToList(primaries);
	}


	@Override
	@Transactional
	public SuperCommand getTopByExecutionFlowEntityOrderByOrderAsc(ExecutionFlow executionFlow) {
		TaskEntity taskEntity = taskEntityRepository.findTopByExecutionFlowEntityOrderByOrderAsc( executionFlowToEntityConverter.convert(executionFlow) );
		
		return taskToDomainConverter.convert(taskEntity);
	}

}
