package org.reaction.engine.persistence.service.impl;

import org.reaction.common.domain.CommandsToBeExecuted;
import org.reaction.engine.persistence.converter.CommandsToBeExecutedToDomainConverter;
import org.reaction.engine.persistence.converter.CommandsToBeExecutedToEntityConverter;
import org.reaction.engine.persistence.model.CommandsToBeExecutedEntity;
import org.reaction.engine.persistence.repository.CommandsToBeExecutedEntityRepository;
import org.reaction.engine.persistence.service.CommandsToBeExecutedService;
import org.reaction.engine.service.PlaceholderVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class RepositoryCommandsToBeExecutedService implements CommandsToBeExecutedService {

	
	@Autowired
	private CommandsToBeExecutedEntityRepository commandsToBeExecutedEntityRepository;
	@Autowired
	private CommandsToBeExecutedToEntityConverter<CommandsToBeExecuted, CommandsToBeExecutedEntity> commandsToBeExecutedToEntityConverter;
	@Autowired
	private CommandsToBeExecutedToDomainConverter<CommandsToBeExecutedEntity, CommandsToBeExecuted> commandsToBeExecutedToDomainConverter;
	@Autowired
	private PlaceholderVariableService placeholderVariableService;

	
	@Override
	@Transactional
	public void save(CommandsToBeExecuted d) {
		CommandsToBeExecutedEntity e = commandsToBeExecutedToEntityConverter.convert(d);
		commandsToBeExecutedEntityRepository.save(e);
	}


	@Override
	@Transactional
	public List<CommandsToBeExecuted> processCommandsToBeExecuted(String host) {
		// executing a SELECT ... FOR UPDATE
		// it retrieves the records where host matches or the host contains '##{' and '}##'
		List<CommandsToBeExecutedEntity> commandsToBeExecutedEntities = commandsToBeExecutedEntityRepository.getCommandsToBeExecutedEntityByHost(host).stream()
					.filter(c -> hasSameHost(c,host))
					.collect(Collectors.toList());
		// updating the executed to true
		commandsToBeExecutedEntities.stream().forEach(c -> c.setExecuted(true));
		commandsToBeExecutedEntityRepository.save(commandsToBeExecutedEntities);
		// returning the result
		return commandsToBeExecutedToDomainConverter.convertToList(commandsToBeExecutedEntities);
	}

	private boolean hasSameHost(CommandsToBeExecutedEntity c, String host) {
		String filledHost = placeholderVariableService.replace(c.getEventLifeEntity().getTaskEntity().getHost(), c.getEventLifeEntity().getId());
		return host.equals(filledHost);
	}

}
