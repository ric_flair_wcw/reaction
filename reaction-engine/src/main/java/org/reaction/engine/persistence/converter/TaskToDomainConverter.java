package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.SuperCommand;
import org.reaction.common.domain.task.*;
import org.reaction.engine.persistence.model.TaskEntity;
import org.reaction.engine.persistence.service.TaskService;
import org.reaction.engine.tasks.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TaskToDomainConverter<A, B> implements SuperConverter<TaskEntity, SuperCommand> {


    @Autowired
    private TaskService taskService;
    @Autowired
    private ApplicationContext context;


    @Override
    public SuperCommand apply(TaskEntity t) {
        if (t == null) {
            return null;
        }
        SuperNextCommand command = null;
        ExecutionFlow executionFlow = null;
        if (t.getExecutionFlowEntity() != null) {
            executionFlow = ExecutionFlow.builder().id(t.getExecutionFlowEntity().getId()).build();
        }
        switch(t.getInternalTask()) {
            case COMMAND_BY_WORKER:
                CommandByWorkerTask commandByWorkerTask = CommandByWorkerTask.builder()
                                                        .command(t.getCommand())
                                                        .osUser(t.getOsUser())
                                                        .host(t.getHost())
                                                        .outputPattern(t.getOutputPattern()).build();
                setBasicProperties(commandByWorkerTask, t, executionFlow);
                command = (CommandByWorkerTaskCommand) context.getBean("commandByWorkerTaskCommand", commandByWorkerTask);
                break;
            case COMMAND_USING_SSH:
                CommandUsingSSHTask commandUsingSSHTask = CommandUsingSSHTask.builder().command(t.getCommand())
                                                                                       .osUser(t.getOsUser())
                                                                                       .osPassword(t.getOsPassword())
                                                                                       .sshAuthType(t.getSshAuthType())
                                                                                       .sshKeyPath(t.getSshKeyPath())
                                                                                       .isPwdSetManually(t.getIsPwdSetManually())
                                                                                       .host(t.getHost())
                                                                                       .executionTimeout(t.getExecutionTimeout())
                                                                                       .outputPattern(t.getOutputPattern()).build();
                setBasicProperties(commandUsingSSHTask, t, executionFlow);
                command = (CommandUsingSSHTaskCommand) context.getBean("commandUsingSSHTaskCommand", commandUsingSSHTask);
                break;
            case IF_ELSE:
                List<SuperCommand> ifBranchCommands = taskService.getByPrimaryTaskOrderBy(t.getId());
                List<SuperCommand> elseBranchCommands = taskService.getBySecondaryTaskOrderBy(t.getId());
                InternalIfElseTask internalIfElseTask = InternalIfElseTask.builder().ifExpression(t.getIfExpression())
                                                                                  .firstCommandInIf(ifBranchCommands == null || ifBranchCommands.size() == 0 ? null : ifBranchCommands.get(0))
                                                                                  .firstCommandInElse(elseBranchCommands == null || elseBranchCommands.size() == 0 ? null : elseBranchCommands.get(0)).build();
                setBasicProperties(internalIfElseTask, t, executionFlow);
                command = (IfElseTaskCommand) context.getBean("ifElseTaskCommand", internalIfElseTask);
                break;
            case LOOP:
                ifBranchCommands = taskService.getByPrimaryTaskOrderBy(t.getId());
                LoopTask loopTask = LoopTask.builder()
                        .loopValueList1(t.getLoopValueList1())
                        .loopSeparator1(t.getLoopSeparator1())
                        .loopRequiresEncryption1(t.getLoopRequiresEncryption1())
                        .loopValueList2(t.getLoopValueList2())
                        .loopSeparator2(t.getLoopSeparator2())
                        .loopRequiresEncryption2(t.getLoopRequiresEncryption2())
                        .loopValueList3(t.getLoopValueList3())
                        .loopSeparator3(t.getLoopSeparator3())
                        .loopRequiresEncryption3(t.getLoopRequiresEncryption3())
                        .loopValueList4(t.getLoopValueList4())
                        .loopSeparator4(t.getLoopSeparator4())
                        .loopRequiresEncryption4(t.getLoopRequiresEncryption4())
                        .firstCommandInLoop(ifBranchCommands == null || ifBranchCommands.size() == 0 ? null : ifBranchCommands.get(0))
                        .build();
                setBasicProperties(loopTask, t, executionFlow);
                command = (LoopTaskCommand) context.getBean("loopTaskCommand", loopTask);
                break;
            case EMAIL_SENDING:
                List<String> recipients = t.getMailRecipients() == null ? new ArrayList<>() :
                                          Arrays.asList(t.getMailRecipients().split(",")).stream()
                                                                                         .map(r -> r.trim())
                                                                                         .collect(Collectors.toList());
                EmailSendingTask emailSendingTask = EmailSendingTask.builder()
                                                                    .recipients(recipients)
                                                                    .subject(t.getMailSubject())
                                                                    .content(t.getMailContent()).build();
                setBasicProperties(emailSendingTask, t, executionFlow);
                command = (EmailSendingTaskCommand) context.getBean("emailSendingTaskCommand", emailSendingTask);
                break;
            case FAILURE:
                FailureTask failureTask = FailureTask.builder().build();
                setBasicProperties(failureTask, t, executionFlow);
                command = (FailureTaskCommand) context.getBean("failureTaskCommand", failureTask);
                break;

        }
        return command;
    }


    private void setBasicProperties(SuperTask superTask, TaskEntity t, ExecutionFlow executionFlow) {
        superTask.setId(t.getId());
        superTask.setName(t.getName());
        superTask.setOrder(t.getOrder());
        superTask.setExecutionFlow(executionFlow);
        superTask.setPrimaryTaskId(t.getPrimaryTaskEntity() == null ? null : t.getPrimaryTaskEntity().getId());
        superTask.setSecondaryTaskId(t.getSecondaryTaskEntity() == null ? null : t.getSecondaryTaskEntity().getId());
    }


}