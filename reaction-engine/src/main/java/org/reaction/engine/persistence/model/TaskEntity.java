package org.reaction.engine.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.reaction.common.contants.SshAuthTypeEnum;
import org.reaction.common.contants.TaskEnum;

import javax.persistence.*;


@Entity
@Table(name = "TASK")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_TaskEntity")
	@SequenceGenerator(name="id_generator_TaskEntity", sequenceName = "TASK_SQ")
	private Long id;
	@Column(name = "NAME")
	private String name;
	@Column(name = "ORDR")
	private Integer order;
	@Column(name = "COMMAND")
	private String command;
	@Column(name = "SSH_AUTH_TYPE")
	@Enumerated(EnumType.STRING)
	private SshAuthTypeEnum sshAuthType;
	@Column(name = "SSH_KEY_PATH")
	private String sshKeyPath;
	@Column(name = "OS_USER")
	private String osUser;
	@Column(name = "OS_PASSWORD")
	private String osPassword;
	@Column(name = "IS_PWD_SET_MANUALLY")
	private Boolean isPwdSetManually;
	@Column(name = "HOST")
	private String host;
	@Column(name = "EXECUTION_TIMEOUT")
	private Integer executionTimeout;
	@Column(name = "INTERNAL_TASK")
	@Enumerated(EnumType.STRING)
	private TaskEnum internalTask;
	@Column(name = "IF_EXPRESSION")
	private String ifExpression;
	@Column(name = "OUTPUT_PATTERN")
	private String outputPattern;
	@Column(name = "MAIL_RECIPIENTS")
	private String mailRecipients;
	@Column(name = "MAIL_SUBJECT")
	private String mailSubject;
	@Column(name = "MAIL_CONTENT")
	private String mailContent;
	@Column(name = "LOOP_VALUE_LIST_1")
	private String loopValueList1;
	@Column(name = "LOOP_SEPARATOR_1")
	private String loopSeparator1;
	@Column(name = "LOOP_REQUIRES_ENCRYPTION_1")
	private String loopRequiresEncryption1;
	@Column(name = "LOOP_PLACEHOLDER_VAR_1")
	private String loopPlaceholderVar1;
	@Column(name = "LOOP_VALUE_LIST_2")
	private String loopValueList2;
	@Column(name = "LOOP_SEPARATOR_2")
	private String loopSeparator2;
	@Column(name = "LOOP_REQUIRES_ENCRYPTION_2")
	private String loopRequiresEncryption2;
	@Column(name = "LOOP_PLACEHOLDER_VAR_2")
	private String loopPlaceholderVar2;
	@Column(name = "LOOP_VALUE_LIST_3")
	private String loopValueList3;
	@Column(name = "LOOP_SEPARATOR_3")
	private String loopSeparator3;
	@Column(name = "LOOP_REQUIRES_ENCRYPTION_3")
	private String loopRequiresEncryption3;
	@Column(name = "LOOP_PLACEHOLDER_VAR_3")
	private String loopPlaceholderVar3;
	@Column(name = "LOOP_VALUE_LIST_4")
	private String loopValueList4;
	@Column(name = "LOOP_SEPARATOR_4")
	private String loopSeparator4;
	@Column(name = "LOOP_REQUIRES_ENCRYPTION_4")
	private String loopRequiresEncryption4;
	@Column(name = "LOOP_PLACEHOLDER_VAR_4")
	private String loopPlaceholderVar4;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EXECUTION_FLOW_ID")
	private ExecutionFlowEntity executionFlowEntity;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRIMARY_TASK_ID")
	// it is for the commands if the 'if' condition is true; or for the commands in a 'loop' task
	private TaskEntity primaryTaskEntity;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SECONDARY_TASK_ID")
	// it is for the commands if the 'if' condition is false; it doesn't make sense for 'loop'
	private TaskEntity secondaryTaskEntity;

}
