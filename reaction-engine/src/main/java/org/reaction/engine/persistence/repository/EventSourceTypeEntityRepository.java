package org.reaction.engine.persistence.repository;

import org.reaction.engine.persistence.model.EventSourceTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventSourceTypeEntityRepository extends JpaRepository<EventSourceTypeEntity, Long> {
	
	public EventSourceTypeEntity findByCode(String code);
	
	
}
