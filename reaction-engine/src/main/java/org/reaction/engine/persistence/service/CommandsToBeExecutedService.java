package org.reaction.engine.persistence.service;

import java.util.List;

import org.reaction.common.domain.CommandsToBeExecuted;

public interface CommandsToBeExecutedService {

	
	public void save(CommandsToBeExecuted d);
	
	public List<CommandsToBeExecuted> processCommandsToBeExecuted(String host);
	
}
