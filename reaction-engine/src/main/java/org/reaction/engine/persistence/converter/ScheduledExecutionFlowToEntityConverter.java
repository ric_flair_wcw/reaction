package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.ScheduledExecutionFlowEntity;

public class ScheduledExecutionFlowToEntityConverter<A, B> implements SuperConverter<ScheduledExecutionFlow, ScheduledExecutionFlowEntity> {

	
	@Override
	public ScheduledExecutionFlowEntity apply(ScheduledExecutionFlow d) {
		if (d == null) {
			return null;
		}
		
		ExecutionFlowEntity executionFlowEntity = null;
		if (d.getExecutionFlow() != null) {
			executionFlowEntity = ExecutionFlowEntity.builder().id(d.getExecutionFlow().getId()).build();
		}
		
		ScheduledExecutionFlowEntity e = ScheduledExecutionFlowEntity.builder()
								   					 .id(d.getId())
								   					 .name(d.getName())
								   					 .cronExpression(d.getCronExpression())
								   					 .reason(d.getReason())
								   					 .scheduledAlready(d.getScheduledAlready())
								   					 .errorAtLastRun(d.getErrorAtLastRun())
								   					 .executionFlowEntity(executionFlowEntity).build();
		return e;
	}


}
