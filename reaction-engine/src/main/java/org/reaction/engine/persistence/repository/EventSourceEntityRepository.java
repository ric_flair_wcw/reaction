package org.reaction.engine.persistence.repository;

import java.util.List;

import org.reaction.engine.persistence.model.EventSourceTypeEntity;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventSourceEntityRepository extends JpaRepository<EventSourceEntity, Long> {
	
	List<EventSourceEntity> findByHost(String host);
	
	List<EventSourceEntity> findByParent(EventSourceEntity parent);

	List<EventSourceEntity> findByEventSourceTypeEntity(EventSourceTypeEntity eventSourceTypeEntity);
	
}
