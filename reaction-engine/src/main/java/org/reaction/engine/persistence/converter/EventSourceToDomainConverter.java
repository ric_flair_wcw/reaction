package org.reaction.engine.persistence.converter;


import java.time.Clock;

import org.reaction.common.domain.EventSourceType;
import org.reaction.common.domain.MaintenanceWindow;
import org.reaction.common.domain.EventSource;
import org.reaction.engine.persistence.model.EventSourceTypeEntity;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.springframework.beans.factory.annotation.Autowired;


public class EventSourceToDomainConverter<A, B> implements SuperConverter<EventSourceEntity, EventSource> {

	@Autowired
	private EventSourceTypeToDomainConverter<EventSourceTypeEntity, EventSourceType> eventSourceTypeToDomainConverter;

	@Override
	public EventSource apply(EventSourceEntity e) {
		if (e == null) {
			return null;
		}
		// setting the basic properties
		return EventSource.builder()
                              .id(e.getId())
                              .description(e.getDescription())
                              .host(e.getHost())
                              .logPath(e.getLogPath())
                              .name(e.getName())
                              .parent(convert(e.getParent()))
                              .logLevel(e.getLogLevel())
                              .logHeaderPattern(e.getLogHeaderPattern())
                              .logHeaderPatternEnabled(e.getLogHeaderPatternEnabled())
                              .maintenanceWindow(new MaintenanceWindow(e.getMaintenanceWindow(), Clock.systemDefaultZone()))
                              .type(e.getType())
                              .eventSourceType( eventSourceTypeToDomainConverter.convert(e.getEventSourceTypeEntity()) )
                              .identifiers(e.getIdentifiers()).build();
	}
	
}
