package org.reaction.engine.persistence.service.impl;

import org.reaction.common.contants.EventSourceHierarchyTypeEnum;
import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.domain.EventSource;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.domain.MaintenanceWindow;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.converter.EventSourceToDomainConverter;
import org.reaction.engine.persistence.converter.EventSourceTypeToEntityConverter;
import org.reaction.engine.persistence.model.EventSourceEntity;
import org.reaction.engine.persistence.model.EventSourceTypeEntity;
import org.reaction.engine.persistence.repository.EventSourceEntityRepository;
import org.reaction.engine.persistence.service.EventSourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RepositoryEventSourceService implements EventSourceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryEventSourceService.class);
	
	
	@Autowired
	private EventSourceEntityRepository eventSourceEntityRepository;
	@Autowired
	private EventSourceToDomainConverter<EventSourceEntity, EventSource> eventSourceToDomainConverter;
	@Autowired
	private EventSourceTypeToEntityConverter<EventSourceType, EventSourceTypeEntity> eventSourceTypeToEntityConverter;
	
	
	@Override
	@Transactional
	public List<EventSource> getEventSourceByHost(String host) {
		LOGGER.debug("getEventSourceByHost");
		// searching for the EventSource records in the DB by host
		List<EventSource> eventSources = eventSourceToDomainConverter.convertToList( eventSourceEntityRepository.findByHost(host) );
		LOGGER.trace("The following event sources were found: \n{}", eventSources.stream().map(EventSource::toString).collect(Collectors.joining("\n")));
		// there is a hierarchy in EventSource => the ITEM EventSource DOs have to be sent back but it is possible that 2 or more ITEM EventSources inherit
		//      the host (or other properties) value from a GROUP or top level GROUP EventSource
		// 'Melius (GROUP)'  ->  'Melius / soa_server (GROUP)'  ->  'Melius / soa_server1 (ITEM)'
		//                                                      ->  'Melius / soa_server2 (ITEM)'
		// the host is set on 'Melius / soa_server (GROUP)'
		List<EventSource> logFiles = eventSources.stream().filter(s -> s.getType().equals(EventSourceHierarchyTypeEnum.ITEM)).collect(Collectors.toList());
		// fill the null values from the parent (or from the parent of their parent, etc.)
		fillIfNull(logFiles);
		LOGGER.trace("The following event source are ITEMs: \n{}", logFiles.stream().map(EventSource::toString).collect(Collectors.joining("\n")));
		logFiles.addAll(
				dig( eventSources.stream().filter(s -> !s.getType().equals(EventSourceHierarchyTypeEnum.ITEM)).collect(Collectors.toList()) )
		);
		return logFiles;
	}
	
	
	@Override
	@Transactional
	public List<EventSource> getEventSourceByEventSourceType(EventSourceType eventSourceType) {
		// searching for the event source records in the DB by eventSourceType
		List<EventSource> eventSources = eventSourceToDomainConverter.convertToList( eventSourceEntityRepository.findByEventSourceTypeEntity(eventSourceTypeToEntityConverter.convert(eventSourceType)) );
		LOGGER.trace("The following event sources were found which had the event source type [{}]: \n{}", eventSourceType.getName(), eventSources.stream().map(EventSource::toString).collect(Collectors.joining("\n")));
		// extraction the lowest level event sources
		List<EventSource> itemEventSources = eventSources.stream().filter(s -> s.getType().equals(EventSourceHierarchyTypeEnum.ITEM))
				                                                  .collect(Collectors.toList());
		// fill the null values from the parent (or from the parent of their parent, etc.)
		fillIfNull(itemEventSources);
		LOGGER.trace("The following event sources are ITEMs: \n{}", itemEventSources.stream().map(EventSource::toString).collect(Collectors.joining("\n")));
		itemEventSources.addAll(
				dig( eventSources.stream().filter(s -> !s.getType().equals(EventSourceHierarchyTypeEnum.ITEM))
						                  .collect(Collectors.toList()) )
		);
		LOGGER.trace("The log file event sources before filtering are {}.", itemEventSources);
		LOGGER.trace("The filter eventSourceType is {}.", eventSourceType);
		eventSources = itemEventSources.stream().filter(s -> s.getEventSourceType().getCode().equals(eventSourceType.getCode())).collect(Collectors.toList());
		eventSources = new ArrayList<>(new HashSet<>(eventSources));
		LOGGER.debug("The found event sources are {}.", eventSources);
		return eventSources;
	}

	
	@Override
	@Transactional
	public EventSourceEntity getEventSourceEntity(Long eventSourceId) {
		return eventSourceEntityRepository.getOne(eventSourceId);
	}
	
	
	@Override
	@Transactional
	public EventSource getEventSource(Long eventSourceId) {
		return eventSourceToDomainConverter.convert(eventSourceEntityRepository.getOne(eventSourceId));
	}

	
	@Override
	@Transactional
	public EventSource getEventSourceAndFillProperties(Long eventSourceId) {
		EventSource eventSource = eventSourceToDomainConverter.convert(eventSourceEntityRepository.getOne(eventSourceId));
		if (eventSource == null) {
			return null;
		} else {
			fillIfNull(eventSource);
			return eventSource;
		}
	}


	private List<EventSource> dig(List<EventSource> notItemEventSource) {
		LOGGER.trace("The method 'dig' is called for the following NOT item event sources: \n{}", notItemEventSource.stream().map(EventSource::toString).collect(Collectors.joining("\n")));
		List<EventSource> children = new ArrayList<>();
		if (notItemEventSource != null && notItemEventSource.size() > 0) {
			// the notItemEventSource list contains not ITEM eventSources only so find all their children and put them in the children list
			// the children have to inherit properties from their parent if the property is null
			for (EventSource s : notItemEventSource) {
				List<EventSource> childrenEventSources = eventSourceToDomainConverter.convertToList(eventSourceEntityRepository.findByParent( EventSourceEntity.builder().id(s.getId()).build() ));
				children.addAll(childrenEventSources);
			}
			LOGGER.trace("The event source list contains not ITEM event sources only so find all their children and put them in the children list, the children are\n{}",
					children.stream().map(EventSource::toString).collect(Collectors.joining("\n")));
			// if a child is a ITEM then the dig(...) method doesn't have to be be called for it but only put them to the itemEventSources list to be retrieved
			List<EventSource> itemEventSources = children.stream().filter(s -> s.getType().equals(EventSourceHierarchyTypeEnum.ITEM)).collect(Collectors.toList());
			// fill the null values from the parent (or from the parent of their parent, etc.)
			fillIfNull(itemEventSources);
			// call the dig(...) method for the not ITEM eventSources and and the result to the logFiles list
			itemEventSources.addAll(
					dig( children.stream().filter(s -> !s.getType().equals(EventSourceHierarchyTypeEnum.ITEM)).collect(Collectors.toList()) )
			);
			LOGGER.trace("The method 'dig' returns the following list:\n{}", itemEventSources.stream().map(EventSource::toString).collect(Collectors.joining(", ")));
			return itemEventSources;
		} else {
			LOGGER.trace("The method 'dig' returns an empty list.");
			return new ArrayList<>();
		}
	}


	// the children eventSources have to inherit properties from their parent (or from the parent of their parent, etc.) if one of their properties is null
	private void fillIfNull(List<EventSource> eventSources) {
		eventSources.stream().forEach(eventSource -> fillIfNull(eventSource) );
	}


	private void fillIfNull(EventSource eventSource) {
		LOGGER.trace("fillIfNull - eventSource:\n{}", eventSource);
		boolean isEventSourceTypeNull = false;
		if (eventSource.getEventSourceType() == null) {
			eventSource.setEventSourceType(getEventSourceTypeFromParentEventSource(eventSource.getParent()));
			isEventSourceTypeNull = true;
		}
		if (eventSource.getEventSourceType().getCode().equals("REACTION-WORKER")) {
			Boolean logHeaderPatternEnabled = eventSource.getLogHeaderPatternEnabled();
			if (eventSource.getHost() == null || eventSource.getHost().length() == 0) {
				eventSource.setHost(getHostFromParentEventSource(eventSource.getParent()));
			}
			if (eventSource.getLogHeaderPatternEnabled() == null) {
				logHeaderPatternEnabled = getLogHeaderPatternEnabledFromParentEventSource(eventSource.getParent());
				eventSource.setLogHeaderPatternEnabled(logHeaderPatternEnabled);
			}
			if (eventSource.getLogHeaderPattern() == null || eventSource.getLogHeaderPattern().length() == 0) {
				String logHeaderPattern = getLogHeaderPatternFromParentEventSource(eventSource.getParent());
				// only throw exception if the logHeaderPatternEnabled is true but the logHeaderPattern is null
				if (logHeaderPatternEnabled && logHeaderPattern == null) {
					throw new ReactionRuntimeException("The LogHeaderPattern is enabled and LogHeaderPattern value is not defined!");
				}
				eventSource.setLogHeaderPattern(logHeaderPattern);
			}
			if (eventSource.getLogLevel() == null) {
				LogLevelEnum logLevel = getLogLevelFromParentEventSource(eventSource.getParent());
				// only throw exception if the logHeaderPatternEnabled is true but the logLevel is null
				if (logHeaderPatternEnabled && logLevel == null) {
					throw new ReactionRuntimeException("The LogHeaderPattern is enabled and logLevel value is not defined!");
				}
				eventSource.setLogLevel(logLevel);
			}
			if (eventSource.getLogPath() == null || eventSource.getLogPath().length() == 0) {
				eventSource.setLogPath(getLogPathFromParentEventSource(eventSource.getParent()));
			}
		} else {
			if (isEventSourceTypeNull && (StringUtils.isEmpty(eventSource.getIdentifiers()) || "[]".equals(eventSource.getIdentifiers().replace(" ", "")))) {
				eventSource.setIdentifiers(getIdentifiersFromParentEventSource(eventSource.getParent()));
			}
		}
		if (eventSource.getMaintenanceWindow().isEmpty()) {
			eventSource.setMaintenanceWindow(getMaintenanceWindowFromParentEventSource(eventSource.getParent()));
		}
	}
	
	
	private String getIdentifiersFromParentEventSource(EventSource parent) {
		if (parent == null) {
			throw new ReactionRuntimeException("The identifiers is not defined!");
		}
		return StringUtils.isEmpty(parent.getIdentifiers()) || "{}".equals(parent.getIdentifiers().replace(" ", "")) ? getIdentifiersFromParentEventSource(parent.getParent()) : parent.getIdentifiers();
	}


	private EventSourceType getEventSourceTypeFromParentEventSource(EventSource parent) {
		if (parent == null) {
			throw new ReactionRuntimeException("The event source type is not defined!");
		}
		return parent.getEventSourceType() == null ? getEventSourceTypeFromParentEventSource(parent.getParent()) : parent.getEventSourceType();
	}


	private Boolean getLogHeaderPatternEnabledFromParentEventSource(EventSource parent) {
		if (parent == null) {
			throw new ReactionRuntimeException("The log header pattern enabled value is not defined!");
		}
		return parent.getLogHeaderPatternEnabled() == null ? getLogHeaderPatternEnabledFromParentEventSource(parent.getParent()) : parent.getLogHeaderPatternEnabled();
	}

	private String getHostFromParentEventSource(EventSource parent) {
		if (parent == null) {
			throw new ReactionRuntimeException("The host value is not defined!");
		}
		return parent.getHost() == null ? getHostFromParentEventSource(parent.getParent()) : parent.getHost();
	}

	private String getLogHeaderPatternFromParentEventSource(EventSource parent) {
		if (parent == null) {
			return null;
		}
		return parent.getLogHeaderPattern() == null ? getLogHeaderPatternFromParentEventSource(parent.getParent()) : parent.getLogHeaderPattern();
	}

	private LogLevelEnum getLogLevelFromParentEventSource(EventSource parent) {
		if (parent == null) {
			return null;
		}
		return parent.getLogLevel() == null ? getLogLevelFromParentEventSource(parent.getParent()) : parent.getLogLevel();
	}

	private String getLogPathFromParentEventSource(EventSource parent) {
		if (parent == null) {
			throw new ReactionRuntimeException("The LogPath value is not defined!");
		}
		return parent.getLogPath() == null ? getLogPathFromParentEventSource(parent.getParent()) : parent.getLogPath();
	}

	private MaintenanceWindow getMaintenanceWindowFromParentEventSource(EventSource parent) {
		if (parent == null) {
			return null;
		}
		return parent.getMaintenanceWindow().isEmpty() ? getMaintenanceWindowFromParentEventSource(parent.getParent()) : parent.getMaintenanceWindow();
	}

}