package org.reaction.engine.persistence.service;

import java.util.List;
import java.util.Map;

import org.reaction.common.domain.ErrorDetector;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.exception.ReactionConfigurationException;
import org.reaction.common.exception.ReactionExecutionFlowException;

public interface ErrorDetectorService {

	ErrorDetector search(final Event event,
						 final boolean isFromWorker,
						 final Map<String, String> payloadValues,
						 final Map<String, String> queryParameters,
						 final EventSourceType eventSourceType) throws ReactionConfigurationException;
	
	List<ErrorDetector> getErrorDetectorsByEventSourcedIds(List<Long> ids);

	ErrorDetector getErrorDetectorById(Long id);
	
}
