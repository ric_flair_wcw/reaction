package org.reaction.engine.persistence.repository;


import java.util.List;

import javax.persistence.LockModeType;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface EventEntityRepository extends JpaRepository<EventEntity, Long> {
	
	
	@Query(value = "SELECT ee " +
	               "FROM EventEntity ee " +
			       "WHERE ee.status in (:statuses) AND " +
	               "ee.startDate in " +
			       "(SELECT MAX(ee.startDate) " +
                   "FROM EventEntity ee " +
			       "WHERE ee.executionFlowEntity = :executionFlowEntity AND " +
                   "ee.status in (:statuses)) " +
                   "ORDER BY ee.id")
	@Lock(LockModeType.PESSIMISTIC_WRITE)
//	@QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="10000")})
	public List<EventEntity> findLatestEventEntitiesByExecutionFlowWithLock(@Param("executionFlowEntity") ExecutionFlowEntity executionFlowEntity, 
			                                                                @Param("statuses")List<EventStatusEnum> statuses);
	
	@Query(value = "SELECT ee " +
                   "FROM EventEntity ee " +
		           "WHERE ee.status in (:statuses) AND " +
                   "ee.startDate in " +
		           "(SELECT MAX(ee.startDate) " +
                   "FROM EventEntity ee " +
     		       "WHERE ee.executionFlowEntity = :executionFlowEntity AND " +
                   "ee.status in (:statuses)) " +
                   "ORDER BY ee.id")
	public List<EventEntity> findLatestEventEntitiesByExecutionFlowWithoutLock(@Param("executionFlowEntity") ExecutionFlowEntity executionFlowEntity, 
		                                                                       @Param("statuses")List<EventStatusEnum> statuses);

	
	@Query(value = "SELECT ee " +
            	   "FROM EventEntity ee " +
			       "WHERE ee.status in ('SCHEDULED') AND " +
                   "ee.startDate in " +
		           "(SELECT MIN(ee.startDate) " +
                   "FROM EventEntity ee " +
		           "WHERE ee.executionFlowEntity = :executionFlowEntity AND " +
                   "ee.status in ('SCHEDULED')) " +
                   "ORDER BY ee.id")
	@Lock(LockModeType.PESSIMISTIC_WRITE)
//	@QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value ="10000")})
	public List<EventEntity> findClosestScheduledEventEntitiesByExecutionFlow(@Param("executionFlowEntity") ExecutionFlowEntity executionFlowEntity);

	
	@Query(value = "SELECT ee " +
                   "FROM EventEntity ee " +
                   "WHERE ee.status = 'WAITING_FOR_OTHERS' AND " +
                   "ee.firstEventArrived in " +
                   "(SELECT MAX(ee.firstEventArrived) " +
                   "FROM EventEntity ee LEFT JOIN ee.errorDetectorEntity ese " +
                   "WHERE ese.executionFlowEntity = :executionFlowEntity AND " +
                   "ee.status = 'WAITING_FOR_OTHERS' AND " +
                   "ee.firstEventArrived is not null) " +
                   "ORDER BY ee.id")
	public List<EventEntity> findLatestWaitingEventEntityByExecutionFlow(@Param("executionFlowEntity") ExecutionFlowEntity executionFlowEntity);

	
	@Query(value =  "SELECT e.* " +
		            "FROM event e " +
		            "WHERE e.id = :id AND " +
		            "e.status = 'SCHEDULED' " +
		            "FOR UPDATE",
           nativeQuery = true)
	public List<EventEntity> getScheduledEventNotBeingExecuted(@Param("id") Long id);


	@Query(value =  "SELECT e.* " +
		            "FROM event e " +
		            "WHERE e.status = 'SCHEDULED' " +
		            "FOR UPDATE",
           nativeQuery = true)
	public List<EventEntity> getScheduledEventsNotBeingExecuted();
	
	
	public EventEntity findByIdentifier(String identifier);

}
