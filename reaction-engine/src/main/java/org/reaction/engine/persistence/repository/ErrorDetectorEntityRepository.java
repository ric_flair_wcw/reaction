package org.reaction.engine.persistence.repository;

import java.util.List;

import org.reaction.engine.persistence.model.ErrorDetectorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ErrorDetectorEntityRepository extends JpaRepository<ErrorDetectorEntity, Long> {

	// TODO
	//@Cacheable(cacheNames = "errorDetector", key = "#eventSourceId")
	// TODO refreshing the cache when the DB is modified
	@Query(value = "SELECT ese " +
                   "FROM ErrorDetectorEntity ese LEFT JOIN ese.eventSourceEntity se " +
                   "WHERE ese.activated = true AND se.id in :eventSourceIds")
	public List<ErrorDetectorEntity> findByEventSourceIds(@Param("eventSourceIds") List<Long> eventSourceIds);
	
	
}
