package org.reaction.engine.persistence.service;

import java.util.List;

import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.SuperCommand;
import org.reaction.common.domain.task.SuperTask;

public interface TaskService {

	public List<SuperCommand> getNextTask(Long primaryTaskId, Long secondaryTaskId, Long executionFlowId, Integer order);
	
	public SuperCommand getCommandById(Long taskId);
	
	public List<SuperCommand> getByPrimaryTaskOrderBy(Long taskId);
	
	public List<SuperCommand> getBySecondaryTaskOrderBy(Long taskId);
	
	public SuperCommand getTopByExecutionFlowEntityOrderByOrderAsc(ExecutionFlow executionFlow);

	public SuperCommand getNextTaskRecursively(SuperTask _task);
	
}
