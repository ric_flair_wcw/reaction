package org.reaction.engine.persistence.converter;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface SuperConverter<A, B> extends Function<A, B> {
	
	default B convert(final A a) {
		return apply(a);
	}

	default List<B> convertToList(final List<A> as) {
		if (as == null) {
			return null;
		} else {
			return as.stream().map(this::apply).collect(Collectors.toList());
		}
	}

}
