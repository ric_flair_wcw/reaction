package org.reaction.engine.persistence.service;

import org.reaction.common.domain.EventLife;

public interface WorkerStatusService {

	void refreshReaderReportEvent(Long eventSourceId);

	void refreshExecutorCheckCommands(String host);

	void refreshExecutorSendCommandResult(EventLife eventLife);

	void refreshReaderRefreshEventSources(String host);

}
