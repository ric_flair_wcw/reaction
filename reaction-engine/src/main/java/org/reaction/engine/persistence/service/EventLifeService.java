package org.reaction.engine.persistence.service;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;

public interface EventLifeService {

	EventLife getEventLife(Long eventLifeId);
	
	EventLife save(EventLife eventLife);

	EventLife getEventLifeByOrderAndEvent(Integer order, Event event);

	int getCountByEventAndEventStatus(Event latestEvent, EventStatusEnum eventStatus);

	Integer getMaximumLoopIndexByEventEntityAndTaskEntity_overflow(Long eventId, Long taskId);

}
