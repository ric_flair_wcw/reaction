package org.reaction.engine.persistence.service.impl;

import javax.transaction.Transactional;

import org.reaction.common.domain.EventSourceType;
import org.reaction.engine.persistence.converter.EventSourceTypeToDomainConverter;
import org.reaction.engine.persistence.model.EventSourceTypeEntity;
import org.reaction.engine.persistence.repository.EventSourceTypeEntityRepository;
import org.reaction.engine.persistence.service.EventSourceTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryEventSourceTypeService implements EventSourceTypeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryEventSourceTypeService.class);
	
	
	@Autowired
	private EventSourceTypeEntityRepository eventSourceTypeEntityRepository;
	@Autowired
	private EventSourceTypeToDomainConverter<EventSourceTypeEntity, EventSourceType> converter;
	
	
	@Override
	@Transactional
	public EventSourceType getEventSourceTypeByCode(String code) {
		return converter.convert(eventSourceTypeEntityRepository.findByCode(code));
	}
	
}