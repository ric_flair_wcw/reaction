package org.reaction.engine.persistence.repository;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EventLifeEntityRepository extends JpaRepository<EventLifeEntity, Long> {
	
//	@Query("SELECT ele.result " +
//			"FROM EventLifeEntity ele " +
//			"WHERE ele.order = :order AND " +
//			" ele.taskEntity.id")
//	public String findResult();
	
	EventLifeEntity findByOrderAndEventEntity(Integer order, EventEntity eventEntity);
	
	int countByEventEntityAndEventStatus(EventEntity eventEntity, EventStatusEnum eventStatus);

	@Query("SELECT ele FROM EventLifeEntity ele WHERE ele.eventEntity.id = :eventEntityId AND ele.taskEntity.internalTask = 'LOOP' AND ele.loopIndex IS NOT NULL ORDER BY ele.order DESC")
	List<EventLifeEntity> getMaximumLoopIndexByEventEntity(@Param("eventEntityId") Long eventEntityId);

}
