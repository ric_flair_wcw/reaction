package org.reaction.engine.persistence.repository;

import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExecutionFlowEntityRepository extends JpaRepository<ExecutionFlowEntity, Long> {
	
}
