package org.reaction.engine.persistence.service.impl;

import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.EventSource;
import org.reaction.common.domain.task.CommandByWorkerTask;
import org.reaction.engine.persistence.model.WorkerStatusEntity;
import org.reaction.engine.persistence.repository.WorkerStatusEntityRepository;
import org.reaction.engine.persistence.service.EventSourceService;
import org.reaction.engine.persistence.service.WorkerStatusService;
import org.reaction.engine.tasks.CommandByWorkerTaskCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;

@Service
public class RepositoryWorkerStatusService implements WorkerStatusService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryWorkerStatusService.class);
	
	@Autowired
	private EventSourceService eventSourceService;
	@Autowired
	private WorkerStatusEntityRepository workerStatusEntityRepository; 

	
	@Transactional
	@Override
	public void refreshReaderReportEvent(Long eventSourceId) {
		try {
			// getting the host (recursive call is needed as the host name might be on its parent)
			String hostName = getHostName(eventSourceService.getEventSource(eventSourceId));
			// insert / update the WorkerStatusEntity
			WorkerStatusEntity entity = workerStatusEntityRepository.findByHost(hostName);
			if (entity == null) {
				entity = WorkerStatusEntity.builder().reportEvent(Calendar.getInstance())
						                             .host(hostName).build();
			} else {
				entity.setReportEvent(Calendar.getInstance());
			}
			workerStatusEntityRepository.save(entity);
		} catch(Exception e) {
			LOGGER.error("Error occurred while refreshing the worker status!", e);
		}
	}

	
	@Transactional
	@Override
	public void refreshExecutorCheckCommands(String host) {
		try {
			// insert / update the WorkerStatusEntity
			WorkerStatusEntity entity = workerStatusEntityRepository.findByHost(host);
			if (entity == null) {
				entity = WorkerStatusEntity.builder().checkCommands(Calendar.getInstance())
						                             .host(host).build();
			} else {
				entity.setCheckCommands(Calendar.getInstance());
			}
			workerStatusEntityRepository.save(entity);
		} catch(Exception e) {
			LOGGER.error("Error occurred while refreshing the worker status!", e);
		}
	}

	
	@Transactional
	@Override
	public void refreshExecutorSendCommandResult(EventLife eventLife) {
		try {
			// getting the host name
			String hostName = ( (CommandByWorkerTask)((CommandByWorkerTaskCommand)eventLife.getTask()).getSuperTask()).getHost();
			// insert / update the WorkerStatusEntity
			WorkerStatusEntity entity = workerStatusEntityRepository.findByHost(hostName);
			if (entity == null) {
				entity = WorkerStatusEntity.builder().sendCommandResult(Calendar.getInstance())
						                             .host(hostName).build();
			} else {
				entity.setSendCommandResult(Calendar.getInstance());
			}
			workerStatusEntityRepository.save(entity);
		} catch(Exception e) {
			LOGGER.error("Error occurred while refreshing the worker status!", e);
		}
	}

	
	@Transactional
	@Override
	public void refreshReaderRefreshEventSources(String host) {
		try {
			// insert / update the WorkerStatusEntity
			WorkerStatusEntity entity = workerStatusEntityRepository.findByHost(host);
			if (entity == null) {
				entity = WorkerStatusEntity.builder().refreshEventSource(Calendar.getInstance())
						                             .host(host).build();
			} else {
				entity.setRefreshEventSource(Calendar.getInstance());
			}
			workerStatusEntityRepository.save(entity);
		} catch(Exception e) {
			LOGGER.error("Error occurred while refreshing the worker status!", e);
		}
	}


	private String getHostName(EventSource eventSource) {
		return eventSource.getHost() != null ? eventSource.getHost() : getHostName(eventSource.getParent());
	}
}
