package org.reaction.engine.persistence.service;

import java.util.List;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.ExecutionFlow;

public interface EventService {

	public Event save(Event event);
	
	public Event getLatestEventsByExecutionFlow(ExecutionFlow executionFlow, List<EventStatusEnum> statuses, boolean withLock);
	
	public Event getLatestWaitingEventByExecutionFlow(ExecutionFlow executionFlow);
	
	public Event getEvent(Long id);
	
	public boolean isScheduledEventExecuted(Event e);
	
	public List<Event> getScheduledEventsNotBeingExecuted();

	public Event getEventByIdentifier(String identifier);

	public Event getClosestScheduledEventsByExecutionFlow(ExecutionFlow executionFlow);
	
}
