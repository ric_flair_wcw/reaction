package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.ErrorDetector;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.engine.persistence.model.ErrorDetectorEntity;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.ScheduledExecutionFlowEntity;
import org.springframework.beans.factory.annotation.Autowired;

public class EventToDomainConverter<A, B> implements SuperConverter<EventEntity, Event> {


    @Autowired
    private ErrorDetectorToDomainConverter<ErrorDetectorEntity, ErrorDetector> errorDetectorToDomainConverter;
    @Autowired
    private ExecutionFlowToDomainConverter<ExecutionFlowEntity, ExecutionFlow> executionFlowToDomainConverter;
    @Autowired
    private ScheduledExecutionFlowToDomainConverter<ScheduledExecutionFlowEntity, ScheduledExecutionFlow> scheduledExecutionFlowToDomainConverter;


    @Override
    public Event apply(EventEntity e) {
        if (e == null) {
            return null;
        }
        Event d = Event.builder()
                       .errorDetector(errorDetectorToDomainConverter.convert(e.getErrorDetectorEntity()))
                       .eventSourceId(e.getEventSourceEntity() == null ? null : e.getEventSourceEntity().getId())
                       .id(e.getId())
				       .identifier(e.getIdentifier())
                       .message(e.getMessage())
                       .logLevel(e.getLogLevel())
                       .initiatedBy(e.getInitiatedBy())
                       .status(e.getStatus())
                       .startDate(e.getStartDate())
                       .endDate(e.getEndDate())
                       .firstEventArrived(e.getFirstEventArrived())
                       .multipleEventsCounter(e.getMultipleEventsCounter())
                       .reason(e.getReason())
                       .payloadValues(e.getPayloadValues())
                       .queryParameters(e.getQueryParameters())
                       .executionFlow(executionFlowToDomainConverter.convert(e.getExecutionFlowEntity()))
                       .scheduledExecutionFlow(scheduledExecutionFlowToDomainConverter.convert(e.getScheduledExecutionFlowEntity()))
                       .message(e.getMessage()).build();
        return d;
    }


}
