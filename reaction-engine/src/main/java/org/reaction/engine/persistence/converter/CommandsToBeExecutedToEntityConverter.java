package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.CommandsToBeExecuted;
import org.reaction.common.domain.EventLife;
import org.reaction.engine.persistence.model.CommandsToBeExecutedEntity;
import org.reaction.engine.persistence.model.EventLifeEntity;
import org.springframework.beans.factory.annotation.Autowired;

public class CommandsToBeExecutedToEntityConverter<A, B> implements SuperConverter<CommandsToBeExecuted, CommandsToBeExecutedEntity> {

	
	@Autowired
	private EventLifeToEntityConverter<EventLife, EventLifeEntity> eventLifeToEntityConverter;
	
	
	@Override
	public CommandsToBeExecutedEntity apply(CommandsToBeExecuted d) {
		if (d == null) {
			return null;
		}
		CommandsToBeExecutedEntity e = CommandsToBeExecutedEntity.builder()
																 .id(d.getId())
																 .executed(d.getExecuted())
																 .eventLifeEntity( eventLifeToEntityConverter.convert(d.getEventLife()) ).build();
		return e;
	}
	

}
