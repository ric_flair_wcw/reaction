package org.reaction.engine.persistence.service;

import org.reaction.common.domain.ScheduledExecutionFlow;

public interface ScheduledExecutionFlowService {

	public ScheduledExecutionFlow save(ScheduledExecutionFlow scheduledExecutionFlow);
	
	public ScheduledExecutionFlow getScheduledExecutionFlow(Long scheduledExecutionFlowId);
	
}
