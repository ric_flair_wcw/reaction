package org.reaction.engine.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.reaction.common.contants.ExecutionFlowStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "EXECUTION_FLOW")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ExecutionFlowEntity {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_ExecutionFlowEntity")
	@SequenceGenerator(name="id_generator_ExecutionFlowEntity", sequenceName = "EXECUTION_FLOW_SQ")
	private Long id;

	@Column(name = "NAME")
	private String name;
	
	// the time to wait until a new executionFlow can be started
	// e.g. the same exception arrives 10 times in one minute -> should the executionFlow be started 10 times...? NO but ignore the exceptions until the executionFlow finished and the TIME_BETWEEN_EXEC passed
	// in seconds
	@Column(name = "TIME_BETWEEN_EXEC")
	private Integer timeBetweenExecutions;
	
	// the average execution time of the flow )
	// in minutes
	@Column(name = "EXECUTION_TIME")
	private Integer executionTime;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private ExecutionFlowStatusEnum status;

	@Column(name = "ACCESS_GROUP")
	private String accessGroup;

	@Column(name = "ERROR_MAIL_SENDING_RECIPIENTS")
	private String errorMailSendingRecipients;
	
	@Column(name = "START_MAIL_SENDING_RECIPIENTS")
	private String startMailSendingRecipients;

	@Column(name = "HOSTS")
	private String hosts;
	
}