package org.reaction.engine.persistence.repository;

import org.reaction.engine.persistence.model.CommandsToBeExecutedEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.List;

public interface CommandsToBeExecutedEntityRepository extends JpaRepository<CommandsToBeExecutedEntity, Long> {

	// I had to add the sub SELECT due to H2
	@Query(value = "SELECT DISTINCT c FROM CommandsToBeExecutedEntity c WHERE c in (SELECT ce " +
		           "FROM CommandsToBeExecutedEntity ce LEFT JOIN ce.eventLifeEntity ele LEFT JOIN ele.taskEntity te " +
			       "WHERE ce.executed = false AND " +
		           "(te.host = :host OR (te.host LIKE '%##{%' AND te.host LIKE '%}##%')))")
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	public List<CommandsToBeExecutedEntity> getCommandsToBeExecutedEntityByHost(@Param("host") String host);
	
}
