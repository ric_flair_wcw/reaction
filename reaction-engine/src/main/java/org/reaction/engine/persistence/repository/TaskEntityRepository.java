package org.reaction.engine.persistence.repository;

import java.util.List;

import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TaskEntityRepository extends JpaRepository<TaskEntity, Long> {

	public List<TaskEntity> findByPrimaryTaskEntityOrderByOrderAsc(TaskEntity primaryTaskEntity);
	
	public List<TaskEntity> findBySecondaryTaskEntityOrderByOrderAsc(TaskEntity secondaryTaskEntity);
	
	public TaskEntity findTopByExecutionFlowEntityOrderByOrderAsc(ExecutionFlowEntity executionFlowEntity);
	
	@Query( "SELECT te " +
			"FROM TaskEntity te " +
			"WHERE te.order > :order AND " +
			"((:primaryTaskId IS NOT NULL AND te.primaryTaskEntity IS NOT NULL AND :primaryTaskId = te.primaryTaskEntity.id) OR " +
			"(:secondaryTaskId IS NOT NULL AND te.secondaryTaskEntity IS NOT NULL AND :secondaryTaskId = te.secondaryTaskEntity.id) OR " +
			"(:executionFlowId IS NOT NULL AND te.executionFlowEntity IS NOT NULL AND :executionFlowId = te.executionFlowEntity.id)) " +
			"ORDER BY te.order ASC")
	public List<TaskEntity> findNextTask(@Param("primaryTaskId") Long primaryTaskId, 
										 @Param("secondaryTaskId") Long secondaryTaskId, 
										 @Param("executionFlowId") Long executionFlowId, 
										 @Param("order") Integer order);
	
}
