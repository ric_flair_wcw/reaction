package org.reaction.engine.persistence.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.engine.persistence.converter.EventToDomainConverter;
import org.reaction.engine.persistence.converter.EventToEntityConverter;
import org.reaction.engine.persistence.converter.ExecutionFlowToEntityConverter;
import org.reaction.engine.persistence.model.EventEntity;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.repository.EventEntityRepository;
import org.reaction.engine.persistence.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RepositoryEventService implements EventService {

	
	@Autowired
	private EventEntityRepository eventEntityRepository;
	@Autowired
	private EventToEntityConverter<Event, EventEntity> eventToEntityConverter;
	@Autowired
	private EventToDomainConverter<EventEntity, Event> eventToDomainConverter;
	@Autowired
	private ExecutionFlowToEntityConverter<ExecutionFlowEntity, ExecutionFlow> executionFlowToEntityConverter;
	
	
	@Override
	@Transactional
	public Event save(Event event) {
		EventEntity entity = eventToEntityConverter.convert(event);
		EventEntity savedEntity = eventEntityRepository.save(entity);
		return eventToDomainConverter.convert(savedEntity);
	}


	@Override
	@Transactional
	public Event getLatestEventsByExecutionFlow(ExecutionFlow executionFlow, List<EventStatusEnum> statuses, boolean withLock) {
		ExecutionFlowEntity executionFlowEntity = executionFlowToEntityConverter.convert(executionFlow);
		List<EventEntity> latestEventEntities;
		if (withLock) {
			latestEventEntities = eventEntityRepository.findLatestEventEntitiesByExecutionFlowWithLock(executionFlowEntity, statuses);
		} else {
			latestEventEntities = eventEntityRepository.findLatestEventEntitiesByExecutionFlowWithoutLock(executionFlowEntity, statuses);
		}
		if (latestEventEntities.size() == 0) {
			return null;
		} else {
			return eventToDomainConverter.convert(latestEventEntities.get(0));
		}
	}

	
	@Override
	@Transactional
	public Event getClosestScheduledEventsByExecutionFlow(ExecutionFlow executionFlow) {
		ExecutionFlowEntity executionFlowEntity = executionFlowToEntityConverter.convert(executionFlow);
		List<EventEntity> closestEventEntities = eventEntityRepository.findClosestScheduledEventEntitiesByExecutionFlow(executionFlowEntity);
		if (closestEventEntities.size() == 0) {
			return null;
		} else {
			return eventToDomainConverter.convert(closestEventEntities.get(0));
		}
	}
	

	@Override
	@Transactional
	public Event getLatestWaitingEventByExecutionFlow(ExecutionFlow executionFlow) {
		ExecutionFlowEntity executionFlowEntity = executionFlowToEntityConverter.convert(executionFlow);
		List<EventEntity> latestEventEntities = eventEntityRepository.findLatestWaitingEventEntityByExecutionFlow(executionFlowEntity);
		if (latestEventEntities.size() == 0) {
			return null;
		} else {
			return eventToDomainConverter.convert(latestEventEntities.get(0));
		}
	}


	@Override
	@Transactional
	public Event getEvent(Long id) {
		EventEntity eventEntity = eventEntityRepository.findOne(id);
		return eventToDomainConverter.convert(eventEntity);
	}

	
	@Override
	@Transactional
	public boolean isScheduledEventExecuted(Event e) {
		List<EventEntity> scheduledEventEntities = eventEntityRepository.getScheduledEventNotBeingExecuted(e.getId());
		if (scheduledEventEntities.size() > 1) {
			throw new ReactionRuntimeException("There are more than one record for a scheduled event (id="+e.getId()+") !");
		} else {
			return scheduledEventEntities.size() == 0;
		}
	}


	@Override
	@Transactional
	public List<Event> getScheduledEventsNotBeingExecuted() {
		List<EventEntity> scheduledEventEntities = eventEntityRepository.getScheduledEventsNotBeingExecuted();
		return eventToDomainConverter.convertToList(scheduledEventEntities);
	}
	

	@Override
	@Transactional
	public Event getEventByIdentifier(String identifier) {
		EventEntity eventEntity = eventEntityRepository.findByIdentifier(identifier);
		return eventToDomainConverter.convert(eventEntity);
	}


}