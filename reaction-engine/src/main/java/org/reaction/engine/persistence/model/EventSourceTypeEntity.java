package org.reaction.engine.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.reaction.common.contants.AuthenticationTypeEnum;
import org.reaction.common.contants.DataTypeEnum;

import javax.persistence.*;

@Entity
@Table(name = "EVENT_SOURCE_TYPE")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventSourceTypeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_EventSourceTypeEntity")
	@SequenceGenerator(name="id_generator_EventSourceTypeEntity", sequenceName = "EVENT_SOURCE_TYPE_SQ")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "CODE")
	private String code;

	@Column(name = "DATA_TYPE")
	@Enumerated(EnumType.STRING)
	private DataTypeEnum dataType;

	@Column(name = "NAMESPACES")
	private String namespaces;

	@Column(name = "PATH_TO_IDENTIFIERS", length = 3000)
	private String pathToIdentifiers;

	@Column(name = "ENFORCE_SSL")
	private Boolean enforceSsl;

	@Column(name = "AUTHENTICATION_TYPE")
	@Enumerated(EnumType.STRING)
	private AuthenticationTypeEnum authenticationType;

	@Column(name = "SECRET")
	private String secret;

	@Column(name = "USER")
	private String user;

}
