package org.reaction.engine.persistence.converter;

import org.reaction.common.domain.ExecutionFlow;
import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.engine.persistence.model.ExecutionFlowEntity;
import org.reaction.engine.persistence.model.ScheduledExecutionFlowEntity;
import org.springframework.beans.factory.annotation.Autowired;

public class ScheduledExecutionFlowToDomainConverter<A, B> implements SuperConverter<ScheduledExecutionFlowEntity, ScheduledExecutionFlow> {

	
	@Autowired
	private ExecutionFlowToDomainConverter<ExecutionFlowEntity, ExecutionFlow> executionFlowToDomainConverter;


	@Override
	public ScheduledExecutionFlow apply(ScheduledExecutionFlowEntity e) {
		if (e == null) {
			return null;
		}
		ScheduledExecutionFlow d = ScheduledExecutionFlow.builder()
				 	   					 .id(e.getId())
				 	   					 .name(e.getName())
				 	   					 .cronExpression(e.getCronExpression())
				 	   					 .reason(e.getReason())
				 	   					 .scheduledAlready(e.getScheduledAlready())
				 	   					 .errorAtLastRun(e.getErrorAtLastRun())
				 	   					 .executionFlow(executionFlowToDomainConverter.convert(e.getExecutionFlowEntity())).build();
		return d;
	}


}
