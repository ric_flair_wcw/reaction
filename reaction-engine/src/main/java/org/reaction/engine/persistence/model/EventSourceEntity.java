package org.reaction.engine.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.contants.EventSourceHierarchyTypeEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "EVENT_SOURCE")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventSourceEntity {

	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "id_generator_EventSourceEntity")
	@SequenceGenerator(name="id_generator_EventSourceEntity", sequenceName = "EVENT_SOURCE_SQ")
	private Long id;

	@Column(name = "NAME")
	private String name;
	
	@Column(name = "HOST")
	private String host;

	@Column(name = "LOG_PATH")
	private String logPath;

	// see the end of the class
	// e.g. <[~DATE:dd-MMM-yyyy HH:mm:ss] o'clock BST> <[~LOGLEVEL]> <[~COMPONENT]> <[~ERRORCODE]> <[~MESSAGE]>
	@Column(name = "LOG_HEADER_PATTERN")
	private String logHeaderPattern;

	@Column(name = "LOG_HEADER_PATTERN_ENABLED")
	private Boolean logHeaderPatternEnabled;

	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "TYPE")
	@Enumerated(EnumType.STRING)
	private EventSourceHierarchyTypeEnum type;

	@Column(name = "LOG_LEVEL")
	@Enumerated(EnumType.STRING)
	private LogLevelEnum logLevel;
	
	/*
	 * Storing the maintenance window in JSON
			{ "Sun": "14:00-04:00",
			  "Mon": "23:00-04:00",
			  "Tue": "23:00-04:00",
			  "Wed": "23:00-04:00",
			  "Thu": "23:00-04:00",
			  "Fri": "23:00-04:00",
			  "Sat": "20:00-04:00" }
		In this example the Mon is missing (it is covered at Sun) and Sat has 2 periods
			{ "Sun": "14:00-04:00",
			  "Tue": "01:00-04:00",
			  "Wed": "01:00-04:00",
			  "Thu": "01:00-04:00",
			  "Fri": "01:00-04:00",
			  "Sat": ["01:00-04:00", "20:00-04:00"] }
  	 */
	@Column(name = "MAINTENANCE_WINDOW")
	private String maintenanceWindow;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID")
	private EventSourceEntity parent;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "EVENT_SOURCE_TYPE_ID")
	private EventSourceTypeEntity eventSourceTypeEntity;
	
	@Column(name = "IDENTIFIERS", length = 3000)
	private String identifiers;

}

/*
Melius
<23-Jun-2014 15:10:34 o'clock BST> <Error> <oracle.soa.adapter> <BEA-000000> <JCABinding=>  CaseDataBamUtil:LPCaseWorkflowTasksDO_BamAdapter [ writetoBAM_ptt::write
...
>

<[~DATE:dd-MMM-yyyy HH:mm:ss] o'clock BST> <[~LOGLEVEL]> <[~COMPONENT]> <[~ERRORCODE]> <[~MESSAGE]>






Tomcat
29-Feb-2016 12:32:04.831 SEVERE [main] org.apache.catalina.core.StandardService.initInternal Failed to initialize connector [Connector[HTTP/1.1-8090]]
 org.apache.catalina.LifecycleException: Failed to initialize component [Connector[HTTP/1.1-8090]]
	at org.apache.catalina.util.LifecycleBase.init(LifecycleBase.java:106)
	at org.apache.catalina.core.StandardService.initInternal(StandardService.java:568)
    
[~DATE:dd-MMM-yyyy HH:mm:ss.SSS] [~LOGLEVEL] [[~COMPONENT]] [~CLASS] [~MESSAGE]






Nexus
jvm 1    | 2015-03-25 14:48:22,836+0000 ERROR [qtp1468866485-44] anonymous org.sonatype.nexus.proxy.maven.maven2.M2Repository - Got RemoteStorageException in proxy repository "Central" [id=central] while retrieving remote artifact "ResourceStoreRequest{requestPath='/com/goldmansachs/gs-collections-api/5.1.0/gs-collections-api-5.1.0.jar', requestContext=RequestContext{this=org.sonatype.nexus.proxy.RequestContext@70862aa3, parent=null}, pathStack=[/groups/public/com/goldmansachs/gs-collections-api/5.1.0/gs-collections-api-5.1.0.jar], processedRepositories=[public, releases, snapshots, thirdparty, central], appliedMappings={public=[]}}" from URL http://repo1.maven.org/maven2/, this is 1 (re)try, cause: java.net.SocketTimeoutException: Read timed out

[~UNKNOWN]| [~DATE:yyyy-MM-dd HH:mm:ss,SSSZ] [~LOGLEVEL] [[~COMPONENT]] [~UNKNOWN] [~CLASS] - [~MESSAGE]
*/