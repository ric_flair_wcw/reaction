package org.reaction.engine.persistence.converter;


import org.reaction.common.domain.EventSourceType;
import org.reaction.engine.persistence.model.EventSourceTypeEntity;


public class EventSourceTypeToEntityConverter<A, B> implements SuperConverter<EventSourceType, EventSourceTypeEntity> {


	@Override
	public EventSourceTypeEntity apply(EventSourceType e) {
		if (e == null) {
			return null;
		}
		// setting the basic properties
		return EventSourceTypeEntity.builder().id(e.getId())
											  .name(e.getName())
											  .code(e.getCode())
											  .dataType(e.getDataType())
											  .namespaces(e.getNamespaces())
											  .pathToIdentifiers(e.getPathToIdentifiers())
											  .enforceSsl(e.getEnforceSsl())
											  .authenticationType(e.getAuthenticationType())
											  .user(e.getUser())
											  .secret(e.getSecret())
											  .build();
	}
	
}
