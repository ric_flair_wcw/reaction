package org.reaction.engine.scheduling.impl;

import java.util.Date;

import org.reaction.common.domain.Event;
import org.reaction.engine.scheduling.ScheduledRunnable;
import org.reaction.engine.scheduling.SuperScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.commonj.TimerManagerTaskScheduler;

public class CommonJWorkmanagerScheduler extends SuperScheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommonJWorkmanagerScheduler.class);
	
	@Autowired
    private TimerManagerTaskScheduler timerManagerTaskScheduler;
	
	@Override
	public void schedule(Event event) {
		LOGGER.info("The following event will be scheduled: {}", event);
		ScheduledRunnable scheduledRunnable = getTask();
		scheduledRunnable.setEvent(event);
		scheduledRunnable.setDate(event.getStartDate().getTime());
		timerManagerTaskScheduler.schedule(scheduledRunnable, event.getStartDate().getTime());
    }
	
	@Override
	public void schedule(Runnable task, long period) {
		Date startTime = new Date(System.currentTimeMillis() + period);
		timerManagerTaskScheduler.scheduleAtFixedRate(task, startTime, period);
	}

}
