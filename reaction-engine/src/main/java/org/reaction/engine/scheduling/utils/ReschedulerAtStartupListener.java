package org.reaction.engine.scheduling.utils;

import java.util.List;

import org.reaction.common.domain.Event;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.scheduling.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ReschedulerAtStartupListener {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReschedulerAtStartupListener.class);
	
	
	@Autowired
	private Scheduler scheduler;
	@Autowired
	private EventService eventService;
	
	
    @EventListener({ContextRefreshedEvent.class})
    void contextRefreshedEvent() {
        // rescheduling all the scheduled tasks that haven't started yet
    	// in cluster environment the tasks will be scheduled on all the nodes BUT likely it is not a big problem as the SELECT .. FOR UPDATE query will filter them in ScheduledRunnable
    	
    	// TODO it is possible that a task is scheduled to run at 11PM but the reaction is restarted at 10:59PM and it is up at 11:01PM => the task will be rescheduled in the past...
    	// what to do in this case:
    	//    - send a mail that the task is not executed due to the restart
    	//    - if there is a scheduler behind the scheduled task 
    	
    	List<Event> events = eventService.getScheduledEventsNotBeingExecuted();
		LOGGER.info("{} scheduled event(s) will be rescheduled.", events.size());
    	
    	events.stream().forEach( s -> scheduler.schedule(s) );
    }
    
}
