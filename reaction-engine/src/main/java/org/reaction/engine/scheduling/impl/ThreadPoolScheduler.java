package org.reaction.engine.scheduling.impl;

import java.util.Date;

import org.reaction.common.domain.Event;
import org.reaction.engine.scheduling.ScheduledRunnable;
import org.reaction.engine.scheduling.SuperScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class ThreadPoolScheduler extends SuperScheduler {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThreadPoolScheduler.class);
	
	@Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;
	
	@Override
	public void schedule(Event event) {
		LOGGER.info("The following event will be scheduled: {}", event);
		ScheduledRunnable scheduledRunnable = getTask();
		scheduledRunnable.setEvent(event);
		scheduledRunnable.setDate(event.getStartDate().getTime());
		threadPoolTaskScheduler.schedule(scheduledRunnable, event.getStartDate().getTime());
    }

	@Override
	public void schedule(Runnable task, long period) {
		Date startTime = new Date(System.currentTimeMillis() + period);
		threadPoolTaskScheduler.scheduleAtFixedRate(task, startTime, period);
	}
	
}