package org.reaction.engine.scheduling.impl;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

public class ThreadPoolHandler implements ApplicationListener<ContextClosedEvent> {

	private ThreadPoolTaskScheduler threadPoolTaskScheduler;
	
	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		threadPoolTaskScheduler.getScheduledExecutor().shutdownNow();
		threadPoolTaskScheduler.shutdown();
	}

	public void setThreadPoolTaskScheduler(ThreadPoolTaskScheduler threadPoolTaskScheduler) {
		this.threadPoolTaskScheduler = threadPoolTaskScheduler;
	}

}
