package org.reaction.engine.scheduling;

import org.reaction.common.domain.Event;

public interface Scheduler {

	public void schedule(Event event);
	
	public void schedule(Runnable task, long period);
	
}
