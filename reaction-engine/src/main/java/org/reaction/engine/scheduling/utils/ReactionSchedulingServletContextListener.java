package org.reaction.engine.scheduling.utils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@WebListener
public class ReactionSchedulingServletContextListener implements ServletContextListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReactionSchedulingServletContextListener.class);
	
	@Autowired 
	private ThreadPoolTaskScheduler scheduler;
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		if (scheduler != null) {
			LOGGER.info("I am trying to shutdown the ThreadPoolTaskScheduler. If it takes too much time then you might consider to kill the application / application server process.");
			scheduler.setWaitForTasksToCompleteOnShutdown(true);
			scheduler.getScheduledExecutor().shutdownNow();
			scheduler.shutdown();
			//scheduler.setAwaitTerminationSeconds(awaitTerminationSeconds);
		}
	}

}
