package org.reaction.engine.scheduling.impl;

import java.time.Duration;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;

import org.reaction.common.domain.Event;
import org.reaction.engine.scheduling.ScheduledRunnable;
import org.reaction.engine.scheduling.SuperScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// http://blog.chris-ritchie.com/2013/09/how-to-configure-scheduled-tasks-in-wildfly..html
public class WildflyScheduler extends SuperScheduler {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(WildflyScheduler.class);
	
	@Resource(lookup="java:jboss/ee/concurrency/scheduler/reaction/scheduled-executor-service")
    private ManagedScheduledExecutorService executorService;
 
	
	@Override
	public void schedule(Event event) {
		LOGGER.info("The following event will be scheduled: {}", event);
		ScheduledRunnable scheduledRunnable = getTask();
		scheduledRunnable.setEvent(event);
		scheduledRunnable.setDate(event.getStartDate().getTime());
		long delay = Duration.between(Calendar.getInstance().toInstant(), event.getStartDate().toInstant()).toMillis();
		LOGGER.info("The delay in millisec is {}.", delay);
		executorService.schedule(scheduledRunnable, delay, TimeUnit.MILLISECONDS);
	}
	
	
	@Override
	public void schedule(Runnable task, long period) {
		executorService.scheduleAtFixedRate(task, period, period, TimeUnit.MILLISECONDS);
	}

	
}
