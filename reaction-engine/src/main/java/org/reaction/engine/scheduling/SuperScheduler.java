package org.reaction.engine.scheduling;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public abstract class SuperScheduler implements Scheduler {

	@Autowired
	private ApplicationContext context;
	
	public ScheduledRunnable getTask() {
		return (ScheduledRunnable) context.getBean("scheduledRunnable");
	}

}
