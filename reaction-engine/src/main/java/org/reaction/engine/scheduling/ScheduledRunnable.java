package org.reaction.engine.scheduling;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.domain.Event;
import org.reaction.common.domain.EventLife;
import org.reaction.common.domain.ScheduledExecutionFlow;
import org.reaction.engine.errors.ErrorHandler;
import org.reaction.engine.persistence.service.EventLifeService;
import org.reaction.engine.persistence.service.EventService;
import org.reaction.engine.persistence.service.ScheduledExecutionFlowService;
import org.reaction.engine.service.ExecutionFlowMachineService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.support.CronSequenceGenerator;

public class ScheduledRunnable implements Runnable {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledRunnable.class);

	Event event = null;
	Date date = null;
	
	@Autowired
	private ExecutionFlowMachineService executionFlowMachineService;
	@Autowired
	private ScheduledExecutionFlowService scheduledExecutionFlowService;
	@Autowired
	private ErrorHandler errorHandler;
	@Autowired
	private EventService eventService;
	@Autowired
	private Scheduler scheduler;
	@Autowired
	private EventLifeService eventLifeService;

	
	@Transactional
	@Override
	public void run() {
		// refreshing the event
		event = eventService.getEvent(event.getId());
		LOGGER.info("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n"+
	            "Starting the scheduled event!"+
	            "\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n{}", event);
		try {
			// check if the scheduled execution flow was descheduled then the scheduled event mustn't be started
			if (event.getScheduledExecutionFlow() != null) {
				ScheduledExecutionFlow currentScheduledExecutionFlow = scheduledExecutionFlowService.getScheduledExecutionFlow(event.getScheduledExecutionFlow().getId());
				if (!currentScheduledExecutionFlow.getScheduledAlready()) {
					LOGGER.warn("The scheduled event (id={}) cannot be started as the scheduled execution flow (name={}, id={}) was stopped meanwhile (Already scheduled = false)! Also the event won't be rescheduled.", 
							event.getId(), currentScheduledExecutionFlow.getName(), currentScheduledExecutionFlow.getId() );
					event.setStatus(EventStatusEnum.IGNORED);
					event.setReason("The event was ignored as the scheduled execution flow (name='"+currentScheduledExecutionFlow.getName()+"') in the scheduler was stopped meanwhile (manually or an error occurred on the previous event).");
					event.setEndDate(Calendar.getInstance());
					eventService.save(event);
					return;
				}
				event.setScheduledExecutionFlow(currentScheduledExecutionFlow);
			}
			
			// using SELECT .. FOR UPDATE to check if this task hasn't been executed yet (important in clustered environment)
			if (eventService.isScheduledEventExecuted(event)) {
				LOGGER.warn("The scheduled event has been already executed (likely by another node in the cluster) or cancelled!\n{}", event);
				return;
			}
			
			// ok, so the followings have to be done:
			//   1. create a next scheduledEvent (and event) and schedule it with the next date
			//   2. start the current scheduledEvent (event)
			
			// there will be a next event if it comes from the scheduler and not scheduled by the maintenance window
			if (event.getInitiatedBy().equals( EventInitiatorEnum.BY_SCHEDULER )) {
				// A new event and scheduledEvent record has to be inserted to the ScheduledTask with the next date
				//       calculating the future date when the execution flow will run
				String cronExpression = event.getScheduledExecutionFlow().getCronExpression();
				LOGGER.trace("The cron to be used for scheduling the next event is {}", cronExpression);
				CronSequenceGenerator cronSequenceGenerator = new CronSequenceGenerator(cronExpression);
				Calendar next = getNextDate(cronSequenceGenerator);
				LOGGER.trace("The next Calendar is {}", Constants.getDateFormat().format(next.getTime()));
				//       event
				Event nextEvent = Event.builder()
									   .identifier(UUID.randomUUID().toString())
									   .initiatedBy(EventInitiatorEnum.BY_SCHEDULER)
									   .status(EventStatusEnum.SCHEDULED)
									   .startDate(next)
									   .executionFlow(event.getExecutionFlow())
									   .scheduledExecutionFlow(event.getScheduledExecutionFlow()).build();
				Event savedNextEvent = eventService.save(nextEvent);
				LOGGER.debug("The following Event is saved for scheduling the next event: \n{}", savedNextEvent);
				EventLife eventLife = EventLife.builder()
	                                           .eventDate(Calendar.getInstance())
	                                           .event(event)
	                                           .eventStatus(EventStatusEnum.SCHEDULED)
	                                           .order(-2).build();
				eventLife.addToHistory();
				eventLifeService.save(eventLife);
	
				// schedule the next scheduledEvent only if it has to (i.e. it comes from the scheduled and not scheduled by maintenance window)
				scheduler.schedule(savedNextEvent);
			}
		} catch (Exception e) {
			LOGGER.error("Error occured when starting the scheduled event!", e);
			LOGGER.info("{}", event);
			// handle the error
			errorHandler.handleError(event, e);
			// altering the scheduled execution flow: not scheduled anymore and set the error message
			if (event.getScheduledExecutionFlow() != null) {
				ScheduledExecutionFlow scheduledExecutionFlow = event.getScheduledExecutionFlow();
				LOGGER.warn("Due to an error the scheduled execution flow (scheduled execution flow id={}) will be stopped!", scheduledExecutionFlow.getId());
				scheduledExecutionFlow.setScheduledAlready(false);
				scheduledExecutionFlow.setErrorAtLastRun(e.getMessage());
				scheduledExecutionFlowService.save(scheduledExecutionFlow);
			}
			return;
		}
		
		// start the executionFlow
		try {
			// if the start date is in the past (older than current time - 1 min) then the event mustn't be started
			Date nowMinusOneMinute = new Date(System.currentTimeMillis() - 60 * 1000);
			if (event.getStartDate().getTime().after(nowMinusOneMinute)) {
				executionFlowMachineService.startIt(event);
			} else {
				LOGGER.warn("The event cannot be started as its start date is in the past! It will be cancelled.");
				event.setStatus(EventStatusEnum.CANCELLED);
				event.setReason("The event was cancelled as its start date was in the past when the scheduler wanted to start it.");
				event.setEndDate(Calendar.getInstance());
				eventService.save(event);
				EventLife eventLife = EventLife.builder()
                                               .eventDate(Calendar.getInstance())
                                               .event(event)
                                               .eventStatus(EventStatusEnum.CANCELLED)
                                               .order(-1).build();
				eventLife.addToHistory();
				eventLifeService.save(eventLife);
			}
		} catch (Exception e) {
			LOGGER.error("Error occured when starting the scheduled event!", e);
			LOGGER.info("{}", event);
			errorHandler.handleError(event, e);
		}
	}

	
	// it can happen that the engine was stopped for long and when it is started the cron generator will generate a next date which still in the past...
	private Calendar getNextDate(CronSequenceGenerator cronSequenceGenerator) {
		Date nextDate = cronSequenceGenerator.next(date);
		Date currentDate = new Date();
		while (!nextDate.after(currentDate)) {
			nextDate = cronSequenceGenerator.next(nextDate);
		}
		LOGGER.debug("The nextDate generated by CronSequenceGenerator is {}, the date recorded in ScheduledRunnable is {}", nextDate, date);
		Calendar next = Calendar.getInstance();
		next.setTime(nextDate);
		return next;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

}