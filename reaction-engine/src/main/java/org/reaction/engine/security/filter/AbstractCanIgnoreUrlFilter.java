package org.reaction.engine.security.filter;


import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

public abstract class AbstractCanIgnoreUrlFilter extends OncePerRequestFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCanIgnoreUrlFilter.class);
	
    private List<String> endpointsToBeIgnored;

    
    protected boolean isEndpointNeededToBeIgnored(String servletPath) {
    	for (String patternText : endpointsToBeIgnored) {
    		boolean hasFound = Pattern.compile(patternText).matcher(servletPath).find();
    		LOGGER.trace("{} matches? {}  ->  {}", servletPath, patternText, hasFound);
        	if (hasFound) {
        		return true;
        	}
    	}
		return false;
	}


	public void setEndpointsToBeIgnored(List<String> endpointsToBeIgnored) {
		this.endpointsToBeIgnored = endpointsToBeIgnored;
	}

}