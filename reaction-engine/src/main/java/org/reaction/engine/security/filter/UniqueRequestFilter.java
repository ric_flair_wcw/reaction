package org.reaction.engine.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.reaction.engine.security.AuthHeader;
import org.reaction.engine.security.HmacUtil;
import org.reaction.engine.security.NonceStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StringUtils;

/*
 * The filter will check if the nonce value being presented in the HTTP header is unique based on the nonce list maintained in the server.
 */
public class UniqueRequestFilter extends AbstractCanIgnoreUrlFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(UniqueRequestFilter.class);

	private NonceStorage nonceStorage;
	private Long requestDelay;

	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		try {
			if (!isEndpointNeededToBeIgnored(request.getServletPath())) {
				LOGGER.debug("UniqueRequestFilter is in action.");
				
		        final AuthHeader authHeader = HmacUtil.getAuthHeader(request);
		    	
		        if (authHeader == null) {
		            // invalid authorization token
		        	String message = "Authorization header is missing!";
		            logger.error(message);
		            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
		            return;
		        }
		
		        String nonce = authHeader.getNonce();
		        if (StringUtils.isEmpty(nonce)) {
		            // Nonce value is missing
		        	String message = "Nonce value in authorization header is missing!";
		            logger.error(message);
		            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
		            return;
		        }
		        
		        // checking the HTTP request is not too old
		        Long dateNumber = request.getDateHeader(HttpHeaders.DATE);
		        if (dateNumber == -1) {
		        	String message = "Date cannot be found in the HTTP request header!";
		            logger.warn(message);
		            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
		            return;
		        }
		        if (System.currentTimeMillis() - dateNumber > requestDelay*1000) {
		        	String message = "The HTTP request is too old, it is rejected!";
		            logger.warn(message);
		            response.sendError(HttpServletResponse.SC_FORBIDDEN, message);
		            return;        	
		        }
		        
		        // checking if the nonce is already used (replay attack ?)
		        if (nonceStorage.contains(nonce)) {
		            // the current nonce value is already used
		        	String message = "The current nonce value is already used! Replay attack might be in progress...";
		            logger.error(message);
		            response.sendError(HttpServletResponse.SC_FORBIDDEN, message);
		            return;
		        }
		        
		        nonceStorage.add(nonce);
			}
		    filterChain.doFilter(request, response);
		} catch(Exception e) {
			String message = "Unknown error occured!";
			logger.error(message, e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message + " " + e.getMessage());
			return;
		}
	}

	
	public void setNonceStorage(NonceStorage nonceStorage) {
		this.nonceStorage = nonceStorage;
	}


	public void setRequestDelay(Long requestDelay) {
		this.requestDelay = requestDelay;
	}

}