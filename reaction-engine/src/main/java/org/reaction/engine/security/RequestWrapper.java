package org.reaction.engine.security;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestWrapper extends HttpServletRequestWrapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(RequestWrapper.class);
	
	private byte[] rawData;
	private HttpServletRequest request;
	private ResettableServletInputStream servletStream;

	public RequestWrapper(HttpServletRequest request) {
		super(request);
		this.request = request;
		this.servletStream = new ResettableServletInputStream();
	}

	public void resetInputStream(byte[] newRawData) {
		LOGGER.trace("resetInputStream - {}", Arrays.toString(newRawData));
		servletStream.stream = new ByteArrayInputStream(newRawData);
		rawData = newRawData;
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		if (rawData == null) {
			LOGGER.trace("getInputStream - rawData == null");
			rawData = IOUtils.toByteArray(this.request.getReader(), StandardCharsets.UTF_8);
			LOGGER.trace("getInputStream - rawData: {}", Arrays.toString(rawData));
			servletStream.stream = new ByteArrayInputStream(rawData);
		}
		LOGGER.debug("getInputStream - servletStream: {}", servletStream);
		return servletStream;
	}

	@Override
	public BufferedReader getReader() throws IOException {
		if (rawData == null) {
			LOGGER.trace("getReader - rawData == null");
			rawData = IOUtils.toByteArray(this.request.getReader(), StandardCharsets.UTF_8);
			LOGGER.trace("getReader - rawData: {}", Arrays.toString(rawData));
			servletStream.stream = new ByteArrayInputStream(rawData);
		}
		LOGGER.trace("getInputStream - servletStream: {}", servletStream);
		return new BufferedReader(new InputStreamReader(servletStream));
	}

	public byte[] getRawData() {
		LOGGER.trace("getRawData: {}", Arrays.toString(rawData));
		return this.rawData;
	}
	
	private class ResettableServletInputStream extends ServletInputStream {

		private InputStream stream;

		@Override
		public int read() throws IOException {
			return stream.read();
		}

		@Override
		public boolean isFinished() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isReady() {
			// TODO Auto-generated method stub
			return true;
		}

		@Override
		public void setReadListener(ReadListener listener) {
			// TODO Auto-generated method stub
			
		}
	}
}