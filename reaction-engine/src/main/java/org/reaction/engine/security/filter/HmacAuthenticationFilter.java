package org.reaction.engine.security.filter;


import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Objects;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.reaction.common.security.HmacSignatureBuilder;
import org.reaction.engine.security.AuthHeader;
import org.reaction.engine.security.HmacUtil;
import org.reaction.engine.security.RequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

public class HmacAuthenticationFilter extends AbstractCanIgnoreUrlFilter {

	private static final Logger LOGGER = LoggerFactory.getLogger(HmacAuthenticationFilter.class);
	
    private UserDetailsService userDetailsService;

    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    	if (!isEndpointNeededToBeIgnored(request.getServletPath())) {
    		try {
		    	logger.debug("HmacAuthenticationFilter is in action.");
		        final AuthHeader authHeader = HmacUtil.getAuthHeader(request);
		
		        if (authHeader == null) {
		            // invalid authorization token
		        	String message = "Authorization header is missing!";
		            logger.error(message);
		            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
		            return;
		        }
		
		        // getting the username (api key) from the credentials file and load the userDetails object
		        final String username = authHeader.getApiKey();
		        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		        Objects.requireNonNull(userDetails);
		        logger.debug("username="+username+", userDetails="+userDetails.toString());
		
		        // likely the EncryptionFilter is executed before the HmacAuthenticationFilter so the request is a RequestWrapper -> the RequestWrapper contains the byte array of the request body
		        //      if it isn't then create the RequestWrapper
		        byte[] contentAsByteArray;
		        RequestWrapper requestWrapper;
		        if (!(request instanceof RequestWrapper)) {
					requestWrapper = new RequestWrapper((HttpServletRequest) request);
					contentAsByteArray = IOUtils.toByteArray(requestWrapper.getReader(), StandardCharsets.UTF_8);
		        } else {
		        	requestWrapper = (RequestWrapper)request;
		        	contentAsByteArray = ((RequestWrapper)request).getRawData();
		        }
		        LOGGER.trace("contentAsByteArray: {}", Arrays.toString(contentAsByteArray));
		        // it has to be reset as the position of the ByteArrayInputStream is at the end -> the reset will get to the beginning so the subsequent filters / service can read the content
		        requestWrapper.resetInputStream(contentAsByteArray);	
		
		        // create the signatureBuilder
		        final HmacSignatureBuilder signatureBuilder = new HmacSignatureBuilder()
		                .method(request.getMethod())
		        		.endpoint(request.getRequestURL().toString())
		                .contentType(request.getContentType() == null ? MediaType.APPLICATION_JSON_VALUE : request.getContentType())
		                .date(request.getHeader(HttpHeaders.DATE))
		                .nonce(authHeader.getNonce())
		                .apiKey(username)
		                .apiSecret(userDetails.getPassword())
		                .payload(contentAsByteArray);
		        
		        // comparing the hash values
		        if (!signatureBuilder.isHashEquals(authHeader.getDigest())) {
		            logger.error("HmacAuthenticationFilter.badSignature: The hash values are not matching! The REST client cannot be authenticated. Check the credentials files if the passwords are matching!");
		            response.sendError(HttpServletResponse.SC_FORBIDDEN, "HmacAuthenticationFilter.badSignature: Invalid authorization data");            
		        } 
		        else {
		        	logger.debug("The hash values are matching.");
			        final PreAuthenticatedAuthenticationToken authentication = new PreAuthenticatedAuthenticationToken(
			                userDetails.getUsername(),
			                null,
			                userDetails.getAuthorities());
			        authentication.setDetails(userDetails);
			
			        SecurityContextHolder.getContext().setAuthentication(authentication);
			        try {
			            filterChain.doFilter(requestWrapper, response);
			        } finally {
			            SecurityContextHolder.clearContext();
			        }
		        }
    		} catch(Exception e) {
    			String message = "Unknown error occured!";
    			logger.error(message, e);
    			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message + " " + e.getMessage());
    			return;
    		}
    	}
    	else {
    		filterChain.doFilter(request, response);
    	}
    }

	@Required
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

}