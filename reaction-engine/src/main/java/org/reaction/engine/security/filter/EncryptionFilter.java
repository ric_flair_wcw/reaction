package org.reaction.engine.security.filter;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.io.IOUtils;
import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EncryptionTypeEnum;
import org.reaction.common.exception.ReactionRuntimeSecurityException;
import org.reaction.common.security.CryptoTool;
import org.reaction.engine.security.AuthHeader;
import org.reaction.engine.security.CredentialLoader;
import org.reaction.engine.security.HmacUtil;
import org.reaction.engine.security.RequestWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.filter.OncePerRequestFilter;

public class EncryptionFilter extends OncePerRequestFilter {


	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionFilter.class);
	
    private CryptoTool cryptoTool;
    private CredentialLoader credentialLoader;
    private List<String> endpointsWhereEncryptionIsNeeded;

    
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
    	LOGGER.debug("EncryptionFilter is in action.");
    	
        // check if the request is encrypted (examine the HTTP header)
		// if no SECURITY_SETTING is provided then the request is a suspicious one so use the strictest (CERTIFICATE_BASED) encryption
        EncryptionTypeEnum encryptionType = request.getHeader(Constants.HTTP_HEADER_SECURITY_SETTING) == null ? EncryptionTypeEnum.CERTIFICATE_BASED :
        	EncryptionTypeEnum.findByCode(Integer.valueOf(request.getHeader(Constants.HTTP_HEADER_SECURITY_SETTING)));

        // ---------------------------------------------------------------------------------------------------------------
        // ------------------------ if no encryption is needed then just perform the filter chain ------------------------
        // ---------------------------------------------------------------------------------------------------------------
        if (encryptionType.equals(EncryptionTypeEnum.NONE) || !isEncryptionNeeded(request.getServletPath())) {
        	filterChain.doFilter(request, response);
        }
        
        // ---------------------------------------------------------------------------------------------------------------
        // ----------------------------------------- do the encryption ---------------------------------------------------
        // ---------------------------------------------------------------------------------------------------------------
        else {
        	try {	
		    	final AuthHeader authHeader = HmacUtil.getAuthHeader(request);
		    	if (authHeader == null) {
		        	handleError(response, HttpServletResponse.SC_BAD_REQUEST, "The AuthHeader is missing, the decryption cannot be performed!");
		            return;
		    	}
		    	
		    	// ***************************** decrypting *****************************
				// get the content of the request
				RequestWrapper requestWrapper = new RequestWrapper((HttpServletRequest) request);
				final byte[] contentAsByteArray = IOUtils.toByteArray(requestWrapper.getReader(), StandardCharsets.UTF_8);
				LOGGER.trace("contentAsByteArray: " + new String(contentAsByteArray, StandardCharsets.UTF_8));
		        
		        byte[] decryptedContentAsBytes = contentAsByteArray;
		        Integer keySize = null;
	
		        // decrypt it and pass it to the other filters
	        	try {
	        		String encryptedSecurityKey = request.getHeader(Constants.HTTP_HEADER_SECURITY_KEY);
	        		String parameterIV = request.getHeader(Constants.HTTP_HEADER_SECURITY_IV);
	        		keySize = request.getHeader(Constants.HTTP_HEADER_SECURITY_KEY_SIZE) == null ? null : Integer.valueOf(request.getHeader(Constants.HTTP_HEADER_SECURITY_KEY_SIZE));
					decryptedContentAsBytes = cryptoTool.decrypt(new String(contentAsByteArray, StandardCharsets.UTF_8), 
							                                     encryptionType, 
							                                     encryptedSecurityKey == null ? null : encryptedSecurityKey.getBytes(StandardCharsets.UTF_8),
							                                     parameterIV == null ? null : parameterIV.getBytes(StandardCharsets.UTF_8),
					                                    		 credentialLoader.getPrivateKey(authHeader.getApiKey()).toCharArray());
					String decryptedContentAsBytesAsString = new String(decryptedContentAsBytes, StandardCharsets.UTF_8);
					LOGGER.trace("decryptedContentAsBytesAsString: {}, its size is {}", decryptedContentAsBytesAsString, decryptedContentAsBytesAsString != null ? -1 : decryptedContentAsBytesAsString.length());
					LOGGER.trace("decryptedContentAsBytes: {}", Arrays.toString(decryptedContentAsBytes));
				} catch (UnrecoverableKeyException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidAlgorithmParameterException e) {
		        	handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error occurred while decrypting! Please check if the certificates or the passwords are correct.", e);
		            return;
				} catch(ReactionRuntimeSecurityException e) {
		        	handleError(response, HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
		            return;
				}
		        // it has to be reset as the position of the ByteArrayInputStream is at the end -> the reset will get to the beginning so the subsequent filters / service can read the content
		        requestWrapper.resetInputStream(decryptedContentAsBytes);	
		    	
		    	// execute the subsequent filters
		    	ResponseWrapper responseWrapper = new ResponseWrapper((HttpServletResponse) response);
		        filterChain.doFilter(requestWrapper, responseWrapper);
		        byte[] dataResponse = responseWrapper.getDataStream();
				LOGGER.trace("response: {}", new String(dataResponse, StandardCharsets.UTF_8));
				// if all the filters and the service are called then ...
				
		    	// ***************************** encrypting *****************************
				try {
	    			// encrypt the response body
					String host = request.getHeader(Constants.HTTP_HEADER_SECURITY_HOST);
					Map<String, byte[]> encrypted = cryptoTool.encrypt(dataResponse, 
							                                           encryptionType,
							                                           credentialLoader.getPrivateKey(authHeader.getApiKey()).toCharArray(),
							                                           keySize,
							                                           host);
					response.addHeader(Constants.HTTP_HEADER_SECURITY_IV, new String(encrypted.get("IV"), StandardCharsets.UTF_8));
					byte[] encryptedDataResponse = encrypted.get("data");
					LOGGER.trace("encrypted response: " + new String(encryptedDataResponse, StandardCharsets.UTF_8));
					if (encryptionType.equals(EncryptionTypeEnum.CERTIFICATE_BASED)) {
						byte[] encryptedKey = encrypted.get("key");
						response.addHeader(Constants.HTTP_HEADER_SECURITY_KEY, new String(encryptedKey, StandardCharsets.UTF_8));
					}
					response.setContentLength(encryptedDataResponse.length);
					// and send it
					response.getOutputStream().write(encryptedDataResponse);
				} catch (UnrecoverableKeyException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | KeyStoreException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidParameterSpecException e) {
		        	handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error occurred while encrypting!", e);
		            return;
				} catch(ReactionRuntimeSecurityException e) {
		        	handleError(response, HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
		            return;
				}
        	} catch(Exception e) {
	        	handleError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unknown error occured!", e);
	        	return;
	        }
        }
    }


	private void handleError(HttpServletResponse response, int errorCode, String message, Exception... e) throws IOException {
	    String custoMessage = message;
	    
		if (e.length == 1) {
			logger.error(message, e[0]);
			custoMessage = message + " - " + e[0].getMessage();
		} else {
			logger.error(message);
		}
		
		response.setStatus(errorCode);
		response.getWriter().println(custoMessage);
	}

    
    private boolean isEncryptionNeeded(String servletPath) {
    	for (String patternText : endpointsWhereEncryptionIsNeeded) {
        	if (Pattern.compile(patternText).matcher(servletPath).find()) {
        		return true;
        	}
    	}
		return false;
	}


	@Required
	public void setCryptoTool(CryptoTool cryptoTool) {
		this.cryptoTool = cryptoTool;
	}


    @Required
	public void setCredentialLoader(CredentialLoader credentialLoader) {
		this.credentialLoader = credentialLoader;
	}

    
    public void setEndpointsWhereEncryptionIsNeeded(List<String> endpointsWhereEncryptionIsNeeded) {
    	this.endpointsWhereEncryptionIsNeeded = endpointsWhereEncryptionIsNeeded;
    }
    

class ResponseWrapper extends HttpServletResponseWrapper {

	ByteArrayOutputStream output;
	FilterServletOutputStream filterOutput;

	public ResponseWrapper(HttpServletResponse response) {
		super(response);
		output = new ByteArrayOutputStream();
	}

	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		if (filterOutput == null) {
			filterOutput = new FilterServletOutputStream(output);
		}
		return filterOutput;
	}

	public byte[] getDataStream() {
		return output.toByteArray();
	}
}


class FilterServletOutputStream extends ServletOutputStream {

	DataOutputStream output;

	public FilterServletOutputStream(OutputStream output) {
		this.output = new DataOutputStream(output);
	}

	@Override
	public void write(int arg0) throws IOException {
		output.write(arg0);
	}

	@Override
	public void write(byte[] arg0, int arg1, int arg2) throws IOException {
		output.write(arg0, arg1, arg2);
	}

	@Override
	public void write(byte[] arg0) throws IOException {
		output.write(arg0);
	}

	@Override
	public boolean isReady() {
		return false;
	}

	@Override
	public void setWriteListener(WriteListener listener) {
	}
	
}

}