package org.reaction.engine.security;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.reaction.common.exception.ReactionRuntimeSecurityException;
import org.reaction.engine.scheduling.Scheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;

public class CredentialLoader {

	private static final Logger LOGGER = LoggerFactory.getLogger(CredentialLoader.class);
	
	private Map<String, String> credentails = new ConcurrentHashMap<>();
	@Value("${reaction.security.credentials_file}")
	private String credentialFilePath;
	@Value("${reaction.security.credentials_file_reload_period}")
	private int credentialsFileReloadPperiod;
	@Autowired
	private Scheduler scheduler;
	

	@PostConstruct
	public void init() {
		LOGGER.info("Loading the credential file for the 1st time and scheduling to reload it... ");
		// 1. loading the credential file for the 1st time
		loadCredentail();
		// 2. starting the scheduler to reload the credential file in every X sec
		scheduler.schedule(() -> loadCredentail(), credentialsFileReloadPperiod * 1000);
	}

	
	// In the file the credentials have to be specified in the following format
	//localhost=fjf9wiofjksdfu9iwerkj
	//ormlprap60    =jf8489jsdlk39fgnjfdpo3iflékelknsd
	//    the public key (username) and the private key (password) will be trimmed so it mustn't contain space!!
	public final void loadCredentail() {
		synchronized(credentails) {
			LOGGER.debug("Loading the credential file...");
			try(BufferedReader reader = new BufferedReader( new FileReader( ResourceUtils.getFile(credentialFilePath) )) ) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] credentials = line.split("=",2);
					if (credentials.length != 2 || credentials[0].length() == 0 || credentials[1].length() == 0) {
						throw new ReactionRuntimeSecurityException("The credential file doesn't contain credentials in the expected format!");
					}
					credentails.put(credentials[0].trim(), credentials[1].trim());
				}
	        } catch (IOException e) {
	        	throw new ReactionRuntimeSecurityException("Exception occured when trying to load the credential file!", e);
	        }
		}
	}


	public String getPrivateKey(String publicKey) {
		String privateKey = null;
		synchronized(credentails) {
			privateKey = credentails.get(publicKey);
		}
		if (privateKey == null) {
			throw new ReactionRuntimeSecurityException("No private key exists for the public key '" + publicKey + "'!");
		}
		return privateKey;
	}
	
}
