package org.reaction.engine.security;

import java.util.ArrayList;

import org.reaction.common.exception.ReactionRuntimeSecurityException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class FileBasedUserDetailsService implements UserDetailsService {

	
    private CredentialLoader credentialLoader;

    
	@Override
	public UserDetails loadUserByUsername(String publicKey) {
		try {
			String privateKey = credentialLoader.getPrivateKey(publicKey);
			return new User(publicKey, privateKey, new ArrayList<>());
		} catch(ReactionRuntimeSecurityException e) {
			throw new UsernameNotFoundException(e.getMessage());
		}
	}


    @Required
	public void setCredentialLoader(CredentialLoader credentialLoader) {
		this.credentialLoader = credentialLoader;
	}
	
}
