package org.reaction.engine.security;

import java.util.Collections;
import java.util.List;

import org.reaction.common.queue.LimitedList;


public class NonceStorage {

	private volatile LimitedList<String> nonceList = null;
	
	
	public NonceStorage(int limit) {
		nonceList = new LimitedList<>(limit);
	}
	
	
	public void add(String nonce) {
		synchronized (nonceList) {
			nonceList.add(nonce);
		}
	}
	
	
	public boolean contains(String nonce) {
		if (nonce == null) {
			return false;
		}
		
		synchronized (nonceList) {
			List<String> reversedNonceList = nonceList.subList(0, nonceList.size());
			Collections.reverse(reversedNonceList);
	
			for(String n : reversedNonceList) {
				if (nonce.equals(n)) {
					return true;
				}
			}
			
			return false;
		}
	}

}