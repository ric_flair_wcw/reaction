#! /bin/sh
#  /etc/init.d/reaction-worker-reader-daemon

# ########################################################################################################
# ##################################### VALUES TO BE CHANGED #############################################
# ########################################################################################################

# The path to Jsvc
EXEC=/usr/bin/jsvc

# The path to the folder containing the java runtime
J_HOME=$JAVA_HOME

# The user to run the daemon
USER=root

# ########################################################################################################
# ############################ VALUES THAT DON'T NEED TO BE CHANGED ######################################
# ########################################################################################################

VERSION=1.2-SNAPSHOT
NAME=reaction-worker-reader-daemon
DESC="Reaction Reader Daemon service v$VERSION"

echo ""
echo "---------------------------------"
echo "| Reaction - Reader worker v$VERSION |"
echo "---------------------------------"
echo ""

# The folder where the worker resides
SCRIPT=$(readlink -f "$0")
export BASE_FOLDER=$(dirname "$SCRIPT")

# Our classpath including our jar file and the Apache Commons Daemon library
LIB_PATH=$BASE_FOLDER/lib
CLASS_PATH="$(echo "$LIB_PATH"/*.jar | tr ' ' ':')"
CLASS_PATH=$CLASS_PATH:$BASE_FOLDER/conf:$BASE_FOLDER/security

# The fully qualified name of the class to execute
CLASS="org.reaction.worker.DaemonLauncer"

# The file that will contain our process identification number (pid) for other scripts/programs that need to access it.
PID_FILE=/var/run/$NAME.pid
if [ -f "$PID_FILE" ]; then
    PID="$(sudo cat $PID_FILE)"
fi

# System.out writes to this file...
LOG_OUT=$BASE_FOLDER/logs/$NAME.out

# System.err writes to this file...
LOG_ERR=$BASE_FOLDER/logs/$NAME.err

# check if JAVA_HOME is set correctly
if [ -z "$J_HOME" ]; then
    echo "Warning! The JDK home is not set properly either in the script (see J_HOME variable) or in the JAVA_HOME system variable in the context of the Linux user that runs the script (i.e. if the script is executed with sudo then the JAVA_HOME has to be set for the root user)!"
    echo "Exiting..."
    exit -1
fi

jsvc_exec()
{
    $EXEC -Xss2m -home $J_HOME -cp $CLASS_PATH -user $USER -outfile $LOG_OUT -errfile $LOG_ERR -wait 10 -pidfile $PID_FILE $1 $CLASS "reader"
}

# check the first argument (start/stop/restart/status)
case "$1" in
    start)
        if [ -f "$PID_FILE" ]; then
            echo "The $DESC is running already, no action taken."
        else
            echo "Starting the $DESC..."
            # Start the service
            jsvc_exec "" "$2"
            if [ $? -eq 0 ]; then
                echo "The $DESC has started."
            else
                echo "The $DESC has NOT started successfully!"
                exit 1
            fi
        fi
    ;;
    stop)
        if [ -f "$PID_FILE" ]; then
            echo "Stopping the $DESC..."
            # Stop the service
            jsvc_exec "-stop" "$2"
            if [ $? -eq 0 ]; then
                echo "The $DESC has stopped."
            else
                echo "The $DESC has NOT stopped successfully!"
                exit 1
            fi
        else
            echo "The $DESC is not running, no action taken."
        fi
    ;;
    restart)
        if [ -f "$PID_FILE" ]; then
            echo "Restarting the $DESC..."
            # Stop the service
            jsvc_exec "-stop"
            if [ $? -eq 0 ]; then
                # Start the service
                jsvc_exec
                if [ $? -eq 0 ]; then
                    echo "The $DESC has restarted."
                else
                    echo "The $DESC has NOT restarted successfully!"
                    exit 1
                fi
            else
                echo "The $DESC has NOT stopped successfully!"
                exit 1
            fi
        else
            echo "The $DESC is not running, no action taken."
        fi
    ;;
    status)
        if [ -f "$PID_FILE" ]; then
            STATUS=RUNNING
        else
            STATUS=stopped
        fi
        echo "Status                    : $STATUS"
        echo "Process id                : $PID"
        echo "Location of log files     : $BASE_FOLDER/logs"
        echo "Application config file   : $BASE_FOLDER/conf/worker.yml"
        echo "Logging config file       : $BASE_FOLDER/conf/logback.xml"
        echo "PIF file                  : $PID_FILE"
    ;;
    *)
    echo "Usage: /etc/init.d/$NAME {start|stop|restart|status}" >&2
    exit 3
    ;;
esac
