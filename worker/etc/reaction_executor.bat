@ECHO OFF
	
REM ########################################################################################################
REM ##################################### VALUES TO BE CHANGED #############################################
REM ########################################################################################################

REM The path to the folder containing the java runtime
SET J_HOME=%JAVA_HOME%

REM ########################################################################################################
REM ############################ VALUES THAT DON'T NEED TO BE CHANGED ######################################
REM ########################################################################################################

SET VERSION=1.2-SNAPSHOT
SET NAME=reaction-worker-executor-daemon
SET DESC=Reaction Executor Worker Daemon service v%VERSION%

ECHO.
ECHO -----------------------------------
ECHO ^| Reaction - Executor worker v%VERSION% ^|
ECHO -----------------------------------
ECHO.

REM The folder where the worker resides
SET mypath=%~dp0
SET BASE_FOLDER=%mypath:~0,-1%

REM Our classpath including our jar file and the Apache Commons Daemon library
SET LIB_PATH=%BASE_FOLDER%/lib
SET CLASS_PATH=
FOR /R ./lib %%a in (*.jar) DO CALL :AddToPath %%a
SET CLASS_PATH=%CLASS_PATH%;%BASE_FOLDER%\conf;%BASE_FOLDER%\security

REM The fully qualified name of the class to execute
SET CLASS=org.reaction.worker.DaemonLauncer

REM Log path
SET LOG_PATH=%BASE_FOLDER%/logs
REM System.out writes to this file...
SET LOG_OUT=%LOG_PATH%/%NAME%.out
REM System.err writes to this file...
SET LOG_ERROR=%LOG_PATH%/%NAME%.err

REM The path to Jsvc
REM   Note: if the exe is put into a subfolder (e.g. bin/) then the logs folder will be created in that subfolder...
SET EXEC=%BASE_FOLDER%/prunsrv.exe

REM Service startup mode can be either auto or manual
SET STARTUP_MODE=manual

REM for details see at https://commons.apache.org/proper/commons-daemon/procrun.html
IF "%1" == "install" (
	ECHO Installing the %DESC% ...
	REM enabling the JMX connection to monitor the worker with JConsole:  %EXEC% install %NAME% --Install=%EXEC% --DisplayName=%NAME% --Jvm=auto --StartMode=jvm --StartClass=%CLASS% --StartMethod=startWindowService --StartParams=executor --StopMode=jvm --StopClass=org.reaction.worker.DaemonLauncer --StopMethod=stopWindowService --StopParams=executor ++Environment="BASE_FOLDER=%BASE_FOLDER%" --JvmOptions="-Dapp.home=%LIB_PATH%/..;-Dcom.sun.management.jmxremote=true;-Dcom.sun.management.jmxremote.port=9010;-Dcom.sun.management.jmxremote.local.only=false;-Dcom.sun.management.jmxremote.authenticate=false;-Dcom.sun.management.jmxremote.ssl=false;-Djava.rmi.server.hostname=127.0.0.1" --LogPrefix=%NAME% --LogPath="%LOG_PATH%" --StdError="%LOG_ERROR%" --StdOutput="%LOG_OUT%" --Classpath=%CLASS_PATH% --JavaHome=%J_HOME% --Startup=%STARTUP_MODE%
	%EXEC% install %NAME% --Install=%EXEC% --DisplayName=%NAME% --Jvm=auto --StartMode=jvm --StartClass=%CLASS% --StartMethod=startWindowService --StartParams=executor --StopMode=jvm --StopClass=org.reaction.worker.DaemonLauncer --StopMethod=stopWindowService --StopParams=executor ++Environment="BASE_FOLDER=%BASE_FOLDER%" --JvmOptions="-Dapp.home=%LIB_PATH%/.." --LogPrefix=%NAME% --LogPath="%LOG_PATH%" --StdError="%LOG_ERROR%" --StdOutput="%LOG_OUT%" --Classpath=%CLASS_PATH% --JavaHome=%J_HOME% --Startup=%STARTUP_MODE%
) ELSE IF "%1" == "deinstall" (
	ECHO Deinstalling the %DESC% ...
	%EXEC% delete %NAME%
) ELSE IF "%1" == "start" (
	ECHO Starting the %DESC% ...
	%EXEC% start %NAME%
) ELSE IF "%1" == "stop" (
	ECHO Stopping the %DESC% ...
	%EXEC% stop %NAME%
) ELSE (
	ECHO Usage: reaction_executor.bat {install^|deinstall^|start^|stop}
)
GOTO :EOF

:AddToPath
SET CLASS_PATH=%1;%CLASS_PATH%
GOTO :EOF