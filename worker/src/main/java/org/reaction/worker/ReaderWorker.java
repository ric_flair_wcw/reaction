package org.reaction.worker;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.reaction.common.dto.EventSourceWithErrorDetectorPattern;
import org.reaction.worker.call.RestClient;
import org.reaction.worker.log.LogReader;
import org.reaction.worker.thread.ThreadManager;
import org.reaction.worker.utils.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;


public abstract class ReaderWorker {

	
	@Autowired
	private ApplicationContext applicationContext;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReaderWorker.class);

	private Storage storage = null;
	private RestClient restClient = null;
	private ThreadManager threadManager = null;
	
	@Value("${application.reader.sleeping}")
	private int sleeping;
	
	private volatile boolean shouldRun = true;
	
	
	public void start() {
		loop();
	}
	
	
	void loop() {
		LOGGER.info("The Reader worker has been started.");
		storage.setWorkerType("reader");
		LocalDateTime prevLoop = LocalDateTime.of(1984, 12, 16, 7, 45, 55);
		try {
			while (shouldRun) {
				if (prevLoop.until( LocalDateTime.now(), ChronoUnit.SECONDS) > sleeping) {
					// refreshing the system cache
					refresh();
					// waiting for some sec, configured in the prop file
					LOGGER.debug("I'll wait for " + sleeping + " sec...");
					prevLoop = LocalDateTime.now();
				}
				Thread.sleep(500);
			}
		} catch(Exception e) {
			LOGGER.error("Unexpected exception occured! The worker will be shut down.", e);
		} finally {
			stopStartedLogReaders();
		}
	}
	
	
	void refresh() {
		// getting the eventSource list from the server for the specific host asynchronously -> refreshing the cache
		LOGGER.info("I'll call the engine to get the systems.");
		List<EventSourceWithErrorDetectorPattern> systms = getSystm();
		LOGGER.info("The systems arrived from the server are\n{}", (systms == null ? "null" : systms.stream().map(EventSourceWithErrorDetectorPattern::toString).collect(Collectors.joining("\n"))));
		if (systms != null && systms.size() > 0) {
			// 2. start the Log Reader thread(s) -> only if it is new log file
			// 2.1. remove those systems that didn't change since the last refresh -> no need to stop their thread and start a new one
			List<EventSourceWithErrorDetectorPattern> systmsToBeObserved = systms.stream().filter(s ->  storage.getObservedSystem(s) == null ||
					                                                                              isNotEqual(storage.getObservedSystem(s).getLogPath(), s.getLogPath()) ||
					                                                                              isNotEqual(storage.getObservedSystem(s).getHost(), s.getHost()) ||
					                                                                              isNotEqual(storage.getObservedSystem(s).getLogHeaderPatternEnabled(), s.getLogHeaderPatternEnabled()) ||
					                                                                              isNotEqual(storage.getObservedSystem(s).getLogHeaderPattern(), s.getLogHeaderPattern()) ||
					                                                                              isNotEqual(storage.getObservedSystem(s).getLogLevel(), s.getLogLevel()))
															                        .collect(Collectors.toList());
			LOGGER.debug("I removed those systems from the list that didn't change since the last refresh ie. no need to stop their thread and start a new one. The remaining systems are (that has to be processed, eg. start/stop them, etc.)\n{}", 
					systmsToBeObserved.stream().map(EventSourceWithErrorDetectorPattern::toString).collect(Collectors.joining("\n")));
			
			if (systmsToBeObserved.size() > 0) {
				// 2.2. if the system id matches but the other properties don't then there is an existing thread running -> stop it (i.e. remove it from observedSystems; see in LogReader)
				List<EventSourceWithErrorDetectorPattern> systmsToBeRemoved = systms.stream().filter(s -> storage.getObservedSystem(s) != null &&
									                                                               (isNotEqual(storage.getObservedSystem(s).getLogPath(), s.getLogPath()) ||
									                                                            	isNotEqual(storage.getObservedSystem(s).getHost(), s.getHost()) ||
									                                                            	isNotEqual(storage.getObservedSystem(s).getLogHeaderPatternEnabled(), s.getLogHeaderPatternEnabled()) ||
									                                                            	isNotEqual(storage.getObservedSystem(s).getLogHeaderPattern(), s.getLogHeaderPattern()) ||
									                                                                isNotEqual(storage.getObservedSystem(s).getLogLevel(), s.getLogLevel()) ) )
						                                                               .collect(Collectors.toList());
				systmsToBeRemoved.stream().forEach(s -> { storage.stopLogReader(s); 
				                                          storage.removeFromObservedSystems(s); });
				LOGGER.warn("If the system id matches but the other properties don't then there is an existing thread running -> stop it:\n{}", 
						systmsToBeRemoved.stream().map(EventSourceWithErrorDetectorPattern::toString).collect(Collectors.joining("\n")));
				
				// 2.3. if the system id doesn't arrive for a running thread then it was deleted so stop the thread
				Set<Long> systmIds = systms.stream().map(EventSourceWithErrorDetectorPattern::getId).collect(Collectors.toSet());
				systmsToBeRemoved = storage.getObservedSystems().values().stream().filter(os -> !systmIds.contains(os.getId()))
						                                                          .collect(Collectors.toList());
				systmsToBeRemoved.stream().forEach(s -> { storage.stopLogReader(s);
				                                          storage.removeFromObservedSystems(s); });
				LOGGER.warn("If the system id doesn't arrive for a running thread then it was deleted so stop the thread:\n{}", 
						systmsToBeRemoved.stream().map(EventSourceWithErrorDetectorPattern::toString).collect(Collectors.joining("\n")));
				
				// 2.4. start a new thread for each log file (systm)
				// TODO improving the thread management (thread pool?) to be able to stop the threads easily
				for(EventSourceWithErrorDetectorPattern s : systmsToBeObserved) {
					// add to the storage
					storage.addToObservedSystems(s);
					LogReader logReader = getLogReader().setSystm(s);
					storage.addToLogReaders(s, logReader);
					// start the thread to observe the log file
					threadManager.startNewThread(logReader);
				}
				
				LOGGER.info("The observed systms are \n{}", storage.getObservedSystems().values().stream().map(EventSourceWithErrorDetectorPattern::toString).collect(Collectors.joining("\n")));
			} else {
				LOGGER.info("No system observation has to be restarted...");
			}
			// updating the pattern of the error detectors of the system if the pattern was changed
			updateErrorDetectorPatterns(systms);			
		} else {
			LOGGER.info("No systm list got back from the server.");
		}
	}
	

	private boolean updateErrorDetectorPatterns(List<EventSourceWithErrorDetectorPattern> systms) {
		boolean modified = false;
		for(EventSourceWithErrorDetectorPattern s : systms) {
			List<Map<Long,String>> observedPatterns = storage.getObservedSystem(s).getPattern();
			List<Map<Long,String>> patterns = s.getPattern();
			// if the their size is different then update
			if (observedPatterns.size() != patterns.size()) {
				LOGGER.debug("Updating the error detector patterns of the following system. {}", s);
				storage.getObservedSystem(s).setPattern(patterns);
				storage.getLogReader(s.getId()).setSystm(s);
				modified = true;
				break;
			}
			Comparator<Map<Long,String>> comparator = (o,t) -> o.keySet().iterator().next().compareTo(t.keySet().iterator().next());
			// sort the list by the id
			observedPatterns.sort(comparator);
			patterns.sort(comparator);
			// check if they contain the same ids and the same pattern texts
			for (int i = 0; i < patterns.size(); i++) {
				Long id = patterns.get(i).keySet().iterator().next();
				String pattern = patterns.get(i).get(id);
				// if not equal then update
				if (!pattern.equals( observedPatterns.get(i).get(id) )) {
					LOGGER.debug("Updating the error detector patterns of the following system. {}", s);
					storage.getObservedSystem(s).setPattern(patterns);
					storage.getLogReader(s.getId()).setSystm(s);
					modified = true;
					break;
				}
			}
		}
		return modified;
	}


	private void stopStartedLogReaders() {
		LOGGER.info("Stopping the started log readers...");
		storage.getLogReaders().values().stream().forEach(lr -> lr.stop());
		LOGGER.info("All the started log readers are stopped.");
	}


	private List<EventSourceWithErrorDetectorPattern> getSystm() {
		return restClient.getSystms();
	}

	
	private boolean isNotEqual(Object o1, Object o2) {
		if (o1 == null && o2 == null) {
			return false;
		} else {
			return o1 == null && o2 != null || 
				   o1 != null && o2 == null ||
				   !o1.equals(o2);
		}
	}
	
	
	public void stop() {
		LOGGER.trace("The shouldRun of ReaderWorker is set to false.");
		this.shouldRun = false;
	}
	

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

	public void setRestClient(RestClient restClient) {
		this.restClient = restClient;
	}

	public LogReader getLogReader() {
		return (LogReader) applicationContext.getBean("logReader");
	}

	public void setThreadManager(ThreadManager threadManager) {
		this.threadManager = threadManager;
	}

	protected abstract LogReader createLogReader();
	
}
