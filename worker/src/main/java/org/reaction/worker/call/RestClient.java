package org.reaction.worker.call;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EncryptionTypeEnum;
import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.dto.EventSourceWithErrorDetectorPattern;
import org.reaction.common.security.CryptoTool;
import org.reaction.common.security.EncryptionEnvironmentLoader;
import org.reaction.common.security.HmacSignatureBuilder;
import org.reaction.worker.utils.CredentialLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RestClient {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RestClient.class);
	private static final String USER_AGENT = "Reaction Rest client";
	
			
	@Value("${host_name}")
	private String host;
	@Value("${rest_call.engine_endpoint}")
	private String engineEndpoint;
	@Value("${rest_call.encryption.type}")
	private String encryptionType;
	@Value("${rest_call.encryption.key_size:0}")
	private Integer keySize;

	private RestTemplate restTemplate;
	private CredentialLoader credentialLoader;
	private CryptoTool cryptoTool;
	private EncryptionEnvironmentLoader encryptionEnvironmentLoader;
	private ObjectMapper objectMapper = new ObjectMapper();
	
	
	public List<Map<String, String>> getCommands() {
		try {
			String resource = "worker/command/" + host;
			String url = engineEndpoint + "/" + resource;
			LOGGER.debug("Calling the GET REST service on '{}'.", url);
			
			// building the request
			HttpEntity<?> requestEntity = buildRequestEntity(null, HttpMethod.POST, url);
			LOGGER.debug("The request is '{}'.", requestEntity);
			
			// calling the REST
			Class<?> clazz = EncryptionTypeEnum.valueOf(encryptionType).equals(EncryptionTypeEnum.NONE) ? Object.class : String.class;
			ResponseEntity responseEntity = restTemplate.exchange(url, 
                                                                   HttpMethod.POST,
                                                                   requestEntity,
                                                                   clazz);
			LOGGER.debug("REST is called, the response is '{}'.", responseEntity);
			List<Map<String, String>> commands;
			if (!EncryptionTypeEnum.valueOf(encryptionType).equals(EncryptionTypeEnum.NONE)) {
				commands = cryptoTool.decrypt((String)responseEntity.getBody(), 
						                      new TypeReference<List<Map<String, String>>>() { },
						                      getHeader(responseEntity, Constants.HTTP_HEADER_SECURITY_KEY),
						                      getHeader(responseEntity, Constants.HTTP_HEADER_SECURITY_IV),
						                      credentialLoader.getPrivateKey().toCharArray());
				LOGGER.debug("The encrypted commands are '{}'.", commands);
			} else {
				commands = new ObjectMapper().convertValue(responseEntity.getBody(), new TypeReference<List<Map<String, String>>>() { });
			}
			return commands;
		} catch(ResourceAccessException|HttpClientErrorException|HttpServerErrorException e) {
			handleRestException(e);
			return new ArrayList<>();
		} catch(Exception e) {
			LOGGER.error("Unknown error!", e);
			return new ArrayList<>();
		}
	}


	public List<EventSourceWithErrorDetectorPattern> getSystms() {
		try {
			String resource = "worker/system/" + host;
			String url = engineEndpoint + "/" + resource;
			LOGGER.debug("Calling the GET REST service on '{}'.", url);

			// building the request
			HttpEntity<?> requestEntity = buildRequestEntity(null, HttpMethod.GET, url);
			LOGGER.debug("The request is '{}'.", requestEntity);
			
			// calling the REST
			Class<?> clazz = EncryptionTypeEnum.valueOf(encryptionType).equals(EncryptionTypeEnum.NONE) ? Object.class : String.class;
			ResponseEntity responseEntity = restTemplate.exchange(url, 
																  HttpMethod.GET,
																  requestEntity,
																  clazz);
			LOGGER.debug("REST is called, the response is '{}'.", responseEntity);
			List<EventSourceWithErrorDetectorPattern> systms;
			if (!EncryptionTypeEnum.valueOf(encryptionType).equals(EncryptionTypeEnum.NONE)) {
				systms = cryptoTool.decrypt((String)responseEntity.getBody(), 
						                    new TypeReference<List<EventSourceWithErrorDetectorPattern>>() { },
						                    getHeader(responseEntity, Constants.HTTP_HEADER_SECURITY_KEY),
						                    getHeader(responseEntity, Constants.HTTP_HEADER_SECURITY_IV),
						                    credentialLoader.getPrivateKey().toCharArray());
				LOGGER.debug("The encrypted systems are '{}'.", systms);
			} else {
				systms = new ObjectMapper().convertValue(responseEntity.getBody(), new TypeReference<List<EventSourceWithErrorDetectorPattern>>() { });
			}
			return systms;
		} catch(ResourceAccessException|HttpClientErrorException|HttpServerErrorException e) {
			handleRestException(e);
			return null;
		} catch(Exception e) {
			LOGGER.error("Unknown error!", e);
			return null;
		}
	}


	public void sendResult(String eventLifeId, Boolean successful, String output, String extractedValue) throws UnrecoverableKeyException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeySpecException, InvalidParameterSpecException {
		String resource = "worker/command/result";
		String url = engineEndpoint + "/" + resource;

		// building the request
		Map<String, Object> commandResult = new HashMap<>();
		commandResult.put("eventLifeId", eventLifeId);
		commandResult.put("output", output);
		commandResult.put("extractedValue", extractedValue);
		commandResult.put("successful", successful);
		LOGGER.debug("Calling the POST REST service on '{}'. commandResult: '{}'.", url, commandResult);
		
		HttpEntity<?> requestEntity = buildRequestEntity(commandResult, HttpMethod.POST, url);
		LOGGER.debug("The request is '{}'.", requestEntity);
		
		// calling the REST
		ResponseEntity<String> response = restTemplate.exchange(url, 
				              									HttpMethod.POST,
				              									requestEntity,
				              									String.class);
		if (response.getStatusCode().is2xxSuccessful()) {
			LOGGER.debug("REST is called.");
		} else {
			LOGGER.error("REST is called but not getting back with 2XX status! response: {}", response);
		}
	}


	public void reportEvent(String message, LogLevelEnum logLevel, Long systmId, String uuid) throws UnrecoverableKeyException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeySpecException, InvalidParameterSpecException {
		String resource = "worker/event";
		String url = engineEndpoint + "/" + resource;

		// building the request
		Map<String, Object> eventMap = new HashMap<>();
		eventMap.put("message", message);
		eventMap.put("systmId", systmId);
		eventMap.put("logLevel", logLevel == null ? null : logLevel.toString());
		eventMap.put("uuid", uuid);
		LOGGER.debug("Calling the POST REST service on '{}'. eventMap: '{}'.", url, eventMap);

		HttpEntity<?> requestEntity = buildRequestEntity(eventMap, HttpMethod.POST, url);
		LOGGER.debug("The request is '{}'.", requestEntity);

		// calling the REST
		ResponseEntity<String> response = restTemplate.exchange(url, 
				              									HttpMethod.POST,
				              									requestEntity,
				              									String.class);
		if (response.getStatusCode().is2xxSuccessful()) {
			LOGGER.debug("REST is called.");
		} else {
			LOGGER.error("REST is called but not getting back with 2XX status! response: {}", response);
		}
	}

	
	private HttpEntity<?> buildRequestEntity(Map<String, Object> objectToBeSent, HttpMethod httpMethod, String url) throws UnrecoverableKeyException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidParameterSpecException {
		// setting the accepted HTTP type
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		// adding the HMAC token to the header
		String jsonString = addHmacTokenToHeader(httpHeaders, httpMethod, objectToBeSent, url);
		// adding the security setting to the HTTP header
		addSecuritySettingToHeader(httpHeaders);
		// encrypting the request body if needed
		Map<String, byte[]> encrypted = cryptoTool.encrypt(jsonString == null ? new byte[0] : jsonString.getBytes(StandardCharsets.UTF_8),
														   credentialLoader.getPrivateKey().toCharArray(),
														   keySize,
														   encryptionEnvironmentLoader.getTruststoreAlias());
		// add the encryption key, the IV parameter and the key size to the header if needed
		if (!EncryptionTypeEnum.valueOf(encryptionType).equals(EncryptionTypeEnum.NONE)) {
			if (EncryptionTypeEnum.valueOf(encryptionType).equals(EncryptionTypeEnum.CERTIFICATE_BASED)) {
				httpHeaders.add(Constants.HTTP_HEADER_SECURITY_KEY, new String(encrypted.get("key"), StandardCharsets.UTF_8));
				httpHeaders.add(Constants.HTTP_HEADER_SECURITY_HOST, host);
			}
			httpHeaders.add(Constants.HTTP_HEADER_SECURITY_IV, new String(encrypted.get("IV"), StandardCharsets.UTF_8));
			httpHeaders.add(Constants.HTTP_HEADER_SECURITY_KEY_SIZE, keySize.toString());
		}
		// creating the HttpEntity to be sent
		HttpEntity<Object> requestEntity = new HttpEntity<Object>(encrypted.get("data"), httpHeaders);
		return requestEntity;
	}


	private void addSecuritySettingToHeader(HttpHeaders httpHeaders) {
		httpHeaders.add(Constants.HTTP_HEADER_SECURITY_SETTING, ""+encryptionEnvironmentLoader.getEncryptionType().getCode());
	}


	private String addHmacTokenToHeader(HttpHeaders httpHeaders, HttpMethod httpMethod, Object objectToBeSent, String url) {
        String apiKey = credentialLoader.getPublicKey();
        String apiSecret = credentialLoader.getPrivateKey();

        String nonce = UUID.randomUUID().toString();

        httpHeaders.setDate(Clock.systemUTC().millis());
        String dateString = httpHeaders.getFirst(HttpHeaders.DATE);

        String valueAsString = null;
        if (objectToBeSent != null) {
	        try {
	            valueAsString = objectMapper.writeValueAsString(objectToBeSent);
	        } catch (JsonProcessingException e) {
	            throw new RuntimeException(e);
	        }
        }

        final HmacSignatureBuilder signatureBuilder = new HmacSignatureBuilder()
                .method(httpMethod.name())
                .endpoint(url)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .date(dateString)
                .nonce(nonce)
                .apiKey(apiKey)
                .apiSecret(apiSecret)
                .payload(valueAsString != null ? valueAsString.getBytes(StandardCharsets.UTF_8) : new byte[0]);
        final String signature = signatureBuilder.buildAsBase64String();

        final String authHeader = signatureBuilder.getAlgorithm() + " " + apiKey + ":" + nonce + ":" + signature;
        httpHeaders.add(HttpHeaders.AUTHORIZATION, authHeader);

        httpHeaders.add(HttpHeaders.USER_AGENT, USER_AGENT);
        
        return valueAsString;
	}

	
	private byte[] getHeader(ResponseEntity<?> responseEntity, String header) throws UnsupportedEncodingException {
		List<String> headerValue = responseEntity.getHeaders().get(header);
		if (headerValue == null || headerValue.size() == 0) {
			return null;
		} else {
			return headerValue.get(0).getBytes(StandardCharsets.UTF_8);
		}	
	}


	private void handleRestException(RestClientException e) {
		if (e instanceof HttpStatusCodeException) {
			HttpStatusCodeException httpStatusCodeException = (HttpStatusCodeException)e;
			LOGGER.error("Warning! The server cannot be called!\nError code: {}\nError message: {}", httpStatusCodeException.getStatusCode(), httpStatusCodeException.getResponseBodyAsString());
		} else {
			LOGGER.error("Warning! The server cannot be called!", e);
		}
	}
	

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public void setCredentialLoader(CredentialLoader credentialLoader) {
		this.credentialLoader = credentialLoader;
	}

	public void setCryptoTool(CryptoTool cryptoTool) {
		this.cryptoTool = cryptoTool;
	}

	public void setEncryptionEnvironmentLoader(EncryptionEnvironmentLoader encryptionEnvironmentLoader) {
		this.encryptionEnvironmentLoader = encryptionEnvironmentLoader;
	}
	
}
