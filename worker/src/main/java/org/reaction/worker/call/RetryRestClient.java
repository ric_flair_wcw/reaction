package org.reaction.worker.call;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

import org.reaction.common.contants.LogLevelEnum;
import org.reaction.worker.utils.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

public class RetryRestClient extends RestClient {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(RetryRestClient.class);
	
	@Value("${application.reader.call.queue_capacity}")
	private int queueCapacityReader;
	@Value("${application.executor.call.queue_capacity}")
	private int queueCapacityExecutor;
	@Value("${application.reader.call.validity_interval}") // in sec!!!!!!!!!!
	private int validIntervalReader;
	@Value("${application.executor.call.validity_interval}") // in sec!!!!!!!!!!
	private int validIntervalExecutor;
	@Value("${application.reader.call.sleeping}") // in millisec!!!!!!!!!!
	private int sleepingReader;
	@Value("${application.executor.call.sleeping}") // in millisec!!!!!!!!!!
	private int sleepingExecutor;

	private Storage storage = null;
	
	// if the sending thread runs already then this method won't do anything as all it has to be done is to put the entry to the queue
	private volatile Boolean isLoopWorking = false;
	private volatile LinkedBlockingQueue<Map<String,Object>> queue = new LinkedBlockingQueue<>();
	private boolean shouldRun = true;
	
	
	@Override
	public void sendResult(String eventLifeId, Boolean successful, String output, String extractedValue) {
		Map<String, Object> rec = new HashMap<>();
		rec.put("eventLifeId", eventLifeId);
		rec.put("output", output);
		rec.put("extractedValue", extractedValue);
		rec.put("successful", successful);
		rec.put("date", Calendar.getInstance());
		LOGGER.debug("RetryRestClient.sendResult, map to be sent is '{}'.", rec);
		
		// the 'r' below comes from the sendIt method
		Consumer<Map<String, Object>> sender = (r) -> {
			try {
				super.sendResult((String)r.get("eventLifeId"), 
				         		 (Boolean)r.get("successful"),
				         		 (String)r.get("output"),
				         		 (String)r.get("extractedValue"));
			} catch (Exception e) {
				throw new RuntimeException("Exception occurred when sending the result!", e);
			}				
		};
		
		try {
			if (addToQueue(rec, queueCapacityExecutor)) {
				sendIt(sender, sleepingExecutor, validIntervalExecutor);
			}
		} catch (InterruptedException ie) {
			LOGGER.error("Warning, InterruptedException occured!", ie);
		}
	}
	
	
	@Override
	public void reportEvent(String message, LogLevelEnum logLevel, Long systmId, String uuid) {
		Map<String, Object> rec = new HashMap<>();
		rec.put("message", message);
		rec.put("logLevel", logLevel);
		rec.put("systmId", systmId);
		rec.put("uuid", uuid);
		rec.put("date", Calendar.getInstance());
		LOGGER.debug("RetryRestClient.reportEvent, map to be sent is '{}'.", rec);
		
		// the 'r' below comes from the sendIt method
		Consumer<Map<String, Object>> sender = (r) -> {
			try {
				super.reportEvent((String)r.get("message"), 
				         		  (LogLevelEnum)r.get("logLevel"),
				         		  (Long)r.get("systmId"),
				         		  (String)r.get("uuid"));
			} catch (Exception e) {
				throw new RuntimeException("Exception occurred when reporting the event!", e);
			}
		};
		
		try {
			if (addToQueue(rec, queueCapacityReader)) {
				sendIt(sender, sleepingReader, validIntervalReader);
			}
		} catch (InterruptedException ie) {
			LOGGER.error("Warning, InterruptedException occured!", ie);
		}
	}

	
	public void stop() {
		LOGGER.trace("The shouldRun of RetryRestClient is set to false.");
		shouldRun = false;
	}
	
	
	// 1. if not sending has occurred yet then start a thread 
	//     try to resend until 
	//         - there is an entry in the queue
	//     an entry will be removed from the queue if
	//         - it was sent successfully
	//         - too old : [date in the entry] - [current date] > [validInterval of reader or executor]
	//     the entry won't be put to the queue if it hits the capacity (see in addToQueue)
	//     at network error the thread will wait (-> sleeping)
	// 2. if the thread runs already then this method won't do anything as all it has to be done is to put the entry to the queue
	private void sendIt(Consumer<Map<String, Object>> sender, int sleeping, int validInterval) {
		if (isLoopWorking) {
			return;
		}
		
		synchronized(isLoopWorking) {
			if (isLoopWorking) {
				return;
			}
			Thread t = new Thread(() -> {
				MDC.put("workerType", storage.getWorkerType());
				isLoopWorking = true;
				boolean retry = true;
				try {
					while (retry && shouldRun) {
						Map<String, Object> rec = null;
						try {
							// removing from the queue and in case of error will be put it back
							synchronized (queue) {
								rec = queue.poll();							
							}
							if (rec != null) {
								// check if it is not too old
								Calendar date = (Calendar)rec.get("date");
								if ((Calendar.getInstance().getTimeInMillis() - date.getTimeInMillis()) / 1000 < validInterval) {
									if (shouldRun) {
										LOGGER.trace("Trying to send the following data: {}", rec);
										sender.accept(rec);
									}
								} else {
									LOGGER.warn("The following result of a command execution is too old so no need to send it anymore!\n\t{}", rec);
								}
							} else {
								retry = false;
							}
						} catch(ResourceAccessException|HttpClientErrorException|HttpServerErrorException e) {
							LOGGER.error("Warning! The server cannot be called! {} - {}", e.getMostSpecificCause().getClass().getName(), e.getMessage());
							if ( !(e instanceof HttpClientErrorException) || (e instanceof HttpClientErrorException && ((HttpClientErrorException)e).getStatusCode().equals(HttpStatus.NOT_FOUND) ) ) {
								LOGGER.debug("The server will be called again.");
								// in case of error the rec will be put back to the queue
								try {
									queue.put(rec);
								} catch (InterruptedException ie) {
									LOGGER.error("Warning, InterruptedException occured!", ie);
								}
								// waiting
								if (shouldRun) {
									try { Thread.sleep(sleeping); }    
									catch (Exception e1) { }
								}
							}
						} catch(Exception e2) {
							LOGGER.error("Unknown error during the server call!", e2);
						}
					}
				}
				finally {
					isLoopWorking = false;
				}
			});
			t.start();
		}
	}

	
	// the queue has to be a FIFO
	private boolean addToQueue(Map<String, Object> rec, int queueCapacity) throws InterruptedException {
		if (queue.size() < queueCapacity) {
			LOGGER.trace("The record is added to the queue.");
			queue.put(rec);
			return true;
		} else {
			LOGGER.warn("The following record is not added to the queue (ie. it won't be sent) as it hits the limit ({}) of the queue!\n\t{}", queueCapacity, rec);
			return false;
		}
	}


	public void setStorage(Storage storage) {
		this.storage = storage;
	}
	
}