package org.reaction.worker.call;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.DefaultHostnameVerifier;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.PrivateKeyDetails;
import org.apache.http.ssl.PrivateKeyStrategy;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.Assert;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

public class SSLRestTemplate extends RestTemplate {


    public SSLRestTemplate(HttpComponentsClientHttpRequestFactory requestFactory, boolean sslEnable, boolean twoWaySSl, 
                           String locationKeyStore, String passwordKeyStore, String alias,
                           String locationTrustStore, String passwordTrustStore, boolean enableHostnameVerifier) throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException, KeyManagementException, UnrecoverableKeyException {
        
        if (sslEnable) {
            // checking the input parameteres
            Assert.hasText(locationTrustStore, "The 'rest_call.ssl.truststore.location' must not be empty!");
            Assert.hasText(passwordTrustStore, "The 'rest_call.ssl.truststore.password' must not be empty!");
            if (twoWaySSl) {
                Assert.hasText(locationKeyStore, "The 'rest_call.ssl.keystore.location' must not be empty!");
                Assert.hasText(passwordKeyStore, "The 'rest_call.ssl.keystore.password' must not be empty!");
                Assert.hasText(alias, "The 'rest_call.ssl.keystore.alias' must not be empty!");
            }
            
            // loading the keystore & truststore
            KeyStore keyStore = KeyStore.getInstance("jks");
            KeyStore trustStore = KeyStore.getInstance("jks");
            try (InputStream isKeyStore = twoWaySSl ? new FileInputStream(ResourceUtils.getFile(locationKeyStore)) : null;
                 InputStream isTrustStore = new FileInputStream(ResourceUtils.getFile(locationTrustStore));) {
                if (twoWaySSl) {
                    keyStore.load(isKeyStore, passwordKeyStore.toCharArray());
                }
                trustStore.load(isTrustStore, passwordTrustStore.toCharArray());
                
                // configuring an Apache HttpClient for SSL
                SSLContextBuilder sslContextBuilder = new SSLContextBuilder().loadTrustMaterial(trustStore, new TrustSelfSignedStrategy());
                if (twoWaySSl) {
                    sslContextBuilder.loadKeyMaterial(keyStore, passwordKeyStore.toCharArray(), (m, s) -> alias);
                }
                SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContextBuilder.build(),
                                                                                          enableHostnameVerifier ? new DefaultHostnameVerifier() : new NoopHostnameVerifier());
                
                HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
                requestFactory.setHttpClient(httpClient);
                
                this.setRequestFactory(requestFactory);
            }
        }
    }

    
    
}
