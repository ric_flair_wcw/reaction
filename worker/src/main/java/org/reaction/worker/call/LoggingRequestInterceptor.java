package org.reaction.worker.call;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

public class LoggingRequestInterceptor implements ClientHttpRequestInterceptor {

	
    final static Logger LOGGER = LoggerFactory.getLogger(LoggingRequestInterceptor.class);

    
    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        traceResponse(response);
        return response;
    }

    
    private void traceRequest(HttpRequest request, byte[] body) throws IOException {
    	StringBuffer stringBuffer = new StringBuffer();
    	stringBuffer.append("\n===========================request begin================================================");
    	stringBuffer.append("\nURI         : " + request.getURI());
    	stringBuffer.append("\nMethod      : " + request.getMethod());
    	stringBuffer.append("\nHeaders     : " + request.getHeaders() );
    	stringBuffer.append("\nRequest body: " + new String(body, StandardCharsets.UTF_8));
    	stringBuffer.append("\n==========================request end================================================");
        LOGGER.debug(stringBuffer.toString());
    }

    
    private void traceResponse(ClientHttpResponse _response) throws IOException {
    	try( ClientHttpResponse response = new BufferingClientHttpResponseWrapper(_response); ) {
	        StringBuffer stringBuffer = new StringBuffer();
	        stringBuffer.append("\n============================response begin==========================================");
	        stringBuffer.append("\nStatus code  : " + response.getStatusCode());
	        stringBuffer.append("\nStatus text  : " + response.getStatusText());
	        stringBuffer.append("\nHeaders      : " + response.getHeaders());
	        stringBuffer.append("\nResponse body: " + IOUtils.toString(response.getBody(), StandardCharsets.UTF_8));
	        stringBuffer.append("\n=======================response end=================================================");
	        LOGGER.debug(stringBuffer.toString());
    	}
    }

}


class BufferingClientHttpResponseWrapper implements ClientHttpResponse {

	private final ClientHttpResponse response;

	private byte[] body;


	BufferingClientHttpResponseWrapper(ClientHttpResponse response) {
		this.response = response;
	}


	public HttpStatus getStatusCode() throws IOException {
		return this.response.getStatusCode();
	}

	public int getRawStatusCode() throws IOException {
		return this.response.getRawStatusCode();
	}

	public String getStatusText() throws IOException {
		return this.response.getStatusText();
	}

	public HttpHeaders getHeaders() {
		return this.response.getHeaders();
	}

	public InputStream getBody() throws IOException {
		if (this.body == null) {
			this.body = StreamUtils.copyToByteArray(this.response.getBody());
			response.getBody().skip(0);
		}
		return new ByteArrayInputStream(this.body);
	}

	public void close() {
		this.response.close();
	}

}