package org.reaction.worker;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class DaemonLauncer implements Daemon {

	private static final Logger LOGGER = LoggerFactory.getLogger(DaemonLauncer.class);
	private static final ApplicationContext APPLICATION_CONTEXT = new ClassPathXmlApplicationContext("applicationContext.xml");
	private static final Map<String, Thread> THREADS = new Hashtable<>();
	
	private static ReaderWorker READER = (ReaderWorker) APPLICATION_CONTEXT.getBean("reader");
	private static ExecutorWorker EXECUTOR = (ExecutorWorker) APPLICATION_CONTEXT.getBean("executor");
	
    private static String WORKER_TYPE = null;
    
    
    @Override
    public void init(DaemonContext daemonContext) throws DaemonInitException, Exception {
    	LOGGER.info("Calling the 'init' method of the DaemonLauncer.");

        build(daemonContext.getArguments());
    }

    
    @Override
    public void start() throws Exception {
    	LOGGER.info("Calling the 'start' method of the DaemonLauncer. workerType={}", WORKER_TYPE);
    	
    	getThread().start();
    }

    
    @Override
    public void stop() throws Exception {
    	LOGGER.info("Calling the 'stop' method of the DaemonLauncer. workerType={}", WORKER_TYPE);
        try {
	    	if (WORKER_TYPE.equals("reader")) {
	    		READER.stop();
	    	} else {
	    		EXECUTOR.stop();
	    	}
	    	getThread().join(2000);
        } catch(InterruptedException e){
        	LOGGER.info("Exception occurred while trying to stop the daemon!", e);
            throw e;
        }
    }
   
    
    @Override
    public void destroy() {
    	LOGGER.info("Calling the 'destroy' method of the DaemonLauncer.");
    	getThread().interrupt();
    }

    
    // used py procrun (http://commons.apache.org/proper/commons-daemon/procrun.html)
    // from the doc: "Name of method to be called when service is started. It must be static void and have argument (String args[]). Only applies to jvm mode - in Java mode, the main method is always used. 
    //            Note: in jvm mode, the start method should not return until the stop method has been called."
    // --StartMode=jvm --StartClass=org.reaction.worker.DaemonLauncer --StartMethod=startWindowService --StartParams=executor --StopMode=jvm --StopClass=org.reaction.worker.DaemonLauncer --StopMethod=stopWindowService --StopParams=executor
    public static void startWindowService(String [] args) throws Exception {
    	build(args);
    	new DaemonLauncer().start();
    }


    public static void stopWindowService(String [] args) throws Exception {
    	build(args);
    	new DaemonLauncer().stop();
    }


	private static void build(String[] args) {
		MDC.put("workerType", args[0]);
		// checking the argument
		LOGGER.debug("The command line arguments are {}", Arrays.toString(args));
        if (args.length != 1 || (!args[0].equals("reader") && !args[0].equals("executor"))) {
        	throw new IllegalArgumentException("I expected one argument (reader/executor) but got " + Arrays.toString(args) + "!");
        } else {
        	WORKER_TYPE = args[0];
        }
		// the following is used in logback.xml in order to separate the log files for reader and executor workers
		MDC.put("workerType", WORKER_TYPE);
	}


	private static synchronized Thread getThread() {
		if (WORKER_TYPE.equals("reader") && THREADS.get(WORKER_TYPE) == null) {
	        Thread readerThread = new Thread() {
	            @Override
	            public void run() {
	            	MDC.put("workerType", WORKER_TYPE);
					READER.start();
	            }
	        };
	        THREADS.put(WORKER_TYPE, readerThread);
		} 
		else if (WORKER_TYPE.equals("executor") && THREADS.get(WORKER_TYPE) == null) {
	        Thread executorThread = new Thread() {
	            @Override
	            public void run() {
	            	MDC.put("workerType", WORKER_TYPE);
					EXECUTOR.start();
	            }
	        };
	        THREADS.put(WORKER_TYPE, executorThread);
		}
		return THREADS.get(WORKER_TYPE);
	}

}