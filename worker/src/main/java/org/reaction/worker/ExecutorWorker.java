package org.reaction.worker;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.reaction.worker.call.RestClient;
import org.reaction.worker.call.RetryRestClient;
import org.reaction.worker.execution.OSCommanExecutor;
import org.reaction.worker.utils.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;


public class ExecutorWorker {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExecutorWorker.class);
	
	private OSCommanExecutor osCommandExecutor = null;
	private RestClient restClient = null;
	private RetryRestClient retryRestClient = null;
	private volatile boolean shouldRun = true;
	private ExecutorService threadPool = null;
	private Storage storage = null;

	@Value("${application.executor.sleeping}")
	private int sleeping;
	@Value("${application.executor.max_nr_running_commands}")
	private int maxNrRunningCommands;
	
	
	public void start() {
		threadPool = Executors.newFixedThreadPool(maxNrRunningCommands);
		loop();
	}

	
	void loop() {
		LOGGER.info("The Executor worker thread has been started.");
		storage.setWorkerType("executor");
		LocalDateTime prevLoop = LocalDateTime.of(1984, 12, 16, 7, 45, 55);
		try {
			while (shouldRun) {
				if (prevLoop.until( LocalDateTime.now(), ChronoUnit.SECONDS) > sleeping) {
					// executing the commands coming from the server
					execute();
					// waiting for some sec, configured in the prop file
					LOGGER.debug("I'll wait for "+sleeping+" sec...");
					prevLoop = LocalDateTime.now();
				}
				Thread.sleep(500);
			}
		} catch(Exception e) {
			LOGGER.error("Unexpected exception occured! The worker will be shut down.", e);
		} finally {
			shutdownThreadPool();
			retryRestClient.stop();
		}
	}


	void execute() throws InterruptedException {
		// getting the commands to be executed for this host
		Map<String,List<Map<String, String>>> commands = group( restClient.getCommands() );
		// executing the different commands parallel; the same commands will be executed serially
		//    for example: arrives the following commands (command-C, user-U, pattern-P): (C1, U1, P1) and (C2, U1, P2) and (C1, U1, P1) => 2 threads will be started and one of the threads will execute 2 commands
		commands.values().stream().forEach(l -> {
			threadPool.submit(new Runnable() {
				@Override
				public void run() {
					MDC.put("workerType", "executor");
					// executing the same commands one after another
					for(Map<String, String> c : l) {
						LOGGER.info("Executing the command\n\t{}\nby the user '{}' and the output pattern is\n\t{}", c.get("command"), c.get("osUser"), c.get("outputPattern"));
						// calling the OS command
						String eventLifeId = c.get("eventLifeId");
						Map<String,Object> result = null;
						try {
							result = osCommandExecutor.execute(c.get("command"), c.get("osUser"), c.get("outputPattern"));
						}
						catch (Throwable t) {
							LOGGER.error("Error occurred!", t);
							// sending back the error message
							retryRestClient.sendResult(eventLifeId, false, t.getMessage(), null);
						}
						if (result != null) {
							// sending back the result
							retryRestClient.sendResult(eventLifeId, 
						   			                   (Boolean)result.get("successful"),
						   			                   (String)result.get("output"),
						   			                   (String)result.get("extractedValue"));
						}
					}
				}
			});
		});
	}

	
	// it groups the list by the command, user and pattern
	private Map<String,List<Map<String, String>>> group(List<Map<String, String>> commands) {
		return commands.stream().collect(Collectors.groupingBy(r -> r.get("command") + "-" + r.get("osUser") + "-" + r.get("outputPattern")));
	}

	
	private void shutdownThreadPool() {
		LOGGER.info("Shutting down the thread pool...");
		try {
		    threadPool.shutdown();
		    threadPool.awaitTermination(10, TimeUnit.SECONDS);
		}
		catch (InterruptedException e) {
			LOGGER.error("Tasks interrupted.", e);
		}
		finally {
		    if (!threadPool.isTerminated()) {
		        System.err.println("cancel non-finished tasks");
		        LOGGER.error("Cancel non-finished tasks.");
		    }
		    threadPool.shutdownNow();
		    LOGGER.info("The thread pool is stopped.");
		}		
	}
	

	public void stop() {
		LOGGER.trace("The shouldRun is set to false. " + System.identityHashCode(this));
		this.shouldRun = false;
	}

	public OSCommanExecutor getOsCommandExecutor() {
		return osCommandExecutor;
	}

	public void setOsCommandExecutor(OSCommanExecutor osCommandExecutor) {
		this.osCommandExecutor = osCommandExecutor;
	}

	public RestClient getRestClient() {
		return restClient;
	}

	public void setRestClient(RestClient restClient) {
		this.restClient = restClient;
	}

	public void setRetryRestClient(RetryRestClient retryRestClient) {
		this.retryRestClient = retryRestClient;
	}

	public void setThreadPool(ExecutorService threadPool) {
		this.threadPool = threadPool;
	}

	public void setStorage(Storage storage) {
		this.storage = storage;
	}

}
