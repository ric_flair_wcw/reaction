package org.reaction.worker.log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ThreadFactory;
import java.util.regex.Pattern;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.dto.EventSourceWithErrorDetectorPattern;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.common.log.LogAnalyzer;
import org.reaction.worker.call.RetryRestClient;
import org.reaction.worker.utils.Storage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;

public class LogReader implements Runnable, FileAlterationListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(LogReader.class);

	private RetryRestClient retryRestClient = null;
	private volatile EventSourceWithErrorDetectorPattern systm = null;
	private Long previousPos = null;

	private FileAlterationMonitor monitor;
	private LogAnalyzer logAnalyzer;
	private Storage storage = null;
	
	@Value("${application.reader.multiline_error_supported}")
	private boolean multilineErrorSupported;
	
	@Value("${application.reader.file_system_check_interval}")
	private Integer fileSystemCheckInterval;

	@Value("${application.reader.log_charset}")
	private String logCharset;

	
	@Override
	public void run() {
		MDC.put("workerType", "reader");
		LOGGER.info("The LogReader thread has been started!\n{}", systm);
		
		// http://stackoverflow.com/questions/37442784/java-file-tailer-implementation
		try {
			File logFile = new File(systm.getLogPath());
			// check if the file is readable
			//AccessController.checkPermission(new FilePermission(systm.getLogPath(), "read"));
			checkReadable(logFile);
			// before start watching get the previous position: if it is in the storage then retrieve it and if it is not then it  the end position of the file; 
			//       if the stored prev position is bigger then the file size then use the file size
			previousPos = storage.getPreviousPositionInLogFile(systm.getLogPath());
			LOGGER.trace("previousPos : {}", previousPos);
			Long fileSize = new Long(logFile.length());
			LOGGER.trace("fileSize : {}", fileSize);
			previousPos = previousPos == null ? fileSize : (previousPos > fileSize ? fileSize : previousPos);
			LOGGER.debug("The previous position is {}", previousPos);
			
			// checking if the file is changed
			startMonitoring(systm.getLogPath());
		} catch (Exception e) {
			LOGGER.error("Error while running the LogReader! \n{}\n{}", systm, e);
		}
	}

	
	private void checkReadable(File logFile) {
		try (FileReader fileReader = new FileReader(logFile.getAbsolutePath())) {
			fileReader.read();
		} catch(Exception e) {
			throw new ReactionRuntimeException("The file '" + systm.getLogPath() + "' cannot be read (it doesn't exist or permission missing)!", e);
		}
	}
	


	private void startMonitoring(String logFilePath) throws Exception {
        final File logFile = new File(logFilePath);
		FileAlterationObserver fao = new FileAlterationObserver(new File(logFile.getParent()), new FileFilter() {
	        public boolean accept(File file) {
	            return file.equals(logFile);
	        }
	    });

		monitor = new FileAlterationMonitor(fileSystemCheckInterval);

		fao.addListener(this);
		monitor.addObserver(fao);
		
		monitor.start();

	    // Use daemon thread to avoid thread leakage
	    monitor.setThreadFactory(new ThreadFactory() {
	        public Thread newThread(Runnable runnable) {
	            Thread t = new Thread(runnable);
	            t.setDaemon(true);
	            String nameDameon = "reaction-" + logFile.getName().replaceAll("\\s+","") + "-file-monitor-thread";
	            LOGGER.info("The name of the daemon thread is '{}'.", nameDameon);
	            t.setName(nameDameon);
	            return t;
	        }
	    });
	}

	public void stop() {
		// stop the FileAlterationMonitor
		if (monitor != null) {
			try {
				monitor.stop();
			} catch (Exception e) {
				LOGGER.error("The file monitoring cannot be stopped!", e);
			}
		}
		LOGGER.info("The file monitoring is stopped.");
		// record the previous position (which is the end position of file)  
		Long previousPosition = new Long(new File(systm.getLogPath()).length());
		storage.setPreviousPositionInLogFile(systm.getLogPath(), previousPosition);
		// stop the retryRestClient
		retryRestClient.stop();
	}

	public LogReader setSystm(EventSourceWithErrorDetectorPattern systm) {
		this.systm = systm;
		return this;
	}

	
	public void setRetryRestClient(RetryRestClient retryRestClient) {
		this.retryRestClient = retryRestClient;
	}


	@Override
	public void onStart(FileAlterationObserver observer) { }

	@Override
	public void onDirectoryCreate(File directory) {
		MDC.put("workerType", "reader");
		LOGGER.trace("The onDirectoryCreate is fired. {}", directory);
	}

	@Override
	public void onDirectoryChange(File directory) {
		MDC.put("workerType", "reader");
		LOGGER.trace("The onDirectoryChange is fired. {}", directory);
	}

	@Override
	public void onDirectoryDelete(File directory) {
		MDC.put("workerType", "reader");
		LOGGER.trace("The onDirectoryDelete is fired. {}", directory);		
	}

	@Override
	public void onFileCreate(File file) {
		MDC.put("workerType", "reader");
		LOGGER.trace("The onFileCreate is fired. {}", file);
		previousPos = 0l;
		
		readLogFile(file);
	}

	@Override
	public void onFileDelete(File file) {
		MDC.put("workerType", "reader");
		LOGGER.debug("The onFileDelete is fired. {}", file);
	}

	@Override
	public void onFileChange(File file) {
		MDC.put("workerType", "reader");
		// how the log rotation should work (from internet):
		//    - " 'current logfile is moved' Nope. It is copied, the old file gets emptied (moving would leave a slight option for the software that uses the log file to crash due to it expecting the log to be there)."
		//    - "if the option truncate is enabled in configuration file, then the file will not be moved - its content will only be copied to another file which is newly created and the current file will be cleared, so the inode id will not change. Actually the file is never changed."
		// ie. the onFileCreate is almost never called
		// !! if the log file is edited from vi or similar (for testing purposes) then it will behave differently -> new file is created and write the the whole again => it cannot be tested from vi!
		long fileSize = new File(systm.getLogPath()).length();
		if (fileSize < previousPos) {
			LOGGER.debug("The previous position is bigger then the file size, likely new file is created. file size = {}; previous position = {}", fileSize, previousPos);
			previousPos = 0l;
		}
		LOGGER.debug("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
		LOGGER.debug("The onFileChange is fired. {}; previous position = {}", file, previousPos);
		
		try {
			readLogFile(file);
		} catch(Exception e) {
			LOGGER.error("Error occurred while trying to process the incoming line(s) in the log file!", e);
		}
	}


	@Override
	public void onStop(FileAlterationObserver observer) { }
	
	
	private FileInputStream openFile(File file) {
		FileInputStream is = null;
		for (int i = 0 ; i < 6; i++) {
			try {
				is = new FileInputStream(file);
				return is;
			} catch (FileNotFoundException e) {
				LOGGER.warn("Will try to open the file '{}' again. Error: {}", file.getName(), e.getMessage());
			}
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				throw new ReactionRuntimeException("Weird, cannot sleep for 500 milisec...", e);
			}
		}
		throw new ReactionRuntimeException("Cannot open the file '" + file.getName() + "'!");
	}
	
	
	private void readLogFile(File file) {
		// if the file is changed then
		//       open the file
		try (FileInputStream is = openFile(file) ) {
			//is.getChannel().lock();
			try ( BufferedReader reader = new BufferedReader(new InputStreamReader(is, logCharset)) ) {
				//       go to the previous position
				reader.skip(previousPos);
				
				//       reading till the end of file
				List<String> lines = new ArrayList<>();
				int r = -1;
				char ch;
				String line = new String();
				boolean isReadingInAdvance = false;
				do {
					// not reading now if later (see at '\r') the character was already read -> won't miss anything
					if (!isReadingInAdvance) {
						r = reader.read();
					}
					isReadingInAdvance = false;
					if (r != -1) {
						previousPos++;
						ch = (char)r;
						// https://stackoverflow.com/questions/8969501/how-to-get-eol-character-of-any-file-in-java
						// on Windows: '\r\n'        on Linux: '\n'
						// if it is the end of line then add the string to the list; if not then add the chat to the string
						switch (ch) {
							case '\n': 
							case '\r': if (line.length() > 0) {
								           LOGGER.debug("Read the following line:\n\t{}", line);
								           lines.add(line);
							           }
							           line = new String();
							           // on Windows the line break is '\r\n' => so if the char is '\r' then another char has to be read and check if the next one is '\n' ->
							           //    if it is then skip it (read again) 
							           if (ch == '\r') {
							        	   r = reader.read();
							        	   isReadingInAdvance = true;
							        	   if (r != -1) {
							        		   ch = (char)r;
							        		   if (ch == '\n') {
							        			   r = reader.read();
							        			   previousPos++;
							        		   }
							        	   }
							           }
							           break;
							default: line += ch;
						}
							
					}
						
				} while (r != -1);
				
				//       give the read content to the LogAnalyzer
				processLines(lines);
				
			} catch(IOException ioe) {
				LOGGER.error("Problem occured while handling the file " + file.getName(), ioe);
			}
		} catch(IOException ioe) {
			LOGGER.error("Problem occured while handling the file " + file.getName(), ioe);
		}
	}

	
	private void processLines(List<String> lines) {
		LOGGER.trace("Processing the following lines:\n\t{}", lines);
		if (lines.size() == 0) {
			return;
		}
		// searching for the header in readLines, getting the log level and sending the log message if level >= systm.logLevel
		LogLevelEnum logLevel = null;
		logAnalyzer.setEventSource(systm);
		for(String line : lines) {
			if (!systm.getLogHeaderPatternEnabled()) {
				reportEventIfMatches(logLevel, line);
			} else {
				boolean isHeaderExist = logAnalyzer.process(line);
				if (isHeaderExist) {
					logLevel = LogLevelEnum.convert(logAnalyzer.getLogLevel().toUpperCase());
				}
				LOGGER.trace("isHeaderExist = {}, logLevel = {}, multilineErrorSupported = {}", isHeaderExist, logLevel, multilineErrorSupported);
				// if the multi line log is not supported and the header exists in the line then ... 
				//     ... report it if the current log level is equal to or higher than system's log level
				if (!multilineErrorSupported && isHeaderExist) {
					LOGGER.trace("{} isHigerOrEqual {}", logLevel, systm.getLogLevel());
					if (logLevel != null && logLevel.isHigerOrEqual(systm.getLogLevel())) {
						reportEventIfMatches(logLevel, line);
					}				
				}
				// if multi line is supported then use the log level determined previously
				else if (multilineErrorSupported && logLevel != null) {
					LOGGER.trace("{} isHigerOrEqual {}", logLevel, systm.getLogLevel());
					if (logLevel != null && logLevel.isHigerOrEqual(systm.getLogLevel())) {
						reportEventIfMatches(logLevel, line);
					}
				}
			}
		}
	}


	private void reportEventIfMatches(LogLevelEnum logLevel, String line) {
		String uuid = UUID.randomUUID().toString();
		// before reporting the event check if the line matches one of the Error Detector patterns that relates to the system 
		for(Map<Long,String> rec : systm.getPattern()) {
			String patternText = rec.values().iterator().next();
			LOGGER.debug("\npatternText:\t{}\nline:\t{}", patternText, line);
			if (Pattern.compile(patternText).matcher(line).find()) {
				// send it to the engine
				retryRestClient.reportEvent(line, logLevel, systm.getId(), uuid);
			}
		}
	}

	
	public void setLogAnalyzer(LogAnalyzer logAnalyzer) {
		this.logAnalyzer = logAnalyzer;
	}
	
	public void setStorage(Storage storage) {
		this.storage = storage;
	}

}