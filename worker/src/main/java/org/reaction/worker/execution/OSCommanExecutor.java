package org.reaction.worker.execution;

import org.reaction.common.queue.LimitedList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class OSCommanExecutor {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(OSCommanExecutor.class);
	
	private Boolean successful = null;
	private StringBuilder output = new StringBuilder();
	private String extractedOutput = null;
	private StringBuilder error = new StringBuilder();
	
	private Runtime runtime;
	
	/*
	 * The outputPattern must contain a named capturing group with the name of VALUE_TO_BE_EXTRACTED, eg. 
	 * 			.* [a-zA-Z0-9]* (?<COMMANDOUTPUT>[0-9*]) - [a-zA-Z0-9]* .*
	 */
	public Map<String,Object> execute(String command, String osUser, String outputPattern) throws IOException, InterruptedException {
		// 1. Initialization for command execution
		Process proc = callCommand(command, osUser);
		int waitFor = proc.waitFor();
		LOGGER.info("The status of the command execution is {}", waitFor);
		successful = waitFor == 0 && proc.exitValue() == 0;
		LOGGER.info("The OS command ({}) is executed (OS user={}, output pattern={}). successful={}", command, osUser, outputPattern, successful);
		
		// 2. executing the command and getting the output/error
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
		// read the output from the command
		Pattern pattern = null;
		if (successful && outputPattern != null) { 
			pattern = Pattern.compile(outputPattern);
		}
		String line = null;

		// Only store the last 15 lines in the output!
		LimitedList<String> limitedQueue = new LimitedList<>(15);
		while ((line = stdInput.readLine()) != null) {
			LOGGER.trace("The line to be read from stdInput is \n\t{}", line);
			limitedQueue.add(line);
			if (successful) {
				// if the output pattern is null (no reg expr) then retrieve only the last line
				if (outputPattern == null || outputPattern.trim().isEmpty()) {
					extractedOutput = line;
				} else {
					Matcher matcher = pattern.matcher(line);
					if (matcher.find()) {
						extractedOutput = matcher.group("COMMANDOUTPUT");
					}
				}
			}
		}
		for(String s : limitedQueue) {
			output.append(output.length() == 0 ? "" : "\n").append(s);
		}
		
		// read any errors from the attempted command
		while ((line = stdError.readLine()) != null) {
			LOGGER.trace("The line to be read from stdError is \n\t{}", line);
		    error.append(error.length() == 0 ? "" : "\n")
		         .append(line);
		}
		LOGGER.debug("The standard output:\n{}", output.toString());
		LOGGER.debug("The extracted output:\n{}", extractedOutput == null ? "" : extractedOutput);
		if (!successful) {
			LOGGER.warn("The error output:\n{}", error.toString());
		}
		
		// building the map to be retrieved
		Map<String, Object> result = new HashMap<>();
		result.put("successful", isSuccessful());
		result.put("output", getOutput());
		result.put("extractedValue", getExtractedValue());
		return result;
	}

	
	private Process callCommand(final String command, final String osUser) throws IOException {
		Process proc = null;
		
		if (isUnix()) {
			if (osUser != null && !osUser.isEmpty()) {
				// sudo -u vikhor bash -c "cd /home/vikhor/projects && /opt/scripts/ant.sh container-stop"
				// sudo -u [USER] bash -c "[COMMAND]"
				String[] unixCommand = new String[] {
						"sudo", "-u", osUser, "sh", "-c", command
				};
				LOGGER.info("I will execute the following Unix command:\n{}", Arrays.stream(unixCommand).collect(Collectors.joining(" ")) );
				proc = runtime.exec(unixCommand);
			} else {
				LOGGER.info("I will execute the following Unix command:\n{}", command);
				proc = runtime.exec(command);
			}
		} else {
			// TODO not handled here if osUser is set!!
			String[] cmd = new String[]{"cmd.exe", "/c",command};
			LOGGER.info("I will execute the following non-Unix command: cmd.exe /c {}", command);
			proc = runtime.exec(cmd);
		}
		
		return proc;
	}


    private static boolean isWindows() {
    	String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("win") >= 0);
    }

    private static boolean isMac() {
    	String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("mac") >= 0);
    }

    private static boolean isUnix() {
    	String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0 );
    }

    private static boolean isSolaris() {
    	String os = System.getProperty("os.name").toLowerCase();
        return (os.indexOf("sunos") >= 0);
    }
    
    
    private Boolean isSuccessful() {
		return successful;
	}

    private String getOutput() {
		return isSuccessful() ? 
				output.toString() :
				(error.length() != 0 ? error.toString() : output.toString());
	}

    private String getExtractedValue() {
		return isSuccessful() ? extractedOutput : null;
	}

	public void setRuntime(Runtime runtime) {
		this.runtime = runtime;
	}
	

}