package org.reaction.worker.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.reaction.common.dto.EventSourceWithErrorDetectorPattern;
import org.reaction.worker.log.LogReader;

public class Storage {

	
	private volatile Map<Long, EventSourceWithErrorDetectorPattern> observedSystems = new ConcurrentHashMap<>();
	private volatile Map<Long, LogReader> logReaders = new ConcurrentHashMap<>();
	private volatile Map<String, Long> previousPositionInLogFile = new ConcurrentHashMap<>();
	private volatile String workerType = null;
	
	
	public EventSourceWithErrorDetectorPattern getObservedSystem(EventSourceWithErrorDetectorPattern s) {
		return observedSystems.get(s.getId());
	}

	public synchronized void removeFromObservedSystems(EventSourceWithErrorDetectorPattern s) {
		observedSystems.remove(s.getId());
	}

	public synchronized void addToObservedSystems(EventSourceWithErrorDetectorPattern s) {
		observedSystems.put(s.getId(), s);
	}
	
	
	public synchronized void addToLogReaders(EventSourceWithErrorDetectorPattern s, LogReader logReader) {
		logReaders.put(s.getId(), logReader);
	}
	
	public synchronized void stopLogReader(EventSourceWithErrorDetectorPattern s) {
		logReaders.get(s.getId()).stop();
		logReaders.remove(s.getId());
	}
	
	public synchronized void setWorkerType(String workerType) {
		this.workerType = workerType;
	}

	
	public Map<Long, EventSourceWithErrorDetectorPattern> getObservedSystems() {
		return observedSystems;
	}

	public void setObservedSystems(Map<Long, EventSourceWithErrorDetectorPattern> observedSystems) {
		this.observedSystems = observedSystems;
	}

	public Map<Long, LogReader> getLogReaders() {
		return logReaders;
	}

	public LogReader getLogReader(Long id) {
		return logReaders.get(id);
	}

	public void setLogReaders(Map<Long, LogReader> logReaders) {
		this.logReaders = logReaders;
	}

	public Long getPreviousPositionInLogFile(String logFileLocation) {
		return previousPositionInLogFile.get(logFileLocation);
	}

	public void setPreviousPositionInLogFile(String logFileLocation, Long previousPosition) {
		this.previousPositionInLogFile.put(logFileLocation, previousPosition);
	}

	public String getWorkerType() {
		return workerType;
	}

}