package org.reaction.worker.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.reaction.common.exception.ReactionRuntimeException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ResourceUtils;

public class CredentialLoader {

	
	@Value("${rest_call.credential.file}")
	private String filePath;
	@Value("${host_name}")
	private String host;

	private String privateKey;
	
	
	@PostConstruct
	public void loadCredentail() {
		try(BufferedReader reader = new BufferedReader(new FileReader( ResourceUtils.getFile(filePath) ))) {
			privateKey = reader.readLine();
			if (privateKey == null || privateKey.isEmpty()) {
				throw new ReactionRuntimeException("The private key cannot be found in '"+filePath+"', it has to be in the 2nd line!");
			}
        } catch (IOException e) {
        	throw new ReactionRuntimeException("Exception occured when trying to load the credential file!", e);
        }
	}


	public String getPublicKey() {
		return host;
	}

	public String getPrivateKey() {
		return privateKey;
	}
}
