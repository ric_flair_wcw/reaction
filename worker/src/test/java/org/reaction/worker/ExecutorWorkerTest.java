package org.reaction.worker;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.reaction.worker.call.RestClient;
import org.reaction.worker.execution.OSCommanExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext-test.xml")
public class ExecutorWorkerTest {

	
	@Autowired
	private ApplicationContext applicationContext;

	private ExecutorWorker instance;
	
	@Autowired
	@Qualifier("mockedOSCommanExecutor")
	private OSCommanExecutor mockedOSCommanExecutor;
	
	@Autowired
	@Qualifier("mockedRetryRestClient")
	private RestClient mockedRetryRestClient;
	@Autowired
	@Qualifier("mockedRestClient")
	private RestClient mockedRestClient;
	private ExecutorService mockedExecutorService;

	@Before
	public void setup() {
		// mocking the ExecutorService and implement it so it will execute the runnable directly (no thread will be started)
		// https://stackoverflow.com/a/17218166/1269572
		mockedExecutorService = mock(ExecutorService.class);
	    implementAsDirectExecutor(mockedExecutorService);

	    instance = (ExecutorWorker)applicationContext.getBean("executor");
	    instance.setThreadPool(mockedExecutorService);
	}

	
	@Test
	public void testExecute_successful() throws InterruptedException, IOException, UnrecoverableKeyException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidParameterSpecException {
		String eventLifeId = "11";
		String commandText = "ls -l";
		String outputPattern = ".* [a-zA-Z0-9]* (?<VALUE_TO_BE_EXTRACTED>[0-9*]) - [a-zA-Z0-9]* .*";
		String osUser = "vikhor";
		boolean successful = true;
		String output = "2009-12-12 DEBUG 12 - valami 768576857858757";
		String extractedValue = "12";
		
		// creating the expectations in mocks
		//    for mockedRestClient
		List<Map<String, String>> commands = new ArrayList<>();
		Map<String, String> command = new HashMap<>();
		command.put("eventLifeId", eventLifeId);
		command.put("command", commandText);
		command.put("outputPattern", outputPattern);
		command.put("osUser", osUser);
		commands.add(command);		
		Mockito.when(mockedRestClient.getCommands()).thenReturn(commands);
		//    for mockedOSCommanExecutor
		Map<String, Object> result = new HashMap<>();
		result.put("successful", successful);
		result.put("output", output);
		result.put("extractedValue", extractedValue);
		Mockito.when(mockedOSCommanExecutor.execute(commandText, osUser, outputPattern)).thenReturn(result);

		// calling the method to be tested
		instance.execute();
		
		// checking & verifying
		Mockito.verify(mockedRestClient, Mockito.times(1)).getCommands();
		Mockito.verify(mockedRetryRestClient, Mockito.times(1)).sendResult(eventLifeId, successful, output, extractedValue);
		Mockito.verify(mockedOSCommanExecutor, Mockito.times(1)).execute(commandText, osUser, outputPattern);
		Mockito.verifyNoMoreInteractions(mockedOSCommanExecutor, mockedRetryRestClient);
	}
	

	@Test
	public void testExecute_unsuccessful() throws InterruptedException, IOException, UnrecoverableKeyException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidParameterSpecException {
		String eventLifeId = "11";
		String commandText = "ls -l";
		String outputPattern = ".* [a-zA-Z0-9]* (?<VALUE_TO_BE_EXTRACTED>[0-9*]) - [a-zA-Z0-9]* .*";
		String osUser = "vikhor";
		boolean successful = false;
		String output = "2009-12-12 DEBUG 12 - valami 768576857858757";
		String extractedValue = "12";
		String exceptionText = "valami";
		
		// creating the expectations in mocks
		//    for mockedRestClient
		List<Map<String, String>> commands = new ArrayList<>();
		Map<String, String> command = new HashMap<>();
		command.put("eventLifeId", eventLifeId);
		command.put("command", commandText);
		command.put("outputPattern", outputPattern);
		command.put("osUser", osUser);
		commands.add(command);		
		Mockito.when(mockedRestClient.getCommands()).thenReturn(commands);
		//    for mockedOSCommanExecutor
		Map<String, Object> result = new HashMap<>();
		result.put("successful", successful);
		result.put("output", output);
		result.put("extractedValue", extractedValue);
		Mockito.when(mockedOSCommanExecutor.execute(commandText, osUser, outputPattern)).thenThrow(new RuntimeException(exceptionText));

		// calling the method to be tested
		instance.execute();
		
		// checking & verifying
		Mockito.verify(mockedRestClient, Mockito.times(1)).getCommands();
		Mockito.verify(mockedRetryRestClient, Mockito.times(1)).sendResult(eventLifeId, successful, exceptionText, null);
		Mockito.verify(mockedOSCommanExecutor, Mockito.times(1)).execute(commandText, osUser, outputPattern);
		Mockito.verifyNoMoreInteractions(mockedOSCommanExecutor, mockedRetryRestClient);
	}
	
	
	private void implementAsDirectExecutor(ExecutorService executor) {
	    doAnswer(new Answer<Object>() {
	        public Object answer(InvocationOnMock invocation) throws Exception {
	            ((Runnable) invocation.getArguments()[0]).run();
	            return null;
	        }
	    }).when(executor).submit(any(Runnable.class));
	}
	
	
	@After
	public void clearMocks() {
		Mockito.reset(mockedRetryRestClient);
		Mockito.reset(mockedRestClient);
		Mockito.reset(mockedOSCommanExecutor);
	}
	
}