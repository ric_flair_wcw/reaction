package org.reaction.worker;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.contants.EventSourceHierarchyTypeEnum;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.dto.EventSourceWithErrorDetectorPattern;
import org.reaction.worker.call.RestClient;
import org.reaction.worker.log.LogReader;
import org.reaction.worker.thread.ThreadManager;
import org.reaction.worker.utils.Storage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext-test.xml")
public class ReaderWorkerTest {

	
	private ReaderWorker instance;
	
	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	@Qualifier("mockedRestClient")
	private RestClient mockedRestClient;
	
	@Autowired
	@Qualifier("logReader")
	private LogReader mockedLogReader;

	@Autowired
	@Qualifier("mockedThreadManager")
	private ThreadManager mockedThreadManager;

	@Autowired
	@Qualifier("storage")
	private Storage storage;

	private EventSourceType eventSourceType = EventSourceType.builder().id(1l).code("REACTION-WORKER").build();
	
	@Before
	public void setUp() {
		instance = (ReaderWorker) applicationContext.getBean("reader");
	}
	
	
	// the worker has just started and first refreshing its cache with 2 systems
	@Test
	public void testRefresh_succesful_firstInitialization() throws InterruptedException {
		// creating the expectations in mocks
		//    for mockedRestClient
		Map<Long, String> pattern = new HashMap<>();
		pattern.put(0l, ".*NullpointerException.*");
		List<Map<Long, String>> patterns = new ArrayList<>();
		patterns.add(pattern);
		List<EventSourceWithErrorDetectorPattern> systms = new ArrayList<>();
		systms.add(new EventSourceWithErrorDetectorPattern(0l, null, "Acme",
				"/home/vikhor/work/reaction/src/git/reaction/management_app/reaction_debug.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [[~LOGLEVEL]] [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		systms.add(new EventSourceWithErrorDetectorPattern(1l, null, "Acme2",
				"/local/weblogic/domain/server1/log/soa1.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		Mockito.when(mockedRestClient.getSystms()).thenReturn(systms);
		//    for mockedLogReader
		Mockito.when(mockedLogReader.setSystm(systms.get(0))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(1))).thenReturn(mockedLogReader);
		//    for mockedThreadManager
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		
		// calling the method to be tested
		instance.refresh();
		
		// checking & verifying
		Mockito.verify(mockedRestClient, Mockito.times(1)).getSystms();
		Mockito.verify(mockedLogReader, Mockito.times(2)).setSystm( Mockito.any(EventSourceWithErrorDetectorPattern.class) );
		Mockito.verify(mockedThreadManager, Mockito.times(2)).startNewThread( Mockito.any(Thread.class) );
		assertEquals(systms.get(0).getLogPath(), storage.getObservedSystems().get(0l).getLogPath());
		assertEquals(systms.get(1).getLogPath(), storage.getObservedSystems().get(1l).getLogPath());
		Mockito.verifyNoMoreInteractions(mockedLogReader, mockedRestClient, mockedThreadManager);
	}
	
	
	// 1. the worker contains 3 systms
	// 2. 3 systms arrive from the server (one new one and one changed one that exists already and one unchanged that exists)
	// activity: one existing systm (that the worker had already) has to be replaced, one new systm has to be observed and the other existing systm (that the worker had already) has to be removed
	@Test
	public void testRefresh_succesful_oneChangedOneRemoved() throws InterruptedException {
		// ********************************* initializing the ReaderWorker with 2 systems ************************************
		// creating the expectations in mocks
		//    for mockedRestClient
		Map<Long, String> pattern = new HashMap<>();
		pattern.put(0l, ".*NullpointerException.*");
		List<Map<Long, String>> patterns = new ArrayList<>();
		patterns.add(pattern);
		List<EventSourceWithErrorDetectorPattern> systms = new ArrayList<>();
		systms.add(new EventSourceWithErrorDetectorPattern(0l, null, "Acme",
				"/home/vikhor/work/reaction/src/git/reaction/management_app/reaction_debug.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [[~LOGLEVEL]] [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		systms.add(new EventSourceWithErrorDetectorPattern(1l, null, "Acme2",
				"/local/weblogic/domain/server1/log/soa1.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		systms.add(new EventSourceWithErrorDetectorPattern(2l, null, "Acme3",
				"/local/weblogic/domain/server1/log/soa2.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		Mockito.when(mockedRestClient.getSystms()).thenReturn(systms);
		//    for mockedLogReader
		Mockito.when(mockedLogReader.setSystm(systms.get(0))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(1))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(2))).thenReturn(mockedLogReader);
		//    for mockedThreadManager
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		
		// calling the method to be tested
		instance.refresh();
		
		// ********************************* 3 systems arrive (one new one and one changed one that exists already and one unchanged that exists) ************************************
		// activity: one existing systm (that the worker had already) has to be replaced, one new systm has to be observed and the other existing systm (that the worker had already) has to be removed
		//   id:  first ->   0  second refresh call  ->                        and in the end =>    2  
		//                   1                           1  (changed)                               3
		//                   2                           2  (not changed)                           1
		//                                               3  (new)
		// creating the expectations in mocks
		//    for mockedRestClient
		systms = new ArrayList<>();
		// new system
		systms.add(new EventSourceWithErrorDetectorPattern(3l, null, "Acme4",
				"/websphere/log/server.log", 
				"([~LOGLEVEL]) -- [~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		// existing system (logpath changed)
		systms.add(new EventSourceWithErrorDetectorPattern(1l, null, "Acme2",
				"/local/weblogic/domain/server1/log/soa_1.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		// existing system (not changed)
		systms.add(new EventSourceWithErrorDetectorPattern(2l, null, "Acme3",
				"/local/weblogic/domain/server1/log/soa2.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		Mockito.when(mockedRestClient.getSystms()).thenReturn(systms);
		//    for mockedLogReader
		Mockito.when(mockedLogReader.setSystm(systms.get(0))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(1))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(2))).thenReturn(mockedLogReader);
		Mockito.doNothing().when(mockedLogReader).stop();
		Mockito.doNothing().when(mockedLogReader).stop();
		//    for mockedThreadManager
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		
		// calling the method to be tested
		instance.refresh();
		
		// checking & verifying
		Mockito.verify(mockedRestClient, Mockito.times(2)).getSystms();
		Mockito.verify(mockedLogReader, Mockito.times(5)).setSystm( Mockito.any(EventSourceWithErrorDetectorPattern.class) );
		Mockito.verify(mockedLogReader, Mockito.times(2)).stop();
		Mockito.verify(mockedThreadManager, Mockito.times(5)).startNewThread( Mockito.any(Thread.class) );
		assertEquals(3, storage.getObservedSystems().size());
		// the new system is inserted
		assertEquals("/websphere/log/server.log", storage.getObservedSystems().get(3l).getLogPath());
		// the existing is changed
		assertEquals("/local/weblogic/domain/server1/log/soa_1.log", storage.getObservedSystems().get(1l).getLogPath());
		// the existing is unchanged
		assertEquals("/local/weblogic/domain/server1/log/soa2.log", storage.getObservedSystems().get(2l).getLogPath());
		// the old one is removed
		assertNull(storage.getObservedSystems().get(0l));
		Mockito.verifyNoMoreInteractions(mockedLogReader, mockedRestClient, mockedThreadManager);
	}
	
	
	// 1. the worker contains 3 systms
	// 2. 3 systms arrive from the server (2 changed ones that exists already and one unchanged that exists)
	// activity: one existing systm (the 2nd one that has the worker already) has to be replaced, another one (3rd one) has to be updated 
	//     (the one whose patterns were changed) and the last one (the 1st) should be touched (didn't change)
	@Test
	public void testRefresh_succesful_patternChanged() throws InterruptedException {
		// ********************************* initializing the ReaderWorker with 2 systems ************************************
		// creating the expectations in mocks
		//    for mockedRestClient
		List<EventSourceWithErrorDetectorPattern> systms = new ArrayList<>();
		Map<Long, String> pattern = new HashMap<>();
		pattern.put(0l, ".*NullpointerException.*");
		List<Map<Long, String>> patterns = new ArrayList<>();
		patterns.add(pattern);
		systms.add(new EventSourceWithErrorDetectorPattern(0l, null, "Acme",
				"/home/vikhor/work/reaction/src/git/reaction/management_app/reaction_debug.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [[~LOGLEVEL]] [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		systms.add(new EventSourceWithErrorDetectorPattern(1l, null, "Acme2",
				"/local/weblogic/domain/server1/log/soa1.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		patterns = new ArrayList<>();
		patterns.add(pattern);
		pattern = new HashMap<>();
		pattern.put(1l, ".*OutOfMEmoryException.*");
		patterns.add(pattern);
		systms.add(new EventSourceWithErrorDetectorPattern(2l, null, "Acme3",
				"/local/weblogic/domain/server1/log/soa2.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		Mockito.when(mockedRestClient.getSystms()).thenReturn(systms);
		//    for mockedLogReader
		Mockito.when(mockedLogReader.setSystm(systms.get(0))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(1))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(2))).thenReturn(mockedLogReader);
		//    for mockedThreadManager
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		
		// calling the method to be tested
		instance.refresh();
		
		// ********************************* 3 systems arrive 
		// 	// activity: one existing systm (that the worker had already) has to be replaced, another one has to be updated 
		//     (the one whose patterns were changed) and the other existing systm (that the worker had already) has to be removed
		// creating the expectations in mocks
		//    for mockedRestClient
		systms = new ArrayList<>();
		// existing system (not changed)
		pattern = new HashMap<>();
		pattern.put(0l, ".*NullpointerException.*");
		patterns = new ArrayList<>();
		patterns.add(pattern);
		systms.add(new EventSourceWithErrorDetectorPattern(0l, null, "Acme",
				"/home/vikhor/work/reaction/src/git/reaction/management_app/reaction_debug.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [[~LOGLEVEL]] [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		// existing system (logpath changed)
		systms.add(new EventSourceWithErrorDetectorPattern(1l, null, "Acme2",
				"/local/weblogic/domain/server1/log/soa_1.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		// existing system (pattern is changed)
		patterns = new ArrayList<>();
		pattern = new HashMap<>();
		pattern.put(1l, ".NotEnougMemoryException.*");
		patterns.add(pattern);
		pattern = new HashMap<>();
		pattern.put(0l, ".*NullpointerException.*");
		patterns.add(pattern);
		systms.add(new EventSourceWithErrorDetectorPattern(2l, null, "Acme3",
				"/local/weblogic/domain/server1/log/soa2.log", 
				"[~DATE:yyyy-MM-dd HH:mm:ss,SSS] - [~LOGLEVEL] - [~COMPONENT]:", true,
				null, EventSourceHierarchyTypeEnum.ITEM, LogLevelEnum.ERROR, null,
				null, patterns, eventSourceType, null));
		Mockito.when(mockedRestClient.getSystms()).thenReturn(systms);
		//    for mockedLogReader
		Mockito.when(mockedLogReader.setSystm(systms.get(0))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(1))).thenReturn(mockedLogReader);
		Mockito.when(mockedLogReader.setSystm(systms.get(2))).thenReturn(mockedLogReader);
		Mockito.doNothing().when(mockedLogReader).stop();
		Mockito.doNothing().when(mockedLogReader).stop();
		//    for mockedThreadManager
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		Mockito.doNothing().when(mockedThreadManager).startNewThread(mockedLogReader);
		
		// calling the method to be tested
		instance.refresh();
		
		// checking & verifying
		Mockito.verify(mockedRestClient, Mockito.times(2)).getSystms();
		// 5 = 3 (from the 1st refresh) + 1 (from the 2nd refresh) + 1 (from the update of patterns)
		Mockito.verify(mockedLogReader, Mockito.times(5)).setSystm( Mockito.any(EventSourceWithErrorDetectorPattern.class) );
		Mockito.verify(mockedLogReader, Mockito.times(1)).stop();
		Mockito.verify(mockedThreadManager, Mockito.times(4)).startNewThread( Mockito.any(Thread.class) );
		assertEquals(3, storage.getObservedSystems().size());
		// the new system is inserted
		assertEquals(2, storage.getObservedSystems().get(2l).getPattern().size());
		assertEquals(".*NullpointerException.*", storage.getObservedSystems().get(2l).getPattern().get(0).get(0l));
		assertEquals(".NotEnougMemoryException.*", storage.getObservedSystems().get(2l).getPattern().get(1).get(1l));
		// the existing is changed
		assertEquals("/local/weblogic/domain/server1/log/soa_1.log", storage.getObservedSystems().get(1l).getLogPath());
		// the existing is unchanged
		assertEquals("/home/vikhor/work/reaction/src/git/reaction/management_app/reaction_debug.log", storage.getObservedSystems().get(0l).getLogPath());
		Mockito.verifyNoMoreInteractions(mockedLogReader, mockedRestClient, mockedThreadManager);
	}

	
	@After
	public void clearMocks() {
		Mockito.reset(mockedRestClient);
		Mockito.reset(mockedLogReader);
		Mockito.reset(mockedThreadManager);
		// clean Storage
		storage.setObservedSystems(new HashMap<Long, EventSourceWithErrorDetectorPattern>());
		storage.setLogReaders(new HashMap<Long, LogReader>());
	}
	
}
