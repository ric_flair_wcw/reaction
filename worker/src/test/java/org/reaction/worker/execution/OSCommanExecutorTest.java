package org.reaction.worker.execution;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:applicationContext-test.xml")
public class OSCommanExecutorTest {

	
	@Autowired
	private ApplicationContext applicationContext;

	private OSCommanExecutor instance;
	private String os;
	
	@Autowired
	@Qualifier("mockedRuntime")
	private Runtime mockedRuntime;
	
	@Autowired
	@Qualifier("mockedProcess")
	private Process mockedProcess;
	

	@Before
	public void setUp() {
		instance = (OSCommanExecutor)applicationContext.getBean("osCommandExecutor");
		// store and set the os.name System prop
		os = System.getProperty("os.name");
		System.setProperty("os.name", " nix ");
	}
	
	
	@Test
	public void testExecute_successful() throws IOException, InterruptedException {
		String commandText = "ls -l";
		String outputPattern = ".* [a-zA-Z0-9]* - (?<COMMANDOUTPUT>[ A-Z]*) - [a-zA-Z0-9]* .*";
		String output = "almafa asGF54Gdfh4g - ITISWHAT I NEED - Sgnkgj98DFG8zhHhf sfdsd++";
		String osUser = "vikhor";
		
		// creating the expectations in mocks
		//    for mockedRuntime
		Mockito.when(mockedProcess.waitFor()).thenReturn(0);
		Mockito.when(mockedProcess.exitValue()).thenReturn(0);
		Mockito.when(mockedProcess.getInputStream()).thenReturn(new ByteArrayInputStream(output.getBytes(StandardCharsets.UTF_8)));
		Mockito.when(mockedProcess.getErrorStream()).thenReturn(new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8)));
		String[] cmd = new String[]{"sudo", "-u", osUser, "sh", "-c", commandText};
		Mockito.when(mockedRuntime.exec(cmd)).thenReturn(mockedProcess);
		
		// calling the method to be tested
		Map<String,Object> result = instance.execute(commandText, osUser, outputPattern);

		// checking & verifying
		Mockito.verify(mockedRuntime, Mockito.times(1)).exec(cmd);
		Mockito.verify(mockedProcess, Mockito.times(1)).waitFor();
		Mockito.verify(mockedProcess, Mockito.times(1)).exitValue();
		Mockito.verify(mockedProcess, Mockito.times(1)).getInputStream();
		Mockito.verify(mockedProcess, Mockito.times(1)).getErrorStream();

		assertEquals(output, result.get("output"));
		assertEquals("ITISWHAT I NEED", result.get("extractedValue"));
		assertTrue((Boolean)result.get("successful"));
		Mockito.verifyNoMoreInteractions(mockedRuntime, mockedProcess);
	}
	
	
	@Test
	public void testExecute_successful_emptyOutputPattern() throws IOException, InterruptedException {
		String commandText = "ls -l";
		String outputPattern = null;
		String output = "aaaa\nalmafa asGF54Gdfh4g - ITISWHAT I NEED - Sgnkgj98DFG8zhHhf sfdsd++";
		String osUser = "vikhor";
		
		// creating the expectations in mocks
		//    for mockedRuntime
		Mockito.when(mockedProcess.waitFor()).thenReturn(0);
		Mockito.when(mockedProcess.exitValue()).thenReturn(0);
		Mockito.when(mockedProcess.getInputStream()).thenReturn(new ByteArrayInputStream(output.getBytes(StandardCharsets.UTF_8)));
		Mockito.when(mockedProcess.getErrorStream()).thenReturn(new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8)));
		String[] cmd = new String[]{"sudo", "-u", osUser, "sh", "-c", commandText};
		Mockito.when(mockedRuntime.exec(cmd)).thenReturn(mockedProcess);
		
		// calling the method to be tested
		Map<String,Object> result = instance.execute(commandText, osUser, outputPattern);

		// checking & verifying
		Mockito.verify(mockedRuntime, Mockito.times(1)).exec(cmd);
		Mockito.verify(mockedProcess, Mockito.times(1)).waitFor();
		Mockito.verify(mockedProcess, Mockito.times(1)).exitValue();
		Mockito.verify(mockedProcess, Mockito.times(1)).getInputStream();
		Mockito.verify(mockedProcess, Mockito.times(1)).getErrorStream();

		assertEquals(output, result.get("output"));
		assertEquals("almafa asGF54Gdfh4g - ITISWHAT I NEED - Sgnkgj98DFG8zhHhf sfdsd++", result.get("extractedValue"));
		assertTrue((Boolean)result.get("successful"));
		Mockito.verifyNoMoreInteractions(mockedRuntime, mockedProcess);
	}

	
	@Test
	public void testExecute_unsuccessful() throws IOException, InterruptedException {
		String commandText = "ls -l";
		String outputPattern = ".* [a-zA-Z0-9]* - (?<COMMANDOUTPUT>[ A-Z]*) - [a-zA-Z0-9]* .*";
		String output = "almafa asGF54Gdfh4g - ITISWHAT I NEED - Sgnkgj98DFG8zhHhf sfdsd++";
		String osUser = "vikhor";
		
		// creating the expectations in mocks
		//    for mockedRuntime
		Mockito.when(mockedProcess.waitFor()).thenReturn(1);
		Mockito.when(mockedProcess.exitValue()).thenReturn(0);
		Mockito.when(mockedProcess.getInputStream()).thenReturn(new ByteArrayInputStream("".getBytes(StandardCharsets.UTF_8)));
		Mockito.when(mockedProcess.getErrorStream()).thenReturn(new ByteArrayInputStream(output.getBytes(StandardCharsets.UTF_8)));
		String[] cmd = new String[]{"sudo", "-u", osUser, "sh", "-c", commandText};
		Mockito.when(mockedRuntime.exec(cmd)).thenReturn(mockedProcess);
		
		// calling the method to be tested
		Map<String,Object> result = instance.execute(commandText, osUser, outputPattern);

		// checking & verifying
		Mockito.verify(mockedRuntime, Mockito.times(1)).exec(cmd);
		Mockito.verify(mockedProcess, Mockito.times(1)).waitFor();
		Mockito.verify(mockedProcess, Mockito.times(1)).getInputStream();
		Mockito.verify(mockedProcess, Mockito.times(1)).getErrorStream();

		assertEquals(output, result.get("output"));
		assertNull(result.get("extractedValue"));
		assertFalse((Boolean)result.get("successful"));
		Mockito.verifyNoMoreInteractions(mockedRuntime, mockedProcess);
	}

	
	@After
	public void cleanUp() {
		System.setProperty("os.name", os);
		Mockito.reset(mockedRuntime);
		Mockito.reset(mockedProcess);
	}

}
