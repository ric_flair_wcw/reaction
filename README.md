# Reaction
## automatic incident detector and resolver

Detecting incidents (e.g. app crashing due to memory leak) in business applications and resolving it automatically by predefined execution flow (chain of OS commands).    
Please visit the [Reaction website](https://reaction-engine.bitbucket.io).    

### Getting Started

The [Getting Started](https://reaction-engine.bitbucket.io/getting-started.html) page gives some insight how to start using the Reaction components.    
If you want to know more how Reaction can help to remedy an IT incident please open the [Use Case](https://reaction-engine.bitbucket.io/use-case.html) page.

### Installing

Please open the [Installation](https://reaction-engine.bitbucket.iog/installation.html) page to get more information how the Reaction components have to be installed.
The following components have to be deployed to make the system work:

* [worker](http://reaction-engine.org/documentation.html#2_1_2_1_on_Linux)    
* [engine](http://reaction-engine.org/documentation.html#2_2_5_How_to_install)    
* [management web application](http://reaction-engine.org/documentation.html#2_3_2_How_to_install)    

### Quick set up

On the [Quick set-up](https://reaction-engine.bitbucket.io/quick-setup.html) page instructions can be found how to set up the Reaction components to fix an incident.


### Documentation

Please visit the [Documentation](https://reaction-engine.bitbucket.io/documentation.html) page.    
Useful information / tricks can be found on the [Reaction blog](https://reactionisourfriend.blogspot.hu/).    

### License

This project is licensed under the [AGPL-3.0](https://opensource.org/licenses/AGPL-3.0) license.
