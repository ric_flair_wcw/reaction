package org.reaction.common.queue;

import java.util.LinkedList;


public class LimitedList<E> extends LinkedList<E> {

	private static final long serialVersionUID = 8066840506161190137L;

	private int limit;

    public LimitedList(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean add(E o) {
		if (o == null) {
			throw new IllegalArgumentException("Null cannot be added to LimitedList!");
		}
    	
        super.add(o);
        while (size() > limit) { 
        	super.remove(); 
        }
        return true;
    }
}