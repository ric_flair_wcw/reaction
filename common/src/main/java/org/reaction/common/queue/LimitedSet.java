package org.reaction.common.queue;

import java.util.LinkedList;


public class LimitedSet<E> extends LinkedList<E> {

	private static final long serialVersionUID = 8066840506161190137L;

	private int limit;

    public LimitedSet(int limit) {
        this.limit = limit;
    }

    @Override
    public boolean add(E o) {
    	// I don't want to synchronise it as it is not vital to me that there won't be duplications...
    	if (!this.contains(o)) {
	        super.add(o);
	        while (size() > limit) { super.remove(); }
	        return true;
    	} else {
    		return false;
    	}
    }
}