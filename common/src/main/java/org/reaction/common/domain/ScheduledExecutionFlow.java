package org.reaction.common.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScheduledExecutionFlow {

	
	private Long id;
	private String name;
	private String reason;
	private String cronExpression;
	private Boolean scheduledAlready;
	private String errorAtLastRun;
	private ExecutionFlow executionFlow;

	@Override
	public String toString() {
		return "ScheduledExecutionFlow{" +
				"id=" + id +
				", name='" + name + '\'' +
				", executionFlow=" + executionFlow +
				'}';
	}
}
