package org.reaction.common.domain;


import java.util.Calendar;

import org.reaction.common.Utils;
import org.reaction.common.contants.EventInitiatorEnum;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.contants.LogLevelEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Event {

	
	private Long id;
	private String identifier;
	private LogLevelEnum logLevel;
	private EventInitiatorEnum initiatedBy;
	private EventStatusEnum status;
	private Long eventSourceId;
	private Calendar startDate;
	private Calendar endDate;
	private Calendar firstEventArrived;
	private Integer multipleEventsCounter;
	private String reason;
	private ExecutionFlow executionFlow;
	private ErrorDetector errorDetector;
	private ScheduledExecutionFlow scheduledExecutionFlow;
	private String payloadValues;
	private String queryParameters;
	private String message;
	
	@Override
	public String toString() {
		return String.format("Event(id=%d, identifier=%s, logLevel=%s, initiatedBy=%s, status=%s, payloadValues=%s, queryParameters=%s, eventSourceId=%d, startDate=%s, endDate=%s, firstEventArrived=%s, " +
						"multipleEventsCounter=%d, reason=%s, message=%s, executionFlow=%s, errorDetector=%s, scheduledExecutionFlow=%s)",
				id, identifier, logLevel, initiatedBy, status, payloadValues, queryParameters, eventSourceId, Utils.formatCalendar(startDate), Utils.formatCalendar(endDate), Utils.formatCalendar(firstEventArrived),
				multipleEventsCounter, reason, message, executionFlow, errorDetector, scheduledExecutionFlow);
	}
	
	 
}
