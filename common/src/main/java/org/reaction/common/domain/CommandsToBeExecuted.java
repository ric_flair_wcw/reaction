package org.reaction.common.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommandsToBeExecuted {

	
	private Long id;
	private Boolean executed;
	private EventLife eventLife;

}
