package org.reaction.common.domain.task;

import lombok.*;
import org.reaction.common.domain.SuperCommand;

@Data
@Builder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class LoopTask extends SuperTask {

    private String loopValueList1;
    private String loopSeparator1;
    private String loopRequiresEncryption1;
    private String loopValueList2;
    private String loopSeparator2;
    private String loopRequiresEncryption2;
    private String loopValueList3;
    private String loopSeparator3;
    private String loopRequiresEncryption3;
    private String loopValueList4;
    private String loopSeparator4;
    private String loopRequiresEncryption4;
    private SuperCommand firstCommandInLoop;

}
