package org.reaction.common.domain;

import org.reaction.common.exception.ReactionExecutionFlowException;

public interface Command {

	public void execute(EventLife eventLife) throws ReactionExecutionFlowException;
	
}
