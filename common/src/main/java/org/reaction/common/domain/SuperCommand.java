package org.reaction.common.domain;

import org.reaction.common.domain.task.SuperTask;
import org.reaction.common.exception.ReactionExecutionFlowException;


public abstract class SuperCommand implements Command {

	
	protected EventLife eventLife;
	
	
	public void execute(EventLife eventLife) throws ReactionExecutionFlowException {
		// storing the eventLifeId
		setEventLife(eventLife);
		// calling the _execute
		_execute(eventLife);
	}
	
	protected abstract void _execute(EventLife eventLife) throws ReactionExecutionFlowException;
	
	public abstract void next(final Long _taskId) throws ReactionExecutionFlowException;
	
	public abstract SuperTask getSuperTask();

	public void setEventLife(EventLife eventLife) {
		this.eventLife = eventLife;
	}
	
}
