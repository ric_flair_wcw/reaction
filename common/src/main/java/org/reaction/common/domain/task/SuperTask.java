package org.reaction.common.domain.task;

import org.reaction.common.domain.ExecutionFlow;

import lombok.Data;

@Data
public abstract class SuperTask {

	
	private Long id;
	private Integer order;
	private ExecutionFlow executionFlow;
	private Long primaryTaskId;
	private Long secondaryTaskId;
	private String name;
	
}
