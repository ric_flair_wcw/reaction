package org.reaction.common.domain.task;

import org.reaction.common.domain.SuperCommand;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class InternalIfElseTask extends SuperTask {

	private String ifExpression;
	private SuperCommand firstCommandInIf;
	private SuperCommand firstCommandInElse;

}
