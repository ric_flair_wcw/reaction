package org.reaction.common.domain;

import java.io.IOException;
import java.time.Clock;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.reaction.common.exception.ReactionRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


public class MaintenanceWindow {

	
	// yeah, it is lame but not sure how to test the scheduled integration tests otherwise (e.g. Scheduled_StopHermes_NoError_dueToMaintenanceWindow)
	public static boolean isAlwaysInMaintenanceWindow = false;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MaintenanceWindow.class);
	
	private Map<String,List<String>> maintenanceWindow = null;
	private String maintenanceWindowText = null;
	private Clock clock;
	
	
	public MaintenanceWindow() {
	}
	
	public MaintenanceWindow(String maintenanceWindowJson, Clock clock) {
		this.maintenanceWindowText = maintenanceWindowJson;
		if (maintenanceWindowJson != null && maintenanceWindowJson.length() > 0) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				maintenanceWindow = mapper.readValue(maintenanceWindowJson, new TypeReference<Map<String,List<String>>>(){});
				maintenanceWindow = maintenanceWindow.isEmpty() ? null : maintenanceWindow;
			} catch (IOException e) {
				throw new ReactionRuntimeException("The value in the maintenanceWindow column is not a valid JSON!", e);
			}
		}
		this.clock = clock;
	}	
	
	
	/*
		{ "Sun": ["14:00-04:00"],
		  "Mon": ["23:00-04:00"],
		  "Tue": ["23:00-04:00"],
		  "Wed": ["23:00-04:00"],
		  "Thu": ["23:00-04:00"],
		  "Fri": ["23:00-04:00"],
		  "Sat": ["20:00-04:00"] }
		
		{ "Sun": ["14:00-04:00"],
		  "Tue": ["01:00-04:00"],
		  "Wed": ["01:00-04:00"],
		  "Thu": ["01:00-04:00"],
		  "Fri": ["01:00-04:00"],
		  "Sat": ["01:00-04:00", "20:00-04:00"] }
	 */
	public boolean isInMaintenanceWindow(Integer _executionTime) {
		if (isAlwaysInMaintenanceWindow) {
			return true;
		}
		LOGGER.trace("maintenanceWindow: {}", maintenanceWindow);
		int executionTime = _executionTime == null ? 0 : _executionTime;
		if (maintenanceWindow == null) {
			return true;
		}
		LocalDateTime now = LocalDateTime.ofInstant(clock.instant(), clock.getZone());
		List<String> periods = maintenanceWindow.get(now.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US));
		
		// if periods == null then it might mean that:
		//   - on this day there isn't a maintenance period
		//   - the previous day covers this day too (see above the Sunday in the 2nd example)
		boolean checkOverflow = false;
		if (periods == null) {
			// check if the previous day covers this day too
			checkOverflow = true;
			LocalDateTime yesterday = now.minusDays(1);
			periods = maintenanceWindow.get(yesterday.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US));
		} 
		// the window is open for the whole day
		else if (periods.contains("00:00-24:00")) {
			return true;
		}
		LOGGER.trace("periods: {}", periods);
		
		// if now is Tuesday 01:00 and there maintenance window is 'Tuesday:19:00-23:00' and 'Monday:22:00-03:00'
		//   then first will get the 'Tuesday:19:00-23:00' but it will retrieve false which is wrong => the previous day has to be checked too
		//   (and at this sample it will be true)
		boolean b = checkPeriods(now, periods, checkOverflow, executionTime);
		LOGGER.trace("checkPeriods: {}", b);
		if (periods == null) {
			return false;
		} else if (/*!checkOverflow && */!b) {
			// get the previous day as it might have an overflow period (like 22:00-06:00)
			checkOverflow = true;
			LocalDateTime yesterday = now.minusDays(1);
			periods = maintenanceWindow.get(yesterday.getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.US));
			LOGGER.trace("periods: {}", periods);
			// if the periods contain 00:00-24:00 then it is open only for the previous day ie. no overflowing time range => return false;
			if (periods == null || periods.contains("00:00-24:00")) {
				b = false;
			} else {
				b = checkPeriods(now, periods, checkOverflow, executionTime);
			}
			LOGGER.trace("checkPeriods: {}", b);
		} 
		return b;
	}


	public Calendar nextInMaintenanceWindow(Integer _executionTime) {
		if (maintenanceWindow == null || maintenanceWindow.isEmpty()) {
			return null;
		}
		int executionTime = _executionTime == null ? 0 : _executionTime;
		Instant instant = clock.instant();
		ZoneId zone = clock.getZone();
		LocalDateTime now = LocalDateTime.ofInstant(instant, zone);
		LOGGER.trace("now: {}, clock.instant(): {}, clock.getZone(): {}", now, instant, zone);
		DayOfWeek dayOfWeek = now.getDayOfWeek();
		LOGGER.trace("dayOfWeek: {}", dayOfWeek);
		List<String> periods = null;
		boolean found = false;
		int plusDay = 0;
		do {
			periods = maintenanceWindow.get(dayOfWeek.plus(plusDay).getDisplayName(TextStyle.SHORT, Locale.US));
			LOGGER.trace("periods: {}", periods);
			
			if (periods != null) {
				// find the date in the 'periods' that is older than the current date
				for(String period : periods.stream().sorted( (s1,s2) -> toInt(s1).compareTo(toInt(s2))).collect(Collectors.toList())) {
					String[] twoPeriods = period.split("-");
					String[] hourMinStart = twoPeriods[0].split(":");
					LOGGER.trace("hourMinStart: {}", Arrays.toString(hourMinStart));
					LocalDateTime maintWindowStart = LocalDateTime.of(now.toLocalDate(), LocalTime.of(Integer.parseInt(hourMinStart[0]), Integer.parseInt(hourMinStart[1]), 0, 0)).plusDays(plusDay);
					LOGGER.trace("maintWindowStart: {}", maintWindowStart);
					// if the current maintenance window period is after the current time (maint.window: 01:00-03:00 and now is 00:45)
					if (maintWindowStart.isAfter(now)) {
						LOGGER.trace("maintWindowStart.isAfter(now)   - {}", new GregorianCalendar(maintWindowStart.getYear(), maintWindowStart.getMonthValue()-1, maintWindowStart.getDayOfMonth(), maintWindowStart.getHour(), maintWindowStart.getMinute(), 0));
						return new GregorianCalendar(maintWindowStart.getYear(), maintWindowStart.getMonthValue()-1, maintWindowStart.getDayOfMonth(), maintWindowStart.getHour(), maintWindowStart.getMinute(), 0);
					}
					// if the current date is in the maintenance window (and event fits if execution time is specified): maint.window: 01:00-03:00 and now is 01:45 and the exectuion time is 30 min
					else {
						String[] hourMinEnd = twoPeriods[1].split(":");
						LOGGER.trace("else  -  hourMinEnd: {}", Arrays.toString(hourMinEnd));
						LocalDateTime maintWindowEnd = LocalDateTime.of(now.toLocalDate(), LocalTime.of(Integer.parseInt(hourMinEnd[0]), Integer.parseInt(hourMinEnd[1]), 0, 0)).plusDays(plusDay);
						LOGGER.trace("maintWindowEnd: {}", maintWindowEnd);
						if (maintWindowStart.isBefore(now) && maintWindowEnd.isAfter(now.plusMinutes(executionTime))) {
							LOGGER.trace("maintWindowStart.isBefore(now) && maintWindowEnd.isAfter(now.plusMinutes(executionTime))   - {}", new GregorianCalendar(now.getYear(), now.getMonthValue()-1, now.getDayOfMonth(), now.getHour(), now.getMinute(), 0));
							return new GregorianCalendar(now.getYear(), now.getMonthValue()-1, now.getDayOfMonth(), now.getHour(), now.getMinute(), 0);
						}
					}
				}
			}
			
			plusDay++;
			if (!found && now.getDayOfWeek().equals( dayOfWeek.plus(plusDay) )) {
				throw new ReactionRuntimeException("Cannot find the next maintenance window!");
			}
		} while(!found);
		
		LOGGER.trace("return null");
		return null;
	}

	
	private boolean checkPeriods(LocalDateTime now, List<String> periods, boolean checkOverflow, int executionTime) {
		if (periods == null) {
			return false;
		}
		// check if the prev day (yesterday) really contains a period that overflows to the next day (today) , i.e. end of period < start of period
		for(String period : periods.stream().sorted( (s1,s2) -> toInt(s1).compareTo(toInt(s2))).collect(Collectors.toList()) ) {
			LocalDateTime[] startAndEndDateTime = getStartAndEnd(period, now, checkOverflow);
			// the executionTime has to be deducted from the end of the period (e.g. period is 20:00 - 23:30; execution time is 60 min and the event occured at 23:10 => the end period will be 22:30 so it cannot be started)
			// now >= startAndEndDateTime[0]    AND    now < (startAndEndDateTime[1] + 1 minute)
			LocalDateTime end = startAndEndDateTime[1].minusMinutes(executionTime).plusMinutes(1);
			if ( (now.isAfter(startAndEndDateTime[0]) || now.isEqual(startAndEndDateTime[0]) )
					&& now.isBefore(end) ) {
				return true;
			}
		}
		// there isn't maintenance window for this day
		return false;
	}

	
	private Integer toInt(String period) {
		try {
			return Integer.valueOf(period.replace(":", "").replace("-", ""));
		} catch(Exception e) {
			throw new ReactionRuntimeException("The value '" + period + "' is not a valid time period (01:00-04:00)!", e);
		}
	}
	

	private LocalDateTime[] getStartAndEnd(String period, LocalDateTime now, boolean checkOverflow) {
		try {
			LocalDateTime[] localDateTimes = new LocalDateTime[2];
			String[] twoPeriods = period.split("-");
			String[] hourMin = twoPeriods[0].split(":");
			int plusMinusDay = checkOverflow ? -1 : 0;
			localDateTimes[0] = LocalDateTime.of(now.toLocalDate().plusDays(plusMinusDay), 
					                             LocalTime.of(Integer.parseInt(hourMin[0]), 
					                                          Integer.parseInt(hourMin[1]), 
					                                          0, 
					                                          0)
					                             );
			hourMin = twoPeriods[1].split(":");
			localDateTimes[1] = LocalDateTime.of(now.toLocalDate().plusDays(
														(isOverFlowed(period) ? 1 : 0) + plusMinusDay), 
					                             LocalTime.of(Integer.parseInt(hourMin[0]), 
					                                          Integer.parseInt(hourMin[1]), 
					                                          0, 
					                                          0)
					                             );
			return localDateTimes;
		} catch(Exception e) {
			throw new ReactionRuntimeException("The value '" + period + "' is not a valid time period (01:00-04:00)!", e);
		}
	}


	// 01:00-04:00
	private boolean isOverFlowed(String period) {
		try {
			String[] twoPeriods = period.split("-");
			if (Integer.parseInt(twoPeriods[0].replace(":", "")) > Integer.parseInt(twoPeriods[1].replace(":", ""))) {
				return true;
			} else {
				return false;
			}
		} catch(Exception e) {
			throw new ReactionRuntimeException("The value '" + period + "' is not a valid time period (e.g. 01:00-04:00)!", e);
		}
	}

	@Override
	public String toString() {
		return maintenanceWindow == null ? null : maintenanceWindow.toString();
	}
	
	public boolean isEmpty() {
		return maintenanceWindow == null;
	}

	public String getMaintenanceWindowText() {
		return maintenanceWindowText;
	}

	
}