package org.reaction.common.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Calendar;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkerStatus {

	private Long id;
	private String host;
	private Calendar refreshEventSources;
	private Calendar reportEvent;
	private Calendar checkCommands;
	private Calendar sendCommandResult;
	
}
