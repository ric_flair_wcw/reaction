package org.reaction.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.reaction.common.contants.EventSourceHierarchyTypeEnum;
import org.reaction.common.contants.LogLevelEnum;

import java.util.Objects;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonIgnoreProperties({ "maintenanceWindow" })
public class EventSource {


	protected Long id;
	protected String name;
	protected String host;
	protected String logPath;
	protected String logHeaderPattern;
	protected Boolean logHeaderPatternEnabled;
	private String description;
	protected EventSourceHierarchyTypeEnum type;
	protected LogLevelEnum logLevel;
	private MaintenanceWindow maintenanceWindow;
	private EventSource parent;
	private EventSourceType eventSourceType;
	private String identifiers;

	@Override
	public String toString() {
		return "EventSource{" +
				"id=" + id +
				", name='" + name + '\'' +
				", type=" + type +
				", eventSourceType=" + eventSourceType +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		EventSource that = (EventSource) o;
		return id.equals(that.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}