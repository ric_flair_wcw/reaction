package org.reaction.common.domain.task;

import lombok.*;
import org.reaction.common.contants.SshAuthTypeEnum;

@Data
@Builder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class CommandUsingSSHTask extends SuperTask {

	
	private String command;
	private String osUser;
	private SshAuthTypeEnum sshAuthType;
	private String sshKeyPath;
	private String osPassword;
	private Boolean isPwdSetManually;
	private String host;
	private Integer executionTimeout;
	private String outputPattern;
	
}