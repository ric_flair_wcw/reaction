package org.reaction.common.domain;

import java.util.Calendar;

import org.reaction.common.contants.EventStatusEnum;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventLifeHistory {

	private Calendar date;
	private EventStatusEnum eventStatus;
	
}
