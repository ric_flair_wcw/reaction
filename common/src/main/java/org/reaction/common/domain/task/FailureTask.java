package org.reaction.common.domain.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
public class FailureTask extends SuperTask {

}
