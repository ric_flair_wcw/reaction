package org.reaction.common.domain.task;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class EmailSendingTask extends SuperTask {

	private List<String> recipients;
	private String subject;
	private String content;
	
}
