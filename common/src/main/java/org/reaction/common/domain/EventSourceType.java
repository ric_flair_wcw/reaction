package org.reaction.common.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.reaction.common.contants.AuthenticationTypeEnum;
import org.reaction.common.contants.DataTypeEnum;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EventSourceType {

	private Long id;
	private String name;
	private String code;
	private DataTypeEnum dataType;
	private String namespaces;
	private String pathToIdentifiers;
	private Boolean enforceSsl;
	private AuthenticationTypeEnum authenticationType;
	private String secret;
	private String user;

	@Override
	public String toString() {
		return "EventSourceType{" +
				"id=" + id +
				", name='" + name + '\'' +
				", code='" + code + '\'' +
				'}';
	}
}
