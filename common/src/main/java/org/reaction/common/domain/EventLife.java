package org.reaction.common.domain;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Data;
import org.reaction.common.Utils;
import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EventStatusEnum;
import org.reaction.common.exception.ReactionRuntimeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Data
@Builder
public class EventLife {

	private Long id;
	private Calendar eventDate;
	private Integer order;
	private String output;
	private String extractedValue;
	private Boolean extCommandSuccesful;
	private EventStatusEnum eventStatus;
	private String history;
	private String errorMessage;
	private String byWhom;
	private Integer loopIndex;
	private Event event;
	private SuperCommand task;

	public List<EventLifeHistory> convertHistory() {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(Constants.getDateFormat());
		try {
			return  history == null ?
					new ArrayList<>() : 
					mapper.readValue(history, new TypeReference<List<EventLifeHistory>>() {});
		} catch (IOException e) {
			throw new ReactionRuntimeException("Error during converting the EventLife.history from String to JSON!", e);
		}
	}

	
	// adding the date and eventStatus of the current EventLife as they will be changed after the call addToHistory (see for example IfElseTaskCommand._execute(...))
	public void addToHistory() {
		// get the current history
		List<EventLifeHistory> history = convertHistory();
		// add the date and eventStatus of the current EventLife to it
		history.add(EventLifeHistory.builder().date(this.eventDate)
											  .eventStatus(this.eventStatus).build());
		// serialize it and put it back to history property		
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(Constants.getDateFormat());
		try {
			this.history = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(history);
		} catch (IOException e) {
			throw new ReactionRuntimeException("Error during converting the EventLife.history from String to JSON!", e);
		}
	}

	
	@Override
	public String toString() {
		return String.format("EventLife(id=%d, date=%s, order=%d, output=%s, extractedValue=%s, extCommandSuccessful=%s, eventStatus=%s, history=%s, errorMessage=%s, byWhom=%s, " +
						"loopIndex=%d, event=%s, task=%s)",
				id, Utils.formatCalendar(eventDate), order, output, extractedValue, extCommandSuccesful, eventStatus, history, errorMessage, byWhom,
				loopIndex, event, task);
	}
}