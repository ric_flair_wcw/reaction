package org.reaction.common.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorDetector {

	
	private Long id;
	private String name;
	private String messagePattern;
	private Boolean activated;
	private Boolean confirmationNeeded;	
	private Integer multipleEventsCnt;
	private Integer multipleEventsTimeframe;
	private ExecutionFlow executionFlow;
	private EventSource eventSource;
	private String identifiers;

	@Override
	public String toString() {
		return "ErrorDetector{" +
				"id=" + id +
				", name='" + name + '\'' +
				", executionFlow=" + executionFlow +
				", eventSource=" + eventSource +
				'}';
	}
}
