package org.reaction.common.domain;

import lombok.Builder;
import lombok.Data;
import org.reaction.common.contants.ExecutionFlowStatusEnum;

import java.util.List;

@Data
@Builder
public class ExecutionFlow {

	
	private Long id;
	private String name;
	private Integer timeBetweenExecutions;
	private Integer executionTime;
	private ExecutionFlowStatusEnum status;
	private List<String> accessGroups;
	private List<String> errorMailSendingRecipients;
	private List<String> startMailSendingRecipients;

	@Override
	public String toString() {
		return "ExecutionFlow{" +
				"id=" + id +
				", name='" + name + '\'' +
				", status=" + status +
				'}';
	}
}