package org.reaction.common.domain.task;

import lombok.*;

@Data
@Builder
@EqualsAndHashCode(callSuper=true)
@NoArgsConstructor
@AllArgsConstructor
public class CommandByWorkerTask extends SuperTask {

	
	private String command;
	private String osUser;
	private String host;
	private String outputPattern;
	
}