package org.reaction.common.contants;

public enum LogHeaderPatternTypeEnum {

	DATE("DATE", null),
	LOGLEVEL("LOGLEVEL", "[a-zA-Z]+"),
//	COMPONENT("COMPONENT", "[a-zA-Z0-9 \\:\\-\\.,;=\\-\\/]*"),
//	ERRORCODE("ERRORCODE", "[a-zA-Z0-9 \\-]*"),
//	MESSAGE("MESSAGE", "[a-zA-Z0-9 \\-\\.,]+"),
//	CLASS("CLASS", "[a-zA-Z0-9 \\-\\.]*"),
	UNKNOWN("UNKNOWN", null);
	
	private String value = null;
	private String regExp = null;

	private LogHeaderPatternTypeEnum(String value, String regExp) {
		this.value = value;
		this.regExp = regExp;
	}
	
	public String getValue() {
		return value;
	}

	public String getRegExp() {
		return regExp;
	}
	
}
