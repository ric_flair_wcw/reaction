package org.reaction.common.contants;

public enum TaskEnum {

	IF_ELSE("If-else task"),
	COMMAND_BY_WORKER("Command executed by the Reaction Worker task"),
	COMMAND_USING_SSH("Command using SSH"),
	EMAIL_SENDING("Email sending task"),
	FAILURE("Failing the flow task"),
	LOOP("Loop");
	
	private String desc;
	
	private TaskEnum(String desc) {
		this.desc = desc;
	}
	
	public String getDescription() {
		return desc;
	}
	
}