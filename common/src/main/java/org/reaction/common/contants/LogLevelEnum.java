package org.reaction.common.contants;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public enum LogLevelEnum {

	TRACE(100, "FINER", "FINEST"), 
	DEBUG(200, "CONFIG", "FINE" ), 
	INFO(300), 
	WARN(400, "WARNING"),
	ERROR(500, "SEVERE"), 
	FATAL(600);
	
	private int order;
	private Set<String> others;
	
	private LogLevelEnum(int order, String... others) {
		this.order = order;
		this.others = new HashSet<>(Arrays.asList(others));
	}
	
	public boolean isHigerOrEqual(LogLevelEnum e) {
		return this.order >= e.order;
	}
	
	public static LogLevelEnum convert(String logLevelText) {
		if (logLevelText == null) {
			return null;
		}
		try {
			return LogLevelEnum.valueOf(logLevelText);
		} catch(Exception exception) {
			List<LogLevelEnum> found = Arrays.asList(LogLevelEnum.values()).stream().filter(e -> e.others.contains(logLevelText)).collect(Collectors.toList());
			if (found.size() == 1) {
				return found.get(0);
			} else {
				return null;
			}
		}
	}
}
