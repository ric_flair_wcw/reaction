package org.reaction.common.contants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Constants {

	
	public static final String APPLICATION_CONTEXT_PATH = "applicationContext.xml";
	public static final String MAIL_FROM = "Reaction-admin@NOREPLY";
	public static final String HTTP_HEADER_SECURITY_SETTING = "Reaction-Security-Encryption-Setting";
	public static final String HTTP_HEADER_SECURITY_KEY = "Reaction-Security-Encryption-Key";
	public static final String HTTP_HEADER_SECURITY_HOST = "Reaction-Security-Encryption-Host";
	public static final String KEY_GENERATION_ALGORYTHM = "AES";
	public static final String KEY_GENERATION_TRANSFORMATION = "AES/CBC/PKCS5Padding";
	public static final String HTTP_HEADER_SECURITY_IV = "Reaction-Security-Encryption-IV";
	public static final String HTTP_HEADER_SECURITY_KEY_SIZE = "Reaction-Security-Encryption-KeySize";

	
	private static final ThreadLocal<DateFormat> localDateFormat = new ThreadLocal<DateFormat>() {
		@Override
		protected DateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		}
	};
	
	public static DateFormat getDateFormat() {
		return localDateFormat.get();
	}
}
