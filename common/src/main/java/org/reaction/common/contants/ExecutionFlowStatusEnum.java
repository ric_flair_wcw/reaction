package org.reaction.common.contants;

public enum ExecutionFlowStatusEnum {

	INVALID,
	VALID,
	FROZEN;
	
}
