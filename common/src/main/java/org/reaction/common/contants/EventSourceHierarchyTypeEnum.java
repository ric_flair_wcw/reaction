package org.reaction.common.contants;

public enum EventSourceHierarchyTypeEnum {

	GROUP("Group"),
	ITEM("Item");
	
	private String desc;
	
	private EventSourceHierarchyTypeEnum(String desc) {
		this.desc = desc;
	}
	
	public String getDesc() {
		return desc;
	}
	
}
