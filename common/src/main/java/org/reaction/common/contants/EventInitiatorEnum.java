package org.reaction.common.contants;

public enum EventInitiatorEnum {

	BY_LOG,
	BY_EXTERNAL_SOURCE,
	MANUALLY,
	BY_SCHEDULER;
	
}
