package org.reaction.common.contants;

public enum EncryptionTypeEnum {

	CREDENTIAL_BASED(1), 
	CERTIFICATE_BASED(2), 
	NONE(0);

	private int code;

	private EncryptionTypeEnum(int code) {
		this.code = code;
	}

	public int getCode() {
		return this.code;
	}

	public static EncryptionTypeEnum findByCode(int c) {
		EncryptionTypeEnum[] encryptionTypes = EncryptionTypeEnum.values();
		for (EncryptionTypeEnum encryptionType : encryptionTypes) {
			if (encryptionType.code == c) {
				return encryptionType;
			}
		}
		return null;
	}
	
}