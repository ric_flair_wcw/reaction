package org.reaction.common.contants;

public enum AuthenticationTypeEnum {

    NONE,
    SECRET,
    BASIC_AUTH;

}
