package org.reaction.common.contants;

public enum PermissionEnum {

	CAN_USE_APPROVAL("can_use_approval");
	
	private String codename;
	
	private PermissionEnum(String codename) {
		this.codename = codename;
	}
	
	public String getCodename() {
		return codename;
	}
	
}
