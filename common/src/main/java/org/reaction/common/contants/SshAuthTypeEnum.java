package org.reaction.common.contants;

public enum SshAuthTypeEnum {

    USER_PASSWORD,
    SSH_KEY;

}
