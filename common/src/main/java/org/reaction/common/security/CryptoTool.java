package org.reaction.common.security;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.reaction.common.contants.Constants;
import org.reaction.common.contants.EncryptionTypeEnum;
import org.reaction.common.exception.ReactionRuntimeException;
import org.reaction.common.exception.ReactionRuntimeSecurityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class CryptoTool {

	private static final Logger LOGGER = LoggerFactory.getLogger(CryptoTool.class);
	
	private EncryptionEnvironmentLoader encryptionEnvironmentLoader;

	
	public Map<String, byte[]> encrypt(byte[] raw, char[] password, Integer keySize, String trustStoreAlias) throws UnrecoverableKeyException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException, InvalidParameterSpecException {
		return encrypt(raw, encryptionEnvironmentLoader.getEncryptionType(), password, keySize, trustStoreAlias);
	}

	
	public Map<String, byte[]> encrypt(byte[] raw, EncryptionTypeEnum type, char[] password, Integer keySize, String trustStoreAlias) throws IllegalBlockSizeException, BadPaddingException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException {
		Map<String, byte[]> result = new HashMap<>();
		Cipher cipher;
		if (raw.length == 0) {
			result.put("data", raw);
		}
		switch (type) {
			case NONE:
				result.put("data", raw);
				break;

			case CERTIFICATE_BASED:
				if (keySize == null) {
					throw new ReactionRuntimeSecurityException("The HTTP header " + Constants.HTTP_HEADER_SECURITY_KEY_SIZE + " cannot be found in the request!");
				}
				if (trustStoreAlias == null || trustStoreAlias.isEmpty()) {
					throw new ReactionRuntimeSecurityException("The HTTP header " + Constants.HTTP_HEADER_SECURITY_HOST + " cannot be found in the request!");
				}
				if (encryptionEnvironmentLoader.getTruststore() == null || encryptionEnvironmentLoader.getKeystore() == null) {
					throw new ReactionRuntimeException("The keystore / truststore configuration is not defined in the config file!");
				}
				
				// with clientTrustStore and "server" alias

				// RSA and AES serve two different purposes. AES is a fast algorithm, suited to encrypt a whole conversation. But it has a problem: how to decide which key to use between two parties without the other ones knowing it?
				// RSA can be the solution to this. If Every participant has the public RSA key of every other participant, anyone can start an encrypted communication with anyone else (by using the other participant's public key) and 
				//      decide of a secret AES key to use. Once the AES key is decided, the rest of the conversation can be encrypted using AES. To prove that to participant B that it's really A which wants to tals to him, a digital signature can be used. 
		
				// The RSA algorithm can only encrypt data that has a maximum byte length of the RSA key length in bits divided with eight minus eleven padding bytes, i.e. number of maximum bytes = key length in bits / 8 - 11.
				// So basicly you divide the key length with 8 -11(if you have padding). For example if you have a 2048bit key you can encrypt 2048/8 = 256 bytes (- 11 bytes if you have padding). So, either use a larger key or you encrypt the data with a symmetric key, and encrypt that key with rsa (which is the recommended approach).
				// That will require you to:
				//			1. generate a symmetric key
				//			2. Encrypt the data with the symmetric key
				//			3. Encrypt the symmetric key with rsa
				//			4. send the encrypted key and the data
				//			5. Decrypt the encrypted symmetric key with rsa
				//			6. decrypt the data with the symmetric key
				// 
				
				// 1. generate symmetric key
				KeyGenerator generator = KeyGenerator.getInstance(Constants.KEY_GENERATION_ALGORYTHM);
				generator.init(keySize);
				SecretKey encryptionKey = generator.generateKey();
				LOGGER.trace("encryptionKey: {}   -   {}", encryptionKey.toString(), Arrays.toString(encryptionKey.getEncoded()));
				// 2. Encrypt the data with the symmetric key
				cipher = Cipher.getInstance(Constants.KEY_GENERATION_TRANSFORMATION);
				cipher.init(Cipher.ENCRYPT_MODE, encryptionKey);
				result.put("data", Base64.getEncoder().encode(cipher.doFinal(raw)));
				// 3. Encrypt the symmetric key with public key
				//     getting the public key
				Key key = null;
				if (encryptionEnvironmentLoader.getTruststore().isCertificateEntry(trustStoreAlias)) {
					Certificate cert = encryptionEnvironmentLoader.getTruststore().getCertificate(trustStoreAlias);
					key = cert.getPublicKey();
				} else {
					key = encryptionEnvironmentLoader.getTruststore().getKey(trustStoreAlias, encryptionEnvironmentLoader.getTruststorePassword().toCharArray());
				}
				result.put("IV", Base64.getEncoder().encode(cipher.getIV()));
				LOGGER.trace("IV: {}", Arrays.toString( cipher.getIV() ));
				//     initializing the cipher with the public key
				cipher = Cipher.getInstance(encryptionEnvironmentLoader.getTransformation());
				cipher.init(Cipher.ENCRYPT_MODE, key);
				//     encryption
				result.put("key", Base64.getEncoder().encode(cipher.doFinal(encryptionKey.getEncoded())));
				LOGGER.trace("data: {}                 ---                  key: {}", Arrays.toString(result.get("data")), Arrays.toString(result.get("key")));
				break;

			case CREDENTIAL_BASED:
				// generate key from the password
				MessageDigest digester = MessageDigest.getInstance("MD5");
				for (int i = 0; i < password.length; i++) {
					digester.update((byte) password[i]);
				}
				byte[] passwordData = digester.digest();

				Key secretkey = new SecretKeySpec(passwordData, Constants.KEY_GENERATION_ALGORYTHM);				
				
				// initializing the cipher with the this key
				cipher = Cipher.getInstance(Constants.KEY_GENERATION_TRANSFORMATION);
				cipher.init(Cipher.ENCRYPT_MODE, secretkey);
				
				result.put("IV", Base64.getEncoder().encode(cipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV()));
				LOGGER.trace("IV: {}", Arrays.toString( result.get("IV") ));
				result.put("data", Base64.getEncoder().encode(cipher.doFinal(raw)));
				LOGGER.trace("data: {}", Arrays.toString(result.get("data")));
				break;

			default:
				throw new ReactionRuntimeSecurityException("Ok, I will be surprised if this exception is thrown like ever...");
		}
		return result;		
	}

	
	public <T> T decrypt(String encrypted, TypeReference<T> responseType, byte[] secretKeyBytes, byte[] parameterIV, char[] password) throws UnrecoverableKeyException, InvalidKeyException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, InvalidAlgorithmParameterException, InvalidKeySpecException {
		// getting the byte array and convert them to String
		byte[] decryptedAsBytes = decrypt(encrypted, 
				                          encryptionEnvironmentLoader.getEncryptionType(), 
				                          secretKeyBytes, 
				                          parameterIV,
				                          password);
		String jsonString = new String( decryptedAsBytes, StandardCharsets.UTF_8 );
		// converting the json string to java beans
		return new ObjectMapper().readValue(jsonString, responseType);
	}
	
	
	public byte[] decrypt(String encrypted, EncryptionTypeEnum encryptionType, byte[] secretKeyBytes, byte[] parameterIV, char[] password) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidAlgorithmParameterException {
		if (encrypted == null || encrypted.isEmpty()) {
			return new byte[0];
		}

		Cipher cipher;
		switch (encryptionType) {
			// if NONE then the decrypt is not called
//			case NONE:
			case CERTIFICATE_BASED:
				if (secretKeyBytes == null) {
					throw new ReactionRuntimeSecurityException("The HTTP header " + Constants.HTTP_HEADER_SECURITY_KEY + " cannot be found in the request!");
				}
				if (parameterIV == null) {
					throw new ReactionRuntimeSecurityException("The HTTP header " + Constants.HTTP_HEADER_SECURITY_IV + " cannot be found in the request!");
				}
				if (encryptionEnvironmentLoader.getTruststore() == null || encryptionEnvironmentLoader.getKeystore() == null) {
					throw new ReactionRuntimeException("The keystore / truststore configuration is not defined in the config file!");
				}

				cipher = Cipher.getInstance(encryptionEnvironmentLoader.getTransformation());
				//	1. Decrypt the encrypted symmetric key with private key
				//     getting the private key
				Key privateKey = encryptionEnvironmentLoader.getKeystore().getKey(encryptionEnvironmentLoader.getKeystoreAlias(), encryptionEnvironmentLoader.getKeystorePassword().toCharArray());
				//     initializing the cipher
				cipher.init(Cipher.DECRYPT_MODE, privateKey);
				//     decryption
				byte[] decryptedSecretKeyBytes = cipher.doFinal(Base64.getDecoder().decode(secretKeyBytes));
				SecretKey encryptionKey = new SecretKeySpec(decryptedSecretKeyBytes, 0, decryptedSecretKeyBytes.length, Constants.KEY_GENERATION_ALGORYTHM);
				LOGGER.trace("encryptionKey: {}   -   {}", encryptionKey.toString(), Arrays.toString(encryptionKey.getEncoded()));
				//	2. decrypt the data with the symmetric key
				cipher = Cipher.getInstance(Constants.KEY_GENERATION_TRANSFORMATION);
				cipher.init(Cipher.DECRYPT_MODE, 
						    encryptionKey, 
						    new IvParameterSpec(Base64.getDecoder().decode(parameterIV)));
				return cipher.doFinal(Base64.getDecoder().decode(encrypted.toString().getBytes(StandardCharsets.UTF_8)));

			case CREDENTIAL_BASED:
				if (parameterIV == null) {
					throw new ReactionRuntimeSecurityException("The HTTP header " + Constants.HTTP_HEADER_SECURITY_IV + " cannot be found in the request!");
				}
				
				// generate key from the password
				MessageDigest digester = MessageDigest.getInstance("MD5");
				for (int i = 0; i < password.length; i++) {
					digester.update((byte) password[i]);
				}
				byte[] passwordData = digester.digest();

				Key secretkey = new SecretKeySpec(passwordData, Constants.KEY_GENERATION_ALGORYTHM);				
				
				// initializing the cipher with the this key
				cipher = Cipher.getInstance(Constants.KEY_GENERATION_TRANSFORMATION);
				cipher.init(Cipher.DECRYPT_MODE, secretkey, new IvParameterSpec(Base64.getDecoder().decode(parameterIV)));
				return cipher.doFinal(Base64.getDecoder().decode(encrypted.toString().getBytes(StandardCharsets.UTF_8)));
				
			default:
				throw new ReactionRuntimeSecurityException("Ok, I will be surprised if this exception is thrown like ever...");
		}
	}

	
	public void setEncryptionEnvironmentLoader(EncryptionEnvironmentLoader encryptionEnvironmentLoader) {
		this.encryptionEnvironmentLoader = encryptionEnvironmentLoader;
	}

	
}