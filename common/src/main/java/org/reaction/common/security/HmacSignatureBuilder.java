package org.reaction.common.security;


import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Objects;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HmacSignatureBuilder {

    private static final String HMAC_SHA_512 = "HmacSHA512";
    private static final byte DELIMITER = '\n';
    private static final Logger LOGGER = LoggerFactory.getLogger(HmacSignatureBuilder.class);
    		
    private String algorithm = HMAC_SHA_512;
    private String method;
    private String endpoint;
    private String nonce;
    private String apiKey;
    private byte[] apiSecret;
    private byte[] payload;
    private String date;
    private String contentType;

    public String getAlgorithm() {
        return algorithm;
    }

    public HmacSignatureBuilder algorithm(String algorithm) {
        this.algorithm = algorithm;
        LOGGER.trace("algorithm: " + algorithm + ",  algorithm - bytes: " + Arrays.toString(algorithm.getBytes()));
        return this;
    }

    public HmacSignatureBuilder endpoint(String endpoint) {
        this.endpoint = endpoint;
        LOGGER.trace("endpoint: " + endpoint + ",  endpoint - bytes: " + Arrays.toString(endpoint.getBytes()));
        return this;
    }

    public HmacSignatureBuilder apiKey(String key) {
        this.apiKey = key;
        LOGGER.trace("apiKey: " + apiKey + ",  apiKey - bytes: " + Arrays.toString(apiKey.getBytes()));
        return this;
    }

    public HmacSignatureBuilder method(String method) {
        this.method = method;
        LOGGER.trace("method: " + method + ",  method - bytes: " + Arrays.toString(method.getBytes()));
        return this;
    }

    public HmacSignatureBuilder contentType(String contentType) {
        this.contentType = contentType;
        LOGGER.trace("contentType: " + contentType + ",  contentType - bytes: " + Arrays.toString(contentType.getBytes()));
        return this;
    }

    public HmacSignatureBuilder date(String dateString) {
        this.date = dateString;
        LOGGER.trace("date: " + date + ",  date - bytes: " + Arrays.toString(date.getBytes()));
        return this;
    }


    public HmacSignatureBuilder nonce(String nonce) {
        this.nonce = nonce;
        LOGGER.trace("nonce: " + nonce + ",  nonce - bytes: " + Arrays.toString(nonce.getBytes()));
        return this;
    }

    public HmacSignatureBuilder apiSecret(String secretString) {
        this.apiSecret = secretString.getBytes(StandardCharsets.UTF_8);
        LOGGER.trace("apiSecret:" + secretString + ",  apiSecret - bytes: " + Arrays.toString(apiSecret));
        return this;
    }

    public HmacSignatureBuilder payload(byte[] payloadBytes) {
        this.payload = payloadBytes;
        LOGGER.trace("payload - bytes: {}  -  {}", Arrays.toString(payload), new String(payloadBytes));
        return this;
    }

    public byte[] build() {
        Objects.requireNonNull(algorithm, "algorithm");
        Objects.requireNonNull(endpoint, "endpoint");
        Objects.requireNonNull(method, "method");
        Objects.requireNonNull(contentType, "contentType");
        Objects.requireNonNull(apiKey, "apiKey");
        Objects.requireNonNull(date, "date");
        Objects.requireNonNull(payload, "payload");
        try {
            final Mac digest = Mac.getInstance(algorithm);
            SecretKeySpec secretKey = new SecretKeySpec(apiSecret, algorithm);
            digest.init(secretKey);
            digest.update(method.getBytes(StandardCharsets.UTF_8));
            digest.update(DELIMITER);
            digest.update(endpoint.getBytes(StandardCharsets.UTF_8));
            digest.update(DELIMITER);
            digest.update(contentType.getBytes(StandardCharsets.UTF_8));
            digest.update(DELIMITER);
            digest.update(apiKey.getBytes(StandardCharsets.UTF_8));
            digest.update(DELIMITER);
            if (nonce != null) {
                digest.update(nonce.getBytes(StandardCharsets.UTF_8));
                digest.update(DELIMITER);
            }
            digest.update(date.getBytes(StandardCharsets.UTF_8));
            digest.update(DELIMITER);
            if (payload.length != 0) {
	            digest.update(payload);
	            digest.update(DELIMITER);
        	}
            final byte[] signatureBytes = digest.doFinal();
            digest.reset();
            return signatureBytes;
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Can't create signature: " + e.getMessage(), e);
        }
    }

    public boolean isHashEquals(byte[] expectedSignature) {
        final byte[] signature = build();
        return MessageDigest.isEqual(signature, expectedSignature);
    }

    public String buildAsHexString() {
        return DatatypeConverter.printHexBinary(build());
    }

    public String buildAsBase64String() {
        return DatatypeConverter.printBase64Binary(build());
    }
}