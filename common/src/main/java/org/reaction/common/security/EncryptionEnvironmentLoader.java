package org.reaction.common.security;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.annotation.PostConstruct;

import org.reaction.common.contants.EncryptionTypeEnum;
import org.reaction.common.exception.ReactionRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

public class EncryptionEnvironmentLoader {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionEnvironmentLoader.class);

	private String keystoreLocation;
	private String keystorePassword;
	private String keystoreType;
	private String keystoreAlias;
	
	private String truststoreLocation;
	private String truststorePassword;
	private String truststoreType;
	private String truststoreAlias;
	
	private String transformation;
	private EncryptionTypeEnum encryptionType;
	private String keySize;
	
	private String usedBy;
	
	private KeyStore keystore;
	private KeyStore truststore;

	@PostConstruct
	public void load() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {
		LOGGER.trace("keystoreLocation: {}, is null? {}", keystoreLocation, keystoreLocation==null);
		LOGGER.trace("keystorePassword: {}, is null? {}", keystorePassword, keystorePassword==null);
		LOGGER.trace("keystoreType: {}, is null? {}", keystoreType, keystoreType==null);
		LOGGER.trace("keystoreAlias: {}, is null? {}", keystoreAlias, keystoreAlias==null);
		LOGGER.trace("truststoreLocation: {}, is null? {}", truststoreLocation, truststoreLocation==null);
		LOGGER.trace("truststorePassword: {}, is null? {}", truststorePassword, truststorePassword==null);
		LOGGER.trace("truststoreType: {}, is null? {}", truststoreType, truststoreType==null);
		LOGGER.trace("truststoreAlias: {}, is null? {}", truststoreAlias, truststoreAlias==null);
		LOGGER.trace("transformation: {}, is null? {}", transformation, transformation==null);
		LOGGER.trace("encryptionType: {}, is null? {}", encryptionType, encryptionType==null);
		LOGGER.trace("keySize: {}, is null? {}", keySize, keySize==null);
		LOGGER.trace("usedBy: {}, is null? {}", usedBy, usedBy==null);
		if (usedBy.equals("worker")) {
			if (encryptionType != null && (encryptionType.equals(EncryptionTypeEnum.CERTIFICATE_BASED) || encryptionType.equals(EncryptionTypeEnum.CREDENTIAL_BASED))) {
				if (keySize.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED or CREDENTIAL_BASED but the key_size is not defined!");
				}
				if (transformation.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED or CREDENTIAL_BASED but the transformation is not defined!");
				}
			}
			if (encryptionType != null && encryptionType.equals(EncryptionTypeEnum.CERTIFICATE_BASED)) {
				if (keystoreLocation.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the keystore.location is not defined!");
				}
				if (keystorePassword.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the keystore.password is not defined!");
				}
				if (keystoreType.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the keystore.type is not defined!");
				}
				if (keystoreAlias.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the keystore.alias is not defined!");
				}
				if (truststoreLocation.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the truststore.location is not defined!");
				}
				if (truststorePassword.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the truststore.password is not defined!");
				}
				if (truststoreType.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the truststore.type is not defined!");
				}
				if (truststoreAlias.isEmpty()) {
					throw new ReactionRuntimeException("The encryption type is CERTIFICATE_BASED but the truststore.alias is not defined!");
				}
				
				// loading the keystore
				keystore = KeyStore.getInstance(keystoreType);
				FileInputStream streamKs = new FileInputStream(ResourceUtils.getFile(keystoreLocation));
				keystore.load(streamKs, keystorePassword.toCharArray());
		
				// loading the truststore
				truststore = KeyStore.getInstance(truststoreType);
				FileInputStream streamTs = new FileInputStream(ResourceUtils.getFile(truststoreLocation));
				truststore.load(streamTs, truststorePassword.toCharArray());
			}
		}
		else if (usedBy.equals("engine")) {
			if (transformation.isEmpty()) {
				LOGGER.warn("WARNING! The transformation is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			}			
			if (keystorePassword.isEmpty()) {
				LOGGER.warn("WARNING! The keystore.password is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			}
			if (keystoreType.isEmpty()) {
				LOGGER.warn("WARNING! The keystore.type is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			}
			if (keystoreAlias.isEmpty()) {
				LOGGER.warn("WARNING! The keystore.alias is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			}
			if (keystoreLocation.isEmpty()) {
				LOGGER.warn("WARNING! The keystore.location is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			} else {
				// loading the keystore
				keystore = KeyStore.getInstance(keystoreType);
				FileInputStream streamKs = new FileInputStream(ResourceUtils.getFile(keystoreLocation));
				keystore.load(streamKs, keystorePassword.toCharArray());
			}
			if (truststorePassword.isEmpty()) {
				LOGGER.warn("WARNING! The truststore.password is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			}
			if (truststoreType.isEmpty()) {
				LOGGER.warn("WARNING! The truststore.type is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			}
			if (truststoreLocation.isEmpty()) {
				LOGGER.warn("WARNING! The truststore.location is not defined in the config file, the CERTIFICATE_BASED encrypted communication won't be possible with the worker.");
			} else {
				// loading the truststore
				truststore = KeyStore.getInstance(truststoreType);
				FileInputStream streamTs = new FileInputStream(ResourceUtils.getFile(truststoreLocation));
				truststore.load(streamTs, truststorePassword.toCharArray());
			}
		} else {
			throw new ReactionRuntimeException("The value '" + usedBy + "' is not a valid usedBy.");
		}
	}

	public KeyStore getKeystore() {
		return keystore;
	}

	public KeyStore getTruststore() {
		return truststore;
	}

	public void setKeystoreLocation(String keystoreLocation) {
		this.keystoreLocation = keystoreLocation;
	}

	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}

	public void setKeystoreType(String keystoreType) {
		this.keystoreType = keystoreType;
	}

	public void setTruststoreLocation(String truststoreLocation) {
		this.truststoreLocation = truststoreLocation;
	}

	public void setTruststorePassword(String truststorePassword) {
		this.truststorePassword = truststorePassword;
	}

	public void setTruststoreType(String truststoreType) {
		this.truststoreType = truststoreType;
	}

	public String getKeystoreAlias() {
		return keystoreAlias;
	}

	public void setKeystoreAlias(String keystoreAlias) {
		this.keystoreAlias = keystoreAlias;
	}

	public String getTruststoreAlias() {
		return truststoreAlias;
	}

	public void setTruststoreAlias(String truststoreAlias) {
		this.truststoreAlias = truststoreAlias;
	}

	public String getKeystorePassword() {
		return keystorePassword;
	}

	public String getTruststorePassword() {
		return truststorePassword;
	}

	public String getTransformation() {
		return transformation;
	}

	public void setTransformation(String transformation) {
		this.transformation = transformation;
	}

	public EncryptionTypeEnum getEncryptionType() {
		return encryptionType;
	}

	public void setEncryptionType(String encryptionType) {
		this.encryptionType = EncryptionTypeEnum.valueOf(encryptionType);
	}

	public String getKeySize() {
		return keySize;
	}

	public void setKeySize(String keySize) {
		this.keySize = keySize;
	}

	public String getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(String usedBy) {
		this.usedBy = usedBy;
	}

}
