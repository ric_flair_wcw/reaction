package org.reaction.common.exception;

public class ReactionExecutionFlowException extends Exception {

	private static final long serialVersionUID = -4233591723291011226L;

	public ReactionExecutionFlowException(String message) {
		super(message);
	}

	public ReactionExecutionFlowException(String message, Throwable t) {
		super(message, t);
	}
	
}
