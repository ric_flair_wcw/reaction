package org.reaction.common.exception;

public class ReactionRuntimeException extends RuntimeException {

	private static final long serialVersionUID = -6103342562750893312L;

	public ReactionRuntimeException(Throwable t) {
		super(t);
	}

	public ReactionRuntimeException(String message) {
		super(message);
	}

	public ReactionRuntimeException(String message, Throwable t) {
		super(message, t);
	}
	
}
