package org.reaction.common.exception;

public class ReactionRuntimeSecurityException extends RuntimeException {

	private static final long serialVersionUID = -5861131480696454350L;

	public ReactionRuntimeSecurityException(Throwable t) {
		super(t);
	}

	public ReactionRuntimeSecurityException(String message) {
		super(message);
	}

	public ReactionRuntimeSecurityException(String message, Throwable t) {
		super(message, t);
	}
	
}
