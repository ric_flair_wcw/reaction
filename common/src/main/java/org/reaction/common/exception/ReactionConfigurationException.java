package org.reaction.common.exception;

public class ReactionConfigurationException extends Exception {

	private static final long serialVersionUID = 2123842966225045158L;

	public ReactionConfigurationException(String message) {
		super(message);
	}
	
	public ReactionConfigurationException(String message, Throwable t) {
		super(message, t);
	}

}
