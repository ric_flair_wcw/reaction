package org.reaction.common;

import java.util.Calendar;

import org.reaction.common.contants.Constants;

public class Utils {

	public static String formatCalendar(Calendar calendar) {
		return calendar == null ? null : Constants.getDateFormat().format(calendar.getTime());
	}
}
