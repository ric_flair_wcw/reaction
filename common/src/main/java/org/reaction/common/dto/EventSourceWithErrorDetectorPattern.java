package org.reaction.common.dto;

import java.util.List;
import java.util.Map;

import org.reaction.common.contants.LogLevelEnum;
import org.reaction.common.contants.EventSourceHierarchyTypeEnum;
import org.reaction.common.domain.EventSourceType;
import org.reaction.common.domain.MaintenanceWindow;
import org.reaction.common.domain.EventSource;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString(callSuper = true)
public class EventSourceWithErrorDetectorPattern extends EventSource {
	

	private List<Map<Long,String>> pattern;
	
	public EventSourceWithErrorDetectorPattern(Long id, String name, String host, String logPath, String logHeaderPattern, Boolean logHeaderPatternEnabled,
											   String description, EventSourceHierarchyTypeEnum type, LogLevelEnum logLevel, MaintenanceWindow maintenanceWindow, EventSource parent, List<Map<Long, String>> pattern,
											   EventSourceType eventSourceType, String identifiers) {
		super(id, name, host, logPath, logHeaderPattern, logHeaderPatternEnabled, description, type, logLevel, maintenanceWindow, parent, eventSourceType, identifiers);
		this.pattern = pattern;
	}	
	
}