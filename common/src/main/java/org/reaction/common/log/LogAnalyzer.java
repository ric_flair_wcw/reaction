package org.reaction.common.log;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.reaction.common.contants.LogHeaderPatternTypeEnum;
import org.reaction.common.domain.EventSource;
import org.reaction.common.exception.ReactionRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class LogAnalyzer {

	
	private static final String BRACKET_PATTERN = "\\[\\~(?:[^\\[\\~\\]]|(?<=\\\\)\\])*(?<!\\\\)\\]";
	private static final Logger LOGGER = LoggerFactory.getLogger(LogAnalyzer.class);
	
	// -> SystemEntity
	private String logLevel = null;
	private LocalDateTime dateTime = null;
	private String message = null;
	private List<String> unknown = null;
	
	private EventSource eventSource = null;
	
	
	public boolean process(String line) {
		// if the log header pattern is disabled
		if (!eventSource.getLogHeaderPatternEnabled()) {
			return true;
		}
		
		final String logHeaderPattern = eventSource.getLogHeaderPattern();
		// 1. check if the line contains the header
		// <23-Jun-2014 15:10:34 o'clock BST> <Error> <oracle.soa.adapter> <BEA-000000> <JCABinding=>  CaseDataBamUtil:LPCaseWorkflowTasksDO_BamAdapter [ writetoBAM_ptt::write
		// ...
		// >
		// <[~DATE:dd-MMM-yyyy HH:mm:ss] o'clock BST> <[~LOGLEVEL]> <[~UNKNOWN:a-z\.]> <[~UNKNOWN:A-Z\-0-9]> <[~UNKNOWN:A-Za-z]
		// 1.1. find a good enough candidate -> replace the [~...] to .* and match the new pattern
		//    I have to escape the reg expr special characters -> as I replace to .* (replaceAll(BRACKET_PATTERN, ".*")) so before the replace I will escape the . and 
		//            * characters because in the escape method I wont escape the ' and *
		String logHeaderRegExpr = escapeSpecChars(logHeaderPattern.replaceAll("[\\.\\*]", "\\\\$0")
				                                                  .replaceAll(BRACKET_PATTERN, ".*"));
		LOGGER.debug("\nProcessing the following line:\n\t{}\nThe reg expression of the header is \n\t{}\nThe log header pattern is\n\t{}", line, logHeaderRegExpr, logHeaderPattern);
		Pattern pattern = Pattern.compile( logHeaderRegExpr + ".*" );
		if (pattern.matcher(line).matches()) {
			LOGGER.trace("I might have found the header in the line...");
			// 1.2 finding the markers in the pattern and extracting the values from the line 
			pattern = Pattern.compile(BRACKET_PATTERN);
			Matcher matcher = pattern.matcher(logHeaderPattern);
			// the characters that are in front of the current marker (e.g. DATE) in the logHeaderPattern
			String startingChars = null;
			int startingInd = 0;
			int trailingInd = 0;
			boolean go = true;
			if (!matcher.find()) {
				go = false;
			}
			Map<String, Object> extractedValues = new HashMap<>();
			LOGGER.trace("go = {}", go);
			String builtRegExp = escapeExternalChars(logHeaderPattern);
			List<String> orderOfKeys = new ArrayList<>();
			while (go) {
				// 1.2.1. find the info marker (e.g. [~LOGLEVEL]) in the logHeaderPattern and extract the characters that are before the marker in the pattern and the other characters that after it
				//     e.g. for DATE: starting chars : '<', trailing chars: ' o'clock BST> <'
				// finding the starting chars
				trailingInd = matcher.start();
				startingChars = logHeaderPattern.substring(startingInd, trailingInd);
				startingInd = matcher.end();
				// getting the key from logHeaderPattern (e.g. DATE)
				String key = logHeaderPattern.substring(trailingInd+2, startingInd-1);
				LOGGER.trace("trailingInd = '{}', startingChars = '{}', startingInd = '{}', key = '{}'", trailingInd, startingChars, startingInd, key);
				boolean likelyRealKey = false;
				for (LogHeaderPatternTypeEnum e : Arrays.asList(LogHeaderPatternTypeEnum.values())) {
					if (key.equals(e.name()) || key.startsWith(LogHeaderPatternTypeEnum.DATE.name()+":") || key.startsWith(LogHeaderPatternTypeEnum.UNKNOWN.name()+":")) {
						likelyRealKey = true;
						break;
					}
				}
				LOGGER.trace("likelyRealKey = {}", likelyRealKey);
				if (likelyRealKey) {
					// a reg exp will be created from the log header pattern, for example:
					//		log header pattern:		[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~UNKNOWN:a-z]] ([~UNKNOWN:a-zA-z: \-0-9])
					//		built reg exp:			^([0-9][0-9][0-9][0-9]\-[a-zA-Z0-9][a-zA-Z0-9]\-[0-9][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9],[0-9][0-9][0-9]) ([a-zA-Z]+)  \[([a-z]+)\] \(([a-zA-z: \-0-9]+)\) 
					//		sample log line:		2017-03-01 16:01:58,027 INFO  [stdout] (Hibernate Search: collectionsloader-3)         this_.DICT_ENTRY_ID as DICT9_27_0_
					// see the main method
					
					builtRegExp = builtRegExp.replace("[~"+key+"]", "("+buildRegExpOfKey(key)+")");
					orderOfKeys.add(key);
					
					// finding the trailing chars
					if (matcher.find()) {
						trailingInd = matcher.start();
					} else {
						trailingInd = logHeaderPattern.length();
						go = false;
					}
				}
			}
			
			// extract the values with the build reg exp
			LOGGER.debug("The values will be extracted from the line.\n\tline: {}\n\tbuiltRegExp: {}", line, builtRegExp);
			pattern = Pattern.compile( builtRegExp );
			matcher = pattern.matcher(line);
			if (matcher.find()) {
				LOGGER.trace("found");
				for(int i=0; i < orderOfKeys.size(); i++) {
					try {
						if (orderOfKeys.get(i).startsWith(LogHeaderPatternTypeEnum.UNKNOWN.toString())) {
							List<String> uknownList = (List<String>) extractedValues.get( LogHeaderPatternTypeEnum.UNKNOWN.toString() );
							if (uknownList == null) {
								uknownList = new ArrayList<>();
								extractedValues.put( LogHeaderPatternTypeEnum.UNKNOWN.toString() , uknownList);
							}
							uknownList.add(matcher.group(i+1));
						} else {
							extractedValues.put(orderOfKeys.get(i), matcher.group(i+1));
						}
					} catch(IndexOutOfBoundsException e) {
						LOGGER.info("The line below is rejected as it doesn't contain the log header.\n  line: {}\n  log header:{}", line, logHeaderPattern);
						return false;
					}
				}
			}
			
			// setting the value to a property of LogAnalyser
			extractedValues.keySet().stream().forEach(k -> setValue(k, extractedValues.get(k)));
			LOGGER.trace("return {}. extractedValues = '{}'", extractedValues.keySet().size() > 0, extractedValues);
			return extractedValues.keySet().size() > 0;
		}
		LOGGER.trace("return false");
		return false;
	}


	// We have the following log header pattern:
	//			[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~UNKNOWN:a-z]] ([~UNKNOWN:\\]a-zA-z: \\-0-9])
	// all the chars that are outside the pattern groups (a pattern group is eg. '[~DATE:yyyy-MM-dd HH:mm:ss,SSS]' or '[~COMPONENT]' ) have to be escaped, like
	//			'  ['  and  '] ('  etc.
	private String escapeExternalChars(String logHeaderPattern) {
		// first find the start and end positions of the pattern groups
		String regExp ="([\\[][~][A-Z]+[:]?(?:[^\\[\\~\\]]|(?<=\\\\)\\])*[\\]])";
		Pattern pattern = Pattern.compile( regExp );
		Matcher matcher = pattern.matcher(logHeaderPattern);
		Set<Integer> positions = new HashSet<>();
		int pos = 0;
		if (matcher.find()) {
			while (matcher.find(pos)) {
				for(pos=matcher.start(); pos<matcher.end(); pos++) {
					positions.add(pos);
				}
			}
		} else {
			throw new RuntimeException("The log header pattern doesn't container pattern group!");
		}
		// escape the characters that are not in the pattern group
		StringBuffer newLogHeaderPattern = new StringBuffer();
		for(int i = 0; i < logHeaderPattern.length(); i++) {
			// it doesn't have to be escaped
			if (positions.contains(i)) {
				newLogHeaderPattern.append(logHeaderPattern.charAt(i));
			} 
			// it has to be
			else {
				newLogHeaderPattern.append( escapeSpecChars2(Character.toString(logHeaderPattern.charAt(i))) );
			}
		};
		
		return newLogHeaderPattern.length() != 0 ? "^" + newLogHeaderPattern.toString() : newLogHeaderPattern.toString();
	}


	String buildRegExpOfKey(String key) {
		if (key.startsWith(LogHeaderPatternTypeEnum.DATE.name() + ":")) {
			// checking if the date format is valid
			String dateFormatText = key.replace(LogHeaderPatternTypeEnum.DATE.name() + ":", "");
			try {
				DateTimeFormatter.ofPattern(dateFormatText);
			} catch(IllegalArgumentException iae) {
				LOGGER.warn("The dateformate string ({}) in DATE is not valid! The line won't be taken into account.", dateFormatText);
				LOGGER.warn("The exception is", iae);
			}
			// looping through the date format text and 
			//   - if the examined char is in a valid date format pattern then loop while the same char is found (e.g. __ yyyy MMM    -> the first loop starts from the 1st occurrence of 'y' and stops at the last occurrence of 'y') and
			//       replace it with the reg exp from DateFormatPatternEnum
			//   - if it is not then just append the character to the result
			String dateFormatCharacters = "MEyDHkKhmdFuwWSsaGzZ";
			StringBuffer result = new StringBuffer();
			int start = 0;
			
			if (dateFormatText.length() > 0) {
				for(int ind = 0; ind < dateFormatText.length(); ind++) {
					char c = dateFormatText.charAt(ind);
					if (dateFormatCharacters.indexOf(c) == -1) {
						result.append(c);
					} else {
						start = ind;
						for(;ind < dateFormatText.length() && dateFormatText.charAt(ind) == c; ind++) { }
						result.append(DateFormatPatternEnum.getPatternToBeReplaced(dateFormatText.substring(start, ind)));
						if (ind < dateFormatText.length()) {
							result.append(dateFormatText.substring(ind, ind+1));
						}
					}
				}
			}

			return result.toString();
		} if (key.startsWith(LogHeaderPatternTypeEnum.UNKNOWN.name() + ":")) {
			String acceptedChars = key.replace(LogHeaderPatternTypeEnum.UNKNOWN.name() + ":", "");
			// if the accepted chars contain the '.' (matching any characters) then only the '.' will be taken into account -> I put this info to management web app / system admin / edit form / Log header pattern textfield
			if (acceptedChars.contains(".")) {
				return ".+";
			} else {
				return "[" + acceptedChars + "]+";
			}
		} if (key.equals(LogHeaderPatternTypeEnum.LOGLEVEL.name())) {
			return LogHeaderPatternTypeEnum.valueOf(key).getRegExp();
		} else {
			throw new ReactionRuntimeException("Unknown log header pattern type! " + key);
		}
	}


	private void setValue(String key, Object value) {
		if (key.equals(LogHeaderPatternTypeEnum.LOGLEVEL.name())) {
			this.logLevel = (String) value;
		}
		else if (key.equals(LogHeaderPatternTypeEnum.UNKNOWN.name())) {
			this.unknown = (List<String>) value;
		}
		else if (key.startsWith(LogHeaderPatternTypeEnum.DATE.name() + ":")) {
			// getting the date format and parse the date
			// 'DATE:dd-MMM-yyyy HH:mm:ss'
			String dateFormatText = key.replace(LogHeaderPatternTypeEnum.DATE.name() + ":", "");
			try {
				Date date = new SimpleDateFormat(dateFormatText).parse((String)value);
				dateTime = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
			} catch(IllegalArgumentException iae) {
				LOGGER.warn("The dateformate string ({}) in DATE is not valid! The value won't be taken into account.", dateFormatText);
				LOGGER.warn("The exception is", iae);
			} catch(ParseException pe) {
				LOGGER.warn("The value ({}) cannot be parsed to date! The value won't be taken into account.", value);
				LOGGER.warn("The exception is", pe);
			}
		}
	}


	private String escapeSpecChars(String pattern) {
		String escaped = pattern.replaceAll("[\\<\\(\\[\\{\\^\\-\\=\\$\\!\\|\\]\\}\\)‌​\\?\\+\\>]", "\\\\$0");
		return escaped;
	}

	private String escapeSpecChars2(String pattern) {
		String escaped = pattern.replaceAll("[\\<\\(\\[\\{\\^\\-\\=\\$\\!\\|\\]\\}\\)‌​\\?\\+\\>\\.\\*\\|]", "\\\\$0");
		return escaped;
	}
	
	
	public String getLogLevel() {
		return logLevel;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getUnknown() {
		return unknown;
	}
	
	public LocalDateTime getDateTime() {
		return dateTime;
	}


	public void setEventSource(EventSource eventSource) {
		this.eventSource = eventSource;
	}

}
