package org.reaction.common.log;


// https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
public enum DateFormatPatternEnum {

	// MMMM -> February, MMM -> Feb, MM -> 02, M -> 2
	M_N("[M]{5,100}", "[a-zA-Z]{2,20}"),
	MMMM("[M]{4}", "[a-zA-Z]{2,20}"),
	MMM("[M]{3}", "[a-zA-Z]{2,4}"),
	MM("[M]{2}", "[0-9]{2}"),
	M("[M]{1}", "[0-9]{1,2}"),
	// EEEE -> Wednesday, EEE -> Wed, EE -> Wed, E -> Wed
	E_N("[E]{5,100}", "[a-zA-Z]{2,20}"),
	EEEE("[E]{4}", "[a-zA-Z]{2,20}"),
	EEE("[E]{3}", "[a-zA-Z]{2,4}"),
	EE("[E]{2}", "[a-zA-Z]{2,4}"),
	E("[E]{1}", "[a-zA-Z]{2,4}"),
	// yyyy -> 2017, yyy -> 2017, yy -> 17, y -> 2017
	y_N("[y]{5,100}", "[0-9]{5,100}"),
	yyyy("[y]{4}", "[0-9]{4}"),
	yyy("[y]{3}", "[0-9]{4}"),
	yy("[y]{2}", "[0-9]{2}"),
	y("[y]{1}", "[0-9]{4}"),
	// DDDD -> 0291, DDD -> 291, DD -> 291, D -> 291
	// H, k, K, h, m, d, F, u, w, W, S, s (see at D)
	D_N("[D]{5,100}|[H]{5,100}|[k]{5,100}|[K]{5,100}|[h]{5,100}|[m]{5,100}|[d]{5,100}|[F]{5,100}|[u]{5,100}|[w]{5,100}|[W]{5,100}|[S]{5,100}|[s]{5,100}", "[0-9]{5,100}"),
	DDDD("[D]{4}|[H]{4}|[k]{4}|[K]{4}|[h]{4}|[m]{4}|[d]{4}|[F]{4}|[u]{4}|[w]{4}|[W]{4}|[S]{4}|[s]{4}", "[0-9]{4}"),
	DDD("[D]{3}|[H]{3}|[k]{3}|[K]{3}|[h]{3}|[m]{3}|[d]{3}|[F]{3}|[u]{3}|[w]{3}|[W]{3}|[S]{3}|[s]{3}", "[0-9]{3}"),
	DD("[D]{2}|[H]{2}|[k]{2}|[K]{2}|[h]{2}|[m]{2}|[d]{2}|[F]{2}|[u]{2}|[w]{2}|[W]{2}|[S]{2}|[s]{2}", "[0-9]{2,3}"),
	D("[D]{1}|[H]{1}|[k]{1}|[K]{1}|[h]{1}|[m]{1}|[d]{1}|[F]{1}|[u]{1}|[w]{1}|[W]{1}|[S]{1}|[s]{1}", "[0-9]{1,3}"),
	// a always AM or PM
	a_N("[a]{1,100}", "[A-Z]{2}"),
	// G always AD
	G_N("[G]{1,100}", "[AD]{2}"),
	// z, Z
	z_N("[z]{1,100}", "[a-zA-Z 0-9\\-\\:\\+]{1,40}"),
	Z_N("[Z]{1,100}", "[0-9\\-\\+]{5}")
	;
	
	private String patternToBeExamined;
	private String patternToBeReplaced;
	
	private DateFormatPatternEnum(String patternToBeExamined, String patternToBeReplaced) {
		this.patternToBeExamined = patternToBeExamined;
		this.patternToBeReplaced = patternToBeReplaced;
	}

	public String getPatternToBeExamined() {
		return patternToBeExamined;
	}

	public String getPatternToBeReplaced() {
		return patternToBeReplaced;
	}
	
	public static String getPatternToBeReplaced(String patternToBeExamined) {
		for (DateFormatPatternEnum dateFormatPattern : DateFormatPatternEnum.values()) {
			if (patternToBeExamined.matches(dateFormatPattern.getPatternToBeExamined())) {
				return dateFormatPattern.getPatternToBeReplaced();
			}
		}
		return "";
	}
	
}
