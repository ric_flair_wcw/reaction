package org.reaction.common.log;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Test;
import org.reaction.common.domain.EventSource;

public class LogAnalyzerTest {

	private LogAnalyzer instance;
	
	
	@Before
	public void setUp() {
		instance = new LogAnalyzer();
	}
	
	
	@Test
	public void testProcess_successful_notAll() {
		String line = "..* 2017-01-07 14:35:56,366 [DEBUG].*root: SystemList is called - admin_system.views";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("..* [~DATE:yyyy-MM-dd HH:mm:ss,SSS] [[~LOGLEVEL]].*[~UNKNOWN:a-z]:")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertTrue(b);
		assertEquals("2017-01-07 14:35:56,366", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS").format(instance.getDateTime()));
		assertEquals("DEBUG", instance.getLogLevel());
		assertEquals(1, instance.getUnknown().size());
		assertEquals("root", instance.getUnknown().get(0));
	}
	
	
	@Test
	public void testProcess_successful_notAll2() {
		String line = "2017-03-01 16:01:58,027 INFO  [stdout] (Hibernate Search: collectionsloader-3)         this_.DICT_ENTRY_ID as DICT9_27_0_";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~UNKNOWN:a-z]] ([~UNKNOWN:a-zA-z: \\-0-9])")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertTrue(b);
		assertEquals("2017-03-01 16:01:58,027", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS").format(instance.getDateTime()));
		assertEquals("INFO", instance.getLogLevel());
		assertEquals(2, instance.getUnknown().size());
		assertEquals("stdout", instance.getUnknown().get(0));
		assertEquals("Hibernate Search: collectionsloader-3", instance.getUnknown().get(1));
	} 
	

	@Test
	public void testProcess_successful_notAll3() {
		String line = "..INFO 2017-03-01 16:01:58,027.  [stdout] (Hibernate Search: collectionsloader-3)         this_.DICT_ENTRY_ID as DICT9_27_0_";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("..[~LOGLEVEL] [~DATE:yyyy-MM-dd HH:mm:ss,SSS].  [[~UNKNOWN:a-z]] ([~UNKNOWN:a-zA-z: \\-0-9])")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertTrue(b);
		assertEquals("2017-03-01 16:01:58,027", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS").format(instance.getDateTime()));
		assertEquals("INFO", instance.getLogLevel());
		assertEquals(2, instance.getUnknown().size());
		assertEquals("stdout", instance.getUnknown().get(0));
		assertEquals("Hibernate Search: collectionsloader-3", instance.getUnknown().get(1));
	} 


	@Test
	public void testProcess_successful_all() {
		String line = "2017-01-07 14:35:56,366 *kutty kurutty* [DEBUG] {ORA-676784} [LogMa\\nager] - No more - root: SystemList is called - admin_system.views";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] *[~UNKNOWN:a-z ]* [[~LOGLEVEL]] {[~UNKNOWN:A-Z\\-0-9]} [[~UNKNOWN:\\\\a-zA-Z]] - [~UNKNOWN:a-zA-Z ] - [~UNKNOWN:a-z]:")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertTrue(b);
		assertEquals("2017-01-07 14:35:56,366", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS").format(instance.getDateTime()));
		assertEquals("DEBUG", instance.getLogLevel());
		assertEquals(5, instance.getUnknown().size());

		assertEquals("kutty kurutty", instance.getUnknown().get(0));
		assertEquals("ORA-676784", instance.getUnknown().get(1));
		assertEquals("LogMa\\nager", instance.getUnknown().get(2));
		assertEquals("No more", instance.getUnknown().get(3));
		assertEquals("root", instance.getUnknown().get(4));
	}

	
//	@Test
	// it is failing because there is a bug in the BRACKET_PATTERN but not sure how to fix it...
	//      reg exp: \[\~(?:[^\[\~\]]|(?<=\\)\])*(?<!\\)\]
	//      text:    [[~UNKNOWN:a-zA-Z\\]] - [~UNKNOWN:a-zA-Z ]
	public void testProcess_successful_bug() {
		String line = "2017-01-07 14:35:56,366 *kutty kurutty* [DEBUG] {ORA-676784} [LogMa\\nager] - No more - root: SystemList is called - admin_system.views";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] *[~UNKNOWN:a-z ]* [[~LOGLEVEL]] {[~UNKNOWN:A-Z\\-0-9]} [[~UNKNOWN:a-zA-Z\\\\]] - [~UNKNOWN:a-zA-Z ] - [~UNKNOWN:a-z]:").build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertTrue(b);
		assertEquals("2017-01-07 14:35:56,366", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS").format(instance.getDateTime()));
		assertEquals("DEBUG", instance.getLogLevel());
		assertEquals(4, instance.getUnknown().size());

		assertEquals("kutty kurutty", instance.getUnknown().get(0));
		assertEquals("ORA-676784", instance.getUnknown().get(1));
		assertEquals("LogManager", instance.getUnknown().get(2));
		assertEquals("root", instance.getUnknown().get(3));
	}

	
	@Test
	public void testProcess_unsuccessful_noHeader() {
		String line = "..* 2017-01-07 14:35:56,366 [DEBUG].*root: SystemList is called - admin_system.views";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [[~LOGLEVEL]] [~UNKNOWN:a-z]:")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertFalse(b);
	}


	@Test
	// the text contains 2 ')' characters so the UNKNOWN cannot decide which one is the last one if not specifying that ')' is not accepted for the 2nd UNKNOWN
	public void testProcess_successful_more_parentheses() {
		String line = "2017-03-01 16:01:58,027 INFO  [stdout] (Hibernate Search: collectionsloader-3)         (this_.DICT_ENTRY_ID) as DICT9_27_0_";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~UNKNOWN:a-z]] ([~UNKNOWN:a-zA-z: \\-0-9])")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertTrue(b);
		assertEquals("2017-03-01 16:01:58,027", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS").format(instance.getDateTime()));
		assertEquals("INFO", instance.getLogLevel());
		assertEquals(2, instance.getUnknown().size());
		assertEquals("stdout", instance.getUnknown().get(0));
		assertEquals("Hibernate Search: collectionsloader-3", instance.getUnknown().get(1));
	} 

	
	@Test
	// the pattern of the UNKNOWN can contain a ] character
	public void testProcess_successful_square_bracket() {
		String line = "2017-03-01 16:01:58,027 INFO  [stdout] (Hibernate Searc]]h: collectionsloader-3)         (this_.DICT_ENTRY_ID) as DICT9_27_0_";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("[~DATE:yyyy-MM-dd HH:mm:ss,SSS] [~LOGLEVEL]  [[~UNKNOWN:a-z]] ([~UNKNOWN:\\]a-zA-z: \\-0-9])")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertTrue(b);
		assertEquals("2017-03-01 16:01:58,027", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS").format(instance.getDateTime()));
		assertEquals("INFO", instance.getLogLevel());
		assertEquals(2, instance.getUnknown().size());
		assertEquals("stdout", instance.getUnknown().get(0));
		assertEquals("Hibernate Searc]]h: collectionsloader-3", instance.getUnknown().get(1));
	} 


	@Test
	public void testProcess_unsuccessful_unknown_not_match() {
		String line = "..* 2017-01-07 14:35:56,366 [DEBUG].*root: SystemList is called - admin_system.views";
		
		EventSource eventSource = EventSource.builder().logHeaderPattern("..* [~DATE:yyyy-MM-dd HH:mm:ss,SSS] [[~LOGLEVEL]].*[~UNKNOWN:0-9]:")
				                     .logHeaderPatternEnabled(true).build();
		instance.setEventSource(eventSource);
		
		boolean b = instance.process(line);
		
		assertFalse(b);
	}
	
	
	@Test
	public void testBuildRegExpOfKey_successful() {
		String key = "DATE:___ MMM dd, yyyy HH:mm:ss,SSS a";
		String changed = instance.buildRegExpOfKey(key);
		
		assertEquals("___ [a-zA-Z]{2,4} [0-9]{2,3}, [0-9]{4} [0-9]{2,3}:[0-9]{2,3}:[0-9]{2,3},[0-9]{3} [A-Z]{2}", changed);
	}
	
}
