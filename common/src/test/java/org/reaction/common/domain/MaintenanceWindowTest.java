package org.reaction.common.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.reaction.common.exception.ReactionRuntimeException;


//@RunWith(SpringJUnit4ClassRunner.class)
public class MaintenanceWindowTest {

	private MaintenanceWindow instance;
	private Clock mockedClock;
	
	
	@Before
	public void setUp() {
		mockedClock = mock(Clock.class);
	}
	
	
	@Test
	public void testConstructor_successful() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);
		verifyNoMoreInteractions(mockedClock);
	}

	@Test
	public void testConstructor_unsuccessful_invalidJsonText() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  Tue [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";
		try {
			instance = new MaintenanceWindow(json, mockedClock);
			fail("ReactionRuntimeException should have been thrown!");
		} catch(ReactionRuntimeException e) {
			assertEquals("The value in the maintenanceWindow column is not a valid JSON!", e.getMessage());
		}
		verifyNoMoreInteractions(mockedClock);
	}
	
	
	// I will pass Wednesday 23:11 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";
		instance = new MaintenanceWindow(json, mockedClock);
		
		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-25T23:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();		
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Friday 01:11 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_notInTheMaintenanceWindow2() {
		String json =
				"{\"Mon\": [\"01:03-02:50\", \"12:12-22:00\", \"01:03-02:50\"], " +
				"\"Tue\": [\"01:03-02:50\", \"12:12-22:00\", \"01:03-02:50\"]}";
		instance = new MaintenanceWindow(json, mockedClock);
		
		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-21T01:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertFalse(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();		
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass Sunday 01:11 that is in the maintenance window (-> \"Sat\": [\"20:00-04:00\"])
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_overflow() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-29T03:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass Sunday 01:11 that is in the maintenance window (-> \"Sat\": [\"20:00-01:12\"]) and minute is set in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_overflow_minInWindow() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-01:12\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-29T01:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Sunday 01:13 that is in the maintenance window (-> \"Sat\": [\"20:00-01:12\"]) and minute is set in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_unsuccessful_inTheMaintenanceWindow_overflow_minInWindow() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-01:12\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-29T01:13:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertFalse(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass Monday 23:11 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_overflow2() {
		String json =
				"{ \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-30T23:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Tuesday 00:11 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_overflow3() {
		String json =
				"{ \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-24T00:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Sunday 03:11 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_overflow4() {
		String json =
				"{ \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-29T03:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Sunday 03:11 that is in the maintenance window
	//   it is not in the window of Sunday but in the overflowing window of Saturday
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_overflow5() {
		String json =
				"{ \"Sun\": [\"14:00-17:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-30T03:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Sunday 03:11 that is in the maintenance window
	//   the maintenance window is open for the whole day
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_wholeDay() {
		String json = "{ \"Sun\": [\"00:00-24:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-30T03:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass Wednesday 23:11 that should be in the maintenance window but the json is wrong
	@Test
	public void testIsInMaintenanceWindow_unsuccessful_wrongDayName() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-23:30\", \"23:50-04:00\"]," +
				"  \"Wep\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-25T23:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertFalse(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Sunday 15:11 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_unsuccessful_wrongPeriod() {
		String json =
				"{ \"Sun\": [\"14:00=04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-29T15:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		try {
			instance.isInMaintenanceWindow(null);
			fail("ReactionRuntimeException should have been thrown!");
		} catch(ReactionRuntimeException e) {
			assertEquals("The value '" + "14:00=04:00" + "' is not a valid time period (01:00-04:00)!", e.getMessage());
		}
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass 2017.01.29 Sunday 22:11
	@Test
	public void testNextInMaintenanceWindow_successful1() {
		String json =
				"{ \"Sun\": [\"14:00-22:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-29T22:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		Calendar next = instance.nextInMaintenanceWindow(0);
		
		// check
		// Monday 01:30
		assertEquals(2017, next.get(Calendar.YEAR));
		assertEquals(0, next.get(Calendar.MONTH));
		assertEquals(30, next.get(Calendar.DAY_OF_MONTH));
		assertEquals(1, next.get(Calendar.HOUR_OF_DAY));
		assertEquals(30, next.get(Calendar.MINUTE));
		assertEquals(0, next.get(Calendar.SECOND));
		assertEquals(0, next.get(Calendar.MILLISECOND));
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass 2017.01.30 Monday 01:11
	@Test
	public void testNextInMaintenanceWindow_successful2() {
		String json =
				"{ \"Sun\": [\"14:00-22:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json,mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-30T01:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		Calendar next = instance.nextInMaintenanceWindow(0);
		
		// check
		// Monday 01:30
		assertEquals(2017, next.get(Calendar.YEAR));
		assertEquals(0, next.get(Calendar.MONTH));
		assertEquals(30, next.get(Calendar.DAY_OF_MONTH));
		assertEquals(1, next.get(Calendar.HOUR_OF_DAY));
		assertEquals(30, next.get(Calendar.MINUTE));
		assertEquals(0, next.get(Calendar.SECOND));
		assertEquals(0, next.get(Calendar.MILLISECOND));
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass 2017.04.23 Sunday 01:11
	@Test
	public void testNextInMaintenanceWindow_successful3() {
		String json = "{ \"Mon\": [\"00:00-24:00\"] }";		
		instance = new MaintenanceWindow(json,mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-23T01:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		Calendar next = instance.nextInMaintenanceWindow(0);
		
		// check
		// Monday 01:11
		assertEquals(2017, next.get(Calendar.YEAR));
		assertEquals(3, next.get(Calendar.MONTH));
		assertEquals(24, next.get(Calendar.DAY_OF_MONTH));
		assertEquals(0, next.get(Calendar.HOUR_OF_DAY));
		assertEquals(0, next.get(Calendar.MINUTE));
		assertEquals(0, next.get(Calendar.SECOND));
		assertEquals(0, next.get(Calendar.MILLISECOND));
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass 2018.01.15 Monday 01:11
	@Test
	public void testNextInMaintenanceWindow_successful4_notOutsideBuInsideMaintenanceWindow() {
		String json = "{ \"Mon\": [\"01:00-03:00\"] }";		
		instance = new MaintenanceWindow(json,mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2018-01-15T01:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		Calendar next = instance.nextInMaintenanceWindow(0);
		
		// check
		// Monday 01:11
		assertEquals(2018, next.get(Calendar.YEAR));
		assertEquals(0, next.get(Calendar.MONTH));
		assertEquals(15, next.get(Calendar.DAY_OF_MONTH));
		assertEquals(1, next.get(Calendar.HOUR_OF_DAY));
		assertEquals(11, next.get(Calendar.MINUTE));
		assertEquals(0, next.get(Calendar.SECOND));
		assertEquals(0, next.get(Calendar.MILLISECOND));
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass 2018.01.15 Monday 01:11
	@Test
	public void testNextInMaintenanceWindow_successful5_notOutsideBuInsideMaintenanceWindow_currentDate() {
		String json = "{ \"Mon\": [\"01:00-03:00\"] }";		
		instance = new MaintenanceWindow(json,mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2018-01-15T01:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		Calendar next = instance.nextInMaintenanceWindow(70);
		
		// check
		// Monday 01:11
		assertEquals(2018, next.get(Calendar.YEAR));
		assertEquals(0, next.get(Calendar.MONTH));
		assertEquals(15, next.get(Calendar.DAY_OF_MONTH));
		assertEquals(1, next.get(Calendar.HOUR_OF_DAY));
		assertEquals(11, next.get(Calendar.MINUTE));
		assertEquals(0, next.get(Calendar.SECOND));
		assertEquals(0, next.get(Calendar.MILLISECOND));
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass 2018.01.15 Monday 02:11
	@Test
	public void testNextInMaintenanceWindow_successful6_notOutsideBuInsideMaintenanceWindow_nextInWindow() {
		String json = "{ \"Mon\": [\"01:00-03:00\"], "+
		         	  "  \"Tue\": [\"23:00-04:00\"]}";
		instance = new MaintenanceWindow(json,mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2018-01-15T02:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		Calendar next = instance.nextInMaintenanceWindow(70);
		
		// check
		// Monday 01:11
		assertEquals(2018, next.get(Calendar.YEAR));
		assertEquals(0, next.get(Calendar.MONTH));
		assertEquals(16, next.get(Calendar.DAY_OF_MONTH));
		assertEquals(23, next.get(Calendar.HOUR_OF_DAY));
		assertEquals(00, next.get(Calendar.MINUTE));
		assertEquals(0, next.get(Calendar.SECOND));
		assertEquals(0, next.get(Calendar.MILLISECOND));
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass Wednesday 23:11 and 60 min execution time that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_withExecTime() {
		String json =
				"{ \"Sun\": [\"14:00-04:00\"]," +
				"  \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Wed\": [\"23:00-04:00\"]," +
				"  \"Thu\": [\"23:00-01:00\", \"01:30-02:00\"]," +
				"  \"Fri\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";
		instance = new MaintenanceWindow(json, mockedClock);
		
		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-25T23:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(60);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();		
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Tuesday 00:11 and 48 min execution time that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_inTheMaintenanceWindow_overflow3_withExecTime() {
		String json =
				"{ \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-24T00:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(48);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Tuesday 00:11  and 50 min execution time that is NOT in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_notInTheMaintenanceWindow_overflow3_withExecTime() {
		String json =
				"{ \"Mon\": [\"23:00-01:00\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-24T00:11:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(50);
		
		// check
		assertFalse(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Tuesday 00:10  and 51 min execution time that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_notInTheMaintenanceWindow_overflow3_withExecTime_inWindow() {
		String json =
				"{ \"Mon\": [\"23:00-01:01\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-24T00:10:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(51);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass Tuesday 00:11  and 50 min execution time that is NOT in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_notInTheMaintenanceWindow_overflow3_withExecTime_notInWindow() {
		String json =
				"{ \"Mon\": [\"23:00-01:01\", \"01:30-02:00\", \"03:00-06:00\"]," +
				"  \"Tue\": [\"23:00-04:00\"]," +
				"  \"Sat\": [\"20:00-04:00\"] }";		
		instance = new MaintenanceWindow(json, mockedClock);

		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-01-24T00:10:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));
		
		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(52);
		
		// check
		assertFalse(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();
		verifyNoMoreInteractions(mockedClock);
	}

	
	// I will pass Friday 19:10:34 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_atTheEdge_start() {
		String json =
				"{\"Mon\": [\"01:03-02:50\", \"12:12-22:00\", \"01:03-02:50\"], " +
				"\"Fri\": [\"19:00-23:59\"]}";
		instance = new MaintenanceWindow(json, mockedClock);
		
		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-21T19:00:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();		
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Friday 23:59:34 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_atTheEdge_end() {
		String json =
				"{\"Mon\": [\"01:03-02:50\", \"12:12-22:00\", \"01:03-02:50\"], " +
				"\"Fri\": [\"19:00-23:59\"]}";
		instance = new MaintenanceWindow(json, mockedClock);
		
		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-21T23:59:34.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertTrue(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();		
		verifyNoMoreInteractions(mockedClock);
	}


	// I will pass Friday 23:59:34 that is in the maintenance window
	@Test
	public void testIsInMaintenanceWindow_successful_overTheEdge_end() {
		String json =
				"{\"Mon\": [\"01:03-02:50\", \"12:12-22:00\", \"01:03-02:50\"], " +
				"\"Fri\": [\"19:00-22:59\"]}";
		instance = new MaintenanceWindow(json, mockedClock);
		
		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-21T23:00:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertFalse(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();		
		verifyNoMoreInteractions(mockedClock);
	}
	

	// I will pass Friday 13:59:34 that is NOT in the maintenance window and the previous day is open the whole day
	@Test
	public void testIsInMaintenanceWindow_unsuccessful_currentNotIn_previousFullDay() {
		String json =
				"{\"Thu\": [\"00:00-24:00\"], " +
				"\"Fri\": [\"19:00-05:00\"]}";
		instance = new MaintenanceWindow(json, mockedClock);
		
		// creating the expectations in mocks
		//    for mockedClock
		when(mockedClock.instant()).thenReturn(Instant.parse("2017-04-21T13:00:00.00Z"));
		when(mockedClock.getZone()).thenReturn(ZoneId.of("UTC"));

		// call the method to be tested
		boolean is = instance.isInMaintenanceWindow(null);
		
		// check
		assertFalse(is);
		verify(mockedClock, times(1)).instant();
		verify(mockedClock, times(1)).getZone();		
		verifyNoMoreInteractions(mockedClock);
	}
}

