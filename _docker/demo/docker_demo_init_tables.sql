-- password: reactionengine
Insert into auth_user (ID,PASSWORD,LAST_LOGIN,IS_SUPERUSER,USERNAME,FIRST_NAME,LAST_NAME,EMAIL,IS_STAFF,IS_ACTIVE,DATE_JOINED) 
	values (1,'pbkdf2_sha256$36000$x23MdRsom4au$LvZFMFy/VMZG5/5rafaUTi9OXRq5gIfIyPtH+dIO4lM=','2018-01-04 16:12:03.184455000',1,'admin','admin','admin','reaction.engine.sup@gmail.com',1,1,'2017-12-20 08:19:09.627678000');
-- password: reactionengine
Insert into auth_user (ID,PASSWORD,LAST_LOGIN,IS_SUPERUSER,USERNAME,FIRST_NAME,LAST_NAME,EMAIL,IS_STAFF,IS_ACTIVE,DATE_JOINED) 
	values (2,'pbkdf2_sha256$36000$x23MdRsom4au$LvZFMFy/VMZG5/5rafaUTi9OXRq5gIfIyPtH+dIO4lM=','2018-01-10 07:58:55.600277000',0,'vikhor','Viktor','Horvath','reaction.engine.sup@gmail.com',0,1,'2017-12-20 08:20:55.000000000');


Insert into PROFILE (ID,ACCESS_GROUPS,USER_ID) 
	values (1,'Middleware',2);


Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (1,2,19);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (2,2,20);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (3,2,21);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (4,2,22);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (5,2,23);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (6,2,24);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (7,2,25);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (8,2,26);
Insert into auth_user_user_permissions (ID,USER_ID,PERMISSION_ID) 
	values (9,2,27);


----------------------- adding Hermes restart flow ---------------------------
Insert into EVENT_SOURCE (ID,NAME,HOST,LOG_PATH,LOG_HEADER_PATTERN,LOG_HEADER_PATTERN_ENABLED,TYPE,LOG_LEVEL,MAINTENANCE_WINDOW,PARENT_ID)
	values (27,'Hermes CRM',null,'/home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain/servers/managed/logs/managed.out',null,0,'GROUP',null,'{"Mon": ["19:45-05:00"], "Tue": ["19:00-05:00"], "Wed": ["14:00-05:00"], "Thu": ["19:00-05:00"], "Fri": ["19:00-05:00"], "Sat": ["19:00-05:00"], "Sun": ["19:00-05:00"]}',null);
Insert into EVENT_SOURCE (ID,NAME,HOST,LOG_PATH,LOG_HEADER_PATTERN,LOG_HEADER_PATTERN_ENABLED,TYPE,LOG_LEVEL,MAINTENANCE_WINDOW,PARENT_ID)
	values (29,'Hermes - ACME654','ACME654',null,null,0,'ITEM',null,'{}',27);
Insert into EVENT_SOURCE (ID,NAME,HOST,LOG_PATH,LOG_HEADER_PATTERN,LOG_HEADER_PATTERN_ENABLED,TYPE,LOG_LEVEL,MAINTENANCE_WINDOW,PARENT_ID)
	values (28,'Hermes - ACME653','ACME653',null,null,0,'ITEM',null,'{}',27);


Insert into EXECUTION_FLOW (ID,NAME,TIME_BETWEEN_EXEC,EXECUTION_TIME,STATUS,ACCESS_GROUP,HOSTS) 
	values (43,'Hermes restart if out of memory error occurs',1200,60,'FROZEN','Middleware','ACME653,ACME654');


Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) values 
    (93,'Stop managed server on ACME653',1,'. /home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain/bin/setDomainEnv.sh && (echo "connect(''weblogic'',''Marcius29'',''t3://localhost:7001'')"; echo "shutdown(''managed'',''Server'',''true'',1000,force=''true'', block=''true'')") | java weblogic.WLST','vikhor','ACME653','COMMAND_BY_WORKER',null,null,null,43,null,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (94,'check if managed server stopped correctly on ACME653',2,'. /home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain/bin/setDomainEnv.sh && (echo "nmConnect(''weblogic'',''Marcius29'',''localhost'',''5556'',''base_domain'',''/home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain'',''plain'')"; echo "nmServerStatus(''managed'')") | java weblogic.WLST','vikhor','ACME653','COMMAND_BY_WORKER',null,'SHUT(?<COMMANDOUTPUT>[A-Z]+)',null,43,null,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (95,'If managed server stopped correctly on ACME653',3,null,null,null,'IF_ELSE','$COMMANDOUTPUT.equals("DOWN")',null,null,43,null,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (98,'Send mail that managed server did not stop on ACME653',1,null,null,null,'EMAIL_SENDING',null,null,'WARNING! managed server did not stop on ACME653',null,null,95);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (99,'Fail the flow as managed server did not stop on ACME653',2,null,null,null,'FAILURE',null,null,null,null,null,95);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (121,'__FAULTY_COMMAND__',1,'slepptt 3',null,'ACME653','COMMAND_BY_WORKER',null,null,null,null,95,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (96,'Start managed server on ACME653',2,'. /home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain/bin/setDomainEnv.sh && (echo "connect(''weblogic'',''Marcius29'',''t3://localhost:7001'')"; echo "start(''managed'',''Server'')") | java weblogic.WLST','vikhor','ACME653','COMMAND_BY_WORKER',null,'Server with name managed started (?<COMMANDOUTPUT>[a-z]+)fully',null,null,95,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (100,'If managed server started correctly on ACME653',3,null,null,null,'IF_ELSE','$COMMANDOUTPUT.equals("success")',null,null,null,95,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (119,'wait for 1 min',1,'sleep 60','vikhor','ACME653','COMMAND_BY_WORKER',null,null,null,null,100,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (105,'Stop managed server on ACME654',2,'. /home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain/bin/setDomainEnv.sh && (echo "connect(''weblogic'',''Marcius29'',''t3://localhost:7001'')"; echo "shutdown(''managed'',''Server'',''true'',1000,force=''true'', block=''true'')") | java weblogic.WLST','vikhor','ACME654','COMMAND_BY_WORKER',null,null,null,null,100,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (106,'Check if managed server stopped correctly on ACME654',3,'. /home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain/bin/setDomainEnv.sh && (echo "nmConnect(''weblogic'',''Marcius29'',''localhost'',''5556'',''base_domain'',''/home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain'',''plain'')"; echo "nmServerStatus(''managed'')") | java weblogic.WLST','vikhor','ACME654','COMMAND_BY_WORKER',null,'SHUT(?<COMMANDOUTPUT>[A-Z]+)',null,null,100,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (107,'If managed server stopped correctly on ACME654',4,null,null,null,'IF_ELSE','$COMMANDOUTPUT.equals("DOWN")',null,null,null,100,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (103,'Send mail that managed server did not start on ACME653',1,null,null,null,'EMAIL_SENDING',null,null,'WARNING! managed server did not start on ACME653',null,null,100);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (104,'Fail the flow as managed server did not start on ACME653',2,null,null,null,'FAILURE',null,null,null,null,null,100);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (113,'If managed server started correctly on ACME654',2,null,null,null,'IF_ELSE','$COMMANDOUTPUT.equals("success")',null,null,null,107,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (112,'Start managed server on ACME654',1,'. /home/vikhor/Downloads/wls-12/wls12213/user_projects/domains/base_domain/bin/setDomainEnv.sh && (echo "connect(''weblogic'',''Marcius29'',''t3://localhost:7001'')"; echo "start(''managed'',''Server'')") | java weblogic.WLST','vikhor','ACME654','COMMAND_BY_WORKER',null,'Server with name managed started (?<COMMANDOUTPUT>[a-z]+)fully',null,null,107,null);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (110,'Send mail that managed server did not stop on ACME654',1,null,null,null,'EMAIL_SENDING',null,null,'WARNING! managed server did not stop on ACME654',null,null,107);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (111,'Fail the flow as managed server did not stop on ACME654',2,null,null,null,'FAILURE',null,null,null,null,null,107);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (118,'Fail the flow as managed server did not start on ACME654',2,null,null,null,'FAILURE',null,null,null,null,null,113);
Insert into TASK (ID,NAME,ORDR,COMMAND,OS_USER,HOST,INTERNAL_TASK,IF_EXPRESSION,OUTPUT_PATTERN,MAIL_SUBJECT,EXECUTION_FLOW_ID,PRIMARY_TASK_ID,SECONDARY_TASK_ID) 
	values (116,'Send mail that managed server did not start on ACME654',1,null,null,null,'EMAIL_SENDING',null,null,'WARNING! managed server did not start on ACME654',null,null,113);


Insert into ERROR_DETECTOR (ID,NAME,MESSAGE_PATTERN,ACTIVATED,CONFIRMATION_NEEDED,MULTIPLE_EVENTS_CNT,MULTIPLE_EVENTS_TIMEFR,EXECUTION_FLOW_ID,EVENT_SOURCE_ID)
	values (21,'Restart Hermes if out of memory error occurs','.*java\.lang\.OutOfMemoryError: Java heap space.*',0,1,null,null,43,27);


Insert into EVENT (ID,IDENTIFIER,LOG_LEVEL,INITIATED_BY,STATUS,START_DATE,END_DATE,FIRST_EVENT_ARRIVED,MULTIPLE_EVENTS_COUNTER,ERROR_DETECTOR_ID,EXECUTION_FLOW_ID,SCHEDULED_EXECUTION_FLOW_ID,EVENT_SOURCE_ID)
	values (494,'30ae7849-ce92-4ff7-8724-db69bd7d7f00',null,'BY_LOG','IGNORED','2018-01-10 08:32:24.352000000','2018-01-10 08:32:24.352000000',null,null,21,43,null,28);
Insert into EVENT (ID,IDENTIFIER,LOG_LEVEL,INITIATED_BY,STATUS,START_DATE,END_DATE,FIRST_EVENT_ARRIVED,MULTIPLE_EVENTS_COUNTER,ERROR_DETECTOR_ID,EXECUTION_FLOW_ID,SCHEDULED_EXECUTION_FLOW_ID,EVENT_SOURCE_ID)
	values (479,'cdd57d61-f987-4718-bf14-d57bcdf9eb9c',null,'BY_LOG','FINISHED','2018-01-10 08:28:34.945000000','2018-01-10 08.35.58.602000000',null,null,21,43,null,28);


Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (480,'2018-01-10 08:27:50.607000000',-2,null,null,'CONFIRMATION_NEEDED',null,479,null);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (481,'2018-01-10 08:28:34.937000000',-2,null,null,'CONFIRMED_AND_FORCED_START','vikhor',479,null);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (482,'2018-01-10 08:29:04.233000000',0,'wls:/base_domain/serverConfig/> ',1,'FINISHED',null,479,93);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (484,'2018-01-10 08:29:25.996000000',1,'DOWN',1,'FINISHED',null,479,94);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (486,'2018-01-10 08:29:26.060000000',2,null,null,'FINISHED',null,479,95);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (487,'2018-01-10 08:30:38.492000000',3,null,0,'FAILED_AND_SKIPPED','vikhor',479,121);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (492,'2018-01-10 08:32:51.154000000',6,null,1,'FINISHED',null,479,119);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (495,'2018-01-10 08:33:38.375000000',7,'wls:/base_domain/serverConfig/> ',1,'FINISHED',null,479,105);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (489,'2018-01-10 08:31:46.534000000',4,'success',1,'FINISHED',null,479,96);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (491,'2018-01-10 08:31:46.800000000',5,null,null,'FINISHED',null,479,100);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (497,'2018-01-10 08:33:57.283000000',8,'DOWN',1,'FINISHED',null,479,106);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (499,'2018-01-10 08:33:57.380000000',9,null,null,'FINISHED',null,479,107);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (500,'2018-01-10 08:35:58.143000000',10,'success',1,'FINISHED',null,479,112);
Insert into EVENT_LIFE (ID,EVENT_DATE,ORDR,EXTRACTED_VALUE,EXT_COMMAND_SUCCESSFUL,STATUS,BY_WHOM,EVENT_ID,TASK_ID) 
	values (502,'2018-01-10 08:35:58.282000000',11,null,null,'FINISHED',null,479,113);


Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (485,1,484);
Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (488,1,487);
Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (490,1,489);
Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (483,1,482);
Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (493,1,492);
Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (496,1,495);
Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (498,1,497);
Insert into COMMANDS_TO_BE_EXECUTED (ID,IS_EXECUTED,EVENT_LIFE_ID) 
	values (501,1,500);


Insert into WORKER_STATUS (ID,HOST,REFRESH_EVENT_SOURCES,REPORT_EVENT,CHECK_COMMANDS,SEND_COMMAND_RESULT)
	values (1,'ACME653','2018-01-10 08:24:06.103000000','2018-01-10 08:32:24.264000000','2018-01-10 08:42:14.837000000','2018-01-10 08:32:51.115000000');
Insert into WORKER_STATUS (ID,HOST,REFRESH_EVENT_SOURCES,REPORT_EVENT,CHECK_COMMANDS,SEND_COMMAND_RESULT)
	values (92,'ACME654',null,null,'2018-01-10 08:42:16.438000000','2018-01-10 08:35:58.103000000');


UPDATE `EVENT` SET `message`='java.lang.OutOfMemoryError: Java heap space' WHERE `id`='479';
UPDATE `EVENT` SET `message`='java.lang.OutOfMemoryError: Java heap space', `reason`='The event will be ignored as a same execution flow is (was recently) under operation already! The identifier of this other event is cdd57d61-f987-4718-bf14-d57bcdf9eb9c (its status is STARTED).' WHERE `id`='494';


UPDATE `EVENT_LIFE` SET `history`='[ {\n  \"date\" : \"2018-01-10T08:27:50.607+0100\",\n  \"eventStatus\" : \"CONFIRMATION_NEEDED\"\n} ]' WHERE `id`='480';
UPDATE `EVENT_LIFE` SET `history`='[ {\n  \"date\" : \"2018-01-10T08:28:34.937+0100\",\n  \"eventStatus\" : \"CONFIRMED_AND_FORCED_START\"\n} ]' WHERE `id`='481';
UPDATE `EVENT_LIFE` SET `output`='Welcome to WebLogic Server Administration Scripting Shell\n\nType help() for help on available commands\n\nwls:/offline> connect(\'weblogic\',\'Marcius29\',\'t3://localhost:7001\')\nConnecting to t3://localhost:7001 with userid weblogic ...\nSuccessfully connected to Admin Server \"AdminServer\" that belongs to domain \"base_domain\".\n\nWarning: An insecure protocol was used to connect to the server. \nTo ensure on-the-wire security, the SSL port or Admin port should be used instead.\n\nwls:/base_domain/serverConfig/> shutdown(\'managed\',\'Server\',\'true\',1000,force=\'true\', block=\'true\')\nShutting down the server managed with force=true while connected to AdminServer ...\n.\nwls:/base_domain/serverConfig/> ', `history`='[ {\n  \"date\" : \"2018-01-10T08:28:34.945+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:28:34.977+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:28:38.567+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:29:04.233+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='482';
UPDATE `EVENT_LIFE` SET `output`='Initializing WebLogic Scripting Tool (WLST) ...\n\nWelcome to WebLogic Server Administration Scripting Shell\n\nType help() for help on available commands\n\nwls:/offline> nmConnect(\'weblogic\',\'Marcius29\',\'localhost\',\'5556\',\'base_domain\',\'/home/vikhor/ \nDownloads/wls-12/wls12213/user_projects/domains/base_domain\',\'plain\')\nConnecting to Node Manager ...\nSuccessfully Connected to Node Manager.\nwls:/nm/base_domain> nmServerStatus(\'managed\')\n\nSHUTDOWN\n\nwls:/nm/base_domain> ', `history`='[ {\n  \"date\" : \"2018-01-10T08:29:04.281+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:29:04.296+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:29:09.103+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:29:25.996+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='484';
UPDATE `EVENT_LIFE` SET `history`='[ {\n  \"date\" : \"2018-01-10T08:29:26.048+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:29:26.060+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='486';
UPDATE `EVENT_LIFE` SET `output`='Cannot run program \"slepptt 3\": error=2, No such file or directory', `history`='[ {\n  \"date\" : \"2018-01-10T08:29:26.069+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:29:26.079+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:29:29.369+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:29:29.404+0100\",\n  \"eventStatus\" : \"FAILED\"\n}, {\n  \"date\" : \"2018-01-10T08:30:38.492+0100\",\n  \"eventStatus\" : \"FAILED_AND_SKIPPED\"\n} ]' WHERE `id`='487';
UPDATE `EVENT_LIFE` SET `output`='Welcome to WebLogic Server Administration Scripting Shell\n\nType help() for help on available commands\n\nwls:/offline> connect(\'weblogic\',\'Marcius29\',\'t3://localhost:7001\')\nConnecting to t3://localhost:7001 with userid weblogic ...\nSuccessfully connected to Admin Server \"AdminServer\" that belongs to domain \"base_domain\".\n\nWarning: An insecure protocol was used to connect to the server. \nTo ensure on-the-wire security, the SSL port or Admin port should be used instead.\n\nwls:/base_domain/serverConfig/> start(\'managed\',\'Server\')\nStarting server managed .............................................\nServer with name managed started successfully\nwls:/base_domain/serverConfig/> ', `history`='[ {\n  \"date\" : \"2018-01-10T08:30:38.514+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:30:39.780+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:31:46.534+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='489';
UPDATE `EVENT_LIFE` SET `history`='[ {\n  \"date\" : \"2018-01-10T08:31:46.745+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:31:46.800+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='491';
UPDATE `EVENT_LIFE` SET `history`='[ {\n  \"date\" : \"2018-01-10T08:31:46.828+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:31:46.892+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:31:50.911+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:32:51.154+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='492';
UPDATE `EVENT_LIFE` SET `output`='Welcome to WebLogic Server Administration Scripting Shell\n\nType help() for help on available commands\n\nwls:/offline> connect(\'weblogic\',\'Marcius29\',\'t3://localhost:7001\')\nConnecting to t3://localhost:7001 with userid weblogic ...\nSuccessfully connected to Admin Server \"AdminServer\" that belongs to domain \"base_domain\".\n\nWarning: An insecure protocol was used to connect to the server. \nTo ensure on-the-wire security, the SSL port or Admin port should be used instead.\n\nwls:/base_domain/serverConfig/> shutdown(\'managed\',\'Server\',\'true\',1000,force=\'true\', block=\'true\')\nShutting down the server managed with force=true while connected to AdminServer ...\n......\nwls:/base_domain/serverConfig/> ', `history`='[ {\n  \"date\" : \"2018-01-10T08:32:51.262+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:32:51.323+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:32:52.809+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:33:38.375+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='495';
UPDATE `EVENT_LIFE` SET `output`='Initializing WebLogic Scripting Tool (WLST) ...\n\nWelcome to WebLogic Server Administration Scripting Shell\n\nType help() for help on available commands\n\nwls:/offline> nmConnect(\'weblogic\',\'Marcius29\',\'localhost\',\'5556\',\'base_domain\',\'/home/vikhor/ \nDownloads/wls-12/wls12213/user_projects/domains/base_domain\',\'plain\')\nConnecting to Node Manager ...\nSuccessfully Connected to Node Manager.\nwls:/nm/base_domain> nmServerStatus(\'managed\')\n\nSHUTDOWN\n\nwls:/nm/base_domain> ', `history`='[ {\n  \"date\" : \"2018-01-10T08:33:38.414+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:33:38.432+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:33:38.780+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:33:57.283+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='497';
UPDATE `EVENT_LIFE` SET `history`='[ {\n  \"date\" : \"2018-01-10T08:33:57.355+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:33:57.380+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='499';
UPDATE `EVENT_LIFE` SET `output`='Welcome to WebLogic Server Administration Scripting Shell\n\nType help() for help on available commands\n\nwls:/offline> connect(\'weblogic\',\'Marcius29\',\'t3://localhost:7001\')\nConnecting to t3://localhost:7001 with userid weblogic ...\nSuccessfully connected to Admin Server \"AdminServer\" that belongs to domain \"base_domain\".\n\nWarning: An insecure protocol was used to connect to the server. \nTo ensure on-the-wire security, the SSL port or Admin port should be used instead.\n\nwls:/base_domain/serverConfig/> start(\'managed\',\'Server\')\nStarting server managed .................................................................................................\nServer with name managed started successfully\nwls:/base_domain/serverConfig/> ', `history`='[ {\n  \"date\" : \"2018-01-10T08:33:57.405+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:33:57.434+0100\",\n  \"eventStatus\" : \"WAITING_FOR_EXECUTION\"\n}, {\n  \"date\" : \"2018-01-10T08:33:59.163+0100\",\n  \"eventStatus\" : \"EXECUTION_STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:35:58.143+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='500';
UPDATE `EVENT_LIFE` SET `history`='[ {\n  \"date\" : \"2018-01-10T08:35:58.199+0100\",\n  \"eventStatus\" : \"STARTED\"\n}, {\n  \"date\" : \"2018-01-10T08:35:58.282+0100\",\n  \"eventStatus\" : \"FINISHED\"\n} ]' WHERE `id`='502';


UPDATE `TASK` SET `mail_recipients`='reaction.engine.sup@gmail.com', `mail_content`='managed server didn\'t stop on ACME653\nPlease check the server' WHERE `id`='98';
UPDATE `TASK` SET `mail_recipients`='reaction.engine.sup@gmail.com', `mail_content`='managed server didn\'t start on ACME653\nPlease check the server' WHERE `id`='103';
UPDATE `TASK` SET `mail_recipients`='reaction.engine.sup@gmail.com', `mail_content`='managed server didn\'t stop on ACME654\nPlease check the server' WHERE `id`='110';
UPDATE `TASK` SET `mail_recipients`='reaction.engine.sup@gmail.com', `mail_content`='managed server didn\'t start on ACME654\nPlease check the server' WHERE `id`='116';


----------------------------- adding the 'when record worker status is used' flow ----------------------------------
INSERT INTO `event_source` VALUES (30,'local Reaction Management App log','ACME01','/tmp/reaction_debug.log','',0,'','ITEM',NULL,'{\"Mon\": [\"22:00-05:00\"], \"Tue\": [\"22:00-05:00\"], \"Wed\": [\"22:00-05:00\"], \"Thu\": [\"22:00-05:00\"], \"Fri\": [\"22:00-05:00\"], \"Sat\": [\"22:00-05:00\"], \"Sun\": [\"22:00-05:00\"]}',NULL);

INSERT INTO `execution_flow` VALUES (44,'Record current time',600,NULL,'VALID',NULL,'','','ACME01');

INSERT INTO `task` VALUES (124,'check if /tmp/reactionflow exists',1,'[ -d /tmp/reactionflow ] && echo The directory exists. || echo The directory DOES NOT exist.','root','ACME01','COMMAND_BY_WORKER',NULL,'The directory (?<COMMANDOUTPUT>[ A-Z]*) exist.',NULL,NULL,NULL,44,NULL,NULL);
INSERT INTO `task` VALUES (126,'if /tmp/reactionflow does NOT exist?',2,NULL,NULL,NULL,'IF_ELSE','$COMMANDOUTPUT.equals("DOES NOT")',NULL,NULL,NULL,NULL,44,NULL,NULL);
INSERT INTO `task` VALUES (127,'create the /tmp/reactionflow folder',1,'mkdir /tmp/reactionflow','','ACME01','COMMAND_BY_WORKER',NULL,'',NULL,NULL,NULL,NULL,126,NULL);
INSERT INTO `task` VALUES (122,'get hour of current time',3,'date +%H','','ACME01','COMMAND_BY_WORKER',NULL,'',NULL,NULL,NULL,44,NULL,NULL);
INSERT INTO `task` VALUES (123,'if it is in the afternoon?',4,NULL,NULL,NULL,'IF_ELSE','Integer.parseInt($COMMANDOUTPUT) > 12',NULL,NULL,NULL,NULL,44,NULL,NULL);
INSERT INTO `task` VALUES (129,'add the current time to /tmp/reactionflow/afternoon.txt',1,'echo The worker status was called on `date +%Y%m%d%H%M%S` >> /tmp/reactionflow/afternoon.txt','','ACME01','COMMAND_BY_WORKER',NULL,'',NULL,NULL,NULL,NULL,123,NULL);
INSERT INTO `task` VALUES (131,'add the current time to /tmp/reactionflow/morning.txt',1,'echo The worker status was called on `date +%Y%m%d%H%M%S` >> /tmp/reactionflow/morning.txt','','ACME01','COMMAND_BY_WORKER',NULL,'',NULL,NULL,NULL,NULL,NULL,123);

INSERT INTO `error_detector` VALUES (22,'Recording when the user clicked on the worker status in management app','.+Getting the status of the workers.+GET.+',1,1,NULL,NULL,44,30);
