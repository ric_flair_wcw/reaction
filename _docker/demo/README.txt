### [REACTION](http://reaction-engine.org)

The Docker image is only for demonstrating the capabilities of Reaction components.    
It (based on Ubuntu 18.10) contains all the components of Reaction:
* database: mariadb
* management app: Apache2, python3
* engine: Tomcat 8, openjdk 8
* worker

The image can be pulled with the following command:    
`docker pull reactionengine/demo:1.1`

The image can be started with the following command:    
`docker run -u reaction -it reactionengine/demo:1.1 /bin/bash`

Starting all the services after the docker container started:     
`/local/reaction/start_reaction_services.sh`

For more information please visit the [documentation](http://reaction-engine.org/documentation.html#A_3_3).