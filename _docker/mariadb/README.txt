### [REACTION](http://reaction-engine.org)

The Docker image contains a mariaDB 10.5.12 with the tables that Reaction components need.

The image can be pulled with the following command:    
`docker pull reactionengine/mariadb:1.2`

The image can be started with the following command:    
`docker run -d reactionengine/mariadb:1.2`
or with the following one if the MySQL passwords are set:
`docker run -d -e MYSQL_ROOT_PASSWORD=mysqlroot -e MYSQL_PASSWORD=ricflairthepresident reactionengine/mariadb:1.2`

MYSQL_ROOT_PASSWORD: This specifies the password that will be set for the MariaDB root superuser account.
The default value is 'root' if it is not set when running the container.
MYSQL_PASSWORD: This specifies the password of the 'reaction' database user.
The default value is 'reaction123' if it is not set when running the container.

The default mysql configuration can be overridden by adding the
`-v <path-to-config-on-host>/my.cnf:/etc/my.cnf`
parameters when running the container.

For more information please visit the [documentation](http://reaction-engine.org/documentation.html#A_3_4).