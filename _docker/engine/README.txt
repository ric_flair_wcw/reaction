### [REACTION](http://reaction-engine.org)

The Docker image contains openjdk-11, Tomcat 9.0.8 with the Reaction Engine deployed in it.     
It also has an Oracle Instant Client 12.2-basic installed if Oracle database has to be connected to.

The image can be pulled with the following command:    
`docker pull reactionengine/engine:1.2`

The image can be started with the following command:    
`docker run -d
            -v <path-to-config-on-host>/reaction-engine-application.yml:/local/reaction/engine/reaction-engine-application.yml
            -v <path-to-config-on-host>/logback-include.xml:/local/reaction/engine/logback-include.xml
            -v <path-to-config-on-host>/credentials:/local/reaction/engine/credentials
            -v <path-to-config-on-host>/password_encryption.key:/local/reaction/engine/password_encryption.key
            reactionengine/engine:1.2`

The folder on `<path-to-config-on-host>` must contain the config files enlisted above.

The MySQL database password of the 'reaction' database user can be specified by adding an environment variable:
`docker run ... -e MYSQL_PASSWORD=ricflairthepresident ...`
If the config files are overwritten by the volume parameter (see above) then the `MYSQL_PASSWORD` variable won't have any effect.

For more information please visit the [documentation](http://reaction-engine.org/documentation.html#A_3_1).