### [REACTION](http://reaction-engine.org)

The Docker image contains the Reaction Management web application deployed on Apache2 and using python3.6. The OS is ubuntu:18.04.
It also has an Oracle Instant Client 12.2-basic installed if Oracle database has to be connected to.

The image can be pulled with the following command:    
`docker pull reactionengine/management_app:1.2`

The image can be started with the following command:    
`docker run -d
            -v <path-to-config-on-host>/settings_reaction.py:/local/reaction/management_app/management_app/settings_reaction.py
            -v <path-to-config-on-host>/settings.py:/local/reaction/management_app/management_app/settings.py
            -v <path-to-config-on-host>/password_encryption.key:/local/reaction/management_app/management_app/password_encryption.key
            reactionengine/management_app:1.2`

The folder on `<path-to-config-on-host>` must contain the config files enlisted above.

The image can be brought up with default settings too by not defining the volume parameters:
`docker run -d reactionengine/management_app:1.2`

The MySQL database password of the 'reaction' database user can be specified by adding an environment variable:
`docker run -d -e MYSQL_PASSWORD=ricflairthepresident reactionengine/management_app:1.2`
If the config files are overwritten by the volume parameter (see above) then the `MYSQL_PASSWORD` variable won't have any effect.

For more information please visit the [documentation](http://reaction-engine.org/documentation.html#A_3_2).